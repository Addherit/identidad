﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad
{
    public partial class Admin : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        
        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //redireccion a https
                if (!Request.IsLocal && !Request.IsSecureConnection)
                {
                    string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                    Response.Redirect(redirectUrl, false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
                if (!IsPostBack)
                {

                    lblValidacion.Visible = false;

                    Response.CacheControl = "private";
                    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);

                   
                }
                else
                {
                    string eventTarget = Convert.ToString(Request.Params.Get("__EVENTTARGET"));
                    //if (eventTarget == "validaEnviar")
                    //{
                    //    btnEnviar_Click();
                    //}
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void lkbIniciarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                txtUsuario.Text = "";
                txtContraseña.Text = "";
                lblValidacion.Visible = false;
                txtUsuario.Focus();

                ppIniciarSesion.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            lblValidacion.Attributes["class"] = "divValidacion";
            try
            {
                ppIniciarSesion.Show();

                oMetodo = oAdmin.IniciarSesion(txtUsuario.Text, txtContraseña.Text);

                if (oMetodo.Pass == true)
                {
                    Session["IdUsuario"] = oAdmin.getIdUsuario(txtUsuario.Text);
                    Session["Admin"] = "Admin";
                    Session.Timeout = 480;

                    lblValidacion.Visible = true;
                    lblValidacion.Text = "¡Encontrado!";
                    Response.Redirect("Gestor/MenuAdmin.aspx",false);
                }
                else
                {
                    txtUsuario.Focus();

                    lblValidacion.Visible = true;
                    lblValidacion.Text = oMetodo.Message;
                }

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                ppIniciarSesion.Hide();
                
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

       
        protected void lkbRecuperarContraseña_Click(object sender, EventArgs e)
        {
            try
            {
                ppIniciarSesion.Show();

                oMetodo = oAdmin.RecuperarMiContraseña(txtUsuario.Text);

                lblValidacion.Text = oMetodo.Message;
                lblValidacion.Visible = true;
                lblValidacion.Attributes["class"] = lblValidacion.Attributes["class"].ToString() + " divValidacionVerde";

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
        #endregion

    }
}