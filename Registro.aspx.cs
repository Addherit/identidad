﻿using Identidad.App_Code;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Identidad
{
   
    public partial class Registro : System.Web.UI.Page
    {
        string sJson = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Request.Form["__EVENTTARGET"] == "Pagar_Click")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                sJson = Page.Request.Params["__EVENTARGUMENT"];
                if (sJson.ToString() != "")
                {
                    Pagar_Click(this, new EventArgs());
                }
            }

           
        }

        
        protected void Pagar_Click(object sender, EventArgs e)
        {
                    
                    string sURL = "";

                    //string description = "Servicio";
                    //int amount = 15000;
                    //card = card;
                    //string name = "Petronilo";            
                    
                    Request.Headers.Add("Accept", "application/vnd.conekta-v1.0.0+json");
                    Request.Headers.Add("Content-type", "application/json");
                    //Request.Headers.Add("User-Agent", "key_eYvWV7gSDkNYXsmr");

                    //tok_hadFmstbxt9AWyUA9
                    //tok_test_visa_4242
                    //tok_VGnyfSiAdr9z77kB4
            
                    sURL = "https://api.conekta.io/charges";
                    //sJson = "{\"description\": \" " + description + " \",\"amount\": "+ amount +",\"currency\":\"MXN\",\"card\": \"tok_test_visa_4242\",\"details\": {\"name\": \"Beddy Soto\",\"phone\": \"00000000000\",\"email\": \"beddy@beduardo.com\",\"line_items\": [{\"name\": \"Box of Cohiba S1s\",\"description\": \"Imported From Mex.\",\"unit_price\": 20000,\"quantity\": 1}] } }";
                    var response = cHTTPrequest.ObtenerListaWS(sURL, sJson);
                    Label1.Text = response;
                    //var items = JsonConvert.DeserializeObject<cJson2.ValidaUpdateDatosFac>(response);

                    

                }

        }
    }
