﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;

namespace Identidad
{
    public partial class index : System.Web.UI.Page
    {
        #region Variables
        string sJson = "";

        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        #endregion

        protected void Page_Load(object sender, EventArgs e)//Carga la pagina inicial
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            Response.Redirect("~/site/index.aspx",true);
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Request.Form["__EVENTTARGET"] == "recuperarPass")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                btnRecuperarPass(this, new EventArgs());
            }   
            else if(Request.Form["__EVENTTARGET"] == "login")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                btnIniciarSession(this, new EventArgs());
            }
            else if (Request.Form["__EVENTTARGET"] == "ir")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                btnGo_Click(this, new EventArgs());
            }
            else if (Request.Form["__EVENTTARGET"] == "enviar")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                btnEnviar_Click();
            }
            else if (Request.Form["__EVENTTARGET"] == "enviar")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                btnEnviar_Click();
            }
            else if (Request.Form["__EVENTTARGET"] == "Pagar_Click")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                sJson = Page.Request.Params["__EVENTARGUMENT"];
                if (sJson.ToString() != "")
                {
                    Pagar_Click(this, new EventArgs());
                }
            }

            llenarCombo();

        }

        protected void llenarCombo()
        {
            ddlPaisdf.Items.Clear();
            ddlPaisdf.Items.Add("País");

            DataTable dt = new DataTable();
           
            //dr = cmd.ExecuteReader;
            SqlCommand cmd = new SqlCommand("selPaises");
            cmd.CommandType = CommandType.StoredProcedure;
            
            //dt=oSistema.CargarDataTable(cmd);

            DataTable dtTable = new DataTable();

            dtTable = oSistema.CargarDataTable(cmd);
            foreach (DataRow dtRow in dtTable.Rows)
            {
                
                    string pais = dtRow[0].ToString();
                    ddlPaisdf.Items.Add(pais);
            }

            

            //while (dt.Read())
            //{
            //    ddlllenar.Items.Add(new ListItem(dt.GetString(0).ToString(), dt.GetInt32(1).ToString()));
            //}

           

            
        }
      
        protected void btnRecuperarPass(object sender, EventArgs e)//Funcion para recuperar la contraseña
        {
            try
            {                
                oMetodo = oUsuario.RecuperarMiContraseña(txtcorreoIngreso.Value.ToString());
                lblvalidacionRP.InnerText = oMetodo.Message;
                lblvalidacionRP.Visible = true;
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnIniciarSession(object sender, EventArgs e)//Funcion para recuperar la contraseña
        {
            try
            {
                oMetodo = oUsuario.IniciarSesion(txtcorreoIngreso.Value, txtpassIngreso.Value);

                if (oMetodo.Pass == true)
                {
                    Session["IdUsuario"] = oUsuario.getIdUsuario(txtcorreoIngreso.Value);
                    Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                    Session.Timeout = 480;

                    oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
                    if (ddlmissitios.Items.Count > 1)
                    {
                        //ppIniciarSesion.Hide();

                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                        oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
                        //this.popUpRegistrate.Attributes.Add("style", "display:block");
                        this.popUpMisSitios.Attributes.Add("style", "display:block");
                    }
                    else
                    {
                        if (ddlmissitios.Items.Count != 0)
                        {
                            HttpContext.Current.ApplicationInstance.CompleteRequest();

                            string[] sUrl = ddlmissitios.Items[0].Value.Split('=');
                            string sUrlFinal;

                            if (sUrl.Length == 1)
                            {
                                sUrlFinal = sUrl[0];
                            }
                            else
                            {
                                sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                            }

                            int vencido = oBrandSite.BrandSiteVencido(int.Parse(sUrl[1].ToString()));
                            if (vencido == 0)
                            {
                                HttpContext.Current.ApplicationInstance.CompleteRequest();
                                Response.Redirect("~/" + sUrlFinal, false);
                            }
                            else
                            {
                                Session["IdBrandSite"] = sUrl[1];
                                Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                                //MPE.Show();
                            }

                        }
                        else
                        {
                            lblvalidacionL.Visible = true;
                            lblvalidacionL.InnerText = "¡Sitio no disponible!";
                        }
                    }
                }
                else
                {
                    txtcorreoIngreso.Focus();

                    lblvalidacionL.Visible = true;
                    lblvalidacionL.InnerText = oMetodo.Message;
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlmissitios.SelectedValue.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnEnviar_Click()
        {
            if (txtcorreoIngreso.Value.ToString() != "" || txtComentariosC.Value.ToString() != "" || txtNombresC.Value.ToString() != "")
            {
                oMetodo = oUsuario.EnviarCorreoContactanos("22", txtcorreoIngreso.Value + " (" + txtNombresC.Value + " " + txtApellidosC.Value + ") quiere ponerse en contacto contigo.", txtComentariosC.Value);
                //oMetodo = oUsuario.EnviarCorreoContactanos("5", txtCorreo.Text + " (" + txtNombre.Text + ") quiere ponerse en contacto contigo.", txtComentarios.Text);
                //oMetodo = oUsuario.EnviarCorreoContactanos("8", txtCorreo.Text + " (" + txtNombre.Text + ") quiere ponerse en contacto contigo.", txtComentarios.Text);
                txtcorreoIngreso.Value = "";
                txtComentariosC.Value = "";
                txtNombresC.Value = "";
                txtApellidosC.Value = "";
            }
            else
            {
                lblmessage.Visible = true;
            }
        }

        protected void Pagar_Click(object sender, EventArgs e)
        {
            //if (txtNombresdf.Value != "" || txtRFCdf.Value != "" || txtApellidosdf.Value != "" || txtCalledf.Value != "")
            //{

                string sURL = "";
                
                
                Request.Headers.Add("Accept", "application/vnd.conekta-v1.0.0+json");
                Request.Headers.Add("Content-type", "application/json");

                sURL = "https://api.conekta.io/charges";
                //var sJson = "{\"description\": \"Descripcion \",\"amount\": 100,\"currency\":\"MXN\",\"card\": \"tok_test_visa_4242\",\"details\": {\"name\": \"Beddy Soto\",\"phone\": \"00000000000\",\"email\": \"beddy@beduardo.com\",\"line_items\": [{\"name\": \"Box of Cohiba S1s\",\"description\": \"Imported From Mex.\",\"unit_price\": 20000,\"quantity\": 1}] } }";
                var response = cHTTPrequest.ObtenerListaWS(sURL, sJson);
                //Label1.Text = response;
                string[] tokens = response.Split(',');
                label10.InnerHtml = response;

            //}
            //else
            //{
            //    ppPagos.Show();
            //    lblErrorPagos.Visible = true;
            //    lblErrorPagos.Text = "Faltan llenar campos de facturación o volver a elegir el Plan";
            //}

        }

    }
}