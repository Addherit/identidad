﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

namespace Identidad.Designer 
{
    public partial class EditorManual : System.Web.UI.Page
    {
        cEditorManual oEditorManual = new cEditorManual();
        cSistema           oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                
              
                if (!IsPostBack)
                {

                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Default.aspx", false);
                        return;
                    }
                    Session["IdIndiceSeleccionado"] = "";
                    Session["NombreMenu"] = "";
                    Session["HTML"] = "";
         
                    ObtenerIndices();
                    LlenaPadres();
                    CargaPlantillas();
                }
               

                //try {

                //    CargaElementosSeleccionados();      
                //}catch{}
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Default.aspx", false);
            }

        }

        //Obtiene el menu y lo manda 
        protected void ObtenerIndices() { 
        
            string sIndice=oEditorManual.creaNodos( Session["IdBrandSite"].ToString());
            divTree.InnerHtml=sIndice;


        }
        protected void LlenaPadres() {

            ddlPadres.DataSource = oEditorManual.ObtenerPadresBD(Session["IdBrandSite"].ToString());
            ddlPadres.DataValueField = "IdIndice";
            ddlPadres.DataTextField = "Nombre";
            ddlPadres.DataBind();
            ObtenerIndices();
         
        
        }

        protected void CargaElementosSeleccionados()
        {

            lblIdSeleccionado.Text = Session["IdIndiceSeleccionado"].ToString() == "" ? "" : Session["IdIndiceSeleccionado"].ToString();
            lblNombreMenu.Text = Session["NombreMenu"].ToString() == "" ? "" : Session["NombreMenu"].ToString();
            divHTML.InnerText = hdHTML.Value;
            
        }
        protected void CargaPlantillas() {

            rdblPlantilla.Items.Clear();

            foreach (DataRow row in oBrandSite.CargarPlantillas().Rows)
            {
                ListItem item = new ListItem();
                item.Value = row["IdPlantilla"].ToString();
                item.Text = String.Format("<img src=\"{0}\"/>", row["Preview"].ToString());

                //Agrega el Estilo al div para editarlo
                //  divEditor.InnerHtml = row["HTML"].ToString();

                rdblPlantilla.Items.Add(item);
            }
        
        }

        #region Metodos llamados desde JavaScript

       [WebMethod]
        public static void EnviaDatos(string JSON) //Metodo que se manda llamar desde JS (MenuIncia) por cada cambio en el menu, se guarda en DB
        {

            JavaScriptSerializer js = new JavaScriptSerializer();
            Menu[] mDatos = js.Deserialize<Menu[]>(JSON); //Deserializamos el obajeto JSON en la clase Menu para poder manipular los dtos

            EditorManual oEnvia = new EditorManual();
            oEnvia.EnviaDatos(mDatos);

        }


       [WebMethod]
       public static void GuardaNombreIndice(string sNombreIndice, string sIndice)  //Metodo que se llama desde JS para guardar el nombre del elemento
       {

         
           cEditorManual oEnvia = new cEditorManual();
           oEnvia.ModificaNombreNodo(sNombreIndice,sIndice);


       }

       [WebMethod]
       public static string[] CargaElementos(string sIndice) { //metodo que manda llamar el metodo que carga el contenido del indice seleccionado.

           EditorManual oEnvia = new EditorManual();
          return oEnvia.RegresaDatos(sIndice);

       }

        #endregion

        //Auxiliar para mandar la variable de session
       private void EnviaDatos(Menu[] mDatos) { 
       
        cEditorManual oEnvia= new cEditorManual(); //Se crea una instancia de la clase Editor manual para enviar informacion.
        oEnvia.EnviaInformacionBD(mDatos, Session["IdBrandSite"].ToString());
       
       }

       public string[] RegresaDatos(string sIdIndice) //Regresa los datos para llenar el lbl y el contenido al dar click.
       {
           try
           {
               cEditorManual oObtenerDatos = new cEditorManual(); //Se crea una instancia de la clase Editor manual para enviar informacion.
               DataTable dtDatos = oObtenerDatos.ObtenerDetalle(sIdIndice);

               string[] sArray = new string[3];
               Session["IdIndiceSeleccionado"] = sArray[0] = dtDatos.Rows[0][0].ToString();
               Session["NombreMenu"] = sArray[1] = dtDatos.Rows[0][1].ToString();
               Session["HTML"] = sArray[2] = dtDatos.Rows[0][2].ToString();
               
               return sArray;
           }
           catch (Exception ex) {
               return null;
           }

           return null;
       }

       protected void btnEliminarSeccion_Click(object sender, EventArgs e)
       {
           ppEliminar.Show();
       }

       protected void btnAgregarSeccion_Click(object sender, EventArgs e)
       {
           ppAgregarItemManual.Show();
       }

       protected void btnAgregarItem_Click(object sender, EventArgs e)
       {
           //Guarda el item.
           cEditorManual oGuardarItems = new cEditorManual();
           oGuardarItems.GuardarItems(txtNombreItemIndice.Text, ddlPadres.SelectedValue, Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
           

       }

       protected void btnEliminar_Click(object sender, EventArgs e)
       {
           string sIndiceEliminar = lblIdSeleccionado.Text;
           cEditorManual oEliminarItem = new cEditorManual();
           oEliminarItem.EliminarItemSeleccionado(sIndiceEliminar,Session["IdUSuario"].ToString());
          
           Session["IdIndiceSeleccionado"] = "";
           Session["NombreMenu"] = "";
           Session["HTML"] = "";
         
           CargaElementosSeleccionados();
           ObtenerIndices();
           LlenaPadres();
         
       }

       protected void btnPlantilla_Click(object sender, EventArgs e)
       {
           pPlantillas.Show();
       }


        
     


    }
}

//Esta clase es auxiliar nos sirve para obtener los datos del menu y poder guardar el orden.
public class Menu
{
    public int ID { get; set; } //ID que trae el menu
    public Menu [] children { get; set;} //se llama a la misma clase ya que tiene los mismos atributos.

}

