﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Identidad.Designer.WebForm1" meta:resourcekey="Home" %>

<%@ Register Src="~/Controles/Examinar.ascx" TagPrefix="identidad" TagName="Examinar" %>
<%@ Register Src="~/Controles/Pantones.ascx" TagPrefix="identidad" TagName="Pantones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/HomeDesigner.css" rel="stylesheet" type="text/css" />

   <div class="divFondoPagina">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Image ID="imgHome" runat="server" class="HomeBrandSite" style="cursor:default;" onclick="$get('ContentPlaceHolder1_lkbCambiarImagen').click();" />
                <br />
                <br />
                <asp:TextBox ID="txtBienvenida" ToolTip="Escribe aquí un texto que quisieras añadir en la portada de bienvenida." Style="margin-left:20px!important; width:980px!important;" CssClass="TextBox MultilineBienvenida" TextMode="MultiLine" runat="server"></asp:TextBox>
                <br />
                <br />
                <asp:Button ID="btnGuardarBienvenida" Style="margin-left:20px;" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnGuardarBienvenida" OnClick="btnGuardarBienvenida_Click"/>
                <asp:Label ID="lblValidacion" runat="server" visible="false"></asp:Label>
                <br />
                <br />
                <asp:LinkButton ID="lkbCambiarImagen" ToolTip="Puedes añadir una imagen aquí de un ancho de 1024 pixeles por la longitud que elijas. Ésta puede ser continuación de la imagen de portada o complementarla. Sé creativo e impactante, pues será lo primero que verán los visitantes." Style="margin-left:20px;" runat="server" meta:resourcekey="lkbCambiarImagen" OnClick="lkbCambiarImagen_Click"></asp:LinkButton>
                |
                <asp:LinkButton ID="lkbBorrarImagen" ToolTip="Por si decides dejar en blanco esta parte." runat="server" meta:resourcekey="lkbBorrarImagen" OnClick="lkbBorrarImagen_Click"></asp:LinkButton>
                |
                <asp:LinkButton ID="lkbCambiarFondo" ToolTip="Todas las páginas tendrán el color de fondo que elijas." runat="server" meta:resourcekey="lkbCambiarFondo" OnClick="lkbCambiarFondo_Click"></asp:LinkButton>
                <%--|
                <asp:LinkButton ID="lkbCambiarLogo" runat="server" meta:resourcekey="lkbCambiarLogo" OnClick="lkbCambiarLogo_Click"></asp:LinkButton>--%>
                <br />
                <br />
                <br />

                <identidad:Examinar runat="server" id="eExaminar" />
                <identidad:Examinar runat="server" id="eExaminarLogo" />
                <identidad:Pantones runat="server" ID="ePantones" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

     <script type="text/javascript">
         function reloadPage() {
             //alert('alert');
             // location.reload();
             // alert(document.URL);
             window.location.assign(document.URL);
             //  document.location.href = document.URL;
             //  location = document.URL;

         }


    </script>

    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

</asp:Content>
