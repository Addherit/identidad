﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;

namespace Identidad.Designer
{
    public partial class Historial : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/index.aspx", false);

                        return;
                    }

                    //oBrandSite.CargarNovedades(gvNovedades, Session["IdBrandSite"].ToString(), Session["IdUsuario"].ToString());
                    oBrandSite.FillHistorial(gvNovedades, Session["IdBrandSite"].ToString());
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/index.aspx", false);
            }
        }

        #region Eventos

        protected void lkbVerDetalle_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                lblNombreDetalle.Text = ((System.Web.UI.WebControls.Label)((gvNovedades.Rows[Row].FindControl("lblNombre")))).Text;
                lblFechaActivoDetalle.Text = ((System.Web.UI.WebControls.Label)((gvNovedades.Rows[Row].FindControl("lblFechaActivo")))).Text;

                ppVerDetalle.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCerrar_Click(object sender, EventArgs e)
        {
            try
            {
                ppVerDetalle.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion
    }
}
