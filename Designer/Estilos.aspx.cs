﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Identidad.App_Code;
using System.Text.RegularExpressions;
using System.Threading;

namespace Identidad.Designer
{
    public partial class WebForm5 : System.Web.UI.Page
    {
      
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();
        Thread HArchivo;
        
        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    Session["irow"] = "-1";
                    CargaSesiones();

                    if (lblIdUsuario.Text == "" || lblIdBrandiste.Text == "")
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);
                        return;
                    }
                   
                    cargarEditors();
                    Actualizar();
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        #region Métodos

        private void PonerColorABoton(string sHex)
        {
            System.Drawing.Color cColor = System.Drawing.ColorTranslator.FromHtml("#" + sHex);
            txtBGColorBoton.BackColor = cColor;
            txtBGColorBoton.Text = sHex;
            txtBGColorBoton.ForeColor = cColor;

            btnEjemplo.Style.Add("background", "#" + sHex + "!important");
        }

        private void Actualizar()
        {
            for (int Row = 0; Row < gvwEstilos.Rows.Count; Row++)
            {
                // Pone la fuente
                string sFuente = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpFuente")))))).SelectedItem.Text;
                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-family", sFuente);
                //Pone variante
                //string idFuente = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpFuente")))))).SelectedItem.Value;
                //oBrandSite.obtenerVariantesFuentes((((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpVariante"))))), idFuente);

                // Pone el tamaño

                //string sTamano = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpTamano")))))).SelectedItem.Text;

                int iTamaño = 12;
                string sTamano = string.Empty;

                if (int.TryParse(((((System.Web.UI.WebControls.TextBox)((gvwEstilos.Rows[Row].FindControl("txtTamano")))))).Text, out iTamaño) == true)
                {
                    sTamano = iTamaño.ToString();
                }
                else
                {
                    sTamano = "12";
                }
                if (iTamaño <= 50)
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-size", sTamano + "px");
                }
                else
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-size", "50" + "px");
            
                }
                // Pone negritas
                string sNegrita = ((System.Web.UI.WebControls.DropDownList)(gvwEstilos.Rows[Row].FindControl("dpVariante"))).SelectedItem.Text;

                if (sNegrita.Contains("Italic"))
                {
                    sNegrita=sNegrita.Remove(sNegrita.IndexOf(" Italic"));
                }
                //if (sNegrita == "True")
                //{
                //    sNegrita = "bolder";
                //}
                //else
                //{
                //    sNegrita = "normal";
                //}
                switch (sNegrita)
                {
                    case "Thin": sNegrita = "100"; break;
                    case "Light": sNegrita = "300"; break;
                    case "Medium": sNegrita = "500"; break;
                    case "Extra-Bold": sNegrita = "800"; break;
                    case "Ultra-Bold": sNegrita = "900"; break;
                }

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-weight", sNegrita);

                // Poner subrayada
                string sSubrayada = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkSubrayada"))).Checked.ToString();

                if (sSubrayada == "True")
                {
                    sSubrayada = "underline";
                }
                else
                {
                    sSubrayada = "none";
                }

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("text-decoration", sSubrayada);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                // Poner intalica
                //string sItalica = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkitalica"))).Checked.ToString();
                string sItalica = ((System.Web.UI.WebControls.DropDownList)(gvwEstilos.Rows[Row].FindControl("dpVariante"))).SelectedItem.Text;


                if (sItalica.Contains("Italic"))
                {
                    sItalica = "italic";
                }
                else
                {
                    sItalica = "none";
                }

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-style", sItalica);

                // Poner color
                string sHex = ((System.Web.UI.WebControls.TextBox)((gvwEstilos.Rows[Row].FindControl("txtBGColor")))).Text;
                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("color", "#" + sHex);

                // Si es color blanco pone el fondo gris para poder leer el texto
                if (sHex == "FFFFFF")
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#CCCCCC");
                }
                else
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#FFFFFF");
                }

            }
        } // Actualiza el preview de la letra

        public void CargaSesiones() //Carga las sessiones a labls
        {
            string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

            lblIdUsuario.Text = IdUsuario == null ? "" : IdUsuario;
            lblIdBrandiste.Text = Session["IdBrandSite"].ToString() == null ? "" : Session["IdBrandSite"].ToString();
        }

        protected void cargarEditors() //Carga los richTextbox dependiendo de los que se hayan guardado.
        {
            oBrandSite.obtenerEstilos(lblIdBrandiste.Text, gvwEstilos);
            PonerColorABoton(oBrandSite.ObtenerColorBoton(Session["IdBrandSite"].ToString()));
        }

        public void GuardarDatos() 
        { 
            try
            {
                // Valido que se escriba algo en la parte del tamaño y que sea numerico.
                foreach (GridViewRow gvwRow in gvwEstilos.Rows)
                {
                    string sTamano = ((TextBox)(gvwRow.FindControl("txtTamano"))).Text;

                    if(oSistema.isNumeric(sTamano) == false || oSistema.ValidarVacio(sTamano) == false)
                    {
                        ppValidarTamano.Show(); 
                        return;
                    }
                }

                foreach (GridViewRow gvwRow in gvwEstilos.Rows)
                {
                    bool sitalica;
                    bool sNegrita;
                    int indice = int.Parse(gvwRow.RowIndex.ToString());
                    string idExB = ((Label)(gvwRow.FindControl("lblIdExB"))).Text;
                    string sFuente = ((DropDownList)(gvwRow.FindControl("dpFuente"))).SelectedValue;
                    //string sTamano = ((DropDownList)(gvwRow.FindControl("dpTamano"))).SelectedValue;
                    string sTamano = ((TextBox)(gvwRow.FindControl("txtTamano"))).Text;
                    //bool sNegrita = ((CheckBox)(gvwRow.FindControl("chkNegrita"))).Checked;
                    //bool sitalica = ((CheckBox)(gvwRow.FindControl("chkItalica"))).Checked;
                    bool sSubrayada = ((CheckBox)(gvwRow.FindControl("chkSubrayada"))).Checked;
                    string sColor = ValidaColor(((TextBox)(gvwRow.FindControl("txtBGColor"))).Text);
                    string idEstilo = ((Label)(gvwRow.FindControl("lblidEstilo"))).Text;
                    string sVariante = ((DropDownList)(gvwRow.FindControl("dpVariante"))).SelectedItem.Text;
                    if (sVariante.Contains("Italic"))
                    {
                        sitalica = true;
                    }
                    else
                    {
                        sitalica = false;
                    }
                    if (sVariante.Contains("Bold"))
                    {
                        sNegrita = true;
                    }
                    else
                    {
                        sNegrita = false;
                    }

                    string IDexB = oBrandSite.guardaEstilos(idEstilo, idExB,lblIdUsuario.Text, lblIdBrandiste.Text, sFuente, sTamano, sNegrita, sitalica, sSubrayada, sColor,sVariante);
                }

                oBrandSite.GuardarColorBoton(Session["IdBrandSite"].ToString(), txtBGColorBoton.Text);
                oBrandSite.CrearTemplate(lblIdBrandiste.Text);
                cargarEditors();

                //HArchivo.Start();
            }
            catch
            {
            }
        } //Guarda los datos seleccionados en la tabla de estilos  
        
        public string ValidaColor(string sCadena)  //Valida que los colores sean un numero hexadecimal
        { 
            string  sCadenaValidada="";

            if (Regex.IsMatch(sCadena, @"^[0-9A-Fa-f]{6}$"))
            {
                sCadenaValidada = sCadena;

            }
            else
            {

                sCadenaValidada = "0";
            }
    
            return sCadenaValidada;
        }

        #endregion

        #region Eventos
       
        //Llena el dropDawnlist del gridview de estilos    
        protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    /*brandsiteid*/
                    lblIdBrandiste.Text = Session["IdBrandSite"].ToString() == null ? "" : Session["IdBrandSite"].ToString();
                    /*Carga las fuentes en el combo de Fuentes*/
                    DropDownList ddlFuente = (e.Row.FindControl("dpFuente") as DropDownList);
                    oBrandSite.obtenerFuentes(ddlFuente, lblIdBrandiste.Text);
                    //Selecciona el dato requerido 
                    string fuente = (e.Row.FindControl("lblFuente") as Label).Text;
                    ddlFuente.Items.FindByText(fuente).Selected = true;

                    /*Cargar las variantes de las fuentes*/
                    DropDownList ddlvariante = (e.Row.FindControl("dpVariante") as DropDownList);
                    string idfuente=ddlFuente.Items.FindByText(fuente).Value;

                    oBrandSite.obtenerVariantesFuentes(ddlvariante,idfuente);
                    //Selecciona el dato requerido 
                    string variante = (e.Row.FindControl("lblVariante") as Label).Text;
                    ddlvariante.Items.FindByText(variante).Selected = true;


                    /*Carga Tamaño*/
                    TextBox txtTamano = (e.Row.FindControl("txtTamano") as TextBox);
                    
                    //DropDownList ddlTamano = (e.Row.FindControl("dpTamano") as DropDownList);
                    //oBrandSite.obtenerTamanos(ddlTamano);

                    //Selecciona el dato requerido 

                    txtTamano.Text = (e.Row.FindControl("lblTamano") as Label).Text;

                    //string tamano = (e.Row.FindControl("lblTamano") as Label).Text;
                    //ddlTamano.Items.FindByText(tamano).Selected = true;


                    /*Pinta el color del texbox*/
                    TextBox txtColor = (e.Row.FindControl("txtBGColor") as TextBox);
                    txtColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#"+txtColor.Text);
                    txtColor.ForeColor = System.Drawing.ColorTranslator.FromHtml("#" + txtColor.Text); //System.Drawing.ColorTranslator.FromHtml("#" + (int.Parse(hexOutput) + 0xFF).ToString());

                }
                catch { }
            }
        }

        protected void btnGuardar_Click1(object sender, EventArgs e)
        {
            btnGuardar.Enabled = false;
            GuardarDatos();
            btnGuardar.Enabled = true;

            Actualizar();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            cargarEditors();
            Actualizar();
        }

        protected void btnColor_Click(object sender, EventArgs e)
        {
            try
            {

            Button btn = (Button)sender;
            GridViewRow row = (GridViewRow)(btn.NamingContainer);
            Session["irow"] = int.Parse(row.RowIndex.ToString());

                cPantones.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                cPantones.ColorSeleccionado += new Identidad.Controles.Pantones.EventHandler(Pantones_OnColorSeleccionado);
                cPantonesBoton.ColorSeleccionado += new Identidad.Controles.Pantones.EventHandler(PantonesBoton_OnColorSeleccionado);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void Pantones_OnColorSeleccionado(object sender, EventArgs e)
        {
            try
            {
                
                string sHex = cPantones.ObtenerHexadecimalSeleccionado();
                int irow = int.Parse(Session["irow"].ToString());

                if (irow != -1)
                {
                    TextBox txtColor = (TextBox)gvwEstilos.Rows[irow].FindControl("txtBGColor");                 
                    System.Drawing.Color cColor = System.Drawing.ColorTranslator.FromHtml("#"+sHex);
                    txtColor.BackColor = cColor;
                    txtColor.Text = sHex;
                    txtColor.ForeColor = cColor;

                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[irow].FindControl("lblNombreEstilo")))).Style.Add("color", "#" + sHex);

                    if (sHex == "FFFFFF")
                    {
                        ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[irow].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#CCCCCC");
                    }
                    else
                    {
                        ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[irow].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#FFFFFF");
                    }

                    irow = -1;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void PantonesBoton_OnColorSeleccionado(object sender, EventArgs e)
        {
            try
            {
                PonerColorABoton(cPantonesBoton.ObtenerHexadecimalSeleccionado());
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnColorBoton_Click(object sender, EventArgs e)
        {
            try
            {
                cPantonesBoton.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #region Eventos de controles del grid

        protected void dpFuente_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                

                DropDownList Status = (DropDownList)sender;

                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                string sFuente = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpFuente")))))).SelectedItem.Text;
                string idFuente = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpFuente")))))).SelectedItem.Value;
                oBrandSite.obtenerVariantesFuentes((((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpVariante"))))),idFuente);

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-family", sFuente);


                string sHexa = ((System.Web.UI.WebControls.TextBox)((gvwEstilos.Rows[Row].FindControl("txtBGColor")))).Text;

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("color", "#" + sHexa);

                if (sHexa == "FFFFFF")
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#CCCCCC");
                }
                else
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("background-color", "#FFFFFF");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        //protected void dpTamano_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DropDownList Status = (DropDownList)sender;
        //    int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
        //    string sTamano = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpTamano")))))).SelectedItem.Text;
        //    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-size", sTamano + "px");
        //}

        protected void chkNegrita_CheckedChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckBox Status = (CheckBox)sender;
            //    int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            //    string sChecked = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkNegrita"))).Checked.ToString();

            //    if (sChecked == "True")
            //    {
            //        sChecked = "bolder";
            //    }
            //    else
            //    {
            //        sChecked = "normal";
            //    }

            //    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-weight", sChecked);
            //}
            //catch (Exception error)
            //{
            //    oSistema.saveLog(error.ToString());
            //}
        }

        protected void chkSubrayada_CheckedChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckBox Status = (CheckBox)sender;
            //    int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            //    string sChecked = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkSubrayada"))).Checked.ToString();

            //    if (sChecked == "True")
            //    {
            //        sChecked = "underline";
            //    }
            //    else
            //    {
            //        sChecked = "none";
            //    }

            //    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("text-decoration", sChecked);
            //}
            //catch (Exception error)
            //{
            //    oSistema.saveLog(error.ToString());
            //}
        }

        protected void chkitalica_CheckedChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    CheckBox Status = (CheckBox)sender;
            //    int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            //    string sChecked = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkitalica"))).Checked.ToString();

            //    if (sChecked == "True")
            //    {
            //        sChecked = "italic";
            //    }
            //    else
            //    {
            //        sChecked = "none";
            //    }

            //    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-style", sChecked);
            //}
            //catch (Exception error)
            //{
            //    oSistema.saveLog(error.ToString());
            //}
        }

        #endregion

        protected void dpVariante_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList Status = (DropDownList)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            //    string sChecked = ((System.Web.UI.WebControls.CheckBox)(gvwEstilos.Rows[Row].FindControl("chkNegrita"))).Checked.ToString();
                string varianteFuente = ((System.Web.UI.WebControls.ListControl)(((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpVariante")))))).SelectedItem.Value;
            //    oBrandSite.obtenerVariantesFuentes((((System.Web.UI.WebControls.DropDownList)((gvwEstilos.Rows[Row].FindControl("dpVariante"))))), varianteFuente);

                //if (sChecked == "True")
                //{
                //    sChecked = "bolder";
                //}
                //else
                //{
                //    sChecked = "normal";
                //}
                if (varianteFuente.Contains("Italic"))
                {
                    ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-style", "italic");
                    varianteFuente = varianteFuente.Remove(varianteFuente.IndexOf("; font-style: Italic"));
                }
                switch (varianteFuente)
                {
                    case "Thin": varianteFuente = "100"; break;
                    case "Light": varianteFuente = "300"; break;
                    case "Medium": varianteFuente = "500"; break;
                    case "Extra-Bold": varianteFuente = "800"; break;
                    case "Ultra-Bold": varianteFuente = "900"; break;
                }

                ((System.Web.UI.WebControls.Label)((gvwEstilos.Rows[Row].FindControl("lblNombreEstilo")))).Style.Add("font-weight", varianteFuente);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }

           
        }

      
        #endregion

    }
}