﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="EditorManual.aspx.cs" Inherits="Identidad.Designer.EditorManual" ValidateRequest="false"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
         
    <div id="divPrincipal">

      
     <div id="MySplitter">
             
         <div class="contenido menud"> 
            
             
                     
                      <div id="divMenu">
                        <div id="divAgregarSeccion" class="divAgregarSeccion" style=" background:#373E4E;">
                        <asp:Button ID="btnAgregarSeccion" runat="server" Text="Agregar sección"  CssClass="buttons colorVerde alinear" OnClick="btnAgregarSeccion_Click"/>
                        </div>
          
                       <input type="text" id="txtListaJSON" runat="server" style="display:none; "/>  
            
                     <div id="divTree" runat="server">
                    
                 
                </div> <!-- fin divTree--->

            </div>
                    
      </div>
         <!-- Seccion que tiene el detalle de la etiqueta seleccionada-->
            <div class="contenido panelEdicion" style="min-width:695px">  
                 
                <div id="divOpciones" class="divOpciones">
                 
                     <asp:Panel ID="pnlMenuOpciones" runat="server" >
                              <div id="divCargando" style="float:left"></div>
                              <asp:Button ID="btnEliminarSeccion" runat="server" Text="Eliminar sección" CssClass="buttons colorwhite alinear" OnClick="btnEliminarSeccion_Click" />
                     </asp:Panel>


                </div>
              
                 <div id="divContenido">
                      <div id="divEdicion" class="divEdicion">
                           <div id="divTitulos" class="divTitulos">
                               <asp:Label ID="lblIdSeleccionado" Text="" runat="server" style="display:none"></asp:Label>
                                <asp:Label ID="lblNombreMenu" Text="Nombre título" runat="server"></asp:Label>
                            </div>

                          <%-- Panel de edicion del richTexbox --%>
                          <div id="divEditRich" class="divOpciones" style="background:#373E4E">

                                <asp:Panel ID="pnlPublicar" runat="server" >
                                       <asp:Button ID="btnPlantilla" runat="server" CssClass="buttons colorGris alinear" Text="Cambiar plantilla  " OnClick="btnPlantilla_Click"  /> 
                                       <asp:Button ID="btnRestablecer" runat="server"  CssClass="buttons colorGris alinear" Text="Restablecer  "/>                       
                                       <asp:Button ID="btnGuardar" runat="server"  CssClass="buttons colorGris alinear" Text="Guardar cambios  "/> 
                                       <asp:HiddenField ID="hdHTML" runat="server" />
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </asp:Panel>
                         </div>

                         
                        
                          <!-- Aqui estara guardado todo el html --->
                          <div id="divHTML" runat="server">


                           
                          </div>
                    </div>

               </div>



                
            </div>
     
      </div> 

 
   </div>


    <!--Eliminar una seccion del  manual-->




 <panel id="pnlEliminarSeccion" runat="server" style="display:none" >
            <div class="divTabla divConfirmarEliminar">
                 <div class="divTablaTitulo">
                      <asp:Label ID="lblEliminarTitulo" runat="server" Text="Advertencia"></asp:Label>        
                 </div>
                <div class="divTablaTituloImagen">
                                 <img id="img1" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarEliminar.ClientID %>').click();" />
                        </div>
                 <hr/><br/>

               <div class="divTBODY divTamano">
                    <div class="divTR contenidoCentrar">
                        <asp:Label ID="lblEliminarConfirmacion" runat="server" Text="Esta sección se eliminara permanentemente, ¿Esta seguro que desea continuar?"></asp:Label>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons">
                                           <asp:Button ID="btnCancelarEliminar" runat="server" Text="No, Cancelar" CssClass="buttons colorwhite"/>
                                           <asp:Button ID="btnEliminar" runat="server" Text="Si, Eliminar" CssClass="buttons colorRojo" OnClick="btnEliminar_Click" />                       
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons"></div>
                    </div>
                </div>
            </div>
        </panel>

 <asp:Label ID="label2" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppEliminar" TargetControlID="Label2" runat="server" PopupControlID="pnlEliminarSeccion" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>
        </Ajax:ModalPopupExtender>

    <!--Fin eliminar una seccion del manual-->

    <!--Inicia Popup para agregar un item nuevo al manual--->

        <%--onclick="$get('imgbtnC').click()"--%>
        <panel id="pnlAgregarNuevoItem" runat="server"  style="display:none">
          <div class="divTabla">
                <div>
                        <div class="divTablaTitulo">
                               <asp:Label ID="lblTitulo" runat="server" Text="Agregar nueva sección/capitulo"></asp:Label> 
                         </div>
                        <div class="divTablaTituloImagen">
                                 <img id="imgbtnC" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarItem.ClientID %>').click();" />
                        </div>
                </div>
              
                   <hr/>
                   
               <br/>
               <div class="divTBODY">
                       <div id="divTR">
                            <div class="divTDIzq"> 
                                <asp:Label ID="lblitemNombre" runat="server" Text="Sección"></asp:Label> 
                            </div>
                            <div class="divTDDerechoInput"> 
                               <asp:TextBox ID="txtNombreItemIndice" runat="server" CssClass="TextBox"></asp:TextBox>
                            </div>
                       </div>

                       <div class="divTR">
                            <div class="divTDIzq"> <asp:Label ID="lblItemPadre" runat="server" Text="Capitulo"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput"> 
                                <label>
                                <asp:DropDownList ID="ddlPadres" runat="server" CssClass="DropDownList"></asp:DropDownList>
                                </label>
                            </div>
                       </div>

                      <div class="divTR">              
                           
                            <div class="divTDDerechoInput"> <asp:Label ID="lblValidarNuevoItem" runat="server" Visible="false"></asp:Label>
                            </div>
                       </div>

                   <div class="divTR">
                            <div class="divTDIzq"> 
                            </div>
                            <div class="divTDDerechoButtons" > 
                  
                             <asp:Button ID="btnCancelarItem" runat="server" Text="Cancelar" class="buttons colorwhite" />
                              &nbsp;
                                <asp:Button ID="btnAgregarItem" runat="server" Text="Aceptar"  class="buttons colorVerde" OnClick="btnAgregarItem_Click"/>
           
                                 </div>
                       </div>

                   <div class="divTR">
                        <div class="divTDIzq">
                        </div>
                        <div class="divTDDerechoButtons">
                         </div>
                    </div>

                </div>
</div>         
        </panel>

        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppAgregarItemManual" TargetControlID="Label1" runat="server" PopupControlID="pnlAgregarNuevoItem" BackgroundCssClass="modalBackground" Animations="" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup para agregar un item nuevo al manual--->


    <!--- Plantillas -->

    <asp:Panel ID="pnlTemplate" runat="server" style="display:none">
            
                <div  class="divTabla divSeleccionPlantilla Popup"> 
                    <div>
                    <div class="divTablaTitulo">
                      <asp:Label ID="lblPlantilla"  Text="Cambiar Plantilla" runat="server"></asp:Label>    
                            
                    </div>

                    <div class="divTablaTituloImagen">
                                 <img id="img2" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarItem.ClientID %>').click();" />
                        </div>

                    </div>
                 <hr/><br/>

                    <br/>
                   
                     <div class="divPlantillaContenido">
                        <asp:RadioButtonList ID="rdblPlantilla" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" CellPadding="5" CellSpacing="10"></asp:RadioButtonList> 
                     </div>
                    <br/>


                    <div class="divTR">
                        
                        <div class="divAdvertencia">
                          Aviso: Si cambias tu plantilla a una más basica el contenido del segundo texto o segunda imagen se perderan.
                         </div>
                    </div>
                    <br/>
                     <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons">
                                  
                         <asp:Button ID="btnCancelarPlantilla" runat="server" Text="Cancelar" CssClass="buttons colorwhite" />
                         <asp:Button ID="btnGuardarPlantilla" runat="server" Text="Cambiar" CssClass="buttons colorAzulO" />
               
                         </div>
                    </div>
                     <br/>
                 </div>

          </asp:Panel>

                 <asp:Label ID="lblpp" runat="server" Text=""></asp:Label>
                
         <Ajax:ModalPopupExtender ID="pPlantillas" TargetControlID="lblpp" runat="server" PopupControlID="pnlTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnCancelarPlantilla" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>
         </Ajax:ModalPopupExtender>
         
    
    <!--FIN-->




    <script type="text/javascript">

        document.onkeydown = function (evt) {
        return (evt ? evt.which : event.keyCode) != 13;
        }
   </script>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="../JS/jquery-1.8.3.js"></script>
        <script src="../JS/Manual/jquery-1.8.3.min.js"></script>
        <script src="../JS/Manual/JquerySplitter.js"></script> <%--Esta es la libreria para crear el splitter--%>
        <script src="../JS/Manual/Splitter.js"></script>       <%-- Este es el js que se creo para adaptar el splitter--%>
        <script src="../JS/Manual/JqueryNestable.js"></script>
        <script src="../JS/Manual/MenuInicia.js"></script>
        <script src="../JS/Manual/HtmlEditor.js"></script>  <%--Este script nos ayuda a modificar las cosas del titulo--%>
    
        <link href="../CSS/Site.css" rel="stylesheet" />
        <link href="../CSS/Pages/Manual/splitter.css" rel="stylesheet" />  <%--CSS solo para darle estilo a la rayita de enmedio--%>
        <link href="../CSS/Pages/Manual/EditorManual.css" rel="stylesheet" />
        <link href="../CSS/Pages/Manual/MenuTree.css" rel="stylesheet" />
    
    



        <script type="text/javascript">
  
          
            //Por cada postBack vuelve a cargarlos.
            Sys.Application.initialize();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {

                        $().ready(function () {

                            /*Si hay un postback en la pagina carga las funciones primordiales.*/

                            var updateOutput = function (e) {
                                var list = e.length ? e : $(e.target),
                                    output = list.data('output');
                                if (window.JSON) {

                                    $('#txtListaJSON').val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                                } else {

                                    $('#txtListaJSON').val('null');
                                }
                            };
                       

                            ///Inicializa el spliter.
      
                            $("#MySplitter").splitter();
                            var tamMinimo = 310;         //ES EL TAMAÑO MINIMO A INICIALIZAR LOS DIVS.

                            $(".vsplitbar").append('<div id="collapse"></div>'); //Agrega el div para al darle click oculte el panel del menu
                            $('.dd').nestable('collapseAll');
                            $("#collapse").click(function () {  // se creo la funcion click para el div collapse para que nos ayudara a manejar los eventos

                                if ($("#MySplitter>div").width() <= '10') // si el tamaño del primer div que hay en el div mySplitter es igual a 0 entonces pone el tamaño en 200, abre de nuevo el div
                                    $("#MySplitter").trigger("resize", tamMinimo).animate("prop");
                                else                                    // si no, pone el tamaño en 0, lo cierra                    
                                    $("#MySplitter").trigger("resize", 0).animate("prop");

                                clearTimeout(null);
                            }); // fin del metodo click del collapse

                            // inicializa el div Mysplitter en 200px 

                            $('#nestable3').nestable().on('change', updateOutput);
                            
                            timer = setTimeout(function () {

                                $("#collapse").click();
                                $("#collapse").click();

                            }, 100);


                            CollapseItems();
                        });
     }
                
                        });
                }

        </script>

</asp:Content>
