﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections.Generic;
using System.Web.Services;
using System.Data.SqlClient;
using System.Web.UI;

namespace Identidad.Designer
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página

        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();
        DataTable dtGrupos = new DataTable("Grupos");
        DataColumn column;

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    Session["RutaCarpetas"] = null;

                    CargarArchivos();
                    
                }

            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, consisten generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarArchivos()
        {
            CargarArchivos(lblIdCarpetaPadreSeleccionada.Text);
        } // Carga los archivos para el usuario sin recibir como parametro el id de la carpeta padre

        private void CargarArchivos(string sIdCarpetaPadreSeleccionada)
        {
            if (oSistema.ValidarLenght(txtBusqueda.Text) == false)
            {
                txtBusqueda.Text = "";
            }

            string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

            oBrandSite.CargarArchivos(dlArchivos, Session["IdBrandSite"].ToString(), txtBusqueda.Text, ddlTipo.SelectedValue.ToString(), ddlOrdenadoPor.SelectedValue, IdUsuario, sIdCarpetaPadreSeleccionada);
            
            if (dlArchivos.Items.Count != 0)
            {
                for (int i = 0; i < dlArchivos.Items.Count; i++)
                {
                    if (((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblExtension")))).Text == ".folder")
                    {
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblExtension")))).Visible = false;
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblDescripcion")))).Visible = false;
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblPeso")))).Visible = false;
                        ((System.Web.UI.WebControls.LinkButton)((dlArchivos.Items[i].FindControl("lkbAgregarTag")))).Visible = false;
                        ((System.Web.UI.WebControls.LinkButton)((dlArchivos.Items[i].FindControl("lkbDescargar")))).Visible = false;
                    }
                    else
                    {
                        ((System.Web.UI.WebControls.Image)((dlArchivos.Items[i].FindControl("imgPreview")))).Attributes.Add("style", "background-color:#ffffff!important;");
                        
                        oBrandSite.CargarTagsDeArchivo((DataList)dlArchivos.Items[i].FindControl("dlTags"), ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblIdArchivo")))).Text);

                        DataList dlC = (DataList)dlArchivos.Items[i].FindControl("dlTags");
                        foreach (DataListItem Items in dlC.Items)
                        {
                            Label lblIdTxA = Items.FindControl("lblIdTxA") as Label;
                            if (lblIdTxA.Text == "0")
                            {
                                LinkButton lkbEliminar = Items.FindControl("lkbEliminarTag") as LinkButton;
                                lkbEliminar.Visible = false;
                            }
                        }
                    }
                }
            }
            
            oBrandSite.CargarItemsBusquedaArchivos(ddlBusqueda, Session["IdBrandSite"].ToString());

            List<cRuta> lRutas;
            lRutas = (List<cRuta>)Session["RutaCarpetas"];
            if (lRutas != null)
            {
                if (lRutas.Count == 0)
                {
                    btnRegresar.Visible = false;
                }
                else
                {
                    btnRegresar.Visible = true;
                }
            }
            else
            {
                btnRegresar.Visible = false;
            }

        } // Carga los archivos para el usuario dentro de una carpeta determinada

        //public int CurrentPage
        //{
        //    get
        //    {
        //        // look for current page in ViewState
        //        object o = this.ViewState["_CurrentPage"];
        //        if (o == null)
        //            return 0; // default page index of 0
        //        else
        //            return (int)o;
        //    }

        //    set
        //    {
        //        this.ViewState["_CurrentPage"] = value;
        //    }
        //}

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. se disparan por medio de un control en el HTML :)

        #region Eventos

        #region Subir archivo

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                lblValidacion.Visible = false;
                txtNombre.Enabled = true;
                txtNombre.Visible = true;
                txtNombre.Text = "";
                ckbDetectarNombre.Checked = false;

                ppSubirArchivo.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnSubir_Click(object sender, EventArgs e)   
        {
            try
            {
                ppSubirArchivo.Show();

                if(ckbDetectarNombre.Checked == true)
                {
                    txtNombre.Text = fluArchivo.FileName.Replace(oSistema.ObtenerExtension(fluArchivo.FileName) , "").Trim();
                }

                ppSubirArchivo.Show();

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());


                if (oBrandSite.ValidarEspacio(Session["IdBrandSite"].ToString()))
                    oMetodo = oBrandSite.SubirArchivo("0", txtNombre.Text.Trim(), txtDescripcion.Text.Trim(), fluArchivo, fluPreview, Session["IdBrandSite"].ToString(), IdUsuario, cbArchivoTrabajo.Checked.ToString(), lblIdCarpetaPadreSeleccionada.Text);
                else
                {
                    oMetodo.Message = "Ya no tiene espacio en el servidor. Actualiza tu Plan.";
                    oMetodo.Pass = false;
                }

                if (oMetodo.Pass == false)
                {
                    lblValidacion.Text = oMetodo.Message;
                    lblValidacion.Visible = true;

                    ppSubirArchivo.Show();
                }
                else
                {
                    string sIdArchivoNuevo = oMetodo.Message;

                    // Guarda los grupos que se seleccionaron

                    // Si no selecciona ningun grupo, por default le pone "Todos"
                    if (gvGruposQueVen.Rows.Count == 0 && cbArchivoTrabajo.Checked == false)
                    {
                        oMetodo = oBrandSite.EditarGruposPorArchivo("0", sIdArchivoNuevo, "0", "true");
                    }

                    // recorre el grid para ver que grupos selecciono y guardarlos
                    for (int i = 0; gvGruposQueVen.Rows.Count > i; i++ )
                    {
                        oMetodo = oBrandSite.EditarGruposPorArchivo("0", sIdArchivoNuevo, ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text, "true");
                    }
                        
                    lblValidacion.Visible = false;
                    txtNombre.Text = "";
                    txtDescripcion.Text = "";
                    cbArchivoTrabajo.Checked = false;
                    gvGruposQueVen.DataSource = null;
                    gvGruposQueVen.DataBind();

                    ppSubirArchivo.Hide();

                    CargarArchivos();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnNuevoArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                Session["fluArchivo"] = null;
                Session["fluPreview"] = null;
                Session["fluArchivoModificar"] = null;
                Session["fluPreviewModificar"] = null;

                lblIdArchivoModificar.Text = "";
                txtNombreModificar.Text = "";
                txtDescripcionModificar.Text = "";
                lblValidacionModificacion.Visible = false;
                imgImagen.ImageUrl = "";
                imgImagenPreview.ImageUrl = "";
                txtMensaje.Text = "";
                txtMensaje.Visible = false;
                cbkEnviarCorreo.Checked = false;
                lblGrupos.Visible = true;
                gvGruposQueVen.Visible = true;
                ddlGrupos.Visible = true;
                btnAgregar.Visible = true;
                gvGruposQueVen.DataSource = null;
                gvGruposQueVen.DataBind();
                cbArchivoTrabajo.Checked = false;

                txtNombre.Enabled = true;
                txtNombre.Visible = true;
                txtNombre.Text = "";
                ckbDetectarNombre.Checked = false;

                oBrandSite.CargarGruposBS(ddlGrupos, Session["IdBrandSite"].ToString());
                ppSubirArchivo.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Modificar archivo
        
        protected void btnModificarArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblExtensionModificar.Text != ".folder")
                {
                    ppModificarArchivo.Show();

                    string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                    oMetodo = oBrandSite.ModificarArchivo(lblIdArchivoModificar.Text, txtNombreModificar.Text, txtDescripcionModificar.Text, Session["IdBrandSite"].ToString(), fluArchivoModificar, fluPreviewModificar, cbArchivoTrabajoModificar.Checked.ToString(), IdUsuario);

                    if (oMetodo.Pass == false)
                    {
                        ppModificarArchivo.Show();

                        lblValidacionModificacion.Text = oMetodo.Message;
                        lblValidacionModificacion.Visible = true;

                    }
                    else
                    {

                        // Damos de baja los grupos para crearlos una vez más

                        oMetodo = oBrandSite.DesactivarGruposDeArchivo(lblIdArchivoModificar.Text);
                        if (oMetodo.Pass == true)
                        {
                            // Guarda los grupos que se seleccionaron

                            // Si no selecciona ningun grupo, por default le pone "Todos"
                            if (gvGruposQueVenModificar.Rows.Count == 0 && cbArchivoTrabajoModificar.Checked == false)
                            {
                                oMetodo = oBrandSite.EditarGruposPorArchivo("0", lblIdArchivoModificar.Text, "0", "true");
                            }

                            // Recorre el grid para ver que grupos selecciono
                            for (int i = 0; gvGruposQueVenModificar.Rows.Count > i; i++)
                            {
                                oMetodo = oBrandSite.EditarGruposPorArchivo("0", lblIdArchivoModificar.Text, ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblIdGrupo"))).Text, "true");
                            }
                        }

                        // Envia correos electrónicos en caso de haber seleccionado la opción

                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        if (cbkEnviarCorreo.Checked == true)
                        {
                            oMetodo = oBrandSite.CorreoNotificaReemplazoArchivo(lblIdArchivoModificar.Text, txtMensaje.Text);
                        }

                        cbArchivoTrabajoModificar.Checked = false;
                        gvGruposQueVenModificar.DataSource = null;
                        gvGruposQueVenModificar.DataBind();
                        lblValidacionModificacion.Visible = false;
                        ppModificarArchivo.Hide();
                        CargarArchivos();

                        ppModificarArchivo.Hide();

                    }
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarModificacion_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdArchivoModificar.Text = "";
                ppModificarArchivo.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbEditar_Click(object sender, EventArgs e)
        {
            try
            {
                Session["fluArchivo"] = null;
                Session["fluPreview"] = null;
                Session["fluArchivoModificar"] = null;
                Session["fluPreviewModificar"] = null;

                lblIdArchivoModificar.Text = "";
                txtNombreModificar.Text = "";
                txtDescripcionModificar.Text = "";
                lblValidacionModificacion.Visible = false;
                imgImagen.ImageUrl = "";
                imgImagenPreview.ImageUrl = "";
                txtMensaje.Text = "";
                txtMensaje.Visible = false;
                cbkEnviarCorreo.Checked = false;
                
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                lblIdArchivoModificar.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                lblExtensionModificar.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text;
                string sNombre = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblNombre")))).Text;
                bool bCarpetaTrabajo = bool.Parse(((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblArchivoTrabajo")))).Text);

                if (lblExtensionModificar.Text == ".folder")
                {
                    txtNombreCarpeta.Text = sNombre;
                    ckbCarpetaTrabajo.Checked = bCarpetaTrabajo;
                    ppCrearCarpeta.Show();
                }
                else
                {
                    oBrandSite.CargarGruposBS(ddlGruposModificar, Session["IdBrandSite"].ToString());

                    foreach (DataRow row in oBrandSite.CargarArchivo(lblIdArchivoModificar.Text).Rows)
                    {
                        txtNombreModificar.Text = row["Nombre"].ToString();
                        txtDescripcionModificar.Text = row["Descripcion"].ToString();
                        imgImagen.ImageUrl = row["Url"].ToString() + "?t=" + DateTime.Now.Ticks.ToString();
                        imgImagenPreview.ImageUrl = row["Preview"].ToString();
                        cbArchivoTrabajoModificar.Checked = bool.Parse(row["ArchivoTrabajo"].ToString());
                    }

                    if (cbArchivoTrabajoModificar.Checked == true)
                    {
                        lblGruposModificar.Visible = false;
                        gvGruposQueVenModificar.Visible = false;
                        ddlGruposModificar.Visible = false;
                        btnAgregarGrupoModificar.Visible = false;
                        gvGruposQueVenModificar.DataSource = null;
                        gvGruposQueVenModificar.DataBind();
                    }
                    else
                    {
                        lblGruposModificar.Visible = true;
                        gvGruposQueVenModificar.Visible = true;
                        ddlGruposModificar.Visible = true;
                        btnAgregarGrupoModificar.Visible = true;
                    }

                    oBrandSite.CargarGruposPorArchivo(gvGruposQueVenModificar, lblIdArchivoModificar.Text);

                    if (imgImagenPreview.ImageUrl == "")
                    {
                        imgImagenPreview.ImageUrl = imgImagen.ImageUrl;
                    }
                    else
                    {
                        imgImagenPreview.ImageUrl = imgImagenPreview.ImageUrl + "?t=" + DateTime.Now.Ticks.ToString();
                    }

                    ppModificarArchivo.Show();
                }

                
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Eliminar archivo

        protected void lkbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                lblIdArchivoEliminar.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                lblExtensionEliminar.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text;

                ppEliminarArchivo.Show();
                
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblExtensionEliminar.Text == ".folder")
                {
                    oCarpeta = new cCarpeta();
                    oCarpeta.IdCarpeta = int.Parse(lblIdArchivoEliminar.Text);
                    oMetodo = oCarpeta.EliminarCarpeta();

                }
                else
                {
                    string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                    oMetodo = oBrandSite.EliminarArchivo(lblIdArchivoEliminar.Text, Session["IdBrandSite"].ToString(), IdUsuario);
                }

                if (oMetodo.Pass == true)
                {
                    ppEliminarArchivo.Hide();
                    CargarArchivos();
                }
                else
                {
                    lblConfirmarEliminar.Text = oMetodo.Message;
                    ppEliminarArchivo.Show();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminarArchivo.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Agregar Tag

        protected void imgPreview_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                Image Status = (Image)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.AgregarTagPorArchivo("0", ((System.Web.UI.WebControls.Label)((gvSeleccionaTag.Rows[Row].FindControl("lblIdTagSeleccionado")))).Text, lblIdArchivoParaTag.Text, IdUsuario);

                if (oMetodo.Pass == true)
                {
                    CargarArchivos();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbAgregarTag_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                lblIdArchivoParaTag.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                
                oBrandSite.CargarTags(gvSeleccionaTag, Session["IdBrandSite"].ToString());

                ppAgregarTag.Show();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.AgregarTagPorArchivo("0", ((System.Web.UI.WebControls.Label)((gvSeleccionaTag.Rows[Row].FindControl("lblIdTagSeleccionado")))).Text, lblIdArchivoParaTag.Text, IdUsuario);

                if (oMetodo.Pass == true)
                {
                    CargarArchivos();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarTag_Click(object sender, EventArgs e)
        {
            try
            {
                ppAgregarTag.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbEliminarTag_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                string sIdTxA = ((System.Web.UI.WebControls.Label)(((DataListItem)Status.NamingContainer).FindControl("lblIdTxA"))).Text;
                
                oMetodo = oBrandSite.EliminarTagPorArchivo(sIdTxA);

                if (oMetodo.Pass == true)
                {
                    CargarArchivos();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Búsqueda y filtro de tipo de archivos

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBusqueda.Text.Trim() == "")
                {
                    CargarArchivos();
                }
                else
                {
                    CargarArchivos("%");

                    cCarpeta oCarpeta = new cCarpeta();

                    if (dlArchivos.Items.Count != 0)
                    {
                        for (int i = 0; i < dlArchivos.Items.Count; i++)
                        {
                            ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblRuta")))).Visible = true;
                            ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblRuta")))).Text = oCarpeta.ObtenerCarpetasDeArchivo(int.Parse(((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblIdArchivo")))).Text));
                        }
                    }
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ddlTipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtBusqueda.Text.Trim() == "")
                {
                    CargarArchivos();
                }
                else
                {
                    CargarArchivos("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ddlOrdenadoPor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtBusqueda.Text.Trim() == "")
                {
                    CargarArchivos();
                }
                else
                {
                    CargarArchivos("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Grupos para el archivo al subir

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                ppSubirArchivo.Show();

                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                bool bYaExiste = false;

                for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text == ddlGrupos.SelectedValue)
                    {
                        bYaExiste = true;
                        break;
                    }
                }

                if (bYaExiste == false)
                {
                    DataRow Row = dtGrupos.NewRow();

                    Row["IdGrupo"] = ddlGrupos.SelectedValue;
                    Row["NombreGrupo"] = ddlGrupos.SelectedItem;
                    dtGrupos.Rows.Add(Row);

                    // Llena con los grupos que ya se habian solicitado
                    for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }

                    // Ordena alfabeticamente la lista de grupos
                    DataView dv = dtGrupos.DefaultView;
                    dv.Sort = "NombreGrupo desc";
                    DataTable sortedDT = dv.ToTable();

                    gvGruposQueVen.DataSource = sortedDT;
                    gvGruposQueVen.DataBind();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                ppSubirArchivo.Show();
            }
        }

        protected void btnElimigarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                string sIdGrupoABorrar = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[Row].FindControl("lblIdGrupo"))).Text;

                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                // Llena con los grupos que ya se habian solicitado
                for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text != sIdGrupoABorrar)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }
                }

                // Ordena alfabeticamente la lista de grupos
                DataView dv = dtGrupos.DefaultView;
                dv.Sort = "NombreGrupo desc";
                DataTable sortedDT = dv.ToTable();

                gvGruposQueVen.DataSource = sortedDT;
                gvGruposQueVen.DataBind();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                ppSubirArchivo.Show();
            }
        }

        protected void cbArchivoTrabajo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(cbArchivoTrabajo.Checked == true)
                {
                    lblGrupos.Visible = false;
                    gvGruposQueVen.Visible = false;
                    ddlGrupos.Visible = false;
                    btnAgregar.Visible = false;
                    gvGruposQueVen.DataSource = null;
                    gvGruposQueVen.DataBind();
                }
                else
                {
                    lblGrupos.Visible = true;
                    gvGruposQueVen.Visible = true;
                    ddlGrupos.Visible = true;
                    btnAgregar.Visible = true;
                }

                ppSubirArchivo.Show();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void gvGruposQueVen_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ppSubirArchivo.Show();
        }

        protected void gvGruposQueVen_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            ppSubirArchivo.Show();
        }

        #endregion

        #region Grupos para el archivo al modificar

        protected void cbArchivoTrabajoModificar_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbArchivoTrabajoModificar.Checked == true)
                {
                    lblGruposModificar.Visible = false;
                    gvGruposQueVenModificar.Visible = false;
                    ddlGruposModificar.Visible = false;
                    btnAgregarGrupoModificar.Visible = false;
                    gvGruposQueVenModificar.DataSource = null;
                    gvGruposQueVenModificar.DataBind();
                }
                else
                {
                    lblGruposModificar.Visible = true;
                    gvGruposQueVenModificar.Visible = true;
                    ddlGruposModificar.Visible = true;
                    btnAgregarGrupoModificar.Visible = true;
                }

                ppModificarArchivo.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnAgregarGrupoModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ppModificarArchivo.Show();

                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                bool bYaExiste = false;
                
                for (int i = 0; gvGruposQueVenModificar.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblIdGrupo"))).Text == ddlGruposModificar.SelectedValue)
                    {
                        bYaExiste = true;
                        break;
                    }
                }

                if (bYaExiste == false)
                {
                    DataRow Row = dtGrupos.NewRow();

                    Row["IdGrupo"] = ddlGruposModificar.SelectedValue;
                    Row["NombreGrupo"] = ddlGruposModificar.SelectedItem;
                    dtGrupos.Rows.Add(Row);

                    // Llena con los grupos que ya se habian solicitado
                    for (int i = 0; gvGruposQueVenModificar.Rows.Count > i; i++)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }

                    // Ordena alfabeticamente la lista de grupos
                    DataView dv = dtGrupos.DefaultView;
                    dv.Sort = "NombreGrupo desc";
                    DataTable sortedDT = dv.ToTable();

                    gvGruposQueVenModificar.DataSource = sortedDT;
                    gvGruposQueVenModificar.DataBind();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                ppModificarArchivo.Show();
            }
        }

        protected void gvGruposQueVenModificar_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ppModificarArchivo.Show();
        }

        protected void gvGruposQueVenModificar_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            ppModificarArchivo.Show();
        }

        protected void btnElimigarGrupoModificar_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                string sIdGrupoABorrar = ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[Row].FindControl("lblIdGrupo"))).Text;
                
                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                // Llena con los grupos que ya se habian solicitado
                for (int i = 0; gvGruposQueVenModificar.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblIdGrupo"))).Text != sIdGrupoABorrar)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVenModificar.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }
                }

                // Ordena alfabeticamente la lista de grupos
                DataView dv = dtGrupos.DefaultView;
                dv.Sort = "NombreGrupo desc";
                DataTable sortedDT = dv.ToTable();

                gvGruposQueVenModificar.DataSource = sortedDT;
                gvGruposQueVenModificar.DataBind();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                ppModificarArchivo.Show();
            }
        }

        #endregion

        #region Carpetas

        cCarpeta oCarpeta;

        protected void btnNuevaCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdArchivoModificar.Text = "";
                txtNombreCarpeta.Text = "";
                ckbCarpetaTrabajo.Checked = false;
                lblValidacionCrearCarpeta.Visible = false;
                ppCrearCarpeta.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarCrearCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdArchivoModificar.Text = "";
                lblValidacionCrearCarpeta.Visible = false;
                txtNombreCarpeta.Text = "";
                ckbCarpetaTrabajo.Checked = false;
                ppCrearCarpeta.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCrearCarpeta_Click(object sender, EventArgs e)
        {
            try
            {
                cCarpeta oCarpeta = new cCarpeta();

                if (!(lblIdArchivoModificar.Text == "" || lblIdArchivoModificar.Text == "0"))
                {
                    // Actualizar
                    oCarpeta.IdCarpeta = int.Parse(lblIdArchivoModificar.Text);
                    oCarpeta.IdCarpetaPadre = int.Parse(lblIdCarpetaPadreSeleccionada.Text);
                    oCarpeta.sNombre = txtNombreCarpeta.Text;
                    oCarpeta.CarpetaTrabajo = ckbCarpetaTrabajo.Checked;

                    oMetodo = oCarpeta.ModificarCarpeta();

                    if (oMetodo.Pass == false)
                    {
                        lblValidacionCrearCarpeta.Text = oMetodo.Message;
                        lblValidacionCrearCarpeta.Visible = true;
                        ppCrearCarpeta.Show();
                    }
                    else
                    {
                        lblValidacionCrearCarpeta.Visible = false;
                        lblIdArchivoModificar.Text = "";
                        CargarArchivos();
                        ppCrearCarpeta.Hide();
                    }

                }
                else
                {
                    // crear
                    int IdCarpetaPadreSeleccionada;

                    bool esInt = int.TryParse(lblIdCarpetaPadreSeleccionada.Text, out IdCarpetaPadreSeleccionada);

                    if (esInt == false)
                    {
                        IdCarpetaPadreSeleccionada = 0;
                    }

                    oCarpeta.IdCarpetaPadre = IdCarpetaPadreSeleccionada;
                    oCarpeta.IdBrandSite = int.Parse(Session["IdBrandSite"].ToString());
                    oCarpeta.sNombre = txtNombreCarpeta.Text;
                    oCarpeta.CarpetaTrabajo = ckbCarpetaTrabajo.Checked;

                    oMetodo = oCarpeta.AgregarCarpeta();

                    if (oMetodo.Pass == true)
                    {
                        lblValidacionCrearCarpeta.Text = "";
                        lblValidacionCrearCarpeta.Visible = false;
                        ppCrearCarpeta.Hide();
                        CargarArchivos();
                    }
                    else
                    {
                        lblValidacionCrearCarpeta.Text = oMetodo.Message;
                        lblValidacionCrearCarpeta.Visible = true;
                        ppCrearCarpeta.Show();
                    }
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void imgPreview_Click1(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                ImageButton Status = (ImageButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;

                if (((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text == ".folder")
                {
                    lblIdCarpetaPadreSeleccionada.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;

                    List<cRuta> lRutas;
                    lRutas = (List<cRuta>)Session["RutaCarpetas"];
                    cRuta oRuta = new cRuta();
                    oRuta.IdCarpeta = int.Parse(lblIdCarpetaPadreSeleccionada.Text);
                    oRuta.sNombreCarpeta = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblNombre")))).Text;
                    if (lRutas == null)
                    {
                        lRutas = new List<cRuta>();
                    }

                    lRutas.Add(oRuta);

                    lblRuta.Text = "";
                    if (lRutas.Count > 0)
                    {
                        //lblRuta.CssClass = "divCajaGris";

                        for (int i = 0; i < lRutas.Count; i++)
                        {
                            lblRuta.Text = lblRuta.Text + lRutas[i].sNombreCarpeta + "   >   ";
                        }
                    }
                    else
                    {
                        //lblRuta.CssClass = lblRuta.CssClass.Replace("divCajaGris", "");
                    }

                    Session["RutaCarpetas"] = lRutas;

                    CargarArchivos();
                }
            }
            catch (Exception ex)
            {
                oSistema.saveLog(ex.ToString());
            }

        }

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            try
            {
                List<cRuta> lRutas = new List<cRuta>();
                lRutas = (List<cRuta>)Session["RutaCarpetas"];
                lRutas.RemoveAt(lRutas.Count - 1);
                if (lRutas.Count > 0)
                {
                    lblIdCarpetaPadreSeleccionada.Text = lRutas[lRutas.Count - 1].IdCarpeta.ToString();
                }
                else
                {
                    lblIdCarpetaPadreSeleccionada.Text = "0";
                }

                lblRuta.Text = "";
                if (lRutas.Count > 0)
                {
                    //lblRuta.CssClass = "divCajaGris";
                    for (int i = 0; i < lRutas.Count; i++)
                    {
                        lblRuta.Text = lblRuta.Text + lRutas[i].sNombreCarpeta + "   >   ";
                    }
                }
                else
                {
                    //lblRuta.CssClass = lblRuta.CssClass.Replace("divCajaGris", "");
                }

                Session["RutaCarpetas"] = lRutas;

                CargarArchivos();
            }
            catch (Exception ex)
            {
                oSistema.saveLog(ex.ToString());
            }
        }

        [WebMethod]
        public static string ActualizarArchivoEnCarpeta(int IdArchivo, int IdCarpeta)
        {
            try
            {
                cCarpeta oCarpeta = new cCarpeta();
                cMetodo oMetodo = new cMetodo();
                oMetodo = oCarpeta.ActualizarCarpetaDeArchivo(IdArchivo, IdCarpeta);
                oCarpeta = null;
                return oMetodo.oDato.ToString();
            }
            catch (Exception e)
            {
                cSistema oSistema = new cSistema();
                oSistema.saveLog(e.ToString());

                return "";
            }
        }

        [WebMethod]
        public static int ActualizarCarpetaEnCarpeta(int IdCarpetaAMover, int IdCarpeta)
        {
            try
            {
                cCarpeta oCarpeta = new cCarpeta();
                cMetodo oMetodo = new cMetodo();
                oMetodo = oCarpeta.ActualizarCarpetaDeCarpeta(IdCarpetaAMover, IdCarpeta);
                oCarpeta = null;
                return IdCarpetaAMover;
            }
            catch (Exception e)
            {
                cSistema oSistema = new cSistema();
                oSistema.saveLog(e.ToString());

                return 0;
            }
        }

        #endregion

        #region Mover de carpeta

        protected void lkbMover_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                lblArchivoSeleccionadoMov.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                lblExtensionSeleccionadaMov.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text;

                Session["RutaCarpetasMov"] = null;
                lkbAtrasMov.Visible = false;
                lblIdCarpetaSeleccionadaMov.Text = "0";
                SqlCommand cmd = new SqlCommand("SELECT IdCarpeta, Nombre FROM catCarpetas WHERE IdCarpetaPadre = 0 AND IdBrandSite = @IdBrandSite AND Activo = 1");
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(Session["IdBrandSite"].ToString()));
                oSistema.CargarGridView(gvCarpetas, cmd);
                ppMover.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarMover_Click(object sender, EventArgs e)
        {
            try
            {
                lblExtensionSeleccionadaMov.Text = "";

                lkbAtrasMov.Visible = false;

                Session["RutaCarpetasMov"] = null;

                lblIdCarpetaSeleccionadaMov.Text = "0";

                lblArchivoSeleccionadoMov.Text = "0";

                ppMover.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnAceptarMover_Click(object sender, EventArgs e)
        {
            try
            {
                Session["RutaCarpetasMov"] = null;
                lkbAtrasMov.Visible = false;

                cCarpeta oCarpeta = new cCarpeta();
                cMetodo oMetodo = new cMetodo();
                if (lblExtensionSeleccionadaMov.Text == ".folder")
                {
                    oMetodo = oCarpeta.ActualizarCarpetaDeCarpeta(int.Parse(lblArchivoSeleccionadoMov.Text), int.Parse(lblIdCarpetaSeleccionadaMov.Text));
                }
                else
                {
                    oMetodo = oCarpeta.ActualizarCarpetaDeArchivo(int.Parse(lblArchivoSeleccionadoMov.Text), int.Parse(lblIdCarpetaSeleccionadaMov.Text));
                }

                if (oMetodo.Pass == true)
                {
                    CargarArchivos();
                }

                lblExtensionSeleccionadaMov.Text = "";
                lblIdCarpetaSeleccionadaMov.Text = "0";
                lblArchivoSeleccionadoMov.Text = "0";
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbNombreCarpetaMov_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdCarpetaSeleccionadaMov.Text = ((System.Web.UI.WebControls.Label)(gvCarpetas.Rows[Row].FindControl("lblIdCarpetaMov"))).Text;

                SqlCommand cmd = new SqlCommand("SELECT IdCarpeta, Nombre FROM catCarpetas WHERE IdCarpetaPadre = @IdCarpetaPadre AND IdBrandSite = @IdBrandSite AND Activo = 1");
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(Session["IdBrandSite"].ToString()));
                cmd.Parameters.AddWithValue("@IdCarpetaPadre", int.Parse(lblIdCarpetaSeleccionadaMov.Text));
                oSistema.CargarGridView(gvCarpetas, cmd);


                List<cRuta> lRutasMov;
                lRutasMov = (List<cRuta>)Session["RutaCarpetasMov"];
                cRuta oRuta = new cRuta();
                oRuta.IdCarpeta = int.Parse(lblIdCarpetaSeleccionadaMov.Text);
                if (lRutasMov == null)
                {
                    lRutasMov = new List<cRuta>();
                }

                lRutasMov.Add(oRuta);

                Session["RutaCarpetasMov"] = lRutasMov;

                lkbAtrasMov.Visible = true;

                ppMover.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbAtrasMov_Click(object sender, EventArgs e)
        {
            try
            {
                List<cRuta> lRutasMov = new List<cRuta>();
                lRutasMov = (List<cRuta>)Session["RutaCarpetasMov"];

                if (lRutasMov.Count == 0)
                {
                    lkbAtrasMov.Visible = false;
                }
                else
                {
                    lRutasMov.RemoveAt(lRutasMov.Count - 1);

                    if (lRutasMov.Count == 0)
                    {
                        lblIdCarpetaSeleccionadaMov.Text = "0";
                        lkbAtrasMov.Visible = false;
                    }
                    else
                    {
                        lblIdCarpetaSeleccionadaMov.Text = lRutasMov[lRutasMov.Count - 1].IdCarpeta.ToString();
                    }

                    SqlCommand cmd = new SqlCommand("SELECT IdCarpeta, Nombre FROM catCarpetas WHERE IdCarpetaPadre = @IdCarpetaPadre AND IdBrandSite = @IdBrandSite AND Activo = 1");
                    cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(Session["IdBrandSite"].ToString()));
                    cmd.Parameters.AddWithValue("@IdCarpetaPadre", int.Parse(lblIdCarpetaSeleccionadaMov.Text));
                    oSistema.CargarGridView(gvCarpetas, cmd);
                }

                ppMover.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Descargar archivos

        protected void dlArchivos_ItemDataBound(object sender, DataListItemEventArgs e)
        {

        }

        protected void lkbDescargar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                string sIdArchivo = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                string sNombre = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblNombre")))).Text;
                string sUrl = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblUrl")))).Text;
                string sExtension = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text;
                int iTamaño = int.Parse(((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblBytes")))).Text);

                sUrl = sUrl.Split('?')[0];
                //sUrl = sUrl + sExtension;

                //sNombre = sNombre + sExtension;
                sNombre = sNombre.Replace(' ', '_');

                oBrandSite.DescargarArchivo(sUrl, sNombre, sExtension, iTamaño);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void dlArchivos_ItemCreated(object sender, DataListItemEventArgs e)
        {
            try
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lkbDescargar") as LinkButton;
                if (btn != null)
                {
                    btn.Click += lkbDescargar_Click;
                    scriptMan.RegisterPostBackControl(btn);
                }
            }
            catch (Exception erro)
            {
                oSistema.saveLog(erro.ToString());
            }

        }

        #endregion

        #region Obtener nombre del archivo

        protected void cbkEnviarCorreo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ppModificarArchivo.Show();

                if (cbkEnviarCorreo.Checked == true)
                {
                    btnModificarArchivo.Text = "Sí, modificar y enviar";
                    txtMensaje.Enabled = true;
                    txtMensaje.Visible = true;
                    lblMensajeUsuarios.Visible = true;
                }
                else
                {
                    btnModificarArchivo.Text = "Sí, modificar";
                    txtMensaje.Enabled = false;
                    txtMensaje.Visible = false;
                    lblMensajeUsuarios.Visible = false;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ckbDetectarNombre_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ppSubirArchivo.Show();

                if (ckbDetectarNombre.Checked == true)
                {
                    txtNombre.Enabled = false;
                    txtNombre.Visible = false;
                    txtNombre.Text = "<Nombre del archivo>";
                }
                else
                {
                    txtNombre.Enabled = true;
                    txtNombre.Visible = true;
                    txtNombre.Text = "";
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #endregion

        #region Clase Ruta

        // Clase para crear una lisat de tipo cRuta, que nos de el id de la carpeta y el nombre, de tal forma que podamos navegar de forma más comoda
        private class cRuta
        {
            public int IdCarpeta { get; set; }
            public string sNombreCarpeta { get; set; }
        }

        #endregion

    }
}