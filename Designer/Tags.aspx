﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="Tags.aspx.cs" Inherits="Identidad.Designer.WebForm6" meta:resourcekey="Designer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ Register Src="~/Controles/Examinar.ascx" TagPrefix="identidad" TagName="Examinar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/Tags.css" rel="stylesheet" type="text/css" />

    <div class="divFondoPagina">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                <div class="divTituloCentrado">
                        <asp:Label ID="lblTags" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblTags"></asp:Label>
                </div>
                <%--<hr/>--%>
                <br />
                <asp:Button ID="btnNuevoTag" ToolTip="Puedes crear todas las etiquetas (tags) que quieras añadir a carpetas o archivos, tanto para indicar el tipo de archivo como los usos o contenidos de éstos. Puedes añadir a cada archivo tantas etiquetas como quieras, mismos que te facilitarán la búsqueda y el entendimiento de cada uno." Style="margin-left:50px;" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnNuevoTag" OnClick="btnNuevoTag_Click" />
                <br /><br /><br />

                <asp:GridView ID="gvTags" CssClass="mGrid gridTag" runat="server" AutoGenerateColumns="false">
                     <Columns>

                    <asp:TemplateField >
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width:100px;">
                                            <asp:Label ID="lblIdTag" runat="server" DataField ="IdTag" Text='<%#Eval ("IdTag") %>' Visible="false" ></asp:Label>
                                            <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                            <asp:Image ID="imgImagen" class="tag" DataField ="Url" ImageUrl='<%#Eval ("Url") %>' runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblNombre" CssClass="NombreTag" runat="server" DatTaField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                            <asp:Label ID="lblExtension" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                            <br /><br />
                                            <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                            <br /><br />
                             
                                            <asp:LinkButton ID="lkbEditar" ToolTip="Cambia aquí el nombre, la imagen y la descripción de tus nuevas etiquetas." runat="server" meta:resourcekey="lkbEditar" OnClick="lkbEditar_Click"></asp:LinkButton>
                                            |
                                            <asp:LinkButton ID="lkbEliminar" ToolTip="¿Adiós?" runat="server" meta:resourcekey="lkbEliminar" OnClick="lkbEliminar_Click"></asp:LinkButton>
                                            &nbsp;&nbsp;
                                            <asp:Label ID="lblEtTodosMisBs" ToolTip="Si quieres usar etiquetas genéricas para todos los sitios que diseñes, haz clic aquí. Si las etiquetas tienen el estilo propio de una identidad, déjalo sin seleccionar." runat="server" style="font-size:85%" meta:resourcekey="lblEtTodosMisBs"></asp:Label> <asp:CheckBox ID="ckbTodosMisBS" runat="server" Checked='<%#Eval ("TodosMisBS") %>' DataField ="TodosMisBS" Enabled="False"/>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <hr />
                            </ItemTemplate>
                    </asp:TemplateField>

                     </Columns>
                </asp:GridView>

                <identidad:Examinar runat="server" id="eExaminar" />

                <!--Inicia Popup tag--->
                <panel id="pnlTag" runat="server" style="display:none" >
                    <div class="divTabla divEdicionTag Popup">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloEdicionTag" runat="server" meta:resourcekey="lblTituloEdicionTag" ></asp:Label>
                        </div>
                        <hr />
                        <br />
                        <div class="CentrarTag">
                            <asp:Label ID="lblIdTag" runat="server" visible="false"></asp:Label>
                            <asp:Label ID="lblIdArchivo" runat="server" visible="false"></asp:Label>
                            <asp:Image ID="imgTag" CssClass="tag" runat="server" />
                        </div>
                        
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblNombre" runat="server" meta:resourcekey="lblNombre" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtNombre" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblDescripcion" runat="server" meta:resourcekey="lblDescripcion" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtDescripcion" class=" Multiline" runat="server" TextMode="MultiLine" ></asp:TextBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblExtension" runat="server" meta:resourcekey="lblExtension" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtExtension" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                                
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblEtTodosBS" runat="server" meta:resourcekey="lblEtTodosBS"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:CheckBox ID="ckbTodosBS" runat="server"></asp:CheckBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons">
                                <asp:Button ID="btnSeleccionarArchivo" style="margin-left:10px;" class="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionarArchivo" OnClick="btnSeleccionarArchivo_Click" />
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoInput">
                                <asp:Label ID="lblValidacion" runat="server" visible="false" ></asp:Label>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons">
                                
                                <asp:Button ID="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                                <asp:Button ID="btnSubir" class="buttons colorVerde" runat="server" meta:resourcekey="btnSubir" OnClick="btnSubir_Click" />

                            </div>
                        </div>
                        <div class="divTR">

                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppTag" TargetControlID="Label1" runat="server" PopupControlID="pnlTag" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para tags--->

                <!--Inicia Popup para eliminar un tag--->
                <panel id="pnlEliminarTag" runat="server" style="display:none" >
                    <div class="divTabla" style="width:490px!important; height:250px!important; min-height:250px;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloEliminarTag" runat="server" meta:resourcekey="lblTituloEliminarTag" ></asp:Label>
                        </div>
                        <hr />
                        <br /><br />
                        <div class="divTR">
                            <div class="divTDCentrado">
                                <asp:Label ID="lblIdTagEliminar" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblConfirmarEliminar" runat="server" meta:resourcekey="lblConfirmarEliminar" ></asp:Label>
                                <br /><br /><br />
                                
                                <asp:Button ID="btnCancelarEliminar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" />
                                <asp:Button ID="btnEliminar" class="buttons colorRojo" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" />

                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppEliminarTag" TargetControlID="Label2" runat="server" PopupControlID="pnlEliminarTag" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".10" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para eliminar un tag--->

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        try {

            // Se muestra el menú seleccionado
            document.getElementById("lblTags").setAttribute("class", "Seleccionado");

            // Si hay un postback tengo que cargar por segunda vez por que no entra al script

            //Sys.Application.initialize();

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            if (prm != null) {
                prm.add_endRequest
                (
                    function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            document.getElementById("lblTags").setAttribute("class", "Seleccionado");
                        }
                    }
                );
            };

        } catch (e) {

        }

    </script>

    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

</asp:Content>
