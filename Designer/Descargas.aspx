﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="Descargas.aspx.cs" Inherits="Identidad.Designer.WebForm4" meta:resourcekey="Descargas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/Descargas.css" rel="stylesheet" type="text/css" />

    <div class="divFondoPagina">
        
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                
                <div class="divTituloCentrado">
                    <asp:Label ID="lblDescargas" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblDescargas"></asp:Label>
                </div>
                <%--<hr/>--%>

                <table class="Tabla" border="0">
                    <tr>
                        <td class="Izquierda">
                            <asp:Button ToolTip="Al hacer clic aquí, podrás subir un archivo de cualquier formato y quedará ubicado en este nivel. Si prefieres ordenarlo en una carpeta específica, haz clic en ella o crea una nueva." ID="btnNuevoArchivo" Style="margin-left:50px;" class="buttons colorVerde" runat="server" meta:resourcekey="btnNuevoArchivo" OnClick="btnNuevoArchivo_Click"/>
                            <asp:Button ToolTip="Haz clic para crear una nueva carpeta en este nivel, o entra primero a otra carpeta para crearla como una subcarpeta." ID="btnNuevaCarpeta" Style="margin-left:10px;" class="buttons colorVerde" runat="server" meta:resourcekey="btnNuevaCarpeta" OnClick="btnNuevaCarpeta_Click"/>
                            <asp:Label ID="lblIdCarpetaPadreSeleccionada" runat="server" Text="0" style="display:none;"></asp:Label>
                        </td>
                        <td class="Derecha">      
                            <asp:DropDownList ID="ddlItemsBusqueda" CssClass="DropDownList" runat="server" Visible="false"></asp:DropDownList>
                            
                            <asp:Label ID="lblBuscar" runat="server" meta:resourcekey="lblBuscar"></asp:Label>
                            
                            <asp:TextBox ID="txtBusqueda" Style="margin-right:5px; margin-left:5px; width:240px!important;" CssClass="TextBox BusquedaTextBox" runat="server"></asp:TextBox>
                            <asp:DropDownList ID="ddlBusqueda" runat="server" style="display:none;" ></asp:DropDownList>
                            <asp:Button ID="btnBuscar" ToolTip="Busca por nombre o palabras clave" Style="margin-right:12px;" class="buttons colorAzulO" runat="server" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click" />

                            <br /><br />
                            
                            <asp:Label ID="lblVer" runat="server" meta:resourcekey="lblVer"></asp:Label>
                            <asp:DropDownList ID="ddlTipo" Style="margin-right:136px;" Width="246px" CssClass="DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipo_SelectedIndexChanged">
                                <asp:ListItem Selected="True" Value="%">Todos los archivos</asp:ListItem>
                                <asp:ListItem Value="1">Solo archivos de trabajo</asp:ListItem>
                                <asp:ListItem Value="0">Solo archivos para consumidores de identidad</asp:ListItem>
                            </asp:DropDownList> 
                            
                            <br /><br />

                            <asp:Label ID="lblOrdenadoPor" runat="server" meta:resourcekey="lblOrdenadoPor"></asp:Label>
                            <asp:DropDownList ID="ddlOrdenadoPor" Style="margin-right:136px;" Width="246px" CssClass="DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOrdenadoPor_SelectedIndexChanged">
                                <asp:ListItem Value="FechaActivo DESC">Fecha de actualización</asp:ListItem>
                                <asp:ListItem Value="Nombre ASC">Nombre del archivo</asp:ListItem>
                                <asp:ListItem Value="Bytes DESC">Peso del archivo</asp:ListItem>
                            </asp:DropDownList> 
                            

                        </td>
                    </tr>
                </table>
                
                <br /><br />
                <div id="navCarpetas">
                    <div style="float:left; width:auto;">
                        <asp:Button ID="btnRegresar" Visible="false" Style="margin-left:50px;" class="buttons colorGris" runat="server" meta:resourcekey="btnRegresar" OnClick="btnRegresar_Click"/>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblRuta" runat="server"  ></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                    </div>
                
                    <div id="divPegar" class="invisible" style=" width:auto; text-align:left;" onclick="Pegar();">
                        <img class="icon icons8-Paste" alt="Pegar" title="Pegar" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAABOklEQVRIS+WV3U3DQBCEv00KIFQAJUAFpANaSYTNK8krjpS0QgVAB5RACaaAsOj8d2cnvjs7QgriJEuWd9fjmZ31Cp6jZDnIRU9KLiSXvvo6JuZGeb6BySsw6xYJSZHTPcpGjzzOYXorLD/dWAWSrUBmQrJwg+ZFPpBuTNnsQHMhXfWANOSe2gmxTHRt5QmCQP0l/p7ol5AW8ipGCXsimFiQmKb+HZDSedP7WFbtvP2L8PjhuOu4ppXec+BtIFBRY/oTC9KYIRaoNsN5gijZHCZ3ITbCw3o0kxIEc3mPkWg0SOjl7V9SOaCDe+KX6/tdSBsHjmYSkMvY9XSQM5DLHeZ/5C5pLS0ol1Hlfc8wRrnrcP26u8KC9A5jyF3ba9gb+10dOsgyOcldvmJ3qH4TZAGyHQJgc3UppLsfRYbvGg/DycQAAAAASUVORK5CYII=" width="20" height="20">
                    </div>
                </div>
                <br /><br /><br />

                <asp:DataList ID="dlArchivos" style="margin-left:23px; margin-bottom:23px;" RepeatColumns="3" RepeatDirection="Horizontal" runat="server" OnItemDataBound="dlArchivos_ItemDataBound" OnItemCreated="dlArchivos_ItemCreated" >
                    <ItemTemplate>
                        

                        <div id='divMosaico<%#Eval ("Extension").ToString().Replace('.','E') %><%#Eval("IdArchivo")%>' class="divMosaico" ondragover="allowDrop(event)" draggable="true" ondrop="drop(event, <%#Eval ("IdArchivo") %>, '<%#Eval ("Extension") %>')" ondragstart="drag(event, <%#Eval ("IdArchivo") %> , '<%#Eval ("Extension") %>')">
                            <table class="tablaMosaico">
                                <tr>
                                    <td >
                                        <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                        <%--<asp:Image ID="imgPreview" class="imagenMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" />--%>
                                        <div onmouseover="ShowMenu('divCopiar<%#Eval ("Extension").ToString().Replace('.','E') %><%#Eval ("IdArchivo") %>', event, '<%#Eval ("Extension") %>');" 
                                              onmouseout="HideMenu('divCopiar<%#Eval ("Extension").ToString().Replace('.','E') %><%#Eval ("IdArchivo") %>');">
                                            <asp:ImageButton ID="imgPreview" class="imagenMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" OnClick="imgPreview_Click1" />
                                            <div id="divCopiar<%#Eval ("Extension").ToString().Replace('.','E') %><%#Eval ("IdArchivo") %>" 
                                                style="display:none;" 
                                                onclick="Cortar(<%#Eval ("IdArchivo") %>, '<%#Eval ("Extension") %>');">
                                                <img class="icon icons8-Cutting-Coupon-Filled" alt="Cortar" title="Cortar" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABkAAAAZCAYAAADE6YVjAAABgUlEQVRIS8WW0VHCQBRFzzP8gxVoB1ICVCBWIFQADsFv/BUdsQPsQCvQErADrAAKIK7zsgsuYRlCEMxXZnY3J+/e+14iHOGSIzBIIYbHJpiyED9vgxruqxBd2n3Jm3A73nZGDA8jMA2QGZgplOrCzSx00O7l2q29CHFzG0DXFWJAWnDyCskHMBXievZwUYCDDPTBZShdQdIB2kK84tU+AAd5qrgKLry3Hwlxy/pVTCJfCWe8guaqbw1ETf2EqAbJsIgHWanXImwY9KE0zAIg6sD8HaSqclovId99IB4ucRO7JOd5U7QhkSZQyaoHtoLvttC9yxPX0B7Pk0TzXwMaiz7wJQKWYdgFprKKITU91do7vGy0TLp2BjmI9olUIGos+gSiU7/r9wVlOl4r4gsYC72+L0tRkKskNdoNPAVof8wnQq+yPlo03qL9dJbXoxTyO4U1810F/vkV/J5Y+ur8KkpeVhKYuIeHFH3rTef+V678wy/PsPxrbQLPO97fyqGL+QHxMr5+ef4TkwAAAABJRU5ErkJggg==" width="25" height="25">
                                            </div>
                                        </div>
                                        
                                    </td>
                                    <td>
                                        <asp:DataList ID="dlTags" RepeatColumns="1" runat="server">
                                            <ItemTemplate>
                                                <div>
                                                    <asp:Label ID="lblIdTxA" runat="server" DataField ="IdTxA" Text='<%#Eval ("IdTxA") %>' Visible="false" ></asp:Label>
                                                    <asp:Image ID="imgTag" class="tagMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Url") %>' runat="server" ToolTip='<%#Eval ("Descripcion") %>' />
                                                    <br />
                                                    <asp:LinkButton ID="lkbEliminarTag" runat="server" meta:resourcekey="lkbEliminarTag" OnClick="lkbEliminarTag_Click"></asp:LinkButton>
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="word-break:break-all;">
                                        <asp:Label ID="lblNombre" CssClass="TituloArchivo" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                        <asp:Label ID="lblExtension" CssClass="infoArchivo" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                        <asp:Label ID="lblArchivoTrabajo" CssClass="infoArchivo" runat="server" DataField ="ArchivoTrabajo" Text='<%#Eval ("ArchivoTrabajo") %>' Visible="false" ></asp:Label>
                                        <br /><br />
                                        <asp:Label ID="lblDescripcion" CssClass="DescripcionArchivo" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                        <br /><br />
                                        <asp:Label ID="lblFecha" CssClass="infoArchivo" runat="server" DataField ="FechaActivo" Text='<%#Eval ("FechaActivo") %>' ></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPeso" CssClass="infoArchivo" runat="server" DataField ="PesoBytesArchivo" Text='<%#Eval ("PesoBytesArchivo") %>' ></asp:Label>
                                        <br />
                                        
                                        <asp:Label ID="lblRuta" CssClass="infoArchivo" Font-Italic="true" runat="server" Text="" Visible="false" ></asp:Label>

                                        <br />

                                        <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url")%>' Visible="false" />

                                        <asp:Label ID="lblBytes" runat="server" Text='<%#Eval("Bytes")%>' Visible="false" />
 
                                        <asp:HyperLink ID="hplDescargar" runat="server" Target="_blank" meta:resourcekey="hplDescargar" DataField="Url" download='<%#Eval ("Nombre") %>' data-downloadurl='<%#Eval ("Url") %>' NavigateUrl='<%#Eval ("Url") %>' href='<%#Eval ("Url") %>' Visible="false"></asp:HyperLink>


                                        <asp:LinkButton ID="lkbDescargar" runat="server" Text="Descarga" Visible="true" ></asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lkbEditar" runat="server" meta:resourcekey="lkbEditar" OnClick="lkbEditar_Click" ></asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lkbMover" runat="server" meta:resourcekey="lkbMover" OnClick="lkbMover_Click" ></asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lkbEliminar" runat="server" meta:resourcekey="lkbEliminar" OnClick="lkbEliminar_Click"></asp:LinkButton>
                                        &nbsp;
                                        <asp:LinkButton ID="lkbAgregarTag" runat="server" meta:resourcekey="lkbAgregarTag" OnClick="lkbAgregarTag_Click"></asp:LinkButton>

                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    
                        
                    </ItemTemplate>
                </asp:DataList>

                                                                              
                <!--Inicia Popup para crear carpeta--->
                <panel id="pnlCrearCarpeta" runat="server" style="display:none" >
                    <div id="divCrearCarpeta" class="divTabla divCrearCarpeta" style="height:300px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="Label5" runat="server" meta:resourcekey="lblTituloCrearCarpeta" ></asp:Label>
                        </div>
                        <hr />
                        <br /><br />
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="Label6" runat="server" meta:resourcekey="lblNombreCarpeta" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtNombreCarpeta" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblCarpetaTrabajo" runat="server" meta:resourcekey="lblCarpetaTrabajo" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:CheckBox ID="ckbCarpetaTrabajo" runat="server" Style="padding-top:30px;"></asp:CheckBox>
                            </div>
                        </div>
                        <div class="divTR" style="height:30px;">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoInput">
                                <asp:Label ID="lblValidacionCrearCarpeta" runat="server" visible="false" style="margin-left:18px;" ></asp:Label>
                            </div>
                        </div>
                        <br /><br />
                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons" style="margin-top:-20px;">
                                
                                <asp:Button ID="btnCancelarCrearCarpeta" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarCrearCarpeta" OnClick="btnCancelarCrearCarpeta_Click" />
                                <asp:Button ID="btnCrearCarpeta" OnClientClick="return ValidaInputs('divCrearCarpeta');" class="buttons colorVerde" runat="server" meta:resourcekey="btnCrearCarpeta" OnClick="btnCrearCarpeta_Click" />

                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label13" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppCrearCarpeta" BehaviorID="ppCrearCarpeta" TargetControlID="Label13" runat="server" PopupControlID="pnlCrearCarpeta" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                   <%-- <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>--%>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para crear carpeta--->


                <!--Inicia Popup para mover un archivo o carpeta--->
                <panel id="pnlMover" runat="server" style="display:none" >
                    <div id="divMover" class="divTabla divMover" style="height:325px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="Label7" runat="server" meta:resourcekey="lblTituloMover" ></asp:Label>
                        </div>
                        <hr />
                        <div class="divTR" style="overflow-y:scroll; height:170px;">
                            <asp:Label ID="lblIdCarpetaSeleccionadaMov" runat="server"  text="0" Visible="false" ></asp:Label>
                            <asp:Label ID="lblExtensionSeleccionadaMov" runat="server"  text="" Visible="false" ></asp:Label>
                            <asp:Label ID="lblArchivoSeleccionadoMov" runat="server"  text="0" Visible="false" ></asp:Label>
                            <asp:LinkButton ID="lkbAtrasMov" runat="server" Text="ir a carpeta anterior" OnClick="lkbAtrasMov_Click"></asp:LinkButton>
                            <asp:GridView ID="gvCarpetas" runat="server" 
                                AutoGenerateColumns="false" CssClass="mGrid">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdCarpetaMov" runat="server" DataField ="IdCarpeta" Text='<%#Eval ("IdCarpeta") %>' Visible="false" ></asp:Label>
                                            <img src="../Images/Site/carpetita.png" width="13" />
                                            <asp:LinkButton ID="lkbNombreCarpetaMov" runat="server" DataField ="lblNombreCarpetaMov" Text='<%#Eval ("Nombre") %>' OnClick="lkbNombreCarpetaMov_Click"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>


                        </div>
                        <div class="divTR" style="height:20px;">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoInput">
                                <asp:Label ID="lblValidacionMover" runat="server" visible="false" style="margin-left:18px;" ></asp:Label>
                            </div>
                        </div>
                        
                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons" style="margin-top:-20px;">
                                
                                <asp:Button ID="btnCancelarMover" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarMover" OnClick="btnCancelarMover_Click" />
                                <asp:Button ID="btnAceptarMover" OnClientClick="return ValidaInputs('divMover');" class="buttons colorVerde" runat="server" meta:resourcekey="btnAceptarMover" OnClick="btnAceptarMover_Click" />

                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppMover" BehaviorID="ppMover" TargetControlID="Label10" runat="server" PopupControlID="pnlMover" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                   <%-- <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>--%>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para mover un archivo o carpeta--->

                <!--Inicia Popup para subir un archivo--->
                <panel id="pnlSubirArchivo" runat="server" style="display:none" >
                    <div id="divSubir" class="divTabla divSubirArchivo" style="height:550px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloSubirArchivo1" runat="server" meta:resourcekey="lblTituloSubirArchivo1" ></asp:Label>
                        </div>
                        <hr />
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblDetectarNombre" runat="server" meta:resourcekey="lblDetectarNombre" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:CheckBox ID="ckbDetectarNombre" runat="server" Style="padding-top:30px;" AutoPostBack="true" OnCheckedChanged="ckbDetectarNombre_CheckedChanged" ></asp:CheckBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblNombre" runat="server" meta:resourcekey="lblNombre" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtNombre" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblDescripcion" runat="server" meta:resourcekey="lblDescripcion" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:TextBox ID="txtDescripcion" class="Multiline" runat="server" TextMode="MultiLine"></asp:TextBox>
                            </div>
                        </div>

                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblArchivoTrabajo" runat="server" meta:resourcekey="lblArchivoTrabajo" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:CheckBox ID="cbArchivoTrabajo" runat="server" Style="padding-top:30px;" OnCheckedChanged="cbArchivoTrabajo_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                            </div>
                        </div>
                        <div class="divTR" style="height:155px;">
                            <div class="divTDIzq">
                                <asp:Label ID="lblGrupos" runat="server" meta:resourcekey="lblGrupos" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:DropDownList ID="ddlGrupos" runat="server" CssClass="DropDownList" ></asp:DropDownList>
                                <asp:Button ID="btnAgregar" style="width:20px; height:30px!important;" CssClass="buttons colorVerde" runat="server" meta:resourcekey="lkbAgregarGrupo" OnClick="btnAgregar_Click"></asp:Button>
                                <br /><br />
                                <div style="min-height:100px; overflow-y:auto; height:100px; margin-left:-3px;">
                                    <asp:GridView ID="gvGruposQueVen" runat="server" AutoGenerateColumns="false" CssClass="mGrid" OnRowDeleting="gvGruposQueVen_RowDeleting" OnRowDeleted="gvGruposQueVen_RowDeleted">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIdGrupo" runat="server" DataField ="IdGrupo" Text='<%#Eval ("IdGrupo") %>' Visible="false" ></asp:Label>
                                                    <asp:Label ID="lblNombreGrupo" runat="server" DataField ="NombreGrupo" Text='<%#Eval ("NombreGrupo") %>' ></asp:Label>
                                                    <asp:Button ID="btnElimigarGrupo" style=" min-width:20px!important; width:25px!important; min-height:25px!important; height:20px!important;" CssClass="buttons colorGris" runat="server" meta:resourcekey="lkbEliminarGrupo" OnClick="btnElimigarGrupo_Click"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblArchivo" runat="server" meta:resourcekey="lblArchivo" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:FileUpload ID="fluArchivo" runat="server" ></asp:FileUpload>
                            </div>
                        </div>
                        <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblPreview" runat="server" meta:resourcekey="lblPreview" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput">
                                <asp:FileUpload ID="fluPreview" runat="server"></asp:FileUpload>
                            </div>
                        </div>

                        <div class="divTR" style="height:30px;">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoInput">
                                <asp:Label ID="lblValidacion" runat="server" visible="false" style="margin-left:18px;" ></asp:Label>
                            </div>
                        </div>
                        <div class="divTR" style="margin-top:-5px">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons" style="margin-top:-20px;">
                                
                                <asp:Button ID="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                                <asp:Button ID="btnSubir" OnClientClick="return ValidaInputs('divSubir');" class="buttons colorVerde" runat="server" meta:resourcekey="btnSubir" OnClick="btnSubir_Click" />

                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppSubirArchivo" BehaviorID="ppSubirArchivo" TargetControlID="Label1" runat="server" PopupControlID="pnlSubirArchivo" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                   <%-- <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>--%>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para subir un archivo--->

                <!--Inicia Popup para eliminar un archivo--->
                <panel id="pnlEliminarArchivo" runat="server" style="display:none" >
                    <div class="divTabla" style="height:300px!important; width:450px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloEliminar" runat="server" meta:resourcekey="lblTituloEliminar" ></asp:Label>
                        </div>
                        <hr />
                        <br /><br /><br />
                        <div class="divTR">
                            <div class="divTDCentrado">
                                <asp:Label ID="lblIdArchivoEliminar" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblExtensionEliminar" runat="server" visible="false" ></asp:Label>
                                <asp:Label ID="lblConfirmarEliminar" runat="server" meta:resourcekey="lblConfirmarEliminar" ></asp:Label>
                                <br /><br />
                                
                                <asp:Button ID="btnCancelarEliminar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" />
                                <asp:Button ID="btnEliminar" class="buttons colorRojo" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" />

                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppEliminarArchivo" TargetControlID="Label2" runat="server" PopupControlID="pnlEliminarArchivo" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para eliminar un archivo--->

                <!--Inicia Popup para modificar un archivo--->
                <panel id="pnlModificarArchivo" runat="server" style="display:none" >
                    <div id="divModificar" class="divTabla" style="height:600px!important; width:970px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloModificarArchivo" runat="server" meta:resourcekey="lblTituloModificarArchivo" ></asp:Label>
                        </div>
                        <hr />
                        <br />

                        <div style="float:left; width:54%!important; min-width:54%!important; height:450px; border:none;">                        
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblIdArchivoModificar" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblNombreModificar" runat="server" meta:resourcekey="lblNombreModificar" ></asp:Label>
                                    <asp:Label ID="lblExtensionModificar" runat="server"></asp:Label>
                                    
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtNombreModificar" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblDescripcionModificar" runat="server" meta:resourcekey="lblDescripcionModificar" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtDescripcionModificar" class="Multiline" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                            </div>


                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblArchivoTrabajoModificar" runat="server" meta:resourcekey="lblArchivoTrabajo" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                    <asp:CheckBox ID="cbArchivoTrabajoModificar" runat="server" Style="padding-top:30px;" OnCheckedChanged="cbArchivoTrabajoModificar_CheckedChanged" AutoPostBack="true" ></asp:CheckBox>
                                </div>
                            </div>
                            <div class="divTR" style="height:200px;">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblGruposModificar" runat="server" meta:resourcekey="lblGrupos" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                    <asp:DropDownList ID="ddlGruposModificar" runat="server" CssClass="DropDownList" ></asp:DropDownList>
                                    <asp:Button ID="btnAgregarGrupoModificar" style="width:20px; height:30px!important; margin-left:130px; margin-top:10px;" CssClass="buttons colorVerde" runat="server" meta:resourcekey="lkbAgregarGrupo" OnClick="btnAgregarGrupoModificar_Click"></asp:Button>
                                    <br /><br />
                                    <div style="min-height:100px; overflow-y:auto; height:110px; margin-left:-3px; width:69%">
                                        <asp:GridView ID="gvGruposQueVenModificar" runat="server" AutoGenerateColumns="false" CssClass="mGrid" OnRowDeleting="gvGruposQueVenModificar_RowDeleting" OnRowDeleted="gvGruposQueVenModificar_RowDeleted">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblIdGrupo" runat="server" DataField ="IdGrupo" Text='<%#Eval ("IdGrupo") %>' Visible="false" ></asp:Label>
                                                        <asp:Label ID="lblNombreGrupo" runat="server" DataField ="NombreGrupo" Text='<%#Eval ("NombreGrupo") %>' ></asp:Label>
                                                        <asp:Button ID="btnElimigarGrupoModificar" style=" min-width:20px!important; width:25px!important; min-height:25px!important; height:20px!important;" CssClass="buttons colorGris" runat="server" meta:resourcekey="lkbEliminarGrupo" OnClick="btnElimigarGrupoModificar_Click"></asp:Button>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblArchivoModificar" runat="server" meta:resourcekey="lblArchivo" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput" style="width:465px;">
                                    <asp:FileUpload ID="fluArchivoModificar" runat="server" ></asp:FileUpload>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblPreviewModificar" runat="server" meta:resourcekey="lblPreview" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput" style="width:465px;">
                                    <asp:FileUpload ID="fluPreviewModificar" runat="server"></asp:FileUpload>
                                </div>
                            </div>

                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:Label ID="lblValidacionModificacion" runat="server" Visible="false" Style="margin-left:11px;" ></asp:Label>
                                </div>
                            </div>
                        </div>

                        <div style="float:right; width:44%!important; min-width:44%!important; height:450px; border:none;">
                            <div style="width:520px; max-height:150px!important; min-height:150px; height:150px; border:none; margin-left:auto; margin-right:auto; text-align:center; display:table-cell; vertical-align:middle;">
                                <asp:Image ID="imgImagen" style="image-rendering:optimizeSpeed; max-width:150px; height:auto; max-height:150px!important; background-color:none; background-repeat:no-repeat; background-position:center;" runat="server" />
                                <asp:Image ID="imgImagenPreview" style="image-rendering:optimizeSpeed; max-width:150px; height:auto; max-height:150px!important; background-color:none; background-repeat:no-repeat; background-position:center;" runat="server" />
                            </div>
                            <div style="width:200px; border:none; margin-left:auto; margin-right:auto; text-align:center;">
                                <asp:Label ID="lblArchivoPreview" runat="server" meta:resourcekey="lblArchivoPreview"></asp:Label>
                            </div>
                            <br /><br /><br />
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblEnviarCorreo" runat="server" meta:resourcekey="lblEnviarCorreo" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                    <asp:CheckBox ID="cbkEnviarCorreo" runat="server" Style="padding-top:30px;" OnCheckedChanged="cbkEnviarCorreo_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblMensajeUsuarios" runat="server" meta:resourcekey="lblMensajeUsuarios" Visible="false" ></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtMensaje" TextMode="MultiLine" style="height:100px;" CssClass="TextBox Multiline" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <br />
                        <div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoButtons" <%--style="margin-top:-20px;"--%>>
                                    <asp:Button ID="btnCancelarModificacion" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarModificacion" OnClick="btnCancelarModificacion_Click" />
                                    <asp:Button ID="btnModificarArchivo" OnClientClick="return ValidaInputs('divModificar');" AutoPostBack="true" class="buttons colorVerde" runat="server" meta:resourcekey="btnModificarArchivo" OnClick="btnModificarArchivo_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppModificarArchivo" TargetControlID="Label3" runat="server" PopupControlID="pnlModificarArchivo" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" BehaviorID="bhModificarArchivo">
                    <%--<Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>--%>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para modificar un archivo--->

                <!--Inicia Popup para agregar un tag--->
                <panel id="pnlAgregarTag" runat="server"  style="display:none;" >
                    <div class="divTabla divAgregarTag Popup" style="height:540px!important;">
                        <div class="divTablaTitulo">
                            <asp:Label ID="lblTituloAgregarTag" runat="server" meta:resourcekey="lblTituloAgregarTag" ></asp:Label>
                        </div>
                        <hr />

                        <asp:Label ID="lblIdArchivoParaTag" runat="server" Visible="false"></asp:Label>
                        
                        <div class="divSeleccionaTag">
                            <asp:GridView ID="gvSeleccionaTag" CssClass="mGrid" runat="server" AutoGenerateColumns="false" >
                                <Columns>
                                    <asp:TemplateField >
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="width:100px; vertical-align:top">
                                                            <asp:Label ID="lblIdTagSeleccionado" runat="server" DataField ="IdTag" Text='<%#Eval ("IdTag") %>' Visible="false" ></asp:Label>
                                                            <asp:ImageButton ID="imgPreview" CssClass="previewTag" DataField ="Url" ImageUrl='<%#Eval ("Url") %>' runat="server" OnClick="imgPreview_Click"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblNombre" runat="server" CssClass="NombreTag" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                            <asp:Label ID="lblExtension" runat="server" DataField ="Nombre" Text='<%#Eval ("Extension") %>'></asp:Label>
                                                            <br /><br />
                                                            <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                                            <br /><br />
                                                            <asp:Button ID="btnSeleccionar" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionar" OnClick="btnSeleccionar_Click"></asp:Button>
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="divTR">
                            <div class="divTDIzq">

                            </div>
                            <div class="divTDDerechoButtons">
                                <br />
                                <asp:Button ID="btnCancelarTag" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarTag" OnClick="btnCancelarTag_Click" />
                            </div>
                        </div>
                        
                    </div>
                </panel>

                <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppAgregarTag" TargetControlID="Label4" runat="server" PopupControlID="pnlAgregarTag" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>
                <!--Termina Popup para agregar un tag --->

            </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSubir" />
                    <asp:PostBackTrigger ControlID="btnModificarArchivo" />
                    
                </Triggers>
        </asp:UpdatePanel>
    </div>
    

    <link href="../CSS/autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery-ui-1.11.2.js" type="text/javascript"></script>
    
    <script type="text/javascript">

        $(document).ready
        (
            function autocompleta() {
                var items = document.getElementById('ContentPlaceHolder1_ddlBusqueda');
                var valor = 0;
                var tamano = items.length;

                var elementos = new Array(tamano);

                for (var indice = 0; indice < tamano; indice++) {
                    if (items.options[indice].text != null || items.options[indice].text != "") {
                        elementos[indice] = items.options[indice].text;
                    }
                }

                $("#ContentPlaceHolder1_txtBusqueda").autocomplete
                ({
                    source: elementos
                });

            }
        );

    </script>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblDescargas").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblDescargas").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

    <!-- Bloqueo la tecla enter en toda la página -->
    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

    <!-- Permito la tecla enter solo cuando key up el txtBuscar -->
    <script type="text/javascript">

        $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
            if (e.keyCode == 13) {

                $get('ContentPlaceHolder1_btnBuscar').click();
            }
        })
        
        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) 
        {
            prm.add_endRequest(function (sender, e) 
            {
                if (sender._postBackSettings.panelsToUpdate != null) 
                {
                    $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
                        if (e.keyCode == 13) {
                            
                            $get('ContentPlaceHolder1_btnBuscar').click();
                        }
                    })
                }

            });
         }
        
    </script>

    <!-- Drag and drop para archivos -->
    <script type="text/javascript">

        function allowDrop(ev)
        {
            ev.preventDefault();
        }

        function drag(ev, IdArchivo, Extension)
        {
            try
            {
                if (Extension != ".folder")
                {
                    ev.dataTransfer.setData("text", Extension + '/' + IdArchivo);
                    
                }

            } catch (e)
            {
                //alert(e);
            }
        }

        function drop(ev, IdCarpetaDrop, Extension)
        {
            if (Extension == ".folder")
            {
                ev.preventDefault();

                var transfer = ev.dataTransfer.getData("text").split("/");

                var IdArchivoDrag = transfer[1];
                var ExtensionArchivo = transfer[0];

                if (ExtensionArchivo != ".folder") {

                    if (IdArchivoDrag != IdCarpetaDrop || ExtensionArchivo != Extension) {
                        if (!isNaN(IdArchivoDrag)) {
                            //alert("Update de " + IdArchivoDrag + " a " + IdCarpetaDrop);

                            $.ajax({
                                type: "POST",
                                url: "Descargas.aspx/ActualizarArchivoEnCarpeta",
                                data: "{ 'IdArchivo' : '" + IdArchivoDrag + "','IdCarpeta':'" + IdCarpetaDrop + "'}",
                                contentType: "application/json; charset=utf-8",
                                dataType: "json",
                                success: function () {
                                    //alert("success");
                                },
                                complete: function (data) {

                                    var dato = '#' + data.responseText.replace('{"d":', '').replace('}', '').replace('"', '').replace('"', '');

                                    $(dato).css({ "display": "none" });
                                    $(dato).attr('style', 'display:none');

                                    $(dato).parent().css("display", "none");
                                    $(dato).parent().attr('style', 'display:none');

                                },
                                failure: function (response) {
                                    //alert("No podemos procesar tu ");
                                    //alert(response.d);
                                },
                                error: function (data) {
                                    //alert("error");
                                    //alert(data);
                                }
                            });
                        }

                    }
                }
            }
        }

    </script>

    <!--Cortar y pegar un archivo-->
    <script type="text/javascript">

        function ValidarPapelera()
        {
            var IdArchivo = localStorage.getItem('IdArchivoCortar');
            var Extension = localStorage.getItem('ExtensionCortar');

            if (IdArchivo == 'null' || IdArchivo == null)
            {
                $("#divPegar").removeClass('visible').addClass('invisible');
            }
            else
            {
                $("#divPegar").removeClass('invisible').addClass('visible');
            }

            if (Extension == 'null' || Extension == null)
            {
                $("#divPegar").removeClass('visible').addClass('invisible');
            }
            else {
                $("#divPegar").removeClass('invisible').addClass('visible');
            }
        }
        
        $(document).ready(function ()
        {
            ValidarPapelera();
        });

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    
                        ValidarPapelera();
                    
                }

            });
        }

        function Pegar()
        {
            var IdArchivo = localStorage.getItem('IdArchivoCortar');
            var Extension = localStorage.getItem('ExtensionCortar');

            var IdCarpeta = document.getElementById('ContentPlaceHolder1_lblIdCarpetaPadreSeleccionada').textContent;

            

            if (Extension != ".folder") {
                $.ajax({
                    type: "POST",
                    url: "Descargas.aspx/ActualizarArchivoEnCarpeta",
                    data: "{ 'IdArchivo' : '" + IdArchivo + "','IdCarpeta':'" + IdCarpeta + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function () {
                        //alert("success");
                    },
                    complete: function (data) {
                        localStorage.setItem('IdArchivoCortar', null);
                        localStorage.setItem('ExtensionCortar', null);
                        $("#divPegar").removeClass('visible').addClass('invisible');
                    },
                    failure: function (response) {
                        localStorage.setItem('IdArchivoCortar', null);
                        localStorage.setItem('ExtensionCortar', null);
                        document.getElementById('divPegar').style.display = 'none';
                    },
                    error: function (data) {
                        localStorage.setItem('IdArchivoCortar', null);
                        localStorage.setItem('ExtensionCortar', null);
                        document.getElementById('divPegar').style.display = 'none';
                    }
                });
            }
            else
            {
                if (IdArchivo != IdCarpeta) {

                    $.ajax({
                        type: "POST",
                        url: "Descargas.aspx/ActualizarCarpetaEnCarpeta",
                        data: "{ 'IdCarpetaAMover' : '" + IdArchivo + "','IdCarpeta':'" + IdCarpeta + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function () {
                            //alert("success");
                        },
                        complete: function (data) {
                            localStorage.setItem('IdArchivoCortar', null);
                            localStorage.setItem('ExtensionCortar', null);
                            $("#divPegar").removeClass('visible').addClass('invisible');
                        },
                        failure: function (response) {
                            localStorage.setItem('IdArchivoCortar', null);
                            localStorage.setItem('ExtensionCortar', null);
                            document.getElementById('divPegar').style.display = 'none';
                        },
                        error: function (data) {
                            localStorage.setItem('IdArchivoCortar', null);
                            localStorage.setItem('ExtensionCortar', null);
                            document.getElementById('divPegar').style.display = 'none';
                        }
                    });
                }
                else
                {
                    alert("No puedes pegar aquí tu carpeta.");
                }
            }

             

            $('#ContentPlaceHolder1_txtBusqueda').val('');

            var foo = document.getElementById("ContentPlaceHolder1_btnBuscar");

            foo.addEventListener("click", function ()
            {
                //display("Clicked");

            }, false);

            foo.click();
            
        }

        function Cortar(IdArchivo, Extension)
        {
            localStorage.setItem('IdArchivoCortar', IdArchivo);
            localStorage.setItem('ExtensionCortar', Extension);
            
            $("#divPegar").removeClass('invisible').addClass('visible');

            $("#divMosaico" + Extension.replace('.', 'E') + IdArchivo).css({ "display": "none" });
            $("#divMosaico" + Extension.replace('.', 'E') + IdArchivo).attr('style', 'display:none');
            
            $("#divMosaico" + Extension.replace('.', 'E') + IdArchivo).parent().css("display", "none");
            $("#divMosaico" + Extension.replace('.', 'E') + IdArchivo).parent().attr('style', 'display:none');
        }

        function ShowMenu(control, e, Extension)
        {
            document.getElementById(control).style.position = 'absolute';
            document.getElementById(control).style.display = 'inline';
            document.getElementById(control).style.marginTop = '15px';
         
        }

        function HideMenu(control)
        {
            document.getElementById(control).style.display = 'none';
        }

    </script>

</asp:Content>
