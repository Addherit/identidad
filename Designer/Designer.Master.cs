﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI;
using System.Collections.Generic;
using System.Collections;
 
namespace Identidad.Designer
{
    public partial class Designer : System.Web.UI.MasterPage
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cUsuario oUsuario = new cUsuario();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();
        WebForm1 webform = new WebForm1();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.Form["__EVENTTARGET"] == "regresar")
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
                else if (Request.Form["__EVENTTARGET"] == "pago")
                    Response.Redirect("~/site/pagina-datos-pago.aspx", false);

                Session["bandera"] = "0";
                //revision de que sea el usuario de diseño para que pueda entrar a todos los manuales sin importar su estado.
                int vencido;
                if (int.Parse(Session["IdUsuario"].ToString()) == 671)
                {   vencido = 0; }
                else {  vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString())); }

                if (vencido == 1)
                {
                    if(oUsuario.esAdministador(Session["IdUsuario"].ToString() , Session["IdBrandSite"].ToString()))
                    {
                        Session["bandera"] = "2";
                        //MPE.Show();
                    }
                    else
                    {
                        Session["bandera"] = "1";
                        //popup
                    }
                }
                if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/site/login-registro.aspx", false);

                    return;
                }
                lblDiasPruebas.Text = webform.GetDiasPrueba(int.Parse(Session["IdBrandSite"].ToString()));
                lblBrandSite.Text = Session["IdBrandSite"].ToString();
                lblIdUsuario.Text = Session["IdUsuario"].ToString();
                
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        fillMisSitios();
                        imgUsuario.ImageUrl = Session["FotoUsuario"].ToString() + "?t=" + DateTime.Now.Ticks.ToString();

                        cUsuario oUsuario = new cUsuario();

                        Session["IdAdministradorBS"] = oUsuario.IdUsuarioAdministradorSitio(Session["IdBrandSite"].ToString());

                        if (Session["IdUsuario"].ToString() == Session["IdAdministradorBS"].ToString())
                        {
                            Session["EsAdministrador"] = "1";
                            Session["PuedeEditiar"] = true;
                            lkbSalir.Visible = true;
                        }
                        else
                        {
                            Session["EsAdministrador"] = "0";
                            Session["PuedeEditiar"] = oUsuario.EsDeMiEquipo(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
                            lkbSalir.Visible = false;  
                        }

                    }

                    Response.CacheControl = "private";
                    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);

                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. y se disparan por medio de un control en el HTML :)

        #region Eventos
        
        protected void fillMisSitios()
        {
            oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
            if (ddlmissitios.Items.Count > 1)
            {
                //ppIniciarSesion.Hide();

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
            }
            else
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlmissitios.Items[0].Value.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlmissitios.SelectedValue.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        Session["idBrandSite"] = sUrl[1];
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }


                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/" + sUrlFinal, false);
                    
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbVerSitio_Click(object sender, EventArgs e)
        {
            try
            {

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Brandsite/Login.aspx?Id=" + Session["IdBrandSite"].ToString(), false);
            }
            catch (Exception Error)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        protected void lkbSalir_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IdBrandSite"] = null;
                Session["IdAdministradorBS"] = null;
                Session["EsAdministrador"] = null;
                Session["PuedeEditiar"] = null;

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IdUsuario"] = null;
                Session["IdBrandSite"] = null;
                Session["FotoUsuario"] = null;
                Session["NombreUsuario"] = null;
                Session["IdAdministradorBS"] = null;
                Session["EsAdministrador"] = null;
                Session["PuedeEditiar"] = null;

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
                Response.Cookies["Cookie"].Expires = DateTime.Now.AddDays(-1d);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbActualizarMiPerfil_Click(object sender, EventArgs e)
        {
            try
            {
                MiPerfil.setIdUsuario(Session["IdUsuario"].ToString(), true);

                ppActualizarMiPerfil.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbCambiarBannerSuperior_Click(object sender, EventArgs e)
        {
            try
            {
                eBannerSuperior.Show(Session["IdBrandSite"].ToString(), "Te recomendamos seleccionar una imagen .jpg de 1024 X 250 pixeles.", true);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Este método no se usa, sin embargo al ser una posible opción a futuro lo dejo.
        protected void lkbCambiarBackground_Click(object sender, EventArgs e)
        {
            try
            {
                eBackground.Show(Session["IdBrandSite"].ToString());
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);

                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();

                if (Session["IdBrandSite"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);

                    return;
                }

                // Agrega la referencia a la hoja de estilo dinámica
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/BrandSites/Id" + Session["IdBrandSite"].ToString() + ".css?t=<%= DateTime.Now.Ticks %> />"));

                // Crea eventos
                eBannerSuperior.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eBannerSuperior_OnArchivoSeleccionado);
                eBackground.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eBackground_OnArchivoSeleccionado);
                MiPerfil.EstadoCatalogo += new Identidad.Controles.MiPerfil.EventHandler(MiPerfil_OnEstadoCatalogo);
                 
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eBannerSuperior_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.CambiarBannerPrincipal(eBannerSuperior.ObtenerIdArchivoSeleccionado(), Session["IdBrandSite"].ToString(), IdUsuario);

                string sUrlSeleccionada = eBannerSuperior.ObtenerUrlSeleccionada();

                cabeza.Attributes.Add("style", "background-image: url('" + sUrlSeleccionada.Trim() + "');");

                if (oMetodo.Pass == true)
                {
                    cabeza.Attributes.Add("style", "background-image: url('" + sUrlSeleccionada.Trim() + "');");

                    if (oBrandSite.CrearTemplate(Session["IdBrandSite"].ToString()) == true)
                    {
                        cabeza.Attributes.Add("style", "background-image: url('" + sUrlSeleccionada.Trim() + "');");

                        Page_Init(null, null);
                        
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "reloadPage();", true);

                        cabeza.Attributes.Add("style", "background-image: url('" + sUrlSeleccionada.Trim() + "');");

                    }
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eBackground_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                //oMetodo = oBrandSite.CambiarBackground(eBackground.ObtenerIdArchivoSeleccionado(), Session["IdBrandSite"].ToString(), Session["IdUsuario"].ToString());

                //if (oMetodo.Pass == true)
                //{
                //    if (oBrandSite.CrearTemplate(Session["IdBrandSite"].ToString()) == true)
                //    {
                //        Page_Init(null, null);
                //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "reloadPage();", true);
                //    }
                //}
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en MiPerfil
        private void MiPerfil_OnEstadoCatalogo(object sender, EventArgs e)
        {
            try
            {
                oMetodo = MiPerfil.ObtenerEstado();

                if (oMetodo.Pass == false)
                {
                    ppActualizarMiPerfil.Show();
                }
                else
                {
                    imgUsuario.ImageUrl = Session["FotoUsuario"].ToString(); 

                    ppActualizarMiPerfil.Hide();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }



        #endregion

    }
}