﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;



namespace Identidad.Designer
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    
                    CargarHome();

                    CargarImagen();
                }

                if (imgHome.ImageUrl == "" || imgHome.ImageUrl == null)
                {

                    imgHome.Visible = false;
                }
                else
                {
                    imgHome.ImageUrl = imgHome.ImageUrl + "?t=" + DateTime.Now.Ticks.ToString();
                    imgHome.Visible = true;
                }

            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarHome()
        {
            txtBienvenida.Text = oBrandSite.CargarBienvenida(Session["IdBrandSite"].ToString());
            //txtBienvenida.Text = "Nombre: " + "\n" + " JuanCa Maney" ;
        } // Carga el texto de bienvenida en la caja de texto bienvenida

        private void CargarImagen()
        {
            string sUrl = string.Empty;

            try
            {
                if (Session["imgHome"] == null)
                {
                    sUrl = oBrandSite.CargarImagenBienvenida(Session["IdBrandSite"].ToString());
                }
                else
                {
                    sUrl = Session["imgHome"].ToString();
                }
                        
            }
            catch (Exception)
            {
                sUrl = "";
            }
            
            imgHome.ImageUrl = sUrl;

        } // Carga la imagen principal en el home

        [System.Web.Services.WebMethod]
        public static string[] ObtenerNotificacion12(int session)
        {
            cSistema oSistema = new cSistema();
            SqlConnection cnn = oSistema.connectDataBase();
            SqlDataAdapter da = new SqlDataAdapter();
            int i = 0;
            string[] mensaje = new string[1];
            mensaje[0] = "buena";

            if (cnn != null)
            {
                SqlCommand cmd2 = new SqlCommand("Exec dbo.GetCountNotificaciones " + session);
                cmd2.Connection = cnn;
                Int32 count =(Int32)cmd2.ExecuteScalar();
                mensaje = new string[count];


                DataTable dt = new DataTable();
                da.SelectCommand = new SqlCommand("Exec dbo.SelectNotificacion " + session, cnn);
                da.Fill(dt);

                //cmd.Connection = cnn;
                //dr = cmd.ExecuteReader();

                for (i = 0; i < dt.Rows.Count; i++ )
                {
                    mensaje[i] = dt.Rows[i].ItemArray[0].ToString();
                }

                cnn.Close();
            }
            return mensaje;
        }
        [System.Web.Services.WebMethod]
        public static void ModificarNotificacion(int session, int id)
        {
            cSistema oSistema = new cSistema();
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("Exec dbo.ModificarNotificaciones "+ id +", "+session);
                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();

                cnn.Close();
            }
        }

        public string GetDiasPrueba(int session)
        {
            cSistema oSistema = new cSistema();
            SqlConnection cnn = oSistema.connectDataBase();
            SqlDataAdapter da = new SqlDataAdapter();
            int fechaFin;
            int dias;
            int horas;
            int[] restante = new int[2];
            int i=0;
            if(cnn != null)
            {
                DataTable dt = new DataTable();
                da.SelectCommand = new SqlCommand("Exec dbo.GetHoras " + session , cnn);
                da.Fill(dt);

                if(da != null)
                {
                    try
                    {
                        fechaFin = Convert.ToInt32(dt.Rows[i].ItemArray[0].ToString());
                        dias = fechaFin / 24;
                        horas = fechaFin - (dias * 24);
                        restante[0] = dias;
                        restante[1] = horas;
                        if(restante[0]>0)
                            return "Te quedan " + restante[0] + " dias de prueba";
                        else if(restante[1]>0)
                            return "Te quedan " + restante[1] + " horas de prueba";
                    }
                    catch
                    {
                        restante[0] = 0;
                        restante[1] = 0;
                        return "";
                    }
                   
                }
            }
            return "";
        }

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void btnGuardarBienvenida_Click(object sender, EventArgs e)
        {
            try
            {
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.GuardarBienvenida(Session["IdBrandSite"].ToString(), txtBienvenida.Text, IdUsuario);

                lblValidacion.Text = oMetodo.Message;
                lblValidacion.Visible = true;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbCambiarLogo_Click(object sender, EventArgs e)
        {
            try
            {
                eExaminarLogo.Show(Session["IdBrandSite"].ToString(), "Te recomendamos adjuntar un logo de 100 x 100 px en formato .jpg", true);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbCambiarImagen_Click(object sender, EventArgs e)
        {
            try
            {
                eExaminar.Show(Session["IdBrandSite"].ToString(), "Te recomendamos seleccionar una imagen .jpg de 1024 pixeles de ancho, y el largo de tu preferencia.", true);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);

                Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetNoStore();

                if (Session["IdBrandSite"] == null || Session["IdBrandSite"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);

                    return;
                }

                // Agrega la referencia a la hoja de estilo dinámica
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/BrandSites/Id" + Session["IdBrandSite"].ToString() + ".css?t=<%= DateTime.Now.Ticks %> />"));

                // Crea eventos

                eExaminar.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eExaminar_OnArchivoSeleccionado);
                eExaminarLogo.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eExaminarLogo_OnArchivoSeleccionado);
                ePantones.ColorSeleccionado += new Identidad.Controles.Pantones.EventHandler(ePantones_OnColorSeleccionado);

                // Pone en null la session para que carge la imagen
                Session["imgHome"] = null;

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Quito la imagen de bienvenida en la página inicial
        protected void lkbBorrarImagen_Click(object sender, EventArgs e)
        {
            try
            {
                // Le pongo un "0" para decirle que no va imagen

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.CambiarImagenHome("0", Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == true)
                {
                    Session["imgHome"] = "";
                    imgHome.ImageUrl = Session["imgHome"].ToString();
                    imgHome.Visible = false;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eExaminar_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.CambiarImagenHome(eExaminar.ObtenerIdArchivoSeleccionado(), Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == true)
                {
                    Session["imgHome"] = eExaminar.ObtenerUrlSeleccionada() + "?t=" + DateTime.Now.Ticks.ToString();
                    imgHome.ImageUrl = Session["imgHome"].ToString();
                    imgHome.Visible = true;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eExaminarLogo_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.CambiarLogo(eExaminarLogo.ObtenerIdArchivoSeleccionado(), Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == true)
                {
                    if (oBrandSite.CrearTemplate(Session["IdBrandSite"].ToString()) == true)
                    {
                        //Master.Parent.FindControl("aHome");
                        //HomeBrandSite.Attributes.Add("style", "background-image: url('" + eExaminar.ObtenerUrlSeleccionada() + "');");

                        Page_Init(null, null);
                        ScriptManager.RegisterStartupScript(Page, this.GetType(), "myScript", "reloadPage();", true);
                    }
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Abre control selector de color
        protected void lkbCambiarFondo_Click(object sender, EventArgs e)
        {
            try
            {
                ePantones.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void ePantones_OnColorSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string sHex = ePantones.ObtenerHexadecimalSeleccionado();
                
                oMetodo = oBrandSite.CambiarColorFondo(sHex, Session["IdBrandSite"].ToString());
                
                if (oMetodo.Pass == true)
                {
                    oBrandSite.CrearTemplate(Session["IdBrandSite"].ToString());
                    
                    Page_Init(null, null);
                    ScriptManager.RegisterStartupScript(Page, this.GetType(), "myScript", "reloadPage();", true);
                    
                }


            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}