﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="~/Designer/Manual.aspx.cs" Inherits="Identidad.Designer.WebForm2" meta:resourcekey="Manual" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ Register Src="~/Controles/Pantones.ascx" TagPrefix="identidad" TagName="Pantones" %>
<%@ Register Src="~/Controles/Examinar.ascx" TagPrefix="identidad" TagName="Examinar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <cfheader name="X-XSS-Protection" value="0">

    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css' /> <%--font-family: 'Titillium Web', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css' /> <%--'Roboto Slab', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css' /> <%--'Roboto Condensed', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Serif' rel='stylesheet' type='text/css' /> <%--'Noto Serif', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css' /> <%--'Noto Sans', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css' /> <%--'Asap', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css' /> <%--'Signika', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Gentium+Basic' rel='stylesheet' type='text/css' /> <%--'Gentium Basic', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Alegreya' rel='stylesheet' type='text/css' /> <%--'Alegreya', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css' /> <%--'Lobster', regular--%>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,300italic,400italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Bitter:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Vollkorn:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'/>    
    <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Cinzel:400,700,900' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amaranth:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sacramento' rel='stylesheet' type='text/css'/>

    <!--Google Fonts-->
    <link href="../CSS/Fonts.css" rel="Stylesheet" type="text/css" />
  
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="divPrincipal">
         <asp:UpdatePanel ID="upTreeView" runat="server">
             <Triggers>
                  <asp:PostBackTrigger ControlID="btnAgregarItem"/>
                 <asp:AsyncPostBackTrigger ControlID="btnEliminarSeccion" />

             </Triggers>
            
            <ContentTemplate>
                
               <asp:Label ID="lblIdUsuario" runat="server" Text="Label" Visible="false" ></asp:Label>
               <asp:Label ID="lblIdBrandiste" runat="server" Text="Label" visible="false" ></asp:Label>
               <asp:HiddenField ID="hdHTMl" runat="server" EnableViewState="true" />
               <asp:TextBox ID="txtHtml" runat="server" style="display:none"></asp:TextBox>
               <asp:TextBox ID="txtIdIndiceMenu" runat="server" style="display:none"></asp:TextBox>
               <asp:TextBox ID="txtNombreMenu" runat="server" style="display:none"></asp:TextBox>
               <asp:TextBox ID="txtPoculto" runat="server" value="1" style="display:none"></asp:TextBox>
               <asp:Button ID="btnDefault" runat="server" Text="Button" style="display:none" />
               <div id="divOcultatree" runat="server" style="width:50px; margin-left:23px!important;">Ocultar</div>
               <br/>

               <div id="divTreeView"  runat="server">
                    
                    <div id="divAgregarSeccion" class="divAgregarSeccion">
                        <asp:Button ID="btnAgregarSeccion" runat="server" meta:resourcekey="btnAgregarSeccion" OnClick="btnAgregarSeccion_Click" CssClass="buttons colorVerde alinear" ToolTip="Podrás cambiar el orden de cada sección arrastrando las flechas que aparecen a la izquierda de cada título." />
                    </div>
                    <div style="display:none">
                      <input type="text" id="txtListaJSON" runat="server"  />  
                        </div>
                    
                     <div id="divTree" runat="server" class="">
                       
                         <%--<asp:TreeView ID="tvwIndice" runat="server" OnSelectedNodeChanged="tvwIndice_SelectedNodeChanged1" ForeColor="#717465"  CollapseImageUrl="~/Images/Manual/imenos.gif" ExpandImageUrl="~/Images/Manual/Imas.gif" NodeWrap="True">
                              <NodeStyle HorizontalPadding="5px" />
                              <ParentNodeStyle  CssClass="TreeView" Font-Bold="False" ForeColor="#666666" HorizontalPadding="3px" NodeSpacing="2px" />
                              <SelectedNodeStyle Font-Bold="True" ForeColor="Black" />
                         </asp:TreeView>--%>
                    </div>
              </div> 
          </ContentTemplate>
            </asp:UpdatePanel>   
        
         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
          
            <ContentTemplate>
              <div runat="server"  id="divFondo" class="divFondo">


                    <asp:Label ID="lblIdIndiceSeleccionado" runat="server" style="display:none" ></asp:Label>
                    <asp:Label ID="lblIdTemplate" runat="server"></asp:Label>
                    <Label ID="lblCambios" style="color:#FF008C; margin-left:22px">Los cambios se guardan automáticamente</Label>
                    <div id="divOpciones" class="divOpciones">
                         <asp:Panel ID="pnlMenuOpciones" runat="server" >
                              <%--<asp:Label ID="Label4" runat="server">Los cambios se guardan automáticamente</asp:Label>--%>
                              <asp:Button ID="btnEliminarSeccion" runat="server" meta:resourcekey="btnEliminarSeccion" OnClick="btnEliminarSeccion_Click" CssClass="buttons colorwhite alinear" />
                              <asp:Button ID="btnEditarTemplate" runat="server"  meta:resourcekey="btnEditarTemplate" OnClick="btnEditarTemplate_Click1" CssClass="buttons colorVerde alinear" ToolTip="Haz clic para elegir la ubicación de imágenes y/o texto de esta sección." />
                              
                               <%--   <asp:Button ID="btnEditar" runat="server" meta:resourcekey="btnEditar" OnClick="btnEditar_Click" CssClass="buttons colorAzulO alinear"/>
                            
                               --%>            

                         </asp:Panel>
                   </div>

                  <div id="divContenido" <%--style="background:#8a8585"--%>>
                      <div id="divEdicion" class="divEdicion">
                           <div id="divTitulos" class="Id1" style="margin-left:10px;margin-bottom:10px;">
                                <asp:Label ID="lblNombreMenu" runat="server" ></asp:Label> 
                            </div>

                          <%-- Panel de edicion del richTexbox 
                          <div id="divEditRich" class="divOpciones colorwhite">

                                <asp:Panel ID="pnlPublicar" runat="server" >
                                           <%-- <asp:Button ID="btnEditarHTML2" runat="server" CssClass="buttons colorGris alinear" meta:resourcekey="btnEditarHTML2" OnClick="btnEditarHTML2_Click" />                       
                                   <asp:Button ID="btnPublicar" runat="server" meta:resourcekey="btnPublicar"  CssClass="buttons colorGris alinear" OnClick="btnPublicar_Click" OnClientClick="return EnviaDatos();"/>
                                             
                                   
                                </asp:Panel>
                         </div>--%>
                    </div>

                    <div runat="server" id="divOpcionesEditor" class="divOpcionesEditor">
                        <div style="display:none"> 
                          <asp:Button  ID ="btnEnviaDatos" runat="server" OnClick="lnkCambiarFondo_Click"  Text=" " style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button background_image" ToolTip="Cambiar color de fondo" />
                          <asp:Button  ID="btnImage1"      runat="server" Text=" " OnClick="btnImage1_Click"  style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button cimagen_image" ToolTip="Cambiar 1era imagen"   />  
                          <asp:Button  ID="btnImage2"      runat="server" Text=" " OnClick="btnImage1_Click"  style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button cimagen_image" ToolTip="Cambiar 2nda imagen "  />  
              
                       </div>                       <%--<div runat="server" id="divCambioImagen" class="divCambioImagen" style="display:none">  
                                     </div>   --%>
                    </div>      
          
              <div id="divEditorTexto" class="divEditorTexto">


                <%-- Dialog para agregar tooltip --%>

                    <div id="divRichTexbox" class="divRichTexbox" style="display:none">
                       <br/>               
                    </div>
                    
                       <identidad:Examinar runat="server" id="eSeleccionaImagen" />

                 <div id="divHTML" class="divHTML">
                      <div  id="divPlantilla"  runat="server" style="margin-left:-25px">
                      </div> 
                </div>
            </div> <!--Fin div Editor -->
                      
                      <%-- Botones para los archivos relacionados --%>
                          <div style="margin-left:30px;">
                              <asp:Button ID="btnArchivosRelacionadosImg1" runat="server" Text="Selecciona archivos relacionados a la imagen" OnClick="btnArchivosRelacionadosImg1_Click" CssClass="buttons colorVerde" ToolTip="Los archivos que elijas podrán ser descargados (al estar guardados en la sección de Descargas) al hacer clic en la imagen." />
                              <asp:Button ID="btnArchivosRelacionadosImg2" runat="server" Text="Selecciona los archivos relacionados a la segunda imagen" OnClick="btnArchivosRelacionadosImg2_Click" CssClass="buttons colorVerde" ToolTip="Los archivos que elijas podrán ser descargados (al estar guardados en la sección de Descargas) al hacer clic en la imagen." />
                          </div>
                      <%-- Fin de botones para los archivos relacionados --%>

            <br /><br />
            <asp:Label ID="lblHTML" runat="server"></asp:Label>

              </span></span>
                <div style="clear:both"></div>
                </div>

             

            </div>
               
                </ContentTemplate>
                </asp:UpdatePanel>
                

        <%-- POPUPS --%>

        <asp:Panel ID="pnlControlIndice" runat="server" style="display:none">
            
            <div class="divTabla">
                <div>
                        <div class="divTablaTitulo">
                               <asp:Label ID="lblTituloEditar" runat="server" meta:resourcekey="lblTituloEditar"></asp:Label> 
                         </div>
                        <div class="divTablaTituloImagen">
                                 <img id="img11" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarSeccion.ClientID %>').click();" />
                        </div>
                </div>
              
                   <hr/>
                   
               <br/>
               <div class="divTBODY">
                       <div class="divTR">
                            <div class="divTDIzq" > 
                                <asp:Label ID="llblNombreIndice" runat="server" meta:resourcekey="lblitemNombre"></asp:Label> 
                            </div>
                            <div class="divTDDerechoInput"> 
                               <asp:TextBox ID="txtNombreIndice" runat="server" CssClass="TextBox"></asp:TextBox>
                            </div>
                       </div>

                       <div class="divTR">
                            <div class="divTDIzq"> <asp:Label ID="lblIndicePadre" runat="server" meta:resourcekey="lblItemPadre"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput"> 
                                <label>
                                <asp:DropDownList ID="ddlPadre" runat="server" CssClass="DropDownList"></asp:DropDownList>
                                </label>
                            </div>
                       </div>

                      <div class="divTR">
                            <div class="divTDIzq"> 
                        
                            </div>
                            <div class="divTDDerechoInput"> <asp:Label ID="lblValidacionActualizacion" runat="server" Visible="false"></asp:Label>
                            </div>
                       </div>

                   <div class="divTR">
                            <div class="divTDIzq"> 
                            </div>
                            <div id="divTDDerechoButtons" > 
               
                                 <asp:Button ID="btnCancelarSeccion" runat="server" meta:resourcekey="btnCancelarItem" class="buttons colorwhite" />
               &nbsp;
             
                                <asp:Button ID="btnGuardarSeccion" runat="server" meta:resourcekey="btnAgregarItem" OnClick="btnGuardarSeccion_Click" class="buttons colorVerde"/>
                               </div>
                       </div>

                   <div class="divTR">
                        <div class="divTDIzq">
                        </div>
                        <div class="divTDDerechoButtons">
                         </div>
                    </div>
                      </div>
                </div>
              
            </asp:Panel>
             <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
             <Ajax:ModalPopupExtender ID="pnlControlIndicePop" TargetControlID="Label3" runat="server" CancelControlID="btnCancelarSeccion" PopupControlID="pnlControlIndice" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".30" Fps="48" />                
                    </OnShown>
                </Animations>
             </Ajax:ModalPopupExtender>
       

        <!--Inicia Popup para agregar un item nuevo al manual--->

        <%--onclick="$get('imgbtnC').click()"--%>
        <panel id="pnlAgregarNuevoItem" runat="server"  style="display:none">
          <div class="divTabla">
                <div>
                        <div class="divTablaTitulo">
                               <asp:Label ID="lblTitulo" runat="server" meta:resourcekey="lblTitulo"></asp:Label> 
                         </div>
                        <div class="divTablaTituloImagen">
                                 <img id="imgbtnC" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarItem.ClientID %>').click();" />
                        </div>
                </div>
              
                   <hr/>
                   
               <br/>
               <div class="divTBODY">
                       <div id="divTR">
                            <div class="divTDIzq"> 
                                <asp:Label ID="lblitemNombre" runat="server" style="color:#787878!important;" meta:resourcekey="lblitemNombre"></asp:Label> 
                            </div>
                            <div class="divTDDerechoInput"> 
                               <asp:TextBox ID="txtNombreItemIndice" runat="server" CssClass="TextBox"></asp:TextBox>
                            </div>
                       </div>

                       <div class="divTR">
                            <div class="divTDIzq"> <asp:Label ID="lblItemPadre" runat="server" meta:resourcekey="lblItemPadre"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput"> 
                                <label>
                                <asp:DropDownList ID="ddlPadres" runat="server" CssClass="DropDownList"></asp:DropDownList>
                                </label>
                            </div>
                       </div>

                      <div class="divTR">
                            <div class="divTDIzq"><asp:RadioButtonList ID="rdblPlantillas" runat="server">
                             </asp:RadioButtonList>

                
                            </div>
                            <div class="divTDDerechoInput"> <asp:Label ID="lblValidarNuevoItem" runat="server" Visible="false"></asp:Label>
                            </div>
                       </div>

                   <div class="divTR">
                            <div class="divTDIzq"> 
                            </div>
                            <div class="divTDDerechoButtons" style="margin-top:-25px;"> 
                  
                             <asp:Button ID="btnCancelarItem" runat="server" meta:resourcekey="btnCancelarItem" OnClick="btnCancelarItem_Click" class="buttons colorwhite" />
                              &nbsp;
                                <asp:Button ID="btnAgregarItem" runat="server" meta:resourcekey="btnAgregarItem" OnClick="btnAgregarItem_Click" class="buttons colorVerde"/>
           
                                 </div>
                       </div>

                   <div class="divTR">
                        <div class="divTDIzq">
                        </div>
                        <div class="divTDDerechoButtons">
                         </div>
                    </div>

                </div>
</div>         
        </panel>

        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppAgregarItemManual" TargetControlID="Label1" runat="server" PopupControlID="pnlAgregarNuevoItem" BackgroundCssClass="modalBackground" Animations="" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup para agregar un item nuevo al manual--->

        <!--Inicia Popup para confirmar eliminar una seccion--->
        <panel id="pnlEliminarSeccion" runat="server" style="display:none" >
            <div class="divTabla divConfirmarEliminar">
                 <div class="divTablaTitulo">
                      <asp:Label ID="lblEliminarTitulo" runat="server" meta:resourcekey="lblEliminarTitulo"></asp:Label>        
                 </div>
                 <div class="divTablaTituloImagen">
                                 <img id="img2" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarEliminar.ClientID %>').click();" />
                 </div>
                 <hr/><br/>

               <div class="divTBODY divTamano">
                    <div class="divTR contenidoCentrar">
                        <asp:Label ID="lblEliminarConfirmacion" runat="server" meta:resourcekey="lblEliminarConfirmacion"></asp:Label>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons">
                                           <asp:Button ID="btnCancelarEliminar" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" CssClass="buttons colorwhite"/>
                                           <asp:Button ID="btnEliminar" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" CssClass="buttons colorRojo" />                       
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons"></div>
                    </div>
                </div>
            </div>
        </panel>

        <asp:Label ID="label2" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppEliminar" TargetControlID="Label2" runat="server" PopupControlID="pnlEliminarSeccion" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup para para confirmar eliminar una seccion--->

        <!---INICIO PopoUp para seleccionar una plantilla ---->
        <asp:Panel ID="pnlTemplate" runat="server" style="display:none">
            
                <div  class="divTabla divSeleccionPlantilla Popup"> 

                    <div class="divTablaTitulo">
                      <asp:Label ID="lblPlantilla"  meta:resourcekey="lblPlantilla" runat="server"></asp:Label>        
                 </div>

                      <div class="divTablaTituloImagen">
                                 <img id="img3" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelarPlantilla.ClientID %>').click();" />
                 </div>
               
                 <hr/><br/>

                    <br/>
                   
                     <div class="divPlantillaContenido">
                        <asp:RadioButtonList ID="rdblPlantilla" runat="server" RepeatColumns="5" RepeatDirection="Horizontal" CellPadding="5" CellSpacing="10"></asp:RadioButtonList> 
                     </div>
                    <br/>


                    <div class="divTR">
                        
                            <%--Comentario de Advertencia en cambio de plantilla--%>
                              <div class="divAdvertencia">
                              Aviso: Los registros de imagen y texto permanecerán guardados al cambiar de plantilla.
                             </div>
                    </div>


                    <br/>

                     <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons">
                                  
                         <asp:Button ID="btnCancelarPlantilla" runat="server" meta:resourcekey="btnCancelarItem" CssClass="buttons colorwhite" />
                         <asp:Button ID="btnGuardarPlantilla" runat="server" meta:resourcekey="btnGuardarSeccion" OnClick="btnGuardarPlantilla_Click" CssClass="buttons colorVerde" />
               
                         </div>
                         </div>
                     <br/>
                 </div>

          </asp:Panel>

                 <asp:Label ID="lblpp" runat="server" Text=""></asp:Label>
                
         <Ajax:ModalPopupExtender ID="pPlantillas" TargetControlID="lblpp" runat="server" PopupControlID="pnlTemplate" BackgroundCssClass="modalBackground" CancelControlID="btnCancelarPlantilla" RepositionMode="RepositionOnWindowResizeAndScroll">
          <%--  <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>--%>
         </Ajax:ModalPopupExtender>

        <!--Inicia Popup para ver los archivos relacionados--->
        <panel id="pnlArchivosRelacionados" runat="server" style="display:none;" >
            <div class="divTabla"  style="width:660px;">
                 <div class="divTablaTitulo">
                      <asp:Label ID="lblTituloArchivosrelacionados" runat="server" meta:resourcekey="lblTituloArchivosrelacionados"></asp:Label>        
                 </div>
                 <hr/>
               <div class="divTBODY divTamano" >
                    <div style="overflow-y:auto; height:360px; margin-bottom:30px;">
                        <asp:DataList ID="dlArchivosRelacionados" RepeatColumns="4" RepeatDirection="Horizontal" runat="server">
                        <ItemTemplate>
                        
                            <div id='DivArchivo' class="divMosaico" >
                                <table class="tablaMosaico">
                                    <tr>
                                        <td >
                                            <asp:Label ID="lblIdArchivoRelacionado" runat="server" DataField ="IdArchivoRelacionado" Text='<%#Eval ("IdArchivoRelacionado") %>' Visible="false" ></asp:Label>
                                            <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                            <asp:ImageButton ID="imgPreview" style="background-color:white!important;" class="imagenMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         
                                            <asp:Label ID="lblNombre" CssClass="TituloArchivo" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                            <asp:Label ID="lblExtension" CssClass="infoArchivo" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblPeso" CssClass="infoArchivo" runat="server" DataField ="PesoBytesArchivo" Text='<%#Eval ("PesoBytesArchivo") %>' ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url")%>' Visible="false" />
                                            <asp:Label ID="lblBytes" runat="server" Text='<%#Eval("Bytes")%>' Visible="false" />
                                            <asp:LinkButton ID="lkbEliminar" runat="server" Text="Eliminar" Visible="true" OnClick="lkbEliminar_Click" ></asp:LinkButton>
                                            <br /><br />
                                        </td>

                                    </tr>
                                </table>
                            </div>

                        </ItemTemplate>
                    </asp:DataList>
                    </div>
                    
                    <asp:Button ID="btnSalirArchivosRelacionados" runat="server" meta:resourcekey="btnSalirArchivosRelacionados" CssClass="buttons colorwhite" OnClick="btnSalirArchivosRelacionados_Click" />
                    <asp:Button ID="btnNuevoArchivo" runat="server" meta:resourcekey="btnNuevoArchivo" OnClick="btnNuevoArchivo_Click" CssClass="buttons colorVerde" />

               </div>

            </div>
        </panel>

        <asp:Label ID="label6" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppArchivosRelacionados" TargetControlID="label6" runat="server" PopupControlID="pnlArchivosRelacionados" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
           <%-- <Animations>
                <OnShown>
                    <FadeIn Duration=".10" Fps="48" />                
                </OnShown>
            </Animations>--%>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup para ver los archivos relacionados--->

        <identidad:Examinar runat="server" id="eExaminarArchivosRelacionados" />
        <asp:Label ID="lblNoImagenSeleccionada" runat="server" style="display:none;" Text="0"></asp:Label>

        <!--FIN PopoUp para seleccionar una plantilla-->
     
        <link type="text/css" href="../CSS/Pages/Manual.css" rel="stylesheet" />
        <link type="text/css" href="../CSS/Pages/Manual/MenuTree.css" rel="stylesheet" />

     <!--Referencias al JS-->
        
       <%--  <link href="../JS/themes/tooltip.css" rel="stylesheet" />
         <script src="../JS/themes/tooltip.js"></script>--%>
         <script type="text/javascript" src="../JS/jquery-1.8.3.js"></script> 
         <script type="text/javascript" src="../JS/EditorTexto/ckeditor.js"></script>
        <%-- <script src="../JS/ckeditor2.js"></script>--%>
         <script type="text/javascript" src="../JS/jsManual.js"></script>
         <link type="text/css" href="../CSS/Pages/ManualToolTip.css" rel="stylesheet" />
         <script type="text/javascript" src="../JS/Manual/JqueryNestable.js"></script>
         <script type="text/javascript" src="../JS/Manual/HtmlEditor.js"></script>
        <script type="text/javascript" src="../JS/Manual/MenuInicia.js"></script>
        


        
      <link href="/CSS/rangeslider.css" rel="stylesheet" />
        
    <%--<script src="/JS/Manual/jquery-1.8.3.min.js" type="text/javascript"></script>--%>
    <%--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--%>
    <script src="/JS/Manual/rangeslider.js" type="text/javascript"></script>    
    <script src="/JS/Manual/Slider.js"  type="text/javascript"></script>

        <identidad:Pantones runat="server" id="cPantones" />
    </div>

    
           
    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblManual").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }


 //       $("document").ready(function () {
 //$('.divTemplateTexto').removeAttr("title");
 //           $('.divTemplateTitulo').removeAttr("title");
           
 //       });

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {

                        document.getElementById("lblManual").setAttribute("class", "Seleccionado");

                        $("document").ready(function () {


                            $('#ContentPlaceHolder1_divOcultatree').click(function (e) {

                                if ($('#ContentPlaceHolder1_divTreeView').css("display") == "none") {
                                    // handle non visible state
                                    $('#ContentPlaceHolder1_divTreeView').show("slow");
                                    $('#ContentPlaceHolder1_divOcultatree').html("Ocultar");
                                    $('.divFondo').removeClass(' importantRule');
                                    $('#ContentPlaceHolder1_txtPoculto').val("1");
                                   

                                } else {
                                    $('#ContentPlaceHolder1_divTreeView').hide("slow");
                                    $('#ContentPlaceHolder1_divOcultatree').html("Mostrar");
                                    $('.divFondo').addClass(' importantRule');
                                    $('#ContentPlaceHolder1_txtPoculto').val("0");

                                   
                                    // handle visible state
                                }

                            });

                   

                            //cosa

                            var updateOutput = function (e) {

                                try {
                                    var list = e.length ? e : $(e.target),
                                        output = list.data('output');
                                    if (window.JSON) {

                                        $('#txtListaJSON').val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                                    } else {

                                        $('#txtListaJSON').val('null');
                                    }

                                } catch (exam) { }
                            };
                            $('#nestable3').nestable().on('change', updateOutput);


                        


                        });

                    }
                }
            );
        };

    </script>
      
    <%-- Valida para ver en que casos muestra o no los botones para seleccionar los archivos relacionados --%>      
    <script type="text/javascript">

        function ValidarImgParaArchivosRelacionados()
        {
            try {


                if ($('#Img1').length)
                {
                    document.getElementById('ContentPlaceHolder1_btnArchivosRelacionadosImg1').style.display = 'inline';
                }
                else
                {
                    document.getElementById('ContentPlaceHolder1_btnArchivosRelacionadosImg1').style.display = 'none';
                }

                if ($('#Img2').length)
                {
                    document.getElementById('ContentPlaceHolder1_btnArchivosRelacionadosImg2').style.display = 'inline';
                }
                else
                {
                    document.getElementById('ContentPlaceHolder1_btnArchivosRelacionadosImg2').style.display = 'none';
                }
                setTimeout(function () {
                    slid();
                    slid();
                }, 500);
            } catch (e)
            {
                alert(e);
            }
        }

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    ValidarImgParaArchivosRelacionados();
                }

            });
        }

        $(document).ready(function () {
            ValidarImgParaArchivosRelacionados();

        });


        function pageLoad(sender, args) {
            //espera .4 segundos despues de que empezo a cargar la pagina
            //setTimeout(function () { slid(); }, 400);
            
            var cont = 0;

            console.log("antes");
            //Como habia veces que tardaba mas la carga, se puso un timer que cada .1 segundo entre a tratar de poner el slider, si entra en la funcion se quita
             /*Intervalo = setInterval(
                function () {
                    slid();
                    cont = cont + 1;
                    console.log(cont);
                    if (cont >= 5) {
                        //si de plano no encontro la funcion que no pase de mas de 500 intentos (50 seg)
                        clearInterval(Intervalo);
                        return false;
                    }
                }, 1000);*/
             setTimeout(function () {
                 slid();
                 slid();
             }, 500);

             $('.download').error(function () {
                 //alert('Image does not exist !!');
             });

        }

        $(document).ready(function () {
            ValidarImgParaArchivosRelacionados();
            var color = $('#divContenido').css("color");
            var back = $('#divContenido').css("background");
            if (color == "rgb(254, 254, 254)") {
                document.getElementById('divContenido').style.color = "#000";
            }

        });

        $('.download').error(function () {
            //alert('Image does not exist !!');
        });

        function imgError(image) {
            $(image).attr("src", "/../Images/Mark.jpg");
            //$(this).parent().remove("divTemplateImagen margen");
            //$("[class=download]").attr("src", "");
            $("[class=download]").parent().remove("divTemplateImagen");
        };

    </script>

</asp:Content>