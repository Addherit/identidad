﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace Identidad.Designer
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cManual oManual = new cManual();
        cMetodo oMetodo = new cMetodo();
        cEditorManual oEditorManual = new cEditorManual();


        //METODOS


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Form.DefaultButton = btnDefault.UniqueID;


                if (!IsPostBack)
                {
                    CargaSesiones();

                    if (lblIdUsuario.Text == "" || lblIdBrandiste.Text == "")
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);
                        return;
                    }

            
                    ObtenerIndices(); //carga el treee
                    revisaPrimerElemento(); //Selecciona el primer elemento
                  



                }
                if (IsPostBack) {

                    CargaElementos();
                
                }
           

            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        #region Métodos

        public void revisaPrimerElemento() {

         int idPrimerElento = oEditorManual.ObtenPrimerElento(Session["idBrandSite"].ToString());

          if (idPrimerElento != 0)
                    {
                      
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "CargaElementos('" + idPrimerElento.ToString() + "');", true);
                       
                    }
                    else {

                         ocultaBotones(true);
                     }
        }

        public void CargaElementos() {


            string cadena = txtHtml.Text;
            lblIdIndiceSeleccionado.Text = txtIdIndiceMenu.Text;            
            lblNombreMenu.Text = txtNombreMenu.Text;

            if (lblNombreMenu.Text != "")
            {
                btnEliminarSeccion.Style.Add("display", "inline");
                btnEditarTemplate.Style.Add("display", "inline");
            }
            else {

                btnEliminarSeccion.Style.Add("display", "none");
                btnEditarTemplate.Style.Add("display", "none");
            }


            if (txtPoculto.Text == "1")
            {
 
                divFondo.Attributes["class"] = "divFondo";
                divTreeView.Attributes.Add("style", "");
                divOcultatree.InnerText = "Ocultar";
             
            }
            else if(txtPoculto.Text=="0"){


                divTreeView.Attributes.Add("style", "display:none");
                divFondo.Attributes["class"] = "divFondo importantRule";
                divOcultatree.InnerText = "Mostrar";

              
             
            }



            ObtenerIndices();

            divPlantilla.InnerHtml = cadena;

         
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "AgregaEventosPagina();", true);
          
        
        }

        public void ocultaBotones(bool tf) {
            if (tf)
            {
                lblNombreMenu.Text = "Agrega secciónes a tu manual";
                btnEliminarSeccion.Style.Add("display","none");
                btnEditarTemplate.Style.Add("display", "none"); 
            }
          
        }

        public void abreImagenes()
        {

            try
            {
                CargaElementos();
                eSeleccionaImagen.Show(lblIdBrandiste.Text); //Dispara evento para abrir el control de imagenes
            }
            catch (Exception)
            {

            }
        }

        public void CargaSesiones() //Carga las sessiones a labls
        {
            string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

            lblIdUsuario.Text = IdUsuario == null ? "" : IdUsuario;
            lblIdBrandiste.Text = Session["IdBrandSite"].ToString() == null ? "" : Session["IdBrandSite"].ToString();

        }
   
        public DataTable obtenerInformacionIndice() {

            DataTable dtInformacion;
            dtInformacion=oManual.obtenerInformacion(lblIdIndiceSeleccionado.Text);

            return dtInformacion;
        }

        public void obtenerHTML() { //obtiene el html de la plantilla.
            

            DataTable dtDatos = obtenerInformacionIndice();
           
            if(dtDatos!=null){
                try
                {
                    divPlantilla.InnerHtml = dtDatos.Rows[0][2].ToString();
                    txtHtml.Text = dtDatos.Rows[0][2].ToString();
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "AgregaEventosPagina();", true);

                    //Aqui vamos a buscar el elemento divImag1 o divImag2 o las que sean, y se les va agregar un bottoncin para que den click y se traiga la imagen
                }catch{}
              
            }
        
        }

    
        public void obtenerPlantilla() { //obtiene la plantilla seleccionada y la pone default
           
            DataTable dtDatos = obtenerInformacionIndice();

            
            if(dtDatos!=null){
                
                for (int i = 0; i < rdblPlantilla.Items.Count; i++) {

                    if (dtDatos.Rows[0][3].ToString() == rdblPlantilla.Items[i].Value) 
                        rdblPlantilla.Items[i].Selected=true;
            
                }

            }
          
            
            //rdblPlantilla.Items.Count
            // = dtDatos.Rows[0][4].ToString();
        
        }

        public void cargaElementosNodo(string idIndice)
        {

            foreach (DataRow row in oManual.CargarItemIndice(idIndice).Rows)
            {
                txtNombreIndice.Text = row["Nombre"].ToString();
                ddlPadre.SelectedValue = row["IdIndicePadre"].ToString();
                rdblPlantilla.SelectedValue = row["IdPlantilla"].ToString();

       
                //MuestraOcultaBotones(row["HTMLPlantilla"].ToString());
                //lblHTML.Text = row["HTMLPlantilla"].ToString();
            }

            pnlMenuOpciones.Visible = true;

        }


        #endregion

        #region Eventos

        protected void ObtenerIndices()
        {

            string sIndice = oEditorManual.creaNodos(Session["IdBrandSite"].ToString());
            divTree.InnerHtml = sIndice;




        }

        protected void btnAgregarSeccion_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombreItemIndice.Text = "";
                lblValidarNuevoItem.Visible = false;
                //rdblPlantillas.Items.Clear();

                oBrandSite.CargarIndices(ddlPadres, lblIdBrandiste.Text);

                //foreach (DataRow row in oBrandSite.CargarPlantillas().Rows)
                //{
                //    ListItem item = new ListItem();

                //    item.Value = row["IdPlantilla"].ToString();
                //    item.Text = String.Format("<img class='imagePlantilla' src=\"{0}\"/>", row["Preview"].ToString());

                //    rdblPlantillas.Items.Add(item);

                //}

                //if (rdblPlantillas.Items.Count != 0)
                //{
                //    rdblPlantillas.Items[0].Selected = true;
                //}
                //rdblPlantillas.Visible = false;


                ppAgregarItemManual.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
        //btnAgregarItem_Click
        protected void btnAgregarItem_Click(object sender, EventArgs e) //Agregar un nuevo item al treeview
        {
            try
            {

                //Guarda el item.
                cEditorManual oGuardarItems = new cEditorManual();

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oGuardarItems.GuardarItems(txtNombreItemIndice.Text, ddlPadres.SelectedValue, IdUsuario, Session["IdBrandSite"].ToString());
                ObtenerIndices();
                LlenaPadres();
                
                
                
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelarItem_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombreItemIndice.Text = "";
                lblValidarNuevoItem.Visible = false;
                ppAgregarItemManual.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
      
        protected void btnEliminarSeccion_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminar.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        //private int FindNode(string cadena) //Busca un nodo en el treeview del  manual
      
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            string IdNodo = lblIdIndiceSeleccionado.Text;

            string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

            oBrandSite.EliminarSeccionIndice(lblIdIndiceSeleccionado.Text, IdUsuario);
            ObtenerIndices();
            LlenaPadres();
            HttpContext.Current.ApplicationInstance.CompleteRequest();
            Response.Redirect("Manual.aspx", false);
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminar.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

     
        protected void LlenaPadres()
        {
            ddlPadres.Items.Clear();
            ddlPadres.DataSource = oEditorManual.ObtenerPadresBD(Session["IdBrandSite"].ToString());
            ddlPadres.DataValueField = "IdIndice";
            ddlPadres.DataTextField = "Nombre";
            ddlPadres.DataBind();



        }

        protected void btnGuardarSeccion_Click(object sender, EventArgs e)
        {
            try
            {
                cEditorManual oGuardarItems = new cEditorManual();
            
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oGuardarItems.GuardarItems(txtNombreItemIndice.Text, ddlPadres.SelectedValue, IdUsuario, Session["IdBrandSite"].ToString());
                ObtenerIndices();
                LlenaPadres();
           
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }     
      
        protected void btnEditarTemplate_Click1(object sender, EventArgs e)
        {
            try
            {
                rdblPlantilla.Items.Clear();
                int idPlantilla = oEditorManual.ObtenerIdPlantilla(Session["IdBrandSite"].ToString(), txtNombreMenu.Text);

                foreach (DataRow row in oBrandSite.CargarPlantillas().Rows)
                {
                    ListItem item = new ListItem();
                    item.Value = row["IdPlantilla"].ToString();
                    item.Text = String.Format("<img class='imagePlantilla' src=\"{0}\"/>", row["Preview"].ToString());
                    rdblPlantilla.Items.Add(item);
                }

                if (rdblPlantilla.Items.Count != 0 )
                {
                    if(idPlantilla > 0)
                        rdblPlantilla.Items[idPlantilla - 1].Selected = true;
                    else
                        rdblPlantilla.Items[0].Selected = true;
                }

                obtenerPlantilla();


                pPlantillas.Show();
            }
            catch(Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
              

        protected void btnEditar_Click(object sender, EventArgs e)
        {
               pnlControlIndicePop.Show();
          
        }

        protected void btnGuardarPlantilla_Click(object sender, EventArgs e)
        {

            bool resultado = oManual.actualizarPlantilla(lblIdIndiceSeleccionado.Text, rdblPlantilla.SelectedItem.Value);
            obtenerHTML();

        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);

                if (Session["IdBrandSite"] == null )
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);

                    return;
                }

                // Agrega la referencia a la hoja de estilo dinámica
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/BrandSites/Id" + Session["IdBrandSite"].ToString() + ".css\" />"));

                // Crea eventos
                eSeleccionaImagen.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eSeleccionaImagen_OnArchivoSeleccionado);
                cPantones.ColorSeleccionado += new Identidad.Controles.Pantones.EventHandler(Pantones_OnColorSeleccionado);
                eExaminarArchivosRelacionados.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eExaminarArchivosRelacionados_OnArchivoSeleccionado);
              
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eSeleccionaImagen_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string url = eSeleccionaImagen.ObtenerUrlSeleccionada();

                //llama la funcion javascript de obteneimage
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "GuardarImage('" + url + "','" + Session["sIDimage"].ToString() + "');", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptImage", "GuardarImage('" + url + "','" + Session["sIDimage"].ToString() + "');", true);
    
                Session["sIDimage"] = "";

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        private void eExaminarArchivosRelacionados_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                string sIdArchivo = eExaminarArchivosRelacionados.ObtenerIdArchivoSeleccionado();
                
                oBrandSite.AgregarArchivoRelacionado(lblIdIndiceSeleccionado.Text, lblNoImagenSeleccionada.Text, sIdArchivo);
                
                oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(lblIdIndiceSeleccionado.Text), int.Parse(lblNoImagenSeleccionada.Text));

                ppArchivosRelacionados.Show();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }


        // Código que entra cuando se dispara el evento en Examinar
        private void Pantones_OnColorSeleccionado(object sender, EventArgs e)
        {
            try
            {

                string sHex = "#"+cPantones.ObtenerHexadecimalSeleccionado();
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "scriptColor", "CambiarFondoBG('"+sHex+"')", true);
      
               // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "CambiarFondoBG('" + sHex + "')", true);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lnkCambiarFondo_Click(object sender, EventArgs e)
        {

            cPantones.Show();

                               
        }

        protected void btnImage1_Click(object sender, EventArgs e) // dependiendo el tipo de imagen (1 o 2) abre el control para seleccionar una imagen.
        {
            Button btn = (Button)sender;
            string sbutton = btn.ID.ToString();

            if (sbutton == "btnImage1")
            {
                Session["sIDimage"] = "Img1";
            
            }
            else if (sbutton == "btnImage2") {
                Session["sIDimage"] = "Img2";
            }
             
              eSeleccionaImagen.Show(lblIdBrandiste.Text, "Te recomendamos subir una imagen formato .jpg cuando que no sea muy chica para evitar distorsiones.", true);

              Response.AppendHeader("X-XSS-Protection", "0");
        }
      
 #endregion

        #region Metodos llamados desde JavaScript

        [WebMethod]
        public static void EnviaDatos(string JSON) //Metodo que se manda llamar desde JS (MenuIncia) por cada cambio en el menu, se guarda en DB
        {

            JavaScriptSerializer js = new JavaScriptSerializer();
            Menu[] mDatos = js.Deserialize<Menu[]>(JSON); //Deserializamos el obajeto JSON en la clase Menu para poder manipular los dtos

            WebForm2 oEnvia = new WebForm2();
            oEnvia.EnviaDatos(mDatos);

        }


        [WebMethod]
        public static void GuardaNombreIndice(string sNombreIndice, string sIndice)  //Metodo que se llama desde JS para guardar el nombre del elemento
        {


            cEditorManual oEnvia = new cEditorManual();
            oEnvia.ModificaNombreNodo(sNombreIndice, sIndice);


        }

        [WebMethod]
        public static string[] CargaElementos(string sIndice)
        { //metodo que manda llamar el metodo que carga el contenido del indice seleccionado.

            WebForm2 oEnvia = new WebForm2();

            return oEnvia.RegresaDatos(sIndice);

        }
        [WebMethod]

        public static void GuardaHTMLPlantilla(string sHTML,string sTitulo,string sTexto,string sTexto2,string sPie,string sPie2,string sImg,string sImg2,string idIndice) {


            cEditorManual oEnvia = new cEditorManual();
            //string simagen = "<img src='" + sImg + "' style='width:100%;height:auto;max-width:100%;'></img>";
            oEnvia.ActualizaContenido(sHTML, sTitulo, sTexto,sTexto2,sPie,sPie2, sImg, sImg2, idIndice);

        }

        #endregion

        //Auxiliar para mandar la variable de session
        private void EnviaDatos(Menu[] mDatos)
        {

            cEditorManual oEnvia = new cEditorManual(); //Se crea una instancia de la clase Editor manual para enviar informacion.
            oEnvia.EnviaInformacionBD(mDatos, Session["IdBrandSite"].ToString());

        }

        public string[] RegresaDatos(string sIdIndice) //Regresa los datos para llenar el lbl y el contenido al dar click.
        {
            try
            {
                cEditorManual oObtenerDatos = new cEditorManual(); //Se crea una instancia de la clase Editor manual para enviar informacion.
                DataTable dtDatos = oObtenerDatos.ObtenerDetalle(sIdIndice);

                string[] sArray = new string[3];
                sArray[0] = dtDatos.Rows[0][0].ToString();
                sArray[1] = dtDatos.Rows[0][1].ToString();
                sArray[2] = dtDatos.Rows[0][2].ToString();

                return sArray;
            }
            catch (Exception ex)
            {
                return null;
            }

            return null;
        }

        protected void btnArchivosRelacionadosImg1_Click(object sender, EventArgs e)
        {
            try
            {
                lblNoImagenSeleccionada.Text = "1";
                oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(lblIdIndiceSeleccionado.Text), 1);
                ppArchivosRelacionados.Show();
                Response.AppendHeader("X-XSS-Protection", "0");
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnArchivosRelacionadosImg2_Click(object sender, EventArgs e)
        {
            try
            {
                lblNoImagenSeleccionada.Text = "2";
                oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(lblIdIndiceSeleccionado.Text), 2);
                ppArchivosRelacionados.Show();
                Response.AppendHeader("X-XSS-Protection", "0");
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnNuevoArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                ppArchivosRelacionados.Hide();
                eExaminarArchivosRelacionados.Show(Session["IdBrandSite"].ToString(), "Selecciona un archivo relaciona a esta imágen. Solo se podrán consultar los archivos que no sean de trabajo.", "0");
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                string sIdArchivoRelacionado = ((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblIdArchivoRelacionado")))).Text;

                oMetodo = oBrandSite.EliminarArchivoRelacionado(sIdArchivoRelacionado);

                if(oMetodo.Pass == true)
                {
                    oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(lblIdIndiceSeleccionado.Text), 1);
                }

                ppArchivosRelacionados.Show();

            }
            catch(Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnSalirArchivosRelacionados_Click(object sender, EventArgs e)
        {
            lblNoImagenSeleccionada.Text = "0";
            ppArchivosRelacionados.Hide();
        }

    }
}

//Esta clase es auxiliar nos sirve para obtener los datos del menu y poder guardar el orden.
public class Menu
{
    public int ID { get; set; } //ID que trae el menu
    public Menu[] children { get; set; } //se llama a la misma clase ya que tiene los mismos atributos.

}