﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="Glosario.aspx.cs" Inherits="Identidad.Designer.WebForm3" meta:resourcekey="Glosario" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="divFondoPagina">

    <link href="../CSS/Pages/Glosario.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/Design.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="divTituloCentrado">
                <asp:Label ID="lblGlosario" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblGlosario"></asp:Label>
            </div>
            <%--<hr/>--%>
            <br /><br />
            <div id="Alfabeto-contenedor">
                <ul id="Alfabeto">
                    <li><a><asp:LinkButton ID="lkbA" runat="server" Text="A" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbB" runat="server" Text="B" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbC" runat="server" Text="C" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbD" runat="server" Text="D" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbE" runat="server" Text="E" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbF" runat="server" Text="F" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbG" runat="server" Text="G" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbH" runat="server" Text="H" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbI" runat="server" Text="I" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbJ" runat="server" Text="J" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbK" runat="server" Text="K" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbL" runat="server" Text="L" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbM" runat="server" Text="M" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbN" runat="server" Text="N" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbO" runat="server" Text="O" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbP" runat="server" Text="P" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbQ" runat="server" Text="Q" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbR" runat="server" Text="R" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbS" runat="server" Text="S" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbT" runat="server" Text="T" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbU" runat="server" Text="U" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbV" runat="server" Text="V" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbW" runat="server" Text="W" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbX" runat="server" Text="X" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbY" runat="server" Text="Y" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                    <li><a><asp:LinkButton ID="lkbZ" runat="server" Text="Z" OnClick="lkbAbecedario_Click"></asp:LinkButton></a></li>
                </ul>
            </div>
            <br /><br />

            <div style="width:100%; text-align:right;">
                <asp:TextBox ID="txtBusqueda" style="width:240px; padding-right:5px" CssClass="TextBox" runat="server"></asp:TextBox> &nbsp;
                <asp:Button ID="btnBuscar" ToolTip="Busca términos que sean ya parte de este Glosario" runat="server" CssClass="buttons colorAzulO" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click" />
                <asp:Button ID="btnAgregar" ToolTip="Añade aquí todas las palabras que desees incluir en el Glosario" style="margin-right:20px;" runat="server" CssClass="buttons colorVerde" meta:resourcekey="btnAgregar" OnClick="btnAgregar_Click" /> 
            </div>
                 
            <br /><br />

            <asp:GridView ID="gvGlosario" CssClass="mGrid" runat="server" AutoGenerateColumns="false" >
                <Columns>

                    <asp:TemplateField>
                            <ItemTemplate>
                                <br /><br />
                                <asp:Label ID="lblIdPalabra" runat="server" DataField ="IdPalabra" Text='<%#Eval ("IdPalabra") %>' Visible="false" ></asp:Label>
                                <asp:Label ID="lblPalabra" CssClass="Palabra" runat="server" DataField ="Palabra" Text='<%#Eval ("Palabra") %>' ></asp:Label>
                                <br /><br />
                                <asp:Label ID="lblDefinicion" runat="server" DataField ="Definicion" Text='<%#Eval ("Definicion") %>' ></asp:Label>
                                <br /><br />
                                <asp:Label ID="lblEtTodosMisBs" ToolTip="Si quieres usar términos genéricos para todos los sitios que diseñes, haz clic aquí. Si los de este sitio tienen un estilo propio (por ser modismos locales o corporativos), déjalo sin seleccionar." runat="server" meta:resourcekey="lblEtTodosMisBs"></asp:Label> 
                                <asp:CheckBox ID="cbkTodosMisBs" ToolTip="Si quieres usar términos genéricos para todos los sitios que diseñes, haz clic aquí. Si los de este sitio tienen un estilo propio (por ser modismos locales o corporativos), déjalo sin seleccionar." runat="server" Checked='<%#Eval ("TodosMisBS") %>' DataField ="TodosMisBS" Enabled="False" />
                                <br /><br />
                                <asp:LinkButton Id="lkbEditar" ToolTip="Cambia aquí el término y su definición." runat="server" OnClick="lkbEditar_Click" meta:resourcekey="lkbEditar"></asp:LinkButton>   
                                |
                                <asp:LinkButton Id="lkbEliminar" ToolTip="¿Adiós?" runat="server" OnClick="lkbEliminar_Click" meta:resourcekey="lkbEliminar"></asp:LinkButton>   
                                <br /><br />
                                <hr />
                                
                            </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

            <!--Inicia Popup para agregar una palabra--->
            <panel id="pnlEdicionPalabra" runat="server" style="display:none" >
                <div class="divTabla" style="height:350px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloAgregarBS" runat="server" meta:resourcekey="lblTituloAgregarBS"></asp:Label>
                    </div>
                    <hr />
                    <br /><br />
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblIdPalabra" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblEtPalabra" runat="server" meta:resourcekey="lblEtPalabra"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtPalabra" class="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divTR" style="height:70px;">
                        <div class="divTDIzq">
                            <asp:Label ID="lblEtDefinicion" runat="server" meta:resourcekey="lblEtDefinicion"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtDefinicion" class=" Multiline" Height="70px" TextMode="MultiLine" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblEtTodosBS" runat="server" meta:resourcekey="lblEtTodosBS"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                            <asp:CheckBox ID="ckbTodosBS" CssClass="ckbTodosBS" runat="server"></asp:CheckBox>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">

                        </div>
                        <div class="divTDDerechoInput">
                            <asp:Label ID="lblValidacion" runat="server" Visible="false"></asp:Label>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">

                        </div>
                        <div class="divTDDerechoButtons" style="margin-top:-25px;">
                            
                            <asp:Button ID="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                            <asp:Button ID="btnGuardarNuevo" class="buttons colorVerde" runat="server" meta:resourcekey="btnGuardarNuevo" OnClick="btnGuardarNuevo_Click" />

                        </div>
                    </div>
                </div>
            </panel>

            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppEdicionPalabra" TargetControlID="Label1" runat="server" PopupControlID="pnlEdicionPalabra" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".30" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para agregar una palabra--->

            <!-- Inicia Popup para eliminar una palabra --->
            <panel id="pnlConfirmarEliminar" runat="server" style="display:none" >
                <div class="divTabla" style="width:400px!important; height:280px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloEliminar" runat="server" meta:resourcekey="lblTituloEliminar"></asp:Label>
                    </div>
                    <hr />
                    <br /><br /><br />
                    <div class="divTR">
                        <div class="divTDCentrado">
                            <asp:Label ID="lblIdPalabraEliminar" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblConfirmarEliminar" runat="server" meta:resourcekey="lblConfirmarEliminar"></asp:Label>
                            <br /><br /><br />
                            
                            <asp:Button ID="btnCancelarEliminar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" />
                            <asp:Button ID="btnEliminar" class="buttons colorRojo" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" />

                        </div>
                    </div>
                </div>
            </panel>

            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppEliminarPalabra" TargetControlID="Label2" runat="server" PopupControlID="pnlConfirmarEliminar" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".10" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para eliminar una palabra --->

        </ContentTemplate>
    </asp:UpdatePanel>

</div>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblGlosario").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblGlosario").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

    <!-- Bloqueo la tecla enter en toda la página -->

    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

    <!-- Permito la tecla enter solo cuando key up el txtBuscar -->

    <script type="text/javascript">

        $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
            if (e.keyCode == 13) {

                $get('ContentPlaceHolder1_btnBuscar').click();
            }
        });

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
                        if (e.keyCode == 13) {

                            $get('ContentPlaceHolder1_btnBuscar').click();
                        }
                    })
                }

            });
        }

    </script>

</asp:Content>