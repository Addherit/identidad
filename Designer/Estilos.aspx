﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Designer/Designer.Master" AutoEventWireup="true" CodeBehind="Estilos.aspx.cs" Inherits="Identidad.Designer.WebForm5" meta:resourcekey="Estilos"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<%@ Register Src="~/Controles/Pantones.ascx" TagPrefix="identidad" TagName="Pantones" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/Pages/Estilos.css" rel="stylesheet" />
    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css' /> <%--font-family: 'Titillium Web', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css' /> <%--'Roboto Slab', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css' /> <%--'Roboto Condensed', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Serif' rel='stylesheet' type='text/css' /> <%--'Noto Serif', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css' /> <%--'Noto Sans', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css' /> <%--'Asap', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css' /> <%--'Signika', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Gentium+Basic' rel='stylesheet' type='text/css' /> <%--'Gentium Basic', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Alegreya' rel='stylesheet' type='text/css' /> <%--'Alegreya', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css' /> <%--'Lobster', regular--%>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,300italic,400italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Bitter:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Vollkorn:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'/>    
    <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Cinzel:400,700,900' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amaranth:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sacramento' rel='stylesheet' type='text/css'/>
      
  
    <!--Google Fonts-->
    <link href="../CSS/Fonts.css" rel="Stylesheet" type="text/css" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>

            <identidad:Pantones runat="server" id="cPantones" />
            <identidad:Pantones runat="server" id="cPantonesBoton" />

            <asp:Label ID="lblIdUsuario" runat="server" Text="Label" visible="false"></asp:Label>
            <asp:Label ID="lblIdBrandiste" runat="server" Text="Label" Visible="false"></asp:Label>

            <div class="divFondoPagina">
                <div class="divEncabezado">
                    <div class="divTitulos">
                        <asp:Label ID="lblEstilos" runat="server" meta:resourcekey="lblEstilos" ></asp:Label>
                    </div>
                    <div class="divOpciones">   
                        <asp:Button ID="btnReset" ToolTip="Por si haces cambios que prefieres ignorar." runat="server" meta:resourcekey="btnReset"     CssClass="buttons colorwhite alinear" OnClick="btnReset_Click" />
                        <asp:Button ID="btnGuardar" ToolTip="Para guardar los cambios de estilo que realices." runat="server" meta:resourcekey="btnGuardar" CssClass="buttons colorVerde alinear" OnClick="btnGuardar_Click1"/>
                    </div>
                    
                    
                </div>
                <%--<hr/>--%>
                
                <div class="divTDCentrado">
                 <asp:Label ID="notificaTam" runat="server">La previsualización de estilos tiene un máximo de 40 puntos, si tu título es mayor, se verá reflejado en el manual pero no en esta página.</asp:Label>
                </div>
                <br/>
                <br/>
                <div class="divBody">
                    <asp:GridView ID="gvwEstilos" CssClass="GridView" runat="server" AutoGenerateColumns="False" OnRowDataBound="OnRowDataBound" BorderStyle="None" ShowHeader="False">
                        <Columns>                         
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div class="divContenidoFentes" >
                                            <div class="divTituloFuente">
                                                <asp:Label ID="lblIdExB" runat="server" Text='<%#Eval ("idExB") %>' style="display:none" ></asp:Label>
                                                <asp:Label ID="lblidEstilo" runat="server" Text='<%#Eval ("idEstilo") %>' style="display:none" ></asp:Label>
                                                <asp:Label ID="lblNombreEstilo" runat="server" Text='<%#Eval ("NombreEstilo") %>'></asp:Label>
                                                <div class="divInputSeleccion">
                                                    <div id="elementos" style="float:left; width:80%!important; min-width:80%!important; margin-top:10px; margin-bottom:10px"">
                                                        <asp:Label ID="lblFuente" ToolTip="Elige la familia tipográfica." runat="server" Text='<%#Eval ("Fuente") %>' style="display:none"></asp:Label>
                                                        <asp:DropDownList ID="dpFuente" ToolTip="Elige la familia tipográfica." runat="server"  CssClass="DropDownList" AutoPostBack="true" OnSelectedIndexChanged="dpFuente_SelectedIndexChanged"></asp:DropDownList>
                                                        <asp:Label ID="lblVariante" ToolTip="Elige las fuentes disponibles en esta familia." runat="server" Text='<%#Eval ("Variante") %>' style="display:none"></asp:Label>
                                                        <asp:DropDownList ID="dpVariante" ToolTip="Elige las fuentes disponibles en esta familia." runat="server"  CssClass="DropDownList" style="min-width:120px!important; width:120px!important;" AutoPostBack="true" OnSelectedIndexChanged="dpVariante_SelectedIndexChanged">
                                                            <asp:ListItem>Thin</asp:ListItem>
                                                        
                                                        </asp:DropDownList>
                                                       <%-- <asp:DropDownList ID="dpItalic" runat="server"  CssClass="DropDownList" style="min-width:80px!important; width:80px!important; " AutoPostBack="true">
                                                            <asp:ListItem>Normal</asp:ListItem>
                                                            <asp:ListItem>Itálica</asp:ListItem>
                                                        </asp:DropDownList>--%>
                                                        <asp:Label ID="lblTamano" ToolTip="Elige el tamaño." runat="server" Text='<%#Eval ("Tamano") %>' style="display:none" ></asp:Label>
                                                    
                                                        <asp:TextBox ID="txtTamano" ToolTip="Elige el tamaño." runat="server" Text="12" style="min-width:50px!important; width:50px!important;" CssClass="TextBox tamano" onkeyup="fn_onkeyup(this);" AutoPostBack="true"></asp:TextBox>

                                                        <asp:DropDownList ID="dpTamano" style="min-width:50px!important; width:50px!important; display:none;" runat="server" CssClass="DropDownList tamano" AutoPostBack="true" >
                                                            <asp:ListItem>8</asp:ListItem>
                                                            <asp:ListItem>9</asp:ListItem>
                                                            <asp:ListItem>10</asp:ListItem>
                                                            <asp:ListItem>11</asp:ListItem>
                                                            <asp:ListItem>12</asp:ListItem>
                                                            <asp:ListItem>14</asp:ListItem>
                                                            <asp:ListItem>16</asp:ListItem>
                                                            <asp:ListItem>18</asp:ListItem>
                                                            <asp:ListItem>20</asp:ListItem>
                                                            <asp:ListItem>22</asp:ListItem>
                                                            <asp:ListItem>24</asp:ListItem>
                                                            <asp:ListItem>26</asp:ListItem>
                                                            <asp:ListItem>28</asp:ListItem>
                                                            <asp:ListItem>36</asp:ListItem>
                                                            <asp:ListItem>48</asp:ListItem>
                                                            <asp:ListItem>72</asp:ListItem>
                                                        </asp:DropDownList>
                                                 
                                                        <asp:CheckBox ID="chkNegrita" style="display:none;" Checked='<%#Eval ("Negrita") %>' runat="server"  Text="Negrita" AutoPostBack="true" OnCheckedChanged="chkNegrita_CheckedChanged"/> 
                                                        <asp:CheckBox ID="chkSubrayada" style="display:none;" Checked='<%#Eval ("Subrayada") %>' runat="server" Text="Subrayada" AutoPostBack="true" OnCheckedChanged="chkSubrayada_CheckedChanged" />
                                                        <asp:CheckBox ID="chkitalica" style="display:none;" Checked='<%#Eval ("Italica") %>' runat="server" Text="Itálica" AutoPostBack="true" OnCheckedChanged="chkitalica_CheckedChanged" />
                                                       
                                                    </div>
                                                    <div id="color" title="Define el color de este estilo tipográfico." style="float:right; width:20%!important; min-width:19%!important;">
                                                        <asp:TextBox ID="txtBGColor" runat="server" MaxLength="6"  Text='<%# Eval("Color") %>'   onkeyup="this.value=ValidaCaracteresHex(this.value) ;" onchange="this.value=ValidaHexadecimal(this.value);"  CssClass="TextBox" Enabled="false"></asp:TextBox>
                                                        <asp:Button ID="btnColor"  runat="server" meta:resourcekey="btnColor" class="buttons colorwhite" OnClick="btnColor_Click"  />
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <div class="divhr">
                                            <hr />
                                        </div>
                                    </ItemTemplate>
                            </asp:TemplateField>   
                        </Columns>
                    </asp:GridView>

                    <div style="width:100%; height:100px;">
                        <div style="float:left; width:65%!important; min-width:65%!important;">
                            <asp:Label ID="lblColorBoton" runat="server" meta:resourcekey="lblColorBoton"></asp:Label>
                            <asp:Button ID="btnEjemplo" runat="server" meta:resourcekey="btnEjemplo" CssClass="buttons colorAzulO" Enabled="false" style="margin-left:20px;"/>
                        </div>
                        <div style="float:right; width:25%!important; min-width:25%!important; margin-right:50px;">
                            <div id="color" style="width:100%; text-align:right;">
                                <asp:TextBox ID="txtBGColorBoton" runat="server" 
                                    MaxLength="6"  
                                    Text='<%# Eval("Color") %>' 
                                    onkeyup="this.value=ValidaCaracteresHex(this.value);" 
                                    onchange="this.value=ValidaHexadecimal(this.value);"
                                    CssClass="TextBox"
                                    Enabled="false"
                                    ></asp:TextBox>
                                <asp:Button ID="btnColorBoton"  runat="server" meta:resourcekey="btnColor" class="buttons colorwhite" OnClick="btnColorBoton_Click"  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!--Inicia Popup para validar tamaño--->
            <panel id="pnlValidar" runat="server" style="display:none" >
                <div class="divTabla" style="height:340px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloValidacion" runat="server" meta:resourcekey="lblTituloValidacion"></asp:Label>
                    </div>
                    <hr />
                    <br /><br /><br />
                    <div class="divTR">
                        <div class="divTDCentrado">
                            
                            <asp:Label ID="lblMensajeValidacion" runat="server" meta:resourcekey="lblMensajeValidacion"></asp:Label>
                            <br /><br />
                            <br />
                            
                            <asp:Button ID="btnOk" class="buttons colorwhite" runat="server" meta:resourcekey="btnOk"/>
                            
                        </div>
                         
                    </div>
  
                </div>
            </panel>

            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppValidarTamano" TargetControlID="Label2" runat="server" PopupControlID="pnlValidar" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".10" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para validar tamaño--->

        </ContentTemplate>
    </asp:UpdatePanel>
 
    <%-- Referencias a archivos Javascript --%>
    <script type="text/javascript" src="../JS/ColorPicker.js"></script>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblCatEstilos").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblCatEstilos").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

    <link href="../CSS/autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery-ui-1.11.2.js" type="text/javascript"></script>

    <script type="text/javascript">

        function cargarTamanos()
        {
            if (document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_0').value < 40) {
                document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_0').style.fontSize = document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_0').value + 'px';
            }
            else {
                document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_0').style.fontSize = 40 + 'px';

            }
            
            document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_1').style.fontSize = document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_1').value + 'px';
            document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_2').style.fontSize = document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_2').value + 'px';
            document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_3').style.fontSize = document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_3').value + 'px';
            //document.getElementById('ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_4').style.fontSize = document.getElementById('ContentPlaceHolder1_gvwEstilos_txtTamano_4').value + 'px';
           
        }

        function autocompleta() {
            var items = document.getElementById('ContentPlaceHolder1_gvwEstilos_dpTamano_0');
            var valor = 0;
            var tamano = items.length;

            var elementos = new Array(tamano);

            for (var indice = 0; indice < tamano; indice++) {
                if (items.options[indice].text != null || items.options[indice].text != "") {
                    elementos[indice] = items.options[indice].text;

                }
            }

            $("#ContentPlaceHolder1_gvwEstilos_txtTamano_0").autocomplete
            ({
                source: elementos
            });

            $("#ContentPlaceHolder1_gvwEstilos_txtTamano_1").autocomplete
            ({
                source: elementos
            });

            $("#ContentPlaceHolder1_gvwEstilos_txtTamano_2").autocomplete
            ({
                source: elementos
            });

            $("#ContentPlaceHolder1_gvwEstilos_txtTamano_3").autocomplete
            ({
                source: elementos
            });

            //$("#ContentPlaceHolder1_gvwEstilos_txtTamano_4").autocomplete
            //({
            //    source: elementos
            //});

        }

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function enPostback() {
                    cargarTamanos();
                    autocompleta();
                }
            );
        };
    
    </script>

    <script type="text/javascript">
        function fn_onkeyup(txtTamano)
        {

            var txtTamanoId = txtTamano.id;

            document.getElementById(txtTamanoId).value = txtTamano.value.replace(/[^0-9]+/g, '');

            var Tamano = document.getElementById(txtTamanoId).value;

            if (txtTamanoId != null) {

             
                var idIndex = txtTamanoId.slice(-1);
                

                if (idIndex != null) {

                    var lblTamano = "ContentPlaceHolder1_gvwEstilos_lblNombreEstilo_" + idIndex;

                    if (Tamano <= 40) {
                        document.getElementById(lblTamano).style.fontSize = Tamano + 'px';
                        
                    }
                    else {
                        document.getElementById(lblTamano).style.fontSize = 40 + 'px';
                        
                    }
                }
            }
            
        };
    </script>

</asp:Content>
