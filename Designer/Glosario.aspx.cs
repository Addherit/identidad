﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Identidad.App_Code;
using System.Data;

namespace Identidad.Designer
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    CargarPalabras("%");
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarPalabras(String sLetra)
        {
            oBrandSite.CargarPalabras(gvGlosario, sLetra, Session["IdBrandSite"].ToString());
        } // Carga todas las palabras relacionadas a este brand site en un gridview

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        #region Agregar palabra

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdPalabra.Text = "0";
                txtPalabra.Text = "";
                txtDefinicion.Text = "";
                ckbTodosBS.Checked = false;
                lblValidacion.Visible = false;

                ppEdicionPalabra.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnGuardarNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                ppEdicionPalabra.Show();

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.ActualizarPalabra(lblIdPalabra.Text, txtPalabra.Text, txtDefinicion.Text, ckbTodosBS.Checked.ToString(), Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == false)
                {
                    lblValidacion.Text = oMetodo.Message;
                    lblValidacion.Visible = true;
                }
                else
                {
                    ppEdicionPalabra.Hide();

                    CargarPalabras("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdPalabra.Text = "0";
                txtPalabra.Text = "";
                txtDefinicion.Text = "";
                ckbTodosBS.Checked = false;
                lblValidacion.Visible = false;

                ppEdicionPalabra.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        #region Eliminar palabra

        protected void lkbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdPalabraEliminar.Text = ((System.Web.UI.WebControls.Label)((gvGlosario.Rows[Row].FindControl("lblIdPalabra")))).Text;

                ppEliminarPalabra.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminarPalabra.Show();

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.EliminarPalabra(lblIdPalabraEliminar.Text, IdUsuario);

                if (oMetodo.Pass == true)
                {
                    CargarPalabras("%");
                    ppEliminarPalabra.Hide();
                }
                else
                {
                    lblConfirmarEliminar.Text = oMetodo.Message;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdPalabraEliminar.Text = "0";
                ppEliminarPalabra.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        protected void lkbEditar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdPalabra.Text = ((System.Web.UI.WebControls.Label)((gvGlosario.Rows[Row].FindControl("lblIdPalabra")))).Text;

                foreach (DataRow row in oBrandSite.CargarPalabra(lblIdPalabra.Text).Rows)
                {
                    txtPalabra.Text = row["Palabra"].ToString();
                    txtDefinicion.Text = row["Definicion"].ToString();
                    ckbTodosBS.Checked = bool.Parse(row["TodosMisBS"].ToString());
                }

                lblValidacion.Visible = false;

                ppEdicionPalabra.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbAbecedario_Click(object sender, EventArgs e)
        {
            try
            {
                if (((System.Web.UI.WebControls.LinkButton)(sender)).Text != null || ((System.Web.UI.WebControls.LinkButton)(sender)).Text != "")
                {
                    CargarPalabras(((System.Web.UI.WebControls.LinkButton)(sender)).Text);
                }
                else
                {
                    CargarPalabras("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarPalabras("%" + txtBusqueda.Text.Trim() + "%");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}