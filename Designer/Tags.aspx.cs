﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;
using System.Data;

namespace Identidad.Designer
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    CargarArchivos();
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarArchivos()
        {
            oBrandSite.CargarTags(gvTags, Session["IdBrandSite"].ToString());
        } // Carga el listado de tags en el gridview

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void btnNuevoTag_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdTag.Text = "0";
                imgTag.ImageUrl = "";
                txtNombre.Text = "";
                txtDescripcion.Text = "";
                txtExtension.Text = "";
                lblValidacion.Visible = false;
                ckbTodosBS.Checked = false;

                ppTag.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }
        
        protected void btnSubir_Click(object sender, EventArgs e)
        {
            try
            {
                ppTag.Show();

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.ActualizarTag(lblIdTag.Text, txtNombre.Text, txtDescripcion.Text, lblIdArchivo.Text, txtExtension.Text, ckbTodosBS.Checked.ToString(), Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == true)
                {
                    ppTag.Hide();

                    CargarArchivos();
                }
                else
                {
                    lblValidacion.Visible = true;
                    lblValidacion.Text = oMetodo.Message;
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                ppTag.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.ElmiminarTag(lblIdTagEliminar.Text, IdUsuario);

                if (oMetodo.Pass == true)
                {
                    ppEliminarTag.Hide();
                    CargarArchivos();
                }
                else
                {
                    lblConfirmarEliminar.Text = oMetodo.Message;
                    ppEliminarTag.Show();
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminarTag.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        protected void btnSeleccionarArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                eExaminar.Show(Session["IdBrandSite"].ToString(), "Te recomendamos seleccionar una imagen .jpg de 20 X 20 pixeles", true);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                eExaminar.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eExaminar_OnArchivoSeleccionado);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eExaminar_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                ppTag.Show();
                lblIdArchivo.Text = eExaminar.ObtenerIdArchivoSeleccionado();
                imgTag.ImageUrl = eExaminar.ObtenerUrlSeleccionada();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbEditar_Click(object sender, EventArgs e)
        {
            LinkButton Status = (LinkButton)sender;
            int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            lblIdTag.Text = ((System.Web.UI.WebControls.Label)((gvTags.Rows[Row].FindControl("lblIdTag")))).Text;

            foreach (DataRow row in oBrandSite.CargarTag(lblIdTag.Text).Rows)
            {
                lblIdArchivo.Text = row["IdArchivo"].ToString();
                imgTag.ImageUrl = row["Url"].ToString();
                txtNombre.Text = row["Nombre"].ToString();
                txtDescripcion.Text = row["Descripcion"].ToString();
                txtExtension.Text = row["Extension"].ToString();
                ckbTodosBS.Checked = bool.Parse(row["TodosMisBS"].ToString());
            }

            lblValidacion.Text = "";

            ppTag.Show();
        }

        protected void lkbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdTagEliminar.Text = ((System.Web.UI.WebControls.Label)((gvTags.Rows[Row].FindControl("lblIdTag")))).Text;

                ppEliminarTag.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}