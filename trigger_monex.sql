-- ================================================
-- Template generated from Template Explorer using:
-- Create Trigger (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- See additional Create Trigger templates for more
-- examples of different Trigger statements.
--
-- This block of comments will not be included in
-- the definition of the function.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<senjuana,,Ernesto Ruiz>
-- Create date: <2/6/2020>
-- Description:	<This trigger just copy the recent insert for monex or monex global, just to share the same list>
-- =============================================
CREATE TRIGGER [dbo].[tgr_monex]
   ON  [dbo].[catUsuariosPorBrandSite] 
   AFTER INSERT
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @idU int,@idTU int,@idBS int,@idUA int, @PI bit,@EQ bit,@Activo bit,@FechaActivo datetime
	DECLARE @cou int
    -- Insert statements for trigger here
	Select @idU = i.IdUsuario,@idTU = i.IdTipoUsuario,@idBS = i.IdBrandSite,@idUA =i.IdUsuarioActualiza,@PI = i.PuedeInvitar,
	@EQ = i.EsDeMiEquipo,@Activo = i.Activo,@FechaActivo = i.FechaActivo from inserted as i

	IF @idBS = 430 -- monex
	BEGIN
		SET @cou = (select count(idUsuario) from catUsuariosPorBrandSite where IdUsuario = @idU and IdBrandSite = 434 and Activo = 1)
		IF @cou = 0
		BEGIN 
			insert into catUsuariosPorBrandSite(IdUsuario,IdTipoUsuario,IdBrandSite,IdUsuarioActualiza,PuedeInvitar,EsDeMiEquipo,Activo,FechaActivo)
			values(@idU ,@idTU ,434,@idUA , @PI ,@EQ ,@Activo ,@FechaActivo )
		END
		
	END
	IF @idBS = 434 --monex global
	BEGIN
		SET @cou = (select count(idUsuario) from catUsuariosPorBrandSite where IdUsuario = @idU and IdBrandSite = 430 and Activo = 1)
		IF @cou = 0
		BEGIN 
			insert into catUsuariosPorBrandSite(IdUsuario,IdTipoUsuario,IdBrandSite,IdUsuarioActualiza,PuedeInvitar,EsDeMiEquipo,Activo,FechaActivo)
			values(@idU ,@idTU ,430,@idUA , @PI ,@EQ ,@Activo ,@FechaActivo )
		END
	END
END
GO
