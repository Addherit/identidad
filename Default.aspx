﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Identidad._default" meta:resourcekey="default" %>
<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta name="google-site-verification" content="xQbATgheTw45Jgd_XcHLAGm6MvXWvM8XVAZhj03xkF4" /> 
    
    <title>Identidad.com | Iniciar sesión</title>
    <link href="CSS/Pages/Default.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Site.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Design.css" rel="stylesheet" type="text/css" />
    <link href='/Images/Site/ID.ico' rel='shortcut icon' type='image/x-icon'/>
    <meta name="application-name" content="Identidad.com"/>
    <meta name="application-tooltip" content="Identidad.com"/>
    <meta name="application-bavbutton-color" content="Blue"/>
    <link href="/Images/Site/ID.ico" rel="icon" type="image/ico"/>
	<link href="/Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="apple-touch-icon-precomposed" href="/Images/Site/ID.ico"/>
	<link rel="apple-touch-icon" href="/Images/Site/ID.ico"/>
    <link rel="apple-touch-startup-image" href="/Images/Site/ID.ico" />
    <meta name="apple-mobile-web-app-title" content="Identidad.com"/>
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='apple-touch-fullscreen' content='yes'/>
    <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
    <link rel="icon" href="/Images/Site/ID.ico" />
	<link rel="shortcut icon" href="/Images/Site/ID.ico" />
    <meta charset="utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:700,300' rel='stylesheet' type='text/css'/>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-56195706-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>



    <form id="form1" name="form1" runat="server" defaultbutton="btnIniciarSesion" defaultfocus="txtUsuario">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

       <label id="Label1"></label>
       <div class="divFondoDefault"  style="background-image:url('Images/Site/IDENT_Home_Grid.png')" >
            <asp:ScriptManager ID="sm" runat="server" ScriptMode="Release" ></asp:ScriptManager>
            <div id="divContenido">
                
                
                <%-- Nuevo diseño Marzo 2016 --%>
                <div id="CopyR">
                    <asp:Label ID="lblCopy" runat="server" Text="© 2015-2016 Identidad.com. All Rights Reserved."></asp:Label>
                </div>
                 <div id="tBienvenido">
                    <asp:Label ID="lblBienvenido" runat="server" Text="Bienvenido"></asp:Label><br />
                     <asp:Label ID="lblText" CssClass="Texto" runat="server" Text="Construyamos"></asp:Label><br />
                     <asp:Label ID="lblText2" CssClass="Texto" runat="server" Text="tu marca"></asp:Label>
                </div>
                <div id="menuIngreso">
                    <asp:Label ID="lblSubtitulo" runat="server" Text="Si ya eres cliente, ingresa"></asp:Label><br />
                    <asp:TextBox ID="txtRCorreo" CssClass="txtregistro" runat="server" meta:resourcekey="txtUsuario" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"></asp:TextBox><br />
                    <asp:TextBox ID="txtRPassword" CssClass="txtregistro" placeholder="Contraseña" TextMode="Password" runat="server" ></asp:TextBox><br />
                    <asp:LinkButton ID="lblRecuperar" runat="server" meta:resourcekey="lkbRecuperarContraseña" OnClick="lkbRecuperarContraseña_Click"></asp:LinkButton>
                    <asp:Label ID="lblValidacion"  runat="server"  Visible="true" ></asp:Label>
                </div>
                
                <%-- ---------------------------------------------------------------------------------- --%>
                <%--<asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate> --%>
                        
                        <div id="divBoton">
                            <input type="button" value="Conócenos" class="colorAqua" onclick="window.open('IDEN_Bienvenidos_documento.pdf', '_blank');" >
                            <asp:Button ID="btnContactanos" runat="server" CssClass="colorAqua" Text="Contáctanos"  OnClick="lkbContactanos_Click" />
                            <asp:LinkButton ID="lkbContactanos" runat="server"  OnClick="lkbContactanos_Click" Visible="false"></asp:LinkButton>
                          
                            <asp:Button ID="btnIniciarSesion" runat="server" CssClass="colorRosa iniciaS" Text="Login"  OnClick="btnIniciarSesion_Click" />
                            <asp:LinkButton ID="lkbIniciarSesion"  runat="server" class="colorRosa iniciaS" meta:resourcekey="lkbIniciarSesion" OnClick="btnIniciarSesion_Click" Visible="false"></asp:LinkButton><br />
                            <asp:Label ID="lblIniciarSesion" runat="server" Visible="false" Text="Valida inicio"></asp:Label>
                           
                        </div>
                        
<%--                    </ContentTemplate>
                </asp:UpdatePanel>--%>
                <%-- -----------------------Nuevo diseño 2---------------------- --%>
                <div id="contentDefault">
                    <asp:Label ID="Label4" CssClass="textoDefault" runat="server" Text="Concentra todos"></asp:Label><br />
                    <asp:Label ID="Label6" CssClass="textoDefault" runat="server" Text="tus recursos"></asp:Label><br />
                    <asp:Label ID="Label7" CssClass="textoDefault" runat="server" Text="para dar "></asp:Label>
                    <asp:Label ID="Label8" CssClass="textoDefaultR" runat="server" Text="vida"></asp:Label><br />
                    <asp:Label ID="Label10" CssClass="textoDefaultR" runat="server" Text="a tu identidad"></asp:Label><br />
                </div>
                <%-- ----------------------------------------------------------- --%>
                <%-- -----------------------Nuevo diseño 2---------------------- --%>
                <div id="menuRegistro">
                    <asp:Label ID="Label11" CssClass="textoRegistro" runat="server" Text="Encuentra un plan para ti"></asp:Label><br />
                    <asp:Label ID="Label12" CssClass="textoRegistro" runat="server" Text="en función de lo que necesites:"></asp:Label><br />
                    <br /><br />
                    <asp:Button ID="btnRegistrate" runat="server" CssClass="colorRosa" Text="Regístrate ahora"  OnClick="lkbRegistro_Click"/>
                    

                            <asp:LinkButton ID="lkbRegistro" runat="server" OnClick="lkbRegistro_Click" Visible="false"></asp:LinkButton>

                    <%--<asp:Button ID="btnRegistroS" runat="server" CssClass="colorRosa" Text="Regístrate ahora"  OnClick="lkbRegistroS_Click" />--%>
                            <%--<asp:LinkButton ID="lkbRegistroS" runat="server" OnClick="lkbRegistroS_Click" Visible="false"></asp:LinkButton>--%>

                    <%--<asp:Button ID="btnPago" runat="server" CssClass="colorRosa" Text="Paga ahora"  OnClick="lkbPago_Click" />
                            <asp:LinkButton ID="lkbPago" runat="server" OnClick="lkbPago_Click" Visible="false"></asp:LinkButton>--%>
                            
                    
                </div>
                <%-- ----------------------------------------------------------- --%>
            </div>
        </div>

<%--       <asp:UpdatePanel ID="upd2" runat="server">
            <ContentTemplate>--%>

                <panel id="pnlIniciarSesion" runat="server" style="display: none">
                    <div class="divTabla" style="height:330px;">
                        <div>
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTitulo" runat="server" meta:resourcekey="lblTitulo"></asp:Label> 
                            </div>
                            <%--<div class="divTablaTituloImagen">
                                <img id="imgbtnC" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelar.ClientID %>').click();" />
                            </div>--%>
                        </div>
                    <hr/>
                    <br />
                    <%-- Divs para los texbox --%>
                    <div class="divTBODY">
                        <div class="divTR">
                            <div class="divTDIzq"> 
                                <asp:Label ID="lblUsuario" runat="server" meta:resourcekey="lblUsuario"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput"> 
                               
                                    <asp:TextBox ID="txtUsuario" CssClass="TextBox" runat="server" meta:resourcekey="txtUsuario" onblur="onLeave(this)" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"></asp:TextBox>
                            </div>
                        </div>

                        <div class="divTR">

                            <div class="divTDIzq"> 
                                <asp:Label ID="lblContraseña" runat="server" meta:resourcekey="lblContraseña"></asp:Label>
                            </div>

                            <div class="divTDDerechoInput"> 
                                <asp:TextBox ID="txtContraseña" CssClass="TextBox" TextMode="Password" runat="server" onblur="onLeave(this)"></asp:TextBox>
                            </div>

                        </div>

                        <div class="divTR">
                                
                            <div class="divTDIzq"></div>

                            <div class="divTDDerechoInput"> 
                                <asp:LinkButton ID="lkbRecuperarContraseña" runat="server" meta:resourcekey="lkbRecuperarContraseña" OnClick="lkbRecuperarContraseña_Click"></asp:LinkButton>
                            </div>

                        </div>

                        <div class="divTR">
                        <div class="divTDIzq"> 
                              
                        </div>
                            <div class="divTDDerechoInput"> 
                        <div id="divValidacion"  runat="server"> 
                            <%--<asp:Label ID="lblValidacion" class="divValidacion" runat="server" ></asp:Label>--%>
                        </div>
                                </div>
                                  
                    </div>

                       <%-- <div class="divTR">
                            <div class="divTDIzq"></div>
                            <div class="divTDDerechoButtons" > 
                                <asp:Button id="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" meta:resourcekey="btnCancelar" runat="server" autopostback="true" OnClick="btnCancelar_Click"></asp:Button>
                                <asp:Button id="btnIniciarSesion" class="buttons colorVerde" meta:resourcekey="btnIniciarSesion" runat="server" autopostback="true" OnClick="btnIniciarSesion_Click"></asp:Button>
                            </div>
                        </div>--%>

                    </div>
                    </div>
                </panel>

                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppIniciarSesion" TargetControlID="Label1" runat="server" PopupControlID="pnlIniciarSesion" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" >
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>


                <panel id="pnlMisSitios" runat="server" style="display: none">
                    <div class="divTabla" style="height:330px;">
                        <div>
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTituloMisSitios" runat="server" meta:resourcekey="lblTituloMisSitios"></asp:Label>
                            </div>
                        </div>
                        <hr />
                        <br />
                        <br />
                        <br />
                        <div class="divTBODY">
                            <div class="divTR" >
                                <div class="divTDIzq">
                                    <asp:Label ID="lblMisSitios" runat="server" meta:resourcekey="lblMisSitios"></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:DropDownList ID="ddlMisSitios" CssClass="DropDownList" runat="server" ></asp:DropDownList>
                                </div>
                                <div class="divTR">
                                    <div class="divTDIzq">

                                    </div>
                                    <div class="divTDDerechoButtons">
                                       
                                        <asp:Button ID="btnGo" class="buttons colorVerde btnGo" style="margin-left:130px;" runat="server" meta:resourcekey="btnGo" OnClick="btnGo_Click"></asp:Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppMisSitios" TargetControlID="Label2" runat="server" PopupControlID="pnlMisSitios" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                </Ajax:ModalPopupExtender>

                <%--  PopUp Contactanos ---------------------------------------------------------------------------------------------------------- --%>
                <panel id="pnlContactanos" runat="server" style="display: none">
                    <div id="Contactanos" class="divTabla" style="height:384px;width:384px; background-color:#23ffff;">
                        <div>
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTituloContactanos" CssClass="lblTitulo" runat="server" meta:resourcekey="lblTituloContactanos">Contáctanos</asp:Label>
                            </div>
                        </div>                       
                        
                        <div class="divTBODY">
                            <div class="" >
                                <%--<asp:Button ID="Button7" CssClass="btnCancelarC" runat="server" Text="X" />--%> 
                                <asp:TextBox ID="txtNombre" Style="margin-left:20px; margin-top:10px; font-family:'open sans regular', sans-serif;" Width="152px" Height="32px" placeholder="Nombres"  runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtApellidos" Style="margin-left:10px; margin-top:10px; font-family:'open sans regular', sans-serif;" Width="152px" Height="32px" placeholder="Apellidos"  runat="server"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtCorreo" Style="margin-left:20px; margin-top:10px; font-family:'open sans regular', sans-serif;" Width="320px" Height="32px" placeholder="Email"  runat="server"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtComentarios" Style="margin-left:20px; margin-top:10px; font-family:'open sans regular', sans-serif;" Width="320px" Height="142px" placeholder="Comentarios"  runat="server" TextMode="MultiLine"></asp:TextBox>
                                
                                
                                <div class="divTR">
                                  
                                         <asp:Button id="btnCancelarCont" Text="Cancelar" style="margin-left:20px;" CssClass="botonesRegistro"  runat="server" autopostback="true" OnClick="btnCancelar_Click"></asp:Button>
                                <input type="button" id="btnEnviar" class="botonesRegistro" style="margin-left:15px;" value="Enviar" onclick="isValidEmail(this);" />
                                        <br /><asp:Label ID="lblmessage" CssClass="" runat="server" Text="Label"  style="color:red; display:none;">Asegurese de que el formato del correo sea correcto y completar todos los campos</asp:Label>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppContactanos" CancelControlID="btnCancelarCont" TargetControlID="Label5" runat="server" PopupControlID="pnlContactanos" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                </Ajax:ModalPopupExtender>


                <%-- PopUp Registro------------------------------------------------------------------ --%>

                <panel id="pnlRegistro" runat="server" style="display: none">
                    <div id="Registro" class="divTabla" style="height:384px; width:384px; background-color:#23ffff;"> 
                        
                            <%--<asp:Button ID="Button6" CssClass="btnCancelarR" runat="server" Text="X" />--%>
                                    
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTituloR" CssClass="lblTitulo" runat="server" meta:resourcekey="lblTituloContactanos">Regístrate ahora</asp:Label>
                            </div>
                                              
                        
                        <div class="divTBODY">
                            <div class="divTR" >
                                 
                                <asp:TextBox ID="txtNombreRegistro"  Style="margin-left:20px;" Width="152px" Height="32px" placeholder="Nombres" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtApellidoRegistro"  Style="margin-left:10px; margin-top:10px;" Width="152px" Height="32px" placeholder="Apellidos" runat="server"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtCorreoRegistro" placeholder="Email"  Style="margin-left:20px; margin-top:10px;" Width="320px" Height="32px" runat="server" ></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtPassRegistro" placeholder="Contraseña"  Style="margin-left:20px; margin-top:10px;" Width="320px" Height="32px" runat="server" TextMode="Password"></asp:TextBox>
                                <br />
                                <asp:TextBox ID="txtPassWRegistro" placeholder="Confirmar Contraseña"  Style="margin-left:20px; margin-top:10px;" Width="320px" Height="32px" runat="server" TextMode="Password"></asp:TextBox>
                                <br /><br />   
                                <div id="chkTerminos" class="checkbox-group">          
                            <input type="checkbox" id="c5" runat="server" name="c5" />       
                            <label for="c5"><span></span></label>
                            </div>                          
                                <%--<asp:CheckBox ID="CheckBox1" Style="margin-left:20px; margin-top:10px;"  runat="server"></asp:CheckBox>--%>
                                <asp:Label ID="lblAceptoTerminos" runat="server">Acepto los</asp:Label>
                                <asp:HyperLink ID="HlkAceptoTerminos" CssClass="subrayados" style="text-decoration:underline;" runat="server"  NavigateUrl="IDEN_Bienvenidos_documento.pdf" Target="_blank" Text="Terminos y condiciones"></asp:HyperLink>
                                <br />
                                <asp:Button ID="btnRegistroCancelar" CssClass="botonesRegistro" runat="server" Text="Cancelar" Style="margin-left:20px; margin-top:10px;"/>
                                <asp:Button ID="btnRegistroAceptar" CssClass="botonesRegistro" runat="server" Text="Aceptar" OnClick="btnRegistroAceptar_Click" Style="margin-left:10px; margin-top:10px;"/>
                                <br /><br />
                                <div class="negritas">
                                <asp:Label ID="Label13" runat="server">¿Ya tienes cuenta?</asp:Label>
                                <asp:HyperLink ID="HyperLink1" CssClass="subrayados" runat="server" style="text-decoration:underline;"  NavigateUrl="Default.aspx" Target="_blank" Text="Inicia Sesión"></asp:HyperLink>
                                </div>

                                <asp:Label ID="lblMensajeRegistro" runat="server" Text="Mensaje" Visible="false"></asp:Label>
                             </div>
                         </div>
                            </div>
                        </div>
                    </div>
                </panel>

                <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppRegistro" CancelControlID="btnRegistroCancelar" TargetControlID="Label18" runat="server" PopupControlID="pnlRegistro" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                </Ajax:ModalPopupExtender>
                
                <%-- ------------------------------------------------------------------------------- --%>
                 <%-- PopUp PosRegistro------------------------------------------------------------------ --%>

                <panel id="pnlRegistroSeleccion" runat="server" style="display: none">
                    <div id="RegistroSeleccion" class="divTabla" style="height: 584px; width: 825px; background: url('Images/Site/PlantillaRegistro.png');">                        
                        
                        <asp:Button ID="btnCancelarPosRegistro" CssClass="btnCancelar" runat="server" Text="X" />
                        <br />
                        <asp:Label ID="lblTituloPosRegistro" CssClass="tituloPosRegistro" runat="server">Accesible para</asp:Label><br />
                        <asp:Label ID="lblTituloPosRegistro2" CssClass="tituloPosRegistro" runat="server">todo</asp:Label>
                        <asp:Label ID="lblTituloPosRegistro3" CssClass="tituloPosRegistroRosa" runat="server">usuario</asp:Label>
                        <br /><br /><br />
                        <asp:Label ID="lblTextoPosRegistro" CssClass="TextoPosRegistro" runat="server">Encuentra un plan para ti</asp:Label><br />
                        <asp:Label ID="lblTextoPosRegistro2" CssClass="TextoPosRegistro" runat="server">en función de lo que necesites:</asp:Label>
                        <%--<asp:Button ID="btnRegistroEmpresa" CssClass="btnsRegistro" runat="server" Text="Eres empresa" OnClick="btnRegistroEmpresa_Click" />--%>
                        
                        <button id="btnRegistroEmpresa" class="btnsRegistro" runat="server" onclick="muestraEmpresa(); return false;">Eres empresa</button>
                        
                        <%--<asp:Button ID="btnRegistroDisenador" CssClass="btnsRegistro" runat="server" Text="Eres diseñador" />--%>
                        <button id="btnRegistroDisenador" class="btnsRegistro" runat="server" onclick="muestraDiseno(); return false;">Eres diseñador</button>

                        <br /><br /><br />
                        <div id="RegistroDisenador" class="RegistroDisenador" style="visibility:hidden;"  runat="server">
                            <div id="DescripcionDisenador">
                                <asp:Label ID="lblTextoRegistroDisenador" runat="server">Amigo diseñador:</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador2" runat="server">Te ofrecemos 5 o 10 sitios</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador3" runat="server">permanentes de identidad.com,</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador4" runat="server">para que tu cliente pruebe</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador5" runat="server">la herramienta</asp:Label><br />
                                <br /><br />
                                <asp:Label ID="lblTextoRegistroDisenador6" runat="server">Cada sitio tiene 3 meses</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador7" runat="server">de servicio de regalo, para que</asp:Label><br />
                                <asp:Label ID="lblTextoRegistroDisenador8" runat="server">lo puedas armar y entregar.</asp:Label>
                            </div>
                            <div id="Plan1" class="Planes espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="lblPlan1Disenador" CssClass="tituloPlan espacio" runat="server">Plan 1</asp:Label><br />
                                <asp:Label ID="lblPlan1Disenador2" CssClass="DescripcionPlan espacio" runat="server">(hasta 5 manuales</asp:Label><br />
                                <asp:Label ID="lblPlan1Disenador3" CssClass="DescripcionPlan espacio" runat="server">permanentes)</asp:Label><br />
                                <br />
                                <asp:Label ID="lblPlan1Disenador4" CssClass="gigas espacio" runat="server"><b>10</b> GB</asp:Label><br />
                                <br /><br />
                                <asp:Label ID="lblPlan1Disenador5" CssClass="pagos espacio" runat="server"><b>$10</b> Dólares/mes</asp:Label><br />
                                <asp:Label ID="lblPlan1Disenador6" CssClass="pagos espacio" runat="server"><b>$120</b> Dólares/año</asp:Label><br />
                                <asp:Label ID="lblPlan1Disenador7" CssClass="pagos espacio" runat="server"><b>$110</b> Dólares/Pago anual</asp:Label><br />
                                <br />
                                <asp:Button ID="btnComienza1" CssClass="btnSeleccionarPlan espacio" OnClientClick="Comienza1();" runat="server" Text="Comienza ahora" />
                                <br />
                            </div>
                            <div id="Plan2" class="Planes espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="lblPlan2Disenador" CssClass="tituloPlan espacio" runat="server">Plan 2</asp:Label><br />
                                <asp:Label ID="lblPlan2Disenador2" CssClass="DescripcionPlan espacio" runat="server">(hasta 10 manuales</asp:Label><br />
                                <asp:Label ID="lblPlan2Disenador3" CssClass="DescripcionPlan espacio" runat="server">permanentes)</asp:Label><br />
                                <br />
                                <asp:Label ID="lblPlan2Disenador4" CssClass="gigas espacio" runat="server"><b>20</b> GB</asp:Label><br />
                                <br /><br />
                                <asp:Label ID="lblPlan2Disenador5" CssClass="pagos espacio" runat="server"><b>$20</b> Dólares/mes</asp:Label><br />
                                <asp:Label ID="lblPlan2Disenador6" CssClass="pagos espacio" runat="server"><b>$240</b> Dólares/año</asp:Label><br />
                                <asp:Label ID="lblPlan2Disenador7" CssClass="pagos espacio" runat="server"><b>$220</b> Dólares/Pago anual</asp:Label><br />
                                <br />
                                <asp:Button ID="btnComienza2" CssClass="btnSeleccionarPlan espacio" OnClientClick="Comienza2();" runat="server" Text="Comienza ahora" />
                                <br />
                            </div>
                        </div>
                        <div id="RegistroEmpresa" class="RegistroEmpresa" style="visibility:hidden;">
                            <div id="PlanPyme" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="Label16" CssClass="tituloPlan espacio" runat="server">PYME</asp:Label><br />
                                 <asp:Label ID="Label21" CssClass="textoemp espacio" runat="server"><b>15</b> Usuarios</asp:Label><br />
                                 <asp:Label ID="Label22" CssClass="textoemp espacio" runat="server"><b>100</b> Archivos</asp:Label><br />
                                 <asp:Label ID="Label23" CssClass="textoemp espacio" runat="server"><b>0.5</b> Gigas</asp:Label><br />
                                <br />
                                <asp:Label ID="Label24" CssClass="textoemp espacio" runat="server"><b>$49</b></asp:Label><br />
                                <asp:Label ID="Label3" CssClass="textoemp espacio" runat="server">Dólares/mes</asp:Label><br />
                                <asp:Label ID="Label9" CssClass="textoemp espacio" runat="server"><b>$588</b></asp:Label><br />
                                <asp:Label ID="Label14" CssClass="textoemp espacio" runat="server">Dólares/año</asp:Label><br />
                                 <asp:Label ID="Label25" CssClass="textoemp espacio" runat="server"><b>$539</b></asp:Label><br />
                                 <asp:Label ID="Label26" CssClass="textoemp espacio" runat="server">Dólares/Pago anual</asp:Label><br />
                                
                                <br />
                                <asp:Button ID="btnComienzaPYME" CssClass="btnSeleccionarPlan espacio"  OnClientClick="PYME();" runat="server" Text="Comienza ahora"  autopostback="true" />
                                <br />
                            </div>
                             <div id="PlanEmpresa" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="Label17" CssClass="tituloPlan espacio" runat="server">Empresa</asp:Label><br />
                                 <asp:Label ID="Label19" CssClass="textoemp espacio" runat="server"><b>50</b> Usuariso</asp:Label><br />
                                 <asp:Label ID="Label20" CssClass="textoemp espacio" runat="server"><b>300</b> Archivos</asp:Label><br />
                                 <asp:Label ID="Label27" CssClass="textoemp espacio" runat="server"><b>2</b> Gigas</asp:Label><br />
                                <br />
                                <asp:Label ID="Label28" CssClass="textoemp espacio" runat="server"><b>$139</b></asp:Label><br />
                                <asp:Label ID="Label29" CssClass="textoemp espacio" runat="server">Dólares/mes</asp:Label><br />
                                <asp:Label ID="Label30" CssClass="textoemp espacio" runat="server"><b>$1,668</b></asp:Label><br />
                                <asp:Label ID="Label31" CssClass="textoemp espacio" runat="server">Dólares/año</asp:Label><br />
                                 <asp:Label ID="Label32" CssClass="textoemp espacio" runat="server"><b>$1,529</b></asp:Label><br />
                                 <asp:Label ID="Label33" CssClass="textoemp espacio" runat="server">Dólares/Pago anual</asp:Label><br />
                                
                                <br />
                                <asp:Button ID="btnComienzaEmpresa" CssClass="btnSeleccionarPlan espacio" OnClientClick="Empresa();"  runat="server" Text="Comienza ahora" />
                                <br />
                            </div>
                             <div id="PlanCorporativo" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="Label34" CssClass="tituloPlan espacio" runat="server">Corporativo</asp:Label><br />
                                 <asp:Label ID="Label35" CssClass="textoemp espacio" runat="server"><b>100</b> Usuarios</asp:Label><br />
                                 <asp:Label ID="Label36" CssClass="textoemp espacio" runat="server"><b>600</b> Archivos</asp:Label><br />
                                 <asp:Label ID="Label37" CssClass="textoemp espacio" runat="server"><b>3</b> Gigas</asp:Label><br />
                                <br />
                                <asp:Label ID="Label38" CssClass="textoemp espacio" runat="server"><b>$259</b></asp:Label><br />
                                <asp:Label ID="Label39" CssClass="textoemp espacio" runat="server">Dólares/mes</asp:Label><br />
                                <asp:Label ID="Label40" CssClass="textoemp espacio" runat="server"><b>$3,108</b></asp:Label><br />
                                <asp:Label ID="Label41" CssClass="textoemp espacio" runat="server">Dólares/año</asp:Label><br />
                                 <asp:Label ID="Label42" CssClass="textoemp espacio" runat="server"><b>$2,849</b></asp:Label><br />
                                 <asp:Label ID="Label43" CssClass="textoemp espacio" runat="server">Dólares/Pago anual</asp:Label><br />
                                
                                <br />
                                <asp:Button ID="btnCorporativo" CssClass="btnSeleccionarPlan espacio" OnClientClick="Corporativo();" runat="server" Text="Comienza ahora" />
                                <br />
                            </div>
                             <div id="PlanPremium" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <asp:Label ID="Label44" CssClass="tituloPlan espacio" runat="server">Premium</asp:Label><br />
                                 <asp:Label ID="Label45" CssClass="textoemp espacio" runat="server"><b>250</b> Usuarios</asp:Label><br />
                                 <asp:Label ID="Label46" CssClass="textoemp espacio" runat="server"><b>1400</b> Archivos</asp:Label><br />
                                 <asp:Label ID="Label47" CssClass="textoemp espacio" runat="server"><b>7</b> Gigas</asp:Label><br />
                                <br />
                                <asp:Label ID="Label48" CssClass="textoemp espacio" runat="server"><b>$599</b></asp:Label><br />
                                <asp:Label ID="Label49" CssClass="textoemp espacio" runat="server">Dólares/mes</asp:Label><br />
                                <asp:Label ID="Label50" CssClass="textoemp espacio" runat="server"><b>$7,188</b></asp:Label><br />
                                <asp:Label ID="Label51" CssClass="textoemp espacio" runat="server">Dólares/año</asp:Label><br />
                                 <asp:Label ID="Label52" CssClass="textoemp espacio" runat="server"><b>$6,589</b></asp:Label><br />
                                 <asp:Label ID="Label53" CssClass="textoemp espacio" runat="server">Dólares/Pago anual</asp:Label><br />
                                
                                <br />
                                <asp:Button ID="btnPremium" CssClass="btnSeleccionarPlan espacio" OnClientClick="Premium();" runat="server" Text="Comienza ahora" />
                                <br />
                            </div>
                        </div>
                            
                </div>
                       
                </panel>

                <asp:Label ID="Label15" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppRegistroSeleccion" CancelControlID="btnCancelarPosRegistro" TargetControlID="Label15" runat="server" PopupControlID="pnlRegistroSeleccion" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                </Ajax:ModalPopupExtender>
                
                <%-- ------------------------------------------------------------------------------- --%>
                <%-- PopUp Pagos --%>
                 <panel id="pnlDatosPago" runat="server" style="display: none">
                    <div id="DatosPago" class="divTabla" style="height: 584px; width: 825px; background: url('Images/Site/PlantillaRegistro.png');">                        
                         
                        <asp:Button ID="Button5" CssClass="btnCancelar" runat="server" Text="X" />
                        <br />
                        <asp:Label ID="Label54" CssClass="tituloPosRegistro" runat="server">Una herramienta</asp:Label><br />
                        <asp:Label ID="Label55" CssClass="tituloPosRegistro" runat="server">de hoy</asp:Label>
                        <asp:Label ID="Label56" CssClass="tituloPosRegistroRosa" runat="server">para el futuro</asp:Label>
                        <br />
                        <div class="columna1Pagos">
                        <asp:Label cssClass="lblTituloDatosF"  runat="server">Datos de facturación</asp:Label><br /><br />
                        <div class="checkbox-group">          
                            <input type="checkbox" id="c1" name="c1" onclick="checkpersona();"  runat="server" />       
                            <label for="c1" ><span ></span></label>
                         </div>
                        <br />
                        <div class="checkbox-group">          
                            <input type="checkbox" id="c2" name="c2" onclick="checkempresa();" runat="server" />       
                            <label for="c2"><span></span></label>
                         </div>
                        <label class="lblPersona" style="position:absolute;">Persona</label>
                        <label class="lblEmpresa" style="position:absolute;">Empresa</label>
                            <br />
                        <input id="txtRFCdf" type="text" runat="server" class="txtDatosPagoG" placeholder="   RFC *"/><br /><br />
                        <input id="txtNombresdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Nombres *"/>
                        <input id="txtApellidosdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Apellidos *"/>
                            <br /><br />
                        <input id="txtCalledf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Calle *"/>
                        <input id="txtNoExtdf" type="text" runat="server" class="txtDatosPagoCh" placeholder="   No. Ext. *"/>
                        <input id="txtNoIntdf" type="text" runat="server" class="txtDatosPagoCh" placeholder="   No. Int. *"/>
                            <br /><br />
                        <input id="txtColoniadf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Colonia*"/>
                        <input id="txtDelegaciondf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Delegación/Municipio *"/>
                            <br /><br />
                        <input id="txtEstadodf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Estado *"/>
                        <input id="txtCPdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Código Postal *"/>
                            <br /><br />
                        <input id="txtPaisdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   País *"/><br /><br />
                        <input id="txtEmaildf" type="text" runat="server" class="txtDatosPagoG" placeholder="   Email *"/><br />

                        <label class="lblDatosObligatorios">Información requerida *</label>
                        </div> 


                        <div class="columna2Pagos">
                            <asp:Label cssClass="lblTituloDatosF"  runat="server">Plan elegido</asp:Label><br /><br />
                            <asp:Label ID="NombrePlan" CssClass="lblPlan" runat="server" Text="PLAN"></asp:Label>
                            <asp:Label ID="CantidadTotal" CssClass="lblCantidad" runat="server" Text="$0.00"></asp:Label>
                            <asp:Label CssClass="lblTotal" runat="server" Text="Total:"></asp:Label>

                            <br /><br />
                            <div class="checkbox-group">          
                            <input type="checkbox" id="c3" name="c3" onclick="checkmes();" runat="server"/>       
                            <label for="c3"><span></span></label>
                            </div>
                            <asp:Label ID="lblmes" CssClass="lblCantidades" runat="server" Text="$0.00"></asp:Label>
                            <asp:Label ID="Label57" CssClass="lblPeriodo" runat="server" Text="Dólares/mes"></asp:Label>

                            <div id="pagoanual" class="checkbox-group">          
                            <input type="checkbox" id="c4" name="c4" onclick="checkanual();" runat="server"/>       
                            <label for="c4"><span></span></label>
                            </div>
                            <asp:Label ID="lblanual" CssClass="lblCantidadesA" runat="server" Text="$0.00"></asp:Label>
                            <asp:Label ID="Label59" CssClass="lblPeriodoA" runat="server" Text="Dólares/Pago anual"></asp:Label>
                            <br /><br />
                            <asp:Label ID="lblFormaDePago" runat="server" Text="Forma de pago"></asp:Label>
                            <br /><br />

                           

                            <input type="text" class="txtDatosPagoG" data-conekta="card[number]" placeholder="   Número de tarjeta *"/>
                            <br /><br />
                            <input type="text" class="txtDatosPagoG" data-conekta="card[name]" placeholder="   Nombre (como aparece en la tarjeta) *"/>
                            <br /><br />
                            <select class="ddlVencimiento" data-conekta="card[exp_month]">
                              <option value="">  Mes de Vencimiento</option>
                              <option value=1>   Enero (01)</option>
                              <option value=2>   Febrero (02)</option>
                              <option value=3>   Marzo (03)</option>
                              <option value=4>   Abril (04)</option>
                              <option value=5>   Mayo (05)</option>
                              <option value=6>   Junio (06)</option>
                              <option value=7>   Julio (07)</option>
                              <option value=8>   Agosto (08)</option>
                              <option value=9>   Septiembre (09)</option>
                              <option value=10>   Octubre (10)</option>
                              <option value=11>   Noviembre (11)</option>
                              <option value=12>   Diciembre (12)</option>
                            </select>
                            <select class="ddlVencimiento" data-conekta="card[exp_year]">
                              <option value="">  Año de Vencimiento</option>
                              <option value=2016>   16</option>
                              <option value=2017>   17</option>
                              <option value=2018>   18</option>
                              <option value=2019>   19</option>
                              <option value=2020>   20</option>
                              <option value=2021>   21</option>
                              <option value=2022>   22</option>
                              <option value=2023>   23</option>
                              <option value=2024>   24</option>
                              <option value=2025>   25</option>
                              
                            </select>
                            <br /><br />
                            <input type="text" class="txtDatosPagoM" data-conekta="card[cvc]" placeholder="   Código de seguridad *"/>
                            <br /><br />
                            <button id="btnEnviarPago" type="submit" class="colorRosaPago" onclick="envia()">Enviar Pago</button><br />
                            <asp:Label ID="lblErrorPagos" runat="server" Visible="false"></asp:Label>
                            
                        </div>
                </div>
                       
                </panel>

                <asp:Label ID="Label121" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppPagos" CancelControlID="Button5" TargetControlID="Label121" runat="server" PopupControlID="pnlDatosPago" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                </Ajax:ModalPopupExtender>
                
                <%-- ------------------------------------------------------------------------------- --%>

<%--            </ContentTemplate>
        </asp:UpdatePanel>--%>

                    </ContentTemplate>
        
    </asp:UpdatePanel>

   </form>


        
    <script src="JS/Site.js" type="text/javascript"></script>
    <script src="JS/jquery-1.12.3.js" type="text/javascript"></script>
    <!-- Incluir Conekta.js -->
  <script type="text/javascript" data-conekta-public-key="key_KJysdbf6PotS2ut2"
    src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>

    <script type="text/javascript">
        window.onbeforeunload = closeIt;
        onLoad(document.getElementById('lkbIniciarSesion'));
        /*funcion para la validación  */
        function isValidEmail(mail) {
            var Correo = $(mail).val();
            var nombre = $("#txtNombre").val();
            var mail = $("#txtCorreo").val();
            var mensaje = $("#txtComentarios").val();            
            var band = (/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail));         
            if (nombre == "" || mensaje == "" || band === false)
            {                
                $("#lblmessage").show();
            }
            else
            {
                __doPostBack("validaEnviar", "");
            }            
        }
        /*funciones para solo elegir un checkbox en popup de pago*/
        function checkpersona() {
            document.getElementById("c2").checked = false;
        }
        function checkempresa() {
            document.getElementById("c1").checked = false;
        }
        function checksvacios() {
            if 
            (document.getElementById("CantidadTotal").innerHTML=="$0.00")
            {
                document.getElementById("c3").checked = false;
                document.getElementById("c4").checked = false;
            }
        }
        function checkmes() {
            var cant
            document.getElementById("c4").checked = false;            
            cant = document.getElementById("lblmes").innerHTML;
            document.getElementById("CantidadTotal").innerHTML = cant;
        }
        function checkanual() {
            var cant
            document.getElementById("c3").checked = false;
            cant = document.getElementById("lblanual").innerHTML;
            document.getElementById("CantidadTotal").innerHTML = cant;
        }

        function PYME() {
            __doPostBack("mPYME", "");
        }
        function Empresa() {
            __doPostBack("mEmpresa", "");
        }
        function Corporativo() {
            __doPostBack("mCorporativo", "");
        }
        function Premium() {
            __doPostBack("mPremium", "");
        }
        function Comienza1() {
            __doPostBack("mComienza1", "");
        }
        function Comienza2() {
            __doPostBack("mComienza2", "");
        }
        
    </script>
    <%-- Funciones para visualizar los controles del posRegistro --%>
    <script  >
        /* funciones para ocultar y mostrar los diferentes planes en el popup de RegistroSelección */
        function muestraEmpresa(){            
            $(".RegistroEmpresa").css("visibility", "visible");
            $(".RegistroDisenador").css("visibility", "hidden");
        }
        function muestraDiseno() {            
                $(".RegistroEmpresa").css("visibility", "hidden");
                $(".RegistroDisenador").css("visibility", "visible");               
        }

        function envia() {
            document.getElementById("form1").submit;
            // alert("funciona!!");
        }
        
     </script>

    <!-- Funciones para pagos con conekta-->
    <script>
       

        jQuery(function ($) {
            //var TipoPersona = document.getElementById("c1").value;
            //var Tipoempresa = document.getElementById("c2").value;
            //var RFC = document.getElementById("NombrePlan").value;
            //var Nombres = document.getElementById("NombrePlan").value;
            //var Apellidos = document.getElementById("NombrePlan").innerHTML;
            //var Calle = document.getElementById("NombrePlan").innerHTML;
            //var NoExt = document.getElementById("NombrePlan").innerHTML;
            //var NoInt = document.getElementById("NombrePlan").innerHTML;
            //var Colonia = document.getElementById("NombrePlan").innerHTML;
            //var Delegacion = document.getElementById("NombrePlan").innerHTML;
            //var Estado = document.getElementById("NombrePlan").innerHTML;
            //var CP = document.getElementById("NombrePlan").innerHTML;
            //var Pais = document.getElementById("NombrePlan").innerHTML;
            //var Email = document.getElementById("NombrePlan").innerHTML;
            //alert();

            var conektaSuccessResponseHandler = function (token)
            {
                var description = document.getElementById("NombrePlan").innerHTML;
                var amount = (((document.getElementById("CantidadTotal").innerHTML).replace(",", "")).replace(".", "")).replace("$", "");
                var currency = "USD"
                var name = document.getElementById("txtNombreRegistro").value;
                name = name+ " " + document.getElementById("txtApellidoRegistro").value;
                var phone = "0000000000"
                var email = document.getElementById("txtCorreoRegistro").value;
                var nombreP = document.getElementById("NombrePlan").innerHTML;
                var unit_price = amount;
                var quantity = 1;

                var $form = $('#form1');
                var card_token = token.id;
                // Insert the token_id into the form
                $form.append($('<input type="hidden" name="card_token"/>').val(card_token));
                // Submit the form data to the server
                //$form.get(0).submit();
                console.log(card_token);
                var sjson = "{\"description\": \" " + description + " \",\"amount\": " + amount + ",\"currency\":\"USD\",\"card\": \"" + card_token + "\",\"details\": {\"name\": \"" + name + "\",\"phone\": \""+ phone+"\",\"email\": \""+ email+"\",\"line_items\": [{\"name\": \""+ nombreP+"\",\"description\": \"Plan: "+ nombreP+"\",\"unit_price\": "+amount+",\"quantity\": 1}] } }";
                __doPostBack('Pagar_Click', sjson);
            };

            var conektaErrorResponseHandler = function (response)
            {
                var $form = $('#form1');
                // Show the errors on the form
                $form.find('.payment-errors').text(response.message);
                $form.find('button').prop('disabled', false);
                console.log(response);
            };

            $('#form1').submit(function (event)
            {
                var $form = $(this);
                // Disable the submit button to prevent repeated clicks
                $form.find('button').prop('disabled', true);
                
                Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                
                // Prevent the form from submitting with the default action
                return false;
            });

        });

  </script>

</body>
</html>
