﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Identidad.App_Code;
using System.Data;
using System.Data.SqlClient;

namespace Identidad.Designer
{
    class cEditorManual
    {
        //Instancias de clase 
        cSistema oSistema = new cSistema();



        #region validaciones
        //Valida que los campos no esten vacios.
        private bool validatexto(params string[] text) //recibe un arreglo de datos y si encuentra uno vacio regresa false.
        {

            for (int i = 0; i < text.Length; i++)
                if (text[i] == "" || text[i] == null)
                    return false;

            return true;
        }



        #endregion

        #region operaciones y otros metodos que interactuan con la pagina

        public string creaNodos(string sIdBrandSite)
        {

            DataTable dtNodos = ObtenElementosIndice(sIdBrandSite);

            if(dtNodos!=null){

            StringBuilder sbMenu = new StringBuilder();

            sbMenu.Append("<div class='dd' id='nestable3'>");
            

            string sInicioLista = "<ol class='dd-list'>";
            string sFinLista = "</ol>";
            string sItem = "<li class='dd-item dd3-item' data-id=@IDNODO>";
            string sFinItem = "</li>";
            string sElemento = "<div class='dd-handle dd3-handle handle-blanco'>Drag</div><div class='dd3-content'><span>@NombreItem</span></div>";

            sbMenu.Append(sInicioLista);
            int band=0,band3=0;

            for (int i = 0; i< dtNodos.Rows.Count ; i++){

                band = 0;
                band3 = 0;

               if (int.Parse(dtNodos.Rows[i]["idIndicePadre"].ToString()) == 0)
                {
                    band= int.Parse(dtNodos.Rows[i]["idIndice"].ToString());


                    sbMenu.Append(sItem).Replace("@IDNODO", dtNodos.Rows[i]["idIndice"].ToString());
                    sbMenu.Append(sElemento).Replace("@NombreItem", dtNodos.Rows[i]["Nombre"].ToString());
                          

                    for (int j = i+1; j < dtNodos.Rows.Count ; j++) {


                        if (int.Parse(dtNodos.Rows[j]["idIndicePadre"].ToString()) == band)
                        {
                            if (band3 == 0)
                            {
                                sbMenu.Append(sInicioLista);
                                band3 = 1;
                            }

                                sbMenu.Append(sItem).Replace("@IDNODO", dtNodos.Rows[j]["idIndice"].ToString());
                                sbMenu.Append(sElemento).Replace("@NombreItem", dtNodos.Rows[j]["Nombre"].ToString());
                                sbMenu.Append(sFinItem);
                            i++;
                        }
                        else {
                            if (band3 != 0)
                            {
                                sbMenu.Append(sFinLista);
                                break;
                            }
                           

                        }

                       
                      
                        
                    }

                   sbMenu.Append(sFinItem);
                  

                }
                    
              
            
                

    
                }
                 



            sbMenu.Append(sFinLista); ///ol;
            sbMenu.Append("</div>");//Cierra el div nestable





            string s = sbMenu.ToString();
            return s;





            /*Estructura de los nodos.*/


            //<div class="dd" id="nestable3">
            //               <ol class="dd-list">
            //                    <li class="dd-item dd3-item" data-id="13">
            //                        <div class="dd-handle dd3-handle">Drag</div>
            //                        <div class="dd3-content">Item 13</div>
            //                    </li>
            //                    <li class="dd-item dd3-item" data-id="14">
            //                        <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 14</div>
            //                    </li>
            //                    <li class="dd-item dd3-item" data-id="20">
            //                        <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 14</div>
            //                    </li>
            //                    <li class="dd-item dd3-item" data-id="21">
            //                        <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 14</div>
            //                    </li>
            //                    <li class="dd-item dd3-item" data-id="15">
            //                        <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 15</div>
            //                             <ol class="dd-list">
            //                                 <li class="dd-item dd3-item" data-id="16">
            //                                 <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 16</div>
            //                                 </li>
            //                                 <li class="dd-item dd3-item" data-id="17">
            //                                 <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 17</div>
            //                                 </li>
            //                                 <li class="dd-item dd3-item" data-id="18">
            //                                 <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content">Item 18</div>
            //                                 </li>
            //                             </ol>
            //                    </li>
            //              </ol>
            //    </div>
            }
            return "Aún no hay secciones en el menu";
        } // Crea nodos para un indice

        public void EnviaInformacionBD(Menu[] mMenu,string idBrandiSite)
        {

            int icount = 0;
           
          
            DataTable dtDatos = new DataTable();
            dtDatos.Columns.Add("Id");
            dtDatos.Columns.Add("Padre");
            dtDatos.Columns.Add("Orden");
            DataRow row;


            for (int i = 0; i < mMenu.Count(); i++)
            {

                row = dtDatos.NewRow();
                row["Id"] = mMenu[i].ID;
                row["Padre"] = 0;
                row["Orden"] = icount;
                dtDatos.Rows.Add(row);
                icount++;

                if (mMenu[i].children != null)
                {
                    for (int j = 0; j < mMenu[i].children.Count(); j++) {
                        row = dtDatos.NewRow();
                        row["Id"] = mMenu[i].children[j].ID;
                        row["Padre"] = mMenu[i].ID;
                        row["Orden"] = icount;
                        dtDatos.Rows.Add(row);
                        icount++;

                    }
                }
            }

            //Llama al metodo que va enviar los datos a la BD
            GuardaOrdenIndice(dtDatos,idBrandiSite);

        } // Manda la inf. del menú del brand site

        public DataTable ObtenerPadresBD(string sIdBrandSite) {

            return ObtenerPadres(sIdBrandSite);
       
        } // Obtiene padres y los pone en un datatable

        public void GuardarItems(string sNombreItem,string sPadre,string sUsuario,string sIdBrandSite) {

            GuardarItemsPadre(sNombreItem, sPadre, sUsuario, sIdBrandSite);

        }

        public void ModificaNombreNodo(string  sNombreNodo, string iIndiceNodo) //llama el metodo que interactua con la BD por que es private
        {

            ModificaIndice(iIndiceNodo, sNombreNodo);


        }

        public void EliminarItemSeleccionado(string idIndiceSeleccionado,string sUsuario) {


            EliminarIndice(idIndiceSeleccionado, sUsuario);
        
        } // eliminar un item del indice
        
        public void ActualizaContenido(string sHTMl,string sTitulo,string sTexto,string sTexto2,string sPie,string sPie2,string sImg,string sImg2,string idIndice){
        
        ModificaPantilla(sHTMl,sTitulo,sTexto,sTexto2,sPie,sPie2, sImg,sImg2, idIndice);
        
        } // actualiza una plantilla o seccion del indice

        

        #endregion


        #region ObtenerDatos

        //Obtener el indice del manual o el Menu.
        private DataTable ObtenElementosIndice(string sIdBrandSite)
        {

            if (validatexto(sIdBrandSite)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selManualIndices";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    return dt;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        }

        private DataTable ObtenerPadres(string sIdBrandSite) {

            if (validatexto(sIdBrandSite)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManual_Indices";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    return dt;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        
        }
        //Obtiene el Nombre del elemento y sus datos para mostrar el detalle
        public DataTable ObtenerDetalle(string sIdIndice) {

            if (validatexto(sIdIndice)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManualInformacion";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndice));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    return dt;

                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }
        
        }

        public void GuardarItemsPadre(string sNombreItem,string sPadre,string sUsuario,string sIdBrandSite)
        {

            if (validatexto(sNombreItem,sPadre,sUsuario)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualGuardarItem";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@NombreIndice",sNombreItem));
                    cmd.Parameters.Add(new SqlParameter("@Padre",sPadre));
                    cmd.Parameters.Add(new SqlParameter("@Usuario",sUsuario));
                    cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    //return dt;

                }
                else
                {
                    //return null;
                }

            }
            else
            {
                //return null;
            }

        } // Guarda los items de un indice del manual

        public void EliminarIndice(string sIdIndiceSeleccionado, string sUsuario)
        {


            if (validatexto(sIdIndiceSeleccionado)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManual_Eliminar";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndiceSeleccionado));
                    cmd.Parameters.Add(new SqlParameter("@IdUsuario", sUsuario));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    //return dt;

                }
                else
                {
                    //return null;
                }

            }
            else
            {
                //return null;
            }
        
        } // eliminar indice del manual

        public int ObtenPrimerElento(string sIdBrandSite) {

            if (validatexto(sIdBrandSite)) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManualPrimerIndice";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                    if (dt.Rows.Count!= 0)
                        return int.Parse(dt.Rows[0][0].ToString());
                    else
                        return 0;
                    //return dt;

                }
                else
                {
                   return 0;
                }

            }
            else
            {
                return 0;
            }
        } // obtiene el primer item del manual

        public int ObtenerIdPlantilla(string sIdBrand,string nombreSeccion)
        {
            int idPlantilla = 0;
            SqlConnection cnn = oSistema.connectDataBase();
            //SqlDataReader dr;
            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "Select IdPlantilla from catIndiceManual where idBrandSite = "+int.Parse(sIdBrand)+" and Nombre = '"+nombreSeccion+"' and Activo = 'true';";
                cmd.CommandType = CommandType.Text;
                cmd.Connection = cnn;
                //dr = cmd.ExecuteScalar();

                idPlantilla = Convert.ToInt32(cmd.ExecuteScalar());
                cnn.Close();
                return idPlantilla;
            }
            else
            {
                return -1;
            }
        }


        #endregion

      

        #region GuardarDatos

        private void GuardaOrdenIndice(DataTable dtDatos, string sIdBrandSite)
        {



            if (dtDatos.Rows.Count>0) //valida que las variables no vengan vacias
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                
                      
                    for (int i = 0; i < dtDatos.Rows.Count; i++)
                    {

                        if (cnn.State.ToString() == "Closed")
                            cnn.Open();

                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.CommandText = "traIndiceManualActualizaOrdenPadre";
                        cmd.CommandType = CommandType.StoredProcedure;
                    
                        cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
                        cmd.Parameters.Add(new SqlParameter("@IdIndice", dtDatos.Rows[i][0].ToString()));
                        cmd.Parameters.Add(new SqlParameter("@IdIndicePadre", dtDatos.Rows[i][1].ToString()));
                        cmd.Parameters.Add(new SqlParameter("@Orden", dtDatos.Rows[i][2].ToString()));
                        cmd.ExecuteNonQuery();
                        //DataTable dt = oSistema.CargarDataTable(cmd);
                        cnn.Close();
                        
                    }
                }
                else
                {
                    
                }

            }
            else
            {
               
            }
        
        } //guarda cada item del indice


        #endregion

        #region ModificarDatos

        private void ModificaIndice(string iIdIndice, string sNombreIndice) {

            if (validatexto(iIdIndice, sNombreIndice))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualModificaNombreIndice";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", iIdIndice));
                    cmd.Parameters.Add(new SqlParameter("@NombreNodo", sNombreIndice));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                }
                else
                {
                    
                }
            }
            else
            {
               
            }
        
        } // actualiza el indice


        private void ModificaPantilla(string sHTMl, string sTitulo, string sTexto, string sTexto2, string sPie, string sPie2, string sImg, string sImg2, string idIndice)
        {

            if (validatexto(sHTMl))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualActualizarHTML";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@HTML", sHTMl));
                    cmd.Parameters.Add(new SqlParameter("@Titulo", sTitulo));
                    cmd.Parameters.Add(new SqlParameter("@Texto", sTexto));
                    cmd.Parameters.Add(new SqlParameter("@Texto2", sTexto2));
                    cmd.Parameters.Add(new SqlParameter("@Pie", sPie));
                    cmd.Parameters.Add(new SqlParameter("@Pie2", sPie2));
                    cmd.Parameters.Add(new SqlParameter("@Img", sImg));
                    cmd.Parameters.Add(new SqlParameter("@Img2", sImg2));
                    cmd.Parameters.Add(new SqlParameter("@idIndiceManual",int.Parse(idIndice)));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                }
                else
                {

                }
            }
            else
            {

            }

        } // actualiza la plantilla de una seccion del indice


        #endregion




    }
}
