﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Identidad.App_Code
{
    public class cCarpeta
    {
        public int IdCarpeta { get; set; }
        public int IdCarpetaPadre { get; set; }
        public int IdBrandSite { get; set; }
        public string sNombre { get; set; }
        public bool CarpetaTrabajo { get; set; }
        public bool bActivo { get; set; }
        public DateTime dtFechaActivo { get; set; }
        private cMetodo oMetodo;
        private cSistema oSistema = new cSistema();

        public cCarpeta()
        {
            IdCarpeta = 0;
            IdCarpetaPadre = 0;
            IdBrandSite = 0;
            sNombre = string.Empty;
            bActivo = true;
            dtFechaActivo = DateTime.Now;   
        }

        public cMetodo AgregarCarpeta()
        {
            oMetodo = ValidarCarpeta(false, false, true);

            if (oMetodo.Pass == false)
                return oMetodo;

            cStoredProcedure oBd = new cStoredProcedure("catCarpeta_Insert");
            oBd.agregarParametro("@IdCarpetaPadre", this.IdCarpetaPadre);
            oBd.agregarParametro("@IdBrandSite", this.IdBrandSite);
            oBd.agregarParametro("@Nombre", this.sNombre);
            oBd.agregarParametro("@CarpetaTrabajo", this.CarpetaTrabajo);

            SqlDataReader dr = oBd.executeReader();

            dr.Read();

            oMetodo = new cMetodo(dr.GetBoolean(0), dr.GetString(1), dr.GetInt32(2));

            oBd.cerrarBaseDatos();

            return oMetodo;
        } // Agregar una carpeta al brand site

        public cMetodo ModificarCarpeta()
        {
            cStoredProcedure oBd = new cStoredProcedure("catCarpeta_Update");
            oBd.agregarParametro("@IdCarpeta", this.IdCarpeta);
            oBd.agregarParametro("@IdCarpetaPadre", this.IdCarpetaPadre);
            oBd.agregarParametro("@Nombre", this.sNombre);
            oBd.agregarParametro("@CarpetaTrabajo", this.CarpetaTrabajo);

            SqlDataReader dr = oBd.executeReader();

            dr.Read();

            oMetodo = new cMetodo(dr.GetBoolean(0), dr.GetString(1), dr.GetInt32(2));

            oBd.cerrarBaseDatos();

            return oMetodo;
        } // cambiar el nombre de la carpeta

        public cMetodo EliminarCarpeta()
        {
            //eliminacion de los archivos
            cStoredProcedure cmd = new cStoredProcedure("catArchivos_Delete");
            cmd.agregarParametro("@IdCarpeta", this.IdCarpeta);

            SqlDataReader d = cmd.executeReader();
            cmd.cerrarBaseDatos();

            //eliminacion de la carpeta
            cStoredProcedure oBd = new cStoredProcedure("catCarpeta_Delete");
            oBd.agregarParametro("@IdCarpeta", this.IdCarpeta);

            SqlDataReader dr = oBd.executeReader();

            dr.Read();

            oMetodo = new cMetodo(dr.GetBoolean(0), dr.GetString(1), dr.GetInt32(2));

            oBd.cerrarBaseDatos();

            return oMetodo;
        } // eliminar una carpeta

        public void Consultar(GridView gv, int IdCarpeta = 0)
        {
            SqlCommand cmd = new SqlCommand("catCarpetas_select");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdCarpeta", IdCarpeta);
            oSistema.CargarGridView(gv, cmd);
        } // metodo que carga todas las carpetas y si le pasas un id carga las carpetas hijas que contenga

        public cMetodo ValidarCarpeta(bool bIdCarpeta, bool bIdCarpetaPadre, bool bNombre)
        {
            oMetodo = new cMetodo();

            if(bIdCarpeta == true)
            {
                if (this.IdCarpeta == 0)
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Id de carpeta inválido.";
                    return oMetodo;
                }
            }

            if(bIdCarpetaPadre == true)
            {
                if (this.IdCarpetaPadre == 0)
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Id de carpeta padre inválido.";
                    return oMetodo;
                }
            }

            if(bNombre == true)
            {
                if (oSistema.ValidarVacioLenght(this.sNombre) == false)
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Escribe un nombre válido.";
                    return oMetodo;
                }
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Ok";
            return oMetodo;
        } // valida los datos de la carpeta

        public cMetodo ActualizarCarpetaDeArchivo(int IdArchivo, int IdCarpeta)
        {
            cStoredProcedure oBd = new cStoredProcedure("catArchivo_CambiarCarpeta");

            oBd.agregarParametro("@IdArchivo", IdArchivo);
            oBd.agregarParametro("@IdCarpeta", IdCarpeta);

            SqlDataReader dr = oBd.executeReader();

            dr.Read();

            oMetodo = new cMetodo(dr.GetBoolean(0), dr.GetString(1), dr.GetString(2));

            oBd.cerrarBaseDatos();

            return oMetodo;
        } // cambiar de lugar a un archivo a otra carpeta

        public cMetodo ActualizarCarpetaDeCarpeta(int IdCarpetaAMover, int IdCarpeta)
        {
            cStoredProcedure oBd = new cStoredProcedure("catCarpeta_CambiarCarpeta");

            oBd.agregarParametro("@IdCarpetaAMover", IdCarpetaAMover);
            oBd.agregarParametro("@IdCarpeta", IdCarpeta);

            SqlDataReader dr = oBd.executeReader();

            dr.Read();

            oMetodo = new cMetodo(dr.GetBoolean(0), dr.GetString(1), dr.GetInt32(2));

            oBd.cerrarBaseDatos();

            return oMetodo;
        } // cambiar de lugar una carpeta a otra carpeta

        public string ObtenerCarpetasDeArchivo(int IdArchivo)
        {
            cStoredProcedure oBd = new cStoredProcedure("selCarpetas_Arbol");

            oBd.agregarParametro("@IdArchivo", IdArchivo);

            return oBd.executeScalar().ToString();
        } // obtiene la carpeta padre de un archivo
    }
}