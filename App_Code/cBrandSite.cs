﻿// Estos son mis using, aquí importo las bibliotecas que voy a usar

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

// Este es mi namespace, un namespace en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos.

namespace Identidad.App_Code
{
    // Esta es mi clase cBrandSite, La clase es la construcción del lenguaje utilizada más frecuentemente para definir los tipos abstractos de datos en lenguajes de programación orientados a objetos. Generalmente, una clase se puede definir como una descripción abstracta de un grupo de objetos, cada uno de los cuales se diferencia por un estado específico y es capaz de realizar una serie de operaciones.

    public class cBrandSite
    {
        // Variables utilizadas en la clase

        #region Variables

        cSistema oSistema = new cSistema(); // Referencia a clase cSistema
        cMetodo oMetodo = new cMetodo(); // Referencia a clase cMetodo
        cAmazon oAmazon = new cAmazon(); // Referencia a clase cAmazon

        #endregion

        // Funciones relacionados al catálogo del brand site dentro de la sección del diseñador

        #region Catálogo de BrandSites

        public string ConvertirPeso(float fPeso)
        {
            if(fPeso > 1073741824)
            {
                return Math.Round((fPeso/1073741824), 2).ToString() + " Gb.";
            }
            
            if(fPeso > 1048576)
            {
                return Math.Round((fPeso/1048576), 2).ToString() + " Mb.";
            }
            
            if(fPeso > 1024)
            {
                return Math.Round((fPeso/1024), 2).ToString() + " Kb.";
            }
            
            if(fPeso > 1)
            {
                return Math.Round((fPeso/1), 2).ToString() + " Byte";
            }

            return fPeso.ToString();
        } // Este método convierte el peso de Byte a la unidad más facil de leer para el usuario ya sea kb, mb o gb, lo regresa en un string.

        public void CargarEstatusBS(DropDownList ddl)
        {
            SqlCommand cmd = new SqlCommand("EXEC selEstatus");

            oSistema.CargarDropDownList(ddl, cmd);
        } // Cargar los posibles estatus (por ejemplo Activado, desactivado) de un brandSite, todo esto en un dropdownlist

        public cMetodo ActualizarBrandSite(String sIdBrandSite, String sNombre, String sUrl, String sIdEstatus, String sIdUsuario, String sPlan)
        {
            oMetodo = ValidarBrandSite(sIdBrandSite, sNombre, sUrl, sIdEstatus, sIdUsuario);

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                int id;
                //SqlDataReader dr;
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("EXEC traBrandSites @IdBrandSite, @Nombre, @Url, @IdUsuario, @IdEstatus, @Plan");


                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@Url", sUrl.Trim().ToLower());
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdEstatus", sIdEstatus.Trim());
                cmd.Parameters.AddWithValue("@Plan", int.Parse(sPlan));
                cmd.Connection = cnn;

                //dr = cmd.ExecuteReader();
                //dt.Load(dr);

                //id = int.Parse(dt.Rows[dt.Rows.Count-1].ToString());
                id = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Brand Site actualizado";
                oMetodo.oDato = id;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Actualiza los datos de un BrandSite en la base de datos y regresa true si se logra exitosamente



        public cMetodo CrearBrandsite1(String sIdBrandSite, String sNombre, String sUrl, String sIdEstatus, String sIdUsuario, String sPlan)
        {
            oMetodo = ValidarBrandSite(sIdBrandSite, sNombre, sUrl, sIdEstatus, sIdUsuario);

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                int id;
                //SqlDataReader dr;
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("EXEC traBrandSites_examp @IdBrandSite, @Nombre, @Url, @IdUsuario, @IdEstatus, @Plan");


                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@Url", sUrl.Trim().ToLower());
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdEstatus", sIdEstatus.Trim());
                cmd.Parameters.AddWithValue("@Plan", int.Parse(sPlan));
                cmd.Connection = cnn;

                //dr = cmd.ExecuteReader();
                //dt.Load(dr);

                //id = int.Parse(dt.Rows[dt.Rows.Count-1].ToString());
                id = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Brand Site actualizado";
                oMetodo.oDato = id;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Actualiza los datos de un BrandSite en la base de datos y regresa true si se logra exitosamente

        private cMetodo ValidarBrandSite(String sIdBrandSite, String sNombre, String sUrl, String sIdEstatus, String sIdUsuario)
        {
            if (oSistema.ValidarVacio(sIdBrandSite) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un brand site";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre";
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Se supero el límite de caracteres permitidos para el nombre";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sUrl) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe una url";
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sUrl) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe una url más corta";
                return oMetodo;
            }

            if (sUrl.Contains(" ") == true)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Url inválida";
                return oMetodo;
            }

            if (sUrl == "identidad")
            {
                oMetodo.Pass = false;
                oMetodo.Message = "No uses la palabra identidad";
                return oMetodo;
            }

            if (sUrl.Contains("http://") == true)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "No es necesario especificar http://";
                return oMetodo;
            }

            if (sUrl.Contains("http") == true)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "No es necesario especificar http";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdEstatus) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un estatus";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdUsuario) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Usuario no válido, por favor inicia sesión una vez más.";
                return oMetodo;
            }

            oMetodo = ValidarExistenciaUrl(sIdBrandSite, sUrl);

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Ok";
            return oMetodo;
        } // Validamos que los datos introducidos sean validos, regresa false en caso de que no lo sean

        private cMetodo ValidarExistenciaUrl(String sIdBrandSite, String sUrl)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("selBrandSites_UrlValida @IdBrandSite, @Url");

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                cmd.Parameters.AddWithValue("@Url", sUrl.Trim());
                
                cmd.Connection = cnn;

                String sUrlValida = cmd.ExecuteScalar().ToString();

                cnn.Close();

                if (sUrlValida.Trim() == "1")
                {
                    oMetodo.Pass = true;
                    oMetodo.Message = "Url válida";
                    return oMetodo;
                }
                else
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Esta Url ya existe";
                    return oMetodo;
                }
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Validamos si la URL solicitada no está ocupada, regresa true si esta disponible

        public cMetodo EliminarBrandSite(String sIdBrandSite, String sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                // Elimina archivos de amazon
                EliminarArchivosBrandSite(sIdBrandSite);

                // Elimina la hoja de estilos
                string sCSS = HttpRuntime.AppDomainAppPath + @"CSS\BrandSites\Id" + sIdBrandSite + ".css";

                if (File.Exists(sCSS) == true)
                {
                    File.Delete(sCSS);
                }

                // Actualiza la base de datos
                SqlCommand cmd = new SqlCommand("EXEC traBrandSites_Eliminar @IdBrandSite, @IdUsuario");

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.Connection = cnn;

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Brand Site eliminado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Dar de baja un BrandSite, regresa true si lo pudo eliminar

        private bool EliminarArchivosBrandSite(String sIdBrandSite)
        {
            bool bResultado = true;

            SqlCommand cmd = new SqlCommand("EXEC selArchivos @IdBrandSite");

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            foreach (DataRow row in oSistema.CargarDataTable(cmd).Rows)
            {
                // Archivos
                if (oAmazon.deleteFile(row["Url"].ToString().Replace("https://s3.amazonaws.com/Identidad/", "")) == false)
                {
                    bResultado = false;
                }

                //Previews
                if (oAmazon.deleteFile(row["Preview"].ToString().Replace("https://s3.amazonaws.com/Identidad/", "")) == false)
                {
                    bResultado = false;
                }
            }

            return bResultado;
        } // Con esta función borramos todos los archivos en Amazon S3 de un BrandSite, regresa true si si pudo eliminarlos

        public void CargarMisBrandSites(GridView gv, String sIdUsuario)
        {
            SqlCommand cmd = new SqlCommand("EXEC selBrandSite @IdUsuario");

            cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

            oSistema.CargarGridView(gv, cmd);
        } // Cargamos los BrandSite generados por un usuario en un gridview

        public DataTable cargarDatosBrandSite(String sIdBrandSite)
        {
            int IdBrandSite;

            bool bParsed = int.TryParse(sIdBrandSite, out IdBrandSite);

            if (bParsed == true)
            {
                SqlCommand cmd = new SqlCommand("SELECT Nombre, Url, IdEstatus, [Plan] FROM catBrandSites WHERE IdBrandSite = @IdBrandSite");

                cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite);

                return oSistema.CargarDataTable(cmd);
            }
            else
            {
                return null;
            }
        } // Regresa una tabla con el detalle de un BrandSite y lo regresa en un datatable

        public void CargarMisUsuarios(GridView gv, string sIdUsuario, string sNombre, string sIdBrandSiteBuscado, string order="Nombre")
        {
            SqlCommand cmd = new SqlCommand("selUsuarios_MisUsuarios");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
            cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSiteBuscado));
            cmd.Parameters.AddWithValue("@Order", order);

            oSistema.CargarGridView(gv, cmd);
        } // Carga la lista de usuarios disponibles para asignarles acceso a un brandSite

        public void CargarMisUsuarios2(GridView gv, string sIdUsuario, string sNombre, string sIdBrandSiteBuscado, string order = "Nombre")
        {
            SqlCommand cmd = new SqlCommand("selUsuarios_MisUsuarios2");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
            cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSiteBuscado));
            cmd.Parameters.AddWithValue("@Order", order);

            oSistema.CargarGridView(gv, cmd);
        } // Carga la lista de usuarios disponibles para asignarles acceso a un brandSite



        public void BuscarUsuario(GridView gv, string sIdUsuario, string sNombre, string sIdBrandSiteBuscado, string order = "Nombre")
        {
            SqlCommand cmd = new SqlCommand("selUsuario_Busqueda");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSiteBuscado));

            oSistema.CargarGridView(gv, cmd);
        } // Carga la lista de usuarios disponibles para asignarles acceso a un brandSite

        public void CargarAccesosBrandSite(GridView gv, string sIdBrandSite, string order ="Nombre")
        {
            SqlCommand cmd = new SqlCommand("selUsuariosPorBrandSite_Accesos");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@Order", order);

            oSistema.CargarGridView(gv, cmd);
        } // Carga los usuarios que tienen acceso a un brand site

        public cMetodo InsertarAccesoBrandSite(string sIdUsuario, string sIdTipoUsuario, string sIdBrandSite, string sIdUsuarioActualiza)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorBrandSite", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUxB", 0);
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdTipoUsuario", int.Parse(sIdTipoUsuario));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", int.Parse(sIdUsuarioActualiza));
                cmd.Parameters.AddWithValue("@Activo", true);

                oMetodo.oDato = cmd.ExecuteScalar().ToString();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Acceso registrado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Da de alta a un usuario con acceso a un brandSite

        public cMetodo EdicionInvitadoBrandSite(int IdUxB = 0, int IdUsuario = 0, int IdBrandSite = 0, int IdUsuarioActualiza = 0, bool bPuedeInvitar = false)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorBrandSiteInvitar", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUxB", IdUxB);
                cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite);
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", IdUsuarioActualiza);
                cmd.Parameters.AddWithValue("@PuedeInvitar", bPuedeInvitar);

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Acceso registrado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Da de alta a un usuario con acceso a un brandSite como invitado, o da de baja la posibilidad de invitar a más usuarios

        public cMetodo EdicionEsDeMiEquipo(int IdUxB = 0, int IdUsuario = 0, int IdBrandSite = 0, int IdUsuarioActualiza = 0, bool bEsDeMiEquipo = false)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorBrandSiteEsDeMiEquipo", cnn);

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUxB", IdUxB);
                cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);
                cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite);
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", IdUsuarioActualiza);
                cmd.Parameters.AddWithValue("@EsDeMiEquipo", bEsDeMiEquipo);

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Usuario actualizado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }

        } // Da de alta a un usuario con acceso a un brandSite como invitado, o da de baja la posibilidad de invitar a más usuarios

        public cMetodo EliminarAccesoBrandSite(string sIdUxB, string sIdUsuarioActualiza)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorBrandSite");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdUxB", int.Parse(sIdUxB));
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", int.Parse(sIdUsuarioActualiza));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse("False"));
                cmd.Connection = cnn;

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Acceso eliminado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Elimina el permiso para entrar a un brandSite para un usuario final consumidor de identidad

        public cMetodo ImportarUsuario(string sCorreoElectronico, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorUsuarios");
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CorreoElectronico", sCorreoElectronico.Trim());
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.Connection = cnn;

                string sImportado = cmd.ExecuteScalar().ToString();

                cnn.Close();

                if (sImportado == "1")
                {
                    oMetodo.Pass = true;
                    oMetodo.Message = "Usuario importado";
                    return oMetodo;
                }
                else
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Este usuario no existe";
                    return oMetodo;
                }
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Hubo un problema en la conexión a la base de datos";
                return oMetodo;
            }
        }  // Importar un usuario a mi lista de usuarios para dar accesos

        public int ObtenerIdEstatusBrandSite(string sIdBrandSite)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 IdEstatus FROM catBrandSites WHERE IdBrandSite = @IdBrandSite");

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                cmd.Connection = cnn;

                int IdEstatus = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();

                return IdEstatus;

            }
            else
            {
                return 0;
            }
        } // Obtiene el id del estatus actual del brand site

        public void CargarTiposCuenta(GridView gv, string sIdUsuario)
        {
            SqlCommand cmd = new SqlCommand("selTiposCuenta");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

            oSistema.CargarGridView(gv, cmd);
        } // Carga los tipos de cuenta para un usuario diseñador

        public cMetodo ActualizarTipoCuenta(string sIdUsuario, string sIdTipoCuenta)
        {
            if (oSistema.ValidarVacioLenght(sIdUsuario) == false || oSistema.ValidarVacioLenght(sIdTipoCuenta) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un tipo de cuenta";
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuarios_TipoCuenta", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario) );
                cmd.Parameters.AddWithValue("@IdTipoCuenta", int.Parse(sIdTipoCuenta));
                cmd.ExecuteNonQuery();
                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "¡Tipo de cuenta actualizada!";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "¡Tuvimos un problema en la base de datos!";
                return oMetodo;
            }
        } // Actualiza el tipo de cuenta de un usuario

        public void CargarGrupos(GridView gv, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selGrupos");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            oSistema.CargarGridView(gv, cmd);
        } // Carga los grupos existentes en un brand site

        public cMetodo EdicionGrupo(string sIdGrupo, string sIdBrandSite, string sNombre, string sDescripcion)
        {
            oMetodo = ValidarGrupo(sNombre);

            if(oMetodo.Pass == false)
            {
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traGrupos", cnn);
                
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@Descripcion", sDescripcion.Trim());

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Grupo registrado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Agreg o actualiza un grupo a un brand site, si pasas en el parametro "0" agregas uno nuevo

        public cMetodo ValidarGrupo(string sNombre)
        {
            if(oSistema.ValidarVacioLenght(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre del grupo válido";
                return oMetodo;
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Datos válidos";
            return oMetodo;
        } // valida los datos que recibe el método para editar grupo, como por ejemplo el nombre y su largo del texto

        public DataTable cargarDatosGrupo(string sIdGrupo)
        {
            int IdGrupo;

            bool bParsed = int.TryParse(sIdGrupo, out IdGrupo);

            if (bParsed == true)
            {
                SqlCommand cmd = new SqlCommand("SELECT IdBrandSite, Nombre, Descripcion, Activo, FechaActivo FROM catGrupos WHERE IdGrupo = @IdGrupo");

                cmd.Parameters.AddWithValue("@IdGrupo", IdGrupo);

                return oSistema.CargarDataTable(cmd);
            }
            else
            {
                return null;
            }
        } // Carga los datos de un grupo en un data table 

        public cMetodo EliminarGrupo(string sIdGrupo)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traGrupos_Eliminar", cnn);
                
                cmd.CommandType = CommandType.StoredProcedure;
                
                cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
                
                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Grupo eliminado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Elimina un grupo de un brand site

        public void CargarUsuariosBrandSiteGrupos(GridView gv, string sIdBrandSite, string sIdGrupo, string sBusqueda)
        {
            SqlCommand cmd = new SqlCommand("selUsuariosPorBrandSite_Grupos");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
            cmd.Parameters.AddWithValue("@Busqueda", sBusqueda.Trim());

            oSistema.CargarGridView(gv, cmd);
        } // Carga los usuarios que tienen acceso a un brand site py que no estan como integrantes de un grupo

        public void CargarIntegrantesGrupo(GridView gv, string sIdGrupo)
        {
            SqlCommand cmd = new SqlCommand("selUsuariosPorBrandSite_Integrantes");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
            
            oSistema.CargarGridView(gv, cmd);
        } // Carga los usuarios que tienen acceso a un brand site py que no estan como integrantes de un grupo

        //Metodo para llenar la tabla catCTE después del pago
        public cMetodo agregarCTE(string idbrandsite, string tipopersona, string rfc, string nombres, string apellidos, string calle, string numero, string numeroint, string colonia, string municipio, string estado, string cp, string pais, string correo, string ctacontable, string activo, string razonsocial)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traRegistroCTE", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdBrandsite", int.Parse(idbrandsite));
                cmd.Parameters.AddWithValue("@TipoPersona", tipopersona);
                cmd.Parameters.AddWithValue("@RFC", rfc);
                cmd.Parameters.AddWithValue("@Nombres", nombres);
                cmd.Parameters.AddWithValue("@Apellidos", apellidos);
                cmd.Parameters.AddWithValue("@Calle", calle);
                cmd.Parameters.AddWithValue("@Numero", numero);
                cmd.Parameters.AddWithValue("@NumeroInt", numeroint);
                cmd.Parameters.AddWithValue("@Colonia", colonia);
                cmd.Parameters.AddWithValue("@Municipio", municipio);
                cmd.Parameters.AddWithValue("@Estado", estado);
                cmd.Parameters.AddWithValue("@CP", cp);
                cmd.Parameters.AddWithValue("@Pais", pais);
                cmd.Parameters.AddWithValue("@CorreoElectronico", correo);
                cmd.Parameters.AddWithValue("@CtaContable", ctacontable);
                cmd.Parameters.AddWithValue("@Activo", activo);
                cmd.Parameters.AddWithValue("@RazonSocial", razonsocial);
                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Acceso actualizado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        }
        //Metodo para llenar la tabla de traVentas después del pago
        public int agregarVenta(string idusuario, string nombre, string apellidos, string calle, string noExt, string noInt, string colonia, string municipio, string estado, string cp, string pais, string email, string rfc, string json,string monto)
        {
            try
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    string idVta;
                    SqlCommand cmd = new SqlCommand("traRegistroVentas", cnn);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(idusuario));
                    cmd.Parameters.AddWithValue("@Nombre", nombre);
                    cmd.Parameters.AddWithValue("@Apellidos", apellidos);
                    cmd.Parameters.AddWithValue("@Calle", calle);
                    cmd.Parameters.AddWithValue("@NumeroExt", noExt);
                    cmd.Parameters.AddWithValue("@NumeroInt", noInt);
                    cmd.Parameters.AddWithValue("@Colonia", colonia);
                    cmd.Parameters.AddWithValue("@Municipio", municipio);
                    cmd.Parameters.AddWithValue("@Estado", estado);
                    cmd.Parameters.AddWithValue("@CP", cp);
                    cmd.Parameters.AddWithValue("@Pais", pais);
                    cmd.Parameters.AddWithValue("@Email", email);
                    cmd.Parameters.AddWithValue("@RFC", rfc);
                    cmd.Parameters.AddWithValue("@Json", json);
                    cmd.Parameters.AddWithValue("@Monto", int.Parse(monto));

                    //cmd.ExecuteScalar();

                    idVta = cmd.ExecuteScalar().ToString();
                    cnn.Close();
                    return int.Parse(idVta);

                }
                else
                {
                    //oMetodo.Pass = false;
                    //oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                    cnn.Close();
                    return 0;
                }

            }
            catch
            {
                return 0;
            }
        }

        public cMetodo EdicionIntegranteGrupo(string sIdUxB, string sIdUsuario, string sIdGrupo, string sActivo)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorGrupo", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdUxG", int.Parse(sIdUxB));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse(sActivo));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Acceso actualizado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        }

        #endregion

        // Todas las funciones relacionadas al designer de un brand site

        #region Opciones del modo designer

        #region Catálogo de archivos relacionados

        public cMetodo AgregarArchivoRelacionado(string sIdIndice, string sNoImagen, string sIdArchivo)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traArchivosRelacionados", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdArchivoRelacionado", 0);
                cmd.Parameters.AddWithValue("@IdIndice", int.Parse(sIdIndice));
                cmd.Parameters.AddWithValue("@NoImagen", int.Parse(sNoImagen));
                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse("True"));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Archivo registrado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "No me pude conectar a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        }

        public cMetodo EliminarArchivoRelacionado(string sIdArchivoRelacionado)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traArchivosRelacionados", cnn);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdArchivoRelacionado", int.Parse(sIdArchivoRelacionado));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse("False"));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Archivo eliminado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "No me pude conectar a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        }

        public void CargarArchivosRelacionados(DataList dl, int IdIndice, int NoImagen)
        {
            SqlCommand cmd = new SqlCommand("selArchivosRelacionados");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdIndice", IdIndice);
            cmd.Parameters.AddWithValue("@NoImagen", NoImagen);

            dl.DataSource = oSistema.CargarDataTable(cmd);
            dl.DataBind();
        }

        #endregion

        #region Catálogo de archivos

        private String traArchivo(String sIdArchivo, String sNombre, String sDescripcion, String sUrl, String sExtension, String sPreview, String sPesoBytesPreview, String sIdBrandSite, String sIdUsuario, String sPesoBytesArchivo, String sActivo, String sArchivoTrabajo)
        {
            return traArchivo (sIdArchivo, sNombre, sDescripcion, sUrl, sExtension, sPreview, sPesoBytesPreview, sIdBrandSite, sIdUsuario, sPesoBytesArchivo, sActivo, sArchivoTrabajo, "0");
        }

        private String traArchivo(String sIdArchivo, String sNombre, String sDescripcion, String sUrl, String sExtension, String sPreview, String sPesoBytesPreview, String sIdBrandSite, String sIdUsuario, String sPesoBytesArchivo, String sActivo, String sArchivoTrabajo, string sIdCarpetaPadre)
        {
            try
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("traArchivo" , cnn);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo.Trim()));
                    cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                    cmd.Parameters.AddWithValue("@Descripcion", sDescripcion.Trim());
                    cmd.Parameters.AddWithValue("@Url", sUrl.Trim());
                    cmd.Parameters.AddWithValue("@Extension", sExtension.Trim());
                    cmd.Parameters.AddWithValue("@Preview", sPreview.Trim());
                    cmd.Parameters.AddWithValue("@PesoBytesPreview", float.Parse(sPesoBytesPreview.Trim()));
                    cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));
                    cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));
                    cmd.Parameters.AddWithValue("@ArchivoTrabajo", bool.Parse(sArchivoTrabajo.Trim()));
                    cmd.Parameters.AddWithValue("@IdCarpetaPadre", int.Parse(sIdCarpetaPadre));
                    cmd.Parameters.AddWithValue("@PesoBytesArchivo", float.Parse(sPesoBytesArchivo.Trim()));
                    cmd.Parameters.AddWithValue("@Activo", bool.Parse(sActivo.Trim()));

                    try
                    {
                        sIdArchivo = cmd.ExecuteScalar().ToString();
                    }
                    catch (Exception)
                    {
                        return "0";
                    }

                    if(sActivo=="True" && sPesoBytesArchivo == "0")
                    {
                        cnn.Close();

                        cnn = oSistema.connectDataBase();
                        cmd = new SqlCommand("InsModificaciones", cnn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@idUsuario", int.Parse(sIdUsuario));
                        cmd.Parameters.AddWithValue("@idArchivo", int.Parse(sIdArchivo));
                        cmd.Parameters.AddWithValue("@idBrandSite", int.Parse(sIdBrandSite));
                        cmd.Parameters.AddWithValue("@accion", 1);
                        cmd.ExecuteNonQuery();
                        cnn.Close();
                    }

                    return sIdArchivo;
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception)
            {
                return "0";
            }
        } // Sube el archivo a la base de datos (insert)

        public cMetodo SubirArchivo(String sIdArchivo, String sNombre, String sDescripcion, FileUpload fluUrl, FileUpload fluPreview, String sIdBrandSite, String sIdUsuario, String sArchivoTrabajo)
        {
            return SubirArchivo(sIdArchivo, sNombre, sDescripcion, fluUrl, fluPreview, sIdBrandSite, sIdUsuario, sArchivoTrabajo, "0");
        }

        public cMetodo SubirArchivo(String sIdArchivo, String sNombre, String sDescripcion, FileUpload fluUrl, FileUpload fluPreview, String sIdBrandSite, String sIdUsuario, String sArchivoTrabajo, string sIdCarpetaPadre)
        {
            try
            {

                String sUrlTemp = String.Empty, sPreviewTemp = String.Empty;
                String sUrl = fluUrl.FileName;
                String sPreview = fluPreview.FileName;

                String sActivo = "True";

                // Valida que todos los datos sean válidos y no esten vacios
                oMetodo = ValidarArchivo(sIdArchivo, sNombre, sDescripcion, sUrl, sIdBrandSite, sIdUsuario, sActivo);

                // Si los datos no son válidos o son vacios se sale del método
                if (oMetodo.Pass == false) {
                    return oMetodo;
                }

                // Si los datos son válidos...

                // Obtiene la extensión del archivo, en base a la ruta del archivo subido por el usuario.
                String sExtension = oSistema.ObtenerExtension(sUrl);

                if (sExtension.ToLower() == ".exe" || sExtension.ToLower() == ".msi")
                {
                    oMetodo.Message = "Selecciona una archivo válido";
                    oMetodo.Pass = false;
                    return oMetodo;
                }

                // Guarda en base de datos el registro para obtener el Id asignado
                sIdArchivo = traArchivo(sIdArchivo, sNombre, sDescripcion, sUrl, sExtension, sPreview, "0", sIdBrandSite, sIdUsuario, "0", sActivo, sArchivoTrabajo, sIdCarpetaPadre);

                // Si el Id del archivo es diferente de cero (es decir, que si lo registro)...
                if (sIdArchivo != "0")
                {
                    // Guardamos el archivo en una carpeta temporal, para subirla a la nube
                    try
                    {
                        // subimos el archivo
                        if (fluUrl.HasFile == true)
                        {
                            sUrlTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\" + sIdArchivo + sExtension;
                            fluUrl.SaveAs(sUrlTemp);
                        }

                        // subimos el preview
                        if (fluPreview.HasFile == true)
                        {
                            sPreviewTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\p" + sIdArchivo + oSistema.ObtenerExtension(fluPreview.FileName);
                            fluPreview.SaveAs(sPreviewTemp);
                        }
                    }
                    catch (Exception)
                    {
                        // Si no pudo subir el archivo, desactiva el registro inicial...
                        traArchivo(sIdArchivo, sNombre, sDescripcion, sUrl, sExtension, sPreview, "0", sIdBrandSite, sIdUsuario, "0", "false", sArchivoTrabajo, sIdCarpetaPadre);

                        oMetodo.Message = "¡No pudimos subir tu archivo!";
                        oMetodo.Pass = false;
                        return oMetodo;
                    }

                    // Una vez en la carpeta tempora,
                    // Sube el archivo a la nube, y si tiene exito...
                    if (oAmazon.uploadFile(sUrlTemp, sIdUsuario, sIdBrandSite, sIdArchivo) == true)
                    {
                        // Crea la nueva liga del archivo ya en la nube
                        sUrl = oAmazon.sRutaAmazon + sIdUsuario + "/" + sIdBrandSite + "/" + sIdArchivo;
                    
                        // Obtiene el peso del archivo en la nube
                        String sPesoBytesArchivo = oAmazon.ObtenerTamaño(sIdUsuario, sIdBrandSite, sIdArchivo).ToString();

                        String sPesoBytesPreview = "0";

                        // Si se anexo algo como preview, se sube el archivo, se obtiene el peso y la ruta final
                        if (sPreview != "")
                        {
                            if (oAmazon.uploadFile(sPreviewTemp, sIdUsuario, sIdBrandSite, "p" + sIdArchivo) == true)
                            {
                                sPesoBytesPreview = oAmazon.ObtenerTamaño(sIdUsuario, sIdBrandSite, "p" + sIdArchivo).ToString();

                                sPreview = oAmazon.sRutaAmazon + sIdUsuario + "/" + sIdBrandSite + "/p" + sIdArchivo;
                            }
                        }

                        // Actualiza todos los datos restantes en la base de datos...
                        traArchivo(sIdArchivo, sNombre, sDescripcion, sUrl, sExtension, sPreview, sPesoBytesPreview, sIdBrandSite, sIdUsuario, sPesoBytesArchivo, sActivo, sArchivoTrabajo, sIdCarpetaPadre);

                        // Borra los archivos temporales de la carpeta Temp
                        try {

                            if (sUrlTemp.Trim() != "")
                            {
                                File.Delete(sUrlTemp);
                            }

                            if (sPreviewTemp.Trim() != "")
                            {

                                File.Delete(sPreviewTemp);
                            }
                        }
                        catch (Exception) { }
                    }
                    else
                    {
                        // Si no pudo subir el archivo, desactiva el registro inicial...
                        traArchivo(sIdArchivo, sNombre, sDescripcion, sUrl, sExtension, sPreview, "0", sIdBrandSite, sIdUsuario, "0", "false", sArchivoTrabajo, sIdCarpetaPadre);

                        oMetodo.Message = "No pudimos subir tu archivo";
                        oMetodo.Pass = false;
                        return oMetodo;
                    }
                }
                else
                {
                    oMetodo.Message = "No pudimos registrar en la base de datos tu archivo";
                    oMetodo.Pass = false;
                    return oMetodo;
                }

                oMetodo.Message = sIdArchivo;
                oMetodo.Pass = true;
                return oMetodo;

            }
            catch (Exception)
            {
                oMetodo.Message = null;
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Sube los archivos a Amazon S3

        private cMetodo ValidarArchivo(String sIdArchivo, String sNombre, String sDescripcion, String sUrl, String sIdBrandSite, String sIdUsuario, String sActivo)
        {
            if (oSistema.ValidarVacio(sIdArchivo) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Id de archivo no válido";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre para el archivo";
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre más corto";
                return oMetodo;
            }

            //if (oSistema.ValidarVacio(sDescripcion.Trim()) == false)
            //{
            //    oMetodo.Pass = false;
            //    oMetodo.Message = "Escribe una descripción";
            //    return oMetodo;
            //}

            if (oSistema.ValidarLenght(sDescripcion.Trim()) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe una descripción más corta";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sUrl) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un archivo para subir";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdBrandSite) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Id de brand site no válido";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdUsuario) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Id de usuario no válido";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sActivo) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Estatus no válido";
                return oMetodo;
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Ok";
            return oMetodo;
        } // Al registrar un nuevo archivo valida que sus campos sean válidos

        public void CargarArchivos(GridView gv, string sIdBrandSite, string sBusqueda, string sArchivoTrabajo, string sIdUsuarioBusca, string sSoloImagen)
        {
            SqlCommand cmd = new SqlCommand("selArchivos");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@Nombre", sBusqueda.Trim());
            cmd.Parameters.AddWithValue("@ArchivoTrabajo", sArchivoTrabajo.Trim());
            cmd.Parameters.AddWithValue("@IdUsuarioBusca", int.Parse(sIdUsuarioBusca));

            if (sSoloImagen == "True")
            {
                DataTable dt = oSistema.CargarDataTable(cmd);
                DataView dv = new DataView(dt);

                dv.RowFilter = "Extension IN ('.png', '.jpg', '.jpeg', '.ico', '.gif', '.bmp', '.jp2', '.tiff', '.tif', '.tga' )";

                dv.Sort = "FechaActivo DESC";

                gv.DataSource = dv;
                gv.DataBind();
            }
            else
            {
                oSistema.CargarGridView(gv, cmd);
            }

        } // Carga los archivos en un gridView con una búsqueda

        public void CargarArchivos(DataList dl, String sIdBrandSite, String sBusqueda, String sArchivoTrabajo, String sOrdenadosPor, string sIdUsuarioBusca, string sIdCarpetaPadre)
        {
            SqlCommand cmd = new SqlCommand("selArchivos");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@Nombre", sBusqueda.Trim());
            cmd.Parameters.AddWithValue("@ArchivoTrabajo", sArchivoTrabajo.Trim());
            cmd.Parameters.AddWithValue("@IdUsuarioBusca", int.Parse(sIdUsuarioBusca));
            cmd.Parameters.AddWithValue("@IdCarpetaPadre", sIdCarpetaPadre);

            DataTable dt = oSistema.CargarDataTable(cmd);
            dt.DefaultView.Sort = sOrdenadosPor;
            dt.AcceptChanges();

            dl.DataSource = dt;
            dl.DataBind();

        } // Carga los archivos en un DataList para ser consultados y buscados

        public void CargarArchivos(DataList dl, string sIdBrandSite, string sBusqueda, string sArchivoTrabajo, string sOrdenadosPor, string sIdUsuarioBusca)
        {
            CargarArchivos(dl, sIdBrandSite, sBusqueda, sArchivoTrabajo, sOrdenadosPor, sIdUsuarioBusca, "%");
        }

        public DataTable CargarArchivos(String sIdBrandSite, String sBusqueda, String sArchivoTrabajo, String sOrdenadosPor, string sIdUsuarioBusca)
        {
            SqlCommand cmd = new SqlCommand("selArchivos");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@Nombre", sBusqueda.Trim());
            cmd.Parameters.AddWithValue("@ArchivoTrabajo", sArchivoTrabajo.Trim());
            cmd.Parameters.AddWithValue("@IdUsuarioBusca", int.Parse(sIdUsuarioBusca));

            DataTable dt = oSistema.CargarDataTable(cmd);
            dt.DefaultView.Sort = sOrdenadosPor;
            dt.AcceptChanges();

            return dt;

        } // Carga los archivos para ser consultados y buscados, y regresa el datatable.

        public void CargarItemsBusquedaArchivos(DropDownList ddl, String sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selArchivos");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            oSistema.CargarDropDownList(ddl, cmd);
        } // Carga un dropdownlist con los nombres de los archivos para su búsqueda

        public cMetodo EliminarArchivo(String sIdArchivo, String sIdBrandSite, String sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();
            int idArch = 0;
            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traArchivo_Eliminar", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo.Trim()));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));

                cmd.ExecuteNonQuery();

                cnn.Close();

                cnn = oSistema.connectDataBase();
                cmd = new SqlCommand("InsModificaciones", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@idArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@idBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@accion", 3);
                cmd.ExecuteNonQuery();
                cnn.Close();

                cnn = oSistema.connectDataBase();
                cmd = new SqlCommand("IdArchivoxIdUsuario", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idArchivo", int.Parse(sIdArchivo));
                idArch = int.Parse(cmd.ExecuteScalar().ToString());
                cnn.Close();


                oAmazon.deleteFile(idArch + "/" + sIdBrandSite + "/" + sIdArchivo);
                oAmazon.deleteFile(idArch + "/" + sIdBrandSite + "/" + "p" + sIdArchivo);

                oMetodo.Message = "Archivo eliminado";
                oMetodo.Pass = true;
                return oMetodo;

            }
            else
            {
                oMetodo.Message = "No pudimos conectarnos a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Da de baja un archivo en la base de datos

        public cMetodo ModificarArchivo(String sIdArchivo, String sNombre, String sDescripcion, String sIdBrandSite, FileUpload fluUrl, FileUpload fluPreview, String sArchivoTrabajo, String sIdUsuario)
        {
            // Primero validamos los datos
            oMetodo = ValidarArchivo(sIdArchivo, sNombre, sDescripcion, "N/A", "N/A", sIdUsuario, "N/A");

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }
            
            // Creamos las variables a utilizar en el método
            String sUrlTemp, sPreviewTemp = String.Empty;
            String sExtension = "", sPesoBytesArchivo = "", sPesoBytesPreview = "", sUrlPreview = " ";

            String sUrl = fluUrl.FileName;
            String sPreview = fluPreview.FileName;

            // Nos conectamos a la base de datos y validamos su conexión para que en caso de que no se conecte, no suba la imagen y evitar inconsistencias.
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn == null)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "No pudimos conectarnos a la base de datos";
                return oMetodo;
            }

            // Guardamos el archivo en una carpeta temporal, para subirla a la nube (si existe el archivo en los flu)
            if (fluUrl.HasFile == true)
            {
                sExtension = oSistema.ObtenerExtension(sUrl);

                sUrlTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\" + sIdArchivo + sExtension;
                fluUrl.SaveAs(sUrlTemp);
                
                //Borramos el archivo que ya existe en la nube, si existe
                oAmazon.deleteFile(sIdUsuario + "/" + sIdBrandSite + "/" + sIdArchivo);
                
                // Subimos el nuevo archivo y obtenemos su tamaño
                if (oAmazon.uploadFile(sUrlTemp, sIdUsuario, sIdBrandSite, sIdArchivo) == true)
                {
                    sPesoBytesArchivo = oAmazon.ObtenerTamaño(sIdUsuario, sIdBrandSite, sIdArchivo).ToString();
                    sUrl = oAmazon.sRutaAmazon + sIdUsuario + "/" + sIdBrandSite + "/" + sIdArchivo;
                    // Borramos el archivo de la carpeta temporal
                    if (sUrlTemp.Trim() != "")
                    {
                        File.Delete(sUrlTemp);
                    }
                }
            }

            // subimos el preview y hacemos los mismos pasos que un archivo
            if (fluPreview.HasFile == true)
            {
                sPreviewTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\p" + sIdArchivo + oSistema.ObtenerExtension(fluUrl.FileName);
                fluPreview.SaveAs(sPreviewTemp);

                oAmazon.deleteFile(sIdUsuario + "/" + sIdBrandSite + "/" + "p" + sIdArchivo);
                
                if (oAmazon.uploadFile(sPreviewTemp, sIdUsuario, sIdBrandSite, "p" + sIdArchivo) == true)
                {
                    sPesoBytesPreview = oAmazon.ObtenerTamaño(sIdUsuario, sIdBrandSite, "p" + sIdArchivo).ToString();
                    sUrlPreview = oAmazon.sRutaAmazon + sIdUsuario + "/" + sIdBrandSite + "/p" + sIdArchivo;
                    
                    // Borramos el archivo de la carpeta temporal
                    if (sPreviewTemp.Trim() != "")
                    {
                        File.Delete(sPreviewTemp);
                    }
                }
            }

            // Actualiza bd
            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traArchivo_Modificar", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@Nombre", sNombre);
                cmd.Parameters.AddWithValue("@Descripcion", sDescripcion);
                cmd.Parameters.AddWithValue("@PesoBytesArchivo", sPesoBytesArchivo);
                cmd.Parameters.AddWithValue("@UrlPreview", sUrlPreview);
                cmd.Parameters.AddWithValue("@Url", sUrl);
                cmd.Parameters.AddWithValue("@PesoBytesPreview", sPesoBytesPreview);
                cmd.Parameters.AddWithValue("@Extension", sExtension);
                cmd.Parameters.AddWithValue("@ArchivoTrabajo", bool.Parse(sArchivoTrabajo));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                cnn = oSistema.connectDataBase();
                cmd = new SqlCommand("InsModificaciones", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@idUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@idArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@idBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@accion", 2);
                cmd.ExecuteNonQuery();
                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Archivo actualizado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = true;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }

        } // Actualizar los datos de un archivo

        public DataTable CargarArchivo(String sIdArchivo)
        {
            SqlCommand cmd = new SqlCommand("SELECT Nombre, Descripcion, Url, Preview, ArchivoTrabajo FROM catArchivos WHERE IdArchivo = @IdArchivo");

            cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));

            return oSistema.CargarDataTable(cmd);
        } // Carga el detalle de un archivo y lo regresa en un datatable

        public cMetodo CorreoNotificaReemplazoArchivo(string sIdArchivoReemplazado, string sMensaje)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("Mail_NotificaReemplazo", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdArchivoModificado", int.Parse(sIdArchivoReemplazado.Trim()));
                cmd.Parameters.AddWithValue("@Texto", sMensaje.Trim());

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "¡Correos enviado exitosamente!";
                oMetodo.Pass = true;
                return oMetodo;

            }
            else
            {
                oMetodo.Message = "No pudimos enviar tu correo";
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Manda un correo a los usuarios consumidores notificandoles el reemplazo de un archivo

        public void CargarGruposBS(DropDownList ddlGrupos, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selGrupos_Lista");
            
            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            oSistema.CargarDropDownList(ddlGrupos, cmd);

        } // Carga en un dropdownlist la lista de todos los grupos + el grupo "Todos"

        public cMetodo EditarGruposPorArchivo(string sIdGxA, string sIdArchivo, string sIdGrupo, string sActivo)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traGruposPorArchivo", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdGxA", int.Parse(sIdGxA));
                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdGrupo", int.Parse(sIdGrupo));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse(sActivo));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Archivo actualizado";
                oMetodo.Pass = true;
                return oMetodo;

            }
            else
            {
                oMetodo.Message = "No pudimos conectarnos a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Este metodo inserta o actualiza a un grupo en un archivo

        public void CargarGruposPorArchivo(GridView gv, string sIdArchivo)
        {
            SqlCommand cmd = new SqlCommand("selGruposPorArchivo");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));

            oSistema.CargarGridView(gv, cmd);
        } // Este método me carga en un grid todos los grupos que pueden ver un determinado archivo

        public cMetodo DesactivarGruposDeArchivo(string sIdArchivo)
        {

            SqlConnection cnn = oSistema.connectDataBase();

            if(cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catGruposPorArchivo SET Activo = 0 WHERE IdArchivo = @IdArchivo AND Activo = 1", cnn);

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Ok";
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "No pudimos conectarnos a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }

        } // Da de baja todos los grupos activos del archivo, esto con la finalidad de a futuro insertar nuevos

        #endregion

        #region Catálogo de palabras (Glosario)

        public cMetodo ActualizarPalabra(String sIdPalabra, String sPalabra, String sDefinicion, String sTodosMisBS, String sIdBrandSite, String sIdUsuario)
        {
            oMetodo = ValidarPalabra(sIdPalabra, sPalabra, sDefinicion, sTodosMisBS, sIdBrandSite, sIdUsuario);

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }

              SqlConnection cnn = oSistema.connectDataBase();

              if (cnn != null)
              {
                  SqlCommand cmd = new SqlCommand("EXEC traGlosario @IdPalabra, @Palabra, @Definicion, @TodosMisBS, @IdBrandSite, @IdUsuario, @Activo", cnn);

                  cmd.Parameters.AddWithValue("@IdPalabra", int.Parse(sIdPalabra.Trim()));
                  cmd.Parameters.AddWithValue("@Palabra", sPalabra.Trim());
                  cmd.Parameters.AddWithValue("@Definicion", sDefinicion.Trim());
                  cmd.Parameters.AddWithValue("@TodosMisBS", bool.Parse(sTodosMisBS.Trim()));
                  cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));
                  cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));
                  cmd.Parameters.AddWithValue("@Activo", bool.Parse("True"));

                  cmd.ExecuteNonQuery();

                  cnn.Close();

                  oMetodo.Message = "Palabra registrada";
                  oMetodo.Pass = true;
                  return oMetodo;
              }
              else
              {
                  oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                  oMetodo.Pass = false;
                  return oMetodo;
              }
        } // Actualiza o registra una palabra

        public cMetodo EliminarPalabra(String sIdPalabra, String sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("EXEC traGlosario_Eliminar @IdPalabra, @IdUsuario", cnn);

                cmd.Parameters.AddWithValue("@IdPalabra", int.Parse(sIdPalabra));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Palabra eliminada";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Da de baja una palabra en la base de datos

        private cMetodo ValidarPalabra(String sIdPalabra, String sPalabra, String sDefinicion, String sTodosMisBS, String sIdBrandSite, String sIdUsuario)
        {
            if (oSistema.ValidarVacio(sIdPalabra) == false)
            {
                oMetodo.Message = "No se tiene un id de palabra válido";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sPalabra) == false)
            {
                oMetodo.Message = "Escribe una palabra";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sDefinicion) == false)
            {
                oMetodo.Message = "Escribe una definición";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sDefinicion, 1020) == false)
            {
                oMetodo.Message = "Escribe una definición más corta";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sTodosMisBS) == false)
            {
                oMetodo.Message = "Selecciona si aplica a uno, o todos tus brand sites";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdBrandSite) == false)
            {
                oMetodo.Message = "Brand Site no válido";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdUsuario) == false)
            {
                oMetodo.Message = "Por favor cierra e inicia sesión";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if(!oSistema.ValidarPalabraExiste(sPalabra))
            {
                oMetodo.Message = "La palabra ya existe";
                oMetodo.Pass = false;
                return oMetodo;
            }

            oMetodo.Message = "Ok";
            oMetodo.Pass = true;
            return oMetodo;
        } // Valida una palabra y todos sus campos

        public DataTable CargarPalabra(String sIdPalabra)
        {
            SqlCommand cmd = new SqlCommand("SELECT Palabra, Definicion, TodosMisBS, Activo FROM catGlosario WHERE IdPalabra = @IdPalabra");

            cmd.Parameters.AddWithValue("@IdPalabra", int.Parse(sIdPalabra));

            return oSistema.CargarDataTable(cmd);
        } // Carga el detalle de una palabra

        public void CargarPalabras(GridView gv, String sLetra, String sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selGlosario");

            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Letra", sLetra.Trim());
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));

            oSistema.CargarGridView(gv, cmd);
        } // Carga todas las palabras del glosario en un brand site

        #endregion

        #region Catálogo Estilos

        public void obtenerEstilos(string sIdBranSite, GridView gvEstilos)
        { 
            //obtiene los Datos en un DataTable.

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selEstilosPorBrandSite";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBranSite));
            DataTable dt = oSistema.CargarDataTable(cmd);
            gvEstilos.DataSource = dt;
            gvEstilos.DataBind();

        } // Obtenemos los estilos en un gridview

        public void obtenerFuentes(DropDownList dpFuentes, string sIdBranSite)
        { //obtiene las fuentes en un DataTable.


            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selFuentes2";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@bran", SqlDbType.VarChar).Value = sIdBranSite;
            DataTable dt = oSistema.CargarDataTable(cmd);
            dpFuentes.DataSource = dt;
            dpFuentes.DataTextField = "fuente";
            dpFuentes.DataValueField = "idFuente";
            dpFuentes.DataBind();

        } // Carga un dropdownlist con las fuentes de texto

        public void obtenerVariantesFuentes(DropDownList dpFuentes, string sIdfuente)
        { //obtiene las fuentes en un DataTable.


            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "sel_VarianteFont";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idfuente", sIdfuente));
            DataTable dt = oSistema.CargarDataTable(cmd);
            dpFuentes.DataSource = dt;
            dpFuentes.DataTextField = "Variante";
            dpFuentes.DataValueField = "VarianteValue";
            dpFuentes.DataBind();

        } // Carga un dropdownlist con las variantes fuentes de texto

        public void obtenerVariantes(DropDownList dpVariante, int sIdfuente)
        {// obtiene las variantes de la fuente en un DataTable
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selVariantesFonts";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@idfuente", sIdfuente));
            DataTable dt = oSistema.CargarDataTable(cmd);
            dpVariante.DataSource = dt;
            dpVariante.DataTextField = "variante";
            dpVariante.DataValueField = "idsubfuente";
            dpVariante.DataBind();
        }
        
        public void obtenerTamanos(DropDownList dpTamano)
        { //obtiene las fuentes en un DataTable.


            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selTamanos";
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = oSistema.CargarDataTable(cmd);
            dpTamano.DataSource = dt;
            dpTamano.DataTextField = "Tamano";
            dpTamano.DataValueField = "idTamano";
            dpTamano.DataBind();

        } // Carga un dropdownlist con los tamaños del texto
        
        public string guardaEstilos(string sIdEstilo, string sIdExB, string sIdUsuario,string sidBrandSite, string sIdFuente,string sTamano, bool bNegrita, bool bItalica, bool bSubrayada, string sColor, string sVariante)
        {

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "traEstilosPorBrandSite";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@IdExB", sIdExB));
            cmd.Parameters.Add(new SqlParameter("@IdUsuario", sIdUsuario));
            cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sidBrandSite));
            cmd.Parameters.Add(new SqlParameter("@IdFuente", sIdFuente));
            cmd.Parameters.Add(new SqlParameter("@Tamano", sTamano));
            cmd.Parameters.Add(new SqlParameter("@IdEstilo", sIdEstilo));           
            cmd.Parameters.Add(new SqlParameter("@Negrita", bNegrita));
            cmd.Parameters.Add(new SqlParameter("@Italica", bItalica));
            cmd.Parameters.Add(new SqlParameter("@Subrayada", bSubrayada));
            cmd.Parameters.Add(new SqlParameter("@Color", sColor));
            cmd.Parameters.Add(new SqlParameter("@Variante", sVariante));
            DataTable dt=oSistema.CargarDataTable(cmd);
            return dt.Rows[0][0].ToString();
           

        } // Guarda los cambios en los estilos
        
        public void crearArchivoEstiloCSS(string sIdBrandSite,string idExB) { //Crea el archivo con los estilos, se ejecuta en un hilo secundario a la aplicacion.


            try
            {

                string sNombreArchivo = "BS" + sIdBrandSite + ".css";
                string sRutaArchivo = HttpRuntime.AppDomainAppPath + @"CSS\";
                bool Nuevo = false;
                //Verifica si existe la carpeta CSS y despues verificamos si existe el archivo BSIDBS
                if (!Directory.Exists(sRutaArchivo))
                {
                    //Elimina el archivo y crea uno en blanco
                    System.IO.Directory.CreateDirectory(sRutaArchivo);
                }

                //Verifica si existe el archivo
                if (File.Exists(sRutaArchivo + sNombreArchivo))
                {
                    Nuevo = false;
                }
                else
                {
                    //Crea el archivo nuevo.
                    Nuevo = true;
                    File.Create(sRutaArchivo + sNombreArchivo);
                }



                //Obtiene el dataTable y CREA EL ARCHIVO
                escribeArchivoEstiloCSS(obtenerEstilos(sIdBrandSite), idExB, sRutaArchivo + sNombreArchivo);
            }
            catch (Exception) { 
            
                //CATCH

            }

        } //Crea el archivo con los estilos, se ejecuta en un hilo secundario a la aplicacion.
        
        protected void escribeArchivoEstiloCSS(DataTable dtEstilos, string idExB,string sRuta) {

            string sfont = "";
            bool sNegrita =false;
            bool sSubrayada = false;
            bool sItalica = false;
            string sColor = "";

      
            //Dependiento el idExB es la seccion a Agregar en el archivo Ruta.
            foreach (DataRow dtRow in dtEstilos.Rows)
            {
                if (idExB == dtRow["IdExB"].ToString())
                {
                    sfont = dtRow["Fuente"].ToString();
                    sNegrita = bool.Parse(dtRow["Negrita"].ToString());
                    sSubrayada = bool.Parse(dtRow["Subrayada"].ToString());
                    sItalica = bool.Parse(dtRow["Italica"].ToString());
                    sColor = "#" + dtRow["Color"].ToString();
                    break;
                }
            }


            /*Escre los datos en el archivo*/

            escribeDatos(idExB, sRuta, sfont, sNegrita, sSubrayada, sItalica, sColor);
       
        } //Crea el archivo.
        
        protected void escribeDatos(string idExB, string sRuta, string sfont, bool sNegrita, bool sSubrayada, bool sItalica, string sColor)
        {
          //Busca si ya esta escrita la clase
            if (BuscarClaseCSS(idExB,sRuta))
            {
                //Borra los datos
                borrarClaseCSS(idExB, sRuta);

            }
           //Escribe en Archivo
            string sBold=sNegrita?"bold":"normal";
            string sItalic=sItalica?"italic":"normal";
            string sunder=sSubrayada?"underline":"none";
          
            FileStream stream = new FileStream(sRuta, FileMode.Append, FileAccess.Write);
            StreamWriter writer = new StreamWriter(stream);
            writer.WriteLine(".Class"+idExB+"{");
            writer.WriteLine("font-family:"+sfont+";");
            writer.WriteLine("font-weight:"+sBold+";");
            writer.WriteLine("font-style:"+sItalic+";");
            writer.WriteLine("text-decoration:"+sunder+";");
            writer.WriteLine("color:"+sColor+";");
            writer.WriteLine("}");
            writer.Close();
        
        } //Escribe el estilo segun los parametros 
        
        protected void borrarClaseCSS(string idExB, string sRuta){

            string sNombreClase = ".Class" + idExB;
           
            int iInicio = sRuta.LastIndexOf(@"\");
            string sRutaCSS = sRuta.Substring(0, iInicio);
            string sNombreTemp = sRutaCSS+@"\BSTemp"+idExB+".css";
            int band = 0;
            string RLine = "";

           //Leer Archivo Del CSS
            FileStream stream = new FileStream(sRuta, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream);
            //Crear el archivo temporal
            StreamWriter writer = File.CreateText(sNombreTemp);

            while (reader.Peek() > -1)
            {
                RLine = reader.ReadLine();

                if (RLine.StartsWith(".Class"))
                {
                    if (RLine.StartsWith(".Class" + idExB))
                    {
                        band = 1;
                    }
                    else {
                        band = 0;
                    }
                  
                }

                if (band == 0) {
                    writer.WriteLine(RLine);
                
                }
                RLine = "";
            }
            reader.Close();
            writer.Close();

            File.Delete(sRuta);
            File.Copy(sNombreTemp,sRuta );
            File.Delete(sNombreTemp);

        } // Borra la clase/hoja de estilo creada para las tipografías
        
        protected bool BuscarClaseCSS(string idExB,string sRuta) {

            bool result = false;

            FileStream stream = new FileStream(sRuta, FileMode.Open, FileAccess.Read);
            StreamReader reader = new StreamReader(stream);

            while (reader.Peek() > -1)
            {
                if (reader.ReadLine().StartsWith(".Class" + idExB))
                {
                    result = true;
                    break;
                }
            }
            reader.Close();

            return result;
        
        } // Si existe la el archivo de la clase regresa true, si no false
        
        protected DataTable obtenerEstilos(string sIdBrandSite)
        { 
           
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "selEstilosPorBrandSite";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@IdBrandSite", sIdBrandSite));
            DataTable dtEstilos = oSistema.CargarDataTable(cmd);
            return dtEstilos;

        } // carga los estilos en un datatable

        public void CargarColores(GridView gv, string sBusqueda)
        {
            SqlCommand cmd = new SqlCommand("selColores");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@Busqueda", sBusqueda);
            oSistema.CargarGridView(gv, cmd);
        } // Carga un gridview con los colores y realiza la búsqueda

        public void CargarPantones(DropDownList dpPantones)
        {
            SqlCommand cmd = new SqlCommand("selPantones");
            cmd.CommandType = CommandType.StoredProcedure;
            dpPantones.DataSource = oSistema.CargarDataTable(cmd);;
            dpPantones.DataValueField = "HEX";
            dpPantones.DataTextField = "Nombre";
            dpPantones.DataBind();
           
        } // Carga un dropdownlist con todos los pantones

        public void GuardarColorBoton(string sIdBrandSite, string sHexadecimal)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if(cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET ColorBoton = @ColorBoton WHERE IdBrandSite = @IdBrandSite", cnn);
                cmd.Parameters.AddWithValue("@ColorBoton", sHexadecimal);
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                cmd.ExecuteNonQuery();

                cnn.Close();
            }
        }

        public string ObtenerColorBoton(string sIdBrandSite)
        {
            string sColor = "373E4E";

            SqlConnection cnn = oSistema.connectDataBase();

            if(cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT ISNULL(ColorBoton, '373E4E') AS ColorBoton FROM catBrandSites WHERE IdBrandSite = @IdBrandSite", cnn);
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                sColor = cmd.ExecuteScalar().ToString();

                cnn.Close();

            }

            return sColor;
        }

        public cMetodo InsertarColorFavorito(string sNombre, string sHex, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traColoresFavoritos", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@Hex", sHex.Trim());
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Color insertado";
                oMetodo.Pass = true;
                return oMetodo;

            }
            else
            {
                oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }

        } // Inserta un color favorito a un brandsite

        public cMetodo EliminarColorFavorito(string sHex, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catColoresFavoritos SET Activo = 0, IdUsuario = @IdUsuario, FechaActivo = GETDATE() WHERE Hex = @Hex AND IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@Hex", sHex.Trim());
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Color Eliminado";
                oMetodo.Pass = true;
                return oMetodo;

            }
            else
            {
                oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }

        } // Inserta un color favorito a un brandsite

        public void CargarColoresFavoritos(GridView gv, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selColoresFavoritos");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            oSistema.CargarGridView(gv, cmd);
        } // Carga un gridView con todos los colores favoritos

        #endregion

        #region Catálogo del Manual

        public cMetodo ActualizarItemIndice(string sIdIndice, string sNombre, string sIdIndicePadre, string sIdPlantilla, string sHTML, string sIdBrandSite, string sIdUsuario)
        {
            oMetodo = ValidarItemIndice(sNombre, sIdIndicePadre, sIdBrandSite, sIdUsuario);

            if (oMetodo.Pass != true)
            {
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traIndiceManual", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdIndice", int.Parse(sIdIndice.Trim()));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@IdIndicePadre", int.Parse(sIdIndicePadre));
                cmd.Parameters.AddWithValue("@IdPlantilla", int.Parse(sIdPlantilla));
                cmd.Parameters.AddWithValue("@HTMLPlantilla", sHTML.Trim());
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@Activo", true);

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Sección registrada";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema con la base de datos";
                return oMetodo;
            }
        } // Actualiza el item/nodo de un indice del manual

        public DataTable CargarItemIndice(string sIdIndice)
        {
            SqlCommand cmd = new SqlCommand("SELECT I.Nombre, I.IdIndicePadre, I.IdPlantilla, I.HTMLPlantilla FROM catIndiceManual I WHERE IdIndice = @IdIndice");

            cmd.Parameters.AddWithValue("@IdIndice", int.Parse(sIdIndice));

            return oSistema.CargarDataTable(cmd);
        } // Carga el detalle de un item/nodo del indice

        private cMetodo ValidarItemIndice(string sNombre, string sIdIndicePadre, string sIdBrandSite, string sIdUsuario)
        {
            if (oSistema.ValidarVacio(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre";
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre más corto";
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sIdIndicePadre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un indice padre";
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sIdBrandSite) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un Brand Site";
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sIdUsuario) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Inicia sesión una vez más";
                return oMetodo;
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Ok";
            return oMetodo;
        } // Valida que los campos no esten vacios

        public DataTable CargarPlantillas()
        {
            SqlCommand cmd = new SqlCommand("EXEC selPlantillas");

            return oSistema.CargarDataTable(cmd);
        } // Carga en un datatable las plantillas existentes

        public void CargarIndices(DropDownList ddl, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("EXEC selIndiceManual_Indices @IdBrandSite");

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            oSistema.CargarDropDownList(ddl, cmd);
        } // Carga el indice en un dropdown list en base a un id del brandsite

        public void CargarIndice(TreeView tv, string sIdBrandSite)
        {
            tv.Nodes.Clear();

            TreeNode nodo;

            SqlCommand cmd = new SqlCommand("EXEC selIndiceManual @IdBrandSite");

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            foreach (DataRow row in oSistema.CargarDataTable(cmd).Rows)
            {
                nodo = new TreeNode();

                string sIdIndicePadre = row["IdIndicePadre"].ToString().Trim();
                string sIdIndice = row["IdIndice"].ToString().Trim();
                string sNombre = row["Nombre"].ToString();

                if (sIdIndicePadre == "0")
                {
                    nodo.Value = sIdIndice;
                    nodo.Text =  sNombre ;
                    nodo.ToolTip = nodo.Text;
                    //nodo.ImageUrl = "Images/Nodo.png";
                    nodo.NavigateUrl = null;
                    tv.Nodes.Add(nodo);
                }
                else
                {
                    nodo.Value = sIdIndice;
                    nodo.Text = sNombre;
                    nodo.ToolTip = nodo.Text;
                    //nodo.ImageUrl = "Images/Nodo.png";
                    nodo.NavigateUrl = null;

                    foreach (TreeNode node in tv.Nodes)
                    {
                        if (node.Value == sIdIndicePadre)
                        {
                            node.ChildNodes.Add(nodo);
                        }
                    }


                }
            }
        } // Carga el indice en un treeview en base a un id del brandsite

        public void CargarIndiceBS(TreeView tv, string sIdBrandSite)
        {
            tv.Nodes.Clear();

            TreeNode nodo;

            SqlCommand cmd = new SqlCommand("EXEC selIndiceManualBrandSite @IdBrandSite");

            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            foreach (DataRow row in oSistema.CargarDataTable(cmd).Rows)
            {
                nodo = new TreeNode();

                string sIdIndicePadre = row["IdIndicePadre"].ToString().Trim();
                string sIdIndice = row["IdIndice"].ToString().Trim();
                string sNombre = row["Nombre"].ToString();

                string sNombreAncla = RemplazarSimbolos(sNombre);
                if (sIdIndicePadre == "0")
                {
                    nodo.Value = sIdIndice;

                    nodo.Text =  sNombre;
                    nodo.ToolTip = nodo.Text;
                    //nodo.ImageUrl = "Images/Nodo.png";
                    nodo.NavigateUrl = "#" + sIdIndice;
                    
                    tv.Nodes.Add(nodo);
                }
                else
                {
                    nodo.Value = sIdIndice;
                    nodo.Text = sNombre;
                    nodo.ToolTip = nodo.Text;
                    //nodo.ImageUrl = "Images/Nodo.png";
                    nodo.NavigateUrl = "#" + sIdIndice;
               

                    foreach (TreeNode node in tv.Nodes)
                    {
                        if (node.Value == sIdIndicePadre)
                        {
                            node.ChildNodes.Add(nodo);
                        }
                    }


                }
            }
        } // Carga los indices de un manual de un brandsite en base a un id de un brandsite

        public string RemplazarSimbolos(string sPalabra)
        {
            string nuevaPalabra = sPalabra.Replace(" ", "").Replace('á', 'a').
                Replace('é', 'e').Replace('í', 'i').Replace('ú', 'u').Replace('ó', 'o')
                .Replace('ñ', 'n').Replace('Á', 'A').Replace('É', 'E').Replace('Í', 'I')
                .Replace('Ú', 'U').Replace('Ó', 'O').Replace('Ñ', 'N');




            return nuevaPalabra;
        } // Reemplaza los acentos de un string

        public string CargarHTMLPlantilla(string sIdIndice)
        {
            string sHTML = "";

            SqlCommand cmd = new SqlCommand("SELECT TOP 1 HTMLPlantilla FROM catIndiceManual WHERE IdIndice = @IdIndice");

            cmd.Parameters.AddWithValue("@IdIndice", int.Parse(sIdIndice));

            foreach (DataRow row in oSistema.CargarDataTable(cmd).Rows)
            {
                sHTML = row["HTMLPlantilla"].ToString();
            }

            return sHTML;
        } // Carga el HTML de una sección del manual

        public cMetodo EliminarSeccionIndice(string sIdIndice, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traIndiceManual_Eliminar", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdIndice", int.Parse(sIdIndice));

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Sección eliminada";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema en la conexión a la base de datos";
                return oMetodo;
            }
        } // Elimina una sección del indice del manual

        #endregion

        #region Catálogo de tags

        private cMetodo ValidarTag(string sNombre, string sDescripcion, string sIdArchivo, string sIdUsuario, string sIdBrandSite, string sIdTag)
        {
            if (oSistema.ValidarVacioLenght(sNombre) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe un nombre";
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sDescripcion) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe una descripción";
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sDescripcion, 1020) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Escribe una descripción más corta";
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sIdArchivo) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Selecciona un archivo";
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sIdUsuario) == false)
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Cierre e inicia sesión por favor";
                return oMetodo;
            }

            if (!oSistema.ValidarTagExiste(sNombre, sIdBrandSite, sIdTag))
            {
                oMetodo.Message = "El tag ya existe";
                oMetodo.Pass = false;
                return oMetodo;
            }

            oMetodo.Pass = true;
            oMetodo.Message = "Datos válidos";
            return oMetodo;
        } // Valida los campos de un tag antes de llegar a la base de datos

        public cMetodo ActualizarTag(string sIdTag, string sNombre, string sDescripcion, string sIdArchivo, string sExtension, string sTodosMisBS, string sIdBrandSite, string sIdUsuario)
        {
            oMetodo = ValidarTag(sNombre, sDescripcion, sIdArchivo, sIdUsuario, sIdBrandSite, sIdTag);

            if (oMetodo.Pass != true)
            {
                return oMetodo;
            }

            if (sExtension.Contains(".") == false)
            {
                sExtension = "." + sExtension;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traTags", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdTag", int.Parse(sIdTag.Trim()));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                cmd.Parameters.AddWithValue("@Descripcion", sDescripcion.Trim());
                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@Extension", sExtension.Trim());
                cmd.Parameters.AddWithValue("@TodosMisBS", bool.Parse(sTodosMisBS));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Tag registrado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema con la base de datos";
                return oMetodo;
            }
        } // actualiza o registra un tag en la base de datos

        public cMetodo ElmiminarTag(string sIdTag, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traTags_Eliminar", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdTag", int.Parse(sIdTag.Trim()));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                
                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                oMetodo.Message = "Tag eliminado";
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema con la base de datos";
                return oMetodo;
            }
        }  // Da de baja un tag en la base de datos

        public void CargarTags(GridView gv, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selTags");

            cmd.CommandType = CommandType.StoredProcedure;
            
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

            oSistema.CargarGridView(gv, cmd);
        } // Carga los tags 

        public DataTable CargarTag(string sIdTag)
        {
            SqlCommand cmd = new SqlCommand("SELECT TOP 1 T.IdTag, T.Nombre, T.Descripcion, T.IdArchivo, A.Url, T.Extension, T.TodosMisBS FROM catTags T LEFT JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1 WHERE T.IdTag = @IdTag");

            cmd.Parameters.AddWithValue("@IdTag", int.Parse(sIdTag));

            return oSistema.CargarDataTable(cmd);
        } // Carga el detalle de un tag en un datatable

        public void CargarTagsDeArchivo(GridView gv, string sIdArchivo)
        {
            SqlCommand cmd = new SqlCommand("selTags_relacion");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
            oSistema.CargarGridView(gv, cmd);
        } // Carga los tags que le corresponden a un archivo en un gridview

        public void CargarTagsDeArchivo(DataList dl, string sIdArchivo)
        {
            SqlCommand cmd = new SqlCommand("selTags_relacion");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
            dl.DataSource = oSistema.CargarDataTable(cmd);
            dl.DataBind();
        } // Carga los tags que le corresponden a un archivo en un datalist

        public cMetodo AgregarTagPorArchivo(string sIdTxA, string sIdTag, string sIdArchivo, string sIdUsuario)
        {
            if (oSistema.ValidarVacioLenght(sIdTag) == false || oSistema.ValidarVacioLenght(sIdArchivo) == false || oSistema.ValidarVacioLenght(sIdUsuario) == false)
            {
                oMetodo.Message = "Todos los campos son obligatorios";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traTagsPorArchivo", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdTag", int.Parse(sIdTag));
                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Tag asignado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema con la base de datos";
                return oMetodo;
            }
        } // Agrega un tag a un archivo determinado

        public cMetodo EliminarTagPorArchivo(string sIdTxA)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traTagsPorArchivo_Eliminar", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdTxA", int.Parse(sIdTxA));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Tag eliminado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Tuvimos un problema con la base de datos";
                return oMetodo;
            }
        } // Eliminar un tag de un archivo seleccionado

        #endregion

        #region Template del brand site

        private void ReemplazarCSS(string sNuevaCSS, string sVariable, string sValor)
        {
            File.WriteAllText(sNuevaCSS, Regex.Replace(File.ReadAllText(sNuevaCSS), sVariable, sValor));
        } // Este métod reemplaza texto en un archivo para poder poner las nuevas propiedades en la hoja de estilo dinámica

        public bool CrearTemplate(string sIdBrandSite)
        {
            try
            {
                string sNuevaCSS = HttpRuntime.AppDomainAppPath + @"CSS\BrandSites\Id" + sIdBrandSite + ".css";

                if (File.Exists(sNuevaCSS) == true)
                {
                    File.Delete(sNuevaCSS);
                }

                System.IO.File.Copy(HttpRuntime.AppDomainAppPath + @"CSS\BrandSite.css", sNuevaCSS);

                foreach (DataRow row in CargarTemplate(sIdBrandSite).Rows)
                {
                    string sVersion = "?v=" + DateTime.Now.ToString("ddMMyyyyhhmmss").ToString();

                    ReemplazarCSS(sNuevaCSS, "#cabeza-background-image", row["BannerSuperior"].ToString() + sVersion);
                    ReemplazarCSS(sNuevaCSS, "#home-background-image", row["Home"].ToString() + sVersion);
                    ReemplazarCSS(sNuevaCSS, "#menu-image-logo", row["Logo"].ToString() + sVersion);
                    ReemplazarCSS(sNuevaCSS, ".Id1{}", row["Id1"].ToString());
                    ReemplazarCSS(sNuevaCSS, ".Id2{}", row["Id2"].ToString());
                    ReemplazarCSS(sNuevaCSS, ".Id3{}", row["Id3"].ToString());
                    ReemplazarCSS(sNuevaCSS, ".Id4{}", row["Id4"].ToString());
                    ReemplazarCSS(sNuevaCSS, ".Id5{}", row["Id5"].ToString());
                    ReemplazarCSS(sNuevaCSS, "#ColorBotonAzul", row["ColorBoton"].ToString());
                    ReemplazarCSS(sNuevaCSS, "#Color-fondo", row["ColorFondo"].ToString());
                }

                return true;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());

                return false;
            }
        } // Este es el método principal que crea la hoja de estilo, si queremos agregar otro atributo perzonalizable podemos agregarlo aqui y en la plantilla BrandSite.css

        public cMetodo CambiarColorFondo(string sColorFondoHex, string sIdBrandSite)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET ColorFondo = @ColorFondo WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@ColorFondo", sColorFondoHex);
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        }

        public cMetodo CambiarBannerPrincipal(string sIdArchivo, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET IdArchivoBannerPrincipal = @IdArchivo WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Cambia el banner principal en el header de un brandsite

        public cMetodo CambiarBackground(string sIdArchivo, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET IdArchivoBackground = @IdArchivo WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Cambia la imagen de background (Este método quedo desactivado)

        private DataTable CargarTemplate(string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selBrandSites_Template");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            return oSistema.CargarDataTable(cmd);
        } // Carga una tabla con todas las propiedades del template del sitio

        #endregion

        #region Opciones de home

        public cMetodo CambiarImagenHome(string sIdArchivo, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET IdArchivoHome = @IdArchivo WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Cambia la imagen principal de un sitio (el home)

        public cMetodo BorrarImagenHome(string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET IdArchivoHome = 0 WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Borra la imagen principal de un sitio (el home)

        public cMetodo CambiarLogo(string sIdArchivo, string sIdBrandSite, string sIdUsuario)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET IdArchivoLogo = @IdArchivo WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdArchivo", int.Parse(sIdArchivo));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Cambia el logo de un sitio

        public cMetodo GuardarBienvenida(string sIdBrandSite, string sBienvenida, string sIdUsuario)
        {
            if(oSistema.ValidarLenght(sBienvenida, 1020) == false)
            {
                oMetodo.Message = "Escribe una bienvenida menor a 1020 caracteres";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catBrandSites SET Bienvenida = @Bienvenida WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@Bienvenida", sBienvenida.Trim());
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Mensaje de bienvenida actualizado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Guarda la bienvenida

        public string CargarBienvenida(string sIdBrandSite)
        {
            try
            {
                string sBienvenida = string.Empty;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("SELECT ISNULL(Bienvenida, '') AS Bienvenida FROM catBrandSites WHERE IdBrandSite = @IdBrandSite", cnn);

                    cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                    sBienvenida = cmd.ExecuteScalar().ToString();

                    cnn.Close();
                }

                return sBienvenida;
            }
            catch (Exception e)
            {

                oSistema.saveLog(e.ToString());

                return "";
            }
        } // Carga el texto de bienvenida

        public string CargarImagenBienvenida(string sIdBrandSite)
        {
            try
            {
                string sUrl = string.Empty;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("SELECT ISNULL(A.Url, '') AS Imagen FROM catBrandSites B INNER JOIN catArchivos A ON A.IdArchivo = B.IdArchivoHome AND A.Activo = 1 WHERE B.IdBrandSite = @IdBrandSite", cnn);

                    cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                    sUrl = cmd.ExecuteScalar().ToString();

                    cnn.Close();
                }

                return sUrl;
            }
            catch (Exception e)
            {
                return "";
            }
        } // Carga la liga imagen de bienvenida para un sitio (El home)

        #endregion

        #endregion

        // Todas las funciones relacionadas a cargar el brand site final el cual será consultado por los consumidores de la identidad

        #region Opciones unicas del brandsite

        public void CargarNovedades(GridView gv, string sIdBrandSite, string sIdUsuario)
        {
            SqlCommand cmd = new SqlCommand("selBitacoraArchivos");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            cmd.Parameters.AddWithValue("@IdUsuarioBusca", int.Parse(sIdUsuario));
            oSistema.CargarGridView(gv, cmd);
        } // Carg un gridview con las novedades recientes del sitio

        public void FillHistorial(GridView gv, string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("SelModificaciones");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            oSistema.CargarGridView(gv, cmd);
        }

        public DataTable CargarContacto(string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selUsuarios_Contacto");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            return oSistema.CargarDataTable(cmd);
        } // Carga un DataTable con los datos del contacto, es decir el creador del brand site

        public DataTable CargarDatosFacturacion(string sIdBrandSite)
        {
            SqlCommand cmd = new SqlCommand("selUsuarios_DatosFacturacion");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
            return oSistema.CargarDataTable(cmd);
        } // Carga un DataTable con los datos del cliente para llenar la página de pago

        public cMetodo EnviarCorreoContacto(string sIdUsuarioEmisor, string sMensaje, string sIdBrandSite)
        {
            if (oSistema.ValidarVacioLenght(sMensaje) == false)
            {
                oMetodo.Message = "Escribe un mensaje";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("Mail_EnviarCorreoContacto", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUsuarioEmisor", int.Parse(sIdUsuarioEmisor.Trim()));
                cmd.Parameters.AddWithValue("@Mensaje", sMensaje.Trim());
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite.Trim()));

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Tu correo fué enviado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Envia un correo electrónico por medio de un stored procedure

        public cMetodo TieneAccesoBS(string sUsuario, string sIdBrandSite)
        {
            if (oSistema.ValidarVacioLenght(sUsuario) == false || oSistema.ValidarVacioLenght(sIdBrandSite) == false)
            {
                oMetodo.Message = "Escribe un usuario";
                oMetodo.Pass = false;
                return oMetodo;
            }

            string sAcceso = string.Empty;

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("selUsuarios_AccesoValido", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Usuario", sUsuario.Trim());
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                sAcceso = cmd.ExecuteScalar().ToString();

                cnn.Close();

                if (sAcceso == "1")
                {
                    oMetodo.Message = "Acceso válido";
                    oMetodo.Pass = true;
                    return oMetodo;
                }
                else
                {
                    oMetodo.Message = "No tienes acceso.";
                    oMetodo.Pass = false;
                    return oMetodo;
                }
            }
            else
            {
                oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }
        } // Valida si un usuario tiene acceso a un brand site

        public int ObtenerIdBrandSite(string sNombre)
        {
            int IdBrandSite = 0;

            try
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("selBrandSite_ObtenerId", cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                    
                    IdBrandSite = int.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return IdBrandSite;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int BrandSiteVencido(int idBrand)
        {
            int vencido = 0;
            try
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("FinalPrueba", cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Session", idBrand);
                  

                    vencido = int.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return vencido;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public bool ValidarEspacio(string sidBrand)
        {
            // Este try catch, cacha cualquier excepción y lo inserta en los logs
            try
            {
                int espacio;
                int i = 0;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlDataReader dr;
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand("selTipoPlan", cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sidBrand));
                    dt = oSistema.CargarDataTable(cmd);

                    espacio = int.Parse(dt.Rows[0][1].ToString());


                    cnn.Close();

                    if (espacio > 0)
                    {
                        cnn = oSistema.connectDataBase();
                        cmd = new SqlCommand("rAlmacenamiento", cnn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sidBrand));

                        dt = oSistema.CargarDataTable(cmd);

                        if(dt.Rows.Count>0)
                            i = int.Parse(dt.Rows[0][3].ToString());

                        cnn.Close();

                        if (i >= espacio)
                            return false;
                        else
                            return true;

                    }
                }

                return true;
            }
            catch (Exception e)
            {

                oSistema.saveLog(e.ToString() + " IdBrandSite:" + sidBrand);

                return false;
            }
        }

        #endregion

        #region Descargar

        public void DescargarArchivo(string sUrl, string sNombreArchivo, string sExtension, int iTamañoArchivo)
        {
            //Crea un strema para el archivo
            Stream stream = null;

            //This controls how many bytes to read at a time and send to the client
            //int tamano = 10000;
            int bytesToRead = iTamañoArchivo;

            // Buffer to read bytes in chunk size specified above
            byte[] buffer = new Byte[bytesToRead];

            // The number of bytes read
            try
            {
                //Prueba de variables con nombres
                //var nombre_url = "https://s3-us-west-2.amazonaws.com/identidadprueba/22/36/496";
                //var nombre_archivo = "Refa quiere un sanwiche";
                //var extention = ".JPEG";


                //Create a WebRequest to get the file
                HttpWebRequest fileReq = (HttpWebRequest)HttpWebRequest.Create(sUrl);

                //Create a response for this request
                HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();

                if (fileReq.ContentLength > 0)
                    fileResp.ContentLength = fileReq.ContentLength;

                //Get the Stream returned from the response
                stream = fileResp.GetResponseStream();

                // prepare the response to the client. resp is the client Response
                var resp = HttpContext.Current.Response;

                //Indicate the type of data being sent
                resp.ContentType = "application/octet-stream";

                //Name the file 
                resp.AddHeader("Content-Disposition", "attachment; filename=\"" + sNombreArchivo + sExtension + "\"");
                resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                int length;
                do
                {
                    // Verify that the client is connected.
                    if (resp.IsClientConnected)
                    {
                        // Read data into the buffer.
                        length = stream.Read(buffer, 0, bytesToRead);

                        // and write it out to the response's output stream
                        resp.OutputStream.Write(buffer, 0, length);

                        // Flush the data
                        resp.Flush();

                        //Clear the buffer
                        buffer = new Byte[bytesToRead];
                    }
                    else
                    {
                        // cancel the download if client has disconnected
                        length = -1;
                    }
                } while (length > 0); //Repeat until no data is read

            }
            catch (Exception err)
            {
                oSistema.saveLog(err.ToString());
            }
            finally
            {
                if (stream != null)
                {
                    //Close the input stream
                    stream.Close();
                }
            }

        }

        #endregion

        public string obtenerIdUsuarioFinal(string EsAdministrador, string IdAdministradorBS, string IdUsuario)
        {
            try
            {
                string IdUsuarioFinal = string.Empty;

                if (EsAdministrador == "0")
                {
                    IdUsuarioFinal = IdAdministradorBS;
                }
                else
                {
                    IdUsuarioFinal = IdUsuario;
                }

                return IdUsuarioFinal;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());

                return IdUsuario;
            }
        } // Este método me sirve para cuando un usuario que no es diseñador está editando poder saver si el es el administrador o no, para en una variable guardar el id de usuario del administrador

        
    }
}