﻿// Estos son mis using, para los que no saben aquí importo las bibliotecas que voy a usar :)

using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;


// Este es mi namespace, si no sabes :) un namespace en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos.

namespace Identidad.App_Code
{
    // Esta es mi clase cSistema, para los que no saben :) La clase es la construcción del lenguaje utilizada más frecuentemente para definir los tipos abstractos de datos en lenguajes de programación orientados a objetos. Generalmente, una clase se puede definir como una descripción abstracta de un grupo de objetos, cada uno de los cuales se diferencia por un estado específico y es capaz de realizar una serie de operaciones.

    public class cSistema
    {
        // Clase con funciones generales del sistema

        // Para los que no saben primero les voy a explicar que es sobrecargar un metodo :)
        // Un método sobrecargado se utiliza para reutilizar el nombre de un método pero con diferentes argumentos (opcionalmente un tipo diferente de retorno). Las reglas para sobrecargar un método son las siguientes:
        //+ Los métodos sobrecargados debeb de cambiar la lista de argumentos.
        //+ Pueden cambiar el tipo de retorno.
        //+ Pueden cambiar el modificador de acceso.
        //+ Pueden declarar nuevas o más amplias excepciones.
        //+ Un método puede ser sobrecargado en la misma clase o en una subclase.

        #region Base de datos

        // Método que genera y abre una conexión a la base de datos de Identidad

        public SqlConnection connectDataBase()
        {
            try
            {
                string sConnectionString = ConfigurationManager.ConnectionStrings["Identidad"].ConnectionString;

                SqlConnection sqlCnn = new SqlConnection(sConnectionString);

                sqlCnn.Open();

                return sqlCnn;
            }
            catch (Exception)
            {
                return null;
            }
        } // Conecta a la base de datos y regresa un objeto de tipo SqlConnection, utiliza la cadena de conexión del web.config

        #endregion

        #region Cargar controles

        /*
            Todas estas funciones sirven para cargar controles desde base de datos.
         
            Para utilizar estos métodos creamos una variable SqlCommand donde le especificamos la consulta y los parametros
            Ejemplo:
         
            SqlCommand cmd = new SqlCommand("EXEC traUsuario @Nombre, @Edad");

            cmd.Parameters.AddWithValue("@Nombre", "Rafael Flores");
            cmd.Parameters.AddWithValue("@Edad", 25);

            sist.CargarGridview(GridView, cmd);
         
         */

        public void CargarDropDownList(DropDownList ddl, SqlCommand cmd)
        {
            SqlConnection cnn = connectDataBase();

            if (cnn != null)
            {
                SqlDataReader dr;

                cmd.Connection = cnn;
                
                dr = cmd.ExecuteReader();

                ddl.Items.Clear();

                while (dr.Read())
                {
                    ddl.Items.Add(new ListItem(dr.GetString(0).ToString(), dr.GetInt32(1).ToString()));
                }

                cnn.Close();
            }
        } // Carga un dropdownlist con un objeto SqlCommand

        public void CargarGridView(GridView gv, SqlCommand cmd)
        {
            SqlConnection cnn = connectDataBase();

            if (cnn != null)
            {
                SqlDataReader dr;
                System.Data.DataTable dt = new System.Data.DataTable();

                cmd.Connection = cnn;

                dr = cmd.ExecuteReader();
                dt.Load(dr);

                gv.DataSource = dt;
                gv.DataMember = dt.TableName;
                gv.DataBind();

                cnn.Close();
            }
        } // Carga un GridView con un objeto SqlCommand

        public DataTable CargarDataTable(SqlCommand cmd) //Carga un DataTable con los datos de la consulta.
        {
            DataTable dt = new DataTable();
            SqlConnection cnn = connectDataBase();

            if (cnn != null)
            {
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.SelectCommand.Connection = cnn;
                da.Fill(dt);
                cnn.Close();
            }

            return dt;
        }

        #endregion

        #region Guardar erroes en el log

        // Estas funciones guardan todos los errores atrapados en un try catch 

        public void saveLog(String sException, String sIdUsuario)
        {
            try
            {

                int IdUsuario;

                bool bParsed = int.TryParse(sIdUsuario, out IdUsuario);

                if (bParsed == false)
                {
                    IdUsuario = 0;
                }

                SqlConnection cnn = connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("EXEC traLog @Exception, @IdUsuario", cnn);

                    cmd.Parameters.AddWithValue("@Exception", sException);
                    cmd.Parameters.AddWithValue("@IdUsuario", IdUsuario);

                    //cmd.ExecuteNonQuery();

                    cnn.Close();
                }
            }
            catch (Exception)
            {

            }
        } // Este método guarda un error en los logs, registrando a el usuario que le fallo

        public void saveLog(String sException)
        {
            try
            {

                SqlConnection cnn = connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("EXEC traLog @Exception, @IdUsuario", cnn);

                    cmd.Parameters.AddWithValue("@Exception", sException);
                    cmd.Parameters.AddWithValue("@IdUsuario", 0);

                    cmd.ExecuteNonQuery();

                    cnn.Close();
                }
            }
            catch (Exception)
            {

            }
        } // Este método guarda un error en los logs, sin registrar un usuario (Metodo sobrecargado)

        #endregion

        #region Otros

        public bool EnviarCorreo(string sPara, string sAsunto, string sMensaje)
        {
            if (ValidarVacioLenght(sPara) == false || ValidarVacioLenght(sAsunto) == false || ValidarVacio(sMensaje) == false)
                return false;

            return EnviarCorreo(sPara, "sitioidentidad@gmail.com", "csBu3n1s1m0", sAsunto, sMensaje);
        } // Este método nos permite mandar un correo electrónico a una persona, con asunto y mensaje desde la cuenta de gmail de sitioidentidad@gmail.com

        public bool EnviarCorreo(string sPara, string sDe, string sContraseñaDe, string sAsunto, string sMensaje)
        {
            try
            {

                /*
                 * Cliente SMTP
                 * Gmail:  smtp.gmail.com  puerto:587
                 * Hotmail: smtp.liva.com  puerto:25
                 */

                SmtpClient Client = new SmtpClient("smtp.gmail.com", 587);
                MailMessage Msg = new MailMessage();

                //Attachment Anexo = new Attachment(sAnexo);
                //Msg.Attachments.Add(Anexo);
                
                Msg.From = new MailAddress(sDe);
                
                Client.UseDefaultCredentials = false;
                Client.Credentials = new System.Net.NetworkCredential(sDe, sContraseñaDe);
                Client.EnableSsl = true;
                
                Msg.IsBodyHtml = true;
                Msg.Body = sMensaje;
                Msg.Subject = sAsunto;
                Msg.To.Clear();
                Msg.To.Add(sPara);

                Client.Send(Msg);

                return true;
            }
            catch(Exception)
            {
                return false;
            }
        } // Este método permite enviar un correo de gmail a un usuario, con la cuenta deseada y la contraseña correspondiente, asunto y mensaje

        public String ObtenerExtension(String sArchivo)
        {
            try
            {
                int punto = sArchivo.LastIndexOf(".");
                return "." + sArchivo.Substring(punto + 1);
            }
            catch (Exception)
            {
                return "";
            }
        } // Este método obtiene la extensión de un archivo y lo regresa en string :)

        public Boolean CorreoElectronicoValido(String sEmail)
        {
            try
            {
                String expresion;

                expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
                if (Regex.IsMatch(sEmail, expresion))
                {
                    if (Regex.Replace(sEmail, expresion, String.Empty).Length == 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        } // Este método valida el formato de un correo electrónico, y regresa true o false si es correcto o incorrecto

        public bool isNumeric(string sNumber)
        {
            try
            {
                int iOutput;
                return int.TryParse(sNumber, out iOutput);
            }
            catch (Exception)
            {
                return false;
            }
        } // Valida si el dato en string es numerico, y regresa true o false si si es número o false si no lo es

        public string ObtenerSubdominio(string sUrl)
        {
            try
            {
                string sSubdominio = string.Empty;

                if(sUrl.Contains("http://"))
                { 
                    sSubdominio = sUrl.Substring(7).Split('.')[0];
                }
                else
                {
                    sSubdominio = sUrl.Split('.')[0];
                }

                if(sUrl == "identidad.identidad.com" || sUrl == "http://identidad.identidad.com")
                {
                    sSubdominio = "";
                }

                return sSubdominio;

            }
            catch(Exception)
            {
                return "";
            }
        } // Obtiene el subdominio de una url

        

        #region Validacion de cajas de texto vacias o con excedente de lenght

        // Con estas funciones validamos que un "texto" no este vacio o que el tamaño del mismo no exceda de 255 caracteres
        // Si queremos validar que no sea mayor a 255 caracteres usamos el método que solo recibe como parametro sTexto
        // Si queremos especificar el largo de los caracteres podemos usar el método que reciba sTexto y iMaxLenght

        public Boolean ValidarLenght(String sTexto)
        {
            return ValidarLenght(sTexto, 255);
        } // Valida el largo del texto y regresa true si es menor a 255 y false si es mayor, este es un método sobrecargado

        public Boolean ValidarLenght(String sTexto, int iMaxLenght)
        {
            try
            {
                if (sTexto.Trim().Length >= iMaxLenght)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        } // Este método valida el largo de una cadena de texto dependiendo del número de caracteres que le solicitemos

        public Boolean ValidarVacio(String sTexto)
        {
            try
            {
                if (sTexto.Trim() == "" || sTexto == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        } // Este método valida que una cadena de texto no este vacia, y regresa true o false si esta vacia o no

        public Boolean ValidarVacioLenght(String sTexto)
        {
            try
            {
                if (ValidarVacioLenght(sTexto, 255) == false)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        } // Este método resume la validación que hace que un string no este vacio y el tamaño que no exceda de 255, esto para tenerlo en una sola línea de código, y regresa true o false si cumple la condición o no

        public Boolean ValidarVacioLenght(String sTexto, int iMaxLenght)
        {
            try
            {
                if (ValidarVacio(sTexto) == false)
                {
                    return false;
                }

                if (ValidarLenght(sTexto, iMaxLenght) == false)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        } // Este método es como el anterior, solo que aqui podemos validar la cantidad de caracteres que nosotros queramos. Regreas true si cumple y false si no lo hace

        public Boolean ValidarPalabraExiste(String sTexto)
        {
            try
            {
                DataTable dt;
                SqlCommand cmd = new SqlCommand("SELECT Palabra, Definicion, TodosMisBS, Activo FROM catGlosario WHERE Palabra = @Palabra");
                cmd.Parameters.AddWithValue("@Palabra", sTexto);
                dt = CargarDataTable(cmd);
                if (dt.Rows.Count > 0)
                    return false;
                else
                    return true;

            }
            catch(Exception e)
            {
                return false;
            }
        }

        public Boolean ValidarTagExiste(String sTexto, String sIdBrandSite, String sIdTag)
        {
            try
            {
                DataTable dt;
                SqlCommand cmd = new SqlCommand("SELECT Nombre FROM catTags WHERE Nombre = @Nombre and IdBrandSite = @Brandsite and IdTag != @Tag");
                cmd.Parameters.AddWithValue("@Nombre", sTexto);
                cmd.Parameters.AddWithValue("@Brandsite", sIdBrandSite);
                cmd.Parameters.AddWithValue("@Tag", sIdTag);
                dt = CargarDataTable(cmd);

                if (dt.Rows.Count > 0)
                    return false;
                else
                    return true;

            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion

        #endregion

        #region Encriptación

        // Estos métodos nos ayudan a encriptar un Id numerico y desencriptar un Id tipo string

        // Encritpa un Id desencriptado, y regresa un string con el dato encriptado
        public String EncriptarId(int IdDesencriptado)
        {
            try
            {
                return Encrypt(IdDesencriptado.ToString().Replace(" ", "+"), "!#$a54?3");
            }
            catch (Exception)
            {
                return null;
            }
        }

        // Este es el método que se usa para encriptar, es privado porque se manda llamar en el método de arriba
        private string Encrypt(string stringToEncrypt, string SEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(SEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(stringToEncrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                return Convert.ToBase64String(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        // Este método nos sirve para desencriptar un id, y nos regresa en un int el valor numerico
        public int DesencriptarId(String IdEncriptado)
        {
            try
            {
                int Id;

                if (int.TryParse(IdEncriptado, out Id))
                {
                    return Id;
                }
                else
                {
                    return int.Parse(Decrypt(IdEncriptado.Replace(" ", "+"), "!#$a54?3"));
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        // Este es el método que se usa para desencriptar, es privado porque se manda llamar en el método de arriba
        private string Decrypt(string stringToDecrypt, string sEncryptionKey)
        {
            byte[] key = { };
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xab, 0xcd, 0xef };
            byte[] inputByteArray = new byte[stringToDecrypt.Length + 1];

            try
            {
                key = System.Text.Encoding.UTF8.GetBytes(sEncryptionKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(stringToDecrypt);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = System.Text.Encoding.UTF8;
                return encoding.GetString(ms.ToArray());
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string[] Encriptar(string user, string pass)
        {
            string[] datos = new string[2];
            int i = 0;
            SqlConnection cnn = connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("selEncrip", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdDato1", user);
                cmd.Parameters.AddWithValue("@IdDato2", pass);

                SqlDataReader dr;
                System.Data.DataTable dt = new System.Data.DataTable();

                dr = cmd.ExecuteReader();
                dt.Load(dr);

                datos[0] = dt.Rows[0][0].ToString();
                datos[1] = dt.Rows[0][1].ToString();

                return datos;
            }
            return datos;
        }

        public string[] Desencriptar(string user, string pass)
        {
            string[] datos = new string[2];
            int i = 0;
            SqlConnection cnn = connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("selDesencrip", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdSolicitud", user);
                cmd.Parameters.AddWithValue("@IdEmpleado", pass);

                SqlDataReader dr;
                System.Data.DataTable dt = new System.Data.DataTable();

                dr = cmd.ExecuteReader();
                dt.Load(dr);

                datos[0] = dt.Rows[0][0].ToString();
                datos[1] = dt.Rows[0][1].ToString();

                return datos;
            }
            return datos;
        }

        #endregion

    }
}