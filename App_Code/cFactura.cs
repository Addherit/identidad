﻿//using System;
//using System.Collections.Generic;           
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using System.Data;
//using System.Windows.Forms;

using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Identidad.App_Code
{
    public class cFactura
    {
        cSistema oSistema = new cSistema(); // Referencia a clase cSistema
        DataTable dtFact;

    public DataTable Dic(int IdVenta,int IdBrandSite) 
        {
            
            //cCampos campo = new cCampos();
             dtFact = CargarFactura(IdVenta,IdBrandSite);

            //Dictionary<string, string> ListaE = new Dictionary<string, string>();
           
            DataTable dt = new DataTable();
            DataRow row = dt.NewRow();

            string Fecha = DateTime.Now.Date.ToString("d");

            string TipoMoneda, NombreMoneda, ClaveMon,RefMon;

        //se acordo el 24 de Agosto q solo se facturara en dolares
            //if (dtFact.Rows[0].ItemArray[23].ToString() == "México" || dtFact.Rows[0].ItemArray[23].ToString() == "Mexico" || dtFact.Rows[0].ItemArray[23].ToString() == "MEX")
            //{
            //    ClaveMon = "MXP";
            //    NombreMoneda = "Pesos";
            //    RefMon = "M.N.";
            //    TipoMoneda = "1";
            //}
            //else
            //{
                ClaveMon = "0US";
                NombreMoneda = "Dólares americanos";
                RefMon = "US";
                TipoMoneda = "2";      
            //}



            dt.Columns.Add("ID");
            row["ID"] = "D-ICOM";

            dt.Columns.Add("SreDcm"); //Este parámetro es la serie del documento (¿En qué serie van o han hecho alguna factura? De igual forma me dijeron que la estructura de la serie me la tenían que pasar ustedes desde su sistema)
            row["sreDcm"] =  "A";

            dt.Columns.Add("NmrDcm"); //Número de documento (Este yo lo asigno pero en ¿qué número van? O ¿Quieren que empiece desde algún número en específico?)
            row["NmrDcm"] =  dtFact.Rows[0].ItemArray[0].ToString();//6010;//

            dt.Columns.Add("Fch"); //(Fecha) ¿Hay varios campos con fecha a todos les asigno la misma? 
            row["Fch"] = Fecha;

            dt.Columns.Add("ImpSnt");
            row["ImpSnt"] = 0;

            dt.Columns.Add("IEPS"); //A todos se les cobrara ese impuesto o a ninguno? O ¿hay casos que a veces sí y a veces no?  Y si sí cuales son esos casos?
            row["IEPS"] = 0;

            dt.Columns.Add("RtnIVA");// (Retención de IVA )Mismo caso que el anterior.
            row["RtnIVA"] = dtFact.Rows[0].ItemArray[1].ToString();

            dt.Columns.Add("RtnISR");// ; (Retención de ISR)Mismo caso que el anterior.
            row["RtnISR"] = 0;
            
            dt.Columns.Add("DvlEfcPrcCnc");// (Devolución de efectivo o precio conocido)Mismo caso que el anterior.
            row["DvlEfcPrcCnc"] = 0;

            dt.Columns.Add("ClnClv");// (Clave del Cliente) En este me dijeron que teníamos que usar la estructura que ustedes manejan (¿Qué estructura manejan?)
            row["ClnClv"] = dtFact.Rows[0].ItemArray[2].ToString();

            dt.Columns.Add("PrcDsc");// Porcentaje de descuento
            row["PrcDsc"] = 0;

            dt.Columns.Add("AgnVntClv");// (Clave del agente de venta)
            //row["AgnVntClv"] = 0;

            dt.Columns.Add("PrcCms");// (Porcentaje de comisión)
            row["PrcCms"] = 0;

            dt.Columns.Add("TpoCmb");// (Tipo de cambio)
            row["TpoCmb"] =  dtFact.Rows[0].ItemArray[25].ToString(); //1; 

            dt.Columns.Add("FchRcp"); // (Fecha Recepción) Campo de fecha
            row["FchRcp"] = Fecha;

            dt.Columns.Add("PlzPgo"); // (Plazo de pago)            

            dt.Columns.Add("PgoPrc"); // (Pago en parcialidades)
            row["PgoPrc"] = 0;

            dt.Columns.Add("EnvTrc"); // (Envio)
            row["EnvTrc"] = 0;

            dt.Columns.Add("DstClv"); // (Clave del Destino)

            dt.Columns.Add("FchVncCtz"); // ()

            dt.Columns.Add("NmrPdd"); // (Número de pedido)

            dt.Columns.Add("NmrPrv"); // (Número de proveedor)

            dt.Columns.Add("Dto1"); //

            dt.Columns.Add("Dto2"); //

            dt.Columns.Add("Dto3"); //

            dt.Columns.Add("Obs"); //
            row["Obs"] = dtFact.Rows[0].ItemArray[3].ToString();

            dt.Columns.Add("FrmPgo"); // (Forma de pago)
            row["FrmPgo"] = "04";

            dt.Columns.Add("CndPgo"); // (Condiciones de pago) Este campo que debe llevar?

            dt.Columns.Add("NmrPrc"); // (Número de parcialidades)

            dt.Columns.Add("FchEmb"); // (Fecha Embarque)

            dt.Columns.Add("FchCncEmb"); // (Fecha Cancelación embarque)

            dt.Columns.Add("FchEnt"); // (Fecha Entrega)

            dt.Columns.Add("FchCncEnt"); // (Fecha Cancelación entrega)

            dt.Columns.Add("DscPrcImp"); // (Descuento en importe)
            row["DscPrcImp"] = 0;

            dt.Columns.Add("DscPrc1"); // (Porcentaje de Descuento)

            dt.Columns.Add("DscClv1"); // (Tipos de descuento)

            dt.Columns.Add("TpoAcmDsc1"); // (Acumulación) Aquí hay dos opciones"); // Cascada o acumulativo ¿Cuál es el que llevarían?
            row["TpoAcmDsc1"] = 0;

            dt.Columns.Add("DscPrc2"); // (Porcentaje o importe de descuento)

            dt.Columns.Add("DscClv2"); // (Tipo de descuento)

            dt.Columns.Add("TpoAcmDsc2"); // (Acumulación) Aquí hay dos opciones: Cascada o acumulativo ¿Cuál es el que llevarían?
            row["TpoAcmDsc2"] = 0;

            dt.Columns.Add("DscPrc3"); // (Porcentaje o Importe de descuento)

            dt.Columns.Add("DscClv3"); // (Tipo de descuento)

            dt.Columns.Add("TpoAcmDsc3"); // (Acumulación) Aquí hay dos opciones: Cascada o acumulativo ¿Cuál es el que llevarían?
            row["TpoAcmDsc3"] = 0;

            dt.Columns.Add("FloRcbMrc"); // (Folio de mercancía) ¿Cómo los vamos a foliar?

            dt.Columns.Add("FchFloRcbMrc"); // (Fecha de follio recibido)

            dt.Columns.Add("FchEntMrc"); // (Fecha entrega)

            dt.Columns.Add("FchOrdCmp"); // (Fecha de orden de compra)
            row["FchOrdCmp"] = Fecha;

            dt.Columns.Add("FchCnc"); // (Fecha de cancelación)

            dt.Columns.Add("HraCnc"); // (Hora de cancelación)

            dt.Columns.Add("MtvCnc"); // (Motivo de cancelación)

            dt.Columns.Add("ClnNmb"); // (Nombre del cliente)
            row["ClnNmb"] = dtFact.Rows[0].ItemArray[5].ToString();

            dt.Columns.Add("Tpo"); // (Tipo de Nacionalidad)
            row["Tpo"] = dtFact.Rows[0].ItemArray[6].ToString();

            dt.Columns.Add("RFC"); //
            row["RFC"] = dtFact.Rows[0].ItemArray[8].ToString(); //"AAA010101AAA";// 

            dt.Columns.Add("Cnt"); //(Contacto)
            row["Cnt"] = dtFact.Rows[0].ItemArray[9].ToString() + " " + dtFact.Rows[0].ItemArray[10].ToString();

            dt.Columns.Add("Cll"); //(Calle)
            row["Cll"] = dtFact.Rows[0].ItemArray[11].ToString();

            dt.Columns.Add("NmrExt"); //(Número exterior"))
            row["NmrExt"] = dtFact.Rows[0].ItemArray[12].ToString();

            dt.Columns.Add("NmrInt"); // (Número interior)
            row["NmrInt"] = dtFact.Rows[0].ItemArray[13].ToString();

            dt.Columns.Add("RfrDmc"); // (Referencia de Domicilio)
            //row["RfrDmc"] = dtFact.Rows[0].ItemArray[14].ToString();

            dt.Columns.Add("Cln"); // (Colonia)
            row["Cln"] =  dtFact.Rows[0].ItemArray[14].ToString();

            dt.Columns.Add("Mnc"); // (Municipio)
            row["Mnc"] =  dtFact.Rows[0].ItemArray[15].ToString();

            dt.Columns.Add("CddClv"); // (Clave de la ciudad)
            //row["CddClv"] = ;

            dt.Columns.Add("CddNmb"); // (Nombre de la ciudad)
            row["CddNmb"] =  dtFact.Rows[0].ItemArray[15].ToString();

            dt.Columns.Add("EstClv"); // (Clave del estado)
            //row["EstClv"] 

            dt.Columns.Add("EstNmb"); // (Nombre del estado)
            row["EstNmb"] =  dtFact.Rows[0].ItemArray[16].ToString();

            dt.Columns.Add("PaisClv"); // (Clave del país)
            row["PaisClv"] = dtFact.Rows[0].ItemArray[23].ToString();
            

            dt.Columns.Add("PaisNmb"); //(Nombre del país) 
            row["PaisNmb"] =  dtFact.Rows[0].ItemArray[17].ToString();

            dt.Columns.Add("CdgPst"); // (Código postal)
            row["CdgPst"] =  dtFact.Rows[0].ItemArray[18].ToString();

            dt.Columns.Add("Tlf"); // (Teléfono )
            row["Tlf"] = "0000 0000";

            dt.Columns.Add("Fax"); //

            dt.Columns.Add("Email"); //
            row["Email"] =  dtFact.Rows[0].ItemArray[19].ToString();

            dt.Columns.Add("FchAlt"); // (Fecha Alta)
            row["FchAlt"] = Fecha;
        
            dt.Columns.Add("MndClv"); // (Clave de la moneda)
            //row["MndClv"] = "OUS";
            row["MndClv"] = ClaveMon;

            dt.Columns.Add("MndNmb"); // (Nombre de la moneda)
            row["MndNmb"] = NombreMoneda;
            //row["MndNmb"] = "Dolares americanos";

            dt.Columns.Add("TpoCmb1"); // (Tipo de cambio) En caso de requerirlo de donde vamos a sacar el tipo de cambio
            row["TpoCmb1"] = dtFact.Rows[0].ItemArray[25].ToString(); 

            dt.Columns.Add("RfrMnt"); // (Referencia monetaria)
            row["RfrMnt"] = RefMon;    
        //row["RfrMnt"] = "USD";

            dt.Columns.Add("Tpo2"); // (Tipo de moneda)
            row["Tpo2"] = TipoMoneda;
            //row["Tpo2"] =  2;

            dt.Columns.Add("AgnVntClv2"); // (Clave del agente de venta)

            dt.Columns.Add("AgnVntNmb"); // (Nombre del agente de venta)

            dt.Columns.Add("FchAltAgnVnt"); // (Fecha de alta del agente de venta)

            dt.Columns.Add("PrcCms2"); // (Porcentaje de comisión)

            dt.Columns.Add("CncPrcClv"); // (Clave del concepto de precio)
            row["CncPrcClv"] = "PUB";

            dt.Columns.Add("LmtCrd"); // (Límite de crédito)

            dt.Columns.Add("AvsExcLmtCrd"); // (Avisar sí excede el límite de crédito)
            row["AvsExcLmtCrd"] = 0;

            dt.Columns.Add("PrcDsc2"); // (Porcentaje de descuento)

            dt.Columns.Add("MnjDst"); // (Maneja Destinos)
            row["MnjDst"] = 0;

            dt.Columns.Add("PlzPgo2"); // (Plazo de pago)

            dt.Columns.Add("NmrLclGlb"); // (Número de localización global) ¿Qué es ese número? Lo tendríamos que utilizar o no?

            dt.Columns.Add("VrsCFD"); // (Versión CFD) ¿Qué versión se utilizaría?
            row["VrsCFD"] = "3.2";

            dt.Columns.Add("Rfr"); // (Referencia receptor)

            dt.Columns.Add("Rfr2"); // (Referencia emisor)

            dt.Columns.Add("ScsNmb"); // (Nombre del socio)

            dt.Columns.Add("CmpNmb"); // (Nombre del comprador)

            dt.Columns.Add("DstXPrt"); // (Distribución por partida) ¿Esa es sí o no?
            row["DstXPrt"] = 0;

            dt.Columns.Add("NmrPrv2"); // (Número del proveedor)

            dt.Columns.Add("CtaCtb"); // (Cuenta contable)
            //row["CtaCtb"] = dtFact.Rows[0].ItemArray[20].ToString();

            dt.Columns.Add("CtaAnt"); // (Cuenta Contable anticipos)

            dt.Columns.Add("ZnaNmr"); // (Número de zona)

            dt.Columns.Add("EnvXMLLna"); // (Enviar xml por email en linea)
            row["EnvXMLLna"] = 1;

            dt.Columns.Add("ArcEnv"); // (Enviar Archivos)
            row["ArcEnv"] = 2;

            dt.Columns.Add("RznScl"); // (Razón social)
            row["RznScl"] = dtFact.Rows[0].ItemArray[5].ToString();

            dt.Columns.Add("Campo vacío"); //

            dt.Columns.Add("Campo vacío2"); //

            dt.Columns.Add("Campo vacío3"); //

            dt.Columns.Add("Campo vacío4"); //

            dt.Columns.Add("Campo vacío5"); //

            dt.Columns.Add("RfrFrmPgo"); // (Referencia del método de pago)
            row["RfrFrmPgo"] =  dtFact.Rows[0].ItemArray[24].ToString();//"TARJETA";//

            dt.Columns.Add("RgmFscClv"); // (Clave del régimen fiscal) La clave debe existir en el catálogo de tablas generales del sistema administrador 2000 Windows
            row["RgmFscClv"] = "001";

            dt.Columns.Add("SreOrg"); // (Serie fiscal) ¿Cuál sería la serie fiscal?

            dt.Columns.Add("FloOrg"); // (Folio fiscal) ¿Cuál es el folio fiscal?

            dt.Columns.Add("UUIDOrg"); // (UUID Origen) ¿Esto se usa y si sí que le debo poner?

            dt.Columns.Add("FchEmsOrg"); // (Fecha de emisión) Campo de fecha

            dt.Columns.Add("ImpTtlOrg"); // (Importe total de origen)

            dt.Columns.Add("PSECFDIClv"); // (Clave del PAC) La clave debe existir en el catálogo de PACs del sistema administrador 2000 Windows
            row["PSECFDIClv"] = "001";

            dt.Columns.Add("FchEms"); // (Fecha Emisión)Campo de fecha

            dt.Columns.Add("HraEms"); // (Hora Emisión)

            dt.Columns.Add("UUID"); // ¿Sí vamos a incluir ese parámetro?

            dt.Columns.Add("Campo vacío6"); //

            dt.Columns.Add("Campo vacío7"); //

            dt.Columns.Add("Campo vacío8"); //

            dt.Columns.Add("Campo vacío9"); //

            dt.Columns.Add("Cnc"); // (Clave del concepto) La clave debe existir en el catálogo de conceptos del sistema contafiscal 2000 Windows
            //row["Cnc"] = "PUB";

            dt.Columns.Add("Campo vacío10"); //

            dt.Columns.Add("Campo vacío11"); //

            dt.Columns.Add("Campo vacío12"); //

            dt.Columns.Add("Domicilio sucursal"); //

            dt.Columns.Add("Emitir documento en línea"); //Sí o no
            //row["Emitir documento en línea"] = "SI";

            dt.Columns.Add("Número de copias"); //

            dt.Columns.Add("ImpApl"); // (Importe de la aplicación)
            row["ImpApl"] = dtFact.Rows[0].ItemArray[21].ToString();

            dt.Columns.Add("Tpocmb2"); // (Tipo de cambio) 
            //row["Tpocmb2"] = ;


            dt.Columns.Add("FchApl"); // (Fecha de aplicación) 
            row["FchApl"] = Fecha;

            dt.Columns.Add("Rfr3"); // (Referencia de la aplicación)

            dt.Columns.Add("BncClv"); // (Clave del banco) La clave debe existir en el catálogo de bancos del sistema administrador 2000 Windows
            row["BncClv"] = "SCO";

            dt.Columns.Add("FrmPgoClv"); // (Clave de la forma de pago) La clave debe existir en el catálogo de formas de pago del sistema administrador 2000 Windows
            row["FrmPgoClv"] = "TCR";


            //int cant = (int)ListaE.Count;
            //MessageBox.Show(Convert.ToString(cant));
            

            dt.Rows.Add(row);
            return dt;

           
    }



    public DataTable Detalle()
    {

        //Dictionary<string, string> ListaE = new Dictionary<string, string>();
        
        DataTable dt = new DataTable();
        DataRow row = dt.NewRow();
        //Datos de las partidas de productos y/o servicios

        dt.Columns.Add("ID"); //Partida, producto y/o servicio) Tendrían que ser todos manejados como productos?
        row["ID"] = "P";

        dt.Columns.Add("PrdClv"); //Clave del producto) En el sistema maneja como obligatorio este campo tendría que clave quieren usar y como es la estructura
        row["PrdClv"] = 002;

        dt.Columns.Add("AlmClv"); //(Clave de almacén) La clave del almacén debe existir en el catálogo de almacenes del sistema administrador 2000 Windows
        
        dt.Columns.Add("Dsc"); //Descripción del producto)
        row["Dsc"] = dtFact.Rows[1].ItemArray[0].ToString();

        dt.Columns.Add("Cnt"); //Cantidad)
        row["Cnt"] = dtFact.Rows[1].ItemArray[2].ToString();

        dt.Columns.Add("CntRfr"); //Cantidad referencial) ¿Sería lo mismo que cantidad?

        dt.Columns.Add("PrcUnt"); //Precio unitario)
        row["PrcUnt"] = dtFact.Rows[1].ItemArray[3].ToString();

        dt.Columns.Add("PrcDsc"); //Porcentaje de Descuento)
        row["PrcDsc"] = 0;

        dt.Columns.Add("PrcIVA"); //(Porcentaje de iva) Todos los casos de México ya manejan el 16? Y a extranjeros les pongo 0 o también se les cobra?
        row["PrcIVA"] = dtFact.Rows[1].ItemArray[1].ToString();

        dt.Columns.Add("PrcIEPS"); //Porcentaje IEPS) Cuando tengo que manejar con todos de este impuesto
        row["PrcIEPS"] = 0;

        dt.Columns.Add("PrcSnt"); //
        row["PrcSnt"] = 0;

        dt.Columns.Add("PrcRtnIVA"); //Porecentaje de retención IVA) ¿Cuál es la diferencia entre el otro IVA?
        row["PrcRtnIVA"] = 0;

        dt.Columns.Add("PrcRtnISR"); //Porecentaje de retención ISR) ¿Cuál es la diferencia entre el otro ISR?
        row["PrcRtnISR"] = 0;

        dt.Columns.Add("Obs"); //Observacion de la partida)

        dt.Columns.Add("DscPrcImp"); //Descuento importe)
        row["DscPrcImp"] = 0;

        dt.Columns.Add("DscPrc1"); //Porcentaje o importe de descuento)

        dt.Columns.Add("DscClv1"); //Tipo de Descuento)

        dt.Columns.Add("TpoAcmDsc1"); //Acumulación) Hay dos opciones Cascada o Acumulativa

        dt.Columns.Add("DscPrc2"); //Porcentaje o importe de descuento)

        dt.Columns.Add("DscClv2"); //Tipo de Descuento)

        dt.Columns.Add("TpoAcmDsc2"); //Acumulación) Hay dos opciones Cascada o Acumulativa

        dt.Columns.Add("DscPrc3"); //Porcentaje o importe de descuento)

        dt.Columns.Add("DscClv3"); //Tipo de Descuento)

        dt.Columns.Add("TpoAcmDsc3"); //Acumulación) Hay dos opciones Cascada o Acumulativa

        dt.Columns.Add("ImpIEPSGD"); //Importe IEPS) Donde obtengo que importe es de ese?

        dt.Columns.Add("PrdNmb"); //Nombre del Producto)
        row["PrdNmb"] = dtFact.Rows[1].ItemArray[0].ToString();

        dt.Columns.Add("Tpo"); //Tipo) Las opciones en este es producto, servicio o impuesto
        row["Tpo"] = "S";

        dt.Columns.Add("Kit"); //

        dt.Columns.Add("DsgEnt"); //Desglosar entradas)

        dt.Columns.Add("CdgBrr"); //Código de barras)

        dt.Columns.Add("Dsc1"); //Descripción del producto)

        dt.Columns.Add("DscAdc"); //Descripción adicional del producto)

        dt.Columns.Add("Inv"); //Inventario)

        dt.Columns.Add("CptSrl"); //Serializado) Opción sí o no

        dt.Columns.Add("CptPdm"); //Pedimentado) Opción sí o no

        dt.Columns.Add("CptLte"); //Lotificado) Opción sí o no

        dt.Columns.Add("PrmSldNgt"); //Maneja saldos negativos) Opción sí o no

        dt.Columns.Add("CptSldRfr"); //Maneja saldo referencial) Opción sí o no

        dt.Columns.Add("TpoCst"); //Tipo de costeo) Opciones: Promedio, UEPS, PEPS, Ultimo costo, Detallista, Costo identificado

        dt.Columns.Add("Grv"); //Gravable) Opción sí o no
        row["Grv"] = dtFact.Rows[1].ItemArray[4].ToString();

        dt.Columns.Add("PrcIVA1"); //Porcentaje de IVA)¿Qué porcentaje maneja?
        row["PrcIVA1"] = dtFact.Rows[1].ItemArray[5].ToString();

        dt.Columns.Add("PrcIEPS1"); //Porcentaje de IEPS) ¿Qué porcentaje maneja?

        dt.Columns.Add("PrcSnt1"); //Porcentaje de Suntuario) ¿Qué porcentaje maneja?

        dt.Columns.Add("PrcRtnIVA1"); //Porcentaje de retención de IVA) ¿Qué porcentaje maneja?

        dt.Columns.Add("PrcRtnISR1"); //Porcentaje de retención de ISR) ¿Qué porcentaje maneja?

        dt.Columns.Add("UndBseClv"); //Clave de la Unidad Base) ¿Qué unidad debe manejar pieza, unidad o cuál sería?
        row["UndBseClv"] = "PZA";

        dt.Columns.Add("UndBseNmb"); //Nombre de la unidad Base) La misma pregunta de arriba
        row["UndBseNmb"] = "Pieza";

        dt.Columns.Add("UndRfrClv"); //Clave de la unidad de referencia) ¿Qué unidad es? ¿Hay diferencia con la de arriba?

        dt.Columns.Add("UndRfrNmb"); //Nombre de la unidad de referencia) Misma pregunta de arriba

        dt.Columns.Add("Fct"); //Factor de la unidad de referencia) ¿Se necesita ese factor?

        dt.Columns.Add("PrcMnmUtl"); //Porcentaje mínimo de utilidad ) ¿Se necesita?

        dt.Columns.Add("UltCstEnt"); //Ultimo costo de entrado) ¿Se necesita?

        dt.Columns.Add("Campo vacío1"); //

        dt.Columns.Add("Campo vacío2"); //

        dt.Columns.Add("Campo vacío3"); //

        dt.Columns.Add("Campo vacío4"); //

        dt.Columns.Add("Campo vacío5"); // 

        dt.Columns.Add("Campo vacío6"); // 

        dt.Columns.Add("Campo vacío7"); // 

        dt.Columns.Add("Campo vacío8"); // 

        dt.Columns.Add("Campo vacío9"); //

        dt.Columns.Add("Campo vacío10"); //

        dt.Columns.Add("Campo vacío11"); //

        dt.Columns.Add("Campo vacío12"); //

        dt.Columns.Add("Campo vacío13"); //

        dt.Columns.Add("Campo vacío14"); //

        dt.Columns.Add("Campo vacío15"); //

        dt.Columns.Add("Campo vacío16"); //

        dt.Columns.Add("Campo vacío17"); //

        dt.Columns.Add("Campo vacío18"); //

        dt.Columns.Add("SubTtl"); //Subtotal)Tenemos que separar el sub total?

        dt.Columns.Add("ImpIVA"); //Importe de IVA) 

        dt.Columns.Add("ImpIEPS"); //Importe de IEPS)

        dt.Columns.Add("ImpSnt"); //Importe suntuario)

        dt.Columns.Add("ImpRtnIVA"); //Importe de retención de IVA) 

        dt.Columns.Add("ImpRtnISR"); //Importe de retención de ISR)

        dt.Columns.Add("Campo vacío19"); //

        dt.Columns.Add("Campo vacío20"); //

        dt.Columns.Add("Campo vacío21"); //

        dt.Columns.Add("Campo vacío22"); //

        dt.Columns.Add("Campo vacío23"); //

        dt.Columns.Add("Campo vacío24"); //

        dt.Columns.Add("Campo vacío25"); //

        dt.Columns.Add("Campo vacío26"); //

        dt.Columns.Add("Campo vacío27"); //

        dt.Columns.Add("Campo vacío28"); //

        dt.Columns.Add("Campo vacío29"); //

        dt.Columns.Add("Campo vacío30"); //

        dt.Columns.Add("Campo vacío31"); //

        dt.Columns.Add("Campo vacío32"); //

        dt.Columns.Add("Campo vacío33"); //

        dt.Columns.Add("Campo vacío34"); //

        dt.Columns.Add("Campo vacío35"); //

        dt.Columns.Add("Campo vacío36"); //

        dt.Columns.Add("Campo vacío37"); //

        dt.Columns.Add("Campo vacío38"); //

        dt.Columns.Add("Campo vacío39"); //

        dt.Columns.Add("Campo vacío40"); //

        dt.Columns.Add("Campo vacío41"); //

        dt.Columns.Add("ClnClv"); //Clave del cliente) Se menciona anteriormente

        dt.Columns.Add("ClnNmb"); //Nombre del cliente)

        dt.Columns.Add("Tpo1"); //Tipo)

        dt.Columns.Add("RFC"); //

        dt.Columns.Add("Cnt1"); //Contacto)

        dt.Columns.Add("Cll"); //Calle)

        dt.Columns.Add("NmrExt"); //Número exterior)

        dt.Columns.Add("NmrInt"); //Número interior)

        dt.Columns.Add("RfrDmc"); //Referencia domicilio)

        dt.Columns.Add("Cln"); //Colonia)

        dt.Columns.Add("Mnc"); //Municipio)

        dt.Columns.Add("CddClv"); //Clave de la ciudad)

        dt.Columns.Add("CddNmb"); //Nombre de la ciudad)

        dt.Columns.Add("EstClv"); //Clave de estado)

        dt.Columns.Add("EstNmb"); //Nombre del estado)

        dt.Columns.Add("PaisClv"); //Clave del país)

        dt.Columns.Add("PaisNmb"); //Nombre del país)

        dt.Columns.Add("CdgPst"); //Código postal)

        dt.Columns.Add("Tlf"); //Teléfono)

        dt.Columns.Add("Fax"); //

        dt.Columns.Add("Email"); //

        dt.Columns.Add("FchAlt"); //Fecha Alta)

        dt.Columns.Add("MndClv"); //Clave de la moneda)

        dt.Columns.Add("MndNmb"); //Nombre de la moneda)

        dt.Columns.Add("TpoCmb"); //Tipo de Cambio)

        dt.Columns.Add("RfrMnt"); //Referencia Monetaria)

        dt.Columns.Add("Tpo2"); //Tipo de moneda)

        dt.Columns.Add("AgnVntClv"); //Clave del agente de venta)

        dt.Columns.Add("AgnVntNmb"); //Nombre del agente de venta)

        dt.Columns.Add("FchAltAgnVnt"); //Fecha de alta del agente de venta)

        dt.Columns.Add("PrcCms"); //Porcentaje de comisión)

        dt.Columns.Add("CncPrcClv"); //Clave del concepto de precio) ¿Qué es el concepto de preció? 

        dt.Columns.Add("LmtCrd"); //Límite de crédito)

        dt.Columns.Add("AvsExcLmtCrd"); //Avisar si hay límite de crédito)

        dt.Columns.Add("PrcDsc1"); //Porcentaje de descuento)

        dt.Columns.Add("MnjDst"); //Maneja destinos)

        dt.Columns.Add("PlzPgo"); //Plazo de pago)

        dt.Columns.Add("NmrLclGlb"); //Número de localización global)

        dt.Columns.Add("VrsCFD"); //Versión CFD)

        dt.Columns.Add("Rfr"); //Referencia receptor)

        dt.Columns.Add("Rfr2"); //Referencia emisor)

        dt.Columns.Add("ScsNmb"); //Nombre del socio)

        dt.Columns.Add("CmpNmb"); //Nombre del comprador)

        dt.Columns.Add("DstXPrt"); //Distribución x partida)

        dt.Columns.Add("NmrPrv"); //Número de proveedor)

        dt.Columns.Add("CtaCtb"); //Cuenta contable)

        dt.Columns.Add("CtaAnt"); //Cuenta contable anticipos)

        dt.Columns.Add("ZnaNmr"); //Número de zona)

        dt.Columns.Add("EnvXMLLna"); //Enviar XML email en linea)

        dt.Columns.Add("Archivo a enviar"); //

        dt.Columns.Add("RznScl"); //Razón social)

        dt.Columns.Add("Campo vacío42"); //

        dt.Columns.Add("Campo vacío43"); //

        dt.Columns.Add("Campo vacío44"); //

        dt.Columns.Add("Campo vacío45"); //

        dt.Columns.Add("DtsImp"); //Datos de impuestos IVA)

        dt.Columns.Add("DtsImp1"); //Datos de impuestos IEPS)

        dt.Columns.Add("DtsImp2"); //Datos de impuestos RIVA)

        dt.Columns.Add("DtsImp3"); //Datos de impuestos RIEPS)

        dt.Columns.Add("Campo Vacío46"); //

        dt.Columns.Add("Campo Vacío47"); //

        dt.Columns.Add("Campo Vacío48"); //

        dt.Columns.Add("Campo Vacío49"); //

        dt.Columns.Add("Campo Vacío50"); //

        dt.Columns.Add("Campo Vacío51"); //

        dt.Columns.Add("Campo Vacío52"); //

        dt.Columns.Add("Campo Vacío53"); //

        dt.Columns.Add("Campo Vacío54"); //

        dt.Columns.Add("Campo Vacío55"); //

        dt.Columns.Add("Campo Vacío56"); //

        dt.Columns.Add("Campo Vacío57"); //

        dt.Columns.Add("Campo Vacío58"); //

        dt.Columns.Add("Campo Vacío59"); //

        dt.Columns.Add("Campo Vacío60"); //

        dt.Columns.Add("Campo Vacío61"); //

        dt.Columns.Add("NmrCta"); //Número de cuenta)

        dt.Columns.Add("CURP"); //

        dt.Columns.Add("AlmNmb"); //Nombre de Alumno)

        dt.Columns.Add("NvlEdc"); //Nivel Educativo)

        dt.Columns.Add("GrdEsc"); //Grado Escolar)

        dt.Columns.Add("RFCPgo"); //RFC de quien realiza el pago)

        dt.Columns.Add("AutRVOE"); //Autorización o Reconocimiento de Validez Oficial de Estudios)

        dt.Columns.Add("FchAutRVOE"); //Fecha Autorización o Reconocimiento de Validez Oficial de Estudios) 

        
        dt.Rows.Add(row);
        
        return dt;
    }



    public void conecFTP(string path)
    {
        string RutaFtp = "ftp://54.186.74.36//";
        //usuario prueba
        //string user = "UserControl";
        //string Pass = "C0ntroldosmil";

        string user = "Identidad";
        string Pass = "Svbg)VWxQ8C9!tf72ajOp";

        //obtener el nombre del archivo para sacar toda la ruta Uri
        System.IO.FileInfo _FileInfo = new System.IO.FileInfo(path);
        string dir = RutaFtp + _FileInfo.Name;// +"/Descargas/"; 

        //para que sea una Uri validad debe tener el nombre del archivo
        System.Net.FtpWebRequest _FtpWebRequest = (FtpWebRequest)FtpWebRequest.Create(dir);//(FtpWebRequest)FtpWebRequest.Create(new Uri(RutaFtp));

        //_FtpWebRequest.EnableSsl = true;

        _FtpWebRequest.Credentials = new System.Net.NetworkCredential(user, Pass);



        _FtpWebRequest.KeepAlive = false;


        _FtpWebRequest.Timeout = 20000;


        _FtpWebRequest.Method = WebRequestMethods.Ftp.UploadFile;


        _FtpWebRequest.UseBinary = true;


        _FtpWebRequest.ContentLength = _FileInfo.Length;


        int buffLength = 2048;
        byte[] buff = new byte[buffLength - 1];


        System.IO.FileStream _FileStream = _FileInfo.OpenRead();

        //try
        //{

            System.IO.Stream _Stream = _FtpWebRequest.GetRequestStream();


            int contentLen = _FileStream.Read(buff, 0, buffLength - 1);


            do
            {
                _Stream.Write(buff, 0, contentLen);
                contentLen = _FileStream.Read(buff, 0, buffLength-1);
            }

            while (contentLen != 0);


            _Stream.Close();
            _Stream.Dispose();
            _FileStream.Close();
            _FileStream.Dispose();
        //}
        //catch (Exception ex)
        //{

            //   MessageBox.Show(ex.Message, "Upload Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //}


    }


    public DataTable CargarFactura(int IdVenta, int IdBrandSite)
    {
        SqlConnection cnn = oSistema.connectDataBase();

        DataTable RetTable = new DataTable();
        //sConn = cConexion.Identidad();
        // SqlConnection cnn = new SqlConnection(sConn);
        SqlDataAdapter da = new SqlDataAdapter();

        SqlCommand cmd = new SqlCommand("[dbo].[traCargarLayout]", cnn);
        cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite );
        cmd.Parameters.AddWithValue("@IdVenta", IdVenta);


        cmd.CommandType = CommandType.StoredProcedure;
        //cmd.Parameters.AddWithValue("@IdBrandSite", "75");


        //cnn.Open();
        da.SelectCommand = cmd;
        da.Fill(RetTable);

        cnn.Close();



        return RetTable;
    }


    public void Lista_csv(DataTable dt, DataTable dtD)
    {
        int cont = 1;
        //se cambia la ruta, dependiendo de donde se ejecute el codigo, si es del server, va en C
        //String path = "D:\\" + dt.Rows[0].ItemArray[52] + "_" + cont.ToString(); //"D:\\ABC12345678MN_1";
        String path = "C:\\inetpub\\ftproot\\Layout\\" + dt.Rows[0].ItemArray[52]  + "_" + cont.ToString();

        do{
            
            //path = "D:\\" + dt.Rows[0].ItemArray[52] + "_" + cont.ToString(); 
            path = "C:\\inetpub\\ftproot\\Layout\\" + dt.Rows[0].ItemArray[52] + "_" + cont.ToString();
            cont = cont + 1;
        }
        while (System.IO.File.Exists(path + ".csv"));

        

        String[] texto;
        texto = new String[dt.Rows.Count + 2];
        //Rellenamos la cabecera del fichero
        texto[0] = String.Empty;
        for (int c = 0; c < dt.Columns.Count; c++)
        {
            texto[0] += dt.Rows[0][c].ToString() + ",";
        }


        //Rellenamos el detalle del fichero
        String linea;
        for (int i = 0; i < dtD.Rows.Count; i++)
        {
            linea = String.Empty;
            for (int j = 0; j < dtD.Columns.Count; j++)
            {
                linea += dtD.Rows[i][j].ToString() + ",";
            }
            texto[i + 1] = linea;
        }

        path = path + ".csv";

        // utf8WithoutBom = new System.Text.UTF8Encoding(true);
        File.WriteAllLines(path, texto, System.Text.Encoding.Default);//, System.Text.Encoding.UTF8);

        //solo cuando se prueba desde localhost se tiene que subir al archivo por ftp, estando dentro del mismo server no
        //conecFTP(path);
       

    }

    }
}














