﻿// Estos son mis using, para los que no saben aquí importo las bibliotecas que voy a usar :)

using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.IO;
using System.Web.UI;

// Este es mi namespace, si no sabes :) un namespace en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos.

namespace Identidad.App_Code
{
    // Esta es mi clase cUsuario, para los que no saben :) La clase es la construcción del lenguaje utilizada más frecuentemente para definir los tipos abstractos de datos en lenguajes de programación orientados a objetos. Generalmente, una clase se puede definir como una descripción abstracta de un grupo de objetos, cada uno de los cuales se diferencia por un estado específico y es capaz de realizar una serie de operaciones.

    public class cUsuario
    {
        // Estan son las variables que voy a usar en esta página :)

        #region Variables

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();
        cAmazon oAmazon = new cAmazon();
        private string sContraseña;
        public String sProfilepictureUrl = "profilepicture/"; // Esta es la ruta en Amazon S3 donde tengo mi carpeta de fotos de usuarios

        #endregion

        // Funciones relacionadas a cualquier usuario de identidad.com

        #region Métodos

        public cMetodo IniciarSesion(String sUsuario, String sContraseña)
        {
            // Este try catch, cacha cualquier excepción y lo inserta en los logs
            try {

                oMetodo = Valida_IniciarSesion(sUsuario, sContraseña);

                if (oMetodo.Pass == true)
                {
                    SqlConnection cnn = oSistema.connectDataBase();

                    if (cnn != null)
                    {
                        SqlCommand cmd = new SqlCommand("EXEC selUsuarios_IniciarSesion @Usuario, @Contraseña", cnn);
                        
                        cmd.Parameters.AddWithValue("@Usuario", sUsuario.Trim());
                        cmd.Parameters.AddWithValue("@Contraseña", sContraseña.Trim());

                        String sIniciaSesion = cmd.ExecuteScalar().ToString();
                        
                        cnn.Close();

                        if (sIniciaSesion.Trim() == "1")
                        {
                            oMetodo.Pass = true;
                            oMetodo.Message = "Ok";
                        }
                        else
                        {
                            oMetodo.Pass = false;
                            oMetodo.Message = "Usuario o contraseña incorrecto";
                        }
                    }
                    else
                    {
                        oMetodo.Pass = false;
                        oMetodo.Message = "No se pudo establecer conexión a la base de datos";
                    }
                }

                return oMetodo;
            }
            catch (Exception e) {

                oMetodo.Pass = true;
                oMetodo.Message = e.ToString();

                oSistema.saveLog(e.ToString());

                return oMetodo;
            }
        } // Valida si el usuario puede o no iniciar sesión, regresando true o false con un comentario



        public int getIdTipoCuenta(String sUsuario)
        {

            int IdUsuario = 0;
            int id = getIdUsuario(sUsuario);

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT idTipoCuenta FROM catUsuarios WHERE IdUsuario = @id", cnn);

                cmd.Parameters.AddWithValue("@id", id);

                IdUsuario = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();
            }

            return IdUsuario;

        }


        private cMetodo Valida_IniciarSesion(String sUsuario, String sContraseña)
        {
            // Este try catch, cacha cualquier excepción y regresa false
            try {

                if (sUsuario.Trim() == "" || sUsuario == null)
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Escribe tu usuario";
                    return oMetodo;
                }

                if (sContraseña.Trim() == "" || sContraseña == null)
                {
                    oMetodo.Pass = false;
                    oMetodo.Message = "Escribe tu contraseña";
                    return oMetodo;
                }

                oMetodo.Pass = true;
                oMetodo.Message = "Ok";
                return oMetodo;

            } catch (Exception e) {

                oSistema.saveLog(e.ToString());
                oMetodo.Pass = false;
                oMetodo.Message = "Algo salió mal al validar los datos";
                return oMetodo;

            }
        } // Valida que los datos de inicio de sesión no esten vacios, regresa true o false

        public int getIdUsuario(String sUsuario)
        {
            // Este try catch, cacha cualquier excepción y lo inserta en los logs
            try {

                int IdUsuario = 0;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("SELECT TOP 1 IdUsuario FROM catUsuarios WHERE LTRIM(RTRIM(CorreoElectronico)) = LTRIM(RTRIM(@Usuario)) AND Activo = 1", cnn);

                    cmd.Parameters.AddWithValue("@Usuario", sUsuario);

                    IdUsuario = int.Parse(cmd.ExecuteScalar().ToString());

                    cnn.Close();
                }

                return IdUsuario;
            }
            catch (Exception e) {
                oSistema.saveLog(e.ToString() + " " +sUsuario);

                return 0;
            }
        } // Obtiene el Id del usuario a partir de su correo electrónico, regresa un int :)

        public string getFotoPerfil(String sIdUsuario)
        {
            // Este try catch, cacha cualquier excepción y lo regresa una imagen default
            try
            {

                string sFotoPefil = string.Empty;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand("SELECT TOP 1 CASE WHEN U.Foto = '' THEN '../Images/Site/ID.png' ELSE ISNULL(U.Foto, '../Images/Site/ID.png') END AS Foto FROM catUsuarios U WHERE U.IdUsuario = @IdUsuario", cnn);

                    cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                    sFotoPefil = cmd.ExecuteScalar().ToString();

                    cnn.Close();
                }

                return sFotoPefil;
            }
            catch (Exception e)
            {

                oSistema.saveLog(e.ToString());

                return "../Images/Site/ID.png";
            }
        } // Obtiene la url de la foto del perfil a partir del id del usuario en string :)

        public void getMisSitios(DropDownList ddl, String sIdUsuario)
        {
            int IdUsuario;

            bool bParsed = int.TryParse(sIdUsuario, out IdUsuario);

            if (bParsed == false)
            {
                IdUsuario = 0;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlDataReader dr;

                SqlCommand cmd = new SqlCommand("EXEC selUsuariosPorBrandSite_MisSitios @IdUsuario", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                dr = cmd.ExecuteReader();

                ddl.Items.Clear();

                while (dr.Read())
                {
                    ddl.Items.Add(new ListItem(dr.GetString(0).ToString(), dr.GetString(1).ToString()));
                }

                cnn.Close();
            }
        } // Obtiene las url de los sitios que se tiene acceso

        public DataTable cargarUsuario(String sIdUsuario)
        {
            int IdUsuario;

            bool bParsed = int.TryParse(sIdUsuario.Trim(), out IdUsuario);

            if (bParsed == false)
            {
                IdUsuario = 0;
            }

            SqlCommand cmd = new SqlCommand("SELECT TOP 1 U.Nombre, U.ApellidoPaterno, U.ApellidoMaterno, U.CorreoElectronico, U.Contraseña, U.Foto, U.Telefono, U.Activo , E.NombreEmpresa FROM catUsuarios U Left Join catEmpresa E on E.IdEmpresa = U.Empresa WHERE IdUsuario = @IdUsuario");

            cmd.Parameters.AddWithValue("@IdUsuario", sIdUsuario);

            return oSistema.CargarDataTable(cmd);
        } // Carga los datos del usuario en una tabla, regresando un data table :)

        public cMetodo ActualizarUsuario(String sIdUsuario, String sNombre, String sApellidoPaterno, String sEmpresa, String sCorreoElectronico, 
            String sContraseña, String sConfirmarContraseña, String sFotoActual, FileUpload fluFoto, String sTelefono, String sActivo, String sIdUsuarioActualiza)
        {
            String sFoto = fluFoto.FileName;
            String sFotoTemp = String.Empty;
            String sIdFoto = String.Empty;

            oMetodo = ValidarUsuario(sIdUsuario, sNombre, sEmpresa, sCorreoElectronico, sContraseña, sConfirmarContraseña, sActivo, sIdUsuarioActualiza);

            if (oMetodo.Pass == false)
            {
                return oMetodo;
            }

            String sUrlFoto = "";

            if (sFoto != "")
            {
                string sExtensionFoto = oSistema.ObtenerExtension(sFoto);

                if ((sExtensionFoto.ToLower() != ".png") && (sExtensionFoto.ToLower() != ".jpg") && (sExtensionFoto.ToLower() != ".jpeg") &&
                    (sExtensionFoto.ToLower() != ".gif") && (sExtensionFoto.ToLower() != ".bmp"))
                {
                    oMetodo.Message = "Selecciona una imagen válida";
                    oMetodo.Pass = false;
                    return oMetodo;
                }

                sIdFoto = sIdUsuario + DateTime.Now.ToString("ddMMyyyyhhmmss");

                if (fluFoto.HasFile == true)
                {
                    sFotoTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\" + sIdFoto + oSistema.ObtenerExtension(fluFoto.FileName);
                    
                    fluFoto.SaveAs(sFotoTemp);
                }

                sUrlFoto = sProfilepictureUrl + sIdFoto;

                if (oAmazon.uploadFile(sFotoTemp, sUrlFoto) == true)
                {
                    sUrlFoto = oAmazon.sRutaAmazon + sUrlFoto;
                }
                else
                {
                    sUrlFoto = "";
                }
            }
            else
            {
                sUrlFoto = sFotoActual;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("EXEC traUsuarios @IdUsuario, @Nombre, @ApellidoPaterno, @ApellidoMaterno, @Empresa, @CorreoElectronico, @Contraseña, @Foto, @Telefono, @IdUsuarioActualiza, @Activo", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@Nombre", sNombre.Trim());
                if(sApellidoPaterno.Length > 0)
                {
                    if(sApellidoPaterno.Contains(" "))
                    {
                        cmd.Parameters.AddWithValue("@ApellidoPaterno", sApellidoPaterno.Substring(0, sApellidoPaterno.IndexOf(' ')).Trim());
                        cmd.Parameters.AddWithValue("@ApellidoMaterno", sApellidoPaterno.Substring(sApellidoPaterno.IndexOf(' ') + 1).Trim());
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@ApellidoPaterno", sApellidoPaterno.Trim());
                        cmd.Parameters.AddWithValue("@ApellidoMaterno", sApellidoPaterno.Trim());
                    }
                }
                else
                {
                    cmd.Parameters.AddWithValue("@ApellidoPaterno", sApellidoPaterno.Trim());
                    cmd.Parameters.AddWithValue("@ApellidoMaterno", sApellidoPaterno.Trim());
                }
                cmd.Parameters.AddWithValue("@Empresa", sEmpresa.Trim());
                cmd.Parameters.AddWithValue("@CorreoElectronico", sCorreoElectronico.Trim());
                cmd.Parameters.AddWithValue("@Contraseña", sContraseña.Trim());
                cmd.Parameters.AddWithValue("@Foto", sUrlFoto.Trim());
                cmd.Parameters.AddWithValue("@Telefono", sTelefono.Trim());
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", int.Parse(sIdUsuarioActualiza));
                cmd.Parameters.AddWithValue("@Activo", bool.Parse(sActivo));

                oMetodo.oDato = cmd.ExecuteScalar().ToString();

                cnn.Close();

                // Borra el archivo temporal de la carpeta Temp
                try
                {
                    if (sFotoTemp.Trim() != "")
                    {
                        File.Delete(sFotoTemp);
                    }
                }
                catch (Exception) { }

                oMetodo.Message = "¡Datos actualizados!";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "¡Tuvimos un problema en la conexión a la base de datos!";
                oMetodo.Pass = true;
                return oMetodo;
            }
        } //Actualiza los datos de un usuario, regresando true o false en caso de exito o fracaso

        public bool CorreoElectronicoDisponible(string sCorreoElectronico)
        {
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("EXEC selUsuarios_CorreoValido @CorreoElectronico", cnn);

                cmd.Parameters.AddWithValue("@CorreoElectronico", sCorreoElectronico.Trim());

                string sValido = cmd.ExecuteScalar().ToString();

                cnn.Close();

                if (sValido == "1")
                {
                    return true;
                }
                else
                {
                    return false;
                }

                ;
            }
            else
            {
                return false;
            }

           
        } // Valida si el correo electrónico no esta ocupadoo es válido regresando true o false segun tenga exito o no

        private cMetodo ValidarUsuario(String sIdUsuario, String sNombre, String sEmpresa, String sCorreoElectronico, String sContraseña, String sConfirmarContraseña, String sActivo, String sIdUsuarioActualiza)
        {

            if (oSistema.ValidarVacio(sIdUsuario) == false)
            {
                oMetodo.Message = "No se ingreso un Id de usuario a la petición";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sNombre) == false)
            {
                oMetodo.Message = "Escribe un nombre";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sNombre) == false)
            {
                oMetodo.Message = "Escribe un nombre más corto";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sEmpresa) == false)
            {
                oMetodo.Message = "Escribe su empresa";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sCorreoElectronico) == false)
            {
                oMetodo.Message = "Escribe un correo electrónico";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sCorreoElectronico) == false)
            {
                oMetodo.Message = "Escribe un correo electrónico más corto";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.CorreoElectronicoValido(sCorreoElectronico) == false)
            {
                oMetodo.Message = "Escribe un correo electrónico válido";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (sIdUsuario == "0" && CorreoElectronicoDisponible(sCorreoElectronico) == false)
            {
                oMetodo.Message = "Este correo electrónico ya existe";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sContraseña) == false)
            {
                oMetodo.Message = "Escribe una contraseña";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarLenght(sContraseña) == false)
            {
                oMetodo.Message = "Escribe una contraseña más corta";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (sConfirmarContraseña != sContraseña)
            {
                oMetodo.Message = "La contraseña no coincide al momento de confirmar";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sActivo) == false)
            {
                oMetodo.Message = "Selecciona un estatus";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacio(sIdUsuarioActualiza) == false)
            {
                oMetodo.Message = "Por favor cierra e inicia sesión para guardar los datos";
                oMetodo.Pass = false;
                return oMetodo;
            }

            oMetodo.Message = "Ok";
            oMetodo.Pass = true;
            return oMetodo;
        } // Valida los datos del usuario, por ejemplo que no esten vacios, y regresa true o false segun sea el caso

        public string CrearContraseña(string sNombre, string sCorreoElectronico)
        {
            Random random = new Random();

            if (oSistema.ValidarVacioLenght(sNombre) == true && oSistema.ValidarVacioLenght(sCorreoElectronico) == true)
            {
                sContraseña = "iTemp" + sNombre[0].ToString().ToUpper() + sCorreoElectronico[0].ToString().ToLower() + DateTime.Today.DayOfYear.ToString() + random.Next(10, 99).ToString();
            }
            else
            {
                sContraseña = "";
            }

            return sContraseña;
        } // Crea una contraseña temporal para cuando se registr un nuevo usuario, y la regresa en string :)

        public cMetodo RecuperarMiContraseña(string sCorreoElectronico)
        {
            if (oSistema.ValidarVacioLenght(sCorreoElectronico) == false)
            {
                oMetodo.Message = "Escribe un correo electrónico";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (CorreoElectronicoDisponible(sCorreoElectronico) == true)
            {
                oMetodo.Message = "Escribe un correo electrónico existente";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("Mail_RecuperarContraseña", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@CorreoElectronico", sCorreoElectronico.Trim());

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Se envió un correo con la contraseña";
                oMetodo.Pass = true;
            }
            else
            {
                oMetodo.Message = "Tenemos un problema con la base de datos";
                oMetodo.Pass = false;
            }

            return oMetodo;
        } // Envia un correo electrónico al usuario con sus datos de inicio de sesión, regresa true o false si se logró o no el envio

        public string getNombreUsuario(string sIdUsuario)
        {
            string sNombre = string.Empty; 

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 Nombre FROM catUsuarios WHERE LTRIM(RTRIM(IdUsuario)) = LTRIM(RTRIM(@IdUsuario)) AND Activo = 1", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", sIdUsuario);

                sNombre = cmd.ExecuteScalar().ToString();

                cnn.Close();
            }

            return sNombre;
        } // Obtiene el nombre del usuario a partri de su id y lo regreas en string :)

        public string getCorreoElectronico(string sIdUsuario)
        {
            string sCorreo = string.Empty;

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 CorreoElectronico FROM catUsuarios WHERE LTRIM(RTRIM(IdUsuario)) = LTRIM(RTRIM(@IdUsuario)) AND Activo = 1", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", sIdUsuario);

                sCorreo = cmd.ExecuteScalar().ToString();

                cnn.Close();
            }

            return sCorreo;
        } // Obtiene el correo del usuario a partri de su id y lo regreas en string :)

        public int EsDiseñadorSitio(string sIdUsuario, string sIdBrandSite)
        {
            int iEsDiseñador = 0;
            
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("selUsuarios_EsDiseñadorSitio", cnn);
                
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                iEsDiseñador = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();
            }

            return iEsDiseñador;
        } // Validamos si un usuario es diseñador de un sitio en especifico y regresa 1 en caso que si y 0 en caso de que no

        public int IdUsuarioAdministradorSitio(string sIdBrandSite)
        {
            int IdUsuario = 0;

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT ISNULL(IdUsuario, '0') FROM catBrandSites WHERE IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                IdUsuario = int.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();
            }

            return IdUsuario;
        }

        public bool PuedeInvitar(string sIdUsuario, string sIdBrandSite)
        {
            bool bPuedeInvitar = false;

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 CASE WHEN PuedeInvitar IS NULL THEN CAST(0 AS BIT) ELSE PuedeInvitar END AS PuedeInvitar FROM catUsuariosPorBrandSite  WHERE IdUsuario = @IdUsuario AND IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                bPuedeInvitar = bool.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();
            }

            return bPuedeInvitar;
        }

        public bool EsDeMiEquipo(string sIdUsuario, string sIdBrandSite)
        {
            bool bPuedeInvitar = false;

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT TOP 1 CASE WHEN EsDeMiEquipo IS NULL THEN CAST(0 AS BIT) ELSE EsDeMiEquipo END AS EsDeMiEquipo FROM catUsuariosPorBrandSite  WHERE IdUsuario = @IdUsuario AND IdBrandSite = @IdBrandSite", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));

                bPuedeInvitar = bool.Parse(cmd.ExecuteScalar().ToString());

                cnn.Close();
            }

            return bPuedeInvitar;
        }

        public cMetodo InvitarUsuario(string sNombre, string sApellidoPaterno, string sApellidoMaterno, string sCorreo, string sContraseña, string sIdBrandSite, string sIdUsuarioActualiza, bool bPuedeInvitar)
        {
            oMetodo.oDato = null;

            if(oSistema.ValidarVacioLenght(sNombre) == false)
            {
                oMetodo.Message = "Escribe un nombre";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sCorreo) == false)
            {
                oMetodo.Message = "Escribe un correo electrónico";
                oMetodo.Pass = false;
                return oMetodo;
            }

            if (oSistema.ValidarVacioLenght(sContraseña) == false)
            {
                oMetodo.Message = "los datos con * son obligatorios";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("traUsuariosPorBrandSiteInvitarUsuario", cnn);

                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.AddWithValue("@Nombre", sNombre);
                cmd.Parameters.AddWithValue("@ApellidoPaterno", sApellidoPaterno);
                cmd.Parameters.AddWithValue("@ApellidoMaterno", sApellidoMaterno);
                cmd.Parameters.AddWithValue("@Correo", sCorreo);
                cmd.Parameters.AddWithValue("@Contraseña", sContraseña);
                cmd.Parameters.AddWithValue("@IdBrandSite", int.Parse(sIdBrandSite));
                cmd.Parameters.AddWithValue("@IdUsuarioActualiza", int.Parse(sIdUsuarioActualiza));
                cmd.Parameters.AddWithValue("@PuedeInvitar", bPuedeInvitar);

                oMetodo.oDato = cmd.ExecuteScalar().ToString();

                cnn.Close();
            }

            if(oMetodo.oDato != null)
            {
                oMetodo.Pass = true;
                oMetodo.Message = "Usuario invitado";
            }
            else
            {
                oMetodo.Pass = false;
                oMetodo.Message = "Error invitando a tu usuario";
            }

            return oMetodo;
        }

        public cMetodo TerminosCondiciones(string sIdUsuario)
        {
            oMetodo = new cMetodo();

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("UPDATE catUsuarios SET AceptoTerminos = 1, FechaAceptoTerminos = GETDATE() WHERE IdUsuario = @IdUsuario ", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                cmd.ExecuteScalar();

                cnn.Close();

                oMetodo.Pass = true;
            }
            else
            {
                oMetodo.Pass = false;
            }

            return oMetodo;
        }

        public cMetodo LeyoTerminosCondiciones(string sIdUsuario)
        {
            oMetodo = new cMetodo();

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("SELECT ISNULL(AceptoTerminos, 0) AS AceptoTerminos FROM catUsuarios WHERE IdUsuario = @IdUsuario ", cnn);

                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario));

                oMetodo.oDato = cmd.ExecuteScalar().ToString();

                cnn.Close();

                oMetodo.Pass = true;
            }
            else
            {
                oMetodo.Pass = true;
            }

            return oMetodo;
        }

        public cMetodo EnviarCorreoContactanos(string sIdUsuario, string sAsunto, string sTexto)
        {
            oMetodo = new cMetodo();

            if (sTexto == "")
            {
                oMetodo.Message = "Escribe un mensaje";
                oMetodo.Pass = false;
                return oMetodo;
            }

            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                SqlCommand cmd = new SqlCommand("Mail_EnviarCorreo", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUsuario", int.Parse(sIdUsuario.Trim()));
                cmd.Parameters.AddWithValue("@Asunto", sAsunto.Trim());
                cmd.Parameters.AddWithValue("@Texto", sTexto.Trim());

                cmd.ExecuteNonQuery();

                cnn.Close();

                oMetodo.Message = "Tu correo fué enviado";
                oMetodo.Pass = true;
                return oMetodo;
            }
            else
            {
                oMetodo.Message = "Tuvimos un problema con la conexión a la base de datos";
                oMetodo.Pass = false;
                return oMetodo;
            }

            //return oMetodo;

        } // Envia un correo electrónico por medio de un stored procedure

        public bool ValidarCantUsuarios(string sIdBrandSite)

        {
            // Este try catch, cacha cualquier excepción y lo inserta en los logs
            try
            {
                int IdBrandSite = int.Parse(sIdBrandSite);
                int countBrand = 0;
                int i = 0;

                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlDataReader dr;
                    DataTable dt = new DataTable();
                    SqlCommand cmd = new SqlCommand("selTipoPlan", cnn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite);

                    dt = oSistema.CargarDataTable(cmd);
                    //dr = cmd.ExecuteReader();

                    countBrand = int.Parse(dt.Rows[0][0].ToString());//dr.GetInt32(0);

                    cnn.Close();

                    if (countBrand > 0)
                    {
                        cnn = oSistema.connectDataBase();
                        cmd = new SqlCommand("selUsuariosPorBrandSite_Accesos", cnn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@IdBrandSite", IdBrandSite);

                        dt = oSistema.CargarDataTable(cmd);

                        i = dt.Rows.Count;

                        cnn.Close();

                        if (i >= countBrand)
                            return false;
                        else
                            return true;

                    }
                    else
                        return true;
                }

                return false;
            }
            catch (Exception e)
            {

                oSistema.saveLog(e.ToString());

                return false;
            }
        }

        public bool esAdministador(string sIdUsuario, string sIdBrandSite)
        {
            int tipo = 0;
            bool administrador = false;
            SqlConnection cnn = oSistema.connectDataBase();

            if (cnn != null)
            {
                DataTable dt = new DataTable();
                SqlCommand cmd = new SqlCommand("selTipoUxB", cnn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@IdUsuario", sIdUsuario);
                cmd.Parameters.AddWithValue("@IdBrandSite", sIdBrandSite);

                tipo = int.Parse(cmd.ExecuteScalar().ToString());
                cnn.Close();

                if (tipo == 2)
                    administrador = true;
            }
            return administrador;
        }
        #endregion

    }
}