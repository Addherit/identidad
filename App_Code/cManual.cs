﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;

namespace Identidad.App_Code
{
    public class cManual
    {

        public cManual()
        {

        }
        //traIndiceManual_CambiarTemplate 
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandsite = new cBrandSite(); 

        #endregion


        #region validaciones


        //Valida que los campos no esten vacios.
        public bool validatexto(params string[] text) 
        {

            for (int i=0; i < text.Length; i++)
                if (text[i] == "" || text[i] == null)
                    return false;

            return true;
        }

        #endregion

        #region Catálogo del Manual



        //SelConsultar Glosario

        public DataTable consultaGlosario(string idBrandSite) {

            if (validatexto(idBrandSite))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selGlosario";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdBrandSite", idBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        
        }


        //Actualiza el puro template o HTML del  manual.
        public bool actualizarPlantilla(string sIdIndice, string sIdPlantilla)
        {
            if (validatexto(sIdIndice, sIdPlantilla))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualCambiarTemplate";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndice));
                    cmd.Parameters.Add(new SqlParameter("@IdPlantilla", sIdPlantilla));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                    return dt != null ? true : false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //Obtiene la informacion del indice seleccionado
        public DataTable obtenerInformacion(string sIdIndice)
        {
            if (validatexto(sIdIndice))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManualInformacion";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndice));
                    DataTable dt = oSistema.CargarDataTable(cmd);
                    return dt;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //Actualiza la informacion del indice seleccionado
        public DataTable actualizarindiceMenu(string sIdIndice, string sNombre,string sPadre,string sUsuario)
        {
            if (validatexto(sIdIndice, sNombre,sPadre, sUsuario))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualActualizarNodo";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndice));
                    cmd.Parameters.Add(new SqlParameter("@NombreNodo", sNombre));
                    cmd.Parameters.Add(new SqlParameter("@NodoPadre", sPadre));
                    cmd.Parameters.Add(new SqlParameter("@Usuario", sUsuario));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                    return dt;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /*Obtiene la informacion del Nodo seleccionado*/
        public DataTable CargarItemIndice(string sIdIndice)
        {
            if (validatexto(sIdIndice))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManualInformacion";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@IdIndice", sIdIndice));
                    DataTable dt = oSistema.CargarDataTable(cmd);

                    return dt;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        //Guarda la plantilla modificada en el brandiste
        public bool GuardarPlantillaModificada(string sIdIndice, string sHTML) {

            if (validatexto(sIdIndice))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "traIndiceManualActualizarPlantilla";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@Indice", sIdIndice));
                    cmd.Parameters.Add(new SqlParameter("@HTML", sHTML));
       
                    DataTable dt = oSistema.CargarDataTable(cmd);

                    return dt != null ? true : false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        
        //Cosas de BrandSite Para poner el HTM del Manual
        public DataTable consultarManualIndices(string sIdBrandSite) { 

            if (validatexto( sIdBrandSite))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selIndiceManual_Indices";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@idBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);

            return dt;
                }
            return null;
            }
            return null;

        
        
        }
       
        //Consulta el html del manual para poner el template
        public DataTable consultarManualHTML(string sIdBrandSite){
            
            
            if (validatexto( sIdBrandSite))
            {
                SqlConnection cnn = oSistema.connectDataBase();

                if (cnn != null)
                {

                    SqlCommand cmd = new SqlCommand();
                    cmd.CommandText = "selInidiceManualBrandSiteDetalle";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@idBrandSite", sIdBrandSite));
                    DataTable dt = oSistema.CargarDataTable(cmd);

            return dt;
                }
            return null;
            }
            return null;
        }
     
        #endregion


    }
}