﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using conekta;//Librería de pago electrónico

using System.Text;
namespace Identidad.App_Code
{
    public class cHTTPrequest
    {
        static cSistema oSistema = new cSistema();
        //Método con los parámetros del pago
        public static string ObtenerListaWS(string sUrl, string sJson, string sIdUsuario = "")
        {
            //Pagos o cargos conekta con los pasos anteriores a importar la librería
            //var httpWebRequest = (HttpWebRequest)WebRequest.Create(sUrl)
            //httpWebRequest.Method = "POST";
            //httpWebRequest.Credentials = CredentialCache.DefaultCredentials;
            //httpWebRequest.Accept = "application/vnd.conekta-v1.0.0+json";
            //httpWebRequest.ContentType = "application/json";

            //Actualización con librerías para asignar las keys entregadas por conekta para la conexión con sus APIs
           // Api.apiKey = "key_rQssuytKsjJrnTEaRLrZkw";//SandBox
            //Api.apiKey = "key_2XgbuxuX6wDb6VpJY3nmnQ";//Producción
            Api.locale = "es";
            Api.version = "1.0.0";

            //Base 64 para ingresar el Key privado en SandBox y Producción 
            //httpWebRequest.Headers.Add("Authorization", "Basic a2V5XzFFeE5hcTl2S0xzc2FWcnlHNE5tcUE="); //Sandbox
            //httpWebRequest.Headers.Add("Authorization", "Basic a2V5XzJYZ2J1eHVYNndEYjZWcEpZM25tblE="); //Producción
            //var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream());

            //Parámetros en fomra de json necesrios para el request de conekta se crea la variable y se le pasa los datos traidos desde pagina-datos-pago
            string json = sJson;
            //json = json.Remove(0,1); //Quita el id asignado
            //json = json.Remove(json.Length-1,1); //Quita parametro agregado para mandar sólo los parámetros necesarios para conekta
            string responseText;
            //streamWriter.Write(json);
            //streamWriter.Close();

            //Si el pago se aprueba pasa correctamente por el try catch y cualquier error que tome el catch no procesara el pago
            try
            {
             
                Charge charge = new Charge().create(@json);
                //Console.WriteLine(charge.status);
                responseText = charge.toJSON();//Regregrasa un json con la respuesta exitosa.
                //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //System.Console.WriteLine(httpResponse);
                //var streamReader = new StreamReader(httpResponse.GetResponseStream());
                //responseText = streamReader.ReadToEnd();
            }
            catch (ConektaException e)
            {
                //Console.WriteLine(e.message_to_purchaser);
                //Si quitas la frase "Su pago no pudo ser procesado." del responseText asegurate de cambiar tambien el contains de la linea 123 del archivo pagina-datos-pago.aspx.cs
                responseText = "Su pago no pudo ser procesado.";// + e.message_to_purchaser; //Trae el error arrojado por conekta concatendo a un comentario de nosotros.
                oSistema.saveLog(e.message + " " + sUrl + " " + sIdUsuario);
               

               
            }

            //var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            //System.Console.WriteLine(httpResponse);
            //var streamReader = new StreamReader(httpResponse.GetResponseStream());
            //var responseText = streamReader.ReadToEnd();



            return responseText;
        }

    }
}