﻿// Estos son mis using, para los que no saben aquí importo las bibliotecas que voy a usar :)

using System;

// Este es mi namespace, si no sabes :) un namespace en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos.

namespace Identidad.App_Code
{
    // Esta es mi clase cMetodo, para los que no saben :) La clase es la construcción del lenguaje utilizada más frecuentemente para definir los tipos abstractos de datos en lenguajes de programación orientados a objetos. Generalmente, una clase se puede definir como una descripción abstracta de un grupo de objetos, cada uno de los cuales se diferencia por un estado específico y es capaz de realizar una serie de operaciones.

    public class cMetodo
    {
        /*
         * Clase Método:
         * 
         * Hice esta clase para que pudieran existir métodos que regresaran un objeto de este tipo.
         * Cuando una clase regrese un objeto de tipo "Método" quiere decir que regresaran:
         * Pass = True: Si la finalidad del método se cumplio y todos sus datos fueron válidos.
         * Pass = False: Si la finalidad del método no se cumplio o algun dato no es válido
         * Message: Si la finalidad del método no se cumplio o algun dato no es valido se regresará
         * el mensaje de error para poder ser mostrado al usuario, en caso de que si fuera exitoso
         * regresara el mensaje mostrando el mensaje de que la accion fué realizada exitosamente.
         */

        // Estos son los atributos de mi clase :) para los que no saben los atributos describen el estado del objeto. 

        public bool Pass { get; set; } // Generalmente regresará TRUE o FALSE :)
        public String Message { get; set; } // Generalmente regresará un mensaje tipo STRING :)
        public object oDato { get; set; } // Valor generico 

         public cMetodo()
        {
            Pass = false;
            Message = null;
            oDato = null;
        }

         public cMetodo(bool Pass, string Message, object oDato)
        {
            this.Pass = Pass;
            this.Message = Message;
            this.oDato = oDato;
        }
    }
}