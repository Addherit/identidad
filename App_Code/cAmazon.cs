﻿// Estos son mis using, para los que no saben aquí importo las bibliotecas que voy a usar :)

using Amazon;
using Amazon.Route53;
using Amazon.Route53.Model;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;

// Este es mi namespace, si no sabes :) un namespace en su acepción más simple, es un conjunto de nombres en el cual todos los nombres son únicos.

namespace Identidad.App_Code
{
    // Esta es mi clase cAmazon, para los que no saben :) La clase es la construcción del lenguaje utilizada más frecuentemente para definir los tipos abstractos de datos en lenguajes de programación orientados a objetos. Generalmente, una clase se puede definir como una descripción abstracta de un grupo de objetos, cada uno de los cuales se diferencia por un estado específico y es capaz de realizar una serie de operaciones.

    public class cAmazon
    {
        // Estan son las variables que voy a usar en esta página :)

        #region Variables

        // Estas son las llaves de Amazon que tengo en el web.config 

        private String sAccessKey = System.Configuration.ConfigurationManager.AppSettings["AmazonAccessKey"];
        private String sSecretKey = System.Configuration.ConfigurationManager.AppSettings["AmazonSecretKey"];
        private String sBucket = System.Configuration.ConfigurationManager.AppSettings["AmazonBucket"];
        public String sRutaAmazon = "https://s3.amazonaws.com/Identidad/"; // Esta es la ruta de Identidad en Amazon BUENA :D
        //public String sRutaAmazon = "https://s3-us-west-2.amazonaws.com/identidadprueba/"; // Esta es la ruta de Identidad en Amazon DE PRUEBAS :D
        private IAmazonS3 client;

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Funciones para subir, borrar y obtener el tamaño de un archivo en amazon
        // El KeyName es un dato compuesto del IdUsuario + IdBrandSite + IdArchivo
        // El keyName es como una ruta dentro del S3 y el IdUsuario, IdBrandSite son carpetas

        #region Métodos

        // Para los que no saben primero les voy a explicar que es sobre cargar un metodo :)
        // Un método sobrecargado se utiliza para reutilizar el nombre de un método pero con diferentes argumentos (opcionalmente un tipo diferente de retorno). Las reglas para sobrecargar un método son las siguientes:
        //+ Los métodos sobrecargados debeb de cambiar la lista de argumentos.
        //+ Pueden cambiar el tipo de retorno.
        //+ Pueden cambiar el modificador de acceso.
        //+ Pueden declarar nuevas o más amplias excepciones.
        //+ Un método puede ser sobrecargado en la misma clase o en una subclase.

        // Estos dos metodos "uploadFile" estan sobre cargados y sirven para subir un archivo :) o en Ingles "UploadFile"
        // Pueden recibir el KeyName (que explico arriba que es) o su equivalente (IdUsuario + IdBrandSite + IdArchivo)
        // Esto lo sube directamente a Amazon S3
        public bool uploadFile(String sRutaArchivo, String sFolderIdUsuario, String sFolderIdBrandSite, String sIdArchivo)
        {
            return uploadFile(sRutaArchivo, sFolderIdUsuario + "/" + sFolderIdBrandSite + "/" + sIdArchivo);
        } // Subir un archivo a Amazon S3

        // Aqui aplica el comentario de arriba ya que es un método sobrecargado :)
        public bool uploadFile(String sRutaArchivo, String sKeyName)
        {
            try
            {
                // cuando este en producción se usa la region Amazon.RegionEndpoint.USEast1 cuando esta en pruebas se Amazon.RegionEndpoint.USWest2
                TransferUtility fileTransferUtility = new TransferUtility(new AmazonS3Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1));

                //Sube un archivo, usando el nombre del archivo como KeyName
                //fileTransferUtility.Upload(sRutaArchivo, sBucket);
                //Console.WriteLine("Upload 1 completed");

                //Establecer el KeyName, es decir, el identificador del archivo.
                fileTransferUtility.Upload(sRutaArchivo, sBucket, sKeyName);
                //Console.WriteLine("Upload 2 completed");

                //Upload data from a type of System.IO.Stream.
                //using (FileStream fileToUpload = new FileStream(sRutaArchivo, FileMode.Open, FileAccess.Read))
                //{
                //    fileTransferUtility.Upload(fileToUpload, sBucket, sKeyName);
                //}

                //Console.WriteLine("Upload 3 completed");

                // Establecemos las opciones avanzadas del archivo.
                TransferUtilityUploadRequest fileTransferUtilityRequest = new TransferUtilityUploadRequest
                {
                    BucketName = sBucket,
                    FilePath = sRutaArchivo,
                    StorageClass = S3StorageClass.Standard,
                    PartSize = 6291456, // Subdivide el archivo en partes de 6 MB.
                    Key = sKeyName,
                    CannedACL = S3CannedACL.PublicRead
                };

                fileTransferUtilityRequest.Metadata.Add("param1", "Value1");
                fileTransferUtilityRequest.Metadata.Add("param2", "Value2");
                fileTransferUtility.Upload(fileTransferUtilityRequest);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        // Estos dos métodos "deleteFile" estan sobrecargados
        // Puede recibir un KeyName (Que explico arriba que es) o su equivalente (IdUsuario + IdBrandSite + IdArchivo)
        // Esto lo borra directamente del Amazon S3
        public bool deleteFile(String sFolderIdUsuario, String sFolderIdBrandSite, String sIdArchivo)
        {
            return deleteFile(sFolderIdUsuario + "/" + sFolderIdBrandSite + "/" + sIdArchivo);
        } // Eliminar un archivo de Amazon S3

        // Aqui aplica el comentario de arriba ya que es un método sobrecargado :)
        public bool deleteFile(String sKeyName)
        {
            // cuando este en producción se usa la region Amazon.RegionEndpoint.USEast1 cuando esta en pruebas se Amazon.RegionEndpoint.USWest2
            using (client = new AmazonS3Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1))
            {
                DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = sBucket,
                    Key = sKeyName
                };

                try
                {
                    client.DeleteObject(deleteObjectRequest);

                    return true;
                }
                catch (AmazonS3Exception)
                {
                    return false;
                }
            }
        }

        // Este método obtiene el tamaño de un archivo en una carpeta nombrada como IdUsuario luego en una carpeta nombrada IdBrandSite, y finalmente el id del archivo
        public float ObtenerTamaño(String sFolderIdUsuario, String sFolderIdBrandSite, String sIdArchivo)
        {
            try
            {
                // cuando este en producción se usa la region Amazon.RegionEndpoint.USEast1 cuando esta en pruebas se Amazon.RegionEndpoint.USWest2
                using (client = new AmazonS3Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1))
                {
                    String sFolder = sFolderIdUsuario + "/" + sFolderIdBrandSite + "/" + sIdArchivo;
                    float fTamaño = 0;

                    ListObjectsRequest lor = new ListObjectsRequest()
                    {
                        BucketName = sBucket,
                        Prefix = sFolder,
                        Delimiter = "/"
                    };

                    ListObjectsResponse lorResponse = client.ListObjects(lor);

                    foreach (S3Object S3Object in lorResponse.S3Objects)
                    {
                        fTamaño += S3Object.Size;
                    }

                    return fTamaño;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        #endregion

        #region Métodos para crear/modificar/eliminar subdominios

        public string DescargarArchivoLocalmente(string sUrl, string sIdArchivo, string sNombre, string sExtension)
        {
            try
            {
                string keyName = sUrl.Replace("https://s3.amazonaws.com/Identidad/", "").Split('.')[0];
                string sNombreNuevo = sIdArchivo + "_" + sNombre + sExtension;
                sNombreNuevo = sNombreNuevo.Replace(' ', '_');
                string sUrlTemp = HttpRuntime.AppDomainAppPath + "Images\\Temp\\" + sNombreNuevo;
                
                if (File.Exists(sUrlTemp))
                {
                    File.Delete(sUrlTemp);
                }

                using (client = new AmazonS3Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1))
                {
                    GetObjectRequest request = new GetObjectRequest
                    {
                        BucketName = sBucket,
                        Key = keyName
                    };

                    using (GetObjectResponse response = client.GetObject(request))
                    {
                        string dest = Path.Combine(sUrlTemp);

                        if (!File.Exists(dest))
                        {
                            response.WriteResponseStreamToFile(dest);
                        }
                    }

                }

                return sNombreNuevo;
            }
            catch (Exception)
            {
                return "";
            }

        }

        private void ManejoSubdominio(string sNombre, bool bNuevoSubdominio)
        {
            // Nuestro dominio es identidad.com
            string domainName = "identidad.com.";

            //[1] Crea un Amazon Route 53 client object, iniciando sesión.
            IAmazonRoute53 route53Client = AWSClientFactory.CreateAmazonRoute53Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1);

            //[2] Create a hosted zone // La zona ya existe por lo cual comentamos el código
            //CreateHostedZoneRequest zoneRequest = new CreateHostedZoneRequest()
            //{
            //    Name = domainName,
            //    CallerReference = "my_change_request"
            //};

            //CreateHostedZoneResponse zoneResponse = route53Client.CreateHostedZone(zoneRequest);

            // Configuramos el alias target del record set.
            AliasTarget aTarget = new AliasTarget();
            aTarget.DNSName = domainName;
            aTarget.HostedZoneId = "Z2047MIYWXAYLS";
            aTarget.EvaluateTargetHealth = false;

            //[3] Create a resource record set change batch
            ResourceRecordSet recordSet = new ResourceRecordSet()
            {
                Name = sNombre + "." + domainName, 
                //TTL = 300,
                Type = RRType.A,
                AliasTarget = aTarget
                //ResourceRecords = new List<ResourceRecord> { new ResourceRecord { Value = "54.186.75.36" } }
            };

            Change change1;

            //if (bNuevoSubdominio == true)
            //{
                change1 = new Change()
                {
                    ResourceRecordSet = recordSet,
                    Action = ChangeAction.CREATE
                };
            //}
            //else
            //{
            //    change1 = new Change()
            //    {
            //        ResourceRecordSet = recordSet,
            //        Action = ChangeAction.DELETE
            //    };
            //}

            ChangeBatch changeBatch = new ChangeBatch()
            {
                Changes = new List<Change> { change1 }
            };

            // Este objeto obtiene la lista de HostedZones en Amazon
            //AmazonRoute53Client aRoute53Client = new AmazonRoute53Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1);
            //var HostedZoneId = aRoute53Client.ListHostedZones();

            //[4] Update the zone's resource record sets
            ChangeResourceRecordSetsRequest recordsetRequest = new ChangeResourceRecordSetsRequest()
            {
                HostedZoneId = "Z2047MIYWXAYLS", //zoneResponse.HostedZone.Id,
                ChangeBatch = changeBatch
            };

            ChangeResourceRecordSetsResponse recordsetResponse = route53Client.ChangeResourceRecordSets(recordsetRequest);

            //[5] Monitor the change status
            GetChangeRequest changeRequest = new GetChangeRequest()
            {
                Id = recordsetResponse.ChangeInfo.Id
            };

            // Reviza los procesos solicitados
            //while (route53Client.GetChange(changeRequest).ChangeInfo.Status == ChangeStatus.PENDING)
            //{
            //    Console.WriteLine("Change is pending.");
            //    Thread.Sleep(15000);
            //}

            //Console.WriteLine("Change is complete.");
            //Console.ReadKey();
        }

        public void CrearSubdominio(string sNombre)
        {
            try
            {
                ManejoSubdominio(sNombre, false);
            }
            catch (InvalidChangeBatchException) { } // Si no existe el subdominio no hace nada, y entonces lo crea

            //ManejoSubdominio(sNombre, true);

        } // Manda llamar el metodo "ManejoSubdominio" para que borre el subdominio y luego lo agrerge en caso de que exista

        public void EliminarSubdominio(string sNombre)
        {
            //ManejoSubdominio(sNombre, false);

            // Nuestro dominio es identidad.com
            string domainName = "identidad.com.";

            //[1] Crea un Amazon Route 53 client object, iniciando sesión.
            IAmazonRoute53 route53Client = AWSClientFactory.CreateAmazonRoute53Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1);

            //[2] Create a hosted zone // La zona ya existe por lo cual comentamos el código
            //CreateHostedZoneRequest zoneRequest = new CreateHostedZoneRequest()
            //{
            //    Name = domainName,
            //    CallerReference = "my_change_request"
            //};

            //CreateHostedZoneResponse zoneResponse = route53Client.CreateHostedZone(zoneRequest);

            // Configuramos el alias target del record set.
            AliasTarget aTarget = new AliasTarget();
            aTarget.DNSName = domainName;
            aTarget.HostedZoneId = "Z2047MIYWXAYLS";
            aTarget.EvaluateTargetHealth = false;

            //[3] Create a resource record set change batch
            ResourceRecordSet recordSet = new ResourceRecordSet()
            {
                Name = sNombre + "." + domainName,
                //TTL = 300,
                Type = RRType.A,
                AliasTarget = aTarget
                //ResourceRecords = new List<ResourceRecord> { new ResourceRecord { Value = "54.186.75.36" } }
            };

            Change change1;

            //if (bNuevoSubdominio == true)
            //{
            change1 = new Change()
            {
                ResourceRecordSet = recordSet,
                Action = ChangeAction.DELETE
            };
            //}
            //else
            //{
            //    change1 = new Change()
            //    {
            //        ResourceRecordSet = recordSet,
            //        Action = ChangeAction.DELETE
            //    };
            //}

            ChangeBatch changeBatch = new ChangeBatch()
            {
                Changes = new List<Change> { change1 }
            };

            // Este objeto obtiene la lista de HostedZones en Amazon
            //AmazonRoute53Client aRoute53Client = new AmazonRoute53Client(sAccessKey, sSecretKey, Amazon.RegionEndpoint.USEast1);
            //var HostedZoneId = aRoute53Client.ListHostedZones();

            //[4] Update the zone's resource record sets
            ChangeResourceRecordSetsRequest recordsetRequest = new ChangeResourceRecordSetsRequest()
            {
                HostedZoneId = "Z2047MIYWXAYLS", //zoneResponse.HostedZone.Id,
                ChangeBatch = changeBatch
            };

            ChangeResourceRecordSetsResponse recordsetResponse = route53Client.ChangeResourceRecordSets(recordsetRequest);

            //[5] Monitor the change status
            GetChangeRequest changeRequest = new GetChangeRequest()
            {
                Id = recordsetResponse.ChangeInfo.Id
            };

            // Reviza los procesos solicitados
            //while (route53Client.GetChange(changeRequest).ChangeInfo.Status == ChangeStatus.PENDING)
            //{
            //    Console.WriteLine("Change is pending.");
            //    Thread.Sleep(15000);
            //}

            //Console.WriteLine("Change is complete.");
            //Console.ReadKey();


        } // Manda llamar el metodo "ManejoSubdominio" para que borre el subdominio

        #endregion

    }
}