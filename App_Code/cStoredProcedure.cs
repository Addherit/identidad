﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Identidad.App_Code
{
    public class cStoredProcedure
    {
        #region Atributos

        private string sStringConnection = string.Empty;

        private SqlConnection cnn;

        private SqlCommand cmd;

        #endregion

        public cStoredProcedure(string sStoredProcedure)
        {
            crearCadenaConexion();
            conectarBaseDatos();
            conectarStoredProcedure(sStoredProcedure);
        }

        #region Métodos privados

        private void crearCadenaConexion()
        {
            sStringConnection = ConfigurationManager.ConnectionStrings["Identidad"].ConnectionString;
        }

        private void conectarBaseDatos()
        {
            try
            {
                this.cnn = new SqlConnection(sStringConnection);

                this.cnn.Open();

            }
            catch (Exception)
            {
                this.cnn = null;
            }
        }

        private void conectarStoredProcedure(string sStoredProcedure)
        {
            try
            {
                this.cmd = new SqlCommand(sStoredProcedure, this.cnn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
            }
            catch (Exception)
            {
                this.cmd = null;
            }
        }

        private bool conexionValida()
        {
            try
            {
                if (this.cnn == null)
                {
                    return false;
                }
                else
                {
                    if (this.cmd.Connection.State != ConnectionState.Open)
                    {
                        this.cmd.Connection.Open();
                    }

                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Métodos públicos

        public void cerrarBaseDatos()
        {
            this.cmd.Connection.Close();
            this.cnn.Close();
        }

        public SqlDataReader executeReader()
        {
            try
            {
                if (conexionValida())
                {
                    return this.cmd.ExecuteReader();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Object executeScalar()
        {
            try
            {
                if (conexionValida())
                {
                    return this.cmd.ExecuteScalar();
                }
                else
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                return e.ToString();
            }
            finally
            {
                cerrarBaseDatos();
            }
        }

        public void executeNonQuery()
        {
            try
            {
                if (conexionValida())
                {
                    this.cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                cerrarBaseDatos();
            }
        }

        public void agregarParametro(string nombreParametro, object valor)
        {
            // si valor es null hay que pasarle explicitamente al Stored procedure un DbNull
            valor = (valor == null) ? DBNull.Value : valor;
            this.cmd.Parameters.AddWithValue(nombreParametro, valor);
        }

        #endregion
    }
}