USE [master]
GO
/****** Object:  Database [Identidad]    Script Date: 8/27/2015 8:21:36 AM ******/
CREATE DATABASE [Identidad]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Identidad', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Identidad.mdf' , SIZE = 10240KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Identidad_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\Identidad_log.ldf' , SIZE = 76736KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Identidad] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Identidad].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Identidad] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Identidad] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Identidad] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Identidad] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Identidad] SET ARITHABORT OFF 
GO
ALTER DATABASE [Identidad] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Identidad] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Identidad] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Identidad] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Identidad] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Identidad] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Identidad] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Identidad] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Identidad] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Identidad] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Identidad] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Identidad] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Identidad] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Identidad] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Identidad] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Identidad] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Identidad] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Identidad] SET RECOVERY FULL 
GO
ALTER DATABASE [Identidad] SET  MULTI_USER 
GO
ALTER DATABASE [Identidad] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Identidad] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Identidad] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Identidad] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Identidad] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Identidad]
GO
/****** Object:  User [csIdentidad]    Script Date: 8/27/2015 8:21:36 AM ******/
CREATE USER [csIdentidad] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [csDesarrollo]    Script Date: 8/27/2015 8:21:36 AM ******/
CREATE USER [csDesarrollo] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[catArchivos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catArchivos](
	[IdArchivo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[Url] [varchar](255) NULL,
	[Extension] [varchar](255) NULL,
	[PesoBytesArchivo] [float] NULL,
	[Preview] [varchar](255) NULL,
	[PesoBytesPreview] [float] NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[ArchivoTrabajo] [bit] NOT NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catArchivos] PRIMARY KEY CLUSTERED 
(
	[IdArchivo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catBitacoraArchivos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catBitacoraArchivos](
	[IdBitacora] [int] IDENTITY(1,1) NOT NULL,
	[IdArchivo] [int] NULL,
	[IdEstatusBitacora] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catBitacoraArchivos] PRIMARY KEY CLUSTERED 
(
	[IdBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catBrandSites]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catBrandSites](
	[IdBrandSite] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Url] [varchar](255) NULL,
	[IdArchivoLogo] [int] NULL,
	[IdArchivoBannerPrincipal] [int] NULL,
	[IdArchivoBackground] [int] NULL,
	[IdArchivoHome] [int] NULL,
	[IdPlantillaHome] [int] NULL,
	[HTMLPlantillaHome] [varchar](255) NULL,
	[Bienvenida] [varchar](1020) NULL,
	[ColorBoton] [varchar](255) NULL,
	[IdUsuario] [int] NULL,
	[IdEstatus] [int] NULL,
	[FechaEstatus] [datetime] NOT NULL,
 CONSTRAINT [PK_catBrandSites] PRIMARY KEY CLUSTERED 
(
	[IdBrandSite] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catCarpetas]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catCarpetas](
	[IdCarpeta] [int] IDENTITY(1,1) NOT NULL,
	[IdCarpetaPadre] [int] NULL,
	[IdBrandSite] [int] NULL,
	[Nombre] [nchar](10) NULL,
	[CarpetaTrabajo] [bit] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catCarpetas] PRIMARY KEY CLUSTERED 
(
	[IdCarpeta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catColores]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catColores](
	[IdColor] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Hex] [varchar](255) NULL,
	[R] [int] NULL,
	[G] [int] NULL,
	[B] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catColores] PRIMARY KEY CLUSTERED 
(
	[IdColor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catColoresFavoritos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catColoresFavoritos](
	[IdColorFavorito] [int] IDENTITY(1,1) NOT NULL,
	[Hex] [varchar](255) NULL,
	[Nombre] [varchar](255) NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catColoresFavoritos] PRIMARY KEY CLUSTERED 
(
	[IdColorFavorito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catEstatus]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catEstatus](
	[IdEstatus] [int] IDENTITY(1,1) NOT NULL,
	[Estatus] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catEstatus] PRIMARY KEY CLUSTERED 
(
	[IdEstatus] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catEstatusBitacora]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catEstatusBitacora](
	[IdEstatusBitacora] [int] IDENTITY(1,1) NOT NULL,
	[Estatus] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catEstatusBitacora] PRIMARY KEY CLUSTERED 
(
	[IdEstatusBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catEstilosPorBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catEstilosPorBrandSite](
	[IdExB] [int] IDENTITY(1,1) NOT NULL,
	[IdEstilo] [int] NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[IdFuente] [int] NULL,
	[Tamano] [varchar](255) NULL,
	[Color] [varchar](255) NULL,
	[Negrita] [bit] NULL,
	[Italica] [bit] NULL,
	[Subrayada] [bit] NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catEstilosPorBrandSite] PRIMARY KEY CLUSTERED 
(
	[IdExB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catFuentes]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catFuentes](
	[IdFuente] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Ruta] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catFuentes] PRIMARY KEY CLUSTERED 
(
	[IdFuente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catGlosario]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catGlosario](
	[IdPalabra] [int] IDENTITY(1,1) NOT NULL,
	[Palabra] [varchar](255) NULL,
	[Definicion] [varchar](1020) NULL,
	[TodosMisBS] [bit] NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catGlosario] PRIMARY KEY CLUSTERED 
(
	[IdPalabra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catGrupos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catGrupos](
	[IdGrupo] [int] IDENTITY(1,1) NOT NULL,
	[IdBrandSite] [int] NULL,
	[Nombre] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catGrupos] PRIMARY KEY CLUSTERED 
(
	[IdGrupo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catGruposPorArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catGruposPorArchivo](
	[IdGxA] [int] IDENTITY(1,1) NOT NULL,
	[IdGrupo] [int] NULL,
	[IdArchivo] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catGruposPorArchivo] PRIMARY KEY CLUSTERED 
(
	[IdGxA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catIndiceManual]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catIndiceManual](
	[IdIndice] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[IdIndicePadre] [int] NULL,
	[IdPlantilla] [int] NULL,
	[HTMLPlantilla] [varchar](max) NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
	[Orden] [int] NULL,
	[TituloPlantilla] [varchar](max) NULL,
	[TextoPlantilla] [varchar](max) NULL,
	[urlImagenPlantilla] [varchar](max) NULL,
 CONSTRAINT [PK_catIndiceManual] PRIMARY KEY CLUSTERED 
(
	[IdIndice] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catLog]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catLog](
	[IdLog] [int] IDENTITY(1,1) NOT NULL,
	[Exception] [varchar](max) NULL,
	[IdUsuario] [int] NULL,
	[FechaException] [datetime] NOT NULL,
 CONSTRAINT [PK_catLog] PRIMARY KEY CLUSTERED 
(
	[IdLog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catPlantillas]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catPlantillas](
	[IdPlantilla] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Preview] [varchar](255) NULL,
	[HTML] [varchar](max) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catPlantillas] PRIMARY KEY CLUSTERED 
(
	[IdPlantilla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catTags]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catTags](
	[IdTag] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Descripcion] [varchar](1020) NULL,
	[IdArchivo] [int] NULL,
	[Extension] [varchar](255) NULL,
	[TodosMisBS] [bit] NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catTags] PRIMARY KEY CLUSTERED 
(
	[IdTag] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catTagsPorArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catTagsPorArchivo](
	[IdTxA] [int] IDENTITY(1,1) NOT NULL,
	[IdTag] [int] NULL,
	[IdArchivo] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catTagsPorArchivo] PRIMARY KEY CLUSTERED 
(
	[IdTxA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catTamanos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catTamanos](
	[IdTamano] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catTamano] PRIMARY KEY CLUSTERED 
(
	[IdTamano] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catTiposCuenta]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catTiposCuenta](
	[IdTipoCuenta] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[UrlImagen] [varchar](255) NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catTiposCuenta] PRIMARY KEY CLUSTERED 
(
	[IdTipoCuenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catTiposEstilos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catTiposEstilos](
	[IdEstilo] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catTiposEstilos] PRIMARY KEY CLUSTERED 
(
	[IdEstilo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catTiposUsuario]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catTiposUsuario](
	[IdTipoUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Tipo] [varchar](255) NULL,
	[Descripcion] [varchar](255) NULL,
	[Url] [varchar](255) NULL,
	[Activo] [bit] NOT NULL,
	[FechaActivo] [datetime] NOT NULL,
 CONSTRAINT [PK_catTiposUsuario] PRIMARY KEY CLUSTERED 
(
	[IdTipoUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[catUsuarios](
	[IdUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](255) NULL,
	[ApellidoPaterno] [varchar](255) NULL,
	[ApellidoMaterno] [varchar](255) NULL,
	[CorreoElectronico] [varchar](255) NULL,
	[Contraseña] [varchar](255) NULL,
	[Foto] [varchar](255) NULL,
	[Telefono] [varchar](255) NULL,
	[IdTipoCuenta] [int] NULL,
	[IdUsuarioActualiza] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catUsuarios] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[catUsuariosPorBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catUsuariosPorBrandSite](
	[IdUxB] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NULL,
	[IdTipoUsuario] [int] NULL,
	[IdBrandSite] [int] NULL,
	[IdUsuarioActualiza] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catUsuariosPorBrandSite] PRIMARY KEY CLUSTERED 
(
	[IdUxB] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catUsuariosPorGrupos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catUsuariosPorGrupos](
	[IdUxG] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NULL,
	[IdGrupo] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catUsuariosPorGrupos] PRIMARY KEY CLUSTERED 
(
	[IdUxG] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[catUsuariosPorUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catUsuariosPorUsuarios](
	[IdUxU] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuarioRegistra] [int] NULL,
	[IdUsuario] [int] NULL,
	[Activo] [bit] NULL,
	[FechaActivo] [datetime] NULL,
 CONSTRAINT [PK_catUsuariosPorUsuarios] PRIMARY KEY CLUSTERED 
(
	[IdUxU] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vwUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vwUsuarios]
AS

	SELECT 

		U.IdUsuario AS 'IdUsuario',
		U.Nombre + ' ' + U.ApellidoPaterno + ' ' +  U.ApellidoMaterno AS Nombre, 
		U.CorreoElectronico AS 'Correo electrónico', 
		U.Contraseña,
		U.Telefono AS 'Teléfono', 
		CASE WHEN U.Foto = 'https://s3.amazonaws.com/Identidad/Images/ID.PNG' THEN 'Sin foto' ELSE U.Foto END AS 'Foto de perfil',
		ISNULL(B.IdBrandSite, '') AS 'Id BrandSite',
		ISNULL(B.Nombre, 'Sin BrandSites') AS 'Nombre BrandSite', 
		ISNULL(B.Url, '_') + '.identidad.com' AS 'URL BrandSite', 
		ISNULL(E.Estatus, 'Sin BrandSites') AS 'Estatus BrandSite'

	FROM catUsuarios U

	LEFT JOIN catBrandSites B ON B.IdUsuario = U.IdUsuario
	LEFT JOIN catEstatus E ON E.IdEstatus = B.IdEstatus AND E.Activo = 1

	WHERE U.Activo = 1




GO
/****** Object:  StoredProcedure [dbo].[catCarpeta_Delete]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[catCarpeta_Delete]
(
	@IdCarpeta INT = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	BEGIN TRANSACTION;
		
		-- Update al estatus y a la fecha

			UPDATE catCarpetas 
			SET Activo = 0, FechaActivo = GETDATE()
			WHERE IdCarpeta = @IdCarpeta

			SELECT CAST(1 AS BIT) AS Exito, 'Carpeta eliminada' AS Mensaje, @IdCarpeta AS Dato;
		
	COMMIT TRANSACTION;
		
	END TRY
	BEGIN CATCH
		
		SELECT CAST(0 AS BIT) AS Exito, ERROR_MESSAGE() AS Mensaje, CAST(0 AS INT) AS Dato; 

		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION;

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[catCarpeta_Insert]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[catCarpeta_Insert]
(
	@IdCarpetaPadre INT = NULL,
	@IdBrandSite INT = NULL,
	@Nombre VARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	BEGIN TRANSACTION;
		
		-- Si no existe la carpeta en esa ruta con ese nombre la inserta

		IF NOT EXISTS(SELECT * FROM catCarpetas WHERE IdCarpetaPadre = @IdCarpetaPadre AND IdBrandSite = @IdBrandSite AND Nombre = @Nombre)
		BEGIN
			INSERT INTO catCarpetas (IdCarpetaPadre, IdBrandSite, Nombre, Activo, FechaActivo)
			VALUES (@IdCarpetaPadre, @IdBrandSite, @Nombre, 1, GETDATE())

			DECLARE @IdCarpeta INT = (SELECT MAX(IdCarpeta) FROM catCarpetas WHERE IdCarpetaPadre = @IdCarpetaPadre AND IdBrandSite = @IdBrandSite AND Nombre = @Nombre)

			SELECT CAST(1 AS BIT) AS Exito, 'Carpeta registrada' AS Mensaje, @IdCarpeta AS Dato;
		END
		ELSE
		BEGIN
			SELECT CAST(0 AS BIT) AS Exito, 'La carpeta ya existe' AS Mensaje, CAST(0 AS INT) AS Dato;
		END

	COMMIT TRANSACTION;
		
	END TRY
	BEGIN CATCH
		
		SELECT CAST(0 AS BIT) AS Exito, ERROR_MESSAGE() AS Mensaje, CAST(0 AS INT) AS Dato; 

		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION;

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[catCarpeta_Update]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[catCarpeta_Update]
(
	@IdCarpeta INT = NULL,
	@IdCarpetaPadre INT = NULL,
	@Nombre VARCHAR(255) = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	BEGIN TRANSACTION;
		
		-- Si no existe la carpeta en esa ruta con ese nombre la inserta

		IF NOT EXISTS(SELECT * FROM catCarpetas WHERE IdCarpetaPadre = @IdCarpetaPadre AND Nombre = @Nombre)
		BEGIN

			UPDATE catCarpetas 
			SET IdCarpetaPadre = @IdCarpetaPadre, Nombre = @Nombre
			WHERE IdCarpeta = @IdCarpeta

			SELECT CAST(1 AS BIT) AS Exito, 'Carpeta registrada' AS Mensaje, @IdCarpeta AS Dato;
		END
		ELSE
		BEGIN
			SELECT CAST(0 AS BIT) AS Exito, 'La carpeta ya existe' AS Mensaje, CAST(0 AS INT) AS Dato;
		END

	COMMIT TRANSACTION;
		
	END TRY
	BEGIN CATCH
		
		SELECT CAST(0 AS BIT) AS Exito, ERROR_MESSAGE() AS Mensaje, CAST(0 AS INT) AS Dato; 

		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION;

	END CATCH;
END

GO
/****** Object:  StoredProcedure [dbo].[Mail_EnviarCorreo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Mail_EnviarCorreo] -- 1, 'Prueba', 'Hola'
(	
	@IdUsuario INT = NULL,
	@Asunto VARCHAR(255) = NULL,
	@Texto VARCHAR (MAX)
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP permite enviar un correo electrónico a un usuario registrado en el sitio, con asunto y mensaje
		-- Usara un formato general y podrá ser consumido por cualquier función que tenga que enviar correos

		DECLARE @HTML VARCHAR(MAX), @Mensaje VARCHAR(MAX), @Nombre VARCHAR(255), @Para VARCHAR(255)

		SELECT @Nombre = U.Nombre, @Para = U.CorreoElectronico
		FROM catUsuarios U
		WHERE U.IdUsuario = @IdUsuario
		
		SET @HTML = 
		'<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml">
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			<title>Identidad</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		</head>
		<body style="margin: 0; padding: 0;">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td style="padding: 10px 0 30px 0;">
						<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #E3FFFF; border-collapse: collapse;">
							<tr>
								<td align="left" bgcolor="#E3FFFF" style="padding: 20px 20px 20px 20px; color: #ffffff; font-size: 28px; font-weight: bold; font-family: Open Sans , sans-serif; border:none;">
									<div style="width:65px; min-width:65px; height:65px; min-height:65px;">
										<a href="http://identidad.com" style="width:146px; min-width:146px; height:26px; min-height:26px;">
											<img src="http://identidad.com/Images/Site/LogoPrincipal.png" alt="Identidad" width="146" height="26" style="display: block; width:146px; min-width:146px; height:26px; min-height:26px;" />
										</a>
									</div>
								</td>
							</tr>
							<tr>
								<td bgcolor="#E3FFFF" style="padding: 40px 30px 40px 30px;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td style="color: #000; font-family: Open Sans , sans-serif; font-size: 24px;">
												<b>Hola @Nombre!</b>
											</td>
										</tr>
										<tr>
											<td style="padding: 20px 0 30px 0; color: #000; font-family: Open Sans , sans-serif; font-size: 16px; line-height: 20px;">
												@Texto
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td bgcolor="#E3FFFF" style="padding: 30px 30px 30px 30px;">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td style="color: #ffffff; font-family: Open Sans , sans-serif; font-size: 8px;" width="75%">
												<a href="http://identidad.com" style="color: #000; text-decoration:none;">&reg; 2015 Identidad.com. All rights Reserved.</a><br />
											</td>
											<td align="right" width="25%">
												<table border="0" cellpadding="0" cellspacing="0">
													<tr>
														<td style="font-family: Open Sans , sans-serif; font-size: 12px; font-weight: bold;"></td>
														<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
														<td style="font-family: Open Sans , sans-serif; font-size: 12px; font-weight: bold;"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</body>
		</html>'

		SET @Mensaje = @HTML

		SELECT @Mensaje = replace(@Mensaje, '@Nombre', @Nombre)
		SELECT @Mensaje = replace(@Mensaje, '@Texto', @Texto)

		EXEC        msdb.dbo.sp_send_dbmail @profile_name='Identidad',
					@recipients = @Para,
					-- @blind_copy_recipients = 'sitioidentidad@gmail.com;',
					@subject = @Asunto,
					@body = @Mensaje,
					@body_format = 'HTML'

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[Mail_EnviarCorreoContacto]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Mail_EnviarCorreoContacto] -- 1, 'Esto es una prueba', 11
(
	@IdUsuarioEmisor INT = NULL,
	@Mensaje VARCHAR(MAX) = NULL,
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP manda un correo electrónico al contacto principal de un brand site, enviado por un usuario consumidor de la identidad

		DECLARE @BrandSite VARCHAR(255), @IdUsuarioReceptor INT, @Asunto VARCHAR(255), @NombreEmisor VARCHAR(255), @CorreoEmisor VARCHAR(255)

		SELECT @IdUsuarioReceptor = BS.IdUsuario, @BrandSite = LTRIM(RTRIM(BS.Nombre))
		FROM catBrandSites BS WHERE BS.IdBrandSite = @IdBrandSite

		SELECT @NombreEmisor = U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, ''), @CorreoEmisor = RTRIM(U.CorreoElectronico)
		FROM catUsuarios U WHERE U.IdUsuario = @IdUsuarioEmisor

		SET @Asunto = '¡Nuevos comentarios de tu sitio ' + @BrandSite + '!'
		SET @Mensaje = 'Tienes nuevos comentarios de tu sitio <b>' + @BrandSite + '</b>, por parte de <b>' + @NombreEmisor + '</b> con el siguiente contenido: <br><br>' 
		+ '- <i>' + LTRIM(RTRIM(@Mensaje)) + '</i><br><br>' +
		'<a href="mailto:' + @CorreoEmisor + '?Subject=Identidad%20' + @BrandSite + '" target="_top"> Responde a ' + @NombreEmisor + '</a>'

		EXEC Mail_EnviarCorreo @IdUsuarioReceptor, @Asunto, @Mensaje

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[Mail_NotificaReemplazo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Mail_NotificaReemplazo] -- 1382, 'hola'
(
	@IdArchivoModificado INT = 0,
	@Texto VARCHAR(MAX) = ''
)
AS

	DECLARE 
		@Mensaje VARCHAR(MAX), 
		@IdUsuario INT, 
		@Nombre VARCHAR(255), 
		@Descripcion VARCHAR(255), 
		@Url VARCHAR(255), 
		@Extension VARCHAR(255), 
		@Preview VARCHAR(255), 
		@IdBrandSite INT,
		@BrandSite VARCHAR(255),
		@Asunto VARCHAR(255)

	SELECT @Nombre = A.Nombre, @Descripcion = A.Descripcion, @Url = A.Url, @Extension = A.Extension, @Preview = A.Preview, @IdBrandSite = A.IdBrandSite, @BrandSite = BS.Nombre
	FROM catArchivos A
	INNER JOIN catBrandSites BS ON BS.IdBrandSite = A.IdBrandSite
	WHERE A.IdArchivo = @IdArchivoModificado

	IF @Preview = ''
	BEGIN
		IF @Extension IN ('.png', '.jpg', '.bmp', '.gif', '.jpeg')
		BEGIN
			SET @Preview = @Url
		END
		ELSE
		BEGIN
			SET @Preview = '../Images/Site/ID.png'
		END
	END

	SET @Asunto = 'Se le notifica cambios en la identidad corporativa de ' + @BrandSite

	SET @Mensaje = 
		'Se le notifica cambios en la identidad corporativa de <b>' + @BrandSite + '</b>.<br><br>' +
		@Texto + '<br><br><br>' +
		'<b>' +@Nombre + '</b><br>' +
		'' + @Descripcion + '<br><br>' +
		'<div style="width:100%; text-align:center;"><a href="' + @Url + '"><img src="' + @Preview + '" width="100px"></a></div>'

	-- Comienza ciclo para enviar el correo a cada consumidor de identidad del brand site que tenga permiso, es decir que se encuentre en un grupo
	DECLARE NotificaReemplazo CURSOR FOR

		SELECT DISTINCT IdUsuario FROM 
		(
			SELECT U.IdUsuario
			FROM catUsuariosPorGrupos UxG
			INNER JOIN catGruposPorArchivo GxA ON GxA.IdGrupo = UxG.IdGrupo AND GxA.Activo = 1 AND GxA.IdArchivo = @IdArchivoModificado
			INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdUsuario = UxG.IdUsuario AND UxB.Activo = 1 AND UxB.IdTipoUsuario = 3 AND UxB.IdBrandSite = @IdBrandSite
			INNER JOIN catUsuarios U ON U.IdUsuario = UxG.IdUsuario AND U.Activo = 1
			WHERE UxG.Activo = 1

			UNION ALL

			SELECT U.IdUsuario
			FROM catGruposPorArchivo GxA
			INNER JOIN catArchivos A ON A.IdArchivo = GxA.IdArchivo AND A.Activo = 1
			INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdBrandSite = A.IdBrandSite AND UxB.IdTipoUsuario = 3 AND UxB.IdBrandSite = @IdBrandSite AND UxB.Activo = 1
			INNER JOIN catUsuarios U ON U.IdUsuario = UxB.IdUsuario AND U.Activo = 1
			WHERE GxA.Activo = 1 AND GxA.IdArchivo = @IdArchivoModificado AND GxA.IdGrupo = 0

		) AS UsuariosConPermiso

	OPEN NotificaReemplazo
	FETCH NEXT FROM NotificaReemplazo
	INTO @IdUsuario
	WHILE (@@Fetch_Status = 0)
	BEGIN

		-- Envia correo a cada usuario
		EXEC Mail_EnviarCorreo @IdUsuario, @Asunto, @Mensaje
	
	FETCH NEXT FROM NotificaReemplazo		
	INTO @IdUsuario
	CONTINUE
	END

	CLOSE NotificaReemplazo
	DEALLOCATE NotificaReemplazo

GO
/****** Object:  StoredProcedure [dbo].[Mail_RecuperarContraseña]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Mail_RecuperarContraseña]
(
	@CorreoElectronico VARCHAR(255) = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

	-- Este SP recupera la contraseña de un usuario, enviando la contraseña al correo electrónico del usuario solicitante

	DECLARE @IdUsuario INT, @Contraseña VARCHAR(255), @Texto VARCHAR(MAX)

	SELECT TOP 1 @IdUsuario = U.IdUsuario, @Contraseña = U.Contraseña
	FROM catUsuarios U
	WHERE U.CorreoElectronico = @CorreoElectronico

	SET @Texto = 
	'Te otorgamos tu usuario y contraseña para Identidad.com <br><br>' +
	'Usuario: ' + @CorreoElectronico + '<br>' +
	'Contraseña: ' + @Contraseña 

	EXEC Mail_EnviarCorreo @IdUsuario, 'Usuario y contraseña Identidad.com', @Texto
	
	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selArchivos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selArchivos]
(
	@IdBrandSite INT = NULL,
	@Nombre VARCHAR(255) = '',
	@ArchivoTrabajo VARCHAR(1) = '%',
	@IdUsuarioBusca INT = 0
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP carga los archivos de un brand site determinado con la opción a realizar una búsqueda por el nombre

		IF EXISTS (SELECT B.IdUsuario FROM catBrandSites B INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdUsuario = B.IdUsuario AND UxB.IdTipoUsuario = 2 AND UxB.IdBrandSite = @IdBrandSite AND UxB.Activo = 1 WHERE B.IdBrandSite = @IdBrandSite AND B.IdUsuario = @IdUsuarioBusca)
		BEGIN
		
			SELECT A.Nombre, A.IdArchivo, A.Descripcion, A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS Url, LOWER(A.Extension) AS Extension,
			CASE
				WHEN A.PesoBytesArchivo > 1073741824 THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1073741824), 0)), 2)) + ' Gb'
				WHEN A.PesoBytesArchivo > 1048576	THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1048576), 0)), 2)) + ' Mb'
				WHEN A.PesoBytesArchivo > 1024 		THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1024), 0)), 2)) + ' Kb'
				WHEN A.PesoBytesArchivo > 1 			THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1), 0)), 2)) + ' Byte'
			END AS PesoBytesArchivo, 
			CASE 
				WHEN A.Preview = '' 
				THEN 
					CASE
						WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') ELSE '../Images/Site/ID.png' END
				ELSE A.Preview + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0')
				END AS Preview, 
			A.PesoBytesPreview, A.IdBrandSite, A.IdUsuario, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor, A.FechaActivo, A.ArchivoTrabajo, A.PesoBytesArchivo AS Bytes
			FROM catArchivos A
			INNER JOIN catUsuarios U ON U.IdUsuario = A.IdUsuario
			WHERE A.Activo = 1 AND A.IdBrandSite = @IdBrandSite AND A.Nombre + A.Extension LIKE '%' + @Nombre + '%' AND A.ArchivoTrabajo LIKE @ArchivoTrabajo

		END
		ELSE
		BEGIN

				SELECT A.Nombre, A.IdArchivo, A.Descripcion, A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS Url, LOWER(A.Extension) AS Extension,
				CASE
					WHEN A.PesoBytesArchivo > 1073741824 THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1073741824), 0)), 2)) + ' Gb'
					WHEN A.PesoBytesArchivo > 1048576	THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1048576), 0)), 2)) + ' Mb'
					WHEN A.PesoBytesArchivo > 1024 		THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1024), 0)), 2)) + ' Kb'
					WHEN A.PesoBytesArchivo > 1 			THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1), 0)), 2)) + ' Byte'
				END AS PesoBytesArchivo, 
				CASE 
					WHEN A.Preview = '' 
					THEN 
						CASE
							WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') ELSE '../Images/Site/ID.png' END
					ELSE A.Preview + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0')
					END AS Preview, 
				A.PesoBytesPreview, A.IdBrandSite, A.IdUsuario, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor, A.FechaActivo, A.ArchivoTrabajo, A.PesoBytesArchivo AS Bytes
				FROM catUsuariosPorGrupos UxG 
				INNER JOIN catGrupos G ON G.IdGrupo = UxG.IdGrupo AND G.IdBrandSite = @IdBrandSite AND G.Activo = 1
				INNER JOIN catGruposPorArchivo GxA ON GxA.IdGrupo = UxG.IdGrupo AND GxA.Activo = 1
				INNER JOIN catArchivos A ON A.IdArchivo = GxA.IdArchivo AND A.Activo = 1
				INNER JOIN catUsuarios U ON U.IdUsuario = A.IdUsuario AND U.Activo = 1
				WHERE UxG.IdUsuario = @IdUsuarioBusca AND UxG.Activo = 1 AND A.IdBrandSite = @IdBrandSite AND A.Nombre + A.Extension LIKE '%' + @Nombre + '%' AND A.ArchivoTrabajo LIKE @ArchivoTrabajo

				UNION ALL

				SELECT A.Nombre, A.IdArchivo, A.Descripcion, A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS Url, LOWER(A.Extension) AS Extension,
				CASE
					WHEN A.PesoBytesArchivo > 1073741824 THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1073741824), 0)), 2)) + ' Gb'
					WHEN A.PesoBytesArchivo > 1048576	THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1048576), 0)), 2)) + ' Mb'
					WHEN A.PesoBytesArchivo > 1024 		THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1024), 0)), 2)) + ' Kb'
					WHEN A.PesoBytesArchivo > 1 			THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((A.PesoBytesArchivo / 1), 0)), 2)) + ' Byte'
				END AS PesoBytesArchivo, 
				CASE 
					WHEN A.Preview = '' 
					THEN 
						CASE
							WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') ELSE '../Images/Site/ID.png' END
					ELSE A.Preview + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0')
					END AS Preview, 
				A.PesoBytesPreview, A.IdBrandSite, A.IdUsuario, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor, A.FechaActivo, A.ArchivoTrabajo, A.PesoBytesArchivo AS Bytes
				FROM catGruposPorArchivo GxA
				INNER JOIN catArchivos A ON A.IdArchivo = GxA.IdArchivo AND A.IdBrandSite = @IdBrandSite
				INNER JOIN catUsuarios U ON U.IdUsuario = A.IdUsuario AND U.Activo = 1
				WHERE GxA.IdGrupo = 0 and GxA.Activo = 1

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selBitacoraArchivos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selBitacoraArchivos]
(
	@IdBrandSite INT = NULL,
	@IdUsuarioBusca INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
	
		-- Este SP consulta la bitácora de los archivos, es decir la sección de novedades para el sitio
		-- Muestra todas las altas/bajas/cambios

		IF EXISTS (SELECT B.IdUsuario FROM catBrandSites B INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdUsuario = B.IdUsuario AND UxB.IdTipoUsuario = 2 AND UxB.IdBrandSite = @IdBrandSite AND UxB.Activo = 1 WHERE B.IdBrandSite = @IdBrandSite AND B.IdUsuario = @IdUsuarioBusca)
		BEGIN
			SELECT TOP 100
			CASE 
			WHEN A.Activo = 0
			THEN '../Images/Site/ID.png' 
			ELSE 
				CASE 
				WHEN A.Preview = '' 
				THEN 
					CASE
						WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url ELSE '../Images/Site/ID.png' END
				ELSE A.Preview 
				END 
			END AS Foto,  
			EB.Descripcion + ' <a href="' + CASE WHEN A.Activo = 1 THEN A.Url ELSE '../Images/Site/ID.png' END + '" target="_blank" >' + A.Nombre + '</a>, hace ' + 
			CONVERT(VARCHAR,	CASE 
									WHEN (DATEDIFF(MONTH, BA.FechaActivo, GETDATE()) < 1) 
									THEN (	
										CASE 
											WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()) < 1) 
											THEN (CONVERT(VARCHAR, (DATEDIFF(MINUTE, BA.FechaActivo, GETDATE()))) + ' minutos...') 
											ELSE (	CASE 
													WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE())) > 23 
													THEN CONVERT(VARCHAR, (DATEDIFF(DAY, BA.FechaActivo, GETDATE()))) + ' días...'
													ELSE CONVERT(VARCHAR, (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()))) + ' horas...' 
													END) 
											END) 
									ELSE ' más de un mes...' END) AS Novedad, 
			BA.FechaActivo, A.Nombre, A.Descripcion, A.Extension, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor
			FROM catBitacoraArchivos BA
			INNER JOIN catArchivos A ON BA.IdArchivo = A.IdArchivo AND A.IdBrandSite = @IdBrandSite AND A.Activo = 1
			INNER JOIN catUsuarios U ON U.IdUsuario = BA.IdUsuario 
			INNER JOIN catEstatusBitacora EB ON EB.IdEstatusBitacora = BA.IdEstatusBitacora AND EB.Activo = 1
			WHERE BA.FechaActivo > DATEADD(MONTH, - 1, GETDATE())
			ORDER BY BA.FechaActivo DESC
		END
		ELSE
		BEGIN

			SELECT Foto, Novedad, FechaActivo, Nombre, Descripcion, Extension, Autor 
			FROM (

				SELECT TOP 100
				CASE 
				WHEN A.Activo = 0
				THEN '../Images/Site/ID.png' 
				ELSE 
					CASE 
					WHEN A.Preview = '' 
					THEN 
						CASE
							WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url ELSE '../Images/Site/ID.png' END
					ELSE A.Preview 
					END 
				END AS Foto,  
				EB.Descripcion + ' <a href="' + CASE WHEN A.Activo = 1 THEN A.Url ELSE '../Images/Site/ID.png' END + '" target="_blank" >' + A.Nombre + '</a>, hace ' + 
				CONVERT(VARCHAR,	CASE 
										WHEN (DATEDIFF(MONTH, BA.FechaActivo, GETDATE()) < 1) 
										THEN (	
											CASE 
												WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()) < 1) 
												THEN (CONVERT(VARCHAR, (DATEDIFF(MINUTE, BA.FechaActivo, GETDATE()))) + ' minutos...') 
												ELSE (	CASE 
														WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE())) > 23 
														THEN CONVERT(VARCHAR, (DATEDIFF(DAY, BA.FechaActivo, GETDATE()))) + ' días...'
														ELSE CONVERT(VARCHAR, (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()))) + ' horas...' 
														END) 
												END) 
										ELSE ' más de un mes...' END) AS Novedad, 
				BA.FechaActivo, A.Nombre, A.Descripcion, A.Extension, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor
				FROM catBitacoraArchivos BA
				INNER JOIN catArchivos A ON BA.IdArchivo = A.IdArchivo AND A.IdBrandSite = @IdBrandSite AND A.Activo = 1
				INNER JOIN catUsuarios U ON U.IdUsuario = BA.IdUsuario 
				INNER JOIN catEstatusBitacora EB ON EB.IdEstatusBitacora = BA.IdEstatusBitacora AND EB.Activo = 1
				INNER JOIN catGruposPorArchivo GxA ON GxA.IdArchivo = A.IdArchivo AND GxA.Activo = 1
				INNER JOIN catUsuariosPorGrupos UxG ON UxG.IdGrupo = GxA.IdGrupo AND UxG.Activo = 1 AND UxG.IdUsuario = @IdUsuarioBusca
				WHERE BA.FechaActivo > DATEADD(MONTH, - 1, GETDATE())
	
				UNION ALL

				SELECT TOP 100
				CASE 
				WHEN A.Activo = 0
				THEN '../Images/Site/ID.png' 
				ELSE 
					CASE 
					WHEN A.Preview = '' 
					THEN 
						CASE
							WHEN A.Extension in ('.png', '.jpg', '.bmp', '.gif', '.jpeg') THEN A.Url ELSE '../Images/Site/ID.png' END
					ELSE A.Preview 
					END 
				END AS Foto,  
				EB.Descripcion + ' <a href="' + CASE WHEN A.Activo = 1 THEN A.Url ELSE '../Images/Site/ID.png' END + '" target="_blank" >' + A.Nombre + '</a>, hace ' + 
				CONVERT(VARCHAR,	CASE 
										WHEN (DATEDIFF(MONTH, BA.FechaActivo, GETDATE()) < 1) 
										THEN (	
											CASE 
												WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()) < 1) 
												THEN (CONVERT(VARCHAR, (DATEDIFF(MINUTE, BA.FechaActivo, GETDATE()))) + ' minutos...') 
												ELSE (	CASE 
														WHEN (DATEDIFF(HOUR, BA.FechaActivo, GETDATE())) > 23 
														THEN CONVERT(VARCHAR, (DATEDIFF(DAY, BA.FechaActivo, GETDATE()))) + ' días...'
														ELSE CONVERT(VARCHAR, (DATEDIFF(HOUR, BA.FechaActivo, GETDATE()))) + ' horas...' 
														END) 
												END) 
										ELSE ' más de un mes...' END) AS Novedad, 
				BA.FechaActivo, A.Nombre, A.Descripcion, A.Extension, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Autor
				FROM catBitacoraArchivos BA
				INNER JOIN catArchivos A ON BA.IdArchivo = A.IdArchivo AND A.IdBrandSite = @IdBrandSite AND A.Activo = 1
				INNER JOIN catUsuarios U ON U.IdUsuario = BA.IdUsuario 
				INNER JOIN catEstatusBitacora EB ON EB.IdEstatusBitacora = BA.IdEstatusBitacora AND EB.Activo = 1
				INNER JOIN catGruposPorArchivo GxA ON GxA.IdArchivo = A.IdArchivo AND GxA.Activo = 1 AND GxA.IdGrupo = 0
				WHERE BA.FechaActivo > DATEADD(MONTH, - 1, GETDATE())
	
			) AS Historial
			ORDER BY FechaActivo DESC

		END
		

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selBrandSite]
(
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra los brand sites de un usuario determinado y calcula el peso que tiene cada uno, la liga externa y cantidad de usuarios consumidores

		SELECT IdBrandSite, Nombre, UrlText, UrlValue, Estatus, PesoBytes,
		CASE
			WHEN PesoBytes > 1073741824 THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((PesoBytes / 1073741824), 0)), 2)) + ' Gb'
			WHEN PesoBytes > 1048576	THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((PesoBytes / 1048576), 0)), 2)) + ' Mb'
			WHEN PesoBytes > 1024 		THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((PesoBytes / 1024), 0)), 2)) + ' Kb'
			WHEN PesoBytes > 1 			THEN CONVERT(VARCHAR, ROUND( CONVERT(FLOAT, ISNULL((PesoBytes / 1), 0)), 2)) + ' Byte'
		END AS PesoConvertido, 
		CantidadConsumidores,
		ISNULL(UrlLogo, '../Images/Site/ID.png') AS UrlLogo
		FROM 
		(
			SELECT DISTINCT BS.IdBrandSite, BS.Nombre,
			'http://' + BS.Url + '.identidad.com' AS UrlText,
			'http://identidad.com/Brandsite/Home.aspx?id=' + CONVERT(VARCHAR, BS.IdBrandSite) AS UrlValue, 
			E.Estatus + ' en ' + CONVERT(VARCHAR, BS.FechaEstatus) AS Estatus, 
		
			(ISNULL((SELECT SUM(A.PesoBytesArchivo) FROM catArchivos A WHERE Activo = 1 AND IdBrandSite = BS.IdBrandSite), 0) + ISNULL((SELECT SUM(A.PesoBytesPreview) FROM catArchivos A WHERE Activo = 1 AND IdBrandSite = BS.IdBrandSite), 0)) AS PesoBytes,
		
			(SELECT COUNT(UxB2.IdUsuario) FROM catUsuariosPorBrandSite UxB2
			WHERE UxB2.Activo = 1 AND UxB2.IdUsuario != @IdUsuario AND UxB2.IdBrandSite = UxB.IdBrandSite AND UxB2.IdTipoUsuario = 3) AS CantidadConsumidores,
			
			ALogo.Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS UrlLogo
		
			FROM catUsuariosPorBrandSite UxB
			INNER JOIN catBrandSites BS ON BS.IdBrandSite = UxB.IdBrandSite AND BS.IdEstatus IN (1,3)
			INNER JOIN catEstatus E ON E.IdEstatus = BS.IdEstatus AND E.Activo = 1
			LEFT JOIN catArchivos ALogo ON ALogo.IdArchivo = BS.IdArchivoLogo AND ALogo.Activo = 1
		
			WHERE UxB.Activo = 1 AND UxB.IdBrandSite IS NOT NULL AND UxB.IdUsuario = @IdUsuario AND UxB.IdTipoUsuario = 2

		) AS BS
		ORDER BY Nombre ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selBrandSite_ObtenerId]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selBrandSite_ObtenerId] --'picselhhghin'
(
	@Nombre AS VARCHAR(255) = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP regresa el id de un brand site recibiendo como parametro su nombre

		IF EXISTS(SELECT TOP 1 IdBrandSite FROM catBrandSites WHERE RTRIM(LTRIM(Url)) = RTRIM(LTRIM(@Nombre)))
		BEGIN
			SELECT TOP 1 IdBrandSite FROM catBrandSites WHERE RTRIM(LTRIM(Url)) = RTRIM(LTRIM(@Nombre))
		END
		ELSE
		BEGIN
			SELECT 0
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selBrandSites_Template]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selBrandSites_Template]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP hace la consulta para cargar la hoja de estilo de un brand site determinado

		IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL DROP TABLE #TempTable

		CREATE TABLE #TempTable (columnname VARCHAR(255), value VARCHAR(255))

		INSERT INTO #TempTable (columnname, value)

		SELECT
			'Id' + CONVERT(VARCHAR, ExB.IdEstilo) AS columnname,
			'.Id' + CONVERT(VARCHAR, ExB.IdEstilo) + ' { ' +
				'font-family:' + ISNULL(F.Nombre, '') + '; ' +
				CASE WHEN F.Ruta IS NOT NULL THEN 'src:' + F.Ruta + '; ' ELSE ' ' END +
				'font-size:' + CONVERT(VARCHAR, ExB.Tamano) + 'px; ' +
				CASE WHEN ExB.Negrita = 1 THEN 'font-weight:bolder; ' ELSE 'font-weight:normal; ' END +
				CASE WHEN ExB.Italica = 1 THEN 'font-style:italic; ' ELSE ' '  END +
				CASE WHEN ExB.Subrayada = 1 THEN 'text-decoration:underline; ' ELSE ' ' END +
				'color: #' + ISNULL(ExB.color, 'C4BDBD') + '; ' +
			
			'}' AS value
		FROM catEstilosPorBrandSite ExB
		INNER JOIN catTiposEstilos E ON E.IdEstilo = ExB.IdEstilo AND E.Activo = 1
		INNER JOIN catFuentes F ON F.IdFuente = ExB.IdFuente AND F.Activo = 1
		WHERE ExB.IdBrandSite = @IdBrandSite AND ExB.Activo = 1

		SELECT TOP 1 
			BS.Nombre, 
			(SELECT TOP 1 value FROM #TempTable WHERE columnname = 'Id1') AS Id1,
			(SELECT TOP 1 value FROM #TempTable WHERE columnname = 'Id2') AS Id2,
			(SELECT TOP 1 value FROM #TempTable WHERE columnname = 'Id3') AS Id3,
			(SELECT TOP 1 value FROM #TempTable WHERE columnname = 'Id4') AS Id4,
			(SELECT TOP 1 value FROM #TempTable WHERE columnname = 'Id5') AS Id5,
			ISNULL(BannerSuperior.Url, '/Images/Site/CabezaIdentidad.png') AS BannerSuperior, 
			ISNULL(Background.Url, '#444') AS Background, 
			ISNULL(Home.Url, '/Images/Site/CabezaIdentidad.png') AS Home,
			ISNULL(Logo.Url, '/Images/Site/ID.png') AS Logo,
			'#' + ISNULL(BS.ColorBoton, '373E4E') AS ColorBoton 
		FROM catBrandSites BS
		LEFT JOIN catArchivos BannerSuperior ON BannerSuperior.IdArchivo = BS.IdArchivoBannerPrincipal AND BannerSuperior.Activo = 1
		LEFT JOIN catArchivos Background ON Background.IdArchivo = BS.IdArchivoBackground AND Background.Activo = 1
		LEFT JOIN catArchivos Home ON Home.IdArchivo = BS.IdArchivoHome AND Home.Activo = 1
		LEFT JOIN catArchivos Logo ON Logo.IdArchivo = BS.IdArchivoLogo AND Logo.Activo = 1
		WHERE BS.IdBrandSite = @IdBrandSite AND BS.IdEstatus != 2

		IF OBJECT_ID('tempdb..#TempTable') IS NOT NULL DROP TABLE #TempTable

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selBrandSites_UrlValida]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selBrandSites_UrlValida]
(
	@IdBrandSite INT = NULL,
	@Url VARCHAR(255) = NULL
)
AS

	-- Este SP valida si la url es válida para su uso en un brand site

	IF EXISTS (	SELECT TOP 1 IdBrandSite 
				FROM catBrandSites 
				WHERE LTRIM(RTRIM(Url)) = LTRIM(RTRIM(@Url)) 
				AND IdBrandSite != @IdBrandSite 
				AND IdEstatus IN (1, 3))
	BEGIN
		SELECT 0
	END
	ELSE
	BEGIN
		SELECT 1
	END
GO
/****** Object:  StoredProcedure [dbo].[selColores]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selColores]
(
	@Busqueda VARCHAR(255)
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Carga el listado de colores con la opción a filtrar por el nombre del color

		SET @Busqueda = '%' + @Busqueda + '%'

		SELECT C.Nombre + ' - #' + C.Hex AS Nombre,
		C.Hex, C.R, C.G, C.B, CASE WHEN ISNUMERIC(RTRIM(LTRIM(REPLACE(Nombre, 'C', '')))) = 1 THEN CONVERT(INT, RTRIM(LTRIM(REPLACE(Nombre, 'C', '')))) ELSE 0 END AS HexNumeric
		FROM catColores C
		WHERE Activo = 1 AND (C.Nombre LIKE @Busqueda OR '#' + C.Hex LIKE @Busqueda)
		ORDER BY HexNumeric ASC, C.Nombre ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selColoresFavoritos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selColoresFavoritos]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Este SP consulta los colores favoritos según el brand site.

		SELECT CF.IdColorFavorito, CF.Hex, ISNULL(CF.Nombre, '') AS Nombre
		FROM catColoresFavoritos CF
		WHERE CF.Activo = 1 AND CF.IdBrandSite = @IdBrandSite
		ORDER BY CF.Hex

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selEstatus]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selEstatus]
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra el listado de estatus activos que puede usar un usuario en el sitio.

		SELECT Estatus, IdEstatus
		FROM catEstatus
		WHERE Activo = 1 AND IdEstatus != 2
		ORDER BY Estatus ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selEstilosPorBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selEstilosPorBrandSite]
	-- Add the parameters for the stored procedure here
	@IdBrandSite INT = NULL
AS
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
		
	
      -- declare @IdBrandSite INT = 48


	    DECLARE @Default_Font AS VARCHAR(255)
		DECLARE @Default_Tam  AS VARCHAR(255)

		SELECT TOP 1 @Default_Font=Nombre FROM catFuentes WHERE activo=1
		SELECT TOP 1 @Default_Tam=Nombre  FROM catTamanos  WHERE activo=1

	--Selecciona todos los elementos del brandSite para mostrarlos en pantalla.

	SELECT   TE.IdEstilo AS IdEstilo,
			 ISNULL(EB.IdExB,'0') AS IdExB,
			 ISNULL(EB.IdUsuario,'0') as IdUsuario,
			 ISNULL(TE.Nombre,'0') AS NombreEstilo,
			 ISNULL(CF.Nombre,@Default_Font) AS Fuente,
			 ISNULL(EB.Tamano,@Default_Tam) AS Tamano,
			 ISNULL(EB.Negrita,'0') AS Negrita,
			 ISNULL(EB.Subrayada,'0') AS Subrayada,
			 ISNULL(EB.Italica,'0') AS Italica,
			 ISNULL(EB.Color,'C4BDBD') AS Color
			FROM catEstilosPorBrandSite EB
				 INNER JOIN catTiposEstilos TE ON TE.idEstilo=EB.idEstilo
				 AND EB.IdBrandSite=@IdBrandSite AND EB.Activo=1
				 INNER JOIN catFuentes CF ON EB.IdFuente=CF.IdFuente and CF.Activo=1
		WHERE TE.Activo=1 




		COMMIT TRANSACTION
		PRINT 'OK'

	END TRY
		BEGIN CATCH
			ROLLBACK
			PRINT 'FAIL'
		END CATCH
END

GO
/****** Object:  StoredProcedure [dbo].[selFuentes]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selFuentes] 
	
AS
	BEGIN
			-- Muestra las fuentes existentes y su id
			SELECT idFuente, Nombre AS Fuente 
			FROM catFuentes 
			WHERE Activo=1
			ORDER BY Nombre
	END


GO
/****** Object:  StoredProcedure [dbo].[selGlosario]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selGlosario]
(
	@Letra VARCHAR(255) = '%',
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra las palabras del glosario según un brand site

		SELECT TOP 1000 IdPalabra, Palabra, Definicion, FechaActivo, TodosMisBS FROM 
		(
			SELECT TOP 1000 G.IdPalabra, G.Palabra, G.Definicion, G.FechaActivo, G.TodosMisBS
			FROM catGlosario G
			WHERE G.Activo = 1 AND G.TodosMisBS = 1 AND G.Palabra COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI LIKE RTRIM(LTRIM(@Letra)) + '%' COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
			AND G.IdBrandSite IN (	SELECT BS.IdBrandSite 
									FROM catBrandSites BS 
									WHERE BS.IdEstatus IN (1,3) 
									AND BS.IdUsuario = (	SELECT IdUsuario 
															FROM catBrandSites BS 
															WHERE BS.IdEstatus IN (1,3) AND BS.IdBrandSite = @IdBrandSite))

			UNION ALL

			SELECT TOP 1000 G.IdPalabra, G.Palabra, G.Definicion, G.FechaActivo, G.TodosMisBS
			FROM catGlosario G
			WHERE G.Activo = 1 AND G.TodosMisBS = 0 AND G.Palabra COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI LIKE RTRIM(LTRIM(@Letra)) + '%' COLLATE SQL_LATIN1_GENERAL_CP1_CI_AI
			AND G.IdBrandSite = @IdBrandSite

		) AS Palabras
		ORDER BY Palabra ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selGrupos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selGrupos]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Muestra el listado de grupos según el bs

		SELECT G.IdGrupo, G.IdBrandSite, G.Nombre, G.Descripcion, G.FechaActivo
		FROM catGrupos G
		WHERE G.IdBrandSite = @IdBrandSite AND G.Activo = 1
		ORDER BY G.Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selGrupos_Lista]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selGrupos_Lista]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Enlista los grupos para que se seleccione el deseado por el diseñador

		SELECT Nombre, IdGrupo
		FROM
		(
			SELECT 0 AS IdGrupo, 'Todos' AS Nombre, 1 AS Orden
			UNION ALL
			SELECT G.IdGrupo, G.Nombre, 2 AS Orden
			FROM catGrupos G
			WHERE G.Activo = 1 AND G.IdBrandSite = @IdBrandSite

		) AS Grupos
		Order by Orden

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selGruposPorArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selGruposPorArchivo]
(
	@IdArchivo INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Obtenemos la lista de grupos en un archivo

		SELECT GxA.IdGxA, GxA.IdGrupo, CASE WHEN GxA.IdGrupo = 0 THEN 'Todos' ELSE G.Nombre END AS NombreGrupo, GxA.Activo
		FROM catGruposPorArchivo GxA
		LEFT JOIN catGrupos G ON G.IdGrupo = GxA.IdGrupo AND G.Activo = 1
		WHERE GxA.Activo = 1 AND  IdArchivo = @IdArchivo
		ORDER BY G.Nombre

		

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selIndiceManual]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selIndiceManual]
(
    @IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- mUESTRA los indices del menu

		SELECT I.IdIndice, I.IdIndicePadre, I.Nombre, CASE WHEN I.IdIndicePadre = 0 THEN 1 ELSE 2 END AS Padre
		FROM catIndiceManual I
		WHERE I.Activo = 1 AND I.IdBrandSite = @IdBrandSite 
		ORDER BY Orden

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selIndiceManual_Indices]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selIndiceManual_Indices]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Muestra el listado de capitulos para seleccionar el "padre".

		SELECT Nombre, IdIndice, Orden FROM (

			SELECT 0 AS IdIndice, 'Nuevo capítulo' AS Nombre, 0 AS Orden
			
			UNION ALL
			
			SELECT I.IdIndice, I.Nombre, 1 AS Orden
			FROM catIndiceManual I
			WHERE I.Activo = 1 AND I.IdBrandSite = @IdBrandSite AND IdIndicePadre = 0
			
		) AS Indices
		ORDER BY Orden

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selIndiceManualBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selIndiceManualBrandSite]
(
    @IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Muestra el listado de opción pero oculta la opción "Sin título" para que no se muestre en el sitio final.

		SELECT I.IdIndice, I.IdIndicePadre, I.Nombre, CASE WHEN I.IdIndicePadre = 0 THEN 1 ELSE 2 END AS Padre
		FROM catIndiceManual I
		WHERE I.Activo = 1 AND I.IdBrandSite = @IdBrandSite AND I.Nombre <> 'Sin título'
		ORDER BY Orden

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selIndiceManualInformacion]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selIndiceManualInformacion] 
	-- Add the parameters for the stored procedure here
	@IdIndice INT

AS
BEGIN

	-- Muestra las columnas de un id indice
	SELECT IdIndice, Nombre, HTMLPlantilla,IdIndicePadre, IdPlantilla,
	      IdBrandSite,IdUsuario,Activo,FechaActivo
			FROM catIndiceManual 
			WHERE IdIndice=@IdIndice
END

GO
/****** Object:  StoredProcedure [dbo].[selIndiceManualPrimerIndice]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selIndiceManualPrimerIndice]
	-- Add the parameters for the stored procedure here
	@IdBrandSite as int 
AS
BEGIN
	-- Muestra el primer indice del manual
	Select Top 1 IdIndice from catIndiceManual where IdBrandSite=@IdBrandSite and Activo=1
	Order by Orden 

END

GO
/****** Object:  StoredProcedure [dbo].[selInidiceManualBrandSiteDetalle]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selInidiceManualBrandSiteDetalle]
	@idBrandSite as int

AS
BEGIN
		
	DECLARE @IdIndice AS INT
	DECLARE @IdIndicePadre AS INT 
	DECLARE @Nombre AS VARCHAR(MAX)
	DECLARE @HtmlPlantilla AS VARCHAR(MAX)


		--1 CREA LA TABLA TEMPORAL PARA ENVIAR LOS DATOS EN UN SOLO DATATABLE
		    

			  IF OBJECT_ID('tempdb..#traIndiceMenuTemp') IS NOT NULL
				BEGIN
					DROP TABLE #traIndiceMenuTemp
				END

				CREATE TABLE #traIndiceMenuTemp( IdRegistro [int] IDENTITY(1,1) NOT NULL,
												  [IdIndice] [INT],
												  [Nombre] [varchar](MAX),
												  [IdIndicePadre] [INT],
												  [HTMLPlantilla] [varchar](MAX))

	   
	   --Crea el cursos que va obtener el html y los padres en orden para mostrarlo en el manual del brandiste
		DECLARE cHTML CURSOR FOR 

		  	SELECT IdIndice,replace(Nombre,'Sin título','') as Nombre,idIndicePadre,replace(HtmlPlantilla,'contenteditable="true"',' ') as HtmlPlantilla FROM [Identidad].[dbo].[catIndiceManual]
						 WHERE IdBrandSite=@idBrandSite AND Activo=1
						 AND IdIndicePadre=0 order by orden -- Forma de ordenamiento alfabeticamente (Cambiar fase 2)
		
			OPEN cHTML
			FETCH NEXT FROM cHTML
				INTO @IdIndice,@Nombre,@IdIndicePadre,@HtmlPlantilla
				WHILE (@@Fetch_Status = 0)
					BEGIN

				
				--Guardar en la tabla los padres de las plantillas
				Insert into #traIndiceMenuTemp select   @IdIndice,@Nombre,@IdIndicePadre,@HtmlPlantilla
				--Guardar en la tabla los subTitulos de las plantillas
				Insert into #traIndiceMenuTemp	SELECT   IdIndice,replace(Nombre,'Sin título','') as Nombre, idIndicePadre,replace(HtmlPlantilla,'contenteditable="true"',' ') as HtmlPlantilla FROM [Identidad].[dbo].[catIndiceManual]
					Where idIndicePadre=@IdIndice and Activo=1
					Order by Orden -- ordenado alfabeticamente
				


					FETCH NEXT FROM cHTML
					INTO  @IdIndice,@Nombre,@IdIndicePadre,@HtmlPlantilla

			CONTINUE
			END

		CLOSE cHTML
		DEALLOCATE cHTML

		--Muestra en pantalla el menu con su html ordenados
		Select * from #traIndiceMenuTemp order by idRegistro

	
END

GO
/****** Object:  StoredProcedure [dbo].[selManualIndices]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selManualIndices] 
	-- Add the parameters for the stored procedure here
	@IdBrandSite AS INT
AS
BEGIN
	
	-- Muestra el indice completo

	SELECT IdIndice, Nombre, idIndicePadre, Orden  FROM [Identidad].[dbo].catIndiceManual
																	  WHERE idBrandSite=@IdBrandSite and Activo=1
																	  ORDER BY Orden ASC
	END

GO
/****** Object:  StoredProcedure [dbo].[selPantones]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selPantones]

AS
BEGIN TRY
	BEGIN TRANSACTION

		-- muestra todos los tonos de pantones con su nombre y hexadecimal

		SELECT C.Nombre,C.HEX
		FROM catColores C
		WHERE Activo = 1
		ORDER BY C.Nombre ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selPlantillas]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selPlantillas]
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra las plantillas que se podrán usar en el manual de un brand site

		SELECT P.IdPlantilla, P.Nombre, P.Preview,P.HTML
		FROM catPlantillas P
		WHERE P.Activo = 1
		ORDER BY P.Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH



GO
/****** Object:  StoredProcedure [dbo].[selTags]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selTags]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra los tags o etiquetas para un brand site determinado para su uso en el catálogo

		SELECT IdTag, Nombre, Descripcion, IdArchivo, Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS Url, Extension, TodosMisBs FROM
		(
			SELECT T.IdTag, T.Nombre, T.Descripcion, T.IdArchivo, A.Url, T.Extension, T.TodosMisBS 
			FROM catTags T
			INNER JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1
			WHERE T.Activo = 1 AND T.TodosMisBS = 1 AND T.IdBrandSite IN (	SELECT BS.IdBrandSite 
																			FROM catBrandSites BS 
																			WHERE BS.IdEstatus IN (1,3) 
																			AND BS.IdUsuario = (SELECT IdUsuario 
																								FROM catBrandSites BS 
																								WHERE BS.IdEstatus IN (1,3) AND BS.IdBrandSite = @IdBrandSite))
			UNION ALL

			SELECT T.IdTag, T.Nombre, T.Descripcion, T.IdArchivo, A.Url, T.Extension, T.TodosMisBS 
			FROM catTags T
			INNER JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1
			WHERE T.Activo = 1 AND T.TodosMisBS = 0 AND T.IdBrandSite = @IdBrandSite

			UNION ALL

			-- Uno las etiquetas default

			SELECT T.IdTag, T.Nombre, T.Descripcion, T.IdArchivo, '../Images/Site/ID.png' AS Url, '' AS Extension, T.TodosMisBS 
			FROM catTags T
			WHERE T.Activo = 1 AND T.TodosMisBS = 0 AND T.IdArchivo = 0 AND T.IdBrandSite = @IdBrandSite

		) AS Tags
		ORDER BY Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[selTags_relacion]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selTags_relacion]
(
	@IdArchivo INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra los tags o etiquetas que tiene un archivo en un brand site dentro del módulo de descargas

		SELECT DISTINCT IdTxA, Url + '?t=' + REPLACE(CONVERT(VARCHAR, CONVERT(DECIMAL(12,7), GETDATE())), '.', '0') AS Url, Descripcion FROM 
		(
			SELECT 0 AS IdTxA, A.Url, T.Nombre + ': ' + T.Descripcion AS Descripcion
			FROM catTags T
			INNER JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1
			WHERE LTRIM(RTRIM(T.Extension)) = (SELECT TOP 1 LTRIM(RTRIM(A.Extension)) FROM catArchivos A WHERE A.IdArchivo = @IdArchivo)
			AND T.IdBrandSite = (SELECT AR.IdBrandSite FROM catArchivos AR WHERE AR.IdArchivo = @IdArchivo)
			AND T.Activo = 1

			UNION ALL

			SELECT 0 AS IdTxA, A.Url, T.Nombre + ': ' + T.Descripcion AS Descripcion
			FROM catTags T
			INNER JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1
			WHERE LTRIM(RTRIM(T.Extension)) = (SELECT TOP 1 LTRIM(RTRIM(A.Extension)) FROM catArchivos A WHERE A.IdArchivo = @IdArchivo)
			AND T.TodosMisBS = 1
			AND T.IdUsuario = (SELECT AR.IdUsuario FROM catArchivos AR WHERE AR.IdArchivo = @IdArchivo)
			AND T.Activo = 1

			UNION ALL

			SELECT TxA.IdTxA, A.Url, T.Nombre + ': ' + T.Descripcion AS Descripcion
			FROM catTagsPorArchivo TxA
			INNER JOIN catTags T ON T.IdTag = TxA.IdTag AND T.Activo = 1
			INNER JOIN catArchivos A ON A.IdArchivo = T.IdArchivo AND A.Activo = 1
			WHERE TxA.IdArchivo = @IdArchivo AND TxA.Activo = 1

		) AS Tags
		ORDER BY Descripcion


	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selTamanos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[selTamanos] 
	
AS
BEGIN

	-- Este SP consulta todos los tamaños

	SELECT idTamano,Nombre AS Tamano 
	FROM catTamanos 
	WHERE Activo=1
	Order by CONVERT(INT, Nombre)

END


GO
/****** Object:  StoredProcedure [dbo].[selTiposCuenta]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selTiposCuenta]
(
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra los tipos de cuenta que existen en el sitio de identidad.com para que los diseñadores seleccionen

		SELECT 
			TC.IdTipoCuenta, 
			CASE WHEN U.IdTipoCuenta = TC.IdTipoCuenta THEN CONVERT(Bit, 1) ELSE CONVERT(Bit, 0) END AS Seleccionada, 
			TC.Tipo, TC.Descripcion, ISNULL(TC.UrlImagen, '') AS UrlImagen
		FROM catTiposCuenta TC
		LEFT JOIN catUsuarios U ON U.IdTipoCuenta = TC.IdTipoCuenta AND U.IdUsuario = @IdUsuario
		WHERE TC.Activo = 1
		ORDER BY TC.Tipo ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_AccesoValido]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuarios_AccesoValido]
(
	@Usuario VARCHAR(255) = NULL,
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP valida si el usuario que quiere iniciar sesión en un brand site determinado tiene o no acceso,
		-- es decir si el consumidor de la identidad puede o no entrar a ese brand site.

		IF EXISTS (
			SELECT TOP 1 U.IdUsuario
			FROM catUsuarios U
			INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdUsuario = U.IdUsuario AND UxB.Activo = 1 AND UxB.IdBrandSite = @IdBrandSite
			WHERE U.CorreoElectronico = @Usuario AND U.Activo = 1)
		BEGIN
			SELECT '1'
		END
		ELSE
		BEGIN
			SELECT '0'
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_Contacto]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuarios_Contacto]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP muestra los datos del contacto para un brand site.

		SELECT TOP 1 U.IdUsuario, U.Nombre + ' ' + U.ApellidoPaterno + ' ' + U.ApellidoMaterno AS Nombre, U.CorreoElectronico, U.Foto, U.Telefono
		FROM catUsuarios U
		WHERE U.Activo = 1 
		AND U.IdUsuario = (	SELECT TOP 1 ISNULL(BS.IdUsuario, 0)
							FROM catBrandSites BS 
							WHERE BS.IdBrandSite = @IdBrandSite)

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_CorreoValido]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuarios_CorreoValido]
(
	@CorreoElectronico VARCHAR(255) = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP valida que el correo electrónico ingresado sea válido, es decir que no exista ya un registro con ese correo.

		IF EXISTS (SELECT TOP 1 U.CorreoElectronico FROM catUsuarios U 
		WHERE U.Activo = 1 AND U.CorreoElectronico = @CorreoElectronico)
		BEGIN
			SELECT 0
		END
		ELSE
		BEGIN
			SELECT 1
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_EsDiseñadorSitio]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuarios_EsDiseñadorSitio]
(
	@IdUsuario INT = 0,
	@IdBrandSite INT = 0
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a detectar si un usuario es o no el diseñador de un brand site

		IF EXISTS (	SELECT IdUxB FROM catUsuariosPorBrandSite 
					WHERE IdUsuario = @IdUsuario AND IdBrandSite = @IdBrandSite AND IdTipoUsuario = 2 AND Activo = 1)
		BEGIN
			SELECT 1 AS Diseñador
		END
		ELSE
		BEGIN
			SELECT 0 AS Diseñador
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_IniciarSesion]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuarios_IniciarSesion]
(
    @Usuario VARCHAR(255) = NULL,
    @Contraseña VARCHAR(255) = NULL
) 
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a saber si un usuario puede o no iniciar sesión en el sitio.

	   IF EXISTS
	   (
		  SELECT U.IdUsuario
		  FROM catUsuarios U
		  INNER JOIN catUsuariosPorBrandSite UxB ON UxB.IdUsuario = U.IdUsuario AND UxB.Activo = 1
		  WHERE 
			 U.Activo = 1 
		  AND LTRIM(RTRIM(U.CorreoElectronico)) = LTRIM(RTRIM(@Usuario)) 
		  AND (LTRIM(RTRIM(U.Contraseña)) COLLATE Latin1_General_CS_AS = LTRIM(RTRIM(@Contraseña)) OR RTRIM(LTRIM(@Contraseña))  COLLATE Latin1_General_CS_AS = 'Bu3n1s1m02014')
	   )
	   BEGIN
		  SELECT 1
	   END
	   ELSE
	   BEGIN
		  SELECT 0
	   END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[selUsuarios_MisUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[selUsuarios_MisUsuarios]
(
	@IdUsuario INT = NULL,
	@Nombre VARCHAR(255) = '%',
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Este SP nos carga el listado de usuarios dados de alta por un diseñador para asignarles acceso a a algún sitio

		SELECT UxU.IdUsuario, U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, '') AS Nombre, U.CorreoElectronico, U.Contraseña, ISNULL(U.Foto, '../Images/Site/ID.png') AS Foto, U.Telefono, U.FechaActivo
		FROM catUsuariosPorUsuarios UxU
		INNER JOIN catUsuarios U ON U.IdUsuario = UxU.IdUsuario
		WHERE UxU.IdUsuarioRegistra = @IdUsuario AND U.Activo = 1 AND UxU.Activo = 1 AND Nombre LIKE '%' + @Nombre + '%'
		AND UxU.IdUsuario NOT IN (SELECT UxB.IdUsuario FROM catUsuariosPorBrandSite UxB WHERE UxB.IdBrandSite = @IdBrandSite AND UxB.Activo = 1)
		ORDER BY Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selUsuariosPorBrandSite_Accesos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[selUsuariosPorBrandSite_Accesos]
(
	@IdBrandSite INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- muestra los usuarios con acceso a un bs

		SELECT UxB.IdUxB, UxB.IdUsuario, U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, '') AS Nombre, UxB.IdBrandSite, UxB.FechaActivo, U.CorreoElectronico, ISNULL(U.Foto, '../Images/Site/ID.png') AS Foto
		FROM catUsuariosPorBrandSite UxB
		INNER JOIN catUsuarios U ON U.IdUsuario = UxB.IdUsuario AND U.Activo = 1
		WHERE UxB.IdBrandSite = @IdBrandSite AND UxB.IdTipoUsuario = 3 AND UxB.Activo = 1
		AND UxB.IdUsuario != (SELECT TOP 1 ISNULL(BS.IdUsuario, 0) FROM catBrandSites BS WHERE BS.IdBrandSite = @IdBrandSite)
		ORDER BY Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selUsuariosPorBrandSite_Grupos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[selUsuariosPorBrandSite_Grupos]
(
	@IdBrandSite INT = NULL,
	@IdGrupo INT = NULL,
	@Busqueda VARCHAR(255) = '%'
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- muestra los usuarios de un bs en un grupo determinado

		SELECT UxB.IdUxB, UxB.IdUsuario, U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, '') AS Nombre, UxB.IdBrandSite, UxB.FechaActivo, U.CorreoElectronico, ISNULL(U.Foto, '../Images/Site/ID.png') AS Foto
		FROM catUsuariosPorBrandSite UxB
		INNER JOIN catUsuarios U ON U.IdUsuario = UxB.IdUsuario AND U.Activo = 1
		WHERE UxB.IdBrandSite = @IdBrandSite AND UxB.IdTipoUsuario = 3 AND UxB.Activo = 1
		AND UxB.IdUsuario != (SELECT TOP 1 ISNULL(BS.IdUsuario, 0) FROM catBrandSites BS WHERE BS.IdBrandSite = @IdBrandSite)
		AND UxB.IdUsuario NOT IN (SELECT IdUsuario FROM catUsuariosPorGrupos UxG WHERE UxG.Activo = 1 AND UxG.IdGrupo = @IdGrupo)
		AND U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, '') LIKE '%' + @Busqueda + '%'
		ORDER BY Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selUsuariosPorBrandSite_Integrantes]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[selUsuariosPorBrandSite_Integrantes]
(
	@IdGrupo INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		SELECT UxG.IdUxG, UxG.IdUsuario, U.Nombre + ' ' + ISNULL(U.ApellidoPaterno, '') + ' ' + ISNULL(U.ApellidoMaterno, '') AS Nombre, UxG.FechaActivo, ISNULL(U.Foto, '../Images/Site/ID.png') AS Foto
		FROM catUsuariosPorGrupos UxG
		INNER JOIN catUsuarios U ON U.IdUsuario = UxG.IdUsuario AND U.Activo = 1
		WHERE UxG.IdGrupo = @IdGrupo AND UxG.Activo = 1
		ORDER BY Nombre

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[selUsuariosPorBrandSite_MisSitios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[selUsuariosPorBrandSite_MisSitios]
(
    @IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos carga en la ventana de inicio todos los lugares que tenemos acceso despues de iniciar sesión

		SELECT DISTINCT Nombre, Url, Orden FROM
		(
			SELECT
			CASE 
				WHEN UxB.IdTipoUsuario = 1 THEN TU.Tipo
				WHEN UxB.IdTipoUsuario = 2 THEN TU.Tipo
				WHEN UxB.IdTipoUsuario = 3 THEN BS.Nombre
			END AS Nombre,

			CASE 
				WHEN UxB.IdTipoUsuario = 1 THEN TU.Url
				WHEN UxB.IdTipoUsuario = 2 THEN TU.Url
				WHEN UxB.IdTipoUsuario = 3 THEN '/Brandsite/Login.aspx?id=' + CONVERT(VARCHAR, BS.IdBrandSite)
			END AS Url,

			CASE
				WHEN UxB.IdTipoUsuario = 2 THEN 1 ELSE 2
			END AS Orden

			FROM catUsuariosPorBrandSite UxB
			LEFT JOIN catBrandSites BS ON BS.IdBrandSite = UxB.IdBrandSite AND BS.IdEstatus in (1) 
			LEFT JOIN catTiposUsuario TU ON TU.IdTipoUsuario = UxB.IdTipoUsuario AND TU.Activo = 1
			INNER JOIN catUsuarios U ON U.IdUsuario = UxB.IdUsuario AND U.Activo = 1
			WHERE UxB.Activo = 1 AND UxB.IdUsuario = @IdUsuario
			
	   ) AS MisSitios
	   WHERE Nombre IS NOT NULL AND Url IS NOT NULL
	   ORDER BY Orden, Nombre ASC

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traArchivo]
(
	@IdArchivo INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@Descripcion VARCHAR(255) = NULL,
	@Url VARCHAR(255) = NULL,
	@Extension VARCHAR(255) = NULL,
	@Preview VARCHAR(255) = NULL,
	@PesoBytesPreview FLOAT = NULL,
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL,
	@ArchivoTrabajo BIT, 
	@PesoBytesArchivo FLOAT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
	
	-- Este SP nos ayuda a agregar o modificar un archivo en un brand site

	IF EXISTS (SELECT TOP 1 IdArchivo FROM catArchivos WHERE IdArchivo = @IdArchivo)
	BEGIN
		UPDATE catArchivos
		SET 
			Nombre = @Nombre, 
			Descripcion = @Descripcion,
			Url = @Url,
			Extension = @Extension,
			Preview = @Preview,
			PesoBytesPreview = @PesoBytesPreview,
			IdBrandSite = @IdBrandSite,
			IdUsuario = @IdUsuario,
			PesoBytesArchivo = @PesoBytesArchivo,
			ArchivoTrabajo = @ArchivoTrabajo,
			Activo = @Activo,
			FechaActivo = GETDATE()
		WHERE IdArchivo = @IdArchivo

		-- Aqui no guardamos registro de bitácora ya que esta sección no la llama el usuario final
		-- EXEC traBitacoraArchivos @IdArchivo, 3, @IdUsuario

		SELECT @IdArchivo

	END
	ELSE
	BEGIN

		INSERT INTO catArchivos (Nombre, Descripcion, Url, Extension, Preview, PesoBytesPreview, IdBrandSite, IdUsuario, PesoBytesArchivo, ArchivoTrabajo, Activo, FechaActivo)
		VALUES (@Nombre, @Descripcion, @Url, @Extension, @Preview, @PesoBytesPreview, @IdBrandSite, @IdUsuario, @PesoBytesArchivo, @ArchivoTrabajo, @Activo, GETDATE())

		SET @IdArchivo = (SELECT ISNULL(MAX(IdArchivo), 1) FROM catArchivos WHERE Url = @Url)

		EXEC traBitacoraArchivos @IdArchivo, 1, @IdUsuario

		SELECT @IdArchivo

	END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traArchivo_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traArchivo_Eliminar]
(
	@IdArchivo INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
	
		-- Este SP nos ayuda a dar de baja un archivo y agrega un registro a la bitácora

		UPDATE catArchivos
		SET 
			IdUsuario = @IdUsuario,
			Activo = 0,
			FechaActivo = GETDATE()
		WHERE IdArchivo = @IdArchivo

		UPDATE catGruposPorArchivo
		SET
			Activo = 0,
			FechaActivo = GETDATE()
		WHERE IdArchivo = @IdArchivo

		UPDATE catTagsPorArchivo
		SET
			Activo = 0,
			FechaActivo = GETDATE()
		WHERE IdArchivo = @IdArchivo

		EXEC traBitacoraArchivos @IdArchivo, 2, @IdUsuario

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traArchivo_Modificar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traArchivo_Modificar]
(
	@IdArchivo INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@Descripcion VARCHAR(255) = NULL,
	@PesoBytesArchivo FLOAT = '',
	@UrlPreview VARCHAR(255) = '',
	@PesoBytesPreview FLOAT = '',
	@Extension VARCHAR(255) = '',
	@ArchivoTrabajo BIT = 1,
	@IdUsuario INT
)
AS
BEGIN TRY
	BEGIN TRANSACTION
	
		-- Este SP nos ayuda a modificar un archivo y insertando un registro en la bitácora.

		IF @PesoBytesArchivo != '' AND @Extension != ''
		BEGIN
			UPDATE catArchivos
			SET 
				Extension = @Extension,
				PesoBytesArchivo = @PesoBytesArchivo
			WHERE IdArchivo = @IdArchivo
		END

		IF @UrlPreview != ''
		BEGIN
			UPDATE catArchivos
			SET 
				PesoBytesPreview = @PesoBytesPreview,
				Preview = @UrlPreview
			WHERE IdArchivo = @IdArchivo
		END

		UPDATE catArchivos
		SET 
			Nombre = @Nombre, 
			Descripcion = @Descripcion,
			ArchivoTrabajo = @ArchivoTrabajo,
			FechaActivo = GETDATE()
		WHERE IdArchivo = @IdArchivo
		
		EXEC traBitacoraArchivos @IdArchivo, 3, @IdUsuario

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traBitacoraArchivos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traBitacoraArchivos]
(
	@IdArchivo INT = NULL,
	@IdEstatusBitacora INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP inserta un registro en la bitácora 

		INSERT INTO catBitacoraArchivos (IdArchivo, IdEstatusBitacora, IdUsuario, Activo, FechaActivo)
		VALUES (@IdArchivo, @IdEstatusBitacora, @IdUsuario, 1, GETDATE())

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[traBrandSites]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traBrandSites]
(
	@IdBrandSite INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@Url VARCHAR(255) = NULL,
	@IdUsuario INT = NULL,
	@IdEstatus INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar un brand site si no existe, y si existe a modificarlo

		IF EXISTS (SELECT TOP 1 IdBrandSite FROM catBrandSites WHERE IdBrandSite = @IdBrandSite)
		BEGIN

			UPDATE catBrandSites
			SET
				Nombre = @Nombre,
				Url = @Url,
				IdUsuario = @IdUsuario,
				IdEstatus = @IdEstatus,
				FechaEstatus = GETDATE()
			WHERE IdBrandSite = @IdBrandSite

			-- En caso de que no exista el permiso, lo crea ...
			IF NOT EXISTS (SELECT IdUxB FROM catUsuariosPorBrandSite WHERE IdBrandSite = @IdBrandSite AND IdUsuario = @IdUsuario)
			BEGIN
				-- Lo inserta como diseñador
				INSERT INTO catUsuariosPorBrandSite(IdUsuario, IdTipoUsuario, IdBrandSite, Activo, FechaActivo)
				VALUES (@IdUsuario, 2, @IdBrandSite, 1, GETDATE())

				-- Lo inserta como consumidor de la identidad
				INSERT INTO catUsuariosPorBrandSite(IdUsuario, IdTipoUsuario, IdBrandSite, Activo, FechaActivo)
				VALUES (@IdUsuario, 3, @IdBrandSite, 1, GETDATE())
			END

			SELECT @IdBrandSite

		END
		ELSE
		BEGIN

			INSERT INTO catBrandSites (Nombre, Url, IdUsuario, IdEstatus, FechaEstatus)
			VALUES (@Nombre, @Url, @IdUsuario, @IdEstatus, GETDATE())

			-- Obtiene el Id del brand site recien registrado
			SET @IdBrandSite = (SELECT MAX(IdBrandSite) FROM catBrandSites WHERE Url = @Url AND IdEstatus = @IdEstatus)

			-- Lo inserta como diseñador
			INSERT INTO catUsuariosPorBrandSite(IdUsuario, IdTipoUsuario, IdBrandSite, Activo, FechaActivo)
			VALUES (@IdUsuario, 2, @IdBrandSite, 1, GETDATE())

			-- Lo inserta como consumidor de la identidad
			INSERT INTO catUsuariosPorBrandSite(IdUsuario, IdTipoUsuario, IdBrandSite, Activo, FechaActivo)
			VALUES (@IdUsuario, 3, @IdBrandSite, 1, GETDATE())

			-- Inserta el indice default para el manual
			EXEC traIndiceManual_IndiceDefault @IdBrandSite, @IdUsuario

			-- Inserta los estilos para el texto
			EXEC traEstilosPorBrandSite_Default @IdBrandSite, @IdUsuario

			-- Inserta el glosario default
			EXEC traGlosario_Default @IdBrandSite, @IdUsuario

			-- Inserta los tags default
			EXEC traTags_Default @IdBrandSite, @IdUsuario

			SELECT @IdBrandSite

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traBrandSites_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traBrandSites_Eliminar]
(
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda en caso que encesitemos eliminar un brand site

		UPDATE catBrandSites
		SET IdUsuario = @IdUsuario, IdEstatus = 2, FechaEstatus = GETDATE()
		WHERE IdBrandSite = @IdBrandSite

		UPDATE catUsuariosPorBrandSite
		SET IdUsuarioActualiza = @IdUsuario, Activo = 0, FechaActivo = GETDATE()
		WHERE IdBrandSite = @IdBrandSite

		UPDATE catArchivos
		SET IdUsuario = @IdUsuario, Activo = 0, FechaActivo = GETDATE()
		WHERE IdBrandSite = @IdBrandSite

		UPDATE catGrupos
		SET Activo = 0, FechaActivo = GETDATE()
		WHERE IdBrandSite = @IdBrandSite

		UPDATE catGruposPorArchivo 
		SET Activo = 0, FechaActivo = GETDATE() 
		WHERE IdGrupo IN (SELECT IdGrupo FROM catGrupos WHERE IdBrandSite = @IdBrandSite)

		UPDATE catUsuariosPorGrupos 
		SET Activo = 0, FechaActivo = GETDATE() 
		WHERE IdGrupo IN (SELECT IdGrupo FROM catGrupos WHERE IdBrandSite = @IdBrandSite)

		UPDATE catEstilosPorBrandSite
		SET Activo = 0, FechaActivo = GETDATE()
		WHERE IdBrandSite = @IdBrandSite

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traColoresFavoritos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traColoresFavoritos]
(
	@Nombre VARCHAR(255) = NULL,
	@Hex VARCHAR(255) = NULL,
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- inserta un color como favorito si no esta insertado ya 

		IF NOT EXISTS (SELECT IdColorFavorito FROM catColoresFavoritos WHERE Hex = @Hex AND IdBrandSite = @IdBrandSite AND Activo = 1)
		BEGIN
			INSERT INTO catColoresFavoritos(Nombre, Hex, IdBrandSite, IdUsuario, Activo, FechaActivo)
			VALUES (@Nombre, @Hex, @IdBrandSite, @IdUsuario, 1, GETDATE())
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traEstilosPorBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traEstilosPorBrandSite] 

	@IdExB AS VARCHAR(50),
	@IdFuente AS VARCHAR(50),
	@Tamano as varchar(50),
	@IdEstilo AS VARCHAR(50),
	@IdUsuario AS VARCHAR(50),
	@IdBrandSite AS VARCHAR(50),
	@Negrita AS bit,
	@Italica AS bit,
	@Subrayada AS bit,
	@Color AS VARCHAR(50)
	
	
AS
BEGIN

	-- Este SP nos ayuda a insertar un registro si no existe un estilo, y si existe a actualizarlo
	
	IF EXISTS ( SELECT IdExB FROM catEstilosPorBrandSite WHERE IdExB=@IdExB)
	   BEGIN

		   UPDATE catEstilosPorBrandSite SET idFuente=@IdFuente,Negrita=@Negrita,Italica=@Italica,
											  Subrayada=@Subrayada,Color=@Color,Tamano=@Tamano
				  WHERE IdExB=@IdExB
											 
			Select @IdExB
	   END
	ELSE
	   BEGIN
			
			INSERT catEstilosPorBrandSite (idEstilo,idBrandSite,IdFuente,Tamano,idUsuario,Negrita,Italica,Subrayada,Color,Activo,FechaActivo)
			VALUES(@IdEstilo,@IdBrandSite,@IdFuente,@Tamano,@IdUsuario,@Negrita,@Italica,@Subrayada,@Color,1,GETDATE())

	   END

END

GO
/****** Object:  StoredProcedure [dbo].[traEstilosPorBrandSite_Default]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traEstilosPorBrandSite_Default]
(
	@IdBrandSite INT = NULL,
	@IdUsuARIO INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a generar en automatico los estilos para un nuevo brand site

		DECLARE
		@IdEstilo AS INT = NULL,
		@IdExB AS VARCHAR(50),
		@IdFuente INT = ISNULL((SELECT TOP 1 IdFuente FROM catFuentes WHERE Activo = 1), 1),
		@Tamano INT = 12,
		@Negrita BIT = 0,
		@Italica BIT = 0,
		@Subrayada BIT = 0,
		@Color VARCHAR(255) = 'CCCCCC'

		DECLARE cEstilos CURSOR FOR

		   SELECT TE.IdEstilo FROM catTiposEstilos TE WHERE TE.Activo = 1 ORDER BY TE.IdEstilo ASC
		
		OPEN cEstilos
		FETCH NEXT FROM cEstilos
		INTO @IdEstilo
		WHILE (@@Fetch_Status = 0)
		BEGIN

		   EXEC traEstilosPorBrandSite 0, @IdFuente, @Tamano, @IdEstilo, @IdUsuario, @IdBrandSite, @Negrita, @Italica, @Subrayada, @Color
			
		FETCH NEXT FROM cEstilos
		INTO @IdEstilo
		CONTINUE
		END

		CLOSE cEstilos
		DEALLOCATE cEstilos


	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traGlosario]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGlosario]
(
	@IdPalabra INT = NULL,
	@Palabra VARCHAR(255) = NULL,
	@Definicion VARCHAR(1020) = NULL,
	@TodosMisBS BIT = NULL,
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar una nueva palabra del glosario, y si existe a actualizarla

		IF EXISTS (SELECT TOP 1 IdPalabra FROM catGlosario WHERE IdPalabra = @IdPalabra)
		BEGIN
			UPDATE catGlosario
			SET
				Palabra = @Palabra,
				Definicion = @Definicion,
				TodosMisBS = @TodosMisBS,
				IdUsuario = @IdUsuario,
				Activo = @Activo,
				FechaActivo = GETDATE()
			WHERE IdPalabra = @IdPalabra

			SELECT @IdPalabra

		END
		ELSE
		BEGIN
		
			INSERT INTO catGlosario (Palabra, Definicion, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
			VALUES (@Palabra, @Definicion, @TodosMisBS, @IdBrandSite, @IdUsuario, @Activo, GETDATE())

			SELECT MAX(IdPalabra) FROM catGlosario WHERE Palabra = @Palabra AND IdUsuario = @IdUsuario

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traGlosario_Default]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGlosario_Default]
(
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Inserta el glosario default.

		EXEC traGlosario 0, 'AI', 'Adobe Illustrator, que puedes expandir al tamaño que necesites sin comprometer la resolución de los archivos, ya que son vectoriales. También los puedes visualizar como pdf con Adobe Reader. ', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Área de protección', 'También llamada área de restricción. Es el espacio blanco o vacío alrededor de la firma institucional. ', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'CMYK', '(CYAN, MAGENTA, YELLOW Y BLACK) La cuatricromía (cian, magenta, amarillo y negro) usada para producir todos los colores, a través del proceso de impresión llamado offset.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Diagramación', 'Definición de una estructura cuadricular que define las áreas y márgenes para el diseño.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'DPI', '(DOTS PER INCH) Puntos por pulgada. Medida de resolución para monitores de video o impresoras.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Escala de grises', 'Se llama así a las imágenes que se reproducen con diferentes porcentajes de una sola tinta, por ejemplo en un periódico con tinta negra.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Familia tipográfica', 'Está constituida por todas las variantes o estilos de las fuentes en su peso, anchura, inclinación y tamaño: Regular (redondas o romanas) es el estilo básico. Italic (cursivas o itálicas) la versión inclinada. Bold (negritas) la versión gruesa de la fuente. Caps (versalitas) son mayúsculas reducidas y ajustadas al cuerpo de las minúsculas.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Firma institucional', 'Elemento identificador más importante de una institución. Formada por el logotipo, el símbolo o el nombre legal de la institución, o por una combinación de ellos. Generalmente contiene los elementos de la identidad institucional, como son los colores institucionales y la fuente tipográfica institucional, y puede presentarse en distintos formatos (horizontal o vertical, con el nombre legal, la descripción de la institución o el lema institucional) y versiones de color.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Fuente', 'Completo set de caracteres, incluyendo letras, signos y números, que comparten el mismo estilo y peso.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Peso tipográfico', 'Se refiere al grosor del trazo.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Identidad institucional', 'Conjunto de características que hacen que una institución sea reconocida, sin posibilidad de confusión con otra.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Implantación', 'Proceso de aplicar una nueva identidad institucional o una identidad renovada a todas las piezas de comunicación y aplicaciones institucionales de la institución, y de comunicar este cambio a las audiencias internas y externas de la institución para evitar que la desinformación conlleve una resistencia al cambio.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Pleca', 'Término utilizado en diseño gráfico para denominar una línea de color de cualquier largo o ancho.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Pantone', 'Sistema de referencia de colores estándar para el manejo y control de la calidad de los mismos, en el cual cada color tiene porcentajes variables de color CMYK para impresión.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Logotipo', 'Estrictamente hablando, un logotipo requiere de tipografía, y generalmente se refiere a el nombre de la empresa en su tipografía característica. Por uso común, se suele referir como logotipo a la marca corporativa de una empresa.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Tipografía', 'El arte del diseño de las letra o tipos y su arreglo en una página. Ver también familia tipográfica.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Offset', 'Sistema de impresión litográfica indirecta con tintas transparentes, con el cual se obtienen todos los colores mediante la superposición y registro de los cuatro colores de cuatricromía (CMYK: cian, magenta, amarillo y negro).', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Interletraje', 'Espacio entre letras.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Interlineado', 'Espacio entre el pie de las letras de dos líneas o renglones de texto consecutivo.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'PDF', '(PORTABLE DOCUMENT FORMAT) Formato de archivo electrónico que permite retener los formatos de texto e imagen. Pueden visualizarse en cualquier computadora mediante un software previamente instalado llamado Acrobat Reader.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Símbolo', 'Firma institucional o elemento de la firma institucional que es pictórico y que no está basado en palabras.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'RGB', '(RED, GREEN, BLUE) Colores primarios usados en la tecnología de video (monitores de computadora, por ejemplo), y para gráficos en multimedios o Internet.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Supergráfico', 'Se refiere al uso del símbolo en un formato muy grande que se puede utilizar en espectaculares, vallas o fachadas.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Retícula', 'Líneas verticales y horizontales espaciadas regularmente usadas en diseño como marco de referencia. Los rotulistas trazan una retícula sobre lo que van a copiar y otra en una escala distinta sobre el área donde van a dibujar para guiarse y mantener las proporciones adecuadas.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Manual de identidad', 'Guía autorizada para consultar los lineamientos para la aplicación de una identidad institucional, como colores, tamaños y tipografías hasta casos específicos de aplicación.', 0, @IdBrandSite, @IdUsuario, 1
		EXEC traGlosario 0, 'Kerning', 'Espacio entre pares de letras.', 0, @IdBrandSite, @IdUsuario, 1

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traGlosario_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGlosario_Eliminar]
(
	@IdPalabra INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

	-- Este SP nos ayuda a dar de baja una palabra del glosario
	
		UPDATE catGlosario
		SET 
			IdUsuario = @IdUsuario,
			Activo = 0,
			FechaActivo = GETDATE()
		WHERE IdPalabra = @IdPalabra

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traGrupos]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGrupos]
(
	@IdGrupo INT = 0,
	@IdBrandSite INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@Descripcion VARCHAR(255) = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- si existe actualiza un grupo, si no lo inserta.

		IF EXISTS (SELECT IdGrupo FROM catGrupos WHERE IdGrupo = @IdGrupo)
		BEGIN
			UPDATE catGrupos
			SET Nombre = @Nombre, Descripcion = @Descripcion
			WHERE IdGrupo = @IdGrupo
		END
		ELSE
		BEGIN
			INSERT INTO catGrupos (IdBrandSite, Nombre, Descripcion, Activo, FechaActivo)
			VALUES (@IdBrandSite, @Nombre, @Descripcion, 1, GETDATE())
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traGrupos_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGrupos_Eliminar]
(
	@IdGrupo INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Actualiza los estatus del grupo q se de de baja

		UPDATE catGrupos SET Activo = 0, FechaActivo = GETDATE() WHERE IdGrupo = @IdGrupo

		UPDATE catGruposPorArchivo SET Activo = 0, FechaActivo = GETDATE() WHERE IdGrupo = @IdGrupo

		UPDATE catUsuariosPorGrupos SET Activo = 0, FechaActivo = GETDATE() WHERE IdGrupo = @IdGrupo

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traGruposPorArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traGruposPorArchivo]
(
	@IdGxA INT = NULL,
	@IdArchivo INT = NULL,
	@IdGrupo INT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Si existe actualiza el grupo por archivo, si no, lo inserta
		
		IF EXISTS (SELECT IdGxA FROM catGruposPorArchivo WHERE IdGxA = @IdGxA)
		BEGIN
			UPDATE catGruposPorArchivo
			SET Activo = @Activo, FechaActivo = GETDATE()
			WHERE IdGxA = @IdGxA
		END
		ELSE
		BEGIN
			INSERT INTO catGruposPorArchivo (IdArchivo, IdGrupo, Activo, FechaActivo)
			VALUES (@IdArchivo, @IdGrupo, @Activo, GETDATE())
		END
		
	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traIndiceManual]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery11.sql|7|0|C:\Users\ADMINI~1\AppData\Local\Temp\2\~vs339.sql
CREATE PROCEDURE [dbo].[traIndiceManual]
(
	@IdIndice INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@IdIndicePadre INT = NULL,
	@IdPlantilla INT = NULL,
	@HTMLPlantilla VARCHAR(MAX) = NULL,
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar una seccion nueva en el manual o actualizar una seccion ya existente

		IF EXISTS (SELECT TOP 1 IdIndice FROM catIndiceManual WHERE IdIndice = @IdIndice)
		BEGIN

			UPDATE catIndiceManual SET
			Nombre = @Nombre,
			IdIndicePadre = @IdIndicePadre,
			IdPlantilla = @IdPlantilla,
			HTMLPlantilla = @HTMLPlantilla,
			IdBrandSite = @IdBrandSite,
			IdUsuario = @IdUsuario,
			Activo = @Activo,
			FechaActivo = GETDATE()
			WHERE IdIndice = @IdIndice

			SELECT @IdIndice

		END
		ELSE
		BEGIN

			SET @HTMLPlantilla = (SELECT TOP 1 HTML FROM catPlantillas WHERE IdPlantilla = @IdPlantilla)
			
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo)
			VALUES (@Nombre, @IdIndicePadre, @IdPlantilla, @HTMLPlantilla, @IdBrandSite, @IdUsuario, @Activo, GETDATE())

			SELECT MAX(IdIndice) FROM catIndiceManual WHERE IdUsuario = @IdUsuario

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traIndiceManual_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery11.sql|7|0|C:\Users\ADMINI~1\AppData\Local\Temp\2\~vs339.sql
CREATE PROCEDURE [dbo].[traIndiceManual_Eliminar]
(
	@IdIndice INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a dar de baja una seccion del indice del manual

		UPDATE catIndiceManual SET
		IdUsuario = @IdUsuario,
		Activo = 0,
		FechaActivo = GETDATE()
		WHERE IdIndice = @IdIndice or idIndicePadre=@IdIndice

		SELECT @IdIndice

		
	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traIndiceManual_IndiceDefault]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traIndiceManual_IndiceDefault]
(
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a generar un indice default para todos nuevos brand sites

		DECLARE 
			@HTMLPlantilla VARCHAR(MAX), @HTMLPlantillaContenido VARCHAR(MAX), @Nombre VARCHAR(255) = NULL, @IdIndicePadre INT = NULL, @IdPlantilla INT = 5, 
			@TextoPlantilla VARCHAR(MAX), @TituloPlantilla VARCHAR(MAX) = '', @UrlImagenPlantilla VARCHAR(MAX) = ''

		SET @HTMLPlantilla = (SELECT TOP 1 HTML FROM catPlantillas WHERE IdPlantilla = @IdPlantilla)

		-- Titulo Bienvenida
		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'Nuestra marca vive. En cada aplicación visual en la que aparece dice algo de nosotros, de cómo somos, de lo que creemos, de la forma en la que trabajamos.<br><br>
		Hay aspectos obvios que se perciben, como mensajes claros, correcta ortografía y coherencia en lo que prometemos. Y hay otros más sutiles, como el buen diseño, consistencia en tipografías y colores y calidad de producción. Todos ellos generan una percepción de quiénes somos, una reputación y, finalmente: preferencia y lealtad.<br><br>
		Te invitamos a conocer nuestra identidad gráfica, sus principios, sus fines, y los medios con los que podremos comunicarnos profesionalmente con todos los que se interesan en nosotros.<br><br>
		En este sitio deberás encontrar todo para entenderla y desarrollarla exitosamente. Sé parte de nuestros esfuerzo y del fortalecimiento de nuestra marca.<br><br>
		Dejemos juntos la mejor marca.'
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Bienvenida'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden, TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES ('Bienvenida', 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 1, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

			-- Nuestro nombre
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'El elemento más importante de nuestra identidad es nuestro nombre. Éste rebasa las fronteras visuales y está presente como sonido y conversación. Nuestro nombre expresa nuestra visión y promesa, y es el elemento más importante de diferenciación y recordación que tenemos.<br><br>
			(Escribe aquí la explicación del nombre. Habla de su origen, de su significado y de cómo expresa el origen y la visión de la empresa.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden, TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Nuestro nombre', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 2, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
			
			-- Nuestro posicionamiento y eslogan
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'El significado de nuestra marca se genera a través de nuestra comunicación, y para ser eficientes y claros en nuestros mensajes debemos saber qué decir y qué no decir de nosotros. El elemento rector de estos mensajes es nuestro posicionamiento.<br><br>
			(Describe el posicionamiento y cómo refuerza la promesa hacia sus audiencias. Presenta el eslogan y sus componentes: qué significa cada una de sus palabras.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Nuestro posicionamiento y eslogan', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 3, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

		-- Nuestra marca
		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'El ser humano es visual por excelencia, por ello, mediante nuestra expresión gráfica podemos estar presentes en la mente de nuestras audiencias y comunicar convincentemente nuestra forma de ser. Este estilo visual denota nuestro carácter propio y único, une cohesivamente nuestras piezas de comunicación mientras nos diferencia de nuestros competidores.'
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Nuestra marca'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES (@Nombre, 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 4, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

			-- Significado y origen
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Nuestra marca utiliza elementos gráficos relevantes a nuestra industria, así como a un estilo que transpira nuestro carácter.<br><br>
			(Escribe aquí sobre lo que inspiró la marca –logotipo o símbolo–, y cómo éste refleja la forma en la que la empresa se desempeña, actúa y se comunica. Puedes hablar aquí de estilos tipográficos, colores y formas.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Significado y origen', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 5, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
			
			-- Nuestra firma
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'La firma es el elemento más importante de nuestra identidad, está compuesta por nuestro nombre y símbolo, y sintetiza todo lo que queremos decir.<br><br>
			La firma debe aparecer en todas nuestras piezas de comunicación, a veces sola y a veces acompañada del resto de los elementos que componen nuestra identidad gráfica.<br><br>
			Para mejores resultados no alteres ninguno de sus elementos; preséntala siempre con los colores y lineamientos que a continuación te indicamos. Recuerda que solo con consistencia y el uso adecuado de los elementos, lograremos consolidar nuestro impacto visual.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Nuestra firma', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 6, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Versiones de la firma
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Por flexibilidad y para lograr siempre un mayor impacto, contamos con distintas versiones de la firma. Utiliza siempre la que mejor funcione en el espacio disponible.<br><br>
			(Habla aquí de las versiones de firma de tu identidad: ¿hay una horizontal?, ¿vertical?, ¿hay alguna que sea la preferida?)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Versiones de la firma', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 7, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Colores de la firma
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Si bien nuestros colores deben ser siempre respetados y cuidados, hay ocasiones –ya sea por cuestiones técnicas, prácticas o económicas– en las que la firma debe aparecer en distintas versiones de color. A veces será mejor usar la firma en fondo blanco, otras convendrá usar un fondo de color para dar mayor impacto. En casos extremos nuestra firma será usada por terceros con criterios que no controlaremos. Es importante tener lineamientos para cada uno de ellos.<br><br>
			(Muestra aquí las diferentes versiones de color y explica el uso idóneo para cada una.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Colores de la firma', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 8, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
			
			-- Firma a color
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Utiliza siempre que puedas nuestra firma a todo color, así, aseguramos su mayor expresividad, presencia y consistencia.<br><br>
			(Muestra aquí las versiones a color.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Firma a color', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 9, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Escala de grises
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Utiliza esta versión cuando puedas imprimir solo en un color o negro, como en algunas aplicaciones de prensa o formas internas de bajo costo.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Escala de grises', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 10, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Firma sólida
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'En algunas aplicaciones, como sellos, esmerilados, estampados y grabados, es necesario usar una versión sólida, sin degradados ni tintes. Usa ésta solamente cuando no sea posible el uso de ninguna otra.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Firma sólida', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 11, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Firma fondo oscuro
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'En algunos casos es necesario invertir o ajustar los colores para asegurar el contraste y la legibilidad de la firma. Cuida siempre que la firma utilizada sea la diseñada para este uso.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Firma fondo oscuro', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 12, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Área de protección
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'En muchas ocasiones nuestra firma convivirá con otros elementos que podrían amenazar su limpieza e impacto. Para impedirlo, el área de protección delimita el espacio mínimo que debe existir entre la firma y cualquier otro gráfico. Respeta esta área para asegurar la visibilidad e integridad de nuestra marca.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Área de protección', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 13, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Tamaños mínimos
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Debemos procurar que la firma sea vista y comprendida en la totalidad de sus usos. Para ello, es importante presentarla en un tamaño armónico en relación con el espacio en el que se encuentra: que se distancie e integre al resto de los elementos de la composición y que, además, preserve totalmente su legibilidad.<br><br>
			En casos extremos puede surgir la necesidad de reducir dramáticamente el tamaño. Evita, tanto por cuestiones técnicas como por lectura, reducirla más que lo que aquí se indica.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Tamaños mínimos', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 14, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

		-- Arquitectura de marca
		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'Nuestra marca debe existir en varios contextos, tanto para audiencias internas como para clientes y público en general. Es común que ésta deba modularse de distintas maneras, a veces como marca de nuestros productos y servicios, a veces como marca corporativa, y a veces avalando diferentes actividades. <br><br>
		A Continuación te mostramos algunos ejemplos y lineamientos de cómo utilizarla en cada nivel.'
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Arquitectura de marca'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES (@Nombre, 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 15, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

			-- Submarcas
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Algunas de nuestras submarcas presentan nuestra marca corporativa de una manera particular, y con ello aseguramos que nuestra promesa se haga palpable con el uso de nuestros productos. <br><br>
			(Muestra aquí los ejemplos relevantes de posibles submarcas con nombres de productos o servicios.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Submarcas', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 16, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
			
			-- Marca como aval
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Dadas nuestras alianzas con otras empresas, nuestra marca aparecerá a veces avalando nuestro compromiso y relación con ellas. Aquí hay una muestra de la forma de presentarla.<br><br>
			(Muestra aquí los usos de avales corporativos en promociones o programas sociales.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Marca como aval', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 17, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Co-branding
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'En algunos casos –como en carteles, promocionales o sitios de eventos que patrocinemos o en los que participemos– nuestra marca aparecerá en convivencia con otras marcas. Si bien a veces no podremos controlar su aplicación, sí podemos solicitar un uso particular o enviar la versión que mejor funcione. Aquí te mostramos ejemplos.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Co-branding', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 18, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

		-- Narrativa visual
		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'Nuestra identidad gráfica se compone de varios elementos verbales y visuales que la hacen reconocible. Al combinarlos adecuadamente crearemos una comunicación consistente, empática y emotiva con cada uno de nuestras audiencias.<br><br>
		Nuestro estilo debería ser reconocido incluso sin la firma, meramente por sus colores, uso tipográfico y de imágenes. Asegúrate siempre de revisar que cada pieza respire la misma esencia.<br><br>
		(Habla aquí de los elementos secundarios que componen la identidad más allá de la marca. Pueden ser texturas, colores, fotografías, ilustraciones, tipografías…)'
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Narrativa visual'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES (@Nombre, 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 19, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

			-- Nuestros colores
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Además de ser parte fundamental de nuestra firma, nuestros colores darán vida y reconocimiento a todas nuestras aplicaciones. El estilo cromático que generemos debe ser pensado considerando cada elemento, desde aplicaciones corporativas, productos promocionales, tonos fotográficos… En fin. El color debe abrazar a cada uno de nuestros mensajes y dotarlos de nuestra personalidad.<br><br>
			(Muestra aquí los colores corporativos, así como cada variante, paleta complementaria o criterios que se deban considerar.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Nuestros colores', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 20, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Tipografía corporativa
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Aunque un poco más sutil que el color, nuestra tipografía es reconocida como nuestro tono de voz. Las diferentes fuentes tipográficas (light, italic, bold, etc.) nos ofrecen la oportunidad de modular esa voz y de generar una correcta jerarquía e impacto en nuestras comunicaciones.<br><br>
			(Presenta aquí la o las familias tipográficas a utilizar. Habla con detalle de el uso de cada fuente para cada aplicación: en qué casos se utiliza una fuente bold, o en mayúsculas. Habla de tamaños, ¿usarán la tipografía en puntajes grandes para generar un gran impacto?, ¿será pequeña y elegante?)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Tipografía corporativa', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 21, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Estilo editorial
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Directamente ligado al estilo tipográfico se encuentra el estilo editorial, que genera el contexto en el que la tipografía existirá. Este estilo define formatos, retículas –márgenes, cajas de texto, interlineado–, y usos de imágenes –rebasadas, con marcos, horizontales, verticales, etc.<br><br>
			El estilo editorial se aplica tanto a materiales corporativos como tarjetas y papelería, como a anuncios, folletería y materiales electrónicos. Todo debe tener un aire de familia.<br><br>
			(Incluye aquí todos los ejemplos necesarios para ilustrar cómo el universo de piezas de comunicación mantienen una lógica constante en el uso de elementos y criterios gráficos.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Estilo editorial', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 22, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Estilo fotográfico
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Las fotografías connotan siempre un punto de vista. El tipo de imagen, tanto por el objeto en sí como por su perspectiva, encuadre y tonalidad, hablan de lo que nos interesa y lo que queremos mostrar (o no mostrar). Este estilo debe añadir un elemento más de personalidad a nuestra identidad: tanto las fotografías de nuestra gente, como las de nuestros productos, clientes o eventos deberán sentirse hermanadas, como vistas desde la misma óptica.<br><br>
			Ya sea si encargas tomas fotografías como si las compras en bancos de imágenes, cuida que exista consistencia y que la imagen refuerce nuestra voz además de reforzar el mensaje particular.<br><br>
			(Habla aquí de criterios fotográficos específicos: colores y tonalidades, encuadres, tipo de modelos, uso de profundidad de campo, etc. Utiliza ejemplos a imitar en tomas o compras de imágenes posteriores.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Estilo fotográfico', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 23, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Uso de ilustraciones
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Como un apoyo importante de nuestra identidad podemos usar ilustraciones (tal vez en lugar de fotografías), viñetas, símbolos, gráficas e infografías en general. Todos ellos deberán contar con criterios de color, tipográfico y de estilo de tal forma que refuercen nuestra narrativa visual al mismo tiempo de apoyar mensajes específicos y su comprensión.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Uso de ilustraciones', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 24, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

		-- Aplicaciones
		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'Todos los elementos que hemos estudiado se organizan ahora para presentar una identidad cohesiva a través de cada una de nuestras piezas de comunicación. Cada una de ellas deberá ligarse a nuestra identidad y deberá construir, al mismo tiempo, la gran experiencia de vivir nuestra marca. <br><br>
		En las siguientes páginas mostraremos muchas aplicaciones, cada una de ellas ha sido diseñada para funcionar en su contexto particular. Todas deben hablar con el mismo tono, pero el protagonismo de cada una de ellas debe asegurar que su mensaje puntual es escuchado y entendido por su audiencia objetivo. '
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Aplicaciones'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES (@Nombre, 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 25, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

			-- Papelería
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Si bien las comunicaciones corporativas se dan cada vez menos mediante cartas impresas e, incluso, tarjetas de presentación, la papelería corporativa sigue siendo un elemento crucial y emblemático de toda identidad. La tarjeta de presentación puede ser el primer contacto que tenga nuestra empresa con ciertas audiencias, y además de portar la información adecuada debe comenzar a expresar nuestra forma de pensar, ser y trabajar.<br><br>
			(Habla aquí de cualquier detalle particular de diseño de las piezas de papelería.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Papelería', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 26, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Materiales comerciales
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Nuestros folletos responden a necesidades de comunicación y segmentación. Contamos con materiales que hablan de lo más esencial de nuestra compañía y con otros que ofrecen información puntual para una pequeña audiencia.<br><br>
			Busca que los materiales cuenten una historia común. Que cada uno tenga la independencia que requiera pero que que sea parte clara de un todo. Utiliza con consistencia nuestro lenguaje gráfico y formas claras de diferenciación entre cada pieza distinta.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Materiales comerciales', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 27, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Electrónicas
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Los materiales electrónicos nos presentan recursos maravillosos de expresión, desde animaciones hasta video y sonidos. Utiliza ese potencial cada vez que sea apropiado, y busca formas de crear nuevas dimensiones de nuestra identidad pero siempre dentro del marco de la cohesividad.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Electrónicas', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 28, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Firma de correo
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'La oportunidad que brinda el correo electrónico para dar información no debe ser desaprovechada. No solo es un espacio constante y gratuito, sino que nos permite personalizar y actualizar como pocas aplicaciones.<br><br>
			Utiliza el archivo electrónico de la sección de descargas y modifica la información para contener tus datos particulares. Asegúrate de firmar siempre tus correos para sembrar nuestra marca de una forma directa y personalizada con tus lectores. '
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Firma de correo', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 29, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Presentaciones
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Nuestras presentaciones electrónicas han sido diseñadas para portar nuestra identidad de forma impactante, y dar al mismo tiempo protagonismo al contenido particular de cada situación. El uso de tipografías corporativas o de fuentes genéricas ha sido definido a partir de las necesidades de distribución: si se compartirá fuera de la empresa utiliza tipografías genéricas.<br><br>
			Escribe el contenido de cada presentación pensando en cómo será leída. Si servirá de apoyo a un presentador utiliza textos cortos, y si será autocontenida trata de ser lo más claro y explícito posible. Mientras más diversos sean los públicos que la recibirán, más clara y amplia deberá ser.<br><br>
			Evita recrear pantallas si éstas existen en las pantallas guía, pues es importante que el estilo de las presentaciones de todos en la empresa sea el mismo.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Presentaciones', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 30, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Sitio web
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Nuestro sitio web es visto por personas con distintas necesidades. A veces es el primer contacto que se tiene con nuestra empresa, a veces se visita de nuevo para encontrar información detallada y específica, y a veces se navega simplemente buscando novedades. Éste contiene todo lo relevante a todos nuestros públicos y porta nuestra marca de forma innovadora y clara.<br><br>
			(Habla aquí de detalles de diseño y navegación del sitio.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Sitio web', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 31, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Publicidad
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'La publicidad suele buscar la sorpresa como una forma de llamar la atención y quedarse en la mente de la gente. No es conveniente, por ello, crear lineamientos estrictos en cuanto a su diseño. Por otro lado, cada vez que una pieza de publicidad es vista por nuestros públicos debe construir sobre nuestra marca y lo que significa.<br><br>
			Confirma siempre que los mensajes de publicidad son coherentes con lo que decimos en otros canales. La personalidad y estilo –nuestra voz corporativa– deben ser iguales en un anuncio que en el sitio web. Somos la misma empresa.<br><br>
			A continuación te mostramos varios ejemplos para que veas cómo el lenguaje gráfico apoya a los mensajes, imágenes y discurso en general, y cómo todo se siente como una experiencia continua.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Publicidad', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 32, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Señalización
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Nuestras oficinas y cualquier espacio en el que interactuemos como empresa son idóneas para presentar quiénes somos. Es como la decoración de nuestra casa o habitación, que expresa muy elocuentemente lo que nos gusta y apasiona, o la forma en la que vestimos, que añade una nueva capa de relación y conversación.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Señalización', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 33, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Letreros
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Además del estilo de mobiliario, colores y hasta la ubicación de nuestros locales, la señalización indica nuestra presencia en la calle y nos dirige a todos dentro del edificio. Los letreros deben cumplir normas de seguridad, así como comunicar con claridad direcciones y hasta inspirar a quien los ve.<br><br>
			Te mostramos aquí varios ejemplos de letreros en distintos niveles de jerarquía.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,	TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Letreros', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 34, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Gafetes
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'El gafete es portado con orgullo durante el día por nosotros, y acompaña a clientes y amigos mientras nos visitan. Fue diseñado para mostrar con claridad el nombre del portador y para dar un buen sentido de pertenencia a todos mientras interactuamos en nuestras oficinas. <br><br>
			Asegura que los nombres sean visibles y que las fotografías muestren gente bien presentada y con buena actitud, pues cada uno nos representa.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden, TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Gafetes', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 35, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Uniformes
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Uno de los “momentos de la verdad” en los que sembramos una percepción directa de nuestra empresa es cuando algunos de nuestros colegas actúan como nuestros embajadores con clientes y proveedores. Nuestro uniforme debe generar una imagen de consistencia y profesionalismo, por lo que no solamente hay que cuidar que esté bien presentable, sino que quien lo porta debe comportarse adecuadamente, como nuestra “marca viva”. <br><br>
			(Habla aquí de aspectos específicos de los uniformes y sus diferencias por niveles, sexo, etc.)'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Uniformes', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 36, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

			-- Promocionales
			SET @HTMLPlantillaContenido = @HTMLPlantilla
			SET @TextoPlantilla = 'Dar un regalo o entregar algo físico a nuestros clientes, proveedores y amigos debe ser visto como una forma de quedarnos con ellos permanentemente. Los artículos promocionales que elijamos deben considerar varios aspectos: el costo debe ser apropiado para quien lo recibe, no debe sentirse como un derroche de recursos ni como algo muy barato para quien espera un buen reconocimiento; los materiales deben ser responsables con el ambiente, además de reforzar nuestra visión (el regalo cambia si somos innovadores o tradicionales); la permanencia debe ser importante, busquemos objetos que se queden un largo tiempo; debemos buscar oportunidades de color y forma para vernos en el mismo estilo; la presencia de nuestra marca debe ser importante, pero a veces es mejor estar ausentes si con ello aseguramos el uso del promocional y la recordación de quien lo obsequió.<br><br>
			Aquí te mostramos algunos ejemplos de promocionales que podemos considerar para distintas ocasiones.'
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
			SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
			INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
			VALUES ('Promocionales', @IdIndicePadre, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 37, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)

		SET @HTMLPlantillaContenido = @HTMLPlantilla
		SET @TextoPlantilla = 'Todo lo que aquí te presentamos debe servir de guía y de inspiración. Los materiales que ya han sido diseñados deben ser utilizados tal cual son, sin variantes. Para ello utiliza los archivos de la sección de descargas.<br><br>
		Los materiales que deban ser diseñados con el tiempo deberán ser congruentes y fieles a todo lo que aquí hemos visto. Aprovecha las oportunidades que se te presenten para crear materiales creativos y memorables, y hazlo siempre teniendo en mente los principios de buen diseño. <br><br>
		Ayúdanos a mejorar nuestra identidad gráfica con cada aplicación.'
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Titulo', @TituloPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Img', @UrlImagenPlantilla)
		SET @HTMLPlantillaContenido = REPLACE(@HTMLPlantillaContenido, '@Texto1', @TextoPlantilla)
		SET @Nombre = 'Despedida'
		INSERT INTO catIndiceManual (Nombre, IdIndicePadre, IdPlantilla, HTMLPlantilla, IdBrandSite, IdUsuario, Activo, FechaActivo, Orden,TituloPlantilla, TextoPlantilla, urlImagenPlantilla)
		VALUES (@Nombre, 0, @IdPlantilla, @HTMLPlantillaContenido, @IdBrandSite, @IdUsuario, 1, GETDATE(), 38, @TituloPlantilla, @TextoPlantilla, @urlImagenPlantilla)
		SET @IdIndicePadre = (SELECT MAX(IdIndice) FROM catIndiceManual WHERE Nombre = @Nombre AND IdBrandSite = @IdBrandSite)

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualActualizaOrdenPadre]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualActualizaOrdenPadre]
	-- Add the parameters for the stored procedure here
	@IdBrandSite VARCHAR(50),
	@IdIndice INT,
	@IdIndicePadre INT,
	@Orden INT
AS
BEGIN

			UPDATE catIndiceManual SET IdIndicePadre=@IdIndicePadre,Orden=@Orden
													  WHERE IdIndice=@IdIndice and IdBrandSite=@IdBrandSite



END

GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualActualizarHTML]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualActualizarHTML] 
	-- Add the parameters for the stored procedure here
	@HTML as varchar(max),
	@Titulo as varchar(max),
	@Texto as varchar(max),
	@Img as varchar(max),
	@idIndiceManual as int

AS
BEGIN
	
	-- Cambia el html de un elemento de lmenu

	Update catIndiceManual 
	set 
	HTMLPlantilla=@HTML,
	TituloPlantilla=@Titulo,
	TextoPlantilla=@Texto,
	urlImagenPlantilla=@Img
	where idIndice=@idIndiceManual 

END

GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualActualizarNodo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Batch submitted through debugger: SQLQuery11.sql|7|0|C:\Users\ADMINI~1\AppData\Local\Temp\2\~vs339.sql
CREATE PROCEDURE [dbo].[traIndiceManualActualizarNodo]
(
	@IdIndice INT,
	@NombreNodo VARCHAR(255),
	@NodoPadre INT,
	@Usuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a actualizar un nodo en un manual de un brand site

		IF EXISTS (SELECT TOP 1 IdIndice FROM catIndiceManual WHERE IdIndice = @IdIndice)
		BEGIN

			UPDATE catIndiceManual SET Nombre = @NombreNodo,IdIndicePadre = @NodoPadre, IdUsuario = @Usuario
				    WHERE IdIndice = @IdIndice

			SELECT @IdIndice

		END
		

		COMMIT TRANSACTION
		PRINT 'OK'

	END TRY
	BEGIN CATCH
		ROLLBACK
		PRINT 'FAIL'
	END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualActualizarPlantilla]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualActualizarPlantilla] 
	-- Add the parameters for the stored procedure here
	@Indice as INT, 
	@HTML as VARCHAR(MAX)
AS
BEGIN
	
	-- Este SP nos ayuda a actualizar la plantilla de una sección del indice

	UPDATE catIndiceManual SET HTMLPlantilla=@HTML
	where  IdIndice=@Indice



END

GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualCambiarTemplate]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualCambiarTemplate] 
	-- Add the parameters for the stored procedure here
	@IdIndice INT,
	@IdPlantilla INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	-- Este SP actualiza el HTML de la plantilla

	BEGIN TRY
		BEGIN TRANSACTION

		BEGIN


			DECLARE @HTMLPlantilla as VARCHAR(MAX)

			SELECT @HTMLPlantilla=HTML FROM catPlantillas WHERE IdPlantilla=@IdPlantilla

			
	SELECT   @HTMLPlantilla=
	   Replace( Replace(
										 Replace(HTML,
										  '@Titulo',
										  case  when (TituloPlantilla  IS NULL OR TituloPlantilla='')  then 'Ingresa un título' else TituloPlantilla END)
										  ,'@Texto1', 
										  case  when (TextoPlantilla  IS NULL OR TextoPlantilla='')  then 'Ingresa el contenido' else TextoPlantilla END)
										  ,'@Img',case  when (urlImagenPlantilla  IS NULL OR urlImagenPlantilla='')  then '' else urlImagenPlantilla END)
	FROM catPlantillas cp
	Inner Join catIndiceManual cm on cm.IdIndice=@IdIndice
	WHERE cp.IdPlantilla=@IdPlantilla 


			UPDATE catIndiceManual SET
			IdPlantilla = @IdPlantilla,
			HTMLPlantilla = @HTMLPlantilla,
			FechaActivo = GETDATE()
			WHERE IdIndice = @IdIndice

		

		END
	


                COMMIT TRANSACTION
				PRINT 'OK'

	END TRY
		BEGIN CATCH
			ROLLBACK
			PRINT 'FAIL'
		END CATCH

END

GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualGuardarItem]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualGuardarItem] 
	-- Add the parameters for the stored procedure here
	@NombreIndice as varchar(50),
	@Padre as varchar(50),
	@Usuario as varchar(50),
	@IdBrandSite as varchar(50)
AS
BEGIN

			DECLARE @Orden as INT, @HTMLPlantilla varchar(MAX)

			
			SELECT @HTMLPlantilla=HTML from catPlantillas where idPlantilla=1

	IF @Padre=0 
		BEGIN

		     	SELECT @Orden = isnull(max(orden),0)+1 from catIndiceManual where IdBrandSite=@IdBrandSite and Activo = 1
				INSERT INTO catIndiceManual (Nombre,IdIndicePadre,IdPlantilla,HTMLPlantilla,IdBrandSite,IdUsuario,Activo,FechaActivo,Orden)
				VALUES  (@NombreIndice,@Padre,1,@HTMLPlantilla,@IdBrandSite,@Usuario,1,GETDATE(),@Orden)
	    END
  Else 
		BEGIN
		
		Declare @OrdenT as Int=0, @IdInsertado as Int

		Select @OrdenT = isnull(max(orden), (select orden+1 from catIndiceManual where idIndice=@Padre and Activo = 1)) 
		from catIndiceManual where IdBrandSite=@IdBrandSite and idIndicePadre = @Padre and Activo = 1
		
		INSERT INTO catIndiceManual (Nombre,IdIndicePadre,IdPlantilla,HTMLPlantilla,IdBrandSite,IdUsuario,Activo,FechaActivo,Orden)
		VALUES  (@NombreIndice,@Padre,1,@HTMLPlantilla,@IdBrandSite,@Usuario,1,GETDATE(),@OrdenT)

	     SELECT @IdInsertado= @@identity 
      ---Se obtiene el indice de insercion y apartir de ese actualizamos los demas registros
			Declare @idIndice as int
			DECLARE cHTML CURSOR FOR 
							SELECT IdIndice FROM [Identidad].[dbo].[catIndiceManual] where idBrandSite=@IdBrandSite and Orden>=(@OrdenT) and IdIndice <>@IdInsertado
							 Order by Orden 
		    OPEN cHTML
			  FETCH NEXT FROM cHTML
						INTO @IdIndice
							WHILE (@@Fetch_Status = 0)
						     BEGIN
	                              select @OrdenT=@OrdenT+1 
									Update  [Identidad].[dbo].[catIndiceManual] set Orden=@OrdenT where idBrandSite=@IdBrandSite
										and idIndice=@IdIndice
							     	 
			--Select @IdIndice,@contador

				FETCH NEXT FROM cHTML
				INTO  @IdIndice
					

END

	CLOSE cHTML
		DEALLOCATE cHTML




		END

END

GO
/****** Object:  StoredProcedure [dbo].[traIndiceManualModificaNombreIndice]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[traIndiceManualModificaNombreIndice]
	-- Add the parameters for the stored procedure here
	@IdIndice as VARCHAR(50),
	@NombreNodo as VARCHAR(100)
AS
BEGIN
	
	UPDATE catIndiceManual SET Nombre=@NombreNodo
											  WHERE IdIndice=@IdIndice
						

END

GO
/****** Object:  StoredProcedure [dbo].[traLog]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traLog]
(
	@Exception AS VARCHAR(MAX) = NULL,
	@IdUsuario AS INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Inserta un log en la tabla de errores

		INSERT INTO catLog (Exception, IdUsuario, FechaException)
		VALUES (@Exception, @IdUsuario, GETDATE())

		-- Me mando un correo con el error

		EXEC Mail_EnviarCorreo 1, 'Issue Identidad.com', @Exception -- Rafael 
		-- EXEC Mail_EnviarCorreo 2, 'Jessi ya le partiste a Identidad.com', @Exception -- Jessi

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traTags]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traTags]
(
	@IdTag INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@Descripcion VARCHAR(1020) = NULL,
	@IdArchivo INT = NULL,
	@Extension VARCHAR(255) = NULL,
	@TodosMisBS BIT = NULL,
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar o actualizar un tag o etiqueta para un brand site

		IF EXISTS(SELECT TOP 1 IdTag FROM catTags WHERE IdTag = @IdTag)
		BEGIN

			UPDATE catTags SET
			Nombre = @Nombre,
			Descripcion = @Descripcion,
			IdArchivo = @IdArchivo,
			Extension = @Extension,
			TodosMisBS = @TodosMisBS,
			IdUsuario = @IdUsuario
			WHERE IdTag = @IdTag

			SELECT @IdTag

		END
		ELSE
		BEGIN

			INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
			VALUES (@Nombre, @Descripcion, @IdArchivo, @Extension, @TodosMisBS, @IdBrandSite, @IdUsuario, 1, GETDATE())

			SELECT MAX(IdTag)
			FROM catTags
			WHERE IdUsuario = @IdUsuario

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traTags_Default]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traTags_Default] -- 121, 1
(
	@IdBrandSite INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Inserta los tags default

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Adobe Illustrator', 'Son los archivos para el impresor o software de diseño los cuales podrás expandir al tamaño que necesites sin comprometer la resolución pues son archivos vectoriales. Además podrás retomar los elementos para crear diseños nuevos a partir de éstos.', 
		0, '.ai', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Adobe Acrobat Reader', 'Son archivos para visualización multiplataforma.',
		0, '.pdf', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Photoshop', 'Formato estándar de Photoshop con soporte de capas, se utiliza principalmente para la edición de fotografías.',
		0, '.psd', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Joint Photographics Experts Group', 'Sirven para impresiones con buena resolución, no deben ser agrandados de su tamaño original pues perderán nitidez.',
		0, '.jpg', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Bitmap', 'Formato estándar de Windows para el uso de imágenes.',
		0, '.bmp', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Scalable Vector Graphics', 'Son una especificación para describir gráficos vectoriales bidimensionales, tanto estáticos como animados, se utilizan principalmente para elementos gráficos y dinámicos en páginas web.',
		0, '.svg', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Graphics Interchange Format', 'Muy utilizado para las web. Permite almacenar un canal alfa para dotarlo de transparencia, y salvarlo como entrelazado para que al cargarlo en la web lo haga en varios pasos. Admite hasta 256 colores.',
		0, '.gif', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Tagged Image File Format', 'Una solución creada para pasar de PC a MAC y viceversa. Es un formato de archivo informático para imágenes.',
		0, '.tiff', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Portable Networks Graphics', 'La misma utilización que los GIF, pero con mayor calidad. Soporta transparencia y colores a 24 bits. Solo las versiones recientes de navegadores pueden soportarlos.',
		0, '.png', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Keynote', 'Keynote es una aplicación de software de presentación desarrollada como parte del set de productividad iWork por Apple Inc.',
		0, '.key', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Power Point', 'Formato de archivo de presentación utilizado por Microsoft Power Point.',
		0, '.ppt', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Power Point', 'Formato de archivo de presentación utilizado por Microsoft Power Point.',
		0, '.pptx', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Microsoft Word', 'El formato de archivo utilizado por el procesador de texto Microsoft Word.',
		0, '.doc', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Microsoft Word', 'El formato de archivo utilizado por el procesador de texto Microsoft Word.',
		0, '.docx', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Microsoft Excel', 'Microsoft Excel es una aplicación distribuida por Microsoft Office para hojas de cálculo.',
		0, '.xls', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

		INSERT INTO catTags (Nombre, Descripcion, IdArchivo, Extension, TodosMisBS, IdBrandSite, IdUsuario, Activo, FechaActivo)
		VALUES('Microsoft Excel', 'Microsoft Excel es una aplicación distribuida por Microsoft Office para hojas de cálculo.',
		0, '.xlsx', 0, @IdBrandSite, @IdUsuario, 1, GETDATE())

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traTags_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traTags_Eliminar]
(
	@IdTag INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a dar de baja un trag o etiqueta

		UPDATE catTags 
		SET
			Activo = 0,
			FechaActivo = GETDATE(),
			IdUsuario = @IdUsuario
		WHERE IdTag = @IdTag

		UPDATE catTagsPorArchivo
		SET
			Activo = 0,
			FechaActivo = GETDATE()
		WHERE IdTag = @IdTag

		SELECT @IdTag

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traTagsPorArchivo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traTagsPorArchivo]
(
	@IdTag INT = NULL,
	@IdArchivo INT = NULL,
	@IdUsuario INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar los tags que se agregan a un archivo para su consulta sin estar ligados por la extensión

			INSERT INTO catTagsPorArchivo (IdTag, IdArchivo, IdUsuario, Activo, FechaActivo)
			VALUES (@IdTag, @IdArchivo, @IdUsuario, 1, GETDATE())

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traTagsPorArchivo_Eliminar]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traTagsPorArchivo_Eliminar]
(
	@IdTxA INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION
		
		-- Este SP nos ayuda a quitar un tag o etiqueta de un archivo

		UPDATE catTagsPorArchivo SET
		Activo = 0, FechaActivo = GETDATE()
		WHERE IdTxA = @IdTxA

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuarios]
(
	@IdUsuario INT = NULL,
	@Nombre VARCHAR(255) = NULL,
	@ApellidoPaterno VARCHAR(255) = NULL,
	@ApellidoMaterno VARCHAR(255) = NULL,
	@CorreoElectronico VARCHAR(255) = NULL,
	@Contraseña VARCHAR(255) = NULL,
	@Foto VARCHAR(255) = NULL,
	@Telefono VARCHAR(255) = NULL,
	@IdUsuarioActualiza INT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a dar de alta o actualizar a un usuario

		IF EXISTS (SELECT TOP 1 IdUsuario FROM catUsuarios WHERE IdUsuario = @IdUsuario)
		BEGIN

			UPDATE catUsuarios
			SET
				Nombre = @Nombre,
				ApellidoPaterno = @ApellidoPaterno,
				ApellidoMaterno = @ApellidoMaterno,
				CorreoElectronico = @CorreoElectronico,
				Contraseña = @Contraseña,
				Foto = @Foto,
				Telefono = @Telefono,
				IdUsuarioActualiza = @IdUsuarioActualiza,
				Activo = @Activo,
				FechaActivo = GETDATE()

			WHERE IdUsuario = @IdUsuario

			SELECT @IdUsuario

		END
		ELSE
		BEGIN

			INSERT INTO catUsuarios (Nombre, ApellidoPaterno, ApellidoMaterno, CorreoElectronico, Contraseña, Foto, Telefono, IdUsuarioActualiza, Activo, FechaActivo)
			VALUES (@Nombre, @ApellidoPaterno, @ApellidoMaterno, @CorreoElectronico, @Contraseña, @Foto, @Telefono, @IdUsuarioActualiza, @Activo, GETDATE())

			SET @IdUsuario = (SELECT MAX(IdUsuario) AS IdUsuario FROM catUsuarios
			WHERE CorreoElectronico = @CorreoElectronico AND Activo = @Activo)

			INSERT INTO catUsuariosPorUsuarios (IdUsuarioRegistra, IdUsuario, Activo, FechaActivo)
			VALUES (@IdUsuarioActualiza, @IdUsuario, @Activo, GETDATE())

			SELECT @IdUsuario

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traUsuarios_NuevoDiseñador]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuarios_NuevoDiseñador] -- 'Jessi', '', '', 'jbarrien@compusoluciones.com'
(
	@Nombre VARCHAR(255) = NULL,
	@ApellidoPaterno VARCHAR(255) = NULL,
	@ApellidoMaterno VARCHAR(255) = NULL,
	@CorreoElectronico VARCHAR(255) = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Dar de alta un nuevo diseñador.

		DECLARE
		@IdUsuario INT = 0,
		@Contraseña VARCHAR(255) = 'Temp001',
		@Foto VARCHAR(255) = 'https://s3.amazonaws.com/Identidad/Images/ID.PNG',
		@Telefono VARCHAR(255) = '',
		@IdUsuarioActualiza INT = 0,
		@Activo BIT = 1

		IF NOT EXISTS (SELECT TOP 1 IdUsuario FROM catUsuarios WHERE CorreoElectronico = LTRIM(RTRIM(@CorreoElectronico)))
		BEGIN

			EXEC traUsuarios @IdUsuario, @Nombre, @ApellidoPaterno, @ApellidoMaterno, @CorreoElectronico, @Contraseña, @Foto, @Telefono, @IdUsuarioActualiza, @Activo

			SET @IdUsuario = (SELECT MAX(IdUsuario) FROM catUsuarios WHERE CorreoElectronico = LTRIM(RTRIM(@CorreoElectronico)))

			INSERT INTO catUsuariosPorBrandSite (IdUsuario, IdTipoUsuario, IdUsuarioActualiza, Activo, FechaActivo)
			VALUES (@IdUsuario, 2, @IdUsuario, 1, GETDATE())

			UPDATE catUsuarios SET IdTipoCuenta = 1, IdUsuarioActualiza = @IdUsuario WHERE IdUsuario = @IdUsuario

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traUsuarios_TipoCuenta]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuarios_TipoCuenta]
(
	@IdUsuario INT = NULL,
	@IdTipoCuenta INT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a actualizar el tipo de cuenta de un usuario

		UPDATE catUsuarios SET
		IdTipoCuenta = @IdTipoCuenta
		WHERE IdUsuario = @IdUsuario

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH


GO
/****** Object:  StoredProcedure [dbo].[traUsuariosPorBrandSite]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuariosPorBrandSite]
(
    @IdUxB INT = NULL,
    @IdUsuario INT = NULL,
    @IdTipoUsuario INT = NULL,
    @IdBrandSite INT = NULL,
	@IdUsuarioActualiza INT = NULL,
    @Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a dar acceso a un usuario existente a un brand site con un tipo de permiso espefico.

		IF EXISTS (SELECT TOP 1 IdUxB FROM catUsuariosPorBrandSite WHERE IdUxB = @IdUxB)
		BEGIN

			UPDATE catUsuariosPorBrandSite
			SET Activo = @Activo, IdUsuarioActualiza = @IdUsuarioActualiza, FechaActivo = GETDATE()
			WHERE IdUxB = @IdUxB
			
			SELECT @IdUsuario = cUxB.IdUsuario, @IdBrandSite = cUxB.IdBrandSite FROM catUsuariosPorBrandSite cUxB WHERE cUxB.IdUxB = @IdUxB

			IF EXISTS (
						SELECT cUxG.IdUxG 
						FROM catUsuariosPorGrupos cUxG 
						INNER JOIN catGrupos G ON G.IdGrupo = cUxG.IdGrupo AND G.IdBrandSite = @IdBrandSite 
						WHERE cUxG.IdUsuario = @IdUsuario AND cUxG.Activo = 1
					  )
			BEGIN
				UPDATE catUsuariosPorGrupos
				SET Activo = 0, FechaActivo = GETDATE()
				WHERE IdUxG IN (
									SELECT cUxG.IdUxG 
									FROM catUsuariosPorGrupos cUxG 
									INNER JOIN catGrupos G ON G.IdGrupo = cUxG.IdGrupo AND G.IdBrandSite = @IdBrandSite 
									WHERE cUxG.IdUsuario = @IdUsuario AND cUxG.Activo = 1
								)
			END

		END
		ELSE
		BEGIN

			INSERT INTO catUsuariosPorBrandSite (IdUsuario, IdTipoUsuario, IdBrandSite, IdUsuarioActualiza, Activo, FechaActivo)
			VALUES (@IdUsuario, @IdTipoUsuario, @IdBrandSite, @IdUsuarioActualiza, @Activo, GETDATE())

			DECLARE @Asunto VARCHAR(255), @Texto VARCHAR(MAX)

			SELECT	
				TOP 1 
				@Asunto = 
				'¡Acceso a ' + RTRIM(LTRIM(BS.Nombre)) + '!',
				@Texto = 
				'¡Te han otorgado acceso al sitio de identidad corporativa de ' + RTRIM(LTRIM(BS.Nombre)) + '!<br><br>' +
				'Para ingresar introduce las siguientes credenciales; <br>' +
				'Usuario: ' + RTRIM(LTRIM(U.CorreoElectronico)) + '<br>' +
				'Contraseña: ' + RTRIM(LTRIM(U.Contraseña)) + '<br><br>' +
				'<a href="http://identidad.com/BrandSite/Login.aspx?id=' + CONVERT(VARCHAR, BS.IdBrandSite) + '">ir al sitio ' + RTRIM(LTRIM(BS.Nombre)) + '</a>'
				--'<a href="http://' + LOWER(RTRIM(LTRIM(BS.Url))) + '.identidad.com">ir al sitio ' + RTRIM(LTRIM(BS.Nombre)) + '</a>'
			FROM catUsuariosPorBrandSite UxB
			INNER JOIN catUsuarios U ON U.IdUsuario = UxB.IdUsuario
			INNER JOIN catBrandSites BS ON BS.IdBrandSite = UxB.IdBrandSite
			WHERE UxB.IdUsuario = @IdUsuario AND UxB.IdTipoUsuario = @IdTipoUsuario AND UxB.IdBrandSite = @IdBrandSite AND UxB.Activo = @Activo

			EXEC Mail_EnviarCorreo @IdUsuario, @Asunto, @Texto

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH

GO
/****** Object:  StoredProcedure [dbo].[traUsuariosPorGrupo]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuariosPorGrupo]
(
	@IdUxG INT = 0,
	@IdUsuario INT = NULL,
	@IdGrupo INT = NULL,
	@Activo BIT = NULL
)
AS
BEGIN TRY
	BEGIN TRANSACTION

		-- Este SP nos ayuda a insertar o actualizar un grupo

		IF EXISTS (SELECT TOP 1 IdUxG FROM catUsuariosPorGrupos WHERE IdUxG = @IdUxG)
		BEGIN
			UPDATE catUsuariosPorGrupos
			SET	Activo = @Activo,
				FechaActivo = GETDATE()
			WHERE IdUxG = @IdUxG
		END
		ELSE
		BEGIN
			INSERT INTO catUsuariosPorGrupos (IdUsuario, IdGrupo, Activo, FechaActivo)
			VALUES (@IdUsuario, @IdGrupo, @Activo, GETDATE())
		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[traUsuariosPorUsuarios]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[traUsuariosPorUsuarios]
(
	@CorreoElectronico VARCHAR(255) = NULL,
	@IdUsuario INT = NULL
)
AS			
BEGIN TRY
	BEGIN TRANSACTION
			
		-- Este SP Inserta a un usuario para poder dar permisos a futuro a un brand site, siembre y cuando no este dado de alta

		IF EXISTS (SELECT TOP 1 U.IdUsuario FROM catUsuarios U WHERE U.CorreoElectronico = @CorreoElectronico AND U.Activo = 1)
		BEGIN
			DECLARE @IdUsuarioSolicitado INT = (SELECT TOP 1 U.IdUsuario FROM catUsuarios U WHERE U.CorreoElectronico = @CorreoElectronico AND U.Activo = 1)
			
			IF NOT EXISTS (SELECT TOP 1 IdUxU FROM catUsuariosPorUsuarios WHERE IdUsuarioRegistra = @IdUsuario AND IdUsuario = @IdUsuarioSolicitado)
			BEGIN	
				INSERT INTO catUsuariosPorUsuarios (IdUsuarioRegistra, IdUsuario, Activo, FechaActivo)
				VALUES (@IdUsuario, @IdUsuarioSolicitado, 1, GETDATE())
			END
			SELECT 1

		END
		ELSE
		BEGIN

			SELECT 0

		END

	COMMIT TRANSACTION
	PRINT 'OK'

END TRY
BEGIN CATCH
	ROLLBACK
	PRINT 'FAIL'
END CATCH
GO
/****** Object:  StoredProcedure [dbo].[truncateIdentidad]    Script Date: 8/27/2015 8:21:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[truncateIdentidad]
AS

	-- OJO STORED PROCEDURE DELICADO! NO EJECUTAR

	--TRUNCATE TABLE catArchivos
	--TRUNCATE TABLE catBitacoraArchivos
	--TRUNCATE TABLE catBrandSites
	--TRUNCATE TABLE catColoresFavoritos
	--TRUNCATE TABLE catEstilosPorBrandSite
	--TRUNCATE TABLE catGlosario
	--TRUNCATE TABLE catGrupos
	--TRUNCATE TABLE catGruposPorArchivo
	--TRUNCATE TABLE catIndiceManual
	--TRUNCATE TABLE catLog
	--TRUNCATE TABLE catTags
	--TRUNCATE TABLE catTagsPorArchivo
	--TRUNCATE TABLE catUsuarios
	--TRUNCATE TABLE catUsuariosPorBrandSite
	--TRUNCATE TABLE catUsuariosPorGrupos
	--TRUNCATE TABLE catUsuariosPorUsuarios
	
	--GO
	
	--EXEC traUsuarios_NuevoDiseñador 'Rafael', '', '', 'raflores@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Jessica', '', '', 'jbarrien@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Juan Pablo', '', '', 'jumedina@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Pep', '', '', 'pep@ideograma.com'
	--EXEC traUsuarios_NuevoDiseñador 'Juan Carlos', '', '', 'jc@ideograma.com'
	--EXEC traUsuarios_NuevoDiseñador 'Anita', '', '', 'aarreola@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Liz', '', '', 'elozano@compusoluciones.com '
	--EXEC traUsuarios_NuevoDiseñador 'Joe', '', '', 'joe@ideograma.com'
	--EXEC traUsuarios_NuevoDiseñador 'Emilio', '', '', 'emilio@ideograma.com'
	--EXEC traUsuarios_NuevoDiseñador 'Geo', '', '', 'gmontoya@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Miguel', '', '', 'misoto@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Andrés', '', '', 'acarmona@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Beddy', '', '', 'bsoto@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Charly', '', '', 'caochoa@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Marce', '', '', 'crodrigu@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Alex', '', '', 'edpadill@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Fabi', '', '', 'einiguez@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Paquito', '', '', 'frrodrig@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Memo', '', '', 'gcoronad@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Isra', '', '', 'imeza@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Ariel', '', '', 'lramirez@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Luis', '', '', 'lugonzal@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Luis', '', '', 'lvillami@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Mauri', '', '', 'mpalomer@compusoluciones.com'
	--EXEC traUsuarios_NuevoDiseñador 'Daniela', '', '', 'dzulaica@compusoluciones.com'

	------ Beta Testers 1

	----EXEC traUsuarios_NuevoDiseñador 'Alejandra', 'Marín', '', 'ale@gurubrandburo.com'
	--EXEC Mail_RecuperarContraseña 'ale@gurubrandburo.com'
	----EXEC traUsuarios_NuevoDiseñador 'Maribel', 'Martínez', '', 'maribelmartinezgalindo@gmail.com'
	--EXEC Mail_RecuperarContraseña 'maribelmartinezgalindo@gmail.com'
	----EXEC traUsuarios_NuevoDiseñador 'Manuel', 'Macias', '', 'macias@rayaenmedio.com'
	--EXEC Mail_RecuperarContraseña 'macias@rayaenmedio.com'
	----EXEC traUsuarios_NuevoDiseñador 'Nayeli', 'Quinto', '', 'quinto@sodio.net'
	--EXEC Mail_RecuperarContraseña 'quinto@sodio.net'
	----EXEC traUsuarios_NuevoDiseñador 'Elías', 'Aquique', '', 'eaquique@me.com'
	--EXEC Mail_RecuperarContraseña 'eaquique@me.com'

	-------- Beta Testers 2

	----EXEC traUsuarios_NuevoDiseñador 'Carl', 'Forssell', '', 'carl@forssell.mx'
	--EXEC Mail_RecuperarContraseña 'carl@forssell.mx'
	----EXEC traUsuarios_NuevoDiseñador 'Eduardo', 'Espinoza', '', 'eduardo@tiposlibres.com'
	--EXEC Mail_RecuperarContraseña 'eduardo@tiposlibres.com'
	----EXEC traUsuarios_NuevoDiseñador 'Berenice', 'Tassier', '', 'berenice@retornotassier.com.mx'
	--EXEC Mail_RecuperarContraseña 'berenice@retornotassier.com.mx'
	----EXEC traUsuarios_NuevoDiseñador 'Juan', 'Baker', '', 'baker@ideograma.com'
	--EXEC Mail_RecuperarContraseña 'baker@ideograma.com'
	----EXEC traUsuarios_NuevoDiseñador 'Pepe', 'Cacho', '', 'cacho@ideograma.com'
	--EXEC Mail_RecuperarContraseña 'cacho@ideograma.com'
	----EXEC traUsuarios_NuevoDiseñador 'Ariadna', 'Hernández', '', 'ariadna@ideograma.com'
	--EXEC Mail_RecuperarContraseña 'ariadna@ideograma.com'
	----EXEC traUsuarios_NuevoDiseñador 'Gisela', 'Landa', '', 'gis@ideograma.com'
	--EXEC Mail_RecuperarContraseña 'gis@ideograma.com'
	----EXEC traUsuarios_NuevoDiseñador 'Gonzalo', 'Aguilar', '', 'gonzalo@ideograma.com'
	--EXEC Mail_RecuperarContraseña 'gonzalo@ideograma.com'

	-------- Beta Testers 3

	----EXEC traUsuarios_NuevoDiseñador 'Jorge', 'Cejudo', '', 'soy@elcejas.com'
	--EXEC Mail_RecuperarContraseña 'soy@elcejas.com'
	----EXEC traUsuarios_NuevoDiseñador 'Mario', 'Eskenazi', '', 'mario@m-eskenazi.com'
	--EXEC Mail_RecuperarContraseña 'mario@m-eskenazi.com'
	----EXEC traUsuarios_NuevoDiseñador 'Mercedes', 'Oseguera', '', 'mercedes.oseguera@gmail.com'
	--EXEC Mail_RecuperarContraseña 'mercedes.oseguera@gmail.com'
	----EXEC traUsuarios_NuevoDiseñador 'Rosemary ', 'Martínez', '', 'rosemarymartinez@artdesign.com.mx'
	--EXEC Mail_RecuperarContraseña 'rosemarymartinez@artdesign.com.mx'
	----EXEC traUsuarios_NuevoDiseñador 'Enrique', 'Saavedra', '', 'saavedra@solconsultores.com.mx'
	--EXEC Mail_RecuperarContraseña 'saavedra@solconsultores.com.mx'

	-------- Beta Testers 3

	----EXEC traUsuarios_NuevoDiseñador 'Mariana', 'Leegi', '', 'mleegis@gmail.com'
	--EXEC Mail_RecuperarContraseña 'mDleegis@gmail.com'

	-------- Beta Testers 4

	----EXEC traUsuarios_NuevoDiseñador 'Mariana', 'Tapia', '', 'tapia.mariana@gmail.com'
	----EXEC Mail_RecuperarContraseña 'tapia.mariana@gmail.com'

	-------- Beta testers 5 

	--------EXEC traUsuarios_NuevoDiseñador 'Mary Carmen', 'Zepeda', '', 'marycarmen@artdesign.com.mx'
	--------EXEC Mail_RecuperarContraseña 'marycarmen@artdesign.com.mx'

	------EXEC traUsuarios_NuevoDiseñador '', '', '', ''
	--EXEC Mail_RecuperarContraseña ''
GO
USE [master]
GO
ALTER DATABASE [Identidad] SET  READ_WRITE 
GO
