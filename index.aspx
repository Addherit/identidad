﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Identidad.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <meta name="google-site-verification" content="xQbATgheTw45Jgd_XcHLAGm6MvXWvM8XVAZhj03xkF4" /> 
    
    <title>Identidad.com | Iniciar sesión</title>
    <%--<link href="CSS/Pages/Default.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Site.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Design.css" rel="stylesheet" type="text/css" />
    <link href='/Images/Site/ID.ico' rel='shortcut icon' type='image/x-icon'/>
    <meta name="application-name" content="Identidad.com"/>
    <meta name="application-tooltip" content="Identidad.com"/>
    <meta name="application-bavbutton-color" content="Blue"/>
    <link href="/Images/Site/ID.ico" rel="icon" type="image/ico"/>
	<link href="/Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="apple-touch-icon-precomposed" href="/Images/Site/ID.ico"/>
	<link rel="apple-touch-icon" href="/Images/Site/ID.ico"/>
    <link rel="apple-touch-startup-image" href="/Images/Site/ID.ico" />
    <meta name="apple-mobile-web-app-title" content="Identidad.com"/>
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='apple-touch-fullscreen' content='yes'/>
    <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
    <link rel="icon" href="/Images/Site/ID.ico" />
	
    <meta charset="utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <link href="CSS/Pages/index.css" rel="stylesheet" type="text/css" />--%>
    <link rel="shortcut icon" href="/Images/Site/ID.ico" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'/>
    
    
    <link href="CSS/Pages/index.css" rel="stylesheet" />
    <link href="CSS/tooltipster.css" rel="stylesheet" />
    

</head>
<body>
    <form id="form1" class="tooltip" runat="server" >
    <div id="divContenido">
        <div id="tBienvenido">
            
            <label id="lblBienvenido" runat="server">Bienvenido</label><br />
            <label id="lblTexto" class="Texto" runat="server">Contruyamos</label><br />
            <label id="Label1" class="Texto" runat="server">tu marca</label>
        </div>
        <div id="botonesC">
            <a href="IDEN_Bienvenidos_documento.pdf" class="colorAqua" target="_blank" id="btnConocenos"><p style="margin-top:8px;">Conócenos</p></a>
            <input type="button" id="btnContactanos" class="colorAqua"  onclick="return false;" value="Contáctanos"/>
        </div>

        <div id="menuIngreso">
            <label id="lblSubtitulo" runat="server">Si ya eres cliente, ingresa</label><br />
            <input type="text" id="txtcorreoIngreso" class="txtregistro" runat="server" placeholder="     Correo electrónico"/><br />
            <input type="text" id="txtpassIngreso" class="txtregistro" runat="server" placeholder="     Contraseña"/><br />
            <label id="lblRecuperar" runat="server" onclick="recuperapass();">Recuperar Contraseña</label>
            <label id="lblvalidacionRP" runat="server" visible="false"></label><br />
            <button id="btnLogin" class="colorRosa" runat="server" onclick="login();">Login</button><br />
            <label id="lblvalidacionL" runat="server" visible="false"></label>
        </div>
        <div id="contentDefault">
            <label id="texto1" class="textoDefault" runat="server">Concentra todos</label><br />
            <label id="Label2" class="textoDefault" runat="server">tus recursos</label><br />
            <label id="Label3" class="textoDefault" runat="server">para dar </label>
            <label id="Label4" class="textoDefaultR" runat="server">vida</label><br />
            <label id="Label5" class="textoDefaultR" runat="server">a tu identidad</label>
        </div>
        <div id="menuRegistro">
            <label id="Label6" class="textoRegistro" runat="server">Encuentra un plan para ti</label><br />
            <label id="Label7" class="textoRegistro" runat="server">en funcion de lo que necesites:</label><br />
            <br />
            <input type="button" id="btnRegistro" class="colorRosa" runat="server" value="Regístrate ahora" />
        </div>
        <div id="CopyR">
            <label id="lblcopy" class="" runat="server">© 2015-2016 Identidad.com. All Rights Reserved.</label>
        </div>
    </div>
    <%--</form>--%>
   
<!--  Modal de contactanos -->
<div id="popUpContactanos" runat="server" class="modalContactanos">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">×</span>
      <h1 id="tituloC">Contáctanos</h1>
      <input id="txtNombresC" name="txtNombreC" type="text" class="textblancos textnombres" runat="server" placeholder="   Nombres" />
      <input id="txtApellidosC" name="txtNombreC" type="text" class="textblancos textapellidos" runat="server" placeholder="   Apellidos" />
      <input id="txtEmailC" name="txtNombreC" type="text" class="textblancos textcorreo" runat="server" placeholder="   Email" />
      <%--<input id="txtComentariosC" name="txtNombreR" type="text" cols="40" rows="5"  class="textblancos textcomentarios" runat="server" placeholder="   Comentarios" />--%>
      <textarea id="txtComentariosC" runat="server" class="textblancos textcomentarios" placeholder="   Comentarios"></textarea>
      <button id="btnCancelar" class="botonNegro botonCancelar" runat="server">Cancelar</button>
      <button id="btnEnviar" class="botonNegro botonEnviar" runat="server" onclick="mandarcorreo();">Enviar</button>
      <label id="lblmessage" runat="server"></label>
  </div>

</div>
<!--  Modal de iniciar sesion -->
<div id="popUpMisSitios" runat="server" class="modalMisSitios">
    
  <!-- Modal content -->
  <div class="modal-contentMs">
    <span class="closeMs">×</span>
      <h1 id="tituloMs">Mis sitios</h1>
      <hr />
      <div id="Adonde">
          <label id="lblSitios">¿A dónde quieres ir?</label>      
          <asp:DropDownList ID="ddlmissitios" runat="server"></asp:DropDownList>
      </div>
      <br />
      <input type="button" id="btnIr" runat="server" onclick="ir();" class="colorRosa" value="Ir"/>
  </div>

</div>
</form>


<!--  Modal de Registro -->
<form id="formRegistro">
<div id="popUpRegistrate" runat="server" class="modalRegistrate">

  <!-- Modal content -->
  <div class="modal-contentR">
    <span class="closeR">×</span>
      <h1 id="tituloR">Regístrate ahora</h1>
      <input id="Text1" name="txtNombreR" type="text" class="textblancos textnombres" runat="server" placeholder="   Nombres" required="required" />
      <input id="Text2" name="txtNombreR" type="text" class="textblancos textapellidos" runat="server" placeholder="   Apellidos" required="required" />
      <input id="Text3" name="txtNombreR" type="email" class="textblancos textcorreo" runat="server" placeholder="   Email" required="required" />
      <%--<input id="txtComentariosC" name="txtNombreR" type="text" cols="40" rows="5"  class="textblancos textcomentarios" runat="server" placeholder="   Comentarios" />--%>
      <input id="txtpassR" type="password" class="textblancos textcorreo" runat="server" placeholder="   Contraseña" required="required"/>
      <input id="txtconfirpass" type="password" class="textblancos textcorreo" runat="server" placeholder="   Confirmar Contraseña" required="required" />
      <br />   
                              <div id="divTerminos">  <div id="chkTerminos" class="checkbox-group">          
                            <input type="checkbox" id="c5" runat="server" name="c5" />       
                            <label for="c5"><span></span></label>
                            </div>  
      <asp:Label ID="lblAceptoTerminos" runat="server">Acepto los</asp:Label>
                                <asp:HyperLink ID="HlkAceptoTerminos" CssClass="subrayados" style="text-decoration:underline;" runat="server"  NavigateUrl="IDEN_Bienvenidos_documento.pdf" Target="_blank" Text="Terminos y condiciones"></asp:HyperLink>
                               </div>
       <br />
      <input type="button" id="btnCancelarR" class="botonNegro botonCancelar" runat="server" value="Cancelar"/>
      <input type="button" id="btnEnviarR" class="botonNegro botonEnviar" runat="server" value="Enviar"/>
      <div class="negritas">
                                <asp:Label ID="Label13" runat="server">¿Ya tienes cuenta?</asp:Label>
                                <asp:HyperLink ID="HyperLink1" CssClass="subrayados" runat="server" style="text-decoration:underline;"  NavigateUrl="site/index.aspx" Target="_blank" Text="Inicia Sesión"></asp:HyperLink>
                                </div>
  </div>

</div>

<!--  Modal de Registro Plan-->
<div id="popUpPlan" runat="server" class="modalPlan">

  <!-- Modal content -->
  <div class="modal-contentP">
    <span class="closeP">×</span>
                        <label id="lblTituloPosRegistro" class="tituloPosRegistro" runat="server">Accesible para</label><br />
                        <label id="lblTituloPosRegistro2" class="tituloPosRegistro" runat="server">todo</label>
                        <label id="lblTituloPosRegistro3" class="tituloPosRegistroRosa" runat="server">usuario</label>
                        <br /><br /><br />
                        <label id="lblTextoPosRegistro" class="TextoPosRegistro" runat="server">Encuentra un plan para ti</label><br />
                        <label id="lblTextoPosRegistro2" class="TextoPosRegistro" runat="server">en función de lo que necesites:</label>
                        <%--<asp:Button ID="btnRegistroEmpresa" CssClass="btnsRegistro" runat="server" Text="Eres empresa" OnClick="btnRegistroEmpresa_Click" />--%>
                        
                        <input type="button" id="btnRegistroEmpresa" class="btnsRegistro" runat="server" onclick="muestraEmpresa()" value="Eres empresa"/>
                        
                        <%--<asp:Button ID="btnRegistroDisenador" CssClass="btnsRegistro" runat="server" Text="Eres diseñador" />--%>
                        <input type="button" id="btnRegistroDisenador" class="btnsRegistro" runat="server" onclick="muestraDiseno()" value="Eres diseñador"/>

                        <br /><br /><br />
                        <div id="RegistroDisenador" class="RegistroDisenador" style="visibility:hidden;"  runat="server">
                            <div id="DescripcionDisenador">
                                <label id="lblTextoRegistroDisenador" runat="server">Amigo diseñador:</label><br />
                                <label id="lblTextoRegistroDisenador2" runat="server">Te ofrecemos 5 o 10 sitios</label><br />
                                <label id="lblTextoRegistroDisenador3" runat="server">permanentes de identidad.com,</label><br />
                                <label id="lblTextoRegistroDisenador4" runat="server">para que tu cliente pruebe</label><br />
                                <label id="lblTextoRegistroDisenador5" runat="server">la herramienta</label><br />
                                <br /><br />
                                <label id="lblTextoRegistroDisenador6" runat="server">Cada sitio tiene 3 meses</label><br />
                                <label id="lblTextoRegistroDisenador7" runat="server">de servicio de regalo, para que</label><br />
                                <label id="lblTextoRegistroDisenador8" runat="server">lo puedas armar y entregar.</label>
                            </div>
                            <div id="Plan1" class="Planes espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="lblPlan1Disenador" class="tituloPlan espacio" runat="server">Plan 1</label><br />
                                <label id="lblPlan1Disenador2" class="DescripcionPlan espacio" runat="server">(hasta 5 manuales</label><br />
                                <label id="lblPlan1Disenador3" class="DescripcionPlan espacio" runat="server">permanentes)</label><br />
                                <br />
                                <label id="lblPlan1Disenador4" class="gigas espacio" runat="server"><b>10</b> GB</label><br />
                                <br /><br />
                                <label id="lblPlan1Disenador5" class="pagos espacio" runat="server"><b>$10</b> Dólares/mes</label><br />
                                <label id="lblPlan1Disenador6" class="pagos espacio" runat="server"><b>$120</b> Dólares/año</label><br />
                                <label id="lblPlan1Disenador7" class="pagos espacio" runat="server"><b>$110</b> Dólares/Pago anual</label><br />
                                
                                <input type="button" id="btnComienza1" class="btnSeleccionarPlan espacio" onclick="Comienza1();" runat="server" value="Comienza ahora" />
                                <br />
                            </div>
                            <div id="Plan2" class="Planes espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="lblPlan2Disenador" class="tituloPlan espacio" runat="server">Plan 2</label><br />
                                <label id="lblPlan2Disenador2" class="DescripcionPlan espacio" runat="server">(hasta 10 manuales</label><br />
                                <label id="lblPlan2Disenador3" class="DescripcionPlan espacio" runat="server">permanentes)</label><br />
                                <br />
                                <label id="lblPlan2Disenador4" class="gigas espacio" runat="server"><b>20</b> GB</label><br />
                                <br /><br />
                                <label id="lblPlan2Disenador5" class="pagos espacio" runat="server"><b>$20</b> Dólares/mes</label><br />
                                <label id="lblPlan2Disenador6" class="pagos espacio" runat="server"><b>$240</b> Dólares/año</label><br />
                                <label id="lblPlan2Disenador7" class="pagos espacio" runat="server"><b>$220</b> Dólares/Pago anual</label><br />
                                
                                <input type="button" id="btnComienza2" class="btnSeleccionarPlan espacio" onclient="Comienza2();" runat="server" value="Comienza ahora" />
                                <br />
                            </div>
                        </div>
                        <div id="RegistroEmpresa" class="RegistroEmpresa" style="visibility:hidden;">
                            <div id="PlanPyme" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="Label16" class="tituloPlan espacio" runat="server">PYME</label><br />
                                 <label id="Label21" class="textoemp espacio" runat="server"><b>15</b> Usuarios</label><br />
                                 <label id="Label22" class="textoemp espacio" runat="server"><b>100</b> Archivos</label><br />
                                 <label id="Label23" class="textoemp espacio" runat="server"><b>0.5</b> Gigas</label><br />
                                <br />
                                <label id="Label24" class="textoemp espacio" runat="server"><b>$49</b></label><br />
                                <label id="Label8" class="textoemp espacio" runat="server">Dólares/mes</label><br />
                                <label id="Label9" class="textoemp espacio" runat="server"><b>$588</b></label><br />
                                <label id="Label14" class="textoemp espacio" runat="server">Dólares/año</label><br />
                                 <label id="Label25" class="textoemp espacio" runat="server"><b>$539</b></label><br />
                                 <label id="Label26" class="textoemp espacio" runat="server">Dólares/Pago anual</label><br />
                                                                
                                <input type="button" id="btnComienzaPYME" class="btnSeleccionarPlan espacio"  onclick="PYME();" runat="server" value="Comienza ahora"  autopostback="true" />
                                <br />
                            </div>
                             <div id="PlanEmpresa" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="Label17" class="tituloPlan espacio" runat="server">Empresa</label><br />
                                 <label id="Label19" class="textoemp espacio" runat="server"><b>50</b> Usuariso</label><br />
                                 <label id="Label20" class="textoemp espacio" runat="server"><b>300</b> Archivos</label><br />
                                 <label id="Label27" class="textoemp espacio" runat="server"><b>2</b> Gigas</label><br />
                                <br />
                                <label id="Label28" class="textoemp espacio" runat="server"><b>$139</b></label><br />
                                <label id="Label29" class="textoemp espacio" runat="server">Dólares/mes</label><br />
                                <label id="Label30" class="textoemp espacio" runat="server"><b>$1,668</b></label><br />
                                <label id="Label31" class="textoemp espacio" runat="server">Dólares/año</label><br />
                                 <label id="Label32" class="textoemp espacio" runat="server"><b>$1,529</b></label><br />
                                 <label id="Label33" class="textoemp espacio" runat="server">Dólares/Pago anual</label><br />
                                
                                <input type="button" id="btnComienzaEmpresa" class="btnSeleccionarPlan espacio" onclick="Empresa();"  runat="server" value="Comienza ahora" />
                                <br />
                            </div>
                             <div id="PlanCorporativo" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="Label34" class="tituloPlan espacio" runat="server">Corporativo</label><br />
                                 <label id="Label35" class="textoemp espacio" runat="server"><b>100</b> Usuarios</label><br />
                                 <label id="Label36" class="textoemp espacio" runat="server"><b>600</b> Archivos</label><br />
                                 <label id="Label37" class="textoemp espacio" runat="server"><b>3</b> Gigas</label><br />
                                <br />
                                <label id="Label38" class="textoemp espacio" runat="server"><b>$259</b></label><br />
                                <label id="Label39" class="textoemp espacio" runat="server">Dólares/mes</label><br />
                                <label id="Label40" class="textoemp espacio" runat="server"><b>$3,108</b></label><br />
                                <label id="Label41" class="textoemp espacio" runat="server">Dólares/año</label><br />
                                 <label id="Label42" class="textoemp espacio" runat="server"><b>$2,849</b></label><br />
                                 <label id="Label43" class="textoemp espacio" runat="server">Dólares/Pago anual</label><br />
                                
                                <input type="button" id="btnCorporativo" class="btnSeleccionarPlan espacio" onclick="Corporativo();" runat="server" value="Comienza ahora" />
                                <br />
                            </div>
                             <div id="PlanPremium" class="PlanesE espacio" style="border:1px solid #FF008C;">
                                <br />
                                <label id="Label44" class="tituloPlan espacio" runat="server">Premium</label><br />
                                 <label id="Label45" class="textoemp espacio" runat="server"><b>250</b> Usuarios</label><br />
                                 <label id="Label46" class="textoemp espacio" runat="server"><b>1400</b> Archivos</label><br />
                                 <label id="Label47" class="textoemp espacio" runat="server"><b>7</b> Gigas</label><br />
                                <br />
                                <label id="Label48" class="textoemp espacio" runat="server"><b>$599</b></label><br />
                                <label id="Label49" class="textoemp espacio" runat="server">Dólares/mes</label><br />
                                <label id="Label50" class="textoemp espacio" runat="server"><b>$7,188</b></label><br />
                                <label id="Label51" class="textoemp espacio" runat="server">Dólares/año</label><br />
                                 <label id="Label52" class="textoemp espacio" runat="server"><b>$6,589</b></label><br />
                                 <label id="Label53" class="textoemp espacio" runat="server">Dólares/Pago anual</label><br />
                                
                                <input type="button" id="btnPremium" class="btnSeleccionarPlan espacio" onclick="Premium();" runat="server" value="Comienza ahora" />
                                <br />
                            </div>
                      
  </div>

</div>
    </div>
        </form>

    
<!--  Modal de Registro Plan-->
<div id="popUpPagar" runat="server" class="modalPlan">
    <!-- Modal content -->
    <div class="modal-contentPagar">
    <span class="closePagar">×</span>
        <form id="formconekta" class="tooltip">
                <label id="Label55" class="tituloPosRegistro" runat="server">identidad.com</label>
                <label id="Label56" class="tituloPosRegistroRosa" runat="server">para el futuro</label>
                <br /><br />
                <div class="columna1Pagos">
                <label class="lblTituloDatosF"  runat="server">Datos de facturación</label><br /><br />
                    <select id="ddlPaisdf" class="txtDatosPagoG" onblur="selPais();" runat="server" required="required"></select><br />
                    <label class="lblDatosObligatorios">Para cuestiones de facturación indica tu país.</label>
                    <br />
                    <br />
                <div class="checkbox-group">          
                    <input type="checkbox" id="c1" name="c1" onclick="checkpersona();"  runat="server" />       
                    <label for="c1" ><span ></span></label>
                    </div>
                <br />
                <div class="checkbox-group">          
                    <input type="checkbox" id="c2" name="c2" onclick="checkempresa();" runat="server" />       
                    <label for="c2"><span></span></label>
                    </div>

                <label class="lblPersona" style="position:absolute;">Persona</label>
                <label class="lblEmpresa" style="position:absolute;">Empresa</label>
                    <br />                        
                <input id="txtRFCdf" type="text" runat="server" class="txtDatosPagoG" placeholder="   RFC *" style="visibility:hidden;" pattern="[A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])[A-Z|\d]{3}"/><br /><br />
                <input id="txtNombresdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Nombres *" required="required"/>
                <input id="txtApellidosdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Apellidos *" required="required"/>
                <input id="txtRazonSocialdf" type="text" runat="server" class="txtDatosPagoG" style="visibility:hidden;" placeholder="   Razón Social *" required="required"/>
                    <br /><br />
                <input id="txtCalledf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Calle *" required="required"/>
                <input id="txtNoExtdf" type="text" runat="server" class="txtDatosPagoCh" placeholder="   No. Ext. *" number="true" required="required"/>
                <input id="txtNoIntdf" type="text" runat="server" class="txtDatosPagoCh" placeholder="   No. Int." number="true"/>
                    <br /><br />
                <input id="txtColoniadf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Colonia*" required="required"/>
                <input id="txtDelegaciondf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Delegación/Municipio *" required="required"/>
                    <br /><br />
                <input id="txtEstadodf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Estado *" required="required"/>
                <input id="txtCPdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   Código Postal *" number="true" required="required" maxlength="5"/>
                    <br /><br />
                <%--<input id="txtPaisdf" type="text" runat="server" class="txtDatosPagoM" placeholder="   País *"/><br /><br />--%>
                <%--<input id="txtEmaildf" type="text" runat="server" class="txtDatosPagoG" placeholder="   Email *"/><br />--%>

                <label class="lblDatosObligatorios">Información requerida *</label>
                </div> 
        <%--</form>
        <form id="formPago" class="tooltip">--%>
                <div class="columna2Pagos">
                    <span class="card-errors"></span><br />
                    <label class="lblTituloDatosF"  runat="server">Plan elegido</label><br /><br />
                    <label id="NombrePlan" class="lblPlan" runat="server" value="PLAN">PLAN</label>
                    <label id="CantidadTotal" class="lblCantidad" runat="server" value="$0.00">$0.00</label>
                    <label class="lblTotal" runat="server" value="Total:">Total:</label>

                    <br /><br />
                    <div class="checkbox-group">          
                    <input type="checkbox" id="c3" name="c3" onclick="checkmes();" runat="server"/>       
                    <label for="c3"><span></span></label>
                    </div>
                    <label id="lblmes" class="lblCantidades" runat="server" value="$0.00">$0.00</label>
                    <label id="Label57" class="lblPeriodo" runat="server" value="Dólares/mes">Dólares/mes</label>

                    <div id="pagoanual" class="checkbox-group">          
                    <input type="checkbox" id="c4" name="c4" onclick="checkanual();" runat="server"/>       
                    <label for="c4"><span></span></label>
                    </div>
                    <label id="lblanual" class="lblCantidadesA" runat="server" value="$0.00">$0.00</label>
                    <label id="Label59" class="lblPeriodoA" runat="server" value="Dólares/Pago anual">Dólares/Pago anual</label>
                    <br /><br />
                    <%--<label id="lblFormaDePago" runat="server" value="Forma de pago"></label>
                    <br /><br />--%>

                           

                    <input type="text" class="txtDatosPagoG" data-conekta="card[number]" placeholder="   Número de tarjeta *"/>
                    <br /><br />
                    <input type="text" class="txtDatosPagoG" data-conekta="card[name]" placeholder="   Nombre (como aparece en la tarjeta) *"/>
                    <br /><br />
                    <select class="ddlVencimiento" data-conekta="card[exp_month]">
                        <option value="">  Mes de Vencimiento</option>
                        <option value=1>   Enero (01)</option>
                        <option value=2>   Febrero (02)</option>
                        <option value=3>   Marzo (03)</option>
                        <option value=4>   Abril (04)</option>
                        <option value=5>   Mayo (05)</option>
                        <option value=6>   Junio (06)</option>
                        <option value=7>   Julio (07)</option>
                        <option value=8>   Agosto (08)</option>
                        <option value=9>   Septiembre (09)</option>
                        <option value=10>   Octubre (10)</option>
                        <option value=11>   Noviembre (11)</option>
                        <option value=12>   Diciembre (12)</option>
                    </select>
                    <select class="ddlVencimiento" data-conekta="card[exp_year]">
                        <option value="">  Año de Vencimiento</option>
                        <option value=2016>   16</option>
                        <option value=2017>   17</option>
                        <option value=2018>   18</option>
                        <option value=2019>   19</option>
                        <option value=2020>   20</option>
                        <option value=2021>   21</option>
                        <option value=2022>   22</option>
                        <option value=2023>   23</option>
                        <option value=2024>   24</option>
                        <option value=2025>   25</option>
                              
                    </select>
                    <br /><br />
                    <input type="text" class="txtDatosPagoM" data-conekta="card[cvc]" placeholder="   Código de seguridad *"/>
                    <br /><br />
                    <button id="btnEnviarPago" type="submit" class="colorRosaPago" disabled="disabled">Enviar Pago</button><br />
                    <label id="lblErrorPagos" runat="server" Visible="false"></label>
                            
                </div>
                
            </form>
    </div>
</div>
    
    <div style=" position:absolute;">
    <input type="button" id="btnmissitios" value="prueba mis sitios"/>
        <label id="label10" runat="server"></label>
    </div>
    <script src="JS/index.js"></script>
     
     <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/jquery.validate.min.js"></script>
    <script type="text/javascript" src="http://cdn.jsdelivr.net/jquery.validation/1.15.0/additional-methods.min.js"></script>
    <script type="text/javascript" src="JS/jquery.tooltipster.min.js"></script> 

<script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>

<script>
        $(document).ready(function () {
            $('.tooltip').tooltipster();
        });
    </script> 

<script type="text/javascript">

        // Conekta Public Key
        Conekta.setPublishableKey('key_ByMFFx74HgvCZJRB8diLjRw');

        // ...
        $(function () {
            $("#formPago").submit(function (event) {
                var $form = $(this);

                // Previene hacer submit más de una vez
                $form.find("button").prop("disabled", true);
                Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);

                // Previene que la información de la forma sea enviada al servidor
                return false;
            });
        });

        var conektaSuccessResponseHandler = function (token) {
            var $form = $("#formPago");
            var cardtoken = token.id;
            //console.log(cardtoken);
            //alert(cardtoken);
            /* Inserta el token_id en la forma para que se envíe al servidor */
            $form.append($("<input type='hidden' name='conektaTokenId'>").val(token.id));
            /* and submit */
            var description = document.getElementById("NombrePlan").innerHTML;
            var str = document.getElementById("CantidadTotal").innerHTML;
            var res = str.replace("$", "");
            var res1 = res.replace(".", "");
            var amount = res1.replace(",", "");
            var nombres = document.getElementById("Text1").value;
            var apellidos = document.getElementById("Text2").value;
            var correo = document.getElementById("Text3").value;
            var sjson = "{\"description\": \" "+ description +"\",\"amount\": "+ amount +",\"currency\": \"USD\",\"card\": \""+ cardtoken +"\",\"details\": {\"name\": \""+nombres+" "+apellidos+"\",\"phone\": \"00000000000\",\"email\": \""+ correo +"\",\"line_items\": [{\"name\": \"Identidad\",\"description\": \""+description+"\",\"unit_price\": " + amount + ",\"quantity\": 1}]}}";
               // "{\"description\": \" " + description + " \",\"amount\": " + amount + ",\"currency\":\"USD\",\"card\": \"" + card_token + "\",\"details\": {\"name\": \"" + name + "\",\"phone\": \"" + phone + "\",\"email\": \"" + email + "\",\"line_items\": [{\"name\": \"" + nombreP + "\",\"description\": \"Plan: " + nombreP + "\",\"unit_price\": " + amount + ",\"quantity\": 1}] } }";
            if ($("#formconketa").valid())
            {
                __doPostBack('Pagar_Click', sjson);
            }
            else
            {
                //$("#formconketa").
                $form.find("button").prop("disable", false);
            }
        };

        var conektaErrorResponseHandler = function (response) {
            var $form = $("#formconketa");
            //alert("error");
            /* Muestra los errores en la forma */
            $form.find(".card-errors").text(response.message);
            $form.find("button").prop("disabled", false);
        };

</script>


<script>$('#formRegistro input[type="text"]').tooltipster({
    trigger: 'custom',
    onlyOne: false,
    position: 'right'
});
    $('#formRegistro input[type="email"]').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'right'
    });
    $('#formRegistro input[type="password"]').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'right'
    });
    $('#formRegistro input[type="checkbox"]').tooltipster({
        trigger: 'custom',
        onlyOne: false,
        position: 'left'
    });
</script>        

<script>
    // initialize validate plugin on the form
    $("#formRegistro").validate({
        rules: {
            txtconfirpass: {
                equalTo: "#txtpassR"
            }
        },
        // any other options & rules,
        errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
            $(element).tooltipster('show');
        },
        success: function (label, element) {
            $(element).tooltipster('hide');
        }
    });
</script>

<script>$('#formconekta input[type="text"]').tooltipster({
    trigger: 'custom',
    onlyOne: false,
    position: 'right'
});
     $('#formconekta input[type="email"]').tooltipster({
         trigger: 'custom',
         onlyOne: false,
         position: 'right'
     });
     $('#formconekta input[type="checkbox"]').tooltipster({
         trigger: 'custom',
         onlyOne: false,
         position: 'left'
     });
</script> 

<script>
    // initialize validate plugin on the form
    $("#formconekta").validate({
        // any other options & rules,
        errorPlacement: function (error, element) {
            $(element).tooltipster('update', $(error).text());
            $(element).tooltipster('show');
            $form.find("button").prop("disabled", true);
        },
        success: function (label, element) {
            $(element).tooltipster('hide');
            $form.find("button").prop("disabled", false);
        }
    });
</script>



</body>
</html>
