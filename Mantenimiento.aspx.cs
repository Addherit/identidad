﻿using System;

namespace Identidad
{
    public partial class mantenimiento : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        App_Code.cSistema oSistema = new App_Code.cSistema();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            // Dejo este try catch en blanco, para poderse usar en el futuro

            try
            {
                oSistema.saveLog(Session["Exception"].ToString());
            }
            catch (Exception)
            {
            }
        }

    }
}