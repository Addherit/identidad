﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Net;
using System.Threading;
using System.Web;

namespace Identidad
{
    public class Global : System.Web.HttpApplication
    {
 
        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                string lang = string.Empty; //default to the invariant culture

                HttpCookie cookie = Request.Cookies["idioma"];

                if (cookie != null && cookie.Value != null)
                {

                    lang = cookie.Value;

                    Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo(lang);

                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(lang);

                }
            }
            catch (Exception)
            {
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            if (Server.GetLastError() != null)
            {
                if (Server.GetLastError().InnerException != null)
                {
                    Exception exception = Server.GetLastError();
                    Session["Exception"] = exception.ToString();
                    Server.ClearError();
                    Response.Redirect("~/Mantenimiento.aspx");
                }
            }       
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {

        }
    }
}