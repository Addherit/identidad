﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MiPerfil.ascx.cs" Inherits="Identidad.Controles.MiPerfil"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link href="../CSS/Site.css" rel="stylesheet" type="text/css" />
<link href="../CSS/Pages/MiPerfil.css" rel="stylesheet" type="text/css" />
<script src="../JS/Site.js" type="text/javascript"></script>
<style>
.ajax__combobox_itemlist
{
position: relative !important;
height: 100px !important;
overflow: auto !important;
top: auto !important;
left: auto !important;
}     
</style>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Label ID="lblIdUsuario" runat="server" visible="false" Text="0"></asp:Label>
        <asp:Label ID="lblPass" runat="server" visible="false"></asp:Label>
        <asp:Label ID="lblMessage" runat="server" visible="false"></asp:Label>

        <div id="divFormulario" class="divTabla">
            <div class="divTR">
                <div class="divTablaTitulo">
                    <asp:Label ID="lblMiPerfil" runat="server" meta:resourcekey="lblMiPerfil"></asp:Label>
                </div>
                <div class="divTablaTituloImagen">
                    
                </div
            </div>
            <hr />
            <div class="divFotoPerfil">
                <asp:Image ID="imgFoto" CssClass="imgPerfil imgCircular" runat="server"></asp:Image>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblNombre" runat="server" meta:resourcekey="lblNombre" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtNombre" CssClass="TextBox" runat="server" onblur="onLeave(this)" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblApellidoPaterno" runat="server" meta:resourcekey="lblApellidoPaterno" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtApellidoPaterno" CssClass="TextBox" runat="server" Width="250px"></asp:TextBox>
                </div>
            </div>
            <%--<div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblApellidoMaterno" runat="server" meta:resourcekey="lblApellidoMaterno" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtApellidoMaterno" CssClass="TextBox" runat="server" Width="250px"></asp:TextBox>
                </div>
            </div>--%>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblTelefono" runat="server" meta:resourcekey="lblTelefono" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtTelefono" CssClass="TextBox" runat="server" onkeyup="fnTelefono();" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="divTR" >
                <div class="divTDIzq">
                    <asp:Label ID="lblEmpresa" runat="server" meta:resourcekey="lblEmpresa" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:ComboBox ID="ComboBox1" runat="server" AutoCompleteMode="Suggest" DataSourceID="SqlDataSource1" DataTextField="NombreEmpresa" DataValueField="NombreEmpresa" DropDownStyle="Simple" MaxLength="0" style="display: inline; margin-left:10px;" Width="230px" AppendDataBoundItems="True" AutoPostBack="True" OnSelectedIndexChanged="ComboBox1_SelectedIndexChanged">
                        <asp:ListItem>Otro</asp:ListItem>
                    </asp:ComboBox>               
                    <asp:TextBox ID="txtOtraEmpresa" runat="server" Visible="false" Width="175px"></asp:TextBox>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="SELECT NombreEmpresa FROM catEmpresa WHERE (Activo IS NOT NULL) AND (NombreEmpresa &lt;&gt; 'Otro') ORDER BY NombreEmpresa"></asp:SqlDataSource>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblImagenPerfil" runat="server" meta:resourcekey="lblImagenPerfil" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:FileUpload ID="fluImagenPerfil" runat="server" Width="250px"></asp:FileUpload>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblCorreoElectronico" runat="server" meta:resourcekey="lblCorreoElectronico" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtCorreoElectronico" CssClass="TextBox" runat="server"  onblur="onLeave(this)" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                     <asp:Label ID="lblContraseña" runat="server" meta:resourcekey="lblContraseña" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtContraseña" CssClass="TextBox" TextMode="Password" runat="server" onblur="onLeave(this)" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    <asp:Label ID="lblConfirmarContraseña" runat="server" meta:resourcekey="lblConfirmarContraseña" ></asp:Label>
                </div>
                <div class="divTDDerechoInput">
                    <asp:TextBox ID="txtConfirmarContraseña" CssClass="TextBox" TextMode="Password" runat="server" onblur="onLeave(this)" Width="250px"></asp:TextBox>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    
                </div>
                <div class="divTDDerechoInput">
                    <asp:Label ID="lblValidacion" style="margin-left:11px;" runat="server" visible="false" ></asp:Label>
                </div>
            </div>
            <div class="divTR">
                <div class="divTDIzq">
                    
                </div>
                <div class="divTDDerechoButtons">
                    
                    <asp:Button ID="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" Visible="false" OnClick="btnCancelar_Click" />
                    <asp:Button ID="btnAceptar" style="margin-left:10px;" class="buttons colorAzulO" runat="server" meta:resourcekey="btnAceptar" OnClientClick="return ValidaInputs('divFormulario');" OnClick="btnAceptar_Click" />

                </div>
            </div>
            
        </div>

        <script type="text/javascript">
            function fnTelefono()
            {
                document.getElementById('ContentPlaceHolder1_MiPerfil_txtTelefono').value = document.getElementById('ContentPlaceHolder1_MiPerfil_txtTelefono').value.replace(/[^0-9]+/g, '');
            }
        </script>

    </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnAceptar" />
        </Triggers>
</asp:UpdatePanel>

