﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Examinar.ascx.cs" Inherits="Identidad.Controles.Examinar" meta:resourcekey="Examinar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<cfheader name="X-XSS-Protection" value="0">

<asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
    <ContentTemplate>
        
        <link href="../CSS/Site.css" rel="stylesheet" type="text/css" />
        <link href="../CSS/Pages/Examinar.css" rel="stylesheet" type="text/css" />
        <link href="../CSS/Pages/tooltip.css" rel="stylesheet" type="text/css" />
        <script src="../JS/Site.js" type="text/javascript"></script>

        <div>

            <asp:Label ID="lblVarIdBrandSite" runat="server" visible="false" Text=""></asp:Label>
            <asp:Label ID="lblVarUrl" runat="server" visible="false" Text=""></asp:Label>
            <asp:Label ID="lblVarIdArchivo" runat="server" visible="false" Text=""></asp:Label>
            <asp:Label ID="lblVarSoloImagenes" runat="server" visible="false" Text=""></asp:Label>

            <!--Inicia Popup para explorar --->
            <panel id="pnlExplorador" runat="server" style="display: none">
                <div class="divTabla divTablaExaminar Popup">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblExplorador" runat="server" meta:resourcekey="lblExplorador"></asp:Label>
                    </div>
                    <div class="divTablaTituloImagen">
                        <div>
                            <div class="wrapper">
                                <asp:Image ID="imgToolTip" runat="server" ImageUrl="../Images/Site/info.png" Visible="false"></asp:Image>
                                <div class="tooltip">
                                    <asp:Label ID="lblToolTip" runat="server" Text=""></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />  
                    <div class="divControlesExaminar">
                        <%--<asp:Label ID="lblEtBusqueda" runat="server" meta:resourcekey="lblEtBusqueda"></asp:Label>--%>
                        <asp:TextBox ID="txtBusqueda" CssClass="TextBox" runat="server"></asp:TextBox>&nbsp
                        <asp:Button ID="btnBuscar" class="buttons colorAzulO" runat="server" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click" />
                        <asp:Button ID="btnSubirNuevo" class="buttons colorVerde" runat="server" meta:resourcekey="btnSubirNuevo" OnClick="btnSubirNuevo_Click" />
                        <asp:Button ID="btnCancelar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                    </div>
                    <div class="gridExaminar">
                            <asp:GridView ID="gdvArchivos" CssClass="mGrid" runat="server" AutoGenerateColumns="false" >
                                 <Columns>
                                    <asp:TemplateField >
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td style="width:150px;">
                                                            <asp:Label ID="lblUrl" runat="server" DataField ="Url" Text='<%#Eval ("Url") %>' Visible="false" ></asp:Label>
                                                            <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                                            <asp:ImageButton ID="imgPreview" CssClass="gridExaminarImagen" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" OnClick="imgPreview_Click"></asp:ImageButton>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblNombre" CssClass="gridExaminarNombreImagen" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                            
                                                            <asp:Label ID="lblArchivoTrabajo" runat="server" meta:resourcekey="lblArchivoTrabajo" Visible='<%#Eval ("Archivotrabajo") %>' ></asp:Label>
                                                            
                                                            <asp:Label ID="lblExtension" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                                            <br />
                                                            <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                                            <br /><br />
                                                            <asp:Button ID="btnSeleccionar" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionar" Text="btnSeleccionar" OnClick="btnSeleccionar_Click"></asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                 </Columns>
                            </asp:GridView> 
                    </div>
                </div>
            </panel>

            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppExaminar" TargetControlID="Label1" runat="server" PopupControlID="pnlExplorador" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".30" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup--->

            <!--Inicia Popup para subir un archivo--->
            <panel id="pnlSubirArchivo" runat="server" style="display: none">
                <div class="divTabla divTablaExaminar Popup" style="height:580px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloSubirArchivo" runat="server" meta:resourcekey="lblTituloSubirArchivo" ></asp:Label>
                    </div>
                    <hr />
                    <div class="divTR">
                            <div class="divTDIzq">
                                <asp:Label ID="lblDetectarNombre" runat="server" meta:resourcekey="lblDetectarNombre" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:CheckBox ID="ckbDetectarNombre" runat="server" Style="padding-top:30px;" AutoPostBack="true" OnCheckedChanged="ckbDetectarNombre_CheckedChanged" ></asp:CheckBox>
                            </div>
                        </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                             <asp:Label ID="lblNombre" runat="server" meta:resourcekey="lblNombre"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtNombre" CssClass="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblDescripcion" runat="server" meta:resourcekey="lblDescripcion" ></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtDescripcion" CssClass="Multiline" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblArchivoTrabajoEt" runat="server" meta:resourcekey="lblArchivoTrabajo" ></asp:Label>
                        </div>
                        <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                            <asp:CheckBox ID="cbArchivoTrabajo" runat="server" Style="padding-top:30px;" OnCheckedChanged="cbArchivoTrabajo_CheckedChanged" AutoPostBack="true"></asp:CheckBox>
                        </div>
                    </div>
                    <div class="divTR" style="height:155px;">
                            <div class="divTDIzq">
                                <asp:Label ID="lblGrupos" runat="server" meta:resourcekey="lblGrupos" ></asp:Label>
                            </div>
                            <div class="divTDDerechoInput" style="vertical-align: middle; display: table-cell;">
                                <asp:DropDownList ID="ddlGrupos" runat="server" CssClass="DropDownList" ></asp:DropDownList>
                                <asp:Button ID="btnAgregar" style="width:20px; height:30px!important;" CssClass="buttons colorVerde" runat="server" meta:resourcekey="lkbAgregarGrupo" OnClick="btnAgregar_Click"></asp:Button>
                                <br /><br />
                                <div style="min-height:100px; overflow-y:auto; height:100px; margin-left:-3px;">
                                    <asp:GridView ID="gvGruposQueVen" runat="server" AutoGenerateColumns="false" CssClass="mGrid" OnRowDeleting="gvGruposQueVen_RowDeleting" OnRowDeleted="gvGruposQueVen_RowDeleted">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblIdGrupo" runat="server" DataField ="IdGrupo" Text='<%#Eval ("IdGrupo") %>' Visible="false" ></asp:Label>
                                                    <asp:Label ID="lblNombreGrupo" runat="server" DataField ="NombreGrupo" Text='<%#Eval ("NombreGrupo") %>' ></asp:Label>
                                                    <asp:Button ID="btnElimigarGrupo" style=" min-width:20px!important; width:25px!important; min-height:25px!important; height:20px!important;" CssClass="buttons colorGris" runat="server" meta:resourcekey="lkbEliminarGrupo" OnClick="btnElimigarGrupo_Click"></asp:Button>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                    </div>

                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblArchivo" runat="server" meta:resourcekey="lblArchivo" ></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:FileUpload ID="fluArchivo" runat="server" ></asp:FileUpload>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblPreview" runat="server" meta:resourcekey="lblPreview" ></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:FileUpload ID="fluPreview" runat="server"></asp:FileUpload>
                        </div>
                    </div>

                    <div class="divTR" style="height:30px;">
                        <div class="divTDIzq">

                        </div>
                        <div class="divTDDerechoInput">
                            <asp:Label ID="lblValidacion" runat="server" visible="false" style="margin-left:18px;" ></asp:Label>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">

                        </div>
                        <div class="divTDDerechoButtons">
                            <asp:Button ID="btnCancelarSubida" CssClass="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelarSubida_Click" />
                            <asp:Button ID="btnSubir" OnClientClick="return postbackButtonClick();" AutoPostBack="true" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSubir" OnClick="btnSubir_Click" />
                        </div>
                    </div>
                    <div class="divTR">

                    </div>
                </div>
            </panel>

            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppSubirArchivo" TargetControlID="Label2" runat="server" PopupControlID="pnlSubirArchivo" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para subir un archivo--->

            <%--<script type="text/javascript">

                // onchange="fnFluArchivo();"

                function invokeMeMaster()
                {
                    alert('entra');

                    var chkPostBack = '<%= Page.IsPostBack ? "true" : "false" %>';

                    alert(chkPostBack);

                    if (chkPostBack == 'false')
                    {
                        alert('Only the first time');
                    }
                    else
                    {
                        alert('second time');
                    }
                }

                window.onload = function () { invokeMeMaster(); };

                function fnFluArchivo()
                {
                    var fluArchivoValue = document.getElementById('ContentPlaceHolder1_eExaminarLogo_fluArchivo').value;
                    
                    '<%Session["fluArchivo"] = "' + fluArchivoValue + '"; %>';

                    alert('<%=Session["fluArchivo"] %>');
                }

            </script>--%>

        </div>

    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubir" />
    </Triggers>
</asp:UpdatePanel>