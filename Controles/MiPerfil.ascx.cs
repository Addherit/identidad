﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Identidad.App_Code;
using System.Collections.Generic;

namespace Identidad.Controles
{
    public partial class MiPerfil : System.Web.UI.UserControl
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cUsuario oUsuario = new cUsuario();
        private cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtNombre.Focus();
                btnAceptar.Focus();

                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    
                    CargarDatos();
                }
                //CargarDatos();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarDatos()
        {
            foreach (DataRow row in oUsuario.cargarUsuario(lblIdUsuario.Text).Rows)
            {
                Session["FotoUsuario"] = row["Foto"].ToString();
                imgFoto.ImageUrl = row["Foto"].ToString();
                txtNombre.Text = row["Nombre"].ToString();
                txtApellidoPaterno.Text = row["ApellidoPaterno"].ToString() + " "+ row["ApellidoMaterno"].ToString();
                //txtApellidoMaterno.Text = row["ApellidoMaterno"].ToString();
                ComboBox1.SelectedValue = row["NombreEmpresa"].ToString();
                txtCorreoElectronico.Text = row["CorreoElectronico"].ToString();
                txtContraseña.Attributes.Add("value", ""/*row["Contraseña"].ToString()*/);
                txtConfirmarContraseña.Attributes.Add("value", ""/*row["Contraseña"].ToString()*/);
                txtTelefono.Text = row["Telefono"].ToString();
                lblValidacion.Visible = false;
            }
        } // Carga los datos del usuario

        public void setIdUsuario(string sIdUsuario, bool bBtnCancelar)
        {
            btnCancelar.Visible = bBtnCancelar;

            setIdUsuario(sIdUsuario);
        } // Permite al programador poner o quitar el botón de cancelar, y establece el id de usuario a cargar

        public void setIdUsuario(string sIdUsuarioNew)
        {
            lblIdUsuario.Text = sIdUsuarioNew;

            if (lblIdUsuario.Text == "0")
            {
                imgFoto.ImageUrl = "";
                txtNombre.Text = "";
                txtApellidoPaterno.Text = "";
                //txtApellidoMaterno.Text = "";
                txtCorreoElectronico.Text = "";
                txtTelefono.Text = "";
                lblValidacion.Visible = false;
                lblCorreoElectronico.Visible = true;
                txtCorreoElectronico.Enabled = true;
                txtCorreoElectronico.Visible = true;
                lblContraseña.Visible = false;
                txtContraseña.Visible = false;
                txtContraseña.Text = "NULL";
                txtContraseña.Attributes.Add("value", "NULL");
                lblConfirmarContraseña.Visible = false;
                txtConfirmarContraseña.Visible = false;
                txtConfirmarContraseña.Text = "NULL";
                txtConfirmarContraseña.Attributes.Add("NULL", "NULL");
                lblEmpresa.Visible = true;
                //lblEmpresa.Text = "";
                //txtOtraEmpresa.Attributes.Add("NULL", "NULL");
                //ComboBox1.Attributes.Add("NULL", "NULL");
            }
            else
            {
                lblCorreoElectronico.Visible = true;
                txtCorreoElectronico.Visible = true;
                txtCorreoElectronico.Enabled = true;
                lblContraseña.Visible = true;
                txtContraseña.Visible = true;
                lblConfirmarContraseña.Visible = true;
                txtConfirmarContraseña.Visible = true;
                lblEmpresa.Visible = true;
            }

            CargarDatos();
        } // Establece el id de usuario a cargar

        public cMetodo ObtenerEstado()
        {
            oMetodo.Pass = bool.Parse(lblPass.Text);
            oMetodo.Message = lblMessage.Text;

            return oMetodo;
        } // Obtenemos el estado (si se logro o no afectar el procedimiento) para que cachemos este dato en la página donde usemos el control

        #endregion

            // Eventos para el manejo de eventos del user control

            #region Eventos creados para el user control

            // Creo un evento para que cuando se seleccione un archivo regrese y se obtenga la url

        public delegate void EventHandler(Object obj, EventArgs e);
        public event EventHandler EstadoCatalogo;

        protected void OnEstadoCatalogo()
        {
            if (EstadoCatalogo != null)
            {
                EstadoCatalogo(this, new EventArgs());
            }
        }

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                string sContraseña, sConfirmarContraseña = string.Empty;

                if (lblIdUsuario.Text == "0")
                {
                    sContraseña = sConfirmarContraseña = oUsuario.CrearContraseña(txtNombre.Text, txtCorreoElectronico.Text);
                }
                else
                {
                    sContraseña = txtContraseña.Text;
                    sConfirmarContraseña = txtConfirmarContraseña.Text;
                }

                if (ComboBox1.SelectedValue.Equals("Otro"))
                {
                    if (txtOtraEmpresa.Text.Length > 0)
                    {
                        oMetodo = oUsuario.ActualizarUsuario(lblIdUsuario.Text, txtNombre.Text, txtApellidoPaterno.Text, txtOtraEmpresa.Text, txtCorreoElectronico.Text, sContraseña, sConfirmarContraseña, imgFoto.ImageUrl, fluImagenPerfil, txtTelefono.Text, "True", Session["IdUsuario"].ToString());
                        ComboBox1.DataBind();
                        CargarDatos();
                        ComboBox1_SelectedIndexChanged(null, null);
                    }
                    else
                    {
                        Response.Write("<script>alert('Introduzca el nombre de tu Empresa')</script>");
                        txtOtraEmpresa.Focus();
                    }
                }
                else
                    oMetodo = oUsuario.ActualizarUsuario(lblIdUsuario.Text, txtNombre.Text, txtApellidoPaterno.Text, ComboBox1.SelectedValue, txtCorreoElectronico.Text, sContraseña, sConfirmarContraseña, imgFoto.ImageUrl, fluImagenPerfil, txtTelefono.Text, "True", Session["IdUsuario"].ToString());

                if (oMetodo.Pass == false)
                {
                    lblValidacion.Text = oMetodo.Message;
                    lblValidacion.Visible = true;
                }
                else
                {
                    CargarDatos();

                    lblValidacion.Text = oMetodo.Message;
                    lblValidacion.Visible = true;
                    Session["bandera"] = 4;
                }

                lblPass.Text = oMetodo.Pass.ToString();
                lblMessage.Text = oMetodo.Message;
                
                OnEstadoCatalogo();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString() + " " + txtCorreoElectronico.Text + " " + txtOtraEmpresa.Text);
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo.Pass = true;
                oMetodo.Message = "Cancelado...";

                lblPass.Text = oMetodo.Pass.ToString();
                lblMessage.Text = oMetodo.Message;

                txtOtraEmpresa.Visible = false;
                ComboBox1.Width = 230;

                OnEstadoCatalogo();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ComboBox1.SelectedValue.Equals("Otro"))
            {
                ComboBox1.Width = 30;
                txtOtraEmpresa.Visible = true;
            }
            else
            {
                txtOtraEmpresa.Visible = false;
                ComboBox1.Width = 230;
            }
        }
        #endregion
    }
}