﻿using System;
using System.Web.UI.WebControls;
using Identidad.App_Code;
using System.Web;
using System.Data;
using System.Web.UI;

namespace Identidad.Controles
{
    public partial class Examinar : System.Web.UI.UserControl
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();
        cBrandSite oBrandSite = new cBrandSite();
        DataTable dtGrupos = new DataTable("Grupos");
        DataColumn column;

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    txtBusqueda.Text = "";
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página. Un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public string ObtenerUrlSeleccionada()
        {
            try
            {
                return lblVarUrl.Text;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());

                return "";
            }
        } // Regresa la url que fue seleccionada

        public string ObtenerIdArchivoSeleccionado()
        {
            try
            {
                return lblVarIdArchivo.Text;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());

                return "";
            }
        } // Regresa el id del archivo que fue seleccionado en string

        public void Show(string sParamIdBrandSite)
        {
            try
            {
                lblVarIdBrandSite.Text = sParamIdBrandSite;

                CargarArchivos();

                ppExaminar.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        } // Abre el popup en base a un id de brandsite

        public void Show(string sParamIdBrandSite, string sNotaAyuda, string sArchivosTrabajo)
        {
            try
            {
                lblVarIdBrandSite.Text = sParamIdBrandSite;

                if (sNotaAyuda != "")
                {
                    lblToolTip.Text = sNotaAyuda;
                    imgToolTip.Visible = true;
                }
                else
                {
                    imgToolTip.Visible = false;
                }

                // Pone el texto en el tooltip
                CargarArchivos(sArchivosTrabajo);
                
                ppExaminar.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        } // Abre el popup en base a un id de brandsite

        public void Show(string sParamIdBrandSite, string sNotaAyuda)
        {
            try
            {
                if (sNotaAyuda != "")
                {
                    lblToolTip.Text = sNotaAyuda;
                    imgToolTip.Visible = true;
                }
                else
                {
                    imgToolTip.Visible = false;
                }

                // Pone el texto en el tooltip

                Show(sParamIdBrandSite);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        } // Abre el popup en base a un id de brandsite, recibiendo como parametro una nota de ayuda para mostrarle un "tip" al usuario.

        public void Show(string sParamIdBrandSite, string sNotaAyuda, bool bSoloImagenes)
        {
            lblVarSoloImagenes.Text = bSoloImagenes.ToString();

            Show(sParamIdBrandSite, sNotaAyuda);
        } // Si deseamos que solo se cargen imagenes para evitar el error de que el usuario selccione otro tipo de archivos

        private void CargarArchivos()
        {
            CargarArchivos("%");

            //if (oSistema.ValidarLenght(txtBusqueda.Text) == false)
            //{
            //    txtBusqueda.Text = "";
            //}

            //oBrandSite.CargarArchivos(gdvArchivos, lblVarIdBrandSite.Text, txtBusqueda.Text, "%", Session["IdUsuario"].ToString(), lblVarSoloImagenes.Text);
        } // carga los archivos del brand site

        public void CargarArchivos(string sArchivoTrabajo)
        {
            if (oSistema.ValidarLenght(txtBusqueda.Text) == false)
            {
                txtBusqueda.Text = "";
            }

            string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

            oBrandSite.CargarArchivos(gdvArchivos, lblVarIdBrandSite.Text, txtBusqueda.Text, sArchivoTrabajo, IdUsuario, lblVarSoloImagenes.Text);
        }

        #endregion

        // Eventos para el manejo de eventos del user control

        #region Eventos creados para el user control

        // Creo un evento para que cuando se seleccione un archivo regrese y se obtenga la url

        public delegate void EventHandler(Object obj, EventArgs e);
        public event EventHandler ArchivoSeleccionado;

        protected void OnArchivoSeleccionado()
        {
            if (ArchivoSeleccionado != null)
            {
                ArchivoSeleccionado(this, new EventArgs());
            }
        }

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Se disparan por medio de un control en el HTML :)
        
        #region Eventos

        protected void imgPreview_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Image Status = (Image)sender;
            int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
            lblVarUrl.Text = ((System.Web.UI.WebControls.Label)((gdvArchivos.Rows[Row].FindControl("lblURL")))).Text;
            lblVarIdArchivo.Text = ((System.Web.UI.WebControls.Label)((gdvArchivos.Rows[Row].FindControl("lblIdArchivo")))).Text;

            lblVarIdBrandSite.Text = "";

            ppExaminar.Hide();

            // Disparo el evento
            OnArchivoSeleccionado();
        }

        protected void btnSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblVarUrl.Text = ((System.Web.UI.WebControls.Label)((gdvArchivos.Rows[Row].FindControl("lblURL")))).Text;
                lblVarIdArchivo.Text = ((System.Web.UI.WebControls.Label)((gdvArchivos.Rows[Row].FindControl("lblIdArchivo")))).Text;

                lblVarIdBrandSite.Text = "";

                ppExaminar.Hide();

                // Disparo el evento
                OnArchivoSeleccionado();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                lblVarIdBrandSite.Text = "";
                lblVarUrl.Text = "";
                lblVarIdArchivo.Text = "";

                ppExaminar.Hide();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                ppExaminar.Show();

                CargarArchivos();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnSubirNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                lblValidacion.Visible = false;
                txtNombre.Text = "";
                txtDescripcion.Text = "";
                gvGruposQueVen.DataSource = null;
                cbArchivoTrabajo.Checked = false;
                lblGrupos.Visible = true;
                gvGruposQueVen.Visible = true;
                ddlGrupos.Visible = true;
                btnAgregar.Visible = true;
                txtNombre.Enabled = true;
                txtNombre.Visible = true;
                ckbDetectarNombre.Checked = false;

                oBrandSite.CargarGruposBS(ddlGrupos, lblVarIdBrandSite.Text);

                ppSubirArchivo.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnSubir_Click(object sender, EventArgs e)
        {
            try
            {

                Response.AppendHeader("X-XSS-Protection", "0");

                ppSubirArchivo.Show();

                if (ckbDetectarNombre.Checked == true)
                {
                    txtNombre.Text = fluArchivo.FileName.Replace(oSistema.ObtenerExtension(fluArchivo.FileName), "").Trim();
                }

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                if (oBrandSite.ValidarEspacio(Session["IdBrandSite"].ToString()))
                    oMetodo = oBrandSite.SubirArchivo("0", txtNombre.Text.Trim(), txtDescripcion.Text.Trim(), fluArchivo, fluPreview, lblVarIdBrandSite.Text, IdUsuario, cbArchivoTrabajo.Checked.ToString());
                else
                {
                    oMetodo.Message = "Ya no tiene espacio en el servidor. Actualiza tu Plan.";
                    oMetodo.Pass = false;
                }

                if (oMetodo.Pass == false)
                {
                    lblValidacion.Text = oMetodo.Message;
                    lblValidacion.Visible = true;

                    ppSubirArchivo.Show();
                }
                else
                {

                    string sIdArchivoNuevo = oMetodo.Message;

                    // Guarda los grupos que se seleccionaron

                    // Si no selecciona ningun grupo, por default le pone "Todos"
                    if (gvGruposQueVen.Rows.Count == 0 && cbArchivoTrabajo.Checked == false)
                    {
                        oMetodo = oBrandSite.EditarGruposPorArchivo("0", sIdArchivoNuevo, "0", "true");
                    }

                    // recorre el grid para guardar los nuevos grupos

                    for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                    {
                        oMetodo = oBrandSite.EditarGruposPorArchivo("0", sIdArchivoNuevo, ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text, "true");
                    }

                    lblValidacion.Visible = false;
                    txtNombre.Text = "";
                    txtDescripcion.Text = "";
                    cbArchivoTrabajo.Checked = false;
                    gvGruposQueVen.DataSource = null;
                    ppSubirArchivo.Hide();

                    CargarArchivos();

                    ppExaminar.Show();
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                if (oMetodo.Pass == true)
                {
                    ppExaminar.Show();
                }
                else
                {
                    ppSubirArchivo.Show();
                }

                Response.AppendHeader("X-XSS-Protection", "0");
            }
        }

        protected void btnCancelarSubida_Click(object sender, EventArgs e)
        {
            try
            {
                lblValidacion.Visible = false;
                txtNombre.Text = "";
                txtDescripcion.Text = "";
                gvGruposQueVen.DataSource = null;
                gvGruposQueVen.DataBind();
                cbArchivoTrabajo.Checked = false;

                ppSubirArchivo.Hide();
                ppExaminar.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #region Eventos para grupos de acceso

        protected void cbArchivoTrabajo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbArchivoTrabajo.Checked == true)
                {
                    lblGrupos.Visible = false;
                    gvGruposQueVen.Visible = false;
                    ddlGrupos.Visible = false;
                    btnAgregar.Visible = false;
                    gvGruposQueVen.DataSource = null;
                    gvGruposQueVen.DataBind();
                }
                else
                {
                    lblGrupos.Visible = true;
                    gvGruposQueVen.Visible = true;
                    ddlGrupos.Visible = true;
                    btnAgregar.Visible = true;
                }

                ppSubirArchivo.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                ppSubirArchivo.Show();

                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                bool bYaExiste = false;

                for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text == ddlGrupos.SelectedValue)
                    {
                        bYaExiste = true;
                        break;
                    }
                }

                if (bYaExiste == false)
                {
                    DataRow Row = dtGrupos.NewRow();

                    Row["IdGrupo"] = ddlGrupos.SelectedValue;
                    Row["NombreGrupo"] = ddlGrupos.SelectedItem;
                    dtGrupos.Rows.Add(Row);

                    // Llena con los grupos que ya se habian solicitado
                    for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }

                    // Ordena alfabeticamente la lista de grupos
                    DataView dv = dtGrupos.DefaultView;
                    dv.Sort = "NombreGrupo desc";
                    DataTable sortedDT = dv.ToTable();

                    gvGruposQueVen.DataSource = sortedDT;
                    gvGruposQueVen.DataBind();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                ppSubirArchivo.Show();
            }
        }

        protected void btnElimigarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                string sIdGrupoABorrar = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[Row].FindControl("lblIdGrupo"))).Text;

                dtGrupos.Clear();

                column = new DataColumn();
                column.DataType = System.Type.GetType("System.String");
                column.ColumnName = "IdGrupo";
                dtGrupos.Columns.Add(column);

                column = new DataColumn();
                column.DataType = Type.GetType("System.String");
                column.ColumnName = "NombreGrupo";
                dtGrupos.Columns.Add(column);

                // Llena con los grupos que ya se habian solicitado
                for (int i = 0; gvGruposQueVen.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text != sIdGrupoABorrar)
                    {
                        DataRow RowGV = dtGrupos.NewRow();
                        RowGV["IdGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblIdGrupo"))).Text;
                        RowGV["NombreGrupo"] = ((System.Web.UI.WebControls.Label)(gvGruposQueVen.Rows[i].FindControl("lblNombreGrupo"))).Text;
                        dtGrupos.Rows.Add(RowGV);
                    }
                }

                // Ordena alfabeticamente la lista de grupos
                DataView dv = dtGrupos.DefaultView;
                dv.Sort = "NombreGrupo desc";
                DataTable sortedDT = dv.ToTable();

                gvGruposQueVen.DataSource = sortedDT;
                gvGruposQueVen.DataBind();

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
            finally
            {
                ppSubirArchivo.Show();
            }
        }

        protected void gvGruposQueVen_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            ppSubirArchivo.Show();
        }

        protected void gvGruposQueVen_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {
            ppSubirArchivo.Show();
        }

        #endregion

        protected void ckbDetectarNombre_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ppSubirArchivo.Show();

                if (ckbDetectarNombre.Checked == true)
                {
                    txtNombre.Enabled = false;
                    txtNombre.Visible = false;
                    txtNombre.Text = "<Nombre del archivo>";
                }
                else
                {
                    txtNombre.Enabled = true;
                    txtNombre.Visible = true;
                    txtNombre.Text = "";
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}