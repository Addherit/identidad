﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Pantones.ascx.cs" Inherits="Identidad.Controles.Pantones" meta:resourcekey="Examinar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<link href="../CSS/Site.css" rel="stylesheet" type="text/css" />
<link href="../CSS/Pages/Pantones.css" rel="stylesheet" type="text/css" />

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <!--Inicia Popup -->

        <panel id="pnlPantones" runat="server" style="display:none; padding-bottom:1px;" >
            
            <div class="divTabla Popup">
                <div class="divTablaTitulo">
                    <br />
                    <asp:LinkButton ID="lkbFavoritos" runat="server" meta:resourcekey="lkbFavoritos" OnClick="lkbFavoritos_Click"></asp:LinkButton>
                    &nbsp;|&nbsp;
                    <asp:LinkButton ID="lkbPantones" runat="server" meta:resourcekey="lkbPantones" OnClick="lkbPantones_Click"></asp:LinkButton>
                    &nbsp;|&nbsp;
                    <asp:LinkButton ID="lkbRGB" runat="server" meta:resourcekey="lkbRGB" OnClick="lkbRGB_Click"></asp:LinkButton>
                    &nbsp;|&nbsp;
                    <asp:LinkButton ID="lkbCMYK" runat="server" meta:resourcekey="lkbCMYK" OnClick="lkbCMYK_Click"></asp:LinkButton>
                    
                    <asp:TextBox ID="txtHexadecimal" runat="server" Text="#000000" style="display:none" ></asp:TextBox>
                    <asp:Label ID="lblHexadecimal" runat="server" style="display:none" Text="000000"></asp:Label>
                </div>
                <hr />
                <!--Favoritos-->
                <asp:Panel ID="pnlSeccionesFavoritos" runat="server" Visible="false">
                    <div class="divControlesExaminarColor">
                        <asp:Button ID="btnQuitarColor" class="buttons colorwhite" runat="server" meta:resourcekey="btnQuitarColor" OnClick="btnQuitarColor_Click" />
                        <asp:Button ID="btnCancelarFavoritos" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                    </div>
                    <div class="gridExaminarColor">
                        <asp:GridView ID="gvFavoritos" CssClass="mGrid" runat="server" AutoGenerateColumns="false" >
                                <Columns>
                                <asp:TemplateField >
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div style="width:270px; height:40px; color:white; text-align:center; background-color:#<%#Eval ("Hex") %>;" >
                                                            <br />
                                                            <asp:Label ID="lblIdColorFavorito" CssClass="alinear" runat="server" DataField ="IdColorFavorito" Text='<%#Eval ("IdColorFavorito") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblHexadecimal" runat="server" DataField ="Hex" Text='<%#Eval ("Hex") %>' Visible="false"></asp:Label>
                                                            <asp:Label ID="lblNombre" runat="server" DataField ="Hex" Text='<%#Eval ("Nombre") %>'></asp:Label>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnEliminarFavorito" CssClass="buttons colorwhite" runat="server" meta:resourcekey="btnEliminarFavorito" OnClick="btnEliminarFavorito_Click"></asp:Button>
                                                        <asp:Button ID="btnSeleccionarColorFavorito" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionarColor" OnClick="btnSeleccionarColorFavorito_Click"></asp:Button>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                </asp:TemplateField>
                                </Columns>
                        </asp:GridView> 
                    </div>
                </asp:Panel>

                <!--RGB-->
                <asp:Panel ID="pnlSeccionesRGB" runat="server" Visible="false">
                    <div class="divControlesExaminarColor">
                        <asp:Button ID="btnQuitarColorRGB" class="buttons colorwhite" runat="server" meta:resourcekey="btnQuitarColor" OnClick="btnQuitarColor_Click" />
                        <asp:Button ID="btnCancelarRGB" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                        <asp:Button ID="btnSeleccionarColorRGB" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionarColor" OnClick="btnSeleccionarColorRGB_Click"></asp:Button>
                    </div>
                    <div class="gridExaminarColor">
                        <br /><br /><br />
                        <div style="float:left; width:20%!important; min-width:20%!important; text-align:right;">
                            <asp:Label ID="lblR" runat="server" Text="R" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtR" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtR_TextChanged" onkeyup="fn_onkeyupRGB();" AutoPostBack="true"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="lblG" runat="server" Text="G" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtG" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtG_TextChanged" onkeyup="fn_onkeyupRGB();" AutoPostBack="true"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="lblB" runat="server" Text="B" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtB" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtB_TextChanged" onkeyup="fn_onkeyupRGB();" AutoPostBack="true"></asp:TextBox>
                            <br /><br />
                        </div>
                        <div style="float:right; width:75%!important; min-width:75%!important; border:none; text-align:right; ">
                            <div id="divColorCreado" runat="server" style="border:none; width:300px; height:115px; background-color:#ffffff">
                            </div>
                            <br />
                            <asp:Button ID="btnCrearColor" runat="server" class="buttons colorwhite" meta:resourcekey="btnCrearColor" OnClick="btnCrearColor_Click" Visible="false" ></asp:Button>
                            <asp:Button ID="btnFavoritoRGB" CssClass="buttons colorwhite" runat="server" meta:resourcekey="btnFavorito" OnClick="btnFavoritoRGB_Click" style="margin-right:148px;"></asp:Button>
                        </div>
                    </div>
                </asp:Panel>
                
                <!--CMYK-->
                <asp:Panel ID="pnlSeccionesCMYK" runat="server" Visible="false">
                    <div class="divControlesExaminarColor">
                        <asp:Button ID="btnQuitarColorCMYK" class="buttons colorwhite" runat="server" meta:resourcekey="btnQuitarColor" OnClick="btnQuitarColor_Click" />
                        <asp:Button ID="btnCancelarCMYK" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                        <asp:Button ID="btnSeleccionarColorCMYK" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionarColor" OnClick="btnSeleccionarColorCMYK_Click"></asp:Button>
                    </div>
                    <div class="gridExaminarColor">
                        <br /><br /><br />
                        <div style="float:left; width:20%!important; min-width:20%!important; text-align:right;">
                            <asp:Label ID="Label1" runat="server" Text="C" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtC" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtC_TextChanged" onkeyup="fn_onkeyupCMYK();"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="Label2" runat="server" Text="M" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtM" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtM_TextChanged" onkeyup="fn_onkeyupCMYK();"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="Label3" runat="server" Text="Y" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtY" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtY_TextChanged" onkeyup="fn_onkeyupCMYK();"></asp:TextBox>
                            <br /><br />
                            <asp:Label ID="Label4" runat="server" Text="K" style="margin-right:10px;"></asp:Label>
                            <asp:TextBox ID="txtK" runat="server" class="TextBox" style="width:30px!important; min-width:30px!important;" Text="0" OnTextChanged="txtK_TextChanged" onkeyup="fn_onkeyupCMYK();"></asp:TextBox>
                            <br /><br />
                        </div>
                        <div style="float:right; width:75%!important; min-width:75%!important; border:none; text-align:right; ">
                            <div id="divColorCreadoCMYK" runat="server" style="border:none; width:300px; height:158px; background-color:#000000;">
                            </div>
                            <br />
                            <asp:Button ID="btnCrearColorCMYK" runat="server" class="buttons colorwhite" meta:resourcekey="btnCrearColor" OnClick="btnCrearColorCMYK_Click" visble="false" style="display:none"></asp:Button>
                            <asp:Button ID="btnFavoritoCMYK" CssClass="buttons colorwhite" runat="server" meta:resourcekey="btnFavorito" OnClick="btnFavoritoCMYK_Click" style="margin-right:148px;"></asp:Button>
                        </div>
                    </div>
                </asp:Panel>
                
                <!--Pantones-->
                <asp:Panel ID="pnlSeccionPantones" runat="server" Visible="false">
                    <div class="divControlesExaminarColor">
                        
                        <asp:TextBox ID="txtBusqueda" CssClass="TextBox" runat="server"></asp:TextBox>&nbsp
                        <asp:Button ID="btnBuscar" class="buttons colorAzulO" runat="server" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click" />
                        <asp:Button ID="btnCancelar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                    </div>
                    <div class="gridExaminarColor">
                            <asp:GridView ID="gvPantones" CssClass="mGrid" runat="server" AutoGenerateColumns="false" >
                                    <Columns>
                                    <asp:TemplateField >
                                            <ItemTemplate>
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div style="width:270px; height:40px; color:white; text-align:center; background-color:#<%#Eval ("Hex") %>;" >
                                                                <br />
                                                                <asp:Label ID="lblNombreColor" CssClass="alinear" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>'></asp:Label>
                                                                <asp:Label ID="lblHexadecimal" runat="server" DataField ="Hex" Text='<%#Eval ("Hex") %>' Visible="false"></asp:Label>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnFavoritoPantones" CssClass="buttons colorwhite" runat="server" meta:resourcekey="btnFavorito" OnClick="btnFavoritoPantones_Click"></asp:Button>
                                                            <asp:Button ID="btnSeleccionarColor" CssClass="buttons colorVerde" runat="server" meta:resourcekey="btnSeleccionarColor" OnClick="btnSeleccionarColor_Click"></asp:Button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                    </asp:TemplateField>
                                    </Columns>
                            </asp:GridView> 
                    </div>
                </asp:Panel>
            </div>
        </panel>

        <asp:Label ID="lblPantones" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppPantones" TargetControlID="lblPantones" runat="server" PopupControlID="pnlPantones" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
            <Animations>
                <OnShown>
                    <FadeIn Duration=".15" Fps="24" />                
                </OnShown>
            </Animations>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup  --->

        <script type="text/javascript">

            function fn_onkeyupRGB() {
                
                document.getElementById('ContentPlaceHolder1_cPantones_txtR').value = document.getElementById('ContentPlaceHolder1_cPantones_txtR').value.replace(/[^0-9]+/g, '');
                document.getElementById('ContentPlaceHolder1_cPantones_txtG').value = document.getElementById('ContentPlaceHolder1_cPantones_txtG').value.replace(/[^0-9]+/g, '');
                document.getElementById('ContentPlaceHolder1_cPantones_txtB').value = document.getElementById('ContentPlaceHolder1_cPantones_txtB').value.replace(/[^0-9]+/g, '');
                
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtR').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtR').value = 255 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtG').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtG').value = 255 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtB').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtB').value = 255 }

                var r = document.getElementById('ContentPlaceHolder1_cPantones_txtR').value;
                var g = document.getElementById('ContentPlaceHolder1_cPantones_txtG').value;
                var b = document.getElementById('ContentPlaceHolder1_cPantones_txtB').value;
                
                document.getElementById('ContentPlaceHolder1_cPantones_divColorCreado').style.backgroundColor = 'rgb(' + r + ', ' + g + ', ' + b + ')';

            };

            function fn_onkeyupCMYK() {
                
                document.getElementById('ContentPlaceHolder1_cPantones_txtC').value = document.getElementById('ContentPlaceHolder1_cPantones_txtC').value.replace(/[^0-9]+/g, '');
                document.getElementById('ContentPlaceHolder1_cPantones_txtM').value = document.getElementById('ContentPlaceHolder1_cPantones_txtM').value.replace(/[^0-9]+/g, '');
                document.getElementById('ContentPlaceHolder1_cPantones_txtY').value = document.getElementById('ContentPlaceHolder1_cPantones_txtY').value.replace(/[^0-9]+/g, '');
                document.getElementById('ContentPlaceHolder1_cPantones_txtK').value = document.getElementById('ContentPlaceHolder1_cPantones_txtK').value.replace(/[^0-9]+/g, '');

                if (document.getElementById('ContentPlaceHolder1_cPantones_txtC').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtC').value = 1 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtM').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtM').value = 1 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtY').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtY').value = 1 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtK').value > 255) { document.getElementById('ContentPlaceHolder1_cPantones_txtK').value = 1 }

                if (document.getElementById('ContentPlaceHolder1_cPantones_txtC').value < 0) { document.getElementById('ContentPlaceHolder1_cPantones_txtC').value = 0 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtM').value < 0) { document.getElementById('ContentPlaceHolder1_cPantones_txtM').value = 0 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtY').value < 0) { document.getElementById('ContentPlaceHolder1_cPantones_txtY').value = 0 }
                if (document.getElementById('ContentPlaceHolder1_cPantones_txtK').value < 0) { document.getElementById('ContentPlaceHolder1_cPantones_txtK').value = 0 }

                var c = document.getElementById('ContentPlaceHolder1_cPantones_txtC').value;
                var m = document.getElementById('ContentPlaceHolder1_cPantones_txtM').value;
                var y = document.getElementById('ContentPlaceHolder1_cPantones_txtY').value;
                var k = document.getElementById('ContentPlaceHolder1_cPantones_txtK').value;

                var r = 255 * (1 - c) * (1 - k);
                var g = 255 * (1 - m) * (1 - k);
                var b = 255 * (1 - y) * (1 - k);

                var Hex = cmykToHex(c, m, y, k);

                document.getElementById('ContentPlaceHolder1_cPantones_divColorCreadoCMYK').style.backgroundColor = Hex;

                $("#ContentPlaceHolder1_cPantones_txtHexadecimal").val(Hex);

            };

            function colorToHex(color, tint) {
                switch (color.space) {
                    case ColorSpace.CMYK:
                        return cmykToHex((color.colorValue[0] * tint) / 100, (color.colorValue[1] * tint) / 100, (color.colorValue[2] * tint) / 100, (color.colorValue[3] * tint) / 100);
                        break;
                    case ColorSpace.RGB:
                        return cp_RgbToHex((color.colorValue[0] * tint) / 100, (color.colorValue[1] * tint) / 100, (color.colorValue[2] * tint) / 100);
                        break;
                    default:
                        return "";
                        break;
                }
            }

            function cmykToHex(c, m, y, k) {
                c = c / 100;
                m = m / 100;
                y = y / 100;
                k = k / 100;
                var R = (1 - (c * (1 - k) + k)) * 255;
                var G = (1 - (m * (1 - k) + k)) * 255;
                var B = (1 - (y * (1 - k) + k)) * 255;
                return rgbToHex(R, G, B);
            }

            function rgbToHex(R, G, B) {
                return "#" + nToHex(R) + nToHex(G) + nToHex(B);
            }

            function nToHex(N) {
                if (N == 0 || isNaN(N)) {
                    return "00";
                }
                N = Math.max(0, N);
                N = Math.min(N, 255);
                N = Math.round(N);
                return "0123456789ABCDEF".charAt((N - N % 16) / 16) + "0123456789ABCDEF".charAt(N % 16);
            }

        </script>

    </ContentTemplate>
</asp:UpdatePanel>