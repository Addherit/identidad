﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Identidad.App_Code;
using System.Drawing;

namespace Identidad.Controles
{
    public partial class Pantones : System.Web.UI.UserControl
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página. Un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.
        
        #region Métodos

        public void Show()
        {
            try
            {
                // "Tab" o panel que se carga primero
                PanelSeccionVisible(pnlSeccionesFavoritos);
                
                ppPantones.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
                ppPantones.Hide();
            }
        } // Muestra el control de pantones en la página solcitada

        public string ObtenerHexadecimalSeleccionado()
        {
            try
            {
                return lblHexadecimal.Text;
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());

                return "";
            }
        } // Obtiene el hexadecimal seleccionado en el control

        private void PanelSeccionVisible(Panel pnlPanelVisible)
        {
            // Hacer visible panel pantones
            if (pnlSeccionPantones == pnlPanelVisible)
            {
                txtBusqueda.Text = "";
                CargarPantones();

                pnlSeccionPantones.Visible = true;
            }
            else
            {
                pnlSeccionPantones.Visible = false;
            }

            // Hacer visible panel Favoritos
            if (pnlSeccionesFavoritos == pnlPanelVisible)
            {
                CargarFavoritos();

                pnlSeccionesFavoritos.Visible = true;
            }
            else
            {
                pnlSeccionesFavoritos.Visible = false;
            }

            // Hacer visible panel RGB
            if (pnlSeccionesRGB == pnlPanelVisible)
            {
                CargarRGB();
                pnlSeccionesRGB.Visible = true;
            }
            else
            {
                pnlSeccionesRGB.Visible = false;
            }

            // Hacer visible panel CMYK
            if (pnlSeccionesCMYK == pnlPanelVisible)
            {
                CargarCMYK();
                pnlSeccionesCMYK.Visible = true;
            }
            else
            {
                pnlSeccionesCMYK.Visible = false;
            }
            
        } // Hace visible o invisible el panel seleccionado y carga su contenido

        private void CrearRGB()
        {
            if (txtR.Text == "") { txtR.Text = "0";}
            if (txtG.Text == "") { txtG.Text = "0";}
            if (txtB.Text == "") { txtB.Text = "0";}

            int iR, iG, iB;

            bool bParsedR = int.TryParse(txtR.Text, out iR);
            bool bParsedG = int.TryParse(txtG.Text, out iG);
            bool bParsedB = int.TryParse(txtB.Text, out iB);

            if (bParsedR == true && bParsedG == true && bParsedB == true)
            {
                if (iR > 255) { txtR.Text = "255"; iR = 255; }
                if (iG > 255) { txtG.Text = "255"; iG = 255; }
                if (iB > 255) { txtB.Text = "255"; iB = 255; }

                Color myColor = Color.FromArgb(iR, iG, iB);

                lblHexadecimal.Text = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");

                divColorCreado.Style.Add("background-color", "rgb(" + txtR.Text + "," + txtG.Text + "," + txtB.Text + ");");

            }
        } // Crea el color RGB segun los valores generados en el textbox

        private void CrearCMYK()
        {
            if (txtC.Text == "") { txtC.Text = "0"; }
            if (txtM.Text == "") { txtM.Text = "0"; }
            if (txtY.Text == "") { txtY.Text = "0"; }
            if (txtK.Text == "") { txtK.Text = "0"; }

            int iC, iM, iY, iK;

            bool bParsedC = int.TryParse(txtC.Text, out iC);
            bool bParsedM = int.TryParse(txtM.Text, out iM);
            bool bParsedY = int.TryParse(txtY.Text, out iY);
            bool bParsedK = int.TryParse(txtK.Text, out iK);

            if (bParsedC == true && bParsedM == true && bParsedY == true && bParsedK == true)
            {
                try
                {
                    string sHex = txtHexadecimal.Text.Replace("#", "").Trim();

                    lblHexadecimal.Text = sHex;

                    divColorCreadoCMYK.Style.Add("background-color", "#" + sHex);
                }
                catch(Exception eError)
                {
                    oSistema.saveLog(eError.ToString());
                }
            }
            else
            {
                lblHexadecimal.Text = "000000";
            }
        } // Crea el color CMYK segun los valores generados en los textbox

        #region Métodos que cargan cada panel

        public void CargarPantones()
        {
            txtBusqueda.Focus();
            btnBuscar.Focus();

            oBrandSite.CargarColores(gvPantones, txtBusqueda.Text.Trim());
        } // Carga el listado de pantones en un gridview

        public void CargarFavoritos() 
        {
            oBrandSite.CargarColoresFavoritos(gvFavoritos, Session["IdBrandSite"].ToString());
        
        } // Carga la lista de favoritos según el brandSite

        public void CargarCMYK()
        {
            txtC.Text = "0";
            txtM.Text = "0";
            txtY.Text = "0";
            txtK.Text = "0";
        }

        public void CargarRGB()
        {
            txtR.Text = "0";
            txtG.Text = "0";
            txtB.Text = "0";
        }

        #endregion

        #endregion

        // Eventos para el manejo de eventos del user control

        #region Eventos creados para el user control

        public delegate void EventHandler(Object obj, EventArgs e);
        public event EventHandler ColorSeleccionado;

        protected void OnColorSeleccionado()
        {
            if (ColorSeleccionado != null)
            {
                ColorSeleccionado(this, new EventArgs());
            }
        }

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Se disparan por medio de un control en el HTML :)

        #region Eventos

        #region Eventos de cambio de panel

        protected void lkbFavoritos_Click(object sender, EventArgs e)
        {
            try
            {
                PanelSeccionVisible(pnlSeccionesFavoritos);
                ppPantones.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbPantones_Click(object sender, EventArgs e)
        {
            try
            {
                PanelSeccionVisible(pnlSeccionPantones);
                ppPantones.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbRGB_Click(object sender, EventArgs e)
        {
            try
            {
                PanelSeccionVisible(pnlSeccionesRGB);
                CrearRGB();
                ppPantones.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbCMYK_Click(object sender, EventArgs e)
        {
            try
            {
                PanelSeccionVisible(pnlSeccionesCMYK);
                CrearCMYK();
                ppPantones.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        #region Pantones

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarPantones();
                ppPantones.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        } // Este evento busca solamente pantones

        protected void btnSeleccionarColor_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblHexadecimal.Text = ((System.Web.UI.WebControls.Label)((gvPantones.Rows[Row].FindControl("lblHexadecimal")))).Text;

                ppPantones.Hide();

                OnColorSeleccionado();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        } // Selecciona el color de los pantones

        protected void btnFavoritoPantones_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblHexadecimal.Text = ((System.Web.UI.WebControls.Label)((gvPantones.Rows[Row].FindControl("lblHexadecimal")))).Text;
                string sNombre = ((System.Web.UI.WebControls.Label)((gvPantones.Rows[Row].FindControl("lblNombreColor")))).Text;

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oBrandSite.InsertarColorFavorito(sNombre, lblHexadecimal.Text, Session["IdBrandSite"].ToString(), IdUsuario);

                PanelSeccionVisible(pnlSeccionesFavoritos);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        #endregion

        #region Favoritos

        protected void btnSeleccionarColorFavorito_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblHexadecimal.Text = ((System.Web.UI.WebControls.Label)((gvFavoritos.Rows[Row].FindControl("lblHexadecimal")))).Text;
                
                ppPantones.Hide();

                OnColorSeleccionado();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        } // Evento cuando se selecciona un color de la lista de favoritos

        protected void btnEliminarFavorito_Click(object sender, EventArgs e)
        {
            try
            {
                Button Status = (Button)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oMetodo = oBrandSite.EliminarColorFavorito(((System.Web.UI.WebControls.Label)((gvFavoritos.Rows[Row].FindControl("lblHexadecimal")))).Text, Session["IdBrandSite"].ToString(), IdUsuario);

                if (oMetodo.Pass == true)
                {
                    CargarFavoritos();
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        } // Este evento quita a un color de los favoritos

        #endregion

        #region RGB

        protected void btnSeleccionarColorRGB_Click(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();

                ppPantones.Hide();

                OnColorSeleccionado();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCrearColor_Click(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtR_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtG_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtB_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void btnFavoritoRGB_Click(object sender, EventArgs e)
        {
            try
            {
                CrearRGB();

                string sNombre = "rgb(" + txtR.Text + ", " + txtG.Text + ", " + txtB.Text + ")";

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oBrandSite.InsertarColorFavorito(sNombre, lblHexadecimal.Text, Session["IdBrandSite"].ToString(), IdUsuario);

                PanelSeccionVisible(pnlSeccionesFavoritos);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        #endregion

        #region CMYK

        protected void btnSeleccionarColorCMYK_Click(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();

                ppPantones.Hide();

                OnColorSeleccionado();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void txtC_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtM_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtY_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void txtK_TextChanged(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void btnCrearColorCMYK_Click(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        protected void btnFavoritoCMYK_Click(object sender, EventArgs e)
        {
            try
            {
                CrearCMYK();

                string sNombre = "cmyk(" + txtC.Text + ", " + txtM.Text + ", " + txtY.Text + ", " + txtK.Text + ")";

                string IdUsuario = oBrandSite.obtenerIdUsuarioFinal(Session["EsAdministrador"].ToString(), Session["IdAdministradorBS"].ToString(), Session["IdUsuario"].ToString());

                oBrandSite.InsertarColorFavorito(sNombre, lblHexadecimal.Text, Session["IdBrandSite"].ToString(), IdUsuario);

                PanelSeccionVisible(pnlSeccionesFavoritos);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
            finally
            {
                ppPantones.Show();
            }
        }

        #endregion

        #region Todos

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                ppPantones.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        } // Este evento cancelar para cualquier panel que se tenga en el control

        protected void btnQuitarColor_Click(object sender, EventArgs e)
        {
            try
            {
                lblHexadecimal.Text = "FFFFFF";

                ppPantones.Hide();

                OnColorSeleccionado();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        #endregion

    }
}