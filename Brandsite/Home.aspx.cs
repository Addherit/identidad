﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad.Brandsite
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    lblBienvenida.Text = oBrandSite.CargarBienvenida(Session["IdBrandSite"].ToString());

                    CargarImagen();
                }

                if (imgHome.ImageUrl == "" || imgHome.ImageUrl == null)
                {

                    imgHome.Visible = false;
                }
                else
                {
                    imgHome.Visible = true;
                }
            
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
           
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarImagen()
        {
            string sUrl = string.Empty;

            try
            {
                if (Session["imgHome"] == null)
                {
                    sUrl = oBrandSite.CargarImagenBienvenida(Session["IdBrandSite"].ToString());
                }
                else
                {
                    sUrl = Session["imgHome"].ToString();
                }

            }
            catch (Exception)
            {
                sUrl = "";
            }

            imgHome.ImageUrl = sUrl;
        } // Este método carga la imagen de bienvenida en la página home

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // Pone en null la session para que carge la imagen
                Session["imgHome"] = null;
               
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}