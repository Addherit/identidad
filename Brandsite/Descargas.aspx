﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Brandsite/BrandSite.Master" AutoEventWireup="true" CodeBehind="Descargas.aspx.cs" Inherits="Identidad.Brandsite.WebForm2" meta:resourcekey="Descargas" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/Descargas.css" rel="stylesheet" type="text/css" />

    <div class="divFondoPagina">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

            <div class="divTituloCentrado">
                    <asp:Label ID="lblDescargas" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblDescargas"></asp:Label>
            </div>
            <%--<hr/>--%>
          
                <table class="Tabla">
                    <tr>
                        <td class="Derecha">      
                            <asp:TextBox ID="txtBusqueda" Style="margin-right:5px; width:239px!important;" CssClass="TextBox BusquedaTextBox" runat="server"></asp:TextBox>
                            <asp:DropDownList ID="ddlBusqueda" runat="server" style="display:none;" ></asp:DropDownList>
                            <asp:Button ID="btnBuscar" Style="margin-right:12px;" class="buttons colorAzulO" runat="server" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click" />
                            <br /><br />

                            <asp:Label ID="lblOrdenadoPor" runat="server" meta:resourcekey="lblOrdenadoPor"></asp:Label>
                            <asp:DropDownList ID="ddlOrdenadoPor" Style="margin-right:112px;" Width="246px" CssClass="DropDownList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlOrdenadoPor_SelectedIndexChanged">
                                <asp:ListItem Value="FechaActivo DESC">Fecha de actualización</asp:ListItem>
                                <asp:ListItem Value="Nombre ASC">Nombre del archivo</asp:ListItem>
                                <asp:ListItem Value="Bytes DESC">Peso del archivo</asp:ListItem>
                            </asp:DropDownList> 
                        </td>
                    </tr>
                </table>

                <br /><br />
                <asp:Label ID="lblIdCarpetaPadreSeleccionada" runat="server" Text="0" Visible="false"></asp:Label>
                <asp:Button ID="btnRegresar" Visible="false" Style="margin-left:50px;" class="buttons colorGris" runat="server" meta:resourcekey="btnRegresar" OnClick="btnRegresar_Click"/>
                &nbsp;&nbsp;&nbsp;<asp:Label ID="lblRuta" runat="server"  ></asp:Label>
                <br /><br />

                <asp:DataList ID="dlArchivos" style="margin-left:23px;" RepeatColumns="3" RepeatDirection="Horizontal" runat="server" OnItemCreated="dlArchivos_ItemCreated">
                    <ItemTemplate>
                        <div class="divMosaico">
                            <table class="tablaMosaico">
                                <tr>
                                    <td >
                                        <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                        <%--<asp:Image ID="imgPreview" class="imagenMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" />--%>
                                        <asp:ImageButton ID="imgPreview" class="imagenMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" OnClick="imgPreview_Click" />
                                    </td>
                                    <td>
                                        <asp:DataList ID="dlTags" RepeatColumns="1" runat="server">
                                            <ItemTemplate>
                                                <div>
                                                    <asp:Label ID="lblIdTxA" runat="server" DataField ="IdTxA" Text='<%#Eval ("IdTxA") %>' Visible="false" ></asp:Label>
                                                    <asp:Image ID="imgTag" class="tagMosaico" DataField ="Preview" ImageUrl='<%#Eval ("Url") %>' runat="server" ToolTip='<%#Eval ("Descripcion") %>' />
                                                </div>
                                            </ItemTemplate>
                                        </asp:DataList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="word-break:break-all;">
                                        <asp:Label ID="lblNombre" CssClass="TituloArchivo" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                        <asp:Label ID="lblExtension" CssClass="infoArchivo" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                        <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url")%>' Visible="false" />
                                        <asp:Label ID="lblBytes" runat="server" Text='<%#Eval("Bytes")%>' Visible="false" />
                                        <br /><br />
                                        <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                        <br /><br />
                                        <asp:Label ID="lblFecha" CssClass="infoArchivo" runat="server" DataField ="FechaActivo" Text='<%#Eval ("FechaActivo") %>' ></asp:Label>
                                        <br />
                                        <asp:Label ID="lblPeso" CssClass="infoArchivo" runat="server" DataField ="PesoBytesArchivo" Text='<%#Eval ("PesoBytesArchivo") %>' ></asp:Label>
                                        <br />
                                        <asp:Label ID="lblRuta" CssClass="infoArchivo" Font-Italic="true" runat="server" Text="" Visible="false" ></asp:Label>
                                        <br />
                                        <asp:LinkButton ID="lkbDescargar" runat="server" meta:resourcekey="lkbDescargar" Visible="true" ></asp:LinkButton>
                                        <%--<asp:HyperLink ID="hplDescargar" runat="server" Target="_blank" meta:resourcekey="hplDescargar" DataField="Url" download='<%#Eval ("Nombre") %>' data-downloadurl='<%#Eval ("Url") %>' NavigateUrl='<%#Eval ("Url") %>' href='<%#Eval ("Url") %>'></asp:HyperLink>--%>
                                    </td>
                                    <td>

                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ItemTemplate>
                </asp:DataList>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <link href="../CSS/autocomplete.css" rel="stylesheet" type="text/css" />
    <script src="../JS/jquery-ui-1.11.2.js" type="text/javascript"></script>

    <script type="text/javascript">

        $(document).ready
        (
            function autocompleta()
            {
                var items = document.getElementById('ContentPlaceHolder1_ddlBusqueda');
                var valor = 0;
                var tamano = items.length;

                var elementos = new Array(tamano);

                for (var indice = 0; indice < tamano; indice++)
                {
                    if (items.options[indice].text != null || items.options[indice].text != "") {
                        elementos[indice] = items.options[indice].text;
                    }
                }

                $("#ContentPlaceHolder1_txtBusqueda").autocomplete
                ({
                    source: elementos
                });

            }
        );

    </script>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblDescargas").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblDescargas").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

    <!-- Bloqueo la tecla enter en toda la página -->

    <script type="text/javascript">
        document.onkeydown = function (evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
    </script>

    <!-- Permito la tecla enter solo cuando key up el txtBuscar -->

    <script type="text/javascript">

        $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
            if (e.keyCode == 13) {

                $get('ContentPlaceHolder1_btnBuscar').click();
            }
        })

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#ContentPlaceHolder1_txtBusqueda').keyup(function (e) {
                        if (e.keyCode == 13) {

                            $get('ContentPlaceHolder1_btnBuscar').click();
                        }
                    })
                }

            });
        }

    </script>

</asp:Content>
