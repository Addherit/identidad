﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Brandsite/BrandSite.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Identidad.Brandsite.WebForm1" meta:resourcekey="Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/HomeBrandSite.css" rel="stylesheet" type="text/css" />

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divFondo" class="divFondoPagina" style=" background-color:#444!important; overflow:hidden;">
                
                <asp:Image ID="imgHome" runat="server" class="HomeBrandSite" />

                <div id="divBienvenida" class="Centro">
                    <asp:Label ID="lblBienvenida" CssClass="Bienvenida" runat="server"></asp:Label>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

        <script src="../JS/jquery-1.8.3.js"></script> 

    <script type="text/javascript">
        
        $(function () {
            
            var bienvenida = $('#ContentPlaceHolder1_lblBienvenida').text(); 

            if (bienvenida == '')
            {
                $("#divBienvenida").remove();
            }
            else
            {
                $("#divFondo").css("background-color", "white");
                $("#divFondo").css("background-color", "#FFFFFF!Important");
            }
        });

    </script>

</asp:Content>
