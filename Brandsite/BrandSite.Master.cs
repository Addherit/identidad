﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using Identidad.App_Code;
using System.Collections;
using System.Data;
using System.Threading;
using System.Globalization;

namespace Identidad.Brandsite
{
    public partial class BrandSite : System.Web.UI.MasterPage
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cUsuario oUsuario = new cUsuario();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
               

            try
            {
                if (Request.Form["__EVENTTARGET"] == "regresar")
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
                else if (Request.Form["__EVENTTARGET"] == "pago")
                    Response.Redirect("~/site/pagina-datos-pago.aspx", false);

                Session["bandera"] = "0";

                //revision de que sea el usuario de diseño para que pueda entrar a todos los manuales sin importar su estado.
                int vencido;
                if (int.Parse(Session["IdUsuario"].ToString()) == 671)
                { vencido = 0; }
                else { vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString())); }

                if (vencido == 1)
                {
                    if (oUsuario.esAdministador(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString()))
                    {
                        Session["bandera"] = "2";
                        //MPE.Show();
                    }
                    else
                    {
                        Session["bandera"] = "1";
                        //popup
                    }
                }
                if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/site/login-registro.aspx", false);

                    return;
                }
                

                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        fillMisSitios();

                        oMetodo = oBrandSite.TieneAccesoBS(oUsuario.getCorreoElectronico(Session["IdUsuario"].ToString()), Session["IdBrandSite"].ToString());

                        if (oMetodo.Pass == true)
                        {
                            OpcionesDiseñador();
                            CargarNombreUsuario();
                            CargarParaCompartir();

                            Session["IdAdministradorBS"] = oUsuario.IdUsuarioAdministradorSitio(Session["IdBrandSite"].ToString());

                            if (Session["IdUsuario"].ToString() == Session["IdAdministradorBS"].ToString())
                            {
                                Session["EsAdministrador"] = "1";
                                Session["PuedeEditiar"] =
                                    true;
                                lkbEditarSitio.Visible = true;
                            }
                            else
                            {
                                Session["EsAdministrador"] = "0";
                                Session["PuedeEditiar"] = oUsuario.EsDeMiEquipo(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
                                if (Session["PuedeEditiar"].ToString() == "True")
                                {
                                    lkbEditarSitio.Visible = true;
                                }
                                else
                                {
                                    lkbEditarSitio.Visible = false;
                                }                                
                            }

                        }
                        else
                        {
                            Session["IdUsuario"] = null;
                          
                            string sUrlSegura = "~/Brandsite/Login.aspx?Id=" + oSistema.EncriptarId(int.Parse(Session["IdBrandSite"].ToString()));

                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect(sUrlSegura, false);
                        }
                    }

                    Response.CacheControl = "private";
                    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }


        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarParaCompartir()
        {
            try
            {
                if (bool.Parse(Session["PuedeInvitar"].ToString()) == true)
                {
                    lkbInvitarPersona.Visible = true;
                }
                else
                {
                    lkbInvitarPersona.Visible = false;
                }
            }
            catch (Exception)
            {
            }
        }

        public void CargarNombreUsuario()
        {
            try
            {
                imgUsuario.ImageUrl = Session["FotoUsuario"].ToString();

                lkbActualizarMiPerfil.Text = Session["NombreUsuario"].ToString();
            }
            catch (Exception)
            {
            }
        } // Carga la foto y el nombre de lusuario en la img superior

        private void OpcionesDiseñador()
        {
            try
            {
                if (Session["EsDiseñadorSitio"].ToString() == "1")
                {
                    lkbEditarSitio.Visible = true;
                    lkbSitioDiseñador.Visible = true;
                }
                else
                {
                    lkbEditarSitio.Visible = false;
                    lkbSitioDiseñador.Visible = false;
                }
            }
            catch (Exception)
            {
                lkbEditarSitio.Visible = false;
                lkbSitioDiseñador.Visible = false;
            }
        } // Si es diseñador del sitio activa sus opciones de edición

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos


        protected void fillMisSitios()
        {
            oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
            if (ddlmissitios.Items.Count > 1)
            {
                //ppIniciarSesion.Hide();

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                oUsuario.getMisSitios(ddlmissitios, Session["IdUsuario"].ToString());
            }
            else
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlmissitios.Items[0].Value.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);

                }
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlmissitios.SelectedValue.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        Session["idBrandSite"] = sUrl[1];
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbEditarSitio_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Designer/Home.aspx", false);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbSitioDiseñador_Click(object sender, EventArgs e)
        {
            try
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IdUsuario"] = null;
                Session["FotoUsuario"] = null;
                Session["NombreUsuario"] = null;
                Session["EsDiseñadorSitio"] = null;
                Session["IdAdministradorBS"] = null;
                Session["EsAdministrador"] = null;
                Session["PuedeEditiar"] = null;

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                if (Session["IdBrandSite"] != null)
                {
                    Response.Redirect("~/BrandSite/Login.aspx?id=" + oSistema.EncriptarId(int.Parse(Session["IdBrandSite"].ToString())), false);
                }
                else
                {
                    Response.Redirect("~/site/login-registro.aspx", false);
                }
                Response.Cookies["Cookie"].Expires = DateTime.Now.AddDays(-1d);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

       protected void lkbActualizarMiPerfil_Click(object sender, EventArgs e)
        {
          /*  try
            {
                MiPerfil.setIdUsuario(Session["IdUsuario"].ToString(), true);

                ppActualizarMiPerfil.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }*/
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);

                if (Session["IdBrandSite"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);

                    return;
                }

                // Agrega la referencia a la hoja de estilo dinámica
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/BrandSites/Id" + Session["IdBrandSite"].ToString() + ".css\" />"));

                
              //  MiPerfil.EstadoCatalogo += new Identidad.Controles.MiPerfil.EventHandler(MiPerfil_OnEstadoCatalogo);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en MiPerfil
        private void MiPerfil_OnEstadoCatalogo(object sender, EventArgs e)
        {
            /*try
            {
                oMetodo = MiPerfil.ObtenerEstado();

                if (oMetodo.Pass == false)
                {
                    ppActualizarMiPerfil.Show();
                }
                else
                {
                    Session["NombreUsuario"] = oUsuario.getNombreUsuario(Session["IdUsuario"].ToString());
                    Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());

                    CargarNombreUsuario();

                    ppActualizarMiPerfil.Hide();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }*/
        }

        protected void lkbContacto_Click(object sender, EventArgs e)
        {
            try
            {
                txtCorreoElectronico.Text = "";
                lblValidacionCorreo.Text = "";
                lblValidacionCorreo.Visible = false;

                foreach (DataRow row in oBrandSite.CargarContacto(Session["IdBrandSite"].ToString()).Rows)
                {
                    imgFoto.ImageUrl = row["Foto"].ToString();
                    lblNombre.Text = row["Nombre"].ToString();
                    lblCorreoElectronico.Text = row["CorreoElectronico"].ToString();
                    lblTelefono.Text = row["Telefono"].ToString();
                }

                ppContacto.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                ppContacto.Show();

                oMetodo = oBrandSite.EnviarCorreoContacto(Session["IdUsuario"].ToString(), txtCorreoElectronico.Text, Session["IdBrandSite"].ToString());

                if (oMetodo.Pass == true)
                {
                    txtCorreoElectronico.Text = "";
                }

                lblValidacionCorreo.Visible = true;
                lblValidacionCorreo.Text = oMetodo.Message;

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarContacto_Click(object sender, EventArgs e)
        {
            try
            {
                ppContacto.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbInvitarPersona_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombreInvita.Text = "";
                txtApellidoPaternoInvita.Text = "";
                txtApellidoMaternoInvita.Text = "";
                txtCorreoInvita.Text = "";
                ckbPuedeCompartir.Checked = false;
                lblValidacionCompartir.Visible = false;
                btnCancelarInvitacion.Text = "Cancelar";

                ppInvitarUsuario.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarInvitacion_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombreInvita.Text = "";
                txtApellidoPaternoInvita.Text = "";
                txtApellidoMaternoInvita.Text = "";
                txtCorreoElectronico.Text = "";
                ckbPuedeCompartir.Checked = false;
                lblValidacionCompartir.Visible = false;
                btnCancelarInvitacion.Text = "Cancelar";

                ppInvitarUsuario.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnInvitar_Click(object sender, EventArgs e)
        {
            try
            {
                if (oUsuario.ValidarCantUsuarios(Session["IdBrandSite"].ToString()))
                {
                    string sContraseña = string.Empty;

                    sContraseña = oUsuario.CrearContraseña(txtNombreInvita.Text, txtCorreoInvita.Text);

                    oMetodo = oUsuario.InvitarUsuario(txtNombreInvita.Text, txtApellidoPaternoInvita.Text, txtApellidoMaternoInvita.Text, txtCorreoInvita.Text, sContraseña, Session["IdBrandSite"].ToString(), Session["IdUsuario"].ToString(), ckbPuedeCompartir.Checked);

                    if (oMetodo.Pass == false)
                    {


                        lblValidacionCompartir.Text = oMetodo.Message;
                        lblValidacionCompartir.Visible = true;
                        ppInvitarUsuario.Show();
                    }
                    else
                    {
                        txtNombreInvita.Text = "";
                        txtApellidoPaternoInvita.Text = "";
                        txtApellidoMaternoInvita.Text = "";
                        txtCorreoInvita.Text = "";
                        ckbPuedeCompartir.Checked = false;

                        lblValidacionCompartir.Text = oMetodo.Message;
                        lblValidacionCompartir.Visible = true;
                        ppInvitarUsuario.Show();
                        btnCancelarInvitacion.Text = "Cerrar";
                    }

                }
                else
                {
                    txtNombreInvita.Text = "";
                    txtApellidoPaternoInvita.Text = "";
                    txtApellidoMaternoInvita.Text = "";
                    txtCorreoInvita.Text = "";
                    ckbPuedeCompartir.Checked = false;

                    lblValidacionCompartir.Text = "Los usuarios del Brand Site han llegado al tope. Para tener más usuarios favor de actualizar tu plan.";
                    lblValidacionCompartir.Visible = true;
                    ppInvitarUsuario.Show();
                    btnCancelarInvitacion.Text = "Cerrar";
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                HttpCookie cookie = new HttpCookie("idioma");

                //cookie.Value = DropDownList1.UniqueID;
                Response.SetCookie(cookie);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ImgIngles_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "alerta();", true);//ejecuta reload javascript para cambio de idioma

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                HttpCookie cookie = new HttpCookie("idioma");

                cookie.Value = "en-US";
                Page.Culture = "en-US";
                Page.UICulture = "en";
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en");
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Session["culture"] = "en-US";

                Response.SetCookie(cookie);

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                
                // Las siguientes líneas mandan llamar una vez más a este evento para que el cambio de idioma funcione ya que depende de un evento del global asax on begin request
                
                try
                {
                    if (int.Parse(Session["Vez"].ToString()) == 1)
                    {
                        Session["Vez"] = 2;

                        ImgIngles_Click(sender, e);
                    }
                    else
                    {
                        Session["Vez"] = 1;
                    }
                }
                catch(Exception)
                {
                    Session["Vez"] = 2;

                    ImgIngles_Click(sender, e);
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ImgEspañol_Click(object sender, ImageClickEventArgs e)
        {
            try
            
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "alerta();", true);//ejecuta reload javascript para cambio de idioma

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                HttpCookie cookie = new HttpCookie("idioma");

                cookie.Value = "es-MX";
                Page.Culture = "es-MX";
                Page.UICulture = "es";
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es");
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-MX");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("es-MX");
                Session["culture"] = "es-MX";

                Response.SetCookie(cookie);

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                // Las siguientes líneas mandan llamar una vez más a este evento para que el cambio de idioma funcione ya que depende de un evento del global asax on begin request

                try
                {
                    if (int.Parse(Session["VezEsp"].ToString()) == 1)
                    {
                        Session["VezEsp"] = 2;

                        ImgEspañol_Click(sender, e);
                    }
                    else
                    {
                        Session["VezEsp"] = 1;
                    }
                }
                catch (Exception)
                {
                    Session["VezEsp"] = 2;

                    ImgEspañol_Click(sender, e);
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbIngles_Click(object sender, EventArgs e)
        {

            try
            {
            
            string id = Session["IdBrandSite"].ToString();
            switch (id)
            {
                case "430":
                       string eng = "MhGO1MZq7ig="; //ingles 434
                       HttpContext.Current.ApplicationInstance.CompleteRequest();
                       Response.Redirect("~/Brandsite/Login.aspx?Id=" + eng, false);
                    break;
                case "434":
                     string esp = "7lh7YpjQMXc="; //español 430
                     HttpContext.Current.ApplicationInstance.CompleteRequest();
                     Response.Redirect("~/Brandsite/Login.aspx?Id=" + esp, false);
                    break;
            }
                
              
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "alerta();", true);//ejecuta reload javascript para cambio de idioma

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                HttpCookie cookie = new HttpCookie("idioma");

                cookie.Value = "en-US";
                Page.Culture = "en-US";
                Page.UICulture = "en";
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en");
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                Session["culture"] = "en-US";

                Response.SetCookie(cookie);

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                // Las siguientes líneas mandan llamar una vez más a este evento para que el cambio de idioma funcione ya que depende de un evento del global asax on begin request

                try
                {
                    if (int.Parse(Session["Vez"].ToString()) == 1)
                    {
                        Session["Vez"] = 2;

                        lkbIngles_Click(sender, e);
                    }
                    else
                    {
                        Session["Vez"] = 1;
                    }
                }
                catch (Exception)
                {
                    Session["Vez"] = 2;

                    lkbIngles_Click(sender, e);
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbEstañol_Click(object sender, EventArgs e)
        {
          
        
  
            try
            {

                    string id = Session["IdBrandSite"].ToString();
            switch (id)
            {
                case "430":
                    string eng = "MhGO1MZq7ig="; //ingles 434
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Brandsite/Login.aspx?Id=" + eng, false);
               
                    break;
                case "434":
                    string esp = "7lh7YpjQMXc="; //español 430
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Brandsite/Login.aspx?Id=" + esp, false);
                    break;
            }

                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "myScript", "alerta();", true);//ejecuta reload javascript para cambio de idioma

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                HttpCookie cookie = new HttpCookie("idioma");

                cookie.Value = "es-MX";
                Page.Culture = "es-MX";
                Page.UICulture = "es";
                Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("es");
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-MX");
                Thread.CurrentThread.CurrentUICulture = new CultureInfo("es");
                Thread.CurrentThread.CurrentCulture = new CultureInfo("es-MX");
                Session["culture"] = "es-MX";

                Response.SetCookie(cookie);

                HttpContext.Current.ApplicationInstance.CompleteRequest();

                // Las siguientes líneas mandan llamar una vez más a este evento para que el cambio de idioma funcione ya que depende de un evento del global asax on begin request

                try
                {
                    if (int.Parse(Session["VezEsp"].ToString()) == 1)
                    {
                        Session["VezEsp"] = 2;

                        lkbEstañol_Click(sender, e);
                    }
                    else
                    {
                        Session["VezEsp"] = 1;
                    }
                }
                catch (Exception)
                {
                    Session["VezEsp"] = 2;

                    lkbEstañol_Click(sender, e);
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbzip_Click(object sender, EventArgs e)
        {
            try
            {

                string id = Session["IdBrandSite"].ToString();
                switch (id)
                {
                    case "331":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/130_años_Casa_Brugal.zip", false);

                        break;
                    case "253":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/a!_Diseño.zip", false);
                        break;
                    case "79":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Addenda_Capital.zip", false);
                        break;
                    case "9":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/AFP_Siembra.zip", false);
                        break;
                    case "61":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Aldea_Creativa.zip", false);
                        break;
                    case "304":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/APAP.zip", false);
                        break;
                    case "405":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Aximo.zip", false);
                        break;
                    case "295":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Axity.zip", false);
                        break;
                    case "16":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/BHD_León.zip", false);
                        break;
                    case "64"://64
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Bienvenidos.zip", false);
                        break;
                    case "306":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Casa_Brugal.zip", false);
                        break;
                    case "46":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Cementos_Fortaleza.zip", false);
                        break;
                    case "422":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Colegio_de_Notarios_de_la_Ciudad_de_México.zip", false);
                        break;
                    case "347":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Compartamos.zip", false);
                        break;
                    case "3":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Comunalia.zip", false);
                        break;
                    case "69":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Diseñamexico.zip", false);
                        break;
                    case "391":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/El_Filón.zip", false);
                        break;
                    case "86":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Eqord_Airways.zip", false);
                        break;
                    case "80":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Escato.zip", false);
                        break;
                    case "263":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Farmalivio.zip", false);
                        break;
                    case "274"://274
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Ferman.zip", false);
                        break;
                    case "321":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/FREL.zip", false);
                        break;
                    case "409":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Galicia.zip", false);
                        break;
                    case "41":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/GAMMA.zip", false);
                        break;
                    case "42":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/H+.zip", false);
                        break;
                    case "8":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/identidad.zip", false);
                        break;
                    case "68":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/ideograma.zip", false);
                        break;
                    case "309":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Impuestos_Internos.zip", false);
                        break;
                    case "72":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Inicia_Educación.zip", false);
                        break;
                    case "44":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Jefo.zip", false);
                        break;
                    case "227":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Juicy_Boom.zip", false);
                        break;
                    case "429"://429
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Kipling.zip", false);
                        break;
                    case "420":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/LASH_BB.zip", false);
                        break;
                    case "352":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Merited.zip", false);
                        break;
                    case "430":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Monex.zip", false);
                        break;
                    case "434":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Monex_Global.zip", false);
                        break;
                    case "78":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Petite_Studio.zip", false);
                        break;
                    case "421":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Questum.zip", false);
                        break;
                    case "433":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/San_Luis_Potosi.zip", false);
                        break;
                    case "65":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Talentry.zip", false);
                        break;
                    case "71":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/techBA.zip", false);
                        break;
                    case "267":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Traxión.zip", false);
                        break;
                    case "432":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Un_kilo_de_Ayuda.zip", false);
                        break;
                    case "40":
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/Brandsite/zips/Volaris.zip", false);
                        break;
                    
                }
                //HttpContext.Current.ApplicationInstance.CompleteRequest();
                //Response.Redirect("~/Brandsite/Home.aspx", false);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
        #endregion

    }
}