﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Brandsite/BrandSite.Master" AutoEventWireup="true" CodeBehind="Manual.aspx.cs" Inherits="Identidad.Brandsite.WebForm4" meta:resourcekey="Manual" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   
    <script type="text/javascript" src="../JS/jquery_1.4.min.js"></script>
    <script type="text/javascript" src="../JS/jsManualBrandSite.js"></script>

    <!--Google Fonts-->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Gloria+Hallelujah' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Titillium+Web' rel='stylesheet' type='text/css' /> <%--font-family: 'Titillium Web', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css' /> <%--'Roboto Slab', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css' /> <%--'Roboto Condensed', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Serif' rel='stylesheet' type='text/css' /> <%--'Noto Serif', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans' rel='stylesheet' type='text/css' /> <%--'Noto Sans', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Asap' rel='stylesheet' type='text/css' /> <%--'Asap', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Signika' rel='stylesheet' type='text/css' /> <%--'Signika', sans-serif--%>
    <link href='https://fonts.googleapis.com/css?family=Gentium+Basic' rel='stylesheet' type='text/css' /> <%--'Gentium Basic', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Alegreya' rel='stylesheet' type='text/css' /> <%--'Alegreya', serif--%>
    <link href='https://fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css' /> <%--'Lobster', regular--%>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Merriweather+Sans:400,300,300italic,400italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=PT+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Bitter:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,200,300,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Muli:400,300,300italic,400italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abel' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Play:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Vollkorn:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Alegreya+Sans+SC:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sigmar+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amatic+SC:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Josefin+Slab:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Chewy' rel='stylesheet' type='text/css'/>    
    <link href='https://fonts.googleapis.com/css?family=Quattrocento+Sans:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Quattrocento:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=EB+Garamond' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Lobster+Two:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Cinzel:400,700,900' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Fredoka+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Playball' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Amaranth:400,400italic,700,700italic' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Julius+Sans+One' rel='stylesheet' type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Sacramento' rel='stylesheet' type='text/css'/>
  
    <!--Google Fonts-->
    <link href="../CSS/Fonts.css" rel="Stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div id="divPrincipal">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
          
            <br />
            <div id="divAviso" style="margin-left:700px">
                <asp:Label ID="lblAvisoManual" runat="server" meta:resourcekey="lblAvisoManual" ></asp:Label>
            </div>

            <asp:Button ID="btnImage1" runat="server" Text="Button" Style="display:none;" OnClick="btnImage1_Click" />
            <asp:Button ID="btnImage2" runat="server" Text="Button" Style="display:none;" OnClick="btnImage2_Click" />
            <asp:TextBox ID="txtIdIndiceSeleccionado" runat="server" Text="0" style="display:none;"></asp:TextBox>

            <asp:Label ID="lblIdUsuario" runat="server" Text="Label" visible="false"></asp:Label>
            <asp:Label ID="lblIdBrandiste" runat="server" Text="Label" Visible="false"></asp:Label>

            <div id="divMenuIndice" style="float:left; position:absolute; width: 300px !important; margin-left:1px; " class="divMenu">
            <div id="divOcultatree" style="width:90px; margin:10px!important; ">
                <asp:Label ID="lblMostrarIndice" runat="server" meta:resourcekey="lblMostrarIndice" ForeColor="#717465" Font-Names="'open sans regular', sans-serif"></asp:Label>
            </div>
                
            <div id="divTreeView" style="display:none; ">
                   
                <input type="text" id="txtListaJSON" runat="server" style="display:none;  "/>  
                    <div id="divTree" runat="server" >
                    <asp:TreeView ID="tvwIndice" runat="server"  ForeColor="#717465" style="margin-left:5px;"  CollapseImageUrl="~/Images/Manual/imenos.gif" ExpandImageUrl="~/Images/Manual/Imas.gif" NodeWrap="True">
                            <NodeStyle HorizontalPadding="5px" />
                            <ParentNodeStyle  CssClass="TreeView" Font-Bold="False" ForeColor="#666666" HorizontalPadding="3px" NodeSpacing="2px" />
                            <SelectedNodeStyle Font-Bold="True" ForeColor="Black" />
                        </asp:TreeView>
                        <%--<asp:TreeView ID="tvwIndice" runat="server" OnSelectedNodeChanged="tvwIndice_SelectedNodeChanged1" ForeColor="#717465"  CollapseImageUrl="~/Images/Manual/imenos.gif" ExpandImageUrl="~/Images/Manual/Imas.gif" NodeWrap="True">
                            <NodeStyle HorizontalPadding="5px" />
                            <ParentNodeStyle  CssClass="TreeView" Font-Bold="False" ForeColor="#666666" HorizontalPadding="3px" NodeSpacing="2px" />
                            <SelectedNodeStyle Font-Bold="True" ForeColor="Black" />
                        </asp:TreeView>--%>
                </div>
            </div> 

             </div>
              <div runat="server"  id="divFondo" class="divFondo importantRule" style="float:right !important; ">
                    <asp:Label ID="lblIdIndiceSeleccionado" runat="server" ></asp:Label>
                    <asp:Label ID="lblIdTemplate" runat="server"></asp:Label>
            
                     <%--  <div id="divOpciones" class="divOpciones">
                           

                            <div id="div1" class="divTitulos">
                              
                           
                                      <span>Manual de Identidad</span>
                              
                            </div>
                       <%-- <asp:Panel ID="pnlMenuOpciones" runat="server" >
                          <asp:Button ID="btnEliminarSeccion" runat="server" meta:resourcekey="btnEliminarSeccion" OnClick="btnEliminarSeccion_Click" CssClass="buttons colorwhite alinear" />
                            <asp:Button ID="btnEditar" runat="server" meta:resourcekey="btnEditar" OnClick="btnEditar_Click" CssClass="buttons colorAzulO alinear"/>
                 </asp:Panel>
                   </div>--%>

                  <div id="divContenido" style="margin-top:25px !important;">
                      <div id="divEdicion" class="divEdicion" style="margin-top:0 !important">
                           <%--<div id="divTitulos" class="divTitulos">
                                <asp:Label ID="lblNombreMenu" runat="server"></asp:Label>
                            </div>--%>

                          <%-- Panel de edicion del richTexbox --%>
                         
                    </div>
 <%-- 
                    <div runat="server" id="divOpcionesEditor" class="divOpcionesEditor">
                      <asp:Button  ID ="btnEnviaDatos" runat="server" OnClick="lnkCambiarFondo_Click" OnClientClick="EnviaDatos()" Text=" " style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button background_image" ToolTip="Cambiar color de fondo" />
                      
                        <div runat="server" id="divCambioImagen" class="divCambioImagen">
                          <%--  <asp:Button ID="btnImage1" runat="server" Text=" " OnClick="btnImage1_Click" OnClientClick="editarImagen(this);" style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button cimagen_image" ToolTip="Cambiar 1era imagen"   />  
                            <asp :Button ID="btnImage2" runat="server" Text=" " OnClick="btnImage1_Click" OnClientClick="editarImagen(this);" style='width: 23px; height: 21px;' CssClass="ajax__html_editor_extender_button cimagen_image" ToolTip="Cambiar 2nda imagen "  />  
                       </div>  
                    </div>      
           --%> 
              <div id="divEditorTexto" class="divEditorTexto" style="margin-top:15px;">

                <%-- Dialog para agregar tooltip --%>

                 <div id="divHTML" class="divHTML">
                      <div  id="divPlantilla"  runat="server" >


                           <div class="divFondoManual">
                <asp:DataList ID="dtHTML" runat="server"  style="margin:auto" >
                    <ItemTemplate>
                                             
                         <%--   <a name="<%#Eval ("Nombre").ToString().Replace(" ","").Replace('á','a').Replace('é','e').Replace('í','i').Replace('ú','u').Replace('ó','o').Replace('ñ', 'n').Replace('Á', 'A').Replace('É', 'E').Replace('Í', 'I')
                                 .Replace('Ú', 'U').Replace('Ó', 'O').Replace('Ñ', 'N')+"ANCHOR".ToUpper() %>"></a><br/>                       
                           --%>

                           <a name="<%#Eval ("idIndice").ToString() %>"></a><br/>                       

                            <asp:Label ID="lblIdIndiceSeleccionado" runat="server" Text='<%#Eval("idIndice")%>' Visible="false" />
                       
                            <div class="trPadre<%#Eval("idIndicePadre").ToString()%> Id1" style="margin-left:10px">
                                <%#Eval ("Nombre").ToString() %>   
                            </div>
                            <br/>

                               <div id="<%#Eval ("idIndice").ToString() %>" style="clear:both">
                                   <%#Eval("HTMLPlantilla").ToString()%>
                               </div>
                               
                             <br/>
                        
                    </ItemTemplate>

                </asp:DataList>
                            </div></div> 
                </div>
            </div> <!--Fin div Editor -->
            <br /><br />
            <asp:Label ID="lblHTML" runat="server"></asp:Label>
              </span></span>
                <div style="clear:both"></div>
                </div>
            </div>     
              <div style="clear:both"></div>

            <div style="display:none">

                <asp:GridView ID="gvwGlosario" runat="server"></asp:GridView>


            </div>

                    <!--Inicia Popup para ver los archivos relacionados--->
        <panel id="pnlArchivosRelacionados" runat="server" style="display:none;" >
            <div class="divTabla"  style="width:660px; height:500px;">
                 <div class="divTablaTitulo">
                      <asp:Label ID="lblTituloArchivosrelacionados" runat="server" meta:resourcekey="lblTituloArchivosrelacionados"></asp:Label>        
                 </div>
                 <hr/>
               <div class="divTBODY divTamano" >
                    <div style="overflow-y:auto; height:360px; margin-bottom:30px;">

                        <asp:DataList ID="dlArchivosRelacionados" RepeatColumns="4" RepeatDirection="Horizontal" runat="server" OnItemCreated="dlArchivosRelacionados_ItemCreated">
                        <ItemTemplate>
                        
                          <div id='DivArchivo' class="divMosaico" >
                                <table class="tablaMosaico">
                          
                                    <tr>
                                        <td >
                                            <asp:Label ID="lblIdArchivoRelacionado" runat="server" DataField ="IdArchivoRelacionado" Text='<%#Eval ("IdArchivoRelacionado") %>' Visible="false" ></asp:Label>
                                            <asp:Label ID="lblIdArchivo" runat="server" DataField ="IdArchivo" Text='<%#Eval ("IdArchivo") %>' Visible="false" ></asp:Label>
                                            <asp:ImageButton ID="imgPreview" class="imagenMosaico" style="background-color:white!important;" DataField ="Preview" ImageUrl='<%#Eval ("Preview") %>' runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                         
                                            <asp:Label ID="lblNombre" CssClass="TituloArchivo" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' Font-Bold="true" ></asp:Label>
                                            <asp:Label ID="lblExtension" CssClass="infoArchivo" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblPeso" CssClass="infoArchivo" runat="server" DataField ="PesoBytesArchivo" Text='<%#Eval ("PesoBytesArchivo") %>' ></asp:Label>
                                            <br />
                                            <asp:Label ID="lblUrl" runat="server" Text='<%#Eval("Url")%>' Visible="false" />
                                            <asp:Label ID="lblBytes" runat="server" Text='<%#Eval("Bytes")%>' Visible="false" />
                                            <asp:LinkButton ID="lkbDescargar" runat="server" meta:resourcekey="lkbDescargar" Visible="true"  ></asp:LinkButton>
                                            <br /><br />
                                        </td>
                                    </tr>

                                    </table>
                            </div>
                            <br />
                            <br />
                            <br />
                            <br />


                        </ItemTemplate>
                    </asp:DataList>
                                

                    </div>
                    
                    <asp:Button ID="btnSalirArchivosRelacionados" runat="server" meta:resourcekey="btnSalirArchivosRelacionados" CssClass="buttons colorwhite" OnClick="btnSalirArchivosRelacionados_Click" />

               </div>

            </div>
        </panel>

        <asp:Label ID="label6" runat="server" Text=""></asp:Label>
        <Ajax:ModalPopupExtender ID="ppArchivosRelacionados" TargetControlID="label6" runat="server" PopupControlID="pnlArchivosRelacionados" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
           <%-- <Animations>
                <OnShown>
                    <FadeIn Duration=".30" Fps="48" />                
                </OnShown>
            </Animations>--%>
        </Ajax:ModalPopupExtender>
        <!--Termina Popup para ver los archivos relacionados--->

        </ContentTemplate>
    </asp:UpdatePanel>
 </div> 
         <link href="../CSS/Pages/Manual.css" rel="stylesheet" />
         <link href="../CSS/Pages/Manual/MenuTree.css" rel="stylesheet" />
         <link href="../CSS/Pages/Manual/tooltipBS.css" rel="stylesheet" />


    <%-- <link href="../CSS/Pages/ManualBrandSite.css" rel="stylesheet" />--%>
     <!--Referencias al JS-->
        
<%--     <link href="../JS/themes/tooltip.css" rel="stylesheet" />
         <script src="../JS/themes/tooltip.js"></script>--%>
         <script type="text/javascript" src="../JS/jquery-1.8.3.js"></script> 
<%--         <script src="../JS/EditorTexto/ckeditor.js"></script>--%>
        <%-- <script src="../JS/ckeditor2.js"></script>--%>
<%--         <script src="../JS/jsManual.js"></script>
         <link href="../CSS/Pages/ManualToolTip.css" rel="stylesheet" />--%>
        <%--  <script src="../JS/Manual/HtmlEditor.js"></script>
       <script src="../JS/Manual/MenuInicia.js"></script>--%>
    <script type="text/javascript">

 
      
            



            // Se muestra el menú seleccionado
        document.getElementById("lblManual").setAttribute("class", "Seleccionado");

            // Si hay un postback tengo que cargar por segunda vez por que no entra al script
            $("document").ready(function ()
            {
                $('#divOcultatree').click(function (e)
                {
                    if ($('#divTreeView').css("display") == "none")
                    {
                        // handle non visible state
                        $('#divTreeView').show();
                        //$('#divOcultatree').html("Ocultar índice");
                        $('.divFondo').removeClass(' importantRule');

                    }
                    else
                    {
                        $('#divTreeView').hide();
                        //$('#divOcultatree').html("Mostrar índice");
                        $('.divFondo').addClass(' importantRule');
                        // handle visible state
                    }
                });

                //$('.divTemplateTexto').removeAttr("title").removeAttr("onblur");
                //$('.divTemplateTitulo').removeAttr("title").removeAttr("onblur");
                //$(

    
            });

            //Sys.Application.initialize();

            var prm = Sys.WebForms.PageRequestManager.getInstance();

            if (prm != null) {
                prm.add_endRequest
                (
                    function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            document.getElementById("lblManual").setAttribute("class", "Seleccionado");
                        }
                    }
                );
            };


            //Sys.Application.initialize();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            if (prm != null) {
                prm.add_endRequest(function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {

                        agregaToolTips();
                        //
                        $('#divOcultatree').click(function (e) {
                            if ($('#divTreeView').css("display") == "none") {
                                // handle non visible state
                                $('#divTreeView').show();
                                //$('#divOcultatree').html("Ocultar índice");
                                $('.divFondo').removeClass(' importantRule');

                            }
                            else {
                                $('#divTreeView').hide();
                                //$('#divOcultatree').html("Mostrar índice");
                                $('.divFondo').addClass(' importantRule');
                                // handle visible state
                            }
                        });
                        //
                    }

                });
            }

            /* setTimeout(function () {
                 $('#divAviso').fadeOut('easing');
             }, 7000);*/
            setTimeout(function () {
                $('#divAviso').fadeOut('easing');
            }, 2000);

            function imgError(image) {
                //image.onerror = "imgError(document.getElementsByClassName(\"download\").item(0))";
                //image.src = "/../Images/error.jpg";
                image.src = "../Images/Mark.jpg";
                $(image).parent().remove("divTemplateImagen");
                //$("img").remove();
                //image.style.display = "none";
                return true;
            }

    </script>

  
</asp:Content>
