﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad.Brandsite
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        
        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    CargarPalabras("%");
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarPalabras(String sLetra)
        {
            oBrandSite.CargarPalabras(gvGlosario, sLetra, Session["IdBrandSite"].ToString());
        } // Carga el glosario/palabras del brand site

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void lkbAbecedario_Click(object sender, EventArgs e)
        {
            try
            {
                if (((System.Web.UI.WebControls.LinkButton)(sender)).Text != null || ((System.Web.UI.WebControls.LinkButton)(sender)).Text != "")
                {
                    CargarPalabras(((System.Web.UI.WebControls.LinkButton)(sender)).Text);
                }
                else
                {
                    CargarPalabras("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                CargarPalabras("%" + txtBusqueda.Text.Trim() + "%");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}