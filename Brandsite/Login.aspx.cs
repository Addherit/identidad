﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Collections.Generic;
using System.Collections;

namespace Identidad.Brandsite
{
    public partial class Login : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cUsuario oUsuario = new cUsuario();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (!Request.IsLocal && !Request.IsSecureConnection)
                    {
                        string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                        string sNombreBrandSite = oSistema.ObtenerSubdominio(sUrl);
                        if (sNombreBrandSite == "compartamos")
                        {
                            string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                            Response.Redirect(redirectUrl, false);
                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                        }
                    }
                    NavegadorValido();

                    // Obtener el parametro de la url
                    string sParametro = Request.QueryString["Id"].ToString();

                    // Desencriptar el parametro si viene encriptado
                    if (oSistema.isNumeric(sParametro) == true)
                    {
                        Session["IdBrandSite"] = sParametro;
                    }
                    else
                    {
                        Session["IdBrandSite"] = oSistema.DesencriptarId(sParametro);
                    }

                    // Si viene el id del brand site vacio redirecciona a null, si no da el acceso
                    if (Session["IdBrandSite"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        if (Session["IdUsuario"] != null)
                        {
                            Session["NombreUsuario"] = oUsuario.getNombreUsuario(Session["IdUsuario"].ToString());

                            Session["EsDiseñadorSitio"] = oUsuario.EsDiseñadorSitio(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());

                            Session["PuedeInvitar"] = oUsuario.PuedeInvitar(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());

                            Session["IdAdministradorBS"] = oUsuario.IdUsuarioAdministradorSitio(Session["IdBrandSite"].ToString());

                            if (Session["IdUsuario"].ToString() == Session["IdAdministradorBS"].ToString())
                            {
                                Session["EsAdministrador"] = "1";
                                Session["PuedeEditiar"] = true;
                            }
                            else
                            {
                                Session["EsAdministrador"] = "0";
                                Session["PuedeEditiar"] = oUsuario.EsDeMiEquipo(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
                            }

                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect("~/Brandsite/Home.aspx", false);

                            return;
                        }
                    }
                }
            }
            catch (Exception)
            {
                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }

        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void NavegadorValido()
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string s = "Browser Capabilities\n"
                + "Type = " + browser.Type + "\n"
                + "Name = " + browser.Browser + "\n"
                + "Version = " + browser.Version + "\n"
                + "Major Version = " + browser.MajorVersion + "\n"
                + "Minor Version = " + browser.MinorVersion + "\n"
                + "Platform = " + browser.Platform + "\n"
                + "Is Beta = " + browser.Beta + "\n"
                + "Is Crawler = " + browser.Crawler + "\n"
                + "Is AOL = " + browser.AOL + "\n"
                + "Is Win16 = " + browser.Win16 + "\n"
                + "Is Win32 = " + browser.Win32 + "\n"
                + "Supports Frames = " + browser.Frames + "\n"
                + "Supports Tables = " + browser.Tables + "\n"
                + "Supports Cookies = " + browser.Cookies + "\n"
                + "Supports VBScript = " + browser.VBScript + "\n"
                + "Supports JavaScript = " + browser.EcmaScriptVersion.ToString() + "\n"
                + "Supports Java Applets = " + browser.JavaApplets + "\n"
                + "Supports ActiveX Controls = " + browser.ActiveXControls + "\n";

            if (browser.Browser == "InternetExplorer")
            {
                if (float.Parse(browser.Version) < 9)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("/Browser.html", false);
                }
            }
        } // Valida la version del Internet Explorer sea mayor que la 9

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo = oUsuario.IniciarSesion(txtUsuario.Text, txtContraseña.Text);

                if (oMetodo.Pass == true)
                {//se revisa primero que sea la cuenta de diseño. 
                    int pasa;
                    if (int.Parse(oUsuario.getIdUsuario(txtUsuario.Text).ToString()) == 671) { pasa = 1; }
                    else{pasa = oBrandSite.ObtenerIdEstatusBrandSite(Session["IdBrandSite"].ToString());}
                   // if (oBrandSite.ObtenerIdEstatusBrandSite(Session["IdBrandSite"].ToString()) == 1)
                    if(pasa == 1)
                    {
                        oMetodo = oBrandSite.TieneAccesoBS(txtUsuario.Text, Session["IdBrandSite"].ToString());

                        if (oMetodo.Pass == true)
                        {
                            Session["IdUsuario"] = oUsuario.getIdUsuario(txtUsuario.Text);
                            Session["NombreUsuario"] = oUsuario.getNombreUsuario(Session["IdUsuario"].ToString());
                            Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                            Session["EsDiseñadorSitio"] = oUsuario.EsDiseñadorSitio(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
                            Session["PuedeInvitar"] = oUsuario.PuedeInvitar(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());

                            Session["IdAdministradorBS"] = oUsuario.IdUsuarioAdministradorSitio(Session["IdBrandSite"].ToString());

                            if (Session["IdUsuario"].ToString() == Session["IdAdministradorBS"].ToString())
                            {
                                Session["EsAdministrador"] = "1";
                                Session["PuedeEditiar"] = true;
                            }
                            else
                            {
                                Session["EsAdministrador"] = "0";
                                Session["PuedeEditiar"] = oUsuario.EsDeMiEquipo(Session["IdUsuario"].ToString(), Session["IdBrandSite"].ToString());
                            }

                            Session.Timeout = 480;

                            HttpContext.Current.ApplicationInstance.CompleteRequest();
                            Response.Redirect("~/BrandSite/Home.aspx", false);
                        }
                        else
                        {
                            lblValidacion.Visible = true;
                            lblValidacion.Text = oMetodo.Message;
                        }
                    }
                    else
                    {
                        lblValidacion.Visible = true;
                        lblValidacion.Text = "¡Sitio no disponible!";
                    }
                }
                else
                {
                    txtUsuario.Focus();
                    lblValidacion.Visible = true;
                    lblValidacion.Text = oMetodo.Message;
                }

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }

        }

        protected void lkbRecuperarContraseña_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo = oUsuario.RecuperarMiContraseña(txtUsuario.Text);

                lblValidacion.Text = oMetodo.Message;
                lblValidacion.Visible = true;

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);
                
                Session["IdBrandSite"] = oSistema.DesencriptarId(Request.QueryString["Id"].ToString());

                // Agrega la referencia a la hoja de estilo dinámica
                Page.Header.Controls.Add(new System.Web.UI.LiteralControl("<link rel=\"stylesheet\" type=\"text/css\" href=\"../CSS/BrandSites/Id" + Session["IdBrandSite"].ToString() + ".css\" />"));

            }
            catch (Exception)
            {
               HttpContext.Current.ApplicationInstance.CompleteRequest();
               Response.Redirect("~/site/login-registro.aspx", false);

                return;

            //    oSistema.saveLog(error.ToString());
            }
        }

        #endregion
    }
}