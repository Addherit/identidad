﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Brandsite/BrandSite.Master" AutoEventWireup="true" CodeBehind="Novedades.aspx.cs" Inherits="Identidad.Brandsite.WebForm5" meta:resourcekey="Novedades" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/Novedades.css" rel="stylesheet" type="text/css" />

    <div class="divFondoPagina">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                
            <div class="divTituloCentrado">
                <asp:Label ID="lblNovedades" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblNovedades"></asp:Label>
            </div>
            <%--<hr/>--%>
            <%--<br /><br /><br />--%>
                <asp:GridView ID="gvNovedades" CssClass="mGrid gridNovedades" runat="server" AutoGenerateColumns="false" >
                    <Columns>

                        <asp:TemplateField>
                                <ItemTemplate>
                                    <table style="margin-left:27px;">
                                        <tr>
                                            <td>
                                                <div style="width:100px;">
                                                    <asp:Image ID="imgUsuario" CssClass="ImagenUsuario imgCircular" runat="server" ImageUrl='<%#Eval ("Foto") %>' />
                                                </div>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNovedad" CssClas="textoNovedades" runat="server" DataField ="Novedad" Text='<%#Eval ("Novedad") %>' ></asp:Label>
                                                <br />

                                                <asp:Label ID="lblNombre" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblExtension" runat="server" DataField ="Extension" Text='<%#Eval ("Extension") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblAutor" runat="server" DataField ="Autor" Text='<%#Eval ("Autor") %>' Visible="false" ></asp:Label>
                                                <asp:Label ID="lblFechaActivo" runat="server" DataField ="FechaActivo" Text='<%#Eval ("FechaActivo") %>' Visible="false" ></asp:Label>
                                                <br />
                                                <asp:LinkButton ID="lkbVerDetalle" runat="server" meta:resourcekey="lkbVerDetalle" OnClick="lkbVerDetalle_Click"></asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            <!--Inicia Popup para ver detalle--->
            <panel id="pnlVerDetalle" runat="server" style="display: none">
                <div class="divTabla Popup" style="width:450px; height:360px; margin-top:100px;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloVerDetalle" runat="server" meta:resourcekey="lblTituloVerDetalle" ></asp:Label>
                    </div>
                    <hr />
                    <br /><br />

                    <div style="width:95%;">
                        <div style="width:100%; height:210px;">
                            
                            <div style="float:left; width:30%!important; min-width:30%!important; text-align:left;">
                                <asp:Image ID="imgFoto" CssClass="ImagenUsuario imgCircular" runat="server" />
                            </div>

                            <div style="float:right; width:65%; min-width:65%!important;">
                                <asp:Label ID="lblNombreDetalle" runat="server" Font-Bold="true" Font-Size="150%" ></asp:Label>
                                <asp:Label ID="lblExtensionDetalle" runat="server" ></asp:Label>
                                <br /><br />
                                <asp:Label ID="lblDescripcionDetalle" runat="server" ></asp:Label>
                                <br /><br />
                                <asp:Label ID="lblAutorDetalle" runat="server" ></asp:Label> &nbsp;&nbsp;
                                <asp:Label ID="lblFechaActivoDetalle" runat="server" ></asp:Label>
                            </div>

                        </div>
                        <br />
                        <div style="text-align:right;">
                            <asp:Button ID="btnCerrar" runat="server" class="buttons colorAzulO" meta:resourcekey="btnCerrar" OnClick="btnCerrar_Click"></asp:Button>
                        </div>
                    </div>

                </div>
            </panel>

            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppVerDetalle" TargetControlID="Label2" runat="server" PopupControlID="pnlVerDetalle" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".30" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para ver detalle--->


            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblNovedades").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblNovedades").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

</asp:Content>

