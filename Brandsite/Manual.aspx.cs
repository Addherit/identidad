﻿using Identidad.App_Code;
using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Identidad.Brandsite
{
    public partial class WebForm4 : System.Web.UI.Page
    {

        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cManual oManual = new cManual();
        cMetodo oMetodo = new cMetodo();
        //METODOS

        #endregion


        #region Eventos

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    CargaSesiones();

                    if (lblIdUsuario.Text == "" || lblIdBrandiste.Text == "")
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);
                        return;
                    }
                    CargarIndice();
                    cargarManual();
                    CargaGlosario();

                }
            }catch(NullReferenceException ex){

                //oSistema.saveLog(ex.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            
            
            }
        
        }

        protected void btnSalirArchivosRelacionados_Click(object sender, EventArgs e)
        {
            ppArchivosRelacionados.Hide();
        }

        protected void lkbDescargar_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                string sIdArchivo = ((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblIdArchivo")))).Text;
                string sNombre = ((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblNombre")))).Text;
                string sUrl = ((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblUrl")))).Text;
                string sExtension = ((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblExtension")))).Text;
                int iTamaño = int.Parse(((System.Web.UI.WebControls.Label)((dlArchivosRelacionados.Items[Row].FindControl("lblBytes")))).Text);

                sUrl = sUrl.Split('?')[0];

                sNombre = sNombre.Replace(' ', '_');

                oBrandSite.DescargarArchivo(sUrl, sNombre, sExtension, iTamaño);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnImage1_Click(object sender, EventArgs e)
        {
            try
            {
                int test = int.Parse(txtIdIndiceSeleccionado.Text);

                switch (test)
                {
                    case 16821:
                        //nada tiene que pasar
                        break;
                    case 16827:
                        //nada tiene que pasar
                        break;
                    case 16830:
                        //nada tiene que pasar
                        break;
                    case 16833:
                        //nada tiene que pasar
                        break;
                    case 20084:
                        //nada tiene que pasar
                        break;
                    case 20085:
                        //nada tiene que pasar
                        break;
                    case 20086:
                        //nada tiene que pasar
                        break;
                    case 20087:
                        //nada tiene que pasar
                        break;
                    case 20088:
                        //nada tiene que pasar
                        break;
                    case 16861:
                        //nada tiene que pasar
                        break;
                    case 17077:
                        //nada tiene que pasar
                        break;
                    case 17292:
                        //nada tiene que pasar
                        break;
                    case 17532:
                        //nada tiene que pasar
                        break;
                    case 17533:
                        //nada tiene que pasar
                        break;
                    case 16842:
                        //nada tiene que pasar
                        break;
                    case 16843:
                        //nada tiene que pasar
                        break;
                    case 16845:
                        //nada tiene que pasar
                        break;
                    case 16848:
                        //nada tiene que pasar
                        break;
                    case 17290:
                        //nada tiene que pasar
                        break;
                    case 17370:
                        //nada tiene que pasar
                        break;
                    case 16895:
                        //nada tiene que pasar
                        break;
                    case 16987:
                        //nada tiene que pasar
                        break;
                    case 16988:
                        //nada tiene que pasar
                        break;
                    case 16989:
                        //nada tiene que pasar
                        break;
                    case 16981:
                        //nada tiene que pasar
                        break;
                    case 16995:
                        //nada tiene que pasar
                        break;
                    case 17272:
                        //nada tiene que pasar
                        break;
                    case 17277:
                        //nada tiene que pasar
                        break;
                    case 17280:
                        //nada tiene que pasar
                        break;
                    case 17281:
                        //nada tiene que pasar
                        break;
                    case 17286:
                        //nada tiene que pasar
                        break;
                    case 17411:
                        //nada tiene que pasar
                        break;
                    case 17417:
                        //nada tiene que pasar
                        break;
                    case 17535:
                        //nada tiene que pasar
                        break;
                    case 17540:
                        //nada tiene que pasar
                        break;
                    case 16838:
                        //nada tiene que pasar
                        break;
                    case 16839:
                        //nada tiene que pasar
                        break;
                    case 16840:
                        //nada tiene que pasar
                        break;
                    case 16873:
                        //nada tiene que pasar
                        break;
                    case 16874:
                        //nada tiene que pasar
                        break;
                    case 16875:
                        //nada tiene que pasar
                        break;
                    case 16900:
                        //nada tiene que pasar
                        break;
                    case 20763:
                        //nada tiene que pasar
                        break;
                    case 20764:
                        //nada tiene que pasar
                        break;
                    case 20765:
                        //nada tiene que pasar
                        break;
                    case 20766:
                        //nada tiene que pasar
                        break;
                    case 20769:
                        //nada tiene que pasar
                        break;
                    case 20771:
                        //nada tiene que pasar
                        break;
                    case 20772:
                        //nada tiene que pasar
                        break;
                    case 20773:
                        //nada tiene que pasar
                        break;
                    case 16872:
                        //nada tiene que pasar
                        break;
                    case 16879:
                        //nada tiene que pasar
                        break;
                    case 20776:
                        //nada tiene que pasar
                        break;
                    case 20778:
                        //nada tiene que pasar
                        break;
                    case 20779:
                        //nada tiene que pasar
                        break;
                    case 20781:
                        //nada tiene que pasar
                        break;
                    case 20777:
                        //nada tiene que pasar
                        break;
                    case 20774:
                        //nada tiene que pasar
                        break;
                    case 20775:
                        //nada tiene que pasar
                        break;
                    case 16817:
                        //nada tiene que pasar
                        break;
                    case 16831:
                        //nada tiene que pasar
                        break;
                    case 16859:
                        //nada tiene que pasar
                        break;
                    case 16841:
                        //nada tiene que pasar
                        break;
                    case 16890:
                        //nada tiene que pasar
                        break;
                    case 16883:
                        //nada tiene que pasar
                        break;
                    case 16896:
                        //nada tiene que pasar
                        break;
                    case 16902:
                        //nada tiene que pasar
                        break;
                    case 16904:
                        //nada tiene que pasar
                        break;
                    case 16906:
                        //nada tiene que pasar
                        break;
                    case 16929:
                        //nada tiene que pasar
                        break;
                    case 16910:
                        //nada tiene que pasar
                        break;
                    case 16912:
                        //nada tiene que pasar
                        break;
                    case 16977:
                        //nada tiene que pasar
                        break;
                    case 17271:
                        //nada tiene que pasar
                        break;
                    case 17409:
                        //nada tiene que pasar
                        break;
                    case 16854:
                        //nada tiene que pasar
                        break;
                    case 16820:
                        //nada tiene que pasar
                        break;
                    case 16876:
                        //nada tiene que pasar
                        break;
                 
                 
                    default:
                        oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(txtIdIndiceSeleccionado.Text), 1);
                        ppArchivosRelacionados.Show();
                        
                        break;
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnImage2_Click(object sender, EventArgs e)
        {
            try
            {
                if (int.Parse(txtIdIndiceSeleccionado.Text) != 16821)
                {
                    oBrandSite.CargarArchivosRelacionados(dlArchivosRelacionados, int.Parse(txtIdIndiceSeleccionado.Text), 2);
                    ppArchivosRelacionados.Show();
                }
                else
                {
                    //nada tiene que pasar
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void dlArchivosRelacionados_ItemCreated(object sender, DataListItemEventArgs e)
        {
            try
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lkbDescargar") as LinkButton;
                if (btn != null)
                {
                    btn.Click += lkbDescargar_Click;
                    scriptMan.RegisterPostBackControl(btn);
                }
            }
            catch (Exception erro)
            {
                oSistema.saveLog(erro.ToString());
            }
        }

        #endregion 

        #region Metodos

        //Carga Glosario
        public void CargaGlosario(){

            DataTable dtGlosario = oManual.consultaGlosario(lblIdBrandiste.Text);
            gvwGlosario.DataSource = dtGlosario;
            gvwGlosario.DataBind();
        
        }

        //Carga el Indice del manual en el treeView
        public void CargarIndice() 
        {
            oBrandSite.CargarIndiceBS(tvwIndice, lblIdBrandiste.Text);
            tvwIndice.ExpandAll();

        }
        
        
        //Carga las sessiones a labls
        public void CargaSesiones() 
        {

            lblIdUsuario.Text = Session["IdUsuario"].ToString() == null ? "" : Session["IdUsuario"].ToString();
            lblIdBrandiste.Text = Session["IdBrandSite"].ToString() == null ? "" : Session["IdBrandSite"].ToString();

        }

        //obtiene toda la lista de las plantillas para llenar el manual.
        public void cargarManual() {
            try
            {
                
                DataTable dtIndices = oManual.consultarManualHTML(lblIdBrandiste.Text);
                dtHTML.DataSource = dtIndices;
                dtHTML.DataBind();
            }
            catch { 
            
            
            }
        
        }


        //Reemplaza los simbolos de la palabra para que puedan funcionar las anclas ya que con los acentos y caracteres especiales no funcionan.
        public string RemplazarSimbolos(string sPalabra) {

            return oBrandSite.RemplazarSimbolos(sPalabra);
        }

        #endregion

    }

  
}
