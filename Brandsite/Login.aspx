﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Identidad.Brandsite.Login" meta:resourcekey="Login"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Identidad.com | Iniciar sesión</title>
    
    <link href="../CSS/Pages/Login.css" rel="stylesheet" type="text/css" />
    <link href="../CSS/Site.css" rel="stylesheet" type="text/css"/>
    <link href="../CSS/Menu.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Design.css" rel="stylesheet" type="text/css" />
    <script src="../JS/Site.js" type="text/javascript"></script>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link href='../Images/Site/ID.ico' rel='shortcut icon' type='image/x-icon'/>
    <meta name="application-name" content="Identidad.com"/>
    <meta name="application-tooltip" content="Identidad.com"/>
    <meta name="application-bavbutton-color" content="Blue"/>
    <link href="../Images/Site/ID.ico" rel="icon" type="image/ico"/>
	<link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico"/>
	<link rel="apple-touch-icon" href="../Images/Site/ID.ico"/>
    <link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" />
    <meta name="apple-mobile-web-app-title" content="Identidad.com"/>
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='apple-touch-fullscreen' content='yes'/>
    <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
    <link rel="icon" href="../Images/Site/ID.ico" />
	<link rel="shortcut icon" href="../Images/Site/ID.ico" />
    <meta charset="utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>


</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnIniciarSesion" defaultfocus="txtUsuario">
        <div id="page">
            <div id="cabeza">

            </div>
            <div id="contenido">
            <div class="divFondoPagina">
                <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
            
                <div class="divTituloCentrado">
                    <asp:Label ID="lblLogin" runat="server" meta:resourcekey="lblLogin" Visible="false"></asp:Label>
                    <a href="https://identidad.com" style="display:none;">
                        <img src="../Images/Site/LogoPrincipal.png" />
                    </a>
                </div>
                <br />
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate>
                        <div class="Centrado">
                                <div class="divTBODY">
                                    <div class="divTR">
                                        <div class="divTDIzq"> 
                                            <asp:Label ID="lblUsuario" runat="server" meta:resourcekey="lblUsuario"></asp:Label>
                                        </div>
                                        <div class="divTDDerechoInput"> 
                                           <asp:TextBox ID="txtUsuario" CssClass="TextBox" runat="server" meta:resourcekey="txtUsuario" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="divTR">
                                        <div class="divTDIzq"> 
                                            <asp:Label ID="lblContraseña" runat="server" meta:resourcekey="lblContraseña"></asp:Label>
                                        </div>
                                        <div class="divTDDerechoInput"> 
                                            <asp:TextBox ID="txtContraseña" CssClass="TextBox" TextMode="Password" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="divTR">                            
                                        <div class="divTDIzq"></div>
                                        <div class="divTDDerechoInput"> 
                                            <asp:LinkButton ID="lkbRecuperarContraseña" runat="server" meta:resourcekey="lkbRecuperarContraseña" OnClick="lkbRecuperarContraseña_Click"></asp:LinkButton>
                                            
                                        </div>
                                    </div>
                                    <div class="divTR">
                                        <div class="divTDIzq">

                                        </div>
                                        <div class="divTDDerechoButtons" > 
                                            <asp:Button id="btnIniciarSesion" style="margin-left:110px;" class="buttons colorAzulO" meta:resourcekey="btnIniciarSesion" runat="server" autopostback="true" OnClick="btnIniciarSesion_Click"></asp:Button>
                                            <br />
                                            <asp:Label ID="lblValidacion" style="margin-left:225px;" runat="server" ></asp:Label>
                                        </div>
                                    </div>
                                
                                </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            </div>
        </div>
    </form>

    <script type="text/javascript">

        onLoad(document.getElementById('btnIniciarSesion'));

    </script>

</body>
</html>
