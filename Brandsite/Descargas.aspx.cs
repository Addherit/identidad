﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Web.UI;

namespace Identidad.Brandsite
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        cMetodo oMetodo = new cMetodo();

        #endregion

        #region Clase Ruta

        // Clase para crear una lisat de tipo cRuta, que nos de el id de la carpeta y el nombre, de tal forma que podamos navegar de forma más comoda
        private class cRuta
        {
            public int IdCarpeta { get; set; }
            public string sNombreCarpeta { get; set; }
        }

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Form.DefaultFocus = this.txtBusqueda.UniqueID;
                this.Form.DefaultButton = this.btnBuscar.UniqueID;

                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null || Session["IdBrandSite"] == null) //Si no se tiene guardada en Session el Id del usuario o el id del BrandSite te envía al login
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    Session["RutaCarpetas2"] = null;
                    CargarOrdenadoPor();
                    CargarArchivos();

                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarOrdenadoPor()
        {
            HttpCookie cookieIdioma = Request.Cookies["idioma"];

            if (cookieIdioma != null && cookieIdioma.Value != null)
            {
                ddlOrdenadoPor.Items.Clear();

                if (cookieIdioma.Value == "en-US" || cookieIdioma.Value == "en")
                {
                    ddlOrdenadoPor.Items.Add(new ListItem("Date", "FechaActivo DESC"));
                    ddlOrdenadoPor.Items.Add(new ListItem("File name", "Nombre ASC"));
                    ddlOrdenadoPor.Items.Add(new ListItem("File size", "Bytes DESC"));
                }
                else
                {
                    ddlOrdenadoPor.Items.Add(new ListItem("Fecha de actualización", "FechaActivo DESC"));
                    ddlOrdenadoPor.Items.Add(new ListItem("Nombre del archivo", "Nombre ASC"));
                    ddlOrdenadoPor.Items.Add(new ListItem("Peso del archivo", "Bytes DESC"));
                }
            }



        }// Carga el DDL con la información que se podría ordenar

        public void CargarArchivos()
        {
            CargarArchivos(lblIdCarpetaPadreSeleccionada.Text);
        }

        private void CargarArchivos(string sIdCarpetaPadreSeleccionada)
        {
            if (oSistema.ValidarLenght(txtBusqueda.Text) == false)
            {
                txtBusqueda.Text = "";
            }

            oBrandSite.CargarArchivos(dlArchivos, Session["IdBrandSite"].ToString(), txtBusqueda.Text, "0", ddlOrdenadoPor.SelectedValue, Session["IdUsuario"].ToString(), sIdCarpetaPadreSeleccionada);
            
            if (dlArchivos.Items.Count != 0)
            {

                for (int i = 0; i < dlArchivos.Items.Count; i++)
                {
                    if (((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblExtension")))).Text == ".folder")
                    {
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblExtension")))).Visible = false;
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblDescripcion")))).Visible = false;
                        ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblPeso")))).Visible = false;
                        ((System.Web.UI.WebControls.LinkButton)((dlArchivos.Items[i].FindControl("lkbDescargar")))).Visible = false;
                    }
                    else
                    {
                        ((System.Web.UI.WebControls.Image)((dlArchivos.Items[i].FindControl("imgPreview")))).Attributes.Add("style", "background-color:#ffffff!important;");

                        oBrandSite.CargarTagsDeArchivo((DataList)dlArchivos.Items[i].FindControl("dlTags"), ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblIdArchivo")))).Text);
                    }
                }
                
            }

            oBrandSite.CargarItemsBusquedaArchivos(ddlBusqueda, Session["IdBrandSite"].ToString());

            List<cRuta> lRutas = new List<cRuta>();
            lRutas = (List<cRuta>)Session["RutaCarpetas2"];
            if (lRutas != null)
            {
                if (lRutas.Count == 0)
                {
                    btnRegresar.Visible = false;
                }
                else
                {
                    btnRegresar.Visible = true;
                }
            }
            else
            {
                btnRegresar.Visible = false;
            }

        } // Carga los archivos en el data list de descargas

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtBusqueda.Text.Trim() == "")
                {
                    CargarArchivos();
                }
                else
                {
                    CargarArchivos("%");

                    cCarpeta oCarpeta = new cCarpeta();

                    if (dlArchivos.Items.Count != 0)
                    {
                        for (int i = 0; i < dlArchivos.Items.Count; i++)
                        {
                            ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblRuta")))).Visible = true;
                            ((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblRuta")))).Text = oCarpeta.ObtenerCarpetasDeArchivo(int.Parse(((System.Web.UI.WebControls.Label)((dlArchivos.Items[i].FindControl("lblIdArchivo")))).Text));
                        }
                    }

                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }//Botón de buscar por un archivo especifico

        protected void ddlOrdenadoPor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtBusqueda.Text.Trim() == "")
                {
                    CargarArchivos();
                }
                else
                {
                    CargarArchivos("%");
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }//Evento cuando cambia el index del DDL de ordenar

        protected void btnRegresar_Click(object sender, EventArgs e)
        {
            try
            {
                List<cRuta> lRutas = new List<cRuta>();
                lRutas = (List<cRuta>)Session["RutaCarpetas2"];
                lRutas.RemoveAt(lRutas.Count - 1);
                if (lRutas.Count > 0)
                {
                    lblIdCarpetaPadreSeleccionada.Text = lRutas[lRutas.Count - 1].IdCarpeta.ToString();
                }
                else
                {
                    lblIdCarpetaPadreSeleccionada.Text = "0";
                }

                lblRuta.Text = "";
                if (lRutas.Count > 0)
                {
                    //lblRuta.CssClass = "divCajaGris";
                    for (int i = 0; i < lRutas.Count; i++)
                    {
                        lblRuta.Text = lblRuta.Text + lRutas[i].sNombreCarpeta + "   >   ";
                    }
                }
                else
                {
                    //lblRuta.CssClass = lblRuta.CssClass.Replace("divCajaGris", "");
                }

                Session["RutaCarpetas2"] = lRutas;

                CargarArchivos();
            }
            catch (Exception ex)
            {
                oSistema.saveLog(ex.ToString());
            }
        }//Botón de regresar

        protected void imgPreview_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            try
            {
                ImageButton Status = (ImageButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;

                if (((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text == ".folder")
                {
                    lblIdCarpetaPadreSeleccionada.Text = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;

                    List<cRuta> lRutas;
                    lRutas = (List<cRuta>)Session["RutaCarpetas2"];
                    cRuta oRuta = new cRuta();
                    oRuta.IdCarpeta = int.Parse(lblIdCarpetaPadreSeleccionada.Text);
                    oRuta.sNombreCarpeta = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblNombre")))).Text;
                    if (lRutas == null)
                    {
                        lRutas = new List<cRuta>();
                    }

                    lRutas.Add(oRuta);

                    lblRuta.Text = "";
                    if (lRutas.Count > 0)
                    {
                        //lblRuta.CssClass = "divCajaGris";

                        for (int i = 0; i < lRutas.Count; i++)
                        {
                            lblRuta.Text = lblRuta.Text + lRutas[i].sNombreCarpeta + "   >   ";
                        }
                    }
                    else
                    {
                        //lblRuta.CssClass = lblRuta.CssClass.Replace("divCajaGris", "");
                    }

                    Session["RutaCarpetas2"] = lRutas;

                    CargarArchivos();
                }
            }
            catch (Exception ex)
            {
                oSistema.saveLog(ex.ToString());
            }
        }//Evento de cuando se da click en el preview de la imagen

        protected void lkbDescargar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((DataListItem)Status.NamingContainer).ItemIndex;
                string sIdArchivo = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblIdArchivo")))).Text;
                string sNombre = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblNombre")))).Text;
                string sUrl = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblUrl")))).Text;
                string sExtension = ((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblExtension")))).Text;
                int iTamaño = int.Parse(((System.Web.UI.WebControls.Label)((dlArchivos.Items[Row].FindControl("lblBytes")))).Text);

                sUrl = sUrl.Split('?')[0];
                //sUrl = sUrl + sExtension;

                //sNombre = sNombre + sExtension;
                sNombre = sNombre.Replace(' ', '_');

                oBrandSite.DescargarArchivo(sUrl, sNombre, sExtension, iTamaño);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }//Descarga la imagen seleccionada

        protected void dlArchivos_ItemCreated(object sender, DataListItemEventArgs e)
        {
            try
            {
                ScriptManager scriptMan = ScriptManager.GetCurrent(this);
                LinkButton btn = e.Item.FindControl("lkbDescargar") as LinkButton;
                if (btn != null)
                {
                    btn.Click += lkbDescargar_Click;
                    scriptMan.RegisterPostBackControl(btn);
                }
            }
            catch (Exception erro)
            {
                oSistema.saveLog(erro.ToString());
            }
        }

        #endregion

    }
}