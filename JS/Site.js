﻿// Bloquea el ENTER
//document.onkeydown = function (evt) {
//    return (evt ? evt.which : event.keyCode) != 13;
//}

function onLeave(_input)
{
    var check = _input.value;

    if (check.length < 1)
    {
        _input.className = "TextBoxError";
    }
    else
    {
        _input.className = "TextBox";
    }
}

function onLeave1(_input) {
    var check = _input.value;

    if (check.length < 1) {
        _input.className = "TextBoxError";
    }
    else {
        _input.className = " DropDownList TextBox1";
    }
}


function ValidaInputs(eButton)
{
 
    var vacio = true;
 
    var elementos = '#' + eButton.toString() + ' input[onblur="onLeave(this)"]';
  
    $(elementos).each(function (a) {
       
        if ($(this).val().trim() == "")
        {
            vacio = false;
            $(this).removeClass(" TextBox");
            $(this).addClass(" TextBoxError");   

        } else
        {
            $(this).addClass(" TextBox");
            $(this).removeClass(" TextBoxError");
        }

    });
  
    if (vacio != false)
    var valor = postbackButtonClick();
   
    if (valor == true && vacio == true)
    {
        vacio = true;
    } else
    {
        vacio = false;
    }

    
    return (vacio);

}

// Returns the version of Internet Explorer or a -1 for other browsers.

function getInternetExplorerVersion()
{
    var rv = -1;
    if (navigator.appName == 'Microsoft Internet Explorer') {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    return rv;
}

function onLoad(opcionAbloquear) {

    var rv = -1;

    if (navigator.appName == 'Microsoft Internet Explorer') {

        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
        if (re.exec(ua) != null)
            rv = parseFloat(RegExp.$1);
    }
    
    var version = rv;

    if (version <= 8 && version > -1) {

        window.location.href = "/Browser.html";

        document.getElementById(opcionAbloquear).style.display = 'none';

        alert('Esta versión de navegador no es soportada.');
    }
    else {

    }
}

