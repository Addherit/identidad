﻿$().ready(function () {

    //$('.dd').nestable('collapseAll');

    $("#MySplitter").splitter();

    var tamMinimo = 280;         //ES EL TAMAÑO MINIMO A INICIALIZAR LOS DIVS.

    $(".vsplitbar").append('<div id="collapse"></div>'); //Agrega el div para al darle click oculte el panel del menu

    $("#collapse").click(function () {  // se creo la funcion click para el div collapse para que nos ayudara a manejar los eventos
   

        if ($("#MySplitter>div").width() <= '10') {// si el tamaño del primer div que hay en el div mySplitter es igual a 0 entonces pone el tamaño en 200, abre de nuevo el div
            
            $("#MySplitter").trigger("resize", tamMinimo);
        } else                                    // si no, pone el tamaño en 0, lo cierra                    
            $("#MySplitter").trigger("resize", 0);
    }); // fin del metodo click del collapse


    $("#MySplitter").trigger("resize", tamMinimo); // inicializa el div Mysplitter en 200px 
   

});

