﻿//Crea los tooltips para las clases 
$('.dd3-content').prop('title', 'Doble clic para editar');

var DELAY = 500,
    clicks = 0,
    timer = null,
    texto = "";
$(document).ready(function () {

    $(".dd3-content span").live("click", function (e) { //Ponemos esta funcion para que no ejecute el click cuando se hace un dbl click.


        clicks++;  //count clicks
        var text = $(this).text();
        var $this = $(this);
        var id = $(this).parent().parent().data('id');

        if (id != texto) {

            clicks = 1;
        }

        texto = id;
       
        if (clicks === 1) { // si el numero de clicks es 1 entra y crea el timmer para que espere un ratito y hace la funcion de cargar el contenido


            timer = setTimeout(function () {

              //  CargaElementos($this.parent().parent().data('id'));
                CargaElementos(id);

            }, DELAY);

        } else { // si no 

            clearTimeout(timer);  //limpia el timmer y ejecuta lo del dblClick

            var $input = $('<input  type="text" class="TextMenu"/>');
                    
            $this.text("");
            $input.prop('value', text.trim()); //agrega en el value el texto 
            $input.appendTo($this.parent()); //concatena el texto en el texbox
            $input.focus();

            $(".TextMenu").click(function (e) {
                e.preventDefault();

            });


            $input.focusout(function () { //crea un evento cuando el foco salio del input entonces guarda el cambio
               
                //si el texto esta vacio pone sin titulo                
                //$this.html($input.val().trim() != "" ? $input.val().trim() : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
                $this.html($input.val().trim() != "" ? $input.val().trim() : "Sin título");
                $this.show();
                $input.remove();
               // $('#ContentPlaceHolder1_lblNombreMenu').text(text);
                //GuardarTexto($this.parent().data('id'), $this.text());
                GuardarTexto(id, $this.text());
             
            });
            
            //onkey press enter
            $input.bind('keyup', function (e) { // cacha el evento cuando se preciona una tecla si vemos que es el enter guarda el contenido.

                var key = e.keyCode || e.which;
                //$('#ContentPlaceHolder1_lblNombreMenu').text($input.val());

              if (key === 13) {
                    //si el texto esta vacio pone sin titulo
                  
                  $this.html($input.val().trim() != "" ? $input.val().trim() : "Sin título");
                  
                    $this.show();
                    $input.remove();
                    //$('#ContentPlaceHolder1_lblNombreMenu').text(text);
                    //// GuardarTexto($this.parent().data('id'), $this.text());
                    //alert(id+" "+text);
                    GuardarTexto(id, $this.text());
                };
               

            });

           

            clicks = 0;  //despues de la accion restablece los clicks
        }

    })
    .live("dblclick", function (e) {
        e.preventDefault();  //cancela ele vento de dblclick del sistema
    });


   
});


$("#divTitulos").live("dblclick", function (e) {

    var text = $(this).text();
    var $this = $(this);
    var ID = $("#ContentPlaceHolder1_txtIdIndiceMenu").val();

    var $input2 = $('<input  type="text" class="TextBox" />');
   
    $input2.prop('value', text.trim()); //agrega en el value el texto 
    $("#ContentPlaceHolder1_lblNombreMenu").hide();
    $input2.appendTo($this); //concatena el texto en el texbox
    $input2.focus(); //cancela ele vento de dblclick del sistema

    $input2.focusout(function () { //crea un evento cuando el foco salio del input entonces guarda el cambio

        //si el texto esta vacio pone sin titulo
       // $this.text($input2.val() != "" ? $input2.val() : "Sin título");
        $("#ContentPlaceHolder1_lblNombreMenu").text($input2.val().trim() != "" ? $input2.val().trim() : "Sin título");
        //$("#ContentPlaceHolder1_txtNombreMenu").val($input2.val() != "" ? $input2.val() : "Sin título");
        $("#ContentPlaceHolder1_lblNombreMenu").show();
        $input2.remove();
        GuardarTexto(ID, $this.text());
        $('li[data-id="' + ID + '"] div[class="dd3-content"] span:first').text($this.text());
    });

    //onkey press enter
    $input2.bind('keyup', function (e) { // cacha el evento cuando se preciona una tecla si vemos que es el enter guarda el contenido.

        var key = e.keyCode || e.which;
        //$('#ContentPlaceHolder1_lblNombreMenu').text($input.val());

        if (key === 13) {
            //si el texto esta vacio pone sin titulo

            //$this.text($input2.val() != "" ? $input2.val() : "Sin título");
            //$this.show();
            //$input2.remove();

            $("#ContentPlaceHolder1_lblNombreMenu").text($input2.val().trim() != "" ? $input2.val().trim() : "Sin título");
            //$("#ContentPlaceHolder1_txtNombreMenu").val($input2.val() != "" ? $input2.val() : "Sin título");
            $("#ContentPlaceHolder1_lblNombreMenu").show();
            $input2.remove();
            GuardarTexto(ID, $this.text());
            $('li[data-id="' + ID + '"] div[class="dd3-content"] span:first').text($this.text());
        };


    });

});


function GuardarTexto(Indice, NombreIndice) { //manda el nuevo texto del indice modificado.
  
    
    //Tomar el contenido del texBox y mandar a BD el JSON
    $.ajax({
        type: "POST",
        url: "Manual.aspx/GuardaNombreIndice",
        data: "{ 'sNombreIndice' : '" + NombreIndice+ "','sIndice':'" + Indice + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",

        complete: function () {
          
            if ($("#ContentPlaceHolder1_txtIdIndiceMenu").val() == Indice.toString()) {
                $('#ContentPlaceHolder1_lblNombreMenu').text(NombreIndice);
                $("#ContentPlaceHolder1_txtNombreMenu").val(NombreIndice);
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Hubo un problema al conectar con la Base de datos");
            $('#UpdateProgress1').fadeOut("slow");
        }
 });

}


function CargaElementos(Indice) { //manda el nuevo texto del indice modificado.
    $(document).ready(function () {


        //Tomar el contenido del texBox y mandar a BD el JSON
        $.ajax({
            type: "POST",
            url: "Manual.aspx/CargaElementos",
            data: "{'sIndice':'" + Indice.toString() + "'}",
            contentType: "application/json; charset=utf-8",
            dataType: "json",

            success: function (response) {


                var Datosdt = (typeof response.d) == 'string' ?
                                   eval('(' + response.d + ')') :
                                   response.d;

                $("#ContentPlaceHolder1_lblIdSeleccionado").text(Datosdt[0]); //muestra en pantalla el lbl de idSeccion
                $("#ContentPlaceHolder1_txtIdIndiceMenu").val(Datosdt[0]); //muestra en pantalla el lbl de idSeccion
                $("#ContentPlaceHolder1_lblNombreMenu").text(Datosdt[1]);//Muestra en pantalla los lblsNombreMenu
                $("#ContentPlaceHolder1_txtNombreMenu").val(Datosdt[1]);
                $("#ContentPlaceHolder1_txtHtml").val(Datosdt[2]); //guarda el html

                $("#ContentPlaceHolder1_btnEliminarSeccion").show();
                $("#ContentPlaceHolder1_btnEditarTemplate").show();

                AgregaHTMLEvent(Datosdt[2].toString());




                $('#UpdateProgress1').fadeOut("slow");

                ValidarImgParaArchivosRelacionados();

                //$('#nestable3 div[class="dd3-content"]').removeClass("seleccinarItem");
                //$('li[data-id="' + Indice + '"] div[class="dd3-content"]:first').addClass(" seleccinarItem");

            },
            beforeSend: function () {
                $('#UpdateProgress1').fadeIn(100);
            },
            complete: function () {
                //alert("test");
                $('#UpdateProgress1').fadeOut("slow");
                $("[class=download]").error(
                    function () {
                        $(this).attr("src", "/../Images/Mark.jpg");
                        //$(this).parent().remove("divTemplateImagen margen");
                        //$("[class=download]").attr("src", "");
                        $("[class=download]").parent().remove("divTemplateImagen");
                    });
                //$("[class=download]").attr("onerror", imgError(document.getElementsByClassName("download").item(0)));
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert("Hubo un problema al conectar con la Base de datos");
                $('#UpdateProgress1').fadeOut("slow");
            }
        });

    });
}

/*Manda el html de la pagina para que lo guarde y asigne eventos*/
function AgregaEventosPagina() {


    var htmltext = $('#ContentPlaceHolder1_divPlantilla').html();
    AgregaHTMLEvent(htmltext);

}

function AgregaHTMLEvent(html) {

    var htmlElement = $(html);
    $('#ContentPlaceHolder1_divPlantilla').html(htmlElement);
    /*Agrega los eventos del editor de texto a cada div*/
            
    try {

        if (parseInt(html.indexOf('divTemplateTexto')) != -1) {

            var el2 = $('#ContentPlaceHolder1_divPlantilla .divTemplateTexto').each(function (e) {
                CKEDITOR.inline(this);

            });

        }
        if (parseInt(html.indexOf('divTemplatePie')) != -1) {

            var el2 = $('#ContentPlaceHolder1_divPlantilla .divTemplatePie').each(function (e) {
                CKEDITOR.inline(this);

            });

        }
        if (parseInt(html.indexOf('divTemplateTitulo')) != -1) {

            var el2 = $('#ContentPlaceHolder1_divPlantilla .divTemplateTitulo').each(function (e) {
                CKEDITOR.inline(this);

            });

        }
    }
      catch(err){
                  
    }

}


function CambiarFondoBG(color) {

    $('.divTemplate').removeAttr('style');
    $('.divTemplate').css("background-color", color);

    UpdateHTMLPlantilla();

    //$('#<%=btnPublicar.ClientID %>').click();
}

function UpdateHTMLPlantilla() { //manda el nuevo texto del indice modificado.

    $(document).ready(function(){
        
    $('#ContentPlaceHolder1_txtHtml').val($('#ContentPlaceHolder1_divPlantilla').html().replace("cke_focus", ""));

    var txtHtml = $('#ContentPlaceHolder1_txtHtml').val();
    var idIndice = $('#ContentPlaceHolder1_txtIdIndiceMenu').val();
    var sTexto = $('#idTexto1').html();
    var sTexto2 = $('#idTexto2').html();
    var sPie = $('#idPie1').text();
    var sPie2 = $('#idPie2').text();
    var sTitulo = $('#idTitulo').text();
    var sImg = $('#Img1').html();
    var sImg2 = $('#Img2').html();

   
    if (typeof sImg == 'undefined') {
        sImg = "";
    }
    if (typeof sImg2 == 'undefined') {
        sImg2 = "";
    }
    if (typeof sTexto == 'undefined') {
        sTexto = "";
    }
    if (typeof sTexto2 == 'undefined') {
        sTexto2 = "";
    }
        //actualiza el html con el color nuevo y los contenidos.
   
    $.ajax({
        type: "POST",
        url: "Manual.aspx/GuardaHTMLPlantilla",
        data: "{ 'sHTML' : '" + txtHtml + "','sTitulo':'" + sTitulo + "','sTexto':'" + sTexto + "','sTexto2':'" + sTexto2 + "','sPie':'" + sPie + "','sPie2':'" + sPie2 + "','sImg':'" + sImg + "','sImg2':'" + sImg2 + "','idIndice':'" + idIndice + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Hubo un problema al conectar con la Base de datos " + xhr.responseText);
            $('#UpdateProgress1').fadeOut("slow");
        },
        beforeSend: function () {
            $('#UpdateProgress1').fadeIn(100);
        },
        complete: function () {

            $('#UpdateProgress1').fadeOut("slow");


        }

        // 

    });

});

}

 function guardarHTML(eControl) {
        //alert("entro");
        //var IDIMG = $(eControl).attr("id"); //obtiene el Id del control al que ingreso
    
     UpdateHTMLPlantilla();

 }

//Guarda la imagen en el HTML
 
 function GuardarImage(sUrlImage, sIdImage) {

     //width:100%;
     var HTMLIMAGE = "<img src='" + sUrlImage + "' style='height:auto;max-width:100%; background-color:white;' class='download' onerror=\"imgError(this);\"></img>";
     $("#" + sIdImage).html(HTMLIMAGE);

     UpdateHTMLPlantilla();
 }

 function ControlMG(eControl)
 {
     try
     {
         var sIdImage = $(eControl).attr("id"); //obtiene el Id del control al que ingreso
       
         if (sIdImage == "Img1")
         {
             $get('ContentPlaceHolder1_btnImage1').click();
         }
         else
         {
             $get('ContentPlaceHolder1_btnImage2').click();
         }
        
     } catch (err)
     {
     }
 }