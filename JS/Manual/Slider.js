﻿ 
var  Intervalo;
var slid = function() {



    (function () {

        var $document = $(document);
        var selector = '[data-rangeslider]';
        var $element = $(selector);
        var bloque;
        var imgControl;

        //Agrega el listener a las imagenes, para que al hacerles clic alinie las imagenes del slider
        $('#Aizq').click(function () { Alineacion(this) });
        $('#Acen').click(function () { Alineacion(this) });
        $('#Ader').click(function () { Alineacion(this) });
        



        // For ie8 support
        var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

        // quita el slider adicional que se creo al guardar todo el html y solo crea un slider con el selector
        var elementos = $(document.getElementsByClassName('rangeslider rangeslider--horizontal'));
        if ($('#DivImg2').length > 0 && elementos.length > 3) {
            elementos[3].parentNode.removeChild(elementos[3]);
            elementos[1].parentNode.removeChild(elementos[1]);

            //elementos[3].remove();
            //elementos[1].remove();
        } else if ($('#DivImg2').length == 0 && $('#DivImg1').length > 0 && elementos.length > 1) {
            try {
               
                elementos[1].parentNode.removeChild(elementos[1]);
            }
            catch(err){

            }
            
        }

        //hace un ciclo con todos los input que tengan esta etiqueta
        $document.on('input', 'input[type="range"], ' + selector, function (e) {
            valueOutput(e.target);
        });

        // Example functionality to demonstrate a value feedback
        function valueOutput(element) {
            //obtiene el div en el que se esta moviendo el slider
            bloque = element.parentNode.id.substring(6);
            var value = element.value;
            var output = element.parentNode.getElementsByTagName('output')[0] || element.parentNode.parentNode.getElementsByTagName('output')[0];
            //es un label donde dice el valor en el que esta el slider, se oculto el label y solo se visualiza en el input
            output[textContent] = value + "%";
            element.parentNode.getElementsByTagName('input')['text' + bloque].value = value;

            //Una vez que entra en esta funcion significa que inicializo la funcion de slid correctamente, por lo que se quita el intervalo
            clearInterval(Intervalo);
        }



        // Eventos de click de boton que cambia el valor del slider
        $document.on('click', '#DivImg1 button', function (e) {
            e.preventDefault();
            var $inputRange = $(selector, e.target.parentNode);
            //encuentra el textbox que corresponde al slider que se cambio por el valor del boton
            var value = $('input[type="number"]', e.target.parentNode)[0].value;

            //manda llamar la funcion de mover el slider con el valor que se pasa por parametro
            $inputRange.val(value).change();

            guardarHTML();
        });

        $document.on('click', '#DivImg2 button', function (e) {
            e.preventDefault();
            var $inputRange = $(selector, e.target.parentNode);
            var value = $('input[type="number"]', e.target.parentNode)[0].value;

            $inputRange.val(value).change();

            guardarHTML();
        });
        
        // Basic rangeslider initialization
        $element.rangeslider({

            // Deactivate the feature detection
            polyfill: false,

            // Callback function
            onInit: function () {
                valueOutput(this.$element[0]);

                //busca la imagen que se le va a cambiar el tamaño, como se agregaron como imagenes los botones de la alineacion de la imagen
                //se buscara la que corresponde en el arreglo de imagenes, si es plantilla de una imagen el valor 2 por la posicion del arreglo
                //si la plantilla tiene 2 imagenes y se esta inicializando el slider del bloque 2, esta en la posicion 5 del arreglo
                var image = this.$element[0].parentNode.parentNode.getElementsByClassName('download');
                if (image.length > 1) {
                    imgControl = image[1];
                } else {
                    imgControl = image[0];
                }
                //console.log(imgControl);
                //imgControl = this.$element[0].parentNode.parentNode.getElementsByTagName('img')[parseInt(bloque) + (bloque == 1 ? 2 : 5)];
              
                //Si no encuentra la imagen no hara nada
                if (imgControl != undefined) {

                    //se les pasa el valor de la imagen a los labels
                    document.getElementById("Alto" + bloque).innerText = imgControl.height;
                    document.getElementById("Ancho" + bloque).innerText = imgControl.width;

                    document.getElementById("OriginalH" + bloque).innerText = imgControl.height;
                    document.getElementById("OriginalW" + bloque).innerText = imgControl.width;

                    //se le quita el fondo gris a la imagen
                    $(this.$element[0].parentNode.parentNode.getElementsByClassName("divTemplateImagen")).css('background-image', 'none');
                }
                else {
                    this.$element[0].parentNode.parentNode.hidden = true;
                    return false;
                }

            },

            // Callback function
            // se ejecuta cuando se mueve el slider
            onSlide: function (position, value) {
                //console.log(this);
                valueOutput(this.$element[0]);

                var image = this.$element[0].parentNode.parentNode.getElementsByClassName('download');
                if (image.length > 1) {
                    imgControl = image[1];
                } else {
                    imgControl = image[0];
                }
                //imgControl = this.$element[0].parentNode.parentNode.getElementsByTagName('img')[parseInt(bloque) + (bloque == 1 ? 2 : 5)];
                //console.log('onSlide');
                //console.log('position: ' + position, 'value: ' + value);


                imgControl.width = value * (document.getElementById("OriginalW" + bloque).innerText / 100);
                document.getElementById("Alto" + bloque).innerText = imgControl.height;
                document.getElementById("Ancho" + bloque).innerText = imgControl.width;

                document.getElementById("text" + bloque).value = value
            },

            // Callback function
            //se llama esta funcion cuando se suelta el slider despues de moverlo
            onSlideEnd: function (position, value) {
                guardarHTML();
                //console.log('onSlideEnd');
                //console.log('position: ' + position, 'value: ' + value);
            }
        });

        //try{
        //   /* $('#js-rangeslider-2').remove();
        //    $('#js-rangeslider-3').remove();*/
        //    if($element.length > 2)
        //    $element[0].remove()
        //    $element[1].remove()
        //}catch(e){}

        // Mostrar correcatmente a la hora de crear nuevas plantillas y ocultar los slider que no tengan imagen asignada
        $('#DivImg1').removeAttr('hidden');
        $('.divIzq').removeAttr('hidden');

        $('#divHTML').removeAttr('hidden');
        $('.divTemplate').removeAttr('hidden');

        if ($('#Img1').children().length == 0) {
            $('#DivImg1').css('display', 'none');
        } else {
            $('#DivImg1').css('display', 'block');
        }

        if ($('#Img2').children().length == 0) {
            $('#DivImg2').css('display', 'none');
        } else {
            $('#DivImg2').css('display', 'block');
        }
       
    })();
};

function Alineacion(tipo) {
    var alig;
    if (tipo.id == "Acen") {
        alig = "center";
    }
    if (tipo.id == "Aizq") {
        alig = "left";
    }
    if (tipo.id == "Ader") {
        alig = "right";
    }

    //Se manda llamar a los div que tengan esta clase, si es el bloque de dos imagenes busca el nodo padre y si es del bloque 2, toma la posicion 1 del arreglo de div
    //$(tipo.parentNode.parentNode.getElementsByClassName("divTemplateImagen")[tipo.parentNode.id.substring(6) == 1 ? 0 : 1]).css('text-align', alig); se quito pq mueve toda la clase, queremos solo esa imagen
    tipo.parentNode.parentNode.getElementsByClassName("divTemplateImagen")[tipo.parentNode.id.substring(6) == 1 ? 0 : 1].style.textAlign = alig;
    guardarHTML();
}
