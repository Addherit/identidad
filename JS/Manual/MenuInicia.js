﻿$(document).ready(function () {
    try{
        var updateOutput = function (e) {
            var list = e.length ? e : $(e.target),
            output = list.data('output');

            if (window.JSON) {
          
                //Carga el texbox con el la lista de elementos y su poscision.
                $('#ContentPlaceHolder1_txtListaJSON').val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
             
                GuardarCambios(window.JSON.stringify(list.nestable('serialize')));
        
            } else {
           
                //la verdad no se para que es, pero por algo lo tengo que poner en null
                $('#tContentPlaceHolder1_txtListaJSON').val('null');
          
            }
        };
    
        //por cada change en la lista manda llamar la funcion que crea la lista (o guarda los elementos)
        $('#nestable3').nestable().on('change', updateOutput);
    } catch (exam) { }
});


function GuardarCambios(txtJSON) { //manda la lista de los elementos del menu que se modificaron.
   
    //Tomar el contenido del texBox y mandar a BD el JSON
    $.ajax({
        type: "POST",
        url: "Manual.aspx/EnviaDatos",
        data: "{ 'JSON' : '"+txtJSON.toString()+"'}",
       contentType: "application/json; charset=utf-8",
       dataType: "json",

       beforeSend: function () {
           $('#UpdateProgress1').fadeIn(100);
       },
        complete: function () {
            $('#UpdateProgress1').fadeOut("slow");

        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            alert(xhr.responseText);
            alert(thrownError);
        }
      
    });

}

//function CollapseItems() { //Esta funcion nos sirve para al ejecutar la cosa del menu, colapsa todos para que no tenga nada abierto.

//    //obttiene todos los elemetnos button con la propiedad data-action="expand"
  
//    $("button[data-action=collapse]").each(function (index) {

//        $(this).click();
      
//    });



//}