﻿// Obtener el popUp Contactanos----------------------------------------------------------------
var modal = document.getElementById('popUpContactanos');

// Obtener boton para abrir popUp
var btn = document.getElementById("btnContactanos");

// Obtner botones que cierran el popUp
var span = document.getElementsByClassName("close")[0];
var btnC = document.getElementById("btnCancelar");

// Funcion para mostrar el popUp 
//btn.onclick = function () {
//    modal.style.display = "block";
//}

// Funciones para cerrar el popUp
span.onclick = function () {
    $('.tooltipstered').tooltipster('hide');
    modal.style.display = "none";
}
//btnC.onclick = function () {
//    $('.tooltipstered').tooltipster('hide');
//    modal.style.display = "none";
//}

// Funcion para cerrar el popUp al dar click afuera
window.onclick = function (event) {
    if (event.target == modal) {
        $('.tooltipstered').tooltipster('hide');
        modal.style.display = "none";
    }
}//------------------------------------------------------------------------------------

// Obtener el popUp Mis sitios----------------------------------------------------------------
var modalMs = document.getElementById('popUpMisSitios');

// Obtener boton para abrir popUp
var btnMs = document.getElementById("btnmissitios");

// Obtner botones que cierran el popUp
var spanMs = document.getElementsByClassName("closeMs")[0];

// Funcion para mostrar el popUp 
btnMs.onclick = function () {
    modalMs.style.display = "block";
}

// Funciones para cerrar el popUp
spanMs.onclick = function () {
    $('.tooltipstered').tooltipster('hide');
    modalMs.style.display = "none";
}

// Funcion para cerrar el popUp al dar click afuera
window.onclick = function (event) {
    if (event.target == modalMs) {
        $('.tooltipstered').tooltipster('hide');
        modalMs.style.display = "none";
    }
}//------------------------------------------------------------------------------------

// Obtener el popUp Registro----------------------------------------------------------------
var modalR = document.getElementById('popUpRegistrate');

// Obtener boton para abrir popUp
var btnR = document.getElementById("btnRegistro");

// Obtner botones que cierran el popUp
var spanR = document.getElementsByClassName("closeR")[0];

// Funcion para mostrar el popUp 
btnR.onclick = function () {
        modalR.style.display = "block";
    
}

// Funciones para cerrar el popUp
spanR.onclick = function () {
    $('.tooltipstered').tooltipster('hide');
    modalR.style.display = "none";
}

// Funcion para cerrar el popUp al dar click afuera
window.onclick = function (event) {

    if (event.target == modalR) {
        $('.tooltipstered').tooltipster('hide');
        modalR.style.display = "none";
    }
}//------------------------------------------------------------------------------------

// Obtener el popUp Registro Plan----------------------------------------------------------------
var modalP = document.getElementById('popUpPlan');

// Obtener boton para abrir popUp
var btnP = document.getElementById("btnEnviarR");

// Obtner botones que cierran el popUp
var spanP = document.getElementsByClassName("closeP")[0];

// Funcion para mostrar el popUp 
btnP.onclick = function () {
    if ($("#formRegistro").valid() && document.getElementById("c5").checked==true)
    {
        modalP.style.display = "block";
    }
    
}


// Funciones para cerrar el popUp
spanP.onclick = function () { 
    $('.tooltipstered').tooltipster('hide');
    modalP.style.display = "none";
}

// Funcion para cerrar el popUp al dar click afuera
window.onclick = function (event) {
    if (event.target == modalP) {
        $('.tooltipstered').tooltipster('hide');
        modalP.style.display = "none";
    }
}//------------------------------------------------------------------------------------
// Obtener el popUp Registro Pagar----------------------------------------------------------------
var modalPagar = document.getElementById('popUpPagar');

// Obtener boton para abrir popUp
var btnPagarp1 = document.getElementById("btnComienza1");
var btnPagarp2 = document.getElementById("btnComienza2");
var btnPagarPYME = document.getElementById("btnComienzaPYME");
var btnPagarEmpresa = document.getElementById("btnComienzaEmpresa");
var btnPagarCorporativo = document.getElementById("btnCorporativo");
var btnPagarPremium = document.getElementById("btnPremium");

// Obtner botones que cierran el popUp
var spanPagar = document.getElementsByClassName("closePagar")[0];

// Funcion para mostrar el popUp 
btnPagarp1.onclick = function () {//Plan1
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$10.00";
    document.getElementById("lblanual").innerHTML = "$110.00";
}
btnPagarp2.onclick = function () {//Plan2
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$20.00";
    document.getElementById("lblanual").innerHTML = "$220.00";
}
btnPagarPYME.onclick = function () {//Plan PYME
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$49.00";
    document.getElementById("lblanual").innerHTML = "$539.00";
}
btnPagarEmpresa.onclick = function () {//Plan Empresa
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$139.00";
    document.getElementById("lblanual").innerHTML = "$1,529.00";
}
btnPagarCorporativo.onclick = function () {//Plan Corporativa
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$259.00";
    document.getElementById("lblanual").innerHTML = "$2,849.00";
}
btnPagarPremium.onclick = function () {//Plan Corporativa
    modalPagar.style.display = "block";
    document.getElementById("lblmes").innerHTML = "$599.00";
    document.getElementById("lblanual").innerHTML = "$6,589.00";
}

// Funciones para cerrar el popUp
spanPagar.onclick = function () {
    $('.tooltipstered').tooltipster('hide');
    modalPagar.style.display = "none";
}

// Funcion para cerrar el popUp al dar click afuera
window.onclick = function (event) {
    if (event.target == modalPagar) {
        $('.tooltipstered').tooltipster('hide');
        modalPagar.style.display = "none";
    }
}//------------------------------------------------------------------------------------
/*funciones para solo elegir un checkbox en popup de pago*/
function checkpersona() {
    document.getElementById("c2").checked = false;
    document.getElementById("txtRazonSocialdf").style.visibility = "hidden";
}
function checkempresa() {
    document.getElementById("c1").checked = false;
    document.getElementById("txtRazonSocialdf").style.visibility = "visible";
}
function checksvacios() {
    //if(document.getElementById("CantidadTotal").innerHTML == "$0.00") {
    if (document.getElementById("c3").checked == false && document.getElementById("c4").checked == false)
    {
        document.getElementById("CantidadTotal").innerHTML == "$0.00";
    }
}
function checkmes() {
    var cant
    document.getElementById("c4").checked = false;
    cant = document.getElementById("lblmes").innerHTML;
    document.getElementById("CantidadTotal").innerHTML = cant;
}
function checkanual() {
    var cant
    document.getElementById("c3").checked = false;
    cant = document.getElementById("lblanual").innerHTML;
    document.getElementById("CantidadTotal").innerHTML = cant;
}
//---------------------------------------------------
function recuperapass() {
    __doPostBack("recuperarPass", "");
}

function login() {
    ////alert("presionado");
    __doPostBack("login", "");
}

function ir() {
    //alert("ir");
    __doPostBack("ir", "");
}
function mandarcorreo() {
    //alert("mandar correo");
    __doPostBack("enviar", "");
}
function pagar() {
    //alert("pagar");
    __doPostBack("pagar", "");
}
function ddlEmpresas() {
    __doPostBack("empresas", "");
}
//----------Funciones elige plan
function muestraEmpresa() {
    document.getElementById("RegistroDisenador").style.visibility = "hidden";
    document.getElementById("RegistroEmpresa").style.visibility = "visible";
    //$(".RegistroEmpresa").css("visibility", "visible");
    //$(".RegistroDisenador").css("visibility", "hidden");
}
function muestraDiseno() {
    //$(".RegistroEmpresa").css("visibility", "hidden");
    //$(".RegistroDisenador").css("visibility", "visible");            
    document.getElementById("RegistroDisenador").style.visibility = "visible";
    document.getElementById("RegistroEmpresa").style.visibility = "hidden";

}

//-------------Funciones de muestreo para facturación
function selPais() {
    var pais = document.getElementById("ddlPaisdf").value;
    if (pais == "México") {
        document.getElementById("divRS").className = "col-sm-6";
        document.getElementById("rfc").style.visibility = "visible";
        document.getElementById("rfc").style.display = "block";
        document.getElementById("rfc").required = true;
    }
    else
    {
        document.getElementById("rfc").style.visibility = "hidden";
        document.getElementById("rfc").style.display = "none";
        document.getElementById("divRS").className = "col-sm-12";
        document.getElementById("rfc").required = false;
    }
        
}

//

