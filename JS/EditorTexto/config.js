/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
    // config.language = 'fr';
    //config.basicEntities = true;
    //config.uiColor = '#AADC6E';

    config.toolbar = 'MyToolbar';

    config.toolbar_MyToolbar =
	[
		{ name: 'basicstyles', items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat'] },
        { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
		{ name: 'insert', items: ['HorizontalRule','-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ] }, { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo'] },
        '/',		
		{ name: 'tools', items: ['Maximize', '-'] }
	];

    config.title = false;
};
