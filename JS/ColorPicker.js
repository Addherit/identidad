﻿function ValidaHexadecimal(cadena) {
    
    var RegExPattern = /^([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;
   
    if ((cadena.match(RegExPattern)) && (cadena != '')) {
       
        return cadena;

    } else {
        return '0';
        //alert('false');
    }
}

function ValidaCaracteresHex(cadena){

    var RegExPattern = /^([A-Fa-f0-9]*)$/;
   
    if ((cadena.match(RegExPattern)) && (cadena!= '')) {
        //((alert('true');
       
         return cadena;

    } else {

        var newStr = cadena.substring(0, cadena.length - 1);
        return newStr;
        //alert('false');

    }
}

function colorChanged(sender) {
    sender.get_element().blur();
    sender.get_element().style.color = '#' + sender.get_selectedColor();
    sender.get_element().style.backgroundColor = '#' + sender.get_selectedColor();

}