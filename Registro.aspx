﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Registro.aspx.cs" Inherits="Identidad.Registro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <title>Conekta - Primero Cargo</title>
   <link href="CSS/prueba.css" rel="stylesheet" type="text/css" />
  <!-- Incluir jQuery -->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.3/jquery.min.js"></script>

  <!-- Incluir Conekta.js -->
  <script 
    type="text/javascript"
    data-conekta-public-key="key_KJysdbf6PotS2ut2"
    src="https://conektaapi.s3.amazonaws.com/v0.3.2/js/conekta.js"></script>
 
  <script type="text/javascript">
      var description = "Concepto";
      var amount = 10000;
      var name = "Beddy Landey";

      jQuery(function ($) {
          var conektaSuccessResponseHandler = function (token) {

              var $form = $('#payment-form');
              var card_token = token.id;
              // Insert the token_id into the form
              $form.append($('<input type="hidden" name="card_token"/>').val(card_token));
              // Submit the form data to the server
              //$form.get(0).submit();
              console.log(card_token);
              var sjson= "{\"description\": \" " + description + " \",\"amount\": " + amount + ",\"currency\":\"MXN\",\"card\": \""+ card_token+"\",\"details\": {\"name\": \""+ name +"\",\"phone\": \"00000000000\",\"email\": \"beddy@beduardo.com\",\"line_items\": [{\"name\": \"Box of Cohiba S1s\",\"description\": \"Imported From Mex.\",\"unit_price\": 20000,\"quantity\": 1}] } }";
              __doPostBack('Pagar_Click', sjson);
          };
          var conektaErrorResponseHandler = function (response) {
              var $form = $('#payment-form');
              // Show the errors on the form
              $form.find('.payment-errors').text(response.message);
              $form.find('button').prop('disabled', false);
          };
          $('#payment-form').submit(function (event) {
              var $form = $(this);
              // Disable the submit button to prevent repeated clicks
              $form.find('button').prop('disabled', true);
              Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
              // Prevent the form from submitting with the default action
              return false;
          });
      });
  </script>
</head>

<body>

    <form id="form1" runat="server">

 
    </form>

    <form action="" method="POST" id="payment-form">
        <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        


    <span class="payment-errors"></span>

    <div class="form-row hidden">
      <label>
        <span>Monto</span>
        <input type="text" size="12"/>
      </label>
    </div>

    <div class="form-row hidden">
      <label>
        <span>Descripción</span>
         <input type="text" size="40" value="Compra"/>
      </label>
    </div>

    <div class="form-row hidden">
      <label>
        <span>Moneda</span>
        <input type="text" size="3" value="MXN"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>Tarjetahabiente</span>
        <input type="text" size="20" data-conekta="card[name]"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>Número</span>
        <input type="text" size="20" data-conekta="card[number]"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>CVC</span>
        <input type="text" size="4" data-conekta="card[cvc]"/>
      </label>
    </div>

    <div class="form-row">
      <label>
        <span>Expiración (MM/YYYY)</span>
        <input type="text" size="2" data-conekta="card[exp_month]"/>
      </label>
      <span> / </span>
      <input type="text" size="4" data-conekta="card[exp_year]"/>
    </div>

    <button type="submit">Completar Pago</button>
  </form>

    
</body>
</html>
