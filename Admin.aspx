﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="~/Admin.aspx.cs" Inherits="Identidad.Admin" meta:resourcekey="Admin" %>
<%@ OutputCache NoStore="true" Duration="1" VaryByParam="*"   %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta name="google-site-verification" content="xQbATgheTw45Jgd_XcHLAGm6MvXWvM8XVAZhj03xkF4" /> 
    
    <title>Identidad.com | Admin</title>
    <link href="CSS/Pages/Default.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Site.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Design.css" rel="stylesheet" type="text/css" />
    <link href='/Images/Site/ID.ico' rel='shortcut icon' type='image/x-icon'/>
    <meta name="application-name" content="Identidad.com"/>
    <meta name="application-tooltip" content="Identidad.com"/>
    <meta name="application-bavbutton-color" content="Blue"/>
    <link href="/Images/Site/ID.ico" rel="icon" type="image/ico"/>
	<link href="/Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="apple-touch-icon-precomposed" href="/Images/Site/ID.ico"/>
	<link rel="apple-touch-icon" href="/Images/Site/ID.ico"/>
    <link rel="apple-touch-startup-image" href="/Images/Site/ID.ico" />
    <meta name="apple-mobile-web-app-title" content="Identidad.com"/>
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='apple-touch-fullscreen' content='yes'/>
    <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
    <link rel="icon" href="/Images/Site/ID.ico" />
	<link rel="shortcut icon" href="/Images/Site/ID.ico" />
    <meta charset="utf-8"/>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-56195706-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>
    <form id="form1" runat="server" defaultbutton="btnIniciarSesion" defaultfocus="txtUsuario">

        <div class="divFondoDefault"  style="background-image:url('Images/Site/IDENT_Home_Grid.png')" >
            <asp:ScriptManager ID="sm" runat="server"></asp:ScriptManager>
            <div id="divContenido">
                <asp:UpdatePanel ID="upd1" runat="server">
                    <ContentTemplate> 
                        <div id="divBoton">
                            <asp:Button ID="btnIngresa" runat="server" CssClass="buttons colorVerde" Text="Ingreso Admin" style="width:225px; margin-right:12px;" OnClick="lkbIniciarSesion_Click" />
                            <asp:LinkButton ID="lkbIniciarSesion" runat="server" style="color:white!important; font-weight:bolder!important;" meta:resourcekey="lkbIniciarSesion" OnClick="lkbIniciarSesion_Click" Visible="false"></asp:LinkButton>
                           
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>

        <asp:UpdatePanel ID="upd2" runat="server">
            <ContentTemplate>
                <panel id="pnlIniciarSesion" runat="server" style="display: none">
                    <div class="divTabla" style="height:330px;">
                        <div>
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTitulo" runat="server" meta:resourcekey="lblTitulo"></asp:Label> 
                            </div>
                            <div class="divTablaTituloImagen">
                                <img id="imgbtnC" src="../Images/Manual/cerrar.gif" alt="Cerrar" onclick="$get('<%= btnCancelar.ClientID %>').click();" />
                            </div>
                        </div>
                    <hr/>
                    <br />
                    <%-- Divs para los texbox --%>
                    <div class="divTBODY">
                        <div class="divTR">
                            <div class="divTDIzq"> 
                                <asp:Label ID="lblUsuario" runat="server" meta:resourcekey="lblUsuario"></asp:Label>
                            </div>
                            <div class="divTDDerechoInput"> 
                               
                                    <asp:TextBox ID="txtUsuario" CssClass="TextBox" runat="server" meta:resourcekey="txtUsuario" onblur="onLeave(this)" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$"></asp:TextBox>
                            </div>
                        </div>

                        <div class="divTR">

                            <div class="divTDIzq"> 
                                <asp:Label ID="lblContraseña" runat="server" meta:resourcekey="lblContraseña"></asp:Label>
                            </div>

                            <div class="divTDDerechoInput"> 
                                <asp:TextBox ID="txtContraseña" CssClass="TextBox" TextMode="Password" runat="server" onblur="onLeave(this)"></asp:TextBox>
                            </div>

                        </div>

                        <div class="divTR">
                                
                            <div class="divTDIzq"></div>

                            <div class="divTDDerechoInput"> 
                                <asp:LinkButton ID="lkbRecuperarContraseña" runat="server" meta:resourcekey="lkbRecuperarContraseña" OnClick="lkbRecuperarContraseña_Click"></asp:LinkButton>
                            </div>

                        </div>

                        <div class="divTR">
                        <div class="divTDIzq"> 
                              
                        </div>
                            <div class="divTDDerechoInput"> 
                        <div id="divValidacion"  runat="server"> 
                            <asp:Label ID="lblValidacion" class="divValidacion" runat="server" ></asp:Label>
                        </div>
                                </div>
                                  
                    </div>

                        <div class="divTR">
                            <div class="divTDIzq"></div>
                            <div class="divTDDerechoButtons" > 
                                <asp:Button id="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" meta:resourcekey="btnCancelar" runat="server" autopostback="true" OnClick="btnCancelar_Click"></asp:Button>
                                <asp:Button id="btnIniciarSesion" class="buttons colorVerde" meta:resourcekey="btnIniciarSesion" runat="server" autopostback="true" OnClick="btnIniciarSesion_Click"></asp:Button>
                            </div>
                        </div>

                    </div>
                    </div>
                </panel>

                <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                <Ajax:ModalPopupExtender ID="ppIniciarSesion" TargetControlID="Label1" runat="server" PopupControlID="pnlIniciarSesion" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll" >
                    <Animations>
                        <OnShown>
                            <FadeIn Duration=".30" Fps="48" />                
                        </OnShown>
                    </Animations>
                </Ajax:ModalPopupExtender>


                
               

            </ContentTemplate>
        </asp:UpdatePanel>

    </form>

    <script src="JS/Site.js" type="text/javascript"></script>
    <script src="JS/jquery-1.8.3.js" type="text/javascript"></script>
    <script type="text/javascript">

        onLoad(document.getElementById('lkbIniciarSesion'));


        function isValidEmail(mail) {
            var Correo = $(mail).val();
            var nombre = $("#txtNombre").val();
            var mail = $("#txtCorreo").val();
            var mensaje = $("#txtComentarios").val();   
            
            var band = (/^\w+([\.\+\-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(mail));
            //if (band == "false") {
            //    $("#txtCorreo").addClass(" TextBoxError");
               
            //}
            //else {
            //    $("#txtCorreo").removeClass("TextBoxError").addClass(" DropDownList TextBox1");
            //}
            if (nombre == "" || mensaje == "" || band === false)
            {
                
                $("#lblmessage").show();
            }
            else
            {
                __doPostBack("validaEnviar", "");
            }
            
        }

    </script>


</body>
</html>