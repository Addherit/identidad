﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login-registro.aspx.cs" Inherits="Identidad.site.login_registro" %>

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/template-identidad-pag.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Manual de identidad en linea. Tu manual de marca online para ti y tu equipo. - identidad.com</title>
    <!-- InstanceEndEditable -->
    <meta name="description" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/font-awesome.css" rel="stylesheet"/>
    <link href="css/flaticon.css" rel="stylesheet"/>
    <link href="css/fopen-sans.css" rel="stylesheet"/>
    <link href="css/raleway-webfont.css" rel="stylesheet"/>
    <link href="css/lato.css" rel="stylesheet"/>
    <link href="css/idangerous.swiper.css" rel="stylesheet"/>
    <link href="css/magnific-popup.css" rel="stylesheet"/>
    <link href="img/favicon.ico" rel="shortcut icon"/>
    <link href="css/modalstyle.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet"/>
	<link href="css/responsive.css" rel="stylesheet"/>
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->

       <script>

           function myFunction() {
               //$("#myModal").modal();
               __doPostBack('login');
           }
           function ir() {
               var ddlvar = document.getElementById("ddlmissitios").value;
               __doPostBack('ir',ddlvar);
           }
           function recuperar() {
               __doPostBack('recuperar');
           }
           function Registrar() {
               __doPostBack('registro');
           }

    </script>
  </head>
  <body>


	<!-- 
	*******************
      	HEADER    
	****************** 
	-->
    
    
	<!--<div class="wpc-main-header">
	  <div class="container no-padding-sm">
	    <div class="row">
	      <div class="col-sm-6 no-padding">
	      <<div class="soc-icons"> <a href=""><i class="faTop fa-facebook"></i></a> <a href=""><i class="faTop fa-twitter"></i></a> <a href=""><i class="faTop fa-linkedin"></i></a> </div>
          </div>
	      <div class="col-sm-6 no-padding-right">
	        <section class="header-info">
	          <h6> <a href="../login-registro.aspx"><i class="fa"><img src="../img/icon-2.png" alt="icon"></i>e</a> <a href="#"><i class="fa fa-location-arrow"></i>Soporte</a> <a href="tel:+527773138466"><i class="fa fa-mobile"></i>6</a> <a href="mailto:info@identidad.com"><i class="fa fa-envelope"></i>info@identidad.com</a> </h6>
            </section>
          </div>
        </div>
      </div>
    </div>-->
    
    
    
	<!--
	*******************
       LOGIN   
	****************** 
	-->
	
	<%--<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<!-- form itself -->
		<form action="#" id="test-form" class="mfp-hide white-popup-block login-form">
		
			<div class="row">
				<div class="col-sm-6 col-sm-offset-1">
					<div class="wrapper">
						<h3 class="title">Log in</h3>
						<div class="subtitle">Please fill in your basic info:</div>
						<div><input type="text" name="username" placeholder="Username" class="field" autocomplete="off" required></div>
						<div><input type="password" name="password" placeholder="Password" class="field" autocomplete="off" required></div>
					</div>
				</div>
				<div class="col-sm-4 text-xs-center">
					<label class="label-form"><input type="checkbox" class="btn-check">Remember me</label>
					<input type="submit" class="wpc-btn" value="log in">
				</div>
			</div>
			
		</form>
			</div>
		</div>
	</div>--%>
	


	
<!-- 
	*******************
      NAV MENU    
	****************** 
	-->


	<div class="wpc-menu-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 no-padding">
					<a href="index.aspx"><img src="img/ID_FirmaBETA_3.png" alt="logo" class="logo-menu"></a>
				</div>
				<div class="col-sm-9 col-lg-7 col-lg-offset-2 no-padding">
					<nav class="wpc-nav-menu">
						<button class="menu-toggle">
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    	</button>
					    <ul class="main-menu">
                             <!--
					        <li class="menu-item">
					        	<a href="manual-identidad-online.aspx">La plataforma</a>
				        	</li>
					        <li class="menu-item">
					        	<a href="guia-marca-online-precios.aspx">Los planes</a>
				        	</li>
					        <li class="menu-item">
					            <a href="login-registro.aspx">Ingresa</a>
					        </li>
					        <li class="menu-item">
					            <a href="ayuda-faq.aspx">Ayuda</a>
					        </li>
                                 -->
					        <li class="menu-item"><a href="contacto.aspx">Contacto</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
	
	<!-- InstanceBeginEditable name="Main Content Site" -->
	<div id="Editable Content">
	<!-- 
	*******************
		TOP BANNER
	****************** 
	-->

	<div class="wpc-top-header overlay img-bg">
		<img src="img/id_login.jpg" alt="banner" class="hidden">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="wrapper">
						<div class="heading">Ingresa</div>
	<!--					<div class="subheding">
							<a href="index.aspx" class="link">Inicio</a><span class="round"></span>
							<a href="login-registro.aspx" class="link active">Registro</a>
						</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 
	*******************
      REGISTER  
	*******************
	-->
<form runat="server">
    <div class="wpc-register">
		<div class="container no-padding-sm">
			<div class="row">
                 <div class="col-md-3">
                </div>
				<div class="col-md-6">
					<div  class="wpc-comments-form">
						<div class="title-form">Ingresa</div>
						<div class="subtitle">Si ya cuentas con una cuenta, accede aquí:</div>
						<input id="txtcorreoIngreso" type="text"  placeholder="Correo electrónico" runat="server" class="input"/>
						<input id="txtpassIngreso" type="password"  placeholder="Contraseña" runat="server" autocomplete="off" class="input password"/>
						<label class="label-form"><input id="chkRemember" type="checkbox" class="btn-check" runat="server">Recúerdame</label>
                        <br class="visible-xs-mobile">
						<a href="#" class="link" onclick="recuperar()" >Olvidé mi contraseña</a>
                        
					<%--</form>
                     <form >--%>
                        <button id="btnLogin" class="wpc-btn" runat="server" onclick="myFunction()" >Ingresa</button>
                        <label id="lblvalidacionRP" class="label-form" runat="server" visible="false" style="color:red;"></label><br />
                        <label id="lblvalidacionL" runat="server" visible="false"></label>
    </div>
	</div>
				<!--<div class="col-md-6">
					<div class="wpc-comments-form">
						<div class="title-form">Regístrate para una nueva cuenta</div>
						<div class="subtitle">Es sencillo, solamente te tomará algunos momentos.</div>
						<input type="text" id="txtnombre" runat="server" placeholder="Nombre" class="input" />
                        <input type="text" id="txtapellidoP" runat="server" placeholder="Apellidos" class="input" />
                        <input type="email" id="txtcorreo" runat="server" placeholder="Correo electrónico" class="input email" />
                        <input type="email" id="txtcorreoconfirma" runat="server" placeholder="Confirma correo electrónico" class="input email" />
						<input type="password" id="txtpass" runat="server" placeholder="Contraseña" autocomplete="off" class="input" />
						<input type="password" id="txttpassconfirma" runat="server" placeholder="Confirmar contraseña" autocomplete="off" class="input"/>
						<label class="label-form"><input type="checkbox" id="chkPromos" class="btn-check" runat="server" checked/>¡Gracias! Me encantará recibir buena vibra y promociones de ustedes.</label><br class="visible-xs-mobile"/>
                       <!-- <div class="subtitle">Elige el dominio para tu manual de identidad en identidad.com</div>
                        <input type="text" name="domain" placeholder="elije tu dominio" class="input" required>
                        <input type="checkbox" name="terminos" value="Acepto"><a href="terminos-condiciones.aspx">&nbsp;Acepto términos y condiciones de uso.</a><br>-->
						<!--
                        <div class="text-right text-xs-left">
                            <label id="lblvalidacionR" runat="server" visible="false" style="color:red;"></label>
							<%--<input type="submit" class="wpc-btn" value="Regístrate">--%>
                            <button id="btnRegistro" class="wpc-btn" runat="server" onclick="Registrar()" >Regístrate</button>
						</div>
					</div>
				</div>-->

			</div>
		</div>
	</div>
    </form>
    </div>
    <!--editable content div-->
	<!-- InstanceEndEditable -->
    
    
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
    
    
    
    
    <!-- 
	*******************
		SUBSCRIBE
	****************** 
	-->

	
    
<!--	<div class="wpc-subcribe">	
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-md-offset-1 col-xs-12">
					<h1 class="heading" for="mce-EMAIL">Suscríbete a nuestro newsletter</h1>
				</div>
			</div>
			<div class="row" id="mc_embed_signup">
				<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-7 col-md-offset-1 col-sm-7 col-xs-12" id="mc_embed_signup_scroll">
						<input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Correo electrónico" class="email-input" required>
					</div>
					<div class="col-sm-4 col-xs-12  text-xs-center">
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="wpc-btn">
					</div>
				</form>
			</div>
		</div>
	</div>-->
    
<!-- 
<div id="mc_embed_signup">
<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>*/-->


	<!-- 
	*******************
		FOOTER
	****************** 
	-->

	<footer class="wpc-footer">
        <div class="container no-padding-sm">
            <div class="row">
               
                <div class="col-md-4 col-sm-6">
                     <!--
                    <a href="index.aspx" style="margin-right: 45px;"><img src="img/id_icono_40x40px.png" alt="logo"></a>
                    <a href="https://www.facebook.com/identidad.com.manual" target="_blank"><img src="img/facebook.jpg" alt="logo" style="width:45px;"></a>
                    <a href="https://twitter.com/identidadcom" target="_blank"><img src="img/twitter.jpg" alt="logo" style="width:45px;"></a>
 <!--                   <p class="contact"><i class="flaticon-location"></i></p> 
                    <p class="contact"><a href=""><i class="flaticon-letter"></i></a></p> 
                    <p class="contact"><a href="tel:"><i class="flaticon-technology-1"></i></a></p>-->
                </div>
                <div class="col-md-4 col-sm-6">
                	&nbsp;
           <!--         <div class="heading">Nuestras páginas</div>
                    <ul class="footer-menu classic">
                        <li class="menu-item"><a href="../index.aspx">Inicio</a> </li>                             
                        <li class="menu-item"><a href="../manual-identidad-online.aspx">La plataforma</a> </li>
                        <li class="menu-item"><a href="../guia-marca-online-precios.aspx">Los precios</a></li>
                        <li class="menu-item"><a href="../login-registro.aspx">Accede / Login</a></li>
                        <li class="menu-item"><a href="../contacto.aspx">Contacto</a></li>
                    </ul>-->
                </div>
                <div class="col-md-4 col-sm-12 text-xs-center text-sm-center">
                   	<div class="footer-info">
  Después de crear una marca hay que saber administrarla óptimamente.
                    </div>
                &nbsp;
         <!--           <div class="heading">Contacto</div>
                    <div class="soc-icons">
                        <a href=""><i class="fa fa-facebook"></i> Facebook</a>
                        <a href=""><i class="fa fa-twitter"></i> Twitter</a> 
                        <a href=""><i class="fa fa-linkedin"></i> Linkedin</a>                      
                    </div>    -->              
                </div>
            </div>          
        </div>
             <!--
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 no-padding-sm">
                    <div class="footer-bottom">
                    <div class="container no-padding-sm">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 text-center text-lg-left">
                                <div class="copyrigh">Todos los derechos reservados. Identidad.com - 2017</div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <ul class="footer-menu">
                                    <li class="menu-item"><a href="aviso-privacidad.aspx">Aviso de privacidad</a></li>                             
                                    <li class="menu-item"><a href="terminos-condiciones.aspx">Términos y condiciones</a></li>                             
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                  -->
        </div>
    </footer>



<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <div class="modal-body">
        <%--<span class="close">×</span>--%>
      <h3 id="tituloMs">Mis sitios</h3>
      <hr />
      <div id="Adonde">
          <h3 id="lblSitios">¿Cuál quieres visitar?</h3>   <br />   
          <div id="lista-btn">
          <select id="ddlmissitios" class="txtDatosPagoG"  runat="server" required="required"></select>
      
      <input type="button" id="btnIr" runat="server" onclick="ir();" class="wpc-btn btnmistios"  value="Ir"/>
              </div>
        </div>
      <br />
    </div>
    
  </div>

</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.circliful.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scripts.js"></script>

	<!--INICIA CÓDIGO DE GOOGLE ANALYTICS-->
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78976181-1', 'auto');
        ga('send', 'pageview');

	</script>
    
    <!--TERMINA CÓDIGO DE GOOGLE ANALYTICS-->
    <script>
        
        $(window).load(function () {
            sBandera = '<%= Session["bandera"].ToString()%>';
            //alert(sBandera);
            // Get the modal
            var modal = document.getElementById('myModal');

            if (sBandera == "1") {
                modal.style.display = "block";
            }
            // Get the button that opens the modal
            //var btn = document.getElementById("myBtn");

            // Get the <span> element that closes the modal
            //var span = document.getElementsByClassName("close");

            // When the user clicks the button, open the modal
            //btn.onclick = function () {
            //    modal.style.display = "block";
            //}

            // When the user clicks on <span> (x), close the modal
            //span.onclick = function () {
            //    modal.style.display = "none";
            //}

            // When the user clicks anywhere outside of the modal, close it
            //window.onclick = function (event) {
            //    if (event.target == modal) {
            //        modal.style.display = "none";
            //    }
            //}
        });
    </script>

    <script>
        navigator.sayswho= (function(){
            var ua= navigator.userAgent, tem, 
            M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return {name:'IE',version:(tem[1] || '')};
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return {name:tem[1].replace('OPR', 'Opera'),version:tem[2]};
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return {name:M[0],version:M[1]};
        })();

        console.log(navigator.sayswho); //Object { name: "Firefox", version: "42" }

        if (navigator.sayswho.name == 'IE' || navigator.sayswho.name == 'Firefox') {
            document.getElementById('chkRemember').style.visibility = 'visible';
            document.getElementById('chkPromos').style.visibility = 'visible';
        }
        else {
            document.getElementById('chkRemember').style.visibility = 'hidden';
            document.getElementById('chkPromos').style.visibility = 'hidden';
        }
    </script>
  </body>
<!-- InstanceEnd --></html>