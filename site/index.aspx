﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Identidad.site.index" %>

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/template-identidad-pag.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Manual de identidad en linea. Tu manual de marca online para ti y tu equipo. - identidad.com</title>
    <!-- InstanceEndEditable -->
    <meta name="description" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/fopen-sans.css" rel="stylesheet">
    <link href="css/raleway-webfont.css" rel="stylesheet">
    <link href="css/lato.css" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="img/favicon.ico" rel="shortcut icon">
    <link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
  </head>
  <body>


	<!-- 
	*******************
      	HEADER    
	****************** 
	-->
    
    
	<!--<div class="wpc-main-header">
	  <div class="container no-padding-sm">
	    <div class="row">
	      <div class="col-sm-6 no-padding">
	      <<div class="soc-icons"> <a href=""><i class="faTop fa-facebook"></i></a> <a href=""><i class="faTop fa-twitter"></i></a> <a href=""><i class="faTop fa-linkedin"></i></a> </div>
          </div>
	      <div class="col-sm-6 no-padding-right">
	        <section class="header-info">
	          <h6> <a href="../login-registro.aspx"><i class="fa"><img src="../img/icon-2.png" alt="icon"></i>e</a> <a href="#"><i class="fa fa-location-arrow"></i>Soporte</a> <a href="tel:+527773138466"><i class="fa fa-mobile"></i>6</a> <a href="mailto:info@identidad.com"><i class="fa fa-envelope"></i>info@identidad.com</a> </h6>
            </section>
          </div>
        </div>
      </div>
    </div>-->
    
    
    
	<!--
	*******************
       LOGIN   
	****************** 
	-->
	
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<!-- form itself -->
		<form action="#" id="test-form" class="mfp-hide white-popup-block login-form">
		
			<div class="row">
				<div class="col-sm-6 col-sm-offset-1">
					<div class="wrapper">
						<h3 class="title">Log in</h3>
						<div class="subtitle">Please fill in your basic info:</div>
						<div><input type="text" name="username" placeholder="Username" class="field" autocomplete="off" required></div>
						<div><input type="password" name="password" placeholder="Password" class="field" autocomplete="off" required></div>
					</div>
				</div>
				<div class="col-sm-4 text-xs-center">
					<label class="label-form"><input type="checkbox" class="btn-check">Remember me</label>
					<input type="submit" class="wpc-btn" value="log in">
				</div>
			</div>
			
		</form>
			</div>
		</div>
	</div>


	
<!-- 
	*******************
      NAV MENU    
	****************** 
	-->


	<div class="wpc-menu-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 no-padding">
					<a href="index.aspx"><img src="img/ID_FirmaBETA_3.png" alt="logo" class="logo-menu"></a>
				</div>
				<div class="col-sm-9 col-lg-7 col-lg-offset-2 no-padding">
					<nav class="wpc-nav-menu">
						<button class="menu-toggle">
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    	</button>
					    <ul class="main-menu">
					        <li class="menu-item">
					        	<a href="manual-identidad-online.aspx">La plataforma</a>
				        	</li>
					        <li class="menu-item">
					        	<a href="guia-marca-online-precios.aspx">Los planes</a>
				        	</li>
					        <li class="menu-item">
					            <a href="login-registro.aspx">Ingresa</a>
					        </li>
					        <li class="menu-item">
					            <a href="ayuda-faq.aspx">Ayuda</a>
					        </li>
					        <li class="menu-item"><a href="contacto.aspx">Contacto</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
	
	<!-- InstanceBeginEditable name="Main Content Site" -->
	<div id="Editable Content">
	
    
    <div class="container-fluid no-padding">
		<div class="row no-margin">
			<div class="col-sm-12 no-padding text-xs-center text-sm-center">
				<div class="wpc-top-banner overlay img-bg">
					<img src="img/manual-identidad-home.jpg" alt="banner" class="hidden">
					<div class="container no-padding-sm">
						<div class="heading">¡Bienvenido<br class="hidden-xs"> al nuevo hogar<br class="hidden-xs"> de tu marca!</div>
						<h3 class="subheading">Finalmente una plataforma colaborativa<br class="hidden-xs"> para implantar y administrar tus programas<br class="hidden-xs"> vivos de identidad corporativa.</h3>
						<a href="manual-identidad-online.aspx" class="wpc-btn">Conoce la plataforma</a>
					</div>
				</div>
			</div>
		</div>
	</div>
  

	<div class="wpc-services classic">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h3 class="section-subheading text-center">
						Identidad.com resuelve las necesidades de diseñadores, <br class="hidden"> clientes y otros colaboradores al implementar y administrar<br class="hidden"> programas de identidad.
					</h3>
				</div>
			</div>
		</div>
    </div>
        

 
 	<div class="wpc-host-plans">
		<div class="container no-padding-sm">
 			<div class="row">
				<div class="col-md-5 col-md-push-6">
					<div class="wrap-img text-center">
						<img src="img/id-marca-online.png" alt="guia de marca online" class="img img-responsive none"/>
					</div>
				</div>
				<div class="col-md-7 col-md-pull-5 no-padding-sm text-xs-center text-sm-center">
					<div class="heading">
						Las marcas están vivas:
					</div>
					<div class="info-group">
						<span class="text">Respiran y palpitan todos los días y sus expresiones se enriquecen y adaptan cada vez más rápido. Los <span class="bold">manuales de identidad impresos o en PDF son cosa del pasado</span>, hoy en día requerimos de herramientas dinámicas, que crezcan y se adapten a las necesidades constantes, y que inviten a la colaboración a todos los actores importantes: diseñadores, directivos y proveedores.</span>
					</div>
				
				</div>							
			</div>    
        </div>
	 </div>
 <!-- %%%%%%%%%%%%%%% DISEÑADOR %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
 	<div class="wpc-host-plans-blue">
        <div class="container no-padding-sm">
             <div class="row">
                <div class="col-sm-12">
                    <div class="heading-section text-left">
                        Si eres <span>diseñador</span>
                    </div>
               </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
           		  <div class="info-group">
						<p class="home"><span class="bold">Identidad.com</span> fue creada por diseñadores especializados en programas de identidad –sencillos o complejos– para facilitar la creación de manuales y la administración de todos los recursos de una identidad: archivos de logos, tipografías, imágenes y todas las aplicaciones que puedas imaginar.</p>

<p class="home">En identidad.com podrás crear el manual a partir de un template, mismo que puedes modificar a tu antojo con tu propio estilo gráfico, quitando y añadiendo capítulos, editando textos y ubicando imágenes. </p>

				  <a href="manual-identidad-online.aspx" class="wpc-btn-home">Conoce la plataforma</a>
             	  </div>
                              </div>

               <div class="col-sm-6">
				<h4 class="home">Algunos beneficios</h4>               
               	<ul>
                	<li>Sé dueño de la dirección más memorable: <span class="italic">tumarca</span>.identidad.com </li>
                    <li>Arma un manual de identidad completo y compártelo en cuestión de horas</li>
                    <li>Actualiza y edita con unos cuantos clics</li>
                    <li>Organiza todos los archivos para la correcta implantación de tu identidad</li>
                    <li>Olvídate de los clásicos reenvíos de archivos y PDFs</li>
                </ul>
               </div>
       		 </div>
             <div class="row">
             	<div class="col-sm-12">
                	<div class="wrap-img text-center">
						<img src="img/id-guia-marca-online.png" alt="guia de marca en linea" class="img img-responsive"/>
                    </div>
                </div>
             </div>
    	</div>
	</div>
     <!-- %%%%%%%%%%%%%%% BRAND MANAGER  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%-->
   	<div class="wpc-host-plans-blue">
        <div class="container no-padding-sm">
             <div class="row">
                <div class="col-sm-12">
                    <div class="heading-section text-left">
                        Si eres <span class="italic">brand manager</span>
                    </div>
               </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
           		  <div class="info-group">
						<p class="home">Olvídate de los manuales en PDF multiplicados, las carpetas de logos y aplicaciones que circulan por decenas de computadoras sin control, y la difícil tarea de actualizar y distribuir nuevas versiones. </p>

<p class="home">Con identidad.com todos tus recursos estarán centralizados, respaldados y actualizados en la nube. Puedes dar acceso a quienes tú indiques con privilegios puntuales para descargar archivos específicos e invitar a proveedores a trabajar en el desarrollo de tu identidad. </p>

				  <a href="manual-identidad-online.aspx" class="wpc-btn-home">Conoce la plataforma</a>
             	  </div>
                              </div>

               <div class="col-sm-6">
				<h4 class="home">Algunos beneficios</h4>               
               	<ul>
                	<li>Archivos accesibles en la nube a toda hora.</li>
                    <li>Accesible desde diversas plataformas</li>
                    <li>Encuentra rápidamente el archivo correcto</li>
                    <li>Usa la nube para organizar y respaldar todos tus recursos</li>
                    <li>Invita a colaborar a colegas y proveedores por grupos y privilegios</li>
                </ul>
               </div>
       		 </div>
             <div class="row">
             	<div class="col-sm-12">
                	<div class="wrap-img text-center">
						<img src="img/IDE_manual_identidad_mexico.png" class="img img-responsive" alt="manual de marca en mexico"/>
                    </div>
                </div>
             </div>
    	</div>
	</div>  
    
	<!-- 
	*******************
      	PRECIOS 
	****************** 
	-->

	<div class="wpc-pricing">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="heading">Los planes</div>
					<h3 class="subheading">Accesibles para ti y lo que necesites</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3 no-padding">
					<div class="wrapper first">
						<div class="title">
						Prueba</div>
						<div class="price"><sup>$</sup>0</div>
						<div class="period">Sin cargo</div>
						<ul class="list-info">
							<li><sub></sub>500MB de espacio</li>
							<li>5 usuarios</li>
							<li style="font-family: opensans bold;">2 meses de prueba</li>
						</ul>
					<!--	<a href="" class="wpc-btn">Empieza</a>-->
					</div>
				</div> 
                <div class="col-sm-3 no-padding">
					<div class="wrapper first">
						<div class="title">
						Básico</div>
						<div class="price"><sup>$</sup>19</div>
						<div class="period">USD - Dólares al mes</div>
						<ul class="list-info">
							<li><sub></sub>500MB de espacio</li>
							<li>5 usuarios</li>
							<li style="font-family: opensans bold;">Pago anual: $228 USD</li>
						</ul>
			
					</div>
				</div> 
				<div class="col-sm-3 no-padding">
					<div class="wrapper first">
						<div class="title">
						Empresarial
						</div>
						<div class="price"><sup>$</sup>79</div>
						<div class="period">USD - Dólares al mes</div>
						<ul class="list-info">
							<li><sub></sub>2 GB de espacio</li>
							<li>20 usuarios</li>
							<li style="font-family: opensans bold;">Pago anual: $948 USD</li>
						</ul>
			
					</div>
				</div>	
				<div class="col-sm-3 no-padding">
					<div class="wrapper third">
						<div class="title">
						Ilimitado
						</div>
						<div class="price"><sup>$</sup>199</div>
						<div class="period">USD - Dólares al mes</div>
						<ul class="list-info">
							<li><sub></sub>Espacio ilimitado</li>
							<li>Usuarios ilimitados</li>
							<li style="font-family: opensans bold;">Pago anual: $2388 USD</li>
						</ul>
					</div>
				</div>
			</div>
            
    
            
		</div>
    </div>
    
        <div class="row">
				<div class="col-sm-6 col-sm-offset-3 no-padding-sm">
					<form action="#" class="payment-form">						
						<div class="text-center">
							<a href="formato-registro-cuenta.aspx" class="wpc-btn">Regístrate y elige tu plan</a>
                        </div>
					</form>
			</div>
   		 </div>
    
    <!--editable content div-->
	<!-- InstanceEndEditable -->
    
    
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
    
    
    
    
    <!-- 
	*******************
		SUBSCRIBE
	****************** 
	-->

	
    
<!--	<div class="wpc-subcribe">	
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-md-offset-1 col-xs-12">
					<h1 class="heading" for="mce-EMAIL">Suscríbete a nuestro newsletter</h1>
				</div>
			</div>
			<div class="row" id="mc_embed_signup">
				<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-7 col-md-offset-1 col-sm-7 col-xs-12" id="mc_embed_signup_scroll">
						<input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Correo electrónico" class="email-input" required>
					</div>
					<div class="col-sm-4 col-xs-12  text-xs-center">
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="wpc-btn">
					</div>
				</form>
			</div>
		</div>
	</div>-->
    
<!-- 
<div id="mc_embed_signup">
<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>*/-->


	<!-- 
	*******************
		FOOTER
	****************** 
	-->

	<footer class="wpc-footer">
        <div class="container no-padding-sm">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="index.aspx" style="margin-right: 45px;"><img src="img/id_icono_40x40px.png" alt="logo"></a>
                    <a href="https://www.facebook.com/identidad.com.manual" target="_blank"><img src="img/facebook.jpg" alt="logo" style="width:45px;"></a>
                    <a href="https://twitter.com/identidadcom" target="_blank"><img src="img/twitter.jpg" alt="logo" style="width:45px;"></a>
 <!--                   <p class="contact"><i class="flaticon-location"></i></p> 
                    <p class="contact"><a href=""><i class="flaticon-letter"></i></a></p> 
                    <p class="contact"><a href="tel:"><i class="flaticon-technology-1"></i></a></p>-->
                </div>
                <div class="col-md-4 col-sm-6">
                	&nbsp;
           <!--         <div class="heading">Nuestras páginas</div>
                    <ul class="footer-menu classic">
                        <li class="menu-item"><a href="../index.aspx">Inicio</a> </li>                             
                        <li class="menu-item"><a href="../manual-identidad-online.aspx">La plataforma</a> </li>
                        <li class="menu-item"><a href="../guia-marca-online-precios.aspx">Los precios</a></li>
                        <li class="menu-item"><a href="../login-registro.aspx">Accede / Login</a></li>
                        <li class="menu-item"><a href="../contacto.aspx">Contacto</a></li>
                    </ul>-->
                </div>
                <div class="col-md-4 col-sm-12 text-xs-center text-sm-center">
                   	<div class="footer-info">
  Después de crear una marca hay que saber administrarla óptimamente.
                    </div>
                &nbsp;
         <!--           <div class="heading">Contacto</div>
                    <div class="soc-icons">
                        <a href=""><i class="fa fa-facebook"></i> Facebook</a>
                        <a href=""><i class="fa fa-twitter"></i> Twitter</a> 
                        <a href=""><i class="fa fa-linkedin"></i> Linkedin</a>                      
                    </div>    -->              
                </div>
            </div>          
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 no-padding-sm">
                    <div class="footer-bottom">
                    <div class="container no-padding-sm">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 text-center text-lg-left">
                                <div class="copyrigh">Todos los derechos reservados. Identidad.com - 2017</div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <ul class="footer-menu">
                                    <li class="menu-item"><a href="aviso-privacidad.aspx">Aviso de privacidad</a></li>                             
                                    <li class="menu-item"><a href="terminos-condiciones.aspx">Términos y condiciones</a></li>                             
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.circliful.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scripts.js"></script>

	<!--INICIA CÓDIGO DE GOOGLE ANALYTICS-->
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78976181-1', 'auto');
        ga('send', 'pageview');

	</script>
    
    <!--TERMINA CÓDIGO DE GOOGLE ANALYTICS-->
    
  </body>
<!-- InstanceEnd --></html>