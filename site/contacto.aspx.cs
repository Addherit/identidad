﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Identidad.App_Code;

namespace Identidad.site
{
    public partial class contacto : System.Web.UI.Page
    {
        #region Variables
        string sJson = "";

        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        //cBrandSite oBrandSite = new cBrandSite();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if(!(Request.Form["name"] == null))
                btnEnviar_Click();
        }

        public void btnEnviar_Click()
        {
            string username = Request.Form["name"].ToString();
            string useremail = Request.Form["email"].ToString();
            string usercomment = Request.Form["comment"].ToString();
            if (useremail != "" || username != "" || usercomment != "")
            {
                oMetodo = oUsuario.EnviarCorreoContactanos("22", useremail + " " + username + " quiere ponerse en contacto contigo.", usercomment);
                //oMetodo = oUsuario.EnviarCorreoContactanos("5", email.Value + " " + name.Value + " quiere ponerse en contacto contigo.", comment.Value);
                //oMetodo = oUsuario.EnviarCorreoContactanos("8", email.Value + " " + name.Value + " quiere ponerse en contacto contigo.", comment.Value);
                //email.Value = "";
                comment.Value = "";
                name.Value = "";
            }
            else
            {
                //lblmessage.Visible = true;
            }
        }
    }
}