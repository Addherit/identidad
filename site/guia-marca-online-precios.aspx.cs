﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Identidad.site
{
    public partial class guia_marca_online_precios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            string redirectUrl1 = Request.Url.ToString().Replace("guia-marca-online-precios", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
}