﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;
using conekta;


namespace Identidad.site
{
    public partial class pagina_datos_pago : System.Web.UI.Page
    {
        #region Variables
        string arr = "";
        string sJson = "";
        string tipoP = "";
        string monto = "";
        static string prevPage = String.Empty;

        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["IdBrandSite"] != null)
            {
                Session["IdBrandSite"] = Session["IdBrandSite"].ToString();
            }

            if (String.IsNullOrEmpty((String)Session["IdBrandSite"]))//Si no existe sesión redirige a la página index
            {
                Response.Redirect("index.aspx");
            } 
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if ((Request.Form["nombre"] == null))//Obtiene la página anterior
            {
                if (Request.UrlReferrer != null)
                    prevPage = Request.UrlReferrer.ToString();
            }
            if (Request.Form["__EVENTTARGET"] == "placeorder")//Obtiene el evento de pago al dar click en pagar
            {                
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                arr = Page.Request.Params["__EVENTARGUMENT"];
                Hidden1.InnerText = arr.Substring(0, arr.IndexOf(','));
                arr = arr.Substring(arr.IndexOf(',') + 1);
                //monto = arr.Substring(0, arr.IndexOf(','));
                //arr = arr.Substring(arr.IndexOf(',') + 1);

                //Hacemos la primera validación del plan elegido para enviar el monto a pagar en centavos
                switch(Hidden1.InnerText)
                {
                    case "1": monto="22800";
                        break;
                    case "2": monto="94800";
                        break;
                    case "3": monto="238800";
                        break;
                }
                //Hacemos la segunda validación del monto mandando desde el javascript un conjunto de caracteres para evitar que lo modifiquen desde el editor
                int amount = int.Parse(monto);
                sJson = arr.Replace("**#_test_#**", monto); 
                //sjson = sJson.Replace("**#_test_#**", monto);
                
                if (sJson.ToString() != "")
                {
                    btnPlaceOrder(this, new EventArgs());
                }
            }
            if (Request.Form["__EVENTTARGET"] == "planPrueba")
            {
                PlanPrueba();
            }

            //test con lo nuevo de conekta
            conekta.Api.apiKey = "key_rQssuytKsjJrnTEaRLrZkw";
            conekta.Api.version = "2.0.0";
            conekta.Api.locale = "es";

            
          

            //oBrandSite.CargarDatosFacturacion(Session["idBrandSite"].ToString());
            //oBrandSite.CargarDatosFacturacion(Session["IdBrandSite"].ToString()).Row[1]);
            //btnPlaceOrder();
            
             foreach (DataRow row in oBrandSite.CargarDatosFacturacion(Session["IdBrandSite"].ToString()).Rows)//Parámetros de facturción
                {
                    rsocial.Value = row["RazonSocial"].ToString();
                    rfc.Value = row["RFC"].ToString();
                    nombre.Value = row["Nombres"].ToString();
                    primerapellido.Value = row["Apellidos"].ToString();
                    segundoapellido.Value = row["Apellidos"].ToString();
                    calle.Value = row["calle"].ToString();
                    noext.Value = row["Numero"].ToString();
                    noint.Value = row["NumeroInt"].ToString();
                    colonia.Value = row["Colonia"].ToString();
                    delegacion.Value = row["Municipio"].ToString();
                    estado.Value = row["Estado"].ToString();
                    codigopostal.Value = row["CP"].ToString();
                    pais.Value = row["Pais"].ToString();
                    email.Value = row["CorreoElectronico"].ToString();
                }
            try
            {
             /*   Order order = new conekta.Order().create(@"{
                      ""currency"":""USD"",
                      ""customer_info"": {
                      ""name"": ""Jul Ceballos"",
                      ""phone"": ""3609797490"",
                      ""email"": ""jose.ernesto@addherit.com""
                      },
                      ""line_items"": [{
                      ""name"": ""Basico"",
                      ""unit_price"": 228,
                      ""quantity"": 1
                      }]
                      }");
                Order order2 = new conekta.Order().create(@"{
                      ""currency"":""USD"",
                      ""customer_info"": {
                      ""name"": ""Jul Ceballos"",
                      ""phone"": ""3609797490"",
                      ""email"": ""jose.ernesto@addherit.com""
                      },
                      ""line_items"": [{
                      ""name"": ""Empresarial"",
                      ""unit_price"": 948,
                      ""quantity"": 1
                      }]
                      }");*/

               
                //order3.capture();                

                /*""last4"",""1881"",
""object"": ""card_payment"",
 ""exp_month"": ""12"",
  ""exp_year"": ""20"",
  ""brand"": ""VISA""*/
                //Assert.AreEqual(charge.payment_method.type, "credit");
                //Assert.AreEqual(charge.id.GetType().ToString(), "System.String");
                //Assert.AreEqual(charge.amount, 20000);

              
            }
            catch (ConektaException i)
            {
                
              //  Assert.AreEqual(i._object, "error");
               // Assert.AreEqual(i._type, "parameter_validation_error");
            }
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        protected void btnPlaceOrder(object sender, EventArgs e)//Obtener el token con los parametros del pago en conjunto con los de la tarjeta
        {
         
            
             Order order3 = new conekta.Order().create(@"{
                      ""currency"":""USD"",
                      ""customer_info"": {
                      ""name"": ""Jorge Lopez"",
                      ""phone"": ""3609797490"",
                      ""email"": ""jose.ernesto@addherit.com""
                      },
                      ""line_items"": [{
                      ""name"": ""Ilimitado"",
                      ""unit_price"": 238800,
                      ""quantity"": 1
                      }],
                      ""billing_address"": {
                        ""street1"":""77 Mystery Lane"",
                        ""street2"": ""Suite 124"",
                        ""street3"": null,
                        ""city"": ""Darlington"",
                        ""state"":""NJ"",
                        ""zip"": ""10192"",
                        ""country"": ""Mexico"",
                        ""tax_id"": ""xmn671212drx"",
                        ""company_name"":""X-Men Inc."",
                        ""phone"": ""77-777-7777"",
                        ""email"": ""purshasing@x-men.org""
                      }
                      }");
              String tok = Request.Form["conektaTokenId"];
                order3.createCharge(@"{
                    ""payment_method"": {
                    ""type"": ""card"",
                    ""token_id"": ""tok_test_mastercard_5100""
                    },
                    ""amount"": 238800
                    }");
            string sURL = "";
            int idVta = 0;

            cFactura Fact = new cFactura();

            Request.Headers.Add("Accept", "application/vnd.conekta-v1.0.0+json");
            Request.Headers.Add("Content-type", "application/json");
            
            sURL = "https://api.conekta.io/charges";
            //Creamos un json con los datos fijos por validar que sean los paramtros necesarios
            //var sJson = "{\"description\":" + plan.InnerHtml.ToString() +",\"amount\": 20000,\"currency\":\"MXN\",\"card\": \"tok_test_visa_4242\",\"details\": {\"name\":\"Arnulfo Quimare\",\"phone\":\"403-342-0642\",\"email\":\"logan@x-men.org\",\"line_items\": [{\"name\": \"Box of Cohiba S1s\",\"description\": \"Imported From Mex.\",\"unit_price\": 20000,\"quantity\": 1}]}}}";
            //'{"description":"Stogies","amount": 20000,"currency":"MXN","reference_id":"9839-wolf_pack","card": "tok_test_visa_4242","details": {"name":"Arnulfo Quimare","phone":"403-342-0642","email":"logan@x-men.org","customer": {"corporation_name": "Conekta Inc.","logged_in": true,"successful_purchases": 14,"created_at": 1379784950,"updated_at": 1379784950,"offline_payments": 4,"score": 9},"line_items": [{"name": "Box of Cohiba S1s","description": "Imported From Mex.","unit_price": 20000,"quantity": 1,"sku": "cohb_s1","type": "food"}],"billing_address": {"street1":"77 Mystery Lane","street2":"Suite 124","city":"Darlington","state":"NJ","zip":"10192","country": "Mexico","phone":"77-777-7777","email":"purshasing@x-men.org"},"shipment": {"carrier":"estafeta","service":"international","price": 20000,"address": {"street1": "250 Alexis St","street2": "Interior 303","street3": "Col. Condesa","city":"Red Deer","state":"Alberta","zip":"T4N 0B8","country":"Canada"}}}}';
            
            //var response = cHTTPrequest.ObtenerListaWS(sURL, sJson, Session["IdUsuario"].ToString());//Crea y obtiene la respuesta del request del pago
            var response = "";
            string[] tokens = response.Split(',');

            if (response.Contains("Su pago no pudo ser procesado.") == false && prevPage != "")//Valida que el pago sea exitoso o no
            {
                if (String.IsNullOrEmpty((string)Session["NombreBrand"]) || String.IsNullOrEmpty((string)Session["sUrl"]))//Valida si alguna de las 2 sesiones contenga información
                {
                    foreach (DataRow row in oBrandSite.cargarDatosBrandSite(Session["IdBrandSite"].ToString()).Rows)
                    {
                        Session["NombreBrand"] = row["Nombre"].ToString();//Asigna la sesión del nombre del sitio
                        Session["sUrl"] = row["URL"].ToString();//Asigna la sesión con la url dada de alta den la BD para el sitio
                    }
                }

                //Si el pago fue exitoso entra al evento agregar venta para hacer el registro en la BD
                idVta = oBrandSite.agregarVenta(Session["IdUsuario"].ToString(), nombre.Value.ToString(), primerapellido.Value.ToString(), calle.Value.ToString(), noext.Value.ToString(), noint.Value.ToString(), colonia.Value.ToString(), delegacion.Value.ToString(), estado.Value.ToString(), codigopostal.Value.ToString(), pais.Value.ToString(), email.Value.ToString(), rfc.Value.ToString(), response, monto);

                if (idVta > 1)//Si la inserción de la venta fue exitosa agrega el cliente y después actualiza el Sitio con el nuevo plan en la BD
                {
                    oMetodo = oBrandSite.agregarCTE(Session["IdBrandSite"].ToString(), tipoP, rfc.Value.ToString(), nombre.Value.ToString(), primerapellido.Value.ToString(), calle.Value.ToString(), noext.Value.ToString(), noint.Value.ToString(), colonia.Value.ToString(), delegacion.Value.ToString(), estado.Value.ToString(), codigopostal.Value.ToString(), pais.Value.ToString(), email.Value.ToString(), "", "1", rsocial.Value.ToString());
                    oMetodo = oBrandSite.ActualizarBrandSite(Session["IdBrandSite"].ToString(), Session["NombreBrand"].ToString(), Session["sUrl"].ToString(), "1", Session["IdUsuario"].ToString(), Hidden1.InnerText);
                }

                if (oMetodo.Pass)//Si los metodos fueron exitosos crea el archivo csv para la facturación
                {
                    Fact.Lista_csv(Fact.Dic(idVta, int.Parse(Session["IdBrandSite"].ToString())), Fact.Detalle());
                }

                if (prevPage == "https://localhost:62352/site/pagina-datos-pago.aspx" || prevPage == "https://identidad.com/site/pagina-datos-pago.aspx")//Evita que la redirección después del pago sea a la misma página de pagos
                {
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
                }
                else
                {
                    Response.Redirect(prevPage);//Redirige a la página que se acaba de renovar
                }
            }
            else
            {
                cardErrors.InnerText = response;//Agrega los comentarios para informar el error en caso de que el pago no ea exitoso.
                cardErrors.InnerHtml = response;//Agrega los comentarios para informar el error en caso de que el pago no ea exitoso.
            }

        }

        protected void PlanPrueba ()//Función para el plan de prueba
        {
            string plan = "";
            if (String.IsNullOrEmpty((string)Session["NombreBrand"]) || String.IsNullOrEmpty((string)Session["sUrl"]))//Valida si no tiene nada en la sesiones
            {
                foreach (DataRow row in oBrandSite.cargarDatosBrandSite(Session["IdBrandSite"].ToString()).Rows)
                {
                    Session["NombreBrand"] = row["Nombre"].ToString();//Asigna un valor a la sesión
                    Session["sUrl"] = row["URL"].ToString();//Asigna un valor a la sesión
                    plan = row["Plan"].ToString();
                }
            }

            //Actualiza el plan
           // if (plan.ToString().Equals(""))
           if (plan.ToString() != "0" && plan.ToString() != "")
                oMetodo = oBrandSite.ActualizarBrandSite(Session["IdBrandSite"].ToString(), Session["NombreBrand"].ToString(), Session["sUrl"].ToString(), "1", Session["IdUsuario"].ToString(), "0");
            else
            {
                cardErrors.InnerText = "¡Ups! Si tu plan ha vencido no puedes seguir disfrutando de este beneficio, te invitamos a renovar.";
                cardErrors.InnerHtml = "¡Ups! Si tu plan ha vencido no puedes seguir disfrutando de este beneficio, te invitamos a renovar.";
            }
            //int idVta = 0;
            if (oMetodo.Pass) { 
                
                //idVta = (int)oBrandSite.agregarVenta(Session["IdUsuario"].ToString(), nombre.Value.ToString(), primerapellido.Value.ToString(), calle.Value.ToString(), noext.Value.ToString(), noint.Value.ToString(), colonia.Value.ToString(), delegacion.Value.ToString(), estado.Value.ToString(), codigopostal.Value.ToString(), pais.Value.ToString(), email.Value.ToString(), rfc.Value.ToString(), "Prueba", "0");
                if(prevPage == "http://localhost:62352/site/pagina-datos-pago.aspx" || prevPage == "http://identidad.com/site/pagina-datos-pago.aspx")
                {
                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);
                }
                else
                {
                    Response.Redirect(prevPage);
                }
            }
        }
    }
}