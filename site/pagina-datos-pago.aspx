﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pagina-datos-pago.aspx.cs" Inherits="Identidad.site.pagina_datos_pago" %>

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/template-identidad-pag.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Manual de identidad en linea. Tu manual de marca online para ti y tu equipo. - identidad.com</title>
    <!-- InstanceEndEditable -->
    <meta name="description" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/fopen-sans.css" rel="stylesheet">
    <link href="css/raleway-webfont.css" rel="stylesheet">
    <link href="css/lato.css" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="img/favicon.ico" rel="shortcut icon">
    <link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!-- InstanceBeginEditable name="head" -->  
	<!-- InstanceEndEditable -->
      <script>
          var itemname;
          var amount;
     
          //----------Funciones elige plan
          function muestraFacturacion() {
              document.getElementById("datospago").style.display = "none";
              document.getElementById("datosfacturacion").style.display = "none";
              document.getElementById("btnprueba").style.display = "block";
              document.getElementById("datospago").style.display = "none";
              document.getElementById("btnordenar").style.display = "none";
              document.getElementById("btnordenar").disabled = true;
              document.getElementById("btnprueba").style.display = "block";
              document.getElementById("btnprueba").disabled = false;
          }
          function muestraPago() {            
              document.getElementById("datospago").style.display = "block";
              document.getElementById("datosfacturacion").style.display = "block";
              document.getElementById("btnordenar").style.display = "block";
              document.getElementById("btnprueba").style.display = "none";
              document.getElementById("plan").innerHTML = "Plan Identidad";
              document.getElementById("btnprueba").disabled = true;
          }
          function prueba() {
              itemname = "0";
              document.getElementById("Hidden1").innerText = itemname;
          }
          function basico() {
              itemname = "1";
              amount = 22800;
              document.getElementById("Hidden1").innerText = itemname;
          }
          function empresarial() {
              itemname = "2";
              amount = 94800;
              document.getElementById("Hidden1").innerText = itemname;
          }
          function ilimitado() {
              itemname = "3";
              amount = 238800;
              //alert('itemname: ' + itemname);
              document.getElementById("Hidden1").innerText = itemname;
          }

          function planPrueba() {
              //alert(document.getElementById("rsocial").value);
              //alert("PlanPrueba");
                 // if (document.getElementById("rsocial").value != "" && document.getElementById("nombre").value != "" && document.getElementById("primerapellido").value != "" && document.getElementById("calle").value != "" && document.getElementById("noext").value != "" && document.getElementById("colonia").value != "" && document.getElementById("delegacion").value != "" && document.getElementById("estado").value != "" && document.getElementById("codigopostal").value != "" &&  document.getElementById("email").value != "") {
                      //alert("Dopost");
                      __doPostBack("planPrueba", "");
                 // }
                
          }

          
      </script>
  </head>
  <body>


	<!-- 
	*******************
      	HEADER    
	****************** 
	-->
    
    
	<!--<div class="wpc-main-header">
	  <div class="container no-padding-sm">
	    <div class="row">
	      <div class="col-sm-6 no-padding">
	      <<div class="soc-icons"> <a href=""><i class="faTop fa-facebook"></i></a> <a href=""><i class="faTop fa-twitter"></i></a> <a href=""><i class="faTop fa-linkedin"></i></a> </div>
          </div>
	      <div class="col-sm-6 no-padding-right">
	        <section class="header-info">
	          <h6> <a href="../login-registro.aspx"><i class="fa"><img src="../img/icon-2.png" alt="icon"></i>e</a> <a href="#"><i class="fa fa-location-arrow"></i>Soporte</a> <a href="tel:+527773138466"><i class="fa fa-mobile"></i>6</a> <a href="mailto:info@identidad.com"><i class="fa fa-envelope"></i>info@identidad.com</a> </h6>
            </section>
          </div>
        </div>
      </div>
    </div>-->
    
    
    
	<!--
	*******************
       LOGIN   
	****************** 
	-->
	
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<!-- form itself -->
		<form action="#" id="test-form" class="mfp-hide white-popup-block login-form">
		
			<div class="row">
				<div class="col-sm-6 col-sm-offset-1">
					<div class="wrapper">
						<h3 class="title">Log in</h3>
						<div class="subtitle">Please fill in your basic info:</div>
						<div><input type="text" name="username" placeholder="Username" class="field" autocomplete="off" required></div>
						<div><input type="password" name="password" placeholder="Password" class="field" autocomplete="off" required></div>
					</div>
				</div>
				<div class="col-sm-4 text-xs-center">
					<label class="label-form"><input type="checkbox" class="btn-check">Remember me</label>
					<input type="submit" class="wpc-btn" value="log in">
				</div>
			</div>
			
		</form>
			</div>
		</div>
	</div>
	


	
<!-- 
	*******************
      NAV MENU    
	****************** 
	-->


	<div class="wpc-menu-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 no-padding">
					<a href="index.aspx"><img src="img/ID_FirmaBETA_3.png" alt="logo" class="logo-menu"></a>
				</div>
				<div class="col-sm-9 col-lg-7 col-lg-offset-2 no-padding">
					<nav class="wpc-nav-menu">
						<button class="menu-toggle">
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    	</button>
					    <ul class="main-menu">
					        <li class="menu-item">
					        	<a href="manual-identidad-online.aspx">La plataforma</a>
				        	</li>
					        <li class="menu-item">
					        	<a href="guia-marca-online-precios.aspx">Los planes</a>
				        	</li>
					        <li class="menu-item">
					            <a href="login-registro.aspx">Ingresa</a>
					        </li>
					        <li class="menu-item">
					            <a href="ayuda-faq.aspx">Ayuda</a>
					        </li>
					        <li class="menu-item"><a href="contacto.aspx">Contacto</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
	
	<!-- InstanceBeginEditable name="Main Content Site" -->
	<%--<div id="Editable Content">--%>
    
    	<!-- 
	*******************
		CART
	****************** 
	-->

	<div class="wpc-cart">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="section-heading">
						Pago y facturación</div>
		<!--			<div class="section-subheading"></div>-->
				</div>
			</div>
        
        
<div class="wpc-cart">
		<div class="container">
		<div class="row">
				<div class="col-sm-12">
					<div class="header">Elige tu plan:</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12 no-padding">
                    <div id="boton"></div>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Plan</th>
									<th class="text-left"> tiempo de servicio</th>
									<th>Usuarios</th>
									<th>Espacio</th>
                                    <th>Pago</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Prueba</td>
									<td class="title">2 meses</td>
									<td>5 usuarios</td>
									<td>500MB</td>
                                    <td>Gratuito</td>
									<!--<td class="total"><a id="Prueba" onclick=" setActive(this); muestraFacturacion(); prueba()" href="#boton" class="wpc-btn">Empieza por $0</a></td>-->
									<td class="total"><a id="Prueba"  href="#boton" class="wpc-btn">Empieza por $0</a></td>
							
                                </tr>
								<tr>
									<td>Básico</td>
									<td class="title">12 meses</td>
									<td>5 usuarios</td>
									<td>500MB</td>
                                    <td>Anual</td>
									<!--<td class="total"><a id="Basico" onclick="setActive(this); muestraPago(); basico()" href="#datospago" class="wpc-btn">Empieza por $228 USD</a></td>-->
									<td class="total"><a id="Basico" href="#datospago" class="wpc-btn">Empieza por $228 USD</a></td>
							
                                </tr>                                
								<tr>
									<td>Empresarial</td>
									<td class="title">12 meses</td>
									<td>20 usuarios</td>
									<td>2 GB</td>
                                    <td>Anual</td>
									<!--<td class="total"><a id="Empresarial" onclick="setActive(this); muestraPago(); empresarial()" href="#datospago" class="wpc-btn">Empieza por $948 USD</a></td>-->
								<td class="total"><a id="Empresarial" href="#datospago" class="wpc-btn">Empieza por $948 USD</a></td>
                                </tr>                            
								<tr>
									<td>Ilimitado</td>
									<td class="title">12 meses</td>
									<td>Ilimitado</td>
									<td>Ilimitado</td>
                                    <td>Anual</td>
									<!--<td class="total"><a id="Ilimitado" onclick="setActive(this); muestraPago(); ilimitado()" href="#datospago" class="wpc-btn">Empieza por $2,388 USD</a></td>-->
								    <td class="total"><a id="Ilimitado" href="#datospago" class="wpc-btn">Empieza por $2,388 USD</a></td>
                                </tr>				
							</tbody>
						</table>
                        <%--<p id="plan" runat="server" style="display:none;"></p>--%>
                        <%--<input type="text" id="plan1" runat="server" />--%>
                        <p id="plan" runat="server"></p>
                        <p runat="server" id="cardErrors" style="color: red;font-size: 20px;"></p>
					</div>
				</div>
			</div>
		
		</div>
	</div>


        	
            <form runat="server" id="cardform" method="post">
			<div id="datospago" class="row" style="display: none;">
				<div class="col-sm-12 no-padding-sm" >
					<div class="form">
						<div class="header">Proporciona tus datos de pago:</div>
						<div class="row">
							<div class="col-sm-6">
								<input type="text" data-conekta="card[number]" placeholder="Número de tarjeta *" class="field" onkeypress="return soloNumeros(event)">
							</div>
							<div class="col-sm-6">
								<input type="text" data-conekta="card[name]" placeholder="Nombre como aparece en la tarjeta *" class="field">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
							  <%--<input type="number" data-conekta="card[exp_month]" placeholder="mes vencimiento" class="field">--%>
                                <select class="field" data-conekta="card[exp_month]">
                                    <option value="">  Mes de vencimiento *</option>
                                    <option value=1>   Enero (01)</option>
                                    <option value=2>   Febrero (02)</option>
                                    <option value=3>   Marzo (03)</option>
                                    <option value=4>   Abril (04)</option>
                                    <option value=5>   Mayo (05)</option>
                                    <option value=6>   Junio (06)</option>
                                    <option value=7>   Julio (07)</option>
                                    <option value=8>   Agosto (08)</option>
                                    <option value=9>   Septiembre (09)</option>
                                    <option value=10>   Octubre (10)</option>
                                    <option value=11>   Noviembre (11)</option>
                                    <option value=12>   Diciembre (12)</option>
                                </select>
							</div>
							<div class="col-sm-6">
								<%--<input type="number" data-conekta="card[exp_year]" placeholder="año de vencimiento" class="field">--%>
                                <select class="field" data-conekta="card[exp_year]">
                                    <option value="">  Año de vencimiento *</option>
                                    <option value=2016>   19</option>
                                    <option value=2017>   20</option>
                                    <option value=2018>   21</option>
                                    <option value=2019>   22</option>
                                    <option value=2020>   23</option>
                                    <option value=2021>   24</option>
                                    <option value=2022>   25</option>
                                    <option value=2023>   26</option>
                                    <option value=2024>   27</option>
                                    <option value=2025>   28</option>
                              
                                </select>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<input type="text" data-conekta="card[cvc]" placeholder="Código de seguridad (al reverso de la tarjeta) *" class="field" onkeypress="return soloNumeros(event)" maxlength="3">
							</div>
                            <div class="col-sm-6">
								<input type="text" id="telefono" placeholder="Teléfono * (últimos 10 dígitos)" class="field" onkeypress="return soloNumeros(event)" maxlength="10" required>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            
			<div id="datosfacturacion" class="row" style="display: none;">
				<div class="col-sm-12 no-padding-sm">
					<div class="form">
						<div class="header">Proporciona tus datos de facturación: </div>
                        <div class="subtitle">(* Los campos con asterisco son requeridos)</div>
                        <div class="row">
							<div class="col-sm-6">
								<%--<input type="text"  placeholder="país" class="field">--%>
                                <select id="pais" runat="server" class="field" onchange="changeCountry();" style="-webkit-appearance: menulist-button; height:81px">
                                    <option value="ARG">Argentina	</option>
                                    <option value="BOL">Bolivia	</option>
                                    <option value="CHI">Chile	</option>
                                    <option value="COL">Colombia	</option>
                                    <option value="CRI">Costa Rica	</option>
                                    <option value="CUB">Cuba	</option>
                                    <option value="ECU">Ecuador	</option>
                                    <option value="SAL">El Salvador	</option>
                                    <option value="ESP">España	</option>
                                    <option value="GUA">Guatemala	</option>
                                    <option value="GEC">Guinea Ecuatorial	</option>
                                    <option value="HON">Honduras	</option>
                                    <option value="MEX">México	</option>
                                    <option value="NIC">Nicaragua	</option>
                                    <option value="PAN">Panamá	</option>
                                    <option value="PAR">Paraguay	</option>
                                    <option value="PER">Perú	</option>
                                    <option value="PUR">Puerto Rico	</option>
                                    <option value="0RD">República Dominicana	</option>
                                    <option value="URU">Uruguay	</option>
                                    <option value="VEN">Venezuela	</option>
                                    <option value="BLC">Belice	</option>
                                    <option value="BRA">Brasil	</option>
                                    <option value="CAN">Canadá	</option>
                                    <option value="EUA">Estados Unidos	</option>
                                    <option value="FRA">Francia	</option>
                                    <option value="ITA">Italia	</option>
                                    <option value="AFG">Afganistán	</option>
                                    <option value="AKR">Akrotiri	</option>
                                    <option value="ALB">Albania	</option>
                                    <option value="ALE">Alemania	</option>
                                    <option value="AND">Andorra	</option>
                                    <option value="ANG">Angola	</option>
                                    <option value="AGI">Anguila	</option>
                                    <option value="ANT">Antártida	</option>
                                    <option value="ANB">Antigua y Barbuda	</option>
                                    <option value="ANN">Antillas Neerlandesas	</option>
                                    <option value="ARS">Arabia Saudí	</option>
                                    <option value="ARO">Arctic Ocean	</option>
                                    <option value="AGL">Argelia	</option>
                                    <option value="ARM">Armenia	</option>
                                    <option value="ARU">Aruba	</option>
                                    <option value="ACI">Ashmore and Cartier Islands	</option>
                                    <option value="ATO">Atlantic Ocean	</option>
                                    <option value="AUS">Australia	</option>
                                    <option value="ATR">Austria	</option>
                                    <option value="AZE">Azerbaiyán	</option>
                                    <option value="BAH">Bahamas	</option>
                                    <option value="BAR">Bahráin	</option>
                                    <option value="BAN">Bangladesh	</option>
                                    <option value="BBA">Barbados	</option>
                                    <option value="BEL">Bélgica	</option>
                                    <option value="BEN">Benín	</option>
                                    <option value="BER">Bermudas	</option>
                                    <option value="BIE">Bielorrusia	</option>
                                    <option value="BIR">Birmania	</option>
                                    <option value="BOS">Bosnia y Hercegovina	</option>
                                    <option value="BOT">Botsuana	</option>
                                    <option value="BRU">Brunéi	</option>
                                    <option value="BUL">Bulgaria	</option>
                                    <option value="BUK">Burkina Faso	</option>
                                    <option value="BUR">Burundi	</option>
                                    <option value="BUT">Bután	</option>
                                    <option value="CAV">Cabo Verde	</option>
                                    <option value="CAM">Camboya	</option>
                                    <option value="CME">Camerún	</option>
                                    <option value="CHA">Chad	</option>
                                    <option value="CNA">China	</option>
                                    <option value="CPR">Chipre	</option>
                                    <option value="CLI">Clipperton Island	</option>
                                    <option value="COM">Comoras	</option>
                                    <option value="CON">Congo	</option>
                                    <option value="CSI">Coral Sea Islands	</option>
                                    <option value="CON">Corea del Norte	</option>
                                    <option value="CSU">Corea del Sur	</option>
                                    <option value="CMA">Costa de Marfil	</option>
                                    <option value="CRO">Croacia	</option>
                                    <option value="DHE">Dhekelia	</option>
                                    <option value="DIN">Dinamarca	</option>
                                    <option value="DOM">Dominica	</option>
                                    <option value="EGI">Egipto	</option>
                                    <option value="VAT">El Vaticano	</option>
                                    <option value="EAU">Emiratos Árabes Unidos	</option>
                                    <option value="ERI">Eritrea	</option>
                                    <option value="ESL">Eslovaquia	</option>
                                    <option value="ESV">Eslovenia	</option>
                                    <option value="EST">Estonia	</option>
                                    <option value="ETI">Etiopía	</option>
                                    <option value="FIL">Filipinas	</option>
                                    <option value="FIN">Finlandia	</option>
                                    <option value="FIY">Fiyi	</option>
                                    <option value="GAB">Gabón	</option>
                                    <option value="GAM">Gambia	</option>
                                    <option value="GAZ">Gaza Strip	</option>
                                    <option value="GEO">Georgia	</option>
                                    <option value="GHA">Ghana	</option>
                                    <option value="GIB">Gibraltar	</option>
                                    <option value="GRA">Granada	</option>
                                    <option value="GRE">Grecia	</option>
                                    <option value="GRO">Groenlandia	</option>
                                    <option value="GUM">Guam	</option>
                                    <option value="GUE">Guernsey	</option>
                                    <option value="GUI">Guinea	</option>
                                    <option value="GBI">Guinea-Bissau	</option>
                                    <option value="GUY">Guyana	</option>
                                    <option value="HAI">Haití	</option>
                                    <option value="HOK">Hong Kong	</option>
                                    <option value="HUN">Hungría	</option>
                                    <option value="IND">India	</option>
                                    <option value="IOC">Indian Ocean	</option>
                                    <option value="INE">Indonesia	</option>
                                    <option value="IRN">Irán	</option>
                                    <option value="IRQ">Iraq	</option>
                                    <option value="IRL">Irlanda	</option>
                                    <option value="IBU">Isla Bouvet	</option>
                                    <option value="ICH">Isla Christmas	</option>
                                    <option value="INO">Isla Norfolk	</option>
                                    <option value="ISL">Islandia	</option>
                                    <option value="ICA">Islas Caimán	</option>
                                    <option value="ICO">Islas Cocos	</option>
                                    <option value="ISC">Islas Cook	</option>
                                    <option value="IFE">Islas Feroe	</option>
                                    <option value="IGE">Islas Georgia del Sur y Sandwich del Sur	</option>
                                    <option value="IHM">Islas Heard y McDonald	</option>
                                    <option value="IMA">Islas Malvinas	</option>
                                    <option value="IMN">Islas Marianas del Norte	</option>
                                    <option value="IMS">Islas Marshall	</option>
                                    <option value="IPI">Islas Pitcairn	</option>
                                    <option value="ISA">Islas Salomón	</option>
                                    <option value="ITC">Islas Turcas y Caicos	</option>
                                    <option value="VIA">Islas Vírgenes Americanas	</option>
                                    <option value="VIB">Islas Vírgenes Británicas	</option>
                                    <option value="ISR">Israel	</option>
                                    <option value="JAM">Jamaica	</option>
                                    <option value="JAN">Jan Mayen	</option>
                                    <option value="JAP">Japón	</option>
                                    <option value="JER">Jersey	</option>
                                    <option value="JOR">Jordania	</option>
                                    <option value="KAZ">Kazajistán	</option>
                                    <option value="KEN">Kenia	</option>
                                    <option value="KIG">Kirguizistán	</option>
                                    <option value="KIR">Kiribati	</option>
                                    <option value="KUW">Kuwait	</option>
                                    <option value="LAO">Laos	</option>
                                    <option value="LES">Lesoto	</option>
                                    <option value="LET">Letonia	</option>
                                    <option value="LIB">Líbano	</option>
                                    <option value="LBE">Liberia	</option>
                                    <option value="LBI">Libia	</option>
                                    <option value="LIE">Liechtenstein	</option>
                                    <option value="LIT">Lituania	</option>
                                    <option value="LUX">Luxemburgo	</option>
                                    <option value="MCA">Macao	</option>
                                    <option value="MCE">Macedonia	</option>
                                    <option value="MAD">Madagascar	</option>
                                    <option value="MAL">Malasia	</option>
                                    <option value="MLA">Malaui	</option>
                                    <option value="MDI">Maldivas	</option>
                                    <option value="MLI">Malí	</option>
                                    <option value="MTA">Malta	</option>
                                    <option value="MIO">Man, Isle of	</option>
                                    <option value="MAR">Marruecos	</option>
                                    <option value="MAU">Mauricio	</option>
                                    <option value="MRI">Mauritania	</option>
                                    <option value="MAY">Mayotte	</option>
                                    <option value="MIC">Micronesia	</option>
                                    <option value="MOL">Moldavia	</option>
                                    <option value="MON">Mónaco	</option>
                                    <option value="MGO">Mongolia	</option>
                                    <option value="MTE">Montenegro	</option>
                                    <option value="MTS">Montserrat	</option>
                                    <option value="MOZ">Mozambique	</option>
                                    <option value="MUN">Mundo	</option>
                                    <option value="NAM">Namibia	</option>
                                    <option value="NAU">Nauru	</option>
                                    <option value="NIS">Navassa Island	</option>
                                    <option value="NEP">Nepal	</option>
                                    <option value="NIG">Níger	</option>
                                    <option value="NGR">Nigeria	</option>
                                    <option value="NIU">Niue	</option>
                                    <option value="NOR">Noruega	</option>
                                    <option value="NCA">Nueva Caledonia	</option>
                                    <option value="NZE">Nueva Zelanda	</option>
                                    <option value="OMA">Omán	</option>
                                    <option value="POC">Pacific Ocean	</option>
                                    <option value="PBA">Países Bajos	</option>
                                    <option value="PAK">Pakistán	</option>
                                    <option value="PAL">Palaos	</option>
                                    <option value="PNG">Papúa-Nueva Guinea	</option>
                                    <option value="PIS">Paracel Islands	</option>
                                    <option value="POF">Polinesia Francesa	</option>
                                    <option value="POL">Polonia	</option>
                                    <option value="POR">Portugal	</option>
                                    <option value="QAT">Qatar	</option>
                                    <option value="RUN">Reino Unido	</option>
                                    <option value="RCA">República Centroafricana	</option>
                                    <option value="RCH">República Checa	</option>
                                    <option value="RDC">República Democrática del Congo	</option>
                                    <option value="RUA">Ruanda	</option>
                                    <option value="RUM">Rumania	</option>
                                    <option value="RUS">Rusia	</option>
                                    <option value="SAO">Sáhara Occidental	</option>
                                    <option value="SAM">Samoa	</option>
                                    <option value="SAA">Samoa Americana	</option>
                                    <option value="SCN">San Cristóbal y Nieves	</option>
                                    <option value="SMA">San Marino	</option>
                                    <option value="SPM">San Pedro y Miquelón	</option>
                                    <option value="SVG">San Vicente y las Granadinas	</option>
                                    <option value="SHE">Santa Helena	</option>
                                    <option value="SLU">Santa Lucía	</option>
                                    <option value="STP">Santo Tomé y Príncipe	</option>
                                    <option value="SEN">Senegal	</option>
                                    <option value="SER">Serbia	</option>
                                    <option value="SEY">Seychelles	</option>
                                    <option value="SIL">Sierra Leona	</option>
                                    <option value="SIN">Singapur	</option>
                                    <option value="SIR">Siria	</option>
                                    <option value="SOM">Somalia	</option>
                                    <option value="STO">Southern Ocean	</option>
                                    <option value="SPI">Spratly Islands	</option>
                                    <option value="SRL">Sri Lanka	</option>
                                    <option value="SUA">Suazilandia	</option>
                                    <option value="SAF">Sudáfrica	</option>
                                    <option value="SUD">Sudán	</option>
                                    <option value="SUE">Suecia	</option>
                                    <option value="SUI">Suiza	</option>
                                    <option value="SUR">Surinam	</option>
                                    <option value="SJM">Svalbard y Jan Mayen	</option>
                                    <option value="TAI">Tailandia	</option>
                                    <option value="TWA">Taiwán	</option>
                                    <option value="TAN">Tanzania	</option>
                                    <option value="TAY">Tayikistán	</option>
                                    <option value="TBR">Territorio Británico del Océano Indico	</option>
                                    <option value="TAF">Territorios Australes Franceses	</option>
                                    <option value="TMO">Timor Oriental	</option>
                                    <option value="TOG">Togo	</option>
                                    <option value="TOK">Tokelau	</option>
                                    <option value="TON">Tonga	</option>
                                    <option value="TRT">Trinidad y Tobago	</option>
                                    <option value="TUN">Túnez	</option>
                                    <option value="TUK">Turkmenistán	</option>
                                    <option value="TUR">Turquía	</option>
                                    <option value="TUV">Tuvalu	</option>
                                    <option value="UCR">Ucrania	</option>
                                    <option value="UGA">Uganda	</option>
                                    <option value="UEU">Unión Europea	</option>
                                    <option value="UZB">Uzbekistán	</option>
                                    <option value="VAN">Vanuatu	</option>
                                    <option value="VIE">Vietnam	</option>
                                    <option value="WAI">Wake Island	</option>
                                    <option value="WAF">Wallis y Futuna	</option>
                                    <option value="WEB">West Bank	</option>
                                    <option value="YEM">Yemen	</option>
                                    <option value="YIB">Yibuti	</option>
                                    <option value="ZAM">Zambia	</option>
                                    <option value="ZIM">Zimbabue	</option>
                                </select>
							</div>
                           	<div class="col-sm-6">
								<input type="text" id="email" runat="server" placeholder="Correo electrónico para facturación *" class="field" required>
							</div>
						</div>
						<div class="row">
							<div id="row1" class="col-sm-12">
								<input type="text" id="rsocial" runat="server" placeholder="Razón social / nombre de la empresa *" class="field" required>
							</div>
							<div class="col-sm-6">
								<input type="text" id="rfc" runat="server" placeholder="RFC *" class="field" style="display:none;">
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
							  <input type="text" id="nombre" runat="server" onkeypress="return soloLetras(event)" placeholder="Nombres *" class="field" required>
							</div>
							<div class="col-sm-6">
								<input type="text" id="primerapellido" runat="server" placeholder="Primer apellido *" class="field" required>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-6">
							  <input type="text" id="segundoapellido" runat="server" placeholder="Segundo apellido" class="field" style="display:none;">
							</div>
							<div id="row2" class="col-sm-12">
								<input type="text" id="calle" runat="server" placeholder="Calle *" class="field" required>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-4">
							  <input type="text" id="noext" runat="server" placeholder="Número exterior *" class="field" onkeypress="return soloNumeros(event)" required>
							</div>
							<div class="col-sm-4">
								<input type="text" id="noint" runat="server" placeholder="Número interior" class="field" onkeypress="return">
							</div>
                            <div class="col-sm-4">
								<input type="text" id="colonia" runat="server" placeholder="Colonia/zona (o repite Ciudad) *" class="field" required>
							</div>
						</div>
                        <div class="row">
							<div class="col-sm-4">
							  <input type="text" id="delegacion" runat="server" placeholder="Ciudad *" class="field" required>
							</div>
							<div class="col-sm-4">
								<input type="text" id="estado" runat="server" placeholder="Estado/provincia *" class="field" required>
							</div>
                            <div class="col-sm-4">
								<input type="text" id="codigopostal" runat="server" placeholder="Código postal (o ingresa 00000) *" class="field" onkeypress="return soloNumeros(event)" maxlength="5" required>
							</div>
						</div>
					</div>
				</div>
			</div>
            
			<div id="btnordenar" class="row" style="display: none;">
				<div class="col-sm-6 col-sm-offset-3 no-padding-sm">
					<div class="payment-form">						
						<div class="text-center">
                            <span class="card-errors"></span>
                            <p runat="server" id="errorCampos" style="color: red;font-size: 20px;"></p>
							<input id="pagar" type="submit" value="Pagar" class="wpc-btn btn-submit"/>
						</div>
					</div>
				</div>
			</div>
            <div id="btnprueba" class="row" style="display: none;">
				<div class="col-sm-6 col-sm-offset-3 no-padding-sm">
					<div class="payment-form">						
						<div class="text-center">
                            <input type="button" value="Probar" onclick="planPrueba()" class="wpc-btn btn-submit" />
						</div>
					</div>
				</div>
			</div>
            </form>
            <label id="Hidden1" style="display:none;" runat="server"></label>
            <label id="AHidden" style="display:none;" runat="server"></label>
		</div>
    
	
    </div>
    <!--editable content div-->
	<!-- InstanceEndEditable -->
    
    
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
    
    
    
    
    <!-- 
	*******************
		SUBSCRIBE
	****************** 
	-->

	
    
<!--	<div class="wpc-subcribe">	
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-md-offset-1 col-xs-12">
					<h1 class="heading" for="mce-EMAIL">Suscríbete a nuestro newsletter</h1>
				</div>
			</div>
			<div class="row" id="mc_embed_signup">
				<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-7 col-md-offset-1 col-sm-7 col-xs-12" id="mc_embed_signup_scroll">
						<input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Correo electrónico" class="email-input" required>
					</div>
					<div class="col-sm-4 col-xs-12  text-xs-center">
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="wpc-btn">
					</div>
				</form>
			</div>
		</div>
	</div>-->
    
<!-- 
<div id="mc_embed_signup">
<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>*/-->


	<!-- 
	*******************
		FOOTER
	****************** 
	-->

	<footer class="wpc-footer">
        <div class="container no-padding-sm">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="index.aspx" style="margin-right: 45px;"><img src="img/id_icono_40x40px.png" alt="logo"></a>
                    <a href="https://www.facebook.com/identidad.com.manual" target="_blank"><img src="img/facebook.jpg" alt="logo" style="width:45px;"></a>
                    <a href="https://twitter.com/identidadcom" target="_blank"><img src="img/twitter.jpg" alt="logo" style="width:45px;"></a>
 <!--                   <p class="contact"><i class="flaticon-location"></i></p> 
                    <p class="contact"><a href=""><i class="flaticon-letter"></i></a></p> 
                    <p class="contact"><a href="tel:"><i class="flaticon-technology-1"></i></a></p>-->
                </div>
                <div class="col-md-4 col-sm-6">
                	&nbsp;
           <!--         <div class="heading">Nuestras páginas</div>
                    <ul class="footer-menu classic">
                        <li class="menu-item"><a href="../index.aspx">Inicio</a> </li>                             
                        <li class="menu-item"><a href="../manual-identidad-online.aspx">La plataforma</a> </li>
                        <li class="menu-item"><a href="../guia-marca-online-precios.aspx">Los precios</a></li>
                        <li class="menu-item"><a href="../login-registro.aspx">Accede / Login</a></li>
                        <li class="menu-item"><a href="../contacto.aspx">Contacto</a></li>
                    </ul>-->
                </div>
                <div class="col-md-4 col-sm-12 text-xs-center text-sm-center">
                   	<div class="footer-info">
  Después de crear una marca hay que saber administrarla óptimamente.
                    </div>
                &nbsp;
         <!--           <div class="heading">Contacto</div>
                    <div class="soc-icons">
                        <a href=""><i class="fa fa-facebook"></i> Facebook</a>
                        <a href=""><i class="fa fa-twitter"></i> Twitter</a> 
                        <a href=""><i class="fa fa-linkedin"></i> Linkedin</a>                      
                    </div>    -->              
                </div>
            </div>          
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 no-padding-sm">
                    <div class="footer-bottom">
                    <div class="container no-padding-sm">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 text-center text-lg-left">
                                <div class="copyrigh">Todos los derechos reservados. Identidad.com - 2017</div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <ul class="footer-menu">
                                    <li class="menu-item"><a href="aviso-privacidad.aspx">Aviso de privacidad</a></li>                             
                                    <li class="menu-item"><a href="terminos-condiciones.aspx">Términos y condiciones</a></li>                             
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
      <label id="label10" runat="server"></label>


    <%--<script src="js/jquery.min.js"></script>--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.5.0/js/conekta.js"></script>   


    <script src="js/bootstrap.min.js"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.circliful.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scripts.js"></script>

	<!--INICIA CÓDIGO DE GOOGLE ANALYTICS-->
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78976181-1', 'auto');
        ga('send', 'pageview');

	</script>
    
    <!--Termina CÓDIGO DE GOOGLE ANALYTICS-->
        
   <script type="text/javascript">

       // Conekta Public Key
       //Conekta.setPublishableKey('key_ByMFFx74HgvCZJRB8diLjRw');
       Conekta.setPublicKey('key_ByMFFx74HgvCZJRB8diLjRw'); //v5+ Sandbox
       //Conekta.setPublicKey('key_WZWh6zsPhfnwmUQYycWmLew'); //v5+ Producción
       // ...
       $(function () {
           $("#cardform").submit(function (event) {
               var $form = $("#cardform");
               
               // Previene hacer submit más de una vez
               $form.find("button").prop("disabled", true);
               //Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
               conektaSuccessResponseHandler("key_ByMFFx74HgvCZJRB8diLjRw");
               // Previene que la información de la forma sea enviada al servidor
               return false;
           });
       });

       var conektaSuccessResponseHandler = function (token) {
           var $form = $("#cardform");
           var cardtoken = token.id;
           //console.log(token);
          // alert(cardtoken);
           /* Inserta el token_id en la forma para que se envíe al servidor */
           $form.append($("<input type='hidden' name='conektaTokenId'>").val(token));//token.id
           $form.find("button").prop("disabled", true);
           /* and submit */
           var description = document.getElementById("plan").innerHTML;
           var currency = "USD";
           var nombre = document.getElementById("nombre").value;
           var apellidoM = document.getElementById("primerapellido").value;
           var desname = nombre + " " + apellidoM + " " + document.getElementById("segundoapellido").value;
           var desphone = document.getElementById("telefono").value;
           var rfc = document.getElementById("rfc").value;
           var razon = document.getElementById("rsocial").value;
           var calle = document.getElementById("calle").value;
           var numExt = document.getElementById("noext").value;
           var colonia = document.getElementById("colonia").value;
           var ciudad = document.getElementById("delegacion").value;
           var estado = document.getElementById("estado").value;
           var cp = document.getElementById("codigopostal").value;
           var desemail = document.getElementById("email").value;
           var pais = document.getElementById("pais").value;
           var itemdescription = "Plan identidad";
           var itemunitprice = amount;
           var itemquantity = 1;
           document.getElementById("Hidden1").innerText = itemname;
           
           if (!validaVacio(desphone, desemail, razon, rfc, nombre, apellidoM, calle, numExt, colonia, ciudad, estado, cp, pais)) {
               document.getElementById("pagar").style.backgroundColor = "rgba(255, 0, 152, 0.2)";
               document.getElementById("errorCampos").innerHTML = "Falta algún campo de llenar";
           }
           else {
               var sjson = "{\"description\": \" " + description + "\",\"amount\": **#_test_#**,\"currency\": \"USD\",\"card\": \"" + cardtoken + "\",\"details\": {\"name\": \"" + desname + "\",\"phone\": \"" + desphone + "\",\"email\": \"" + desemail + "\",\"line_items\": [{\"name\": \"" + itemname + "\",\"description\": \"" + description + "\",\"unit_price\": " + amount + ",\"quantity\": 1}]}}";
               var arr = [itemname, sjson]
               __doPostBack('placeorder', arr);
               $("#pagar").removeAttr("style");
               document.getElementById("pagar").removeAttribute("style");
               //document.getElementById("pagar").style.backgroundColor = "";
               document.getElementById("errorCampos").innerHTML = "";
               document.getElementById("errorCampos").value = "";          
           }
       };

       var conektaErrorResponseHandler = function (response) {
           var $form = $("#cardform");
           //alert("error");
           /* Muestra los errores en la forma */
           $form.find(".card-errors").text(response.message_to_purchaser);
           $form.find("button").prop("disabled", false);
       };

</script>
    
<script>
    function validaVacio(desphone, desemail, razon, rfc, nombre, apellidoM, calle, numExt, colonia, ciudad, estado, cp, pais) {
        if (desphone != "" && desemail != "" && razon != "" && nombre != "" && apellidoM != "" && calle != "" && numExt != "" && colonia != "" && ciudad != "" && estado != "" && cp != "") {
            if (pais == "MEX") {
                if (rfc != "") {
                    return true;
                }
                return false;
            }
            rfc = 'XEXX010101000';
            return true;
        }
        else
            return false;
    }

    function soloLetras(e) {
        key = e.keyCode || e.which;
        tecla = String.fromCharCode(key).toLowerCase();
        letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
        especiales = "8-37-39-46";

        tecla_especial = false
        for (var i in especiales) {
            if (key == especiales[i]) {
                tecla_especial = true;
                break;
            }
        }

        if (letras.indexOf(tecla) == -1 && !tecla_especial) {
            return false;
        }
    }
</script>

<script>
    $('document').ready(function () {
        if (document.getElementById("pais").value == "MEX") {
            document.getElementById("row1").removeAttribute("col-sm-12");
            document.getElementById("row1").className = "col-sm-6";
            document.getElementById("rfc").style.display = "block";
            document.getElementById("rfc").setAttribute("required", "true");

            document.getElementById("row2").removeAttribute("col-sm-12");
            document.getElementById("row2").className = "col-sm-6";
            document.getElementById("segundoapellido").style.display = "block";
            document.getElementById("primerapellido").placeholder = "Primer apellido *";
        }
        else {
            document.getElementById("row1").removeAttribute("col-sm-6");
            document.getElementById("row1").className = "col-sm-12";
            document.getElementById("rfc").removeAttribute("required");
            document.getElementById("rfc").style.display = "none";

            document.getElementById("row2").removeAttribute("col-sm-6");
            document.getElementById("row2").className = "col-sm-12";
            document.getElementById("segundoapellido").style.display = "none";
            document.getElementById("primerapellido").placeholder = "Apellidos *";
        }
    });
    function changeCountry() {
        if (document.getElementById("pais").value == "MEX") {
            document.getElementById("row1").removeAttribute("col-sm-12");
            document.getElementById("row1").className = "col-sm-6";
            document.getElementById("rfc").style.display = "block";
            document.getElementById("rfc").setAttribute("required", "true");

            document.getElementById("row2").removeAttribute("col-sm-12");
            document.getElementById("row2").className = "col-sm-6";
            document.getElementById("segundoapellido").style.display = "block";
            document.getElementById("primerapellido").placeholder = "Primer apellido *";
        }
        else {
            document.getElementById("row1").removeAttribute("col-sm-6");
            document.getElementById("row1").className = "col-sm-12";
            document.getElementById("rfc").removeAttribute("required");
            document.getElementById("rfc").style.display = "none";

            document.getElementById("row2").removeAttribute("col-sm-6");
            document.getElementById("row2").className = "col-sm-12";
            document.getElementById("segundoapellido").style.display = "none";
            document.getElementById("primerapellido").placeholder = "Apellidos *";
        }
    }
</script>   

<script type="text/javascript">
        function soloNumeros(e) {
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = "0123456789";
            especiales = ["8","37","39","46"]; //"8-37-39-46";

            tecla_especial = false
            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }

            var txtUrl = document.getElementById('ContentPlaceHolder1_txtUrl').value;
            document.getElementById('ContentPlaceHolder1_txtUrl').value = txtUrl.replace(/\s/g, '');

        }

        function setActive(active) {
            document.getElementById("Prueba").className = "wpc-btn";
            document.getElementById("Basico").className = "wpc-btn";
            document.getElementById("Empresarial").className = "wpc-btn";
            document.getElementById("Ilimitado").className = "wpc-btn";
            $(active).addClass("presionado");
            //$("active").removeClass("wpc-btn");
        }
    </script>
        

  </body>
<!-- InstanceEnd --></html>