﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Identidad.App_Code;
using System.Data.SqlClient;
using System.Data;

namespace Identidad.site
{
    public partial class login_registro : System.Web.UI.Page
    {
        #region Variables
        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        //HttpCookie passC = new HttpCookie("Password");
        HttpCookie cookie = new HttpCookie("Cookie");
        DateTime now = DateTime.Now;
        string ddlvalue;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (IsPostBack == false)
            {
                Session["bandera"] = "0";

                if (Request.Cookies["Cookie"] != null && Request.Cookies["Cookie"].Values.Count > 0)
                {
                    string[] datos = new string[2];
                    datos = oSistema.Desencriptar((Request.Cookies["Cookie"].Values["UserName"]), Request.Cookies["Cookie"].Values["Password"]);

                    txtcorreoIngreso.Attributes["value"] = datos[0];
                    txtpassIngreso.Attributes.Add("Value", datos[1]);
                    chkRemember.Checked = true; // <-- here
                    iniciarSesion();
                }
            }
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Request.Form["__EVENTTARGET"] == "login")
            {
                iniciarSesion();
            }
            if (Request.Form["__EVENTTARGET"] == "ir")
            {
                ddlvalue = Page.Request.Params["__EVENTARGUMENT"];
                btnGo_Click();
            }
            if (Request.Form["__EVENTTARGET"] == "recuperar")
            {
                btnRecuperarPass();
            }
            if (Request.Form["__EVENTTARGET"] == "registro")
            {
                btnAltaUsuario_Click();
            }

        }

        void SaveCookies()
        {
            string[] datos = new string[2];
            datos = oSistema.Encriptar(txtcorreoIngreso.Attributes["value"], txtpassIngreso.Attributes["value"]);

            cookie.Values["UserName"] = datos[0];
            cookie.Values["Password"] = datos[1];
            //cookie.Values["Expire"] = now.AddMonths(1).ToString();
            cookie.Expires = now.AddMonths(1);

            Response.Cookies.Add(cookie);
        }

        protected void iniciarSesion()
        {
            try
            {
                oMetodo = oUsuario.IniciarSesion(txtcorreoIngreso.Value, txtpassIngreso.Value);
                if (oMetodo.Pass == true)
                {
                    #region cookie
                    if (chkRemember.Checked)
                    {
                        SaveCookies();
                    }
                    else
                    {
                        //cookie.Values["Expire"] = now.AddDays(-1).ToString();
                        //Response.Cookies.Add(cookie);
                        Response.Cookies["Cookie"].Expires = DateTime.Now.AddDays(-1d);
                    }
                    #endregion

                    Session["bandera"] = "1";
                    Session["IdUsuario"] = oUsuario.getIdUsuario(txtcorreoIngreso.Value);
                    Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                    Session.Timeout = 480;
                    llenarCombo(Int32.Parse(Session["idUsuario"].ToString()));
                    
                    /*codigo de redireccion
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    Response.Redirect("~/Disenador/MisBrandSites.aspx", false);*/
                }
                else
                {
                    txtcorreoIngreso.Focus();

                    lblvalidacionL.Visible = true;
                    lblvalidacionL.InnerText = oMetodo.Message;
                }
                
            }
            catch
            {

            }
            
            //ClientScript.RegisterClientScriptBlock(this.GetType(), "ModalMisSitios", "<script> document.getElementById('myModal').style.display = 'block'; return false; alert('Beddy el bonito'); </script>");
        }

        protected void llenarCombo(int usuario)
        {
            ddlmissitios.Items.Clear();

            DataTable dt = new DataTable();

            //dr = cmd.ExecuteReader;
            SqlCommand cmd = new SqlCommand("selUsuariosPorBrandSite_MisSitios");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdUsuario", Session["IdUsuario"]);

            //dt=oSistema.CargarDataTable(cmd);

            DataTable dtTable = new DataTable();

            dtTable = oSistema.CargarDataTable(cmd);
            foreach (DataRow dtRow in dtTable.Rows)
            {
                string Nombre = dtRow[0].ToString();
                string Liga = dtRow[1].ToString();
                ListItem oItem = new ListItem(Nombre, Liga);
                ddlmissitios.Items.Add(oItem);

            }
        }

        protected void btnGo_Click()
        {

            try
            {
                if (ddlmissitios.Items.Count != 0)
                {
                    
                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                        string[] sUrl = ddlvalue.Split('=');
                        string sUrlFinal;

                        if (sUrl.Length == 1)
                        {
                            sUrlFinal = sUrl[0];
                        }
                        else
                        {
                            sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                        }

                        Response.Redirect("~/" + sUrlFinal, false);
                    
                    
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnRecuperarPass()//Funcion para recuperar la contraseña
        {
            try
            {

                oMetodo = oUsuario.RecuperarMiContraseña(txtcorreoIngreso.Value.ToString());
                lblvalidacionRP.InnerText = oMetodo.Message;
                lblvalidacionRP.Visible = true;
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnAltaUsuario_Click()
        {
            if (txtcorreo.Value != "" || txtnombre.Value != "" || txtapellidoP.Value != "" || txtcorreoconfirma.Value != "" || txtpass.Value != "" || txttpassconfirma.Value != "")
            {
                if (txtcorreo.Value == txtcorreoconfirma.Value)
                {
                    if (oSistema.CorreoElectronicoValido(txtcorreo.Value.ToString()))
                    {
                        if (oUsuario.getIdUsuario(txtcorreo.Value.ToString()) == 0)
                        {
                            if (txtpass.Value == txttpassconfirma.Value)
                            {
                                cStoredProcedure oStoredProcedure = new cStoredProcedure("traUsuarios_Registro");

                                oStoredProcedure.agregarParametro("@Nombre", txtnombre.Value);
                                oStoredProcedure.agregarParametro("@Apellidos", txtapellidoP.Value);
                                //oStoredProcedure.agregarParametro("@ApellidoMaterno", txtapellidoM.Value);
                                oStoredProcedure.agregarParametro("@CorreoElectronico", txtcorreo.Value);
                                oStoredProcedure.agregarParametro("@Contraseña", txtpass.Value);
                                oStoredProcedure.agregarParametro("@RecibirPromos", chkPromos.Checked);
                                oStoredProcedure.executeNonQuery();

                                Session["bandera"] = "1";
                                Session["IdUsuario"] = oUsuario.getIdUsuario(txtcorreo.Value);
                                Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                                Session.Timeout = 480;
                                llenarCombo(Int32.Parse(Session["idUsuario"].ToString()));
                                //txtnombre.Value = "";
                                //txtapellidoP.Value = "";
                                //txtapellidoM.Value = "";
                                //txtcorreo.Value = "";

                                //lblmsg.Text = "Nuevo Diseñador dado de Alta";
                            }
                            else
                            {
                                lblvalidacionR.InnerText = "Los campos de las contraseñas deben ser iguales";
                                lblvalidacionR.Visible = true;
                            }
                        }
                        else
                        {
                            lblvalidacionR.InnerText = "El correo ya exite, pruebe con un correo diferente";
                            lblvalidacionR.Visible = true;
                        }
                    }
                    else
                    {
                        lblvalidacionR.InnerText = "Ingresa un correo valido";
                        lblvalidacionR.Visible = true;
                    }
                   
                
                }
                else
                {
                    lblvalidacionR.InnerText = "Los campos de los correos deben ser iguales";
                    lblvalidacionR.Visible = true;
                }
            }
            else
            {
                lblvalidacionR.InnerText = "Los campos no deben estar vacios";
                lblvalidacionR.Visible = true;
            }
        }
    }
}