﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Identidad.site
{
    public partial class aviso_privacidad : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
    }
}