﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Identidad.App_Code;
using System.Data.SqlClient;
using System.Data;

namespace Identidad.site
{
    public partial class formato_registro_cuenta : System.Web.UI.Page
    {
        #region Variables
        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        string ddlvalue;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            if (!IsPostBack)
            {
                Session["bandera"] = "0";
            }
            
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Request.Form["__EVENTTARGET"] == "registro")
            {
                registro();
            }
            if (Request.Form["__EVENTTARGET"] == "irSitio")
            {
                ddlvalue = Page.Request.Params["__EVENTARGUMENT"];
                btnGo_Click();
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }




        protected void registro()
        {
            if (txtcorreo.Value != "" && txtnombre.Value != "" && txtapellidoP.Value != "" && txtcorreoconfirma.Value != "" && txtpass.Value != "" && txtpassconfirma.Value != "")
            {
                if (txtcorreo.Value == txtcorreoconfirma.Value )
                {
                    if (oSistema.CorreoElectronicoValido(txtcorreo.Value.ToString()))
                    {
                        if (oUsuario.getIdUsuario(txtcorreo.Value.ToString()) == 0)
                    {
                        if (txtpass.Value == txtpassconfirma.Value)
                        {
                            cStoredProcedure oStoredProcedure = new cStoredProcedure("traUsuarios_Registro");

                            oStoredProcedure.agregarParametro("@Nombre", txtnombre.Value);
                            oStoredProcedure.agregarParametro("@Apellidos", txtapellidoP.Value);
                            //oStoredProcedure.agregarParametro("@ApellidoMaterno", txtapellidoM.Value);
                            oStoredProcedure.agregarParametro("@CorreoElectronico", txtcorreo.Value);
                            oStoredProcedure.agregarParametro("@Contraseña", txtpass.Value);
                            oStoredProcedure.agregarParametro("@RecibirPromos", mailing.Checked);
                            oStoredProcedure.executeNonQuery();

                            Session["bandera"] = "1";
                            Session["IdUsuario"] = oUsuario.getIdUsuario(txtcorreo.Value);
                            Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                            Session.Timeout = 480;
                            llenarCombo(Int32.Parse(Session["idUsuario"].ToString()));
                            //txtnombre.Value = "";
                            //txtapellidoP.Value = "";
                            //txtapellidoM.Value = "";
                            //txtcorreo.Value = "";

                            //lblmsg.Text = "Nuevo Diseñador dado de Alta";
                        }
                        else
                        {
                            lblvalidacionR.InnerText = "Los campos de las contraseñas deben ser iguales";
                            lblvalidacionR.Visible = true;
                        }
                    }
                    else
                    {
                        lblvalidacionR.InnerText = "El correo ya exite, pruebe con un correo diferente";
                        lblvalidacionR.Visible = true;
                    }
                    }
                    else
                    {
                        lblvalidacionR.InnerText = "Ingresa un correo valido";
                        lblvalidacionR.Visible = true;
                    }
                    
                }
                else
                {
                    lblvalidacionR.InnerText = "Los campos de los correos deben ser iguales";
                    lblvalidacionR.Visible = true;
                }
            }
            else
            {
                lblvalidacionR.InnerText = "Los campos no deben estar vacios";
                lblvalidacionR.Visible = true;
            }
        }

        protected void llenarCombo(int usuario)
        {
            ddlmissitios.Items.Clear();

            DataTable dt = new DataTable();

            //dr = cmd.ExecuteReader;
            SqlCommand cmd = new SqlCommand("selUsuariosPorBrandSite_MisSitios");
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdUsuario", Session["IdUsuario"]);

            //dt=oSistema.CargarDataTable(cmd);

            DataTable dtTable = new DataTable();

            dtTable = oSistema.CargarDataTable(cmd);
            foreach (DataRow dtRow in dtTable.Rows)
            {
                string Nombre = dtRow[0].ToString();
                string Liga = dtRow[1].ToString();
                ListItem oItem = new ListItem(Nombre, Liga);
                ddlmissitios.Items.Add(oItem);

            }
        }

        protected void btnGo_Click()
        {
            try
            {
                if (ddlmissitios.Items.Count != 0)
                {

                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string[] sUrl = ddlvalue.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);


                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
    }
}