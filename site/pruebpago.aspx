﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pruebpago.aspx.cs" Inherits="Identidad.site.pruebpago" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.5.0/js/conekta.js"></script>

    <script type="text/javascript">
 
        // Conekta Public Key
        //Conekta.setPublishableKey('key_ByMFFx74HgvCZJRB8diLjRw'); //v3.2
        Conekta.setPublicKey('key_ByMFFx74HgvCZJRB8diLjRw'); //v5+
 
        // ...

        $(function () {
            $("#cardform").submit(function (event) {
                var $form = $(this);
                alert("primero");

                // Previene hacer submit más de una vez
                $form.find("button").prop("disabled", true);
                //Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                Conekta.Token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler); //v5+
                alert("cuarto");
                // Previene que la información de la forma sea enviada al servidor
                return false;
            });
        });

        var conektaSuccessResponseHandler = function (token) {
            var $form = $("#cardform");
            alert("segundo");
            /* Inserta el token_id en la forma para que se envíe al servidor */
            $form.append($("<input type='hidden' name='conektaTokenId'>").val(token.id));
            var cardtoken = token.id
            alert(cardtoken);
            //__doPostBack("pagar", "");
            __doPostBack('Pagar_Click', sjson);
            /* and submit */
            //$form.get(0).submit();
        };

        var conektaErrorResponseHandler = function (response) {
            var $form = $("#cardform");
            alert("tercero");
            /* Muestra los errores en la forma */
            $form.find(".carderrors").text(response.message_to_purchaser);
            $form.find("button").prop("disabled", false);
        };

</script>

    <%--<script type="text/javascript">
        function pagar() {
            alert("pagar");
            __doPostBack("pagar", "");
        }
    </script>--%>

</head>
<body>
    <form runat="server" id="cardform">
  <span class="carderrors"></span>
  <div class="form-row">
    <label>
      <span>Nombre del tarjetahabiente</span>
      <input type="text" size="20" data-conekta="card[name]"/>
    </label>
  </div>
  <div class="form-row">
    <label>
      <span>Número de tarjeta de crédito</span>
      <input type="text" size="20" data-conekta="card[number]"/>
    </label>
  </div>
  <div class="form-row">
    <label>
      <span>CVC</span>
      <input type="text" size="4" data-conekta="card[cvc]"/>
    </label>
  </div>
  <div class="form-row">
    <label>
      <span>Fecha de expiración (MM/AAAA)</span>
      <input type="text" size="2" data-conekta="card[exp_month]"/>
    </label>
    <span>/</span>
    <input type="text" size="4" data-conekta="card[exp_year]"/>
  </div>

  <button type="submit">¡Pagar ahora!</button>
</form>
</body>
</html>
