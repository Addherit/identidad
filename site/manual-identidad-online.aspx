﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="manual-identidad-online.aspx.cs" Inherits="Identidad.site.manual_identidad_online" %>

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/template-identidad-pag.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Manual de identidad en linea. Tu manual de marca online para ti y tu equipo. - identidad.com</title>
    <style type="text/css">
    </style>
    <!-- InstanceEndEditable -->
    <meta name="description" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/fopen-sans.css" rel="stylesheet">
    <link href="css/raleway-webfont.css" rel="stylesheet">
    <link href="css/lato.css" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="img/favicon.ico" rel="shortcut icon">
    <link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
  </head>
  <body>


	<!-- 
	*******************
      	HEADER    
	****************** 
	-->
    
    
	<!--<div class="wpc-main-header">
	  <div class="container no-padding-sm">
	    <div class="row">
	      <div class="col-sm-6 no-padding">
	      <<div class="soc-icons"> <a href=""><i class="faTop fa-facebook"></i></a> <a href=""><i class="faTop fa-twitter"></i></a> <a href=""><i class="faTop fa-linkedin"></i></a> </div>
          </div>
	      <div class="col-sm-6 no-padding-right">
	        <section class="header-info">
	          <h6> <a href="../login-registro.aspx"><i class="fa"><img src="../img/icon-2.png" alt="icon"></i>e</a> <a href="#"><i class="fa fa-location-arrow"></i>Soporte</a> <a href="tel:+527773138466"><i class="fa fa-mobile"></i>6</a> <a href="mailto:info@identidad.com"><i class="fa fa-envelope"></i>info@identidad.com</a> </h6>
            </section>
          </div>
        </div>
      </div>
    </div>-->
    
    
    
	<!--
	*******************
       LOGIN   
	****************** 
	-->
	
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<!-- form itself -->
		<form action="#" id="test-form" class="mfp-hide white-popup-block login-form">
		
			<div class="row">
				<div class="col-sm-6 col-sm-offset-1">
					<div class="wrapper">
						<h3 class="title">Log in</h3>
						<div class="subtitle">Please fill in your basic info:</div>
						<div><input type="text" name="username" placeholder="Username" class="field" autocomplete="off" required></div>
						<div><input type="password" name="password" placeholder="Password" class="field" autocomplete="off" required></div>
					</div>
				</div>
				<div class="col-sm-4 text-xs-center">
					<label class="label-form"><input type="checkbox" class="btn-check">Remember me</label>
					<input type="submit" class="wpc-btn" value="log in">
				</div>
			</div>
			
		</form>
			</div>
		</div>
	</div>
	


	
<!-- 
	*******************
      NAV MENU    
	****************** 
	-->


	<div class="wpc-menu-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 no-padding">
					<a href="index.aspx"><img src="img/ID_FirmaBETA_3.png" alt="logo" class="logo-menu"></a>
				</div>
				<div class="col-sm-9 col-lg-7 col-lg-offset-2 no-padding">
					<nav class="wpc-nav-menu">
						<button class="menu-toggle">
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    	</button>
					    <ul class="main-menu">
					        <li class="menu-item">
					        	<a href="manual-identidad-online.aspx">La plataforma</a>
				        	</li>
					        <li class="menu-item">
					        	<a href="guia-marca-online-precios.aspx">Los planes</a>
				        	</li>
					        <li class="menu-item">
					            <a href="login-registro.aspx">Ingresa</a>
					        </li>
					        <li class="menu-item">
					            <a href="ayuda-faq.aspx">Ayuda</a>
					        </li>
					        <li class="menu-item"><a href="contacto.aspx">Contacto</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
	
	<!-- InstanceBeginEditable name="Main Content Site" -->
	<div id="Editable Content">



	<!-- 
	*******************
		TOP BANNER
	****************** 
	-->

	<div class="wpc-top-header overlay img-bg">
		<img src="img/id_plataforma.jpg" alt="banner" class="hidden">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="wrapper">
						<div class="heading">La plataforma</div>
						<!--<div class="subheding">
							<a href="index.aspx" class="link">Inicio</a><span class="round"></span>
							<a href="manual-identidad-online.aspx" class="link active">La plataforma</a><span class="round"></span>
							<a href="" class="link"  >Los precios</a>
						</div>-->
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- 
	*******************
	  SERVICES SKILLS
	****************** 
	-->

	<div class="wpc-service-skills">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="section-heading small-text">
						Organiza, respalda y comparte<br> todos
                        los recursos <span class="highlight">de tu marca.</span>
				  </div>
					<div class="section-subheading">
					Descubre todas las ventajas que <span>identidad.com</span> tiene<br> para la gestión de tus proyectos de identidad corporativa. </div>
				</div>
			</div>
        </div>
     </div>
            
 	
	<div class="wpc-host-plans">
		<div class="container no-padding-sm">
			<div class="row">
				<div class="col-md-5 col-md-push-7 text-right text-xs-center text-sm-center">
					<div class="wrap-img">
						<img src="img/id-manual-identidad-marca-12.png" alt="manual de identidad en linea" class="img img-responsive">
					</div>
				</div>
				<div class="col-md-5 col-md-pull-5 no-padding-sm text-xs-center text-sm-center">
					<div class="heading">
						La mejor forma de organizar a la organización
					</div>
					<div class="subheading">
						Un programa de identidad requiere de decenas o cientos de archivos entre logos, tipografías, archivos de aplicaciones, fotografías e ilustraciones. En identidad.com puedes organizar todos estos archivos por carpeta y añadir a cada uno una imagen de referencia, una descripción, y hasta etiquetarlos con tags personalizados. Contarás con todos esos recursos para encontrarlos y usarlos en instantes.
					</div>
				</div>							
			</div>
			<div id="features2"  class="row">
				<div class="col-md-6 no-padding text-xs-center text-sm-center">
					<div class="wrap-img">
						<img src="img/id-manual-marca-online-14.png" alt="icon" class="img img-responsive">
					</div>
				</div>
				<div class="col-md-6 no-padding-sm text-xs-center text-sm-center">
					<div class="heading">
						Seremos tu respaldo
					</div>
					<div class="subheading">
						Olvídate de DVDs defectuosos, discos duros dañados, computadoras robadas o excolegas incomunicados, cuando todo está respaldado puedes estar tranquilo de que los recursos de tu identidad no desaparecerán. Y con esa confianza, no hace falta que los archivos se multipliquen descontroladamente. Siempre tendrás a la mano el correcto.</div>
                   	
				</div>
			</div>
			<div id="features2"  class="row">
				<div class="col-md-5 col-md-push-7 text-right text-xs-center text-sm-center">
					<div class="wrap-img wrap">
						<img src="img/id-manual-identidad-11.png" alt="icon" class="img img-responsive">
					</div>
				</div>
				<div class="col-md-6 col-md-pull-5 no-padding-sm text-xs-center text-sm-center">
					<div class="heading">
						Comparte con quien quieras
					</div>
					<div class="subheading">
						Ya sea que quieras mantener todo “en corto” con tus colegas inmediatos, o quieras compartir el manual y ciertos archivos con cientos de oficinas a nivel mundial, identidad.com te permite elegir qué haces público y a quién, y te asegura que cada actualización esté siempre cerca de todos. 
					</div>
				</div>							
			</div>
		</div>
	</div>


            
		 
	  </div>
	</div>


	<div class="wpc-services classic services">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="section-heading small-text">
						Funcionalidad para proyectos<br><span> y usuarios reales</span>
					</div>
					<h3 class="section-subheading"> En cuestión de horas puedes tener todos los recursos<br> de tu marca en identidad.com y dar acceso a tu equipo,<br> clientes o proveedores, con privilegios para que ellos compartan<br> o editen lo que necesiten. <span class="bold">¡Tú tienes el control!</span></h3>
				</div>
			</div>
		</div>
    </div>
        <div class="wpc-cloud">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_manual_estilos.png" alt="estilos de marca" class="img img-responsive">
					
						</div>						
						<div class="content-wrap">
							<h3 class="heading">Agrega tu voz y estilo<br></h3>
							<div class="desc">
								Comienza a trabajar a partir de una plantilla de contenido y texto que puedes conservar o cambiar. Puedes personalizarla y dotarla del diseño de tu marca con fuentes tipográficas, imágenes propias y colores.
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_manual_plantillas.png" alt="manual de identidad y de marca con plantillas" class="img img-responsive">
					
						</div>						
						<div class="content-wrap">
							<h3 class="heading">Personaliza cada sección<br></h3>
							<div class="desc">
								Al modificar la plantilla cuentas con diversas opciones, desde secciones de texto solo, imágenes a una o dos columnas o combinaciones de todas éstas. Puedes añadir, borrar o modificar a tu antojo. 
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 col-sm-6 clear-sm">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_Gifanimado.gif" alt="photo" class="img img-responsive">
				
						</div>						
						<div class="content-wrap">
							<h3 class="heading">GIFs animados</h3>
							<div class="desc">
								Puedes añadir animaciones a tu contenido para darle mayor expresión gráfica a tus manuales de identidad. Los GIFs, como cada imagen, pueden estar ligados a archivos de descarga. Baja el correcto con un par de clics.
							</div>
						</div>
					</div>
				</div>
                </div>
                <div class="row">
                <div id="features" class="col-md-4 col-sm-6 clear-sm">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_manual_tu_marca.png" alt="tu manual de marca online" class="img img-responsive">
					
						</div>						
						<div class="content-wrap">
							<h3 class="heading">URL propio y memorable</h3>
							<div class="desc">
								Tan fácil como recordar el nombre de tu marca y el sufijo identidad.com. El URL de tu sitio (tumarca.identidad.com) será tu nueva dirección en la nube con la oficialidad y neutralidad que otros sitios no pueden dar. 
							</div>
						</div>
					</div>
				</div>
                <div id="features" class="col-md-4 col-sm-6 clear-sm">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_manual_marca_glosario.png" alt="manual de marca con glosario" class="img img-responsive">
					
						</div>						
						<div class="content-wrap">
							<h3 class="heading">Glosario de términos</h3>
							<div class="desc">
								Lo que unos llaman “logo” otros llaman “marca”, otros “isologo” y otros “firma”. Utiliza el glosario para poner a todos de acuerdo en tu organización y evitar confusiones. Usa el texto modelo y mantenlo o modifícalo a tu antojo.
							</div>
						</div>
					</div>
				</div>
                <div id="features" class="col-md-4 col-sm-6 clear-sm">
					<div class="wrapper">
						<div class="img-wrap">
							<img src="img/IDE_manual-marca-comparte.png" alt="manual de identidad compartido con el equipo" class="img img-responsive">
				
						</div>						
						<div class="content-wrap">
							<h3 class="heading">Colabora a distancia</h3>
							<div class="desc">
								Invita a colaborar a quien necesites y dales los privilegios que desees. Algunos podrán invitar a otros o no. Define los usuarios que pueden ver cada carpeta o archivos.<br> Tú decides.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
       
    
           <!-- <div class="row">
				<div class="col-sm-6 col-sm-offset-3 no-padding-sm">
					<form action="#" class="payment-form">						
						<div class="text-center">
							<a href="formato-registro-cuenta.aspx" class="wpc-btn">Regístrate y elige tu plan</a>
                        </div>
					</form>
			</div>
   		 </div>-->

    </div>
    <!--editable content div-->
	<!-- InstanceEndEditable -->
    
    
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
    
    
    
    
    <!-- 
	*******************
		SUBSCRIBE
	****************** 
	-->

	
    
<!--	<div class="wpc-subcribe">	
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-md-offset-1 col-xs-12">
					<h1 class="heading" for="mce-EMAIL">Suscríbete a nuestro newsletter</h1>
				</div>
			</div>
			<div class="row" id="mc_embed_signup">
				<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-7 col-md-offset-1 col-sm-7 col-xs-12" id="mc_embed_signup_scroll">
						<input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Correo electrónico" class="email-input" required>
					</div>
					<div class="col-sm-4 col-xs-12  text-xs-center">
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="wpc-btn">
					</div>
				</form>
			</div>
		</div>
	</div>-->
    
<!-- 
<div id="mc_embed_signup">
<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>*/-->


	<!-- 
	*******************
		FOOTER
	****************** 
	-->

	<footer class="wpc-footer">
        <div class="container no-padding-sm">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="index.aspx" style="margin-right: 45px;"><img src="img/id_icono_40x40px.png" alt="logo"></a>
                    <a href="https://www.facebook.com/identidad.com.manual" target="_blank"><img src="img/facebook.jpg" alt="logo" style="width:45px;"></a>
                    <a href="https://twitter.com/identidadcom" target="_blank"><img src="img/twitter.jpg" alt="logo" style="width:45px;"></a>
 <!--                   <p class="contact"><i class="flaticon-location"></i></p> 
                    <p class="contact"><a href=""><i class="flaticon-letter"></i></a></p> 
                    <p class="contact"><a href="tel:"><i class="flaticon-technology-1"></i></a></p>-->
                </div>
                <div class="col-md-4 col-sm-6">
                	&nbsp;
           <!--         <div class="heading">Nuestras páginas</div>
                    <ul class="footer-menu classic">
                        <li class="menu-item"><a href="../index.aspx">Inicio</a> </li>                             
                        <li class="menu-item"><a href="../manual-identidad-online.aspx">La plataforma</a> </li>
                        <li class="menu-item"><a href="../guia-marca-online-precios.aspx">Los precios</a></li>
                        <li class="menu-item"><a href="../login-registro.aspx">Accede / Login</a></li>
                        <li class="menu-item"><a href="../contacto.aspx">Contacto</a></li>
                    </ul>-->
                </div>
                <div class="col-md-4 col-sm-12 text-xs-center text-sm-center">
                   	<div class="footer-info">
  Después de crear una marca hay que saber administrarla óptimamente.
                    </div>
                &nbsp;
         <!--           <div class="heading">Contacto</div>
                    <div class="soc-icons">
                        <a href=""><i class="fa fa-facebook"></i> Facebook</a>
                        <a href=""><i class="fa fa-twitter"></i> Twitter</a> 
                        <a href=""><i class="fa fa-linkedin"></i> Linkedin</a>                      
                    </div>    -->              
                </div>
            </div>          
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 no-padding-sm">
                    <div class="footer-bottom">
                    <div class="container no-padding-sm">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 text-center text-lg-left">
                                <div class="copyrigh">Todos los derechos reservados. Identidad.com - 2017</div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <ul class="footer-menu">
                                    <li class="menu-item"><a href="aviso-privacidad.aspx">Aviso de privacidad</a></li>                             
                                    <li class="menu-item"><a href="terminos-condiciones.aspx">Términos y condiciones</a></li>                             
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.circliful.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scripts.js"></script>

	<!--INICIA CÓDIGO DE GOOGLE ANALYTICS-->
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78976181-1', 'auto');
        ga('send', 'pageview');

	</script>
    
    <!--TERMINA CÓDIGO DE GOOGLE ANALYTICS-->
    
  </body>
<!-- InstanceEnd --></html>