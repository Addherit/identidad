﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Identidad.App_Code;

namespace Identidad.site
{
    public partial class index : System.Web.UI.Page
    {
        #region variables
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {   //redireccion a https
            if (!Request.IsLocal && !Request.IsSecureConnection)
            {
                string redirectUrl = Request.Url.ToString().Replace("http:", "https:");
                Response.Redirect(redirectUrl, false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }

            if (!IsPostBack)
            {
                ObtenerSubDominio();
            }
         /*   string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
             
        */
           
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
            Response.Redirect(redirectUrl1, false);
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }



        private void ObtenerSubDominio()
        {
            try
            {
                string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;

                string sNombreBrandSite = oSistema.ObtenerSubdominio(sUrl); //url.Substring(7).Split('.')[0];

                if (sNombreBrandSite != "identidad")
                {
                    int IdBrandSite = oBrandSite.ObtenerIdBrandSite(sNombreBrandSite);

                    if (IdBrandSite != 0)
                    {
                        string sIdBrandSiteEncriptado = oSistema.EncriptarId(IdBrandSite);

                        Response.Redirect("~/Brandsite/Login.aspx?Id=" + sIdBrandSiteEncriptado, false);
                    }
                }else{
                    string redirectUrl1 = Request.Url.ToString().Replace("index", "login-registro");
                    Response.Redirect(redirectUrl1, false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }
            catch (Exception e)
            {
                oSistema.saveLog(e.ToString());
            }
        }
    }
}