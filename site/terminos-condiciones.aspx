﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="terminos-condiciones.aspx.cs" Inherits="Identidad.site.terminos_condiciones" %>

<!DOCTYPE html>
<html lang="en"><!-- InstanceBegin template="/Templates/template-identidad-pag.dwt" codeOutsideHTMLIsLocked="false" -->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- InstanceBeginEditable name="doctitle" -->
    <title>Manual de identidad en linea. Tu manual de marca online para ti y tu equipo. - identidad.com</title>
    <!-- InstanceEndEditable -->
    <meta name="description" content="">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/flaticon.css" rel="stylesheet">
    <link href="css/fopen-sans.css" rel="stylesheet">
    <link href="css/raleway-webfont.css" rel="stylesheet">
    <link href="css/lato.css" rel="stylesheet">
    <link href="css/idangerous.swiper.css" rel="stylesheet">
    <link href="css/magnific-popup.css" rel="stylesheet">
    <link href="img/favicon.ico" rel="shortcut icon">
    <link href="css/style.css" rel="stylesheet">
	<link href="css/responsive.css" rel="stylesheet">
	<!-- InstanceBeginEditable name="head" -->
	<!-- InstanceEndEditable -->
  </head>
  <body>


	<!-- 
	*******************
      	HEADER    
	****************** 
	-->
    
    
	<!--<div class="wpc-main-header">
	  <div class="container no-padding-sm">
	    <div class="row">
	      <div class="col-sm-6 no-padding">
	      <<div class="soc-icons"> <a href=""><i class="faTop fa-facebook"></i></a> <a href=""><i class="faTop fa-twitter"></i></a> <a href=""><i class="faTop fa-linkedin"></i></a> </div>
          </div>
	      <div class="col-sm-6 no-padding-right">
	        <section class="header-info">
	          <h6> <a href="../login-registro.aspx"><i class="fa"><img src="../img/icon-2.png" alt="icon"></i>e</a> <a href="#"><i class="fa fa-location-arrow"></i>Soporte</a> <a href="tel:+527773138466"><i class="fa fa-mobile"></i>6</a> <a href="mailto:info@identidad.com"><i class="fa fa-envelope"></i>info@identidad.com</a> </h6>
            </section>
          </div>
        </div>
      </div>
    </div>-->
    
    
    
	<!--
	*******************
       LOGIN   
	****************** 
	-->
	
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-6">
				<!-- form itself -->
		<form action="#" id="test-form" class="mfp-hide white-popup-block login-form">
		
			<div class="row">
				<div class="col-sm-6 col-sm-offset-1">
					<div class="wrapper">
						<h3 class="title">Log in</h3>
						<div class="subtitle">Please fill in your basic info:</div>
						<div><input type="text" name="username" placeholder="Username" class="field" autocomplete="off" required></div>
						<div><input type="password" name="password" placeholder="Password" class="field" autocomplete="off" required></div>
					</div>
				</div>
				<div class="col-sm-4 text-xs-center">
					<label class="label-form"><input type="checkbox" class="btn-check">Remember me</label>
					<input type="submit" class="wpc-btn" value="log in">
				</div>
			</div>
			
		</form>
			</div>
		</div>
	</div>
	


	
<!-- 
	*******************
      NAV MENU    
	****************** 
	-->


	<div class="wpc-menu-wrap">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 no-padding">
					<a href="index.aspx"><img src="img/ID_FirmaBETA_3.png" alt="logo" class="logo-menu"></a>
				</div>
				<div class="col-sm-9 col-lg-7 col-lg-offset-2 no-padding">
					<nav class="wpc-nav-menu">
						<button class="menu-toggle">
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    		<span class="icon"></span>
				    	</button>
					    <ul class="main-menu">
					        <li class="menu-item">
					        	<a href="manual-identidad-online.aspx">La plataforma</a>
				        	</li>
					        <li class="menu-item">
					        	<a href="guia-marca-online-precios.aspx">Los planes</a>
				        	</li>
					        <li class="menu-item">
					            <a href="login-registro.aspx">Ingresa</a>
					        </li>
					        <li class="menu-item">
					            <a href="ayuda-faq.aspx">Ayuda</a>
					        </li>
					        <li class="menu-item"><a href="contacto.aspx">Contacto</a></li>
					    </ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
	
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
	
	<!-- InstanceBeginEditable name="Main Content Site" -->
	<div id="Editable Content">
    
    	<!-- 
	*******************
		TOP BANNER
	****************** 
	-->

	<div class="wpc-top-header overlay img-bg">
		<img src="img/top-banner.jpg" alt="banner" class="hidden">
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm-12">
					<div class="wrapper">
						<div class="heading">Términos y condiciones</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="wpc-history-section" style="margin-top: 0px;">
			<div class="container">
				<h2>Acuerdo de aceptación en los términos de uso</h2>
				<hr>
					<p>En Ideograma Consultores S.C. (en adelante “Ideograma”) sabemos que los tiempos y las marcas cambian. Entendemos que tus necesidades de diseño, y las de tus clientes y aliados, deben responder al ritmo acelerado de movilidad y presencia. Por ello, hemos volcado nuestro gran expertise en identidad corporativa para desarrollar identidad.com (en adelante Herramienta), una plataforma on-line que te permitirá gestionar tus manuales de identidad (subir y actualizar archivos, acceder y modificarlos desde el sitio) y mantenerte comunicado con tus clientes, proveedores y aliados.</p>

<p>Queremos agradecerte tu interés por nuestra plataforma y darte la bienvenida como usuario (en adelante “Usuario”). Antes de iniciar, es importante que leas, conozcas y consientas las siguientes condiciones de nuestros servicios (en adelante Condiciones), ya que al usar identidad.com las estás aceptando, aceptarás implícitamente las Condiciones estipuladas. Si alguno de los términos no se cumpliera, podría significar la terminación temporal o definitiva de  nuestros  servicios. 
</p>

<h3>Conoce más sobre nuestra herramienta</h3>
<p>Buscamos a los mejores para ofrecerte un sitio que siempre esté disponible. Nos hemos aliado con proveedores muy calificados para que nos proporcionen el hardware, software, redes, almacenamiento y tecnología relacionada necesarios para garantizarte una adecuada operatividad de la Herramienta.</p>  

<p>Nuestra Herramienta te permite tener tus materiales de identidad al alcance de todas  las personas que tú autorices como Usuarios, internos o externos, para brindarles acceso a cualquier archivo digital (audio, video, texto e imágenes) en tiempo real. </p>

<p>Éstos son otros de los beneficios que nuestra Herramienta te ofrece: </p>

<ul>
	<li>Centraliza toda tu información de marca en un solo espacio, incluso archivos “listos-para-usarse” para diseñadores, impresores y usuarios de programas de oficina. </li>
<li>Te permite mantener actualizados, permanentemente, los lineamientos identitarios, así como los 
archivos de uso.</li>
<li>Te brinda acceso a tu información desde las distintas plataformas y sistemas operativos: 
computadoras, tabletas, smart phones, laptops, etc.</li>
<li>Te posibilita llevar a cabo procesos para estructurar el contenido, escanear documentos, indexar
 y almacenar tus archivos digitales. 
</li>
</ul>

<p>Además, Identidad.com es una marca registrada y cuenta con las autorizaciones necesarias para utilizar las aplicaciones Web y los códigos fuente desarrollados para su funcionamiento.</p>
<h3>Tus responsabilidades como Usuario</h3>
<p>Déjanos conocerte. Deberás proporcionarnos tu nombre completo, una dirección vigente de correo electrónico y cualquier otra información requerida para completar oficialmente tu registro y que puedas hacer uso de la Herramienta. También deberás mantener actualizada tu cuenta a través de nuestro portal.</p>

<p>Protege lo más valioso. La seguridad de tu información está en tus manos. Tienes el compromiso de mantener y gestionar tus contraseñas, permisos y accesos a la Herramienta. Además, deberás darnos aviso inmediato en caso de robo, extravío, pérdida o apropiación por cualquier título, por terceros no autorizados de tus claves de acceso. </p>

<p>Comprométete con lo que dices y haces. Eres responsable de todo lo que subas, utilices y modifiques dentro del sitio, incluso si este contenido es cargado o actualizado por terceros usando tu cuenta. </p>

<p>Respeta las ideas de otros. Usa únicamente  contenido sobre el que tengas propiedad intelectual, industrial o derecho de uso concedido por sus titulares. Si no eres el titular  de la información, al menos deberás contar con los permisos correspondientes para utilizarla. No copies, subas, descargues ni compartas contenido que no te pertenece. Tenemos la capacidad y nos reservamos el derecho  de eliminar o inhabilitar cualquier contenido que presuntamente viole derechos, y a cancelar cuentas de Usuarios que incurran  en esta conducta. </p>

<p>Juega limpio. Evita manejar información que resulte discriminante, ofensiva, denigrante, inmoral o ilegal. Es muy importante que todos los datos que subas al sitio respeten a las personas y a las leyes correspondientes.</p>

<p>No pierdas. Es necesario que respaldes constantemente tu información, pues nosotros no asumiremos ninguna responsabilidad por la pérdida de tu contenido durante el uso de la Herramienta. </p>

<p>Hazlo funcionar. Deberás contar con el hardware o software necesarios para que la Herramienta funcione correctamente (Apple o PC Windows, conexión a internet y navegadores estándar).</p> 

<h3>Sobre tu contenido</h3>
<p>Tu información te pertenece por completo. El que nos proporciones contenido (archivos, mensajes, contactos, etc.) no nos concede ninguna propiedad sobre él. Únicamente tendremos los derechos limitados que permitirán que la Herramienta funcione correctamente.  </p>

<p>Nuestro servicio te ofrece funciones como: manejo y edición de imágenes, vistas previas, herramientas para ordenar contenido, editarlo, compartirlo o realizar búsquedas. Muchas veces, estas opciones requieren obtener tu información para almacenarla o examinarla. Al utilizar la Herramienta nos autorizas ampliamente a acceder a tu contenido únicamente para realizar estas actividades a tu favor; este permiso se extiende a terceras personas de confianza con las que podamos colaborar.</p>

<p>Tendremos el derecho, aunque no la obligación, de validar que tu información y su contenido no violen ninguna política mencionada en este documento. En ningún caso seremos responsables por el contenido que utilices o almacenes en la Herramienta. 
</p>

<h3>Sobre nuestra esencia: nuestro software </h3>

<p>Siempre que cumplas estas condiciones, te concederemos un servicio  limitado, revocable, no exclusivo e intransferible para usar la Herramienta, con la finalidad única de acceder a nuestros servicios.</p>

<p>Dicho servicio no te otorga ninguna exclusividad, derecho, titularidad o interés legítimo sobre la Herramienta,  su Contenido,  marca, nombre y/o aviso comercial, derechos de autor  u otros derechos referentes a identidad.com. 
</p>
<p>Tenemos el derecho de modificar, eliminar o adicionar cualquier elemento para garantizar que la página web y la Herramienta se adapten a nuevas tecnologías, sean más amigables y, en general, funcionen de manera óptima. Siempre que estés al día en tus pagos y existan nuevos componentes del software, ofrecidos bajo un código abierto, te daremos acceso a ella, pero toma en cuenta que esto puede modificar o anular algunas de las  condiciones de este documento. </p>

<p>Para brindarte un producto de primer nivel, subcontratamos servidores de gran capacidad. Esto no exenta la posibilidad de fallas o retrasos en la Herramienta, pero te aseguramos que siempre actuaremos de manera inmediata para resolver cualquier eventualidad y seguirte ofreciendo operatividad en tu servicio. Sin embargo, no podemos garantizarte que la Herramienta resolverá totalmente tus necesidades o cubrirá tus expectativas, ni que los resultados obtenidos a través de ella sean infalibles, pero haremos todo lo posible porque así sea. </p>

<p>Al usar nuestro servicio, aceptas no aplicar ingeniería inversa a la Herramienta ni descompilarla, o realizar ninguna actividad de investigación y/o copia de su configuración o código fuente. No podrás tampoco remover o modificar los avisos de propiedad intelectual y/o titularidad de la marca.</p>

<p>Recuerda que cada país tiene sus propias leyes de importación y exportación de software. Estás obligado a someterte a la jurisdicción que te corresponda y a cumplir con sus disposiciones. </p>

<p>Nos encanta saber de ti y estar en constante comunicación contigo. Agradeceremos cualquier comentario o sugerencia para la mejora de nuestro servicio y nuestra Herramienta. No obstante, ten en cuenta que podríamos utilizar tus recomendaciones sin que ello implique ninguna obligación hacia ti por nuestra parte. 
</p>

<h3>Sobre el pago y su vigencia</h3> 

<p>Buscamos los mejores recursos para mantener la Herramienta siempre actualizada e idónea a tus expectativas. Al inicio del servicio deberás cubrir a favor de Ideograma Consultores S.C. una anualidad correspondiente al costo del servicio que elijas previamente. Necesitamos, para ello, todo tu apoyo en realizar tus pagos en tiempo y forma para utilizar la Herramienta. El monto de la anualidad no será reembolsable bajo ninguna condición. Eres responsable de pagar cualquier impuesto que resulte aplicable; te lo cobraremos si estamos obligados a ello. La vigencia de tu anualidad iniciará una vez se realice el pago. </p>

<p>El pago antes mencionado deberás realizarlo a la siguiente referencia bancaría y deberás enviar tu comprobante al correo en mención: </p>
<p>Una vez realizado el pago y recibido tu comprobante de pago, recibirás una factura (CFDI) por el importe y concepto del servicio, el documento reunirá todos los requisitos legales. </p>

<p>En caso de que no efectúes tu pago, tenemos el derecho a congelar y deshabilitar tu cuenta. Asimismo, si no cumples con estas condiciones podremos cancelar tu servicio. En caso de que desees utilizar la Herramienta de nuevo, te aplicaremos las nuevas tarifas vigentes al momento de recontratar. </p>

<h3>Sobre las cancelaciones </h3>

<p>Puedes dejar de usar nuestra Herramienta cuando lo desees, sin costo alguno. Sólo podrás terminar el servicio a través de la liga correspondiente en la página Web; no consideraremos como una cancelación el que envíes un correo electrónico, hagas una llamada telefónica, etc. No importa cuándo revoques, no te haremos ningún reembolso.</p> 

<p>Una vez  el servicio sea cancelada o su vigencia vencida, ya no realizaremos ningún cargo pero no tendrás acceso a la Herramienta o a tu contenido almacenado. Por cuestiones de capacidad, sólo estamos obligados a mantener tu información archivada durante 30 días después de terminado nuestro contrato. Posterior a esta fecha, tus datos serán eliminados permanentemente y no seremos responsables de dicho contenido. Por ello recuerda que puedes renovar tu servicio y aún acceder a toda tu información siempre y cuando recontrates y pagues la cuota correspondiente  antes de que transcurra este plazo. </p>

<p>Cuidaremos siempre que tu uso de la Herramienta no provoque ninguna responsabilidad legal ni perturbe la utilización de los servicios por parte de otros usuarios. En caso de que violes cualquier acuerdo o hagas uso de la Herramienta para almacenar o crear información malintencionada, cancelaremos tu servicio y borraremos permanentemente toda tu información. De tal forma, nos reservamos el derecho a suspender o dar por terminados nuestros servicios en cualquier momento a nuestra discreción y sin previo aviso.
</p>
<p>No queremos causarte ningún contratiempo, por lo que te avisaremos a la dirección de correo asociada a tu cuenta antes de llevar a cabo cualquier acción de este tipo.
</p>
<p>Nos reservamos el derecho a brindar nuestro servicio a cualquier persona y por cualquier motivo que consideremos pertinente.</p> 

<h3>Sobre la renovación anual </h3>

<p>Deberemos recibir tu pago previo a los primeros 30 días naturales siguientes a la fecha en que termine la vigencia en curso. En caso contrario, podremos cancelar tu servicio y eliminar tu contenido, sin previo aviso y sin ninguna limitación o penalidad. </p>

<p>Si pagas de manera extemporánea, restableceremos tu servicio dentro de los 10 días naturales siguientes. Te recordamos que en estos casos, perderás de forma automática y sin bonificación el tiempo transcurrido que no utilizaste el servicio, pero nos dará mucho gusto tenerte de vuelta. </p>


<h3>Resolución de disputas </h3>

<p>Tan amigos como siempre. Tenemos las mejores intenciones al brindarte esta Herramienta y enarbolamos como objetivo principal el que siempre estés contento con nuestro servicio. Por ello, si existiese algún conflicto, te pedimos que, antes de recurrir a medidas legales, busquemos la manera de resolverlo entre nosotros, de manera amistosa y cordial.  
</p>
<p>Antes de generar una reclamación formal contra Ideograma, deberás intentar solucionar la disputa mediante el siguiente proceso informal: enviar un correo a la siguiente dirección legal@identidad.com, explicando tu inconformidad. Nos pondremos en contacto contigo para tratar de resolverla de manera expedita. En caso de que seamos nosotros los inconformes, te buscaremos en la dirección de correo asociada a tu cuenta. Si pasados 15 días naturales a partir del envío no ha sido resuelto el problema, cualquiera de las partes (tu o identidad.com) podrá registrar una reclamación formal.</p>

<p>Sólo en los casos en que sea necesario impedir abusos o usos no autorizados de la Herramienta o del contenido del Usuario, o si existiesen infracciones a la propiedad intelectual o industrial (por ejemplo, marcas registradas, secretos comerciales, patentes, derechos de autor, etc.), nosotros o tú podremos recurrir a reclamos formales ante organismos, autoridades o tribunales competentes antes de seguir el proceso informal aquí establecido. </p>

<p>Los tribunales competentes de la Ciudad de México, Distrito Federal, México serán los competentes  para resolver la interpretación de este acuerdo, así como para la resolución de los conflictos entre las partes.  Por lo tanto,  renuncias a cualquier otra jurisdicción que pueda corresponderte por razón de tus domicilios presentes o futuros o por cualquier otro motivo.</p>

<h3>Sobre las leyes aplicables </h3>

<p>Estas condiciones son regidas por el Código Civil Federal, Ley de protección de datos y las leyes federales de la República Mexicana concernientes a los derechos de autor y propiedad industrial.
</p>
<h3>Sobre nuestro soporte técnico </h3>

<p>Queremos que la Herramienta sea una plataforma para que tu trabajo siempre sea exitoso. Para ello, si tuvieras alguna duda o problema, te brindaremos apoyo gratuito mediante el correo electrónico sos@identidad.com, en los siguientes horarios de oficina: </p>

<p>Lunes a jueves de 8:00am a 14:00pm y de 15:30pm a 18:30pm<br>
Viernes de 8:00am a 13:00pm</p>

<h3>Sobre la sesión de derechos </h3>

<p>No podrás ceder a favor de terceras personas ninguno de los derechos vinculados a estas condiciones, al servicio, o a la utilización de la Herramienta. Cualquier intento de hacerlo será considerado nulo. </p>

<p>No estás autorizado para copiar o duplicar, total o parcialmente, cualquier elemento  de la Herramienta.</p> 

<p>Si te embargaran o adquirieran tus derechos sobre este contrato por cualquier medio, esa persona no tendrá ningún derecho a utilizarlos o cederlos.</p> 

<p>Ideograma podrá ceder sus derechos a cualquier persona física o moral, sin necesidad de autorización. 
</p>

<h3>Sobre nuestro límite de responsabilidad </h3>

<p>Estás consciente de que no somos responsables, explícita o implícitamente, por fallas en la Herramienta o por la pérdida total o parcial de tu información almacenada en ella, incluso si fuimos advertidos o no de la posibilidad de tales daños. </p>

<p>Si la legislación aplicable prohibiera la exclusión absoluta de responsabilidades para nosotros y fuéramos considerados causantes de los daños, la reclamación acumulada nunca podrá superar el monto que nos hayas pagado por los servicios referentes al uso de la Herramienta en el año que sucedieron los hechos motivantes de tu reclamación. </p>

<h3>Sobre nuestro Aviso de privacidad </h3>

<p>Los datos que nos proporciones sólo serán utilizados para la operación de este contrato. Puedes consultar nuestro aviso de privacidad en nuestra página web.</p> 
<h3>Sobre nuestro Acuerdo completo </h3>

<p>Estas cláusulas constituyen el acuerdo completo  entre el “Usuario” e “Ideograma”, y sustituye a cualquier otro acuerdo anterior o simultáneo, o cualquier otro término o condición aplicable a los asuntos tratados en este documento.</p>
 
<h3>Sobre la renuncia y divisiblidad </h3>

<p>En caso de que no apliquemos alguna disposición estipulada en este documento, no implica una renuncia a nuestro derecho a aplicarla posteriormente.
</p>
<p>Si alguna disposición se considera no aplicable, el resto de las cláusulas seguirán estando vigentes y se sustituirá por una nueva, que refleje la intención de las partes de la forma más fiel posible. </p>

<h3>Sobre las modificaciones </h3>

<p>Para mantener actualizados nuestros derechos y obligaciones, revisaremos este documento periódicamente. Siempre pondremos a tu alcance la versión más actualizada en nuestro sitio web. En caso de que los cambios a este contrato, en la “Herramienta” o en el servicio te afecten significativamente, te contactaremos para hacértelos saber. Si después de dichas modificaciones sigues utilizando nuestros servicios, aceptarás implícitamente las Condiciones estipuladas. </p>

<h2>¡Bienvenido!</h2>
<h3>Estamos felices de que formes parte de la familia de identidad.com</h3>




<br>
			</div>
		</div>
        
    </div>
    <!--editable content div-->
	<!-- InstanceEndEditable -->
    
    
    <!--HELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLDHELLOWORLD-->
    
    
    
    
    <!-- 
	*******************
		SUBSCRIBE
	****************** 
	-->

	
    
<!--	<div class="wpc-subcribe">	
		<div class="container">
			<div class="row">
				<div class="col-md-11 col-md-offset-1 col-xs-12">
					<h1 class="heading" for="mce-EMAIL">Suscríbete a nuestro newsletter</h1>
				</div>
			</div>
			<div class="row" id="mc_embed_signup">
				<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
					<div class="col-md-7 col-md-offset-1 col-sm-7 col-xs-12" id="mc_embed_signup_scroll">
						<input type="email" value="" name="EMAIL" id="mce-EMAIL" placeholder="Correo electrónico" class="email-input" required>
					</div>
					<div class="col-sm-4 col-xs-12  text-xs-center">
        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
                        <input type="submit" value="Enviar" name="subscribe" id="mc-embedded-subscribe" class="wpc-btn">
					</div>
				</form>
			</div>
		</div>
	</div>-->
    
<!-- 
<div id="mc_embed_signup">
<form action="//identidad.us13.list-manage.com/subscribe/post?u=41b015c2a7428b02a040a8b07&amp;id=797be0bfe7" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

    <div id="mc_embed_signup_scroll">
	<label for="mce-EMAIL">Subscribe to our mailing list</label>
	<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_41b015c2a7428b02a040a8b07_797be0bfe7" tabindex="-1" value=""></div>
    <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </div>*/-->


	<!-- 
	*******************
		FOOTER
	****************** 
	-->

	<footer class="wpc-footer">
        <div class="container no-padding-sm">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="index.aspx" style="margin-right: 45px;"><img src="img/id_icono_40x40px.png" alt="logo"></a>
                    <a href="https://www.facebook.com/identidad.com.manual" target="_blank"><img src="img/facebook.jpg" alt="logo" style="width:45px;"></a>
                    <a href="https://twitter.com/identidadcom" target="_blank"><img src="img/twitter.jpg" alt="logo" style="width:45px;"></a>
 <!--                   <p class="contact"><i class="flaticon-location"></i></p> 
                    <p class="contact"><a href=""><i class="flaticon-letter"></i></a></p> 
                    <p class="contact"><a href="tel:"><i class="flaticon-technology-1"></i></a></p>-->
                </div>
                <div class="col-md-4 col-sm-6">
                	&nbsp;
           <!--         <div class="heading">Nuestras páginas</div>
                    <ul class="footer-menu classic">
                        <li class="menu-item"><a href="../index.aspx">Inicio</a> </li>                             
                        <li class="menu-item"><a href="../manual-identidad-online.aspx">La plataforma</a> </li>
                        <li class="menu-item"><a href="../guia-marca-online-precios.aspx">Los precios</a></li>
                        <li class="menu-item"><a href="../login-registro.aspx">Accede / Login</a></li>
                        <li class="menu-item"><a href="../contacto.aspx">Contacto</a></li>
                    </ul>-->
                </div>
                <div class="col-md-4 col-sm-12 text-xs-center text-sm-center">
                   	<div class="footer-info">
  Después de crear una marca hay que saber administrarla óptimamente.
                    </div>
                &nbsp;
         <!--           <div class="heading">Contacto</div>
                    <div class="soc-icons">
                        <a href=""><i class="fa fa-facebook"></i> Facebook</a>
                        <a href=""><i class="fa fa-twitter"></i> Twitter</a> 
                        <a href=""><i class="fa fa-linkedin"></i> Linkedin</a>                      
                    </div>    -->              
                </div>
            </div>          
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 no-padding-sm">
                    <div class="footer-bottom">
                    <div class="container no-padding-sm">
                        <div class="row">
                            <div class="col-lg-6 col-xs-12 text-center text-lg-left">
                                <div class="copyrigh">Todos los derechos reservados. Identidad.com - 2017</div>
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <ul class="footer-menu">
                                    <li class="menu-item"><a href="aviso-privacidad.aspx">Aviso de privacidad</a></li>                             
                                    <li class="menu-item"><a href="terminos-condiciones.aspx">Términos y condiciones</a></li>                             
                                </ul>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/idangerous.swiper.min.js"></script>
    <script src="js/jquery.viewportchecker.min.js"></script>
    <script src="js/jquery.circliful.min.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/scripts.js"></script>

	<!--INICIA CÓDIGO DE GOOGLE ANALYTICS-->
    
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-78976181-1', 'auto');
        ga('send', 'pageview');

	</script>
    
    <!--TERMINA CÓDIGO DE GOOGLE ANALYTICS-->
    
  </body>
<!-- InstanceEnd --></html>