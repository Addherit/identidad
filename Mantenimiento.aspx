﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Mantenimiento.aspx.cs" Inherits="Identidad.mantenimiento" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Identidad.com | Mantenimiento</title>

    <link href="CSS/Site.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Menu.css" rel="stylesheet" type="text/css" />
    <link href="CSS/Pages/Mantenimiento.css" rel="stylesheet" type="text/css" />

    <link href='/Images/Site/ID.ico' rel='shortcut icon' type='image/x-icon'/>
    <meta name="application-name" content="Identidad.com"/>
    <meta name="application-tooltip" content="Identidad.com"/>
    <meta name="application-bavbutton-color" content="Blue"/>
    <link href="/Images/Site/ID.ico" rel="icon" type="image/ico"/>
	<link href="/Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon"/>
	<link rel="apple-touch-icon-precomposed" href="/Images/Site/ID.ico"/>
	<link rel="apple-touch-icon" href="/Images/Site/ID.ico"/>
    <link rel="apple-touch-startup-image" href="/Images/Site/ID.ico" />
    <meta name="apple-mobile-web-app-title" content="Identidad.com"/>
    <meta name='apple-mobile-web-app-capable' content='yes'/>
    <meta name='apple-touch-fullscreen' content='yes'/>
    <meta name='apple-mobile-web-app-status-bar-style' content='black'/>
    <link rel="icon" href="/Images/Site/ID.ico" />
	<link rel="shortcut icon" href="/Images/Site/ID.ico" />
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-56195706-1', 'auto');
        ga('send', 'pageview');

    </script>

</head>
<body>
<div id="page">
    <div id="contenido">
        <div class="divFondoPagina">
            <a href="site/index.aspx">
                <div style="margin-left:60px;">
                    <br /><br /><br /><br /><br />
                    <h1>ESTAMOS EN</h1>
                    <h2>CONSTRUCCIÓN</h2>
                    <br /><br /><br /><br /><br /><br /><br />
                </div>
            </a>
        </div>
    </div>   
</div>
</body>
</html>
