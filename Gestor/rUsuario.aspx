﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rUsuario.aspx.cs" Inherits="Identidad.Gestor.rUsuario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" onclick="cerrarSesion" href="#">Cerrar sesión</a>
                            <%--<asp:LinkButton ID="lkbCerrarSesion" CssClass="lkbCerrarSesion" OnClick="lkbCerrarSesion_Click" runat="server">Cerrar sesión</asp:LinkButton>--%>
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                    <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="AltaUsuarios.aspx">
                                <span id="lblBrandSites">Alta Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuario.aspx">
                                <span id="lblUsario">Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUxBrandSite.aspx">
                                <span id="lblUxBrandSite">UxBrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSite.aspx">
                                <span id="lblBrandSite">BrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">BrandSites</span>
                            </a>
                        </li>
                                               
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Reporte de Usuario</h1><br />
                         <form id="form2" runat="server">
                             <?php echo "Prueba PHP"?>
                             <asp:Label ID="lblRUsuario" runat="server" Text="Nombres de Usuarios"></asp:Label><br />
                             <asp:DropDownList ID="ddlUsuarios" runat="server" OnSelectedIndexChanged="ddlUsuarios_SelectedIndexChanged" AutoPostBack="True" ></asp:DropDownList>
                             <asp:GridView ID="gvwCountUsuario" runat="server" DataSourceID="dsContUsuario" BorderStyle="None" BorderColor="White">
                                 <HeaderStyle Font-Bold="True" ForeColor="#33CCFF" />
                             </asp:GridView>
                             <asp:SqlDataSource ID="dsContUsuario" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_RContUsuario" SelectCommandType="StoredProcedure">
                                 <SelectParameters>
                                     <asp:ControlParameter ControlID="ddlUsuarios" Name="IdUsuario" PropertyName="SelectedValue" Type="String" />
                                 </SelectParameters>
                             </asp:SqlDataSource>
                    <div><br />
        <asp:GridView ID="gvwrUsuario" runat="server" AllowSorting="True" AutoGenerateColumns="False" CssClass="mGrid tdGrid" DataKeyNames="IdBrandSite" DataSourceID="dsrUsuario">
            <Columns>
                <asp:BoundField DataField="NombreCompleto" HeaderText="NombreCompleto" ReadOnly="True" SortExpression="NombreCompleto" />
                <asp:TemplateField HeaderText="NombreSitio" SortExpression="NombreSitio">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NombreSitio") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "/Gestor/rBrandSite.aspx?i="+Eval("idBrandSite") %>' Text='<%# Eval("NombreSitio") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Url" SortExpression="Url">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Url") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl='<%# "http://"+Eval("Url") %>' Text='<%# Eval("Url") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Permisos" HeaderText="Permisos" ReadOnly="True" SortExpression="Permisos" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsrUsuario" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_RUsuario" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlUsuarios" Name="IdUsuario" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>  
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>
    <script type="text/javascript">
        function cerrarSesion() {
            __doPostBack('lkbCerrarSesion');
        }
    </script>
</body>
</html>
