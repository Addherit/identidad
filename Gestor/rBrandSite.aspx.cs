﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;


namespace Identidad.Gestor
{
    public partial class rBrandSite : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables


        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                if (Page.Request.Params["__EVENTTARGET"] == "GridView1")
                {
                    var Datos = Page.Request.Params["__EVENTARGUMENT"];
                    if (Datos.ToString() != "")
                    {
                        Response.Redirect("rBrandSite.aspx?i=" + Datos.ToString());
                        
                    }
                }
                ClientScript.GetPostBackEventReference(this, string.Empty);
                if (Request.Form["__EVENTTARGET"] == "lkbCerrarSesion")
                {
                    lkbCerrarSesion_Click();
                }
            }
            else
            {
                if (Session["Admin"] == null)
                {
                    Response.Redirect("../Admin.aspx");
                }
                string idBrandSite;
                try
                {
                    idBrandSite = Request["i"].ToString();
                }
                catch
                {
                    idBrandSite = "0";
                }
                SqlCommand cmdd = new SqlCommand("tra_ddlBrandSite");
                cmdd.CommandType = System.Data.CommandType.StoredProcedure;

                oSistema.CargarDropDownList(ddlBrandSite, cmdd);
                ddlBrandSite.SelectedValue = idBrandSite;


                //SqlCommand cmd = new SqlCommand("tra_RBrandSite");
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@IdBrandSite", idBrandSite);

                //oSistema.CargarGridView(GridView1, cmd);
                //try
                //{
                //    GridView1.HeaderRow.Cells[0].Visible = false;
                //    GridView1.HeaderRow.Cells[4].Visible = false;
                //    GridView1.HeaderRow.Cells[5].Visible = false;
                //    GridView1.HeaderRow.Cells[6].Visible = false;
                //}
                //catch { }
                
            }
        }

        //protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)//Carga el GridView
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //     {
        //                      if (e.Row.RowIndex > 0)
        //                      {
        //                          e.Row.Cells[0].Visible = false;
        //                          //e.Row.Cells[0].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[0].Text.ToString()));
        //                          //e.Row.Cells[1].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[0].Text.ToString()));
        //                          //e.Row.Cells[1].Attributes.Add("class", "cursorgvw");
        //                          e.Row.Cells[2].Attributes.Add("onclick", "javascript: window.open('http://" + e.Row.Cells[2].Text.ToString() + "','_blank')");
        //                          e.Row.Cells[2].Attributes.Add("class", "cursorgvw");
        //                          e.Row.Cells[4].Visible = false;
        //                          e.Row.Cells[5].Visible = false;
        //                          e.Row.Cells[6].Visible = false;
        //                      }
               
        //    }       

        //}


        protected void ddlBrandSite_SelectedIndexChanged(object sender, EventArgs e)//Refresca la página con algun cambio en el dropdownlist
        {
            SqlCommand cmd = new SqlCommand("tra_RBrandSite");
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@IdBrandSite", ddlBrandSite.SelectedValue.ToString());

            //oSistema.CargarGridView(GridView1, cmd);
            //try
            //{
            //    GridView1.HeaderRow.Cells[0].Visible = false;
            //    GridView1.HeaderRow.Cells[4].Visible = false;
            //    GridView1.HeaderRow.Cells[5].Visible = false;
            //    GridView1.HeaderRow.Cells[6].Visible = false;
            //}
            //catch { }

            gvwrBrandSite.DataBind();
        }

        protected void lkbCerrarSesion_Click()
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

      

        
    }
}