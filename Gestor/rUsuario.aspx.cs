﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Identidad.Gestor
{
    public partial class rUsuario : System.Web.UI.Page
    { // Estan son las variables que voy a usar en esta página :)
        #region Variables


        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ClientScript.GetPostBackEventReference(this, string.Empty);
                if (Request.Form["__EVENTTARGET"] == "lkbCerrarSesion")
                {
                    lkbCerrarSesion_Click();
                }
                if (Page.Request.Params["__EVENTTARGET"] == "GridView1")
                {
                    var Datos = Page.Request.Params["__EVENTARGUMENT"];
                    if (Datos.ToString() != "")
                    {
                        Response.Redirect("rBrandSite.aspx?i=" + Datos.ToString());

                    }
                } 
            }
            else
            {
                if (Session["Admin"] == null)
                {
                    Response.Redirect("../Admin.aspx");
                }
                string idUsuario;
                try
                {
                    idUsuario = Request["i"].ToString();
                }
                catch
                {
                    idUsuario = "0";
                }
                SqlCommand cmdd = new SqlCommand("tra_ddlUsuarios");
                cmdd.CommandType = System.Data.CommandType.StoredProcedure;

                oSistema.CargarDropDownList(ddlUsuarios, cmdd);
                ddlUsuarios.SelectedValue = idUsuario;

                //SqlCommand cmd = new SqlCommand("tra_RUsuario");
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@IdUsuario", idBrandSite);

                //oSistema.CargarGridView(GridView1, cmd);
                //try
                //{
                //    GridView1.HeaderRow.Cells[1].Visible = false;
                //}
                //catch { }
            }
        }

        //protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        e.Row.Cells[1].Visible = false;
        //        //e.Row.Cells[0].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[0].Text.ToString()));
        //        e.Row.Cells[2].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[1].Text.ToString()));
        //        e.Row.Cells[2].Attributes.Add("class", "cursorgvw");
        //        e.Row.Cells[3].Attributes.Add("onclick", "javascript: window.open('http://" + e.Row.Cells[3].Text.ToString() + "','_blank')");
        //        e.Row.Cells[3].Attributes.Add("class", "cursorgvw");
                
        //    }    

        //}

        protected void ddlUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            //SqlCommand cmd = new SqlCommand("tra_RUsuario");
            //cmd.CommandType = System.Data.CommandType.StoredProcedure;
            //cmd.Parameters.AddWithValue("@IdUsuario", ddlUsuarios.SelectedValue);

            //oSistema.CargarGridView(GridView1, cmd);
            //try
            //{
            //    GridView1.HeaderRow.Cells[1].Visible = false;
            //}
            //catch { }

            gvwrUsuario.DataBind();
        }

        protected void lkbCerrarSesion_Click()
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
    }
}