﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rBrandSite.aspx.cs" EnableEventValidation="false" Inherits="Identidad.Gestor.rBrandSite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" onclick="cerrarSesion" href="#">Cerrar sesión</a>
                            <%--<asp:LinkButton ID="lkbCerrarSesion" CssClass="lkbCerrarSesion" OnClick="lkbCerrarSesion_Click" runat="server">Cerrar sesión</asp:LinkButton>--%>
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                    <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="AltaUsuarios.aspx">
                                <span id="lblBrandSites">Alta Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuario.aspx">
                                <span id="lblUsario">Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUxBrandSite.aspx">
                                <span id="lblUxBrandSite">UxBrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSite.aspx">
                                <span id="lblBrandSite">BrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">BrandSites</span>
                            </a>
                        </li>
                                               
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Reporte de BrandSite</h1><br />
                        
                         <form id="form2" runat="server">
                             <asp:Label ID="lblBrrandSite" runat="server" Text="Urls de BrandSites"></asp:Label><br />
                             <asp:DropDownList ID="ddlBrandSite" runat="server" OnSelectedIndexChanged="ddlBrandSite_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                             <asp:GridView ID="gvwCountBrandsites" runat="server" AutoGenerateColumns="False" BorderColor="White" DataSourceID="dsCountBrandSites">
                                 <Columns>
                                     <asp:BoundField DataField="TotalArchivos" HeaderText="TotalArchivos" ReadOnly="True" SortExpression="TotalArchivos" />
                                     <asp:BoundField DataField="PesoTotalArchivos" HeaderText="PesoTotalArchivos" ReadOnly="True" SortExpression="PesoTotalArchivos" />
                                 </Columns>
                                 <HeaderStyle ForeColor="#00AAFF" />
                             </asp:GridView>
                             <asp:SqlDataSource ID="dsCountBrandSites" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_CountRBrandSite" SelectCommandType="StoredProcedure">
                                 <SelectParameters>
                                     <asp:ControlParameter ControlID="ddlBrandSite" Name="IdBrandSite" PropertyName="SelectedValue" Type="String" />
                                 </SelectParameters>
                             </asp:SqlDataSource>
                            <div>
        <%--<asp:GridView ID="GridView1" CssClass="mGrid tdGrid" runat="server" OnRowDataBound="GridView1_RowDataBound" >
        </asp:GridView>--%>
        <br />
        <asp:GridView ID="gvwrBrandSite" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="IdBrandSite" DataSourceID="dsrBrandSite" CssClass="mGrid tdGrid">
            <Columns>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="Url" SortExpression="Url">
                    <EditItemTemplate>
                        <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="Url" Mode="Edit" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplUrl" runat="server" NavigateUrl='<%# "http://"+Eval("Url") %>' Target="_blank" Text='<%# Eval("Url") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Archivo" HeaderText="Archivo" ReadOnly="True" SortExpression="Archivo" />
                <asp:BoundField DataField="PesoArchivoConvertido" HeaderText="PesoArchivoConvertido" ReadOnly="True" SortExpression="PesoArchivoConvertido" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsrBrandSite" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_RBrandSite" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlBrandSite" Name="IdBrandSite" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>  
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>
     <script type="text/javascript">
        function cerrarSesion() {
            __doPostBack('lkbCerrarSesion');
        }
    </script>
</body>
</html>
