﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rUxBrandSite.aspx.cs" EnableEventValidation="false" Inherits="Identidad.Gestor.rUxBrandSite" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" onclick="cerrarSesion" href="#">Cerrar sesión</a>
                            <%--<asp:LinkButton ID="lkbCerrarSesion" CssClass="lkbCerrarSesion" OnClick="lkbCerrarSesion_Click" runat="server">Cerrar sesión</asp:LinkButton>--%>
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                    <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="AltaUsuarios.aspx">
                                <span id="lblBrandSites">Alta Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuario.aspx">
                                <span id="lblUsario">Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUxBrandSite.aspx">
                                <span id="lblUxBrandSite">UxBrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSite.aspx">
                                <span id="lblBrandSite">BrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">BrandSites</span>
                            </a>
                        </li>
                                               
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Reporte de Usuarios por BrandSites</h1><br />
                         <form id="form2" runat="server">
                             <asp:Label ID="lblUsxBrandSite" runat="server" Text="Urls de BrandSites"></asp:Label><br />
                             <asp:DropDownList ID="ddlBrandSite" runat="server" OnSelectedIndexChanged="ddlBrandSite_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                             <asp:GridView ID="bvwCountUxB" runat="server" AutoGenerateColumns="False" BorderColor="White" DataSourceID="dsCountUxB">
                                 <Columns>
                                     <asp:BoundField DataField="Total Usuarios " HeaderText="Total Usuarios " ReadOnly="True" SortExpression="Total Usuarios " />
                                 </Columns>
                                 <HeaderStyle BorderColor="White"  ForeColor="#00AAFF" />
                             </asp:GridView>
                             <asp:SqlDataSource ID="dsCountUxB" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_CountRUxBrandSite" SelectCommandType="StoredProcedure">
                                 <SelectParameters>
                                     <asp:ControlParameter ControlID="ddlBrandSite" Name="IdBrandSite" PropertyName="SelectedValue" Type="String" />
                                 </SelectParameters>
                             </asp:SqlDataSource>
    <div>
        <%--<asp:GridView ID="GridView1" CssClass="mGrid tdGrid" runat="server" OnRowDataBound="GridView1_RowDataBound" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1" >
            <Columns>
                <asp:BoundField DataField="IdUsuario" HeaderText="IdUsuario" ReadOnly="True" SortExpression="IdUsuario" />
                <asp:BoundField DataField="NombreCompleto" HeaderText="NombreCompleto" ReadOnly="True" SortExpression="NombreCompleto" />
                <asp:BoundField DataField="Permisos" HeaderText="Permisos" ReadOnly="True" SortExpression="Permisos" />
                <asp:BoundField DataField="IdBrandSite" HeaderText="IdBrandSite" InsertVisible="False" ReadOnly="True" SortExpression="IdBrandSite" />
                <asp:BoundField DataField="NombreSitio" HeaderText="NombreSitio" SortExpression="NombreSitio" />
                <asp:BoundField DataField="Url" HeaderText="Url" ReadOnly="True" SortExpression="Url" />
            </Columns>
        </asp:GridView>--%>
        <asp:GridView ID="gvwrUxBransite" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="dsrUxBrandsite" Width="469px" CssClass="mGrid tdGrid" DataKeyNames="IdUsuario,IdBrandSite,Permisos">
            <Columns>
                <asp:TemplateField ConvertEmptyStringToNull="False" HeaderText="NombreCompleto" SortExpression="NombreCompleto">
                    <EditItemTemplate>
                        <asp:DynamicControl ID="DynamicControl1" runat="server" DataField="NombreCompleto" Mode="Edit" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplNombreC" runat="server" Font-Underline="False" ForeColor="Black" style="cursor:pointer" NavigateUrl='<%# "/Gestor/rUsuario.aspx?i="+Eval("idusuario") %>' Text='<%# Eval("NombreCompleto") %>'></asp:HyperLink>
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Permisos" SortExpression="Permisos">
                    <EditItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("Permisos") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlPermisos" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPermisos_SelectedIndexChanged" SelectedValue='<%# Bind("Permisos") %>'>
                            <asp:ListItem Value="Administrador">Administrador</asp:ListItem>
                            <asp:ListItem Value="Colaborador">Colaborador</asp:ListItem>
                            <asp:ListItem Value="Cliente">Cliente</asp:ListItem>
                            <asp:ListItem Value="Lector">Lector</asp:ListItem>
                            <asp:ListItem Value="Eliminar">Eliminar</asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NombreSitio" SortExpression="NombreSitio">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("NombreSitio") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "/Gestor/rBrandSite.aspx?i="+Eval("idBrandSite") %>' Text='<%# Eval("NombreSitio") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Url" SortExpression="Url">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Url") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplUrl" runat="server" NavigateUrl='<%# "http://"+Eval("Url") %>' Target="_blank" Text='<%# Eval("Url") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsrUxBrandSite" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="tra_RUxBrandSite" SelectCommandType="StoredProcedure">
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlBrandSite" Name="IdBrandSite" PropertyName="SelectedValue" Type="String" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
    </form>  
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>
    <script type="text/javascript">
        function cerrarSesion() {
            __doPostBack('lkbCerrarSesion');
        }
    </script>
</body>
</html>
