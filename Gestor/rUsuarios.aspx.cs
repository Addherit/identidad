﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Identidad.Gestor
{
    public partial class rUsuarios : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables


        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Admin"] == null)
            {
                Response.Redirect("../Admin.aspx");
            }
            ClientScript.GetPostBackEventReference(this, string.Empty);
            if (Request.Form["__EVENTTARGET"] == "lkbCerrarSesion")
            {
                lkbCerrarSesion_Click();
            }
            if (Page.Request.Params["__EVENTTARGET"] == "GridView2")
            {
                var Datos = Page.Request.Params["__EVENTARGUMENT"];
                if (Datos.ToString() != "")
                {                    
                    Response.Redirect("rUsuario.aspx?i=" + Datos.ToString());
                 
                }
            } 

        }

        protected void lkbCerrarSesion_Click()
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

    }
}