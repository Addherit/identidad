﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;


namespace Identidad.Gestor
{
    public partial class rUxBrandSite : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables


        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {
                ClientScript.GetPostBackEventReference(this, string.Empty);

                if (Request.Form["__EVENTTARGET"] == "lkbCerrarSesion")
                {
                    lkbCerrarSesion_Click();
                }
                if (Page.Request.Params["__EVENTTARGET"] == "GridView1")
                {
                    var Datos = Page.Request.Params["__EVENTARGUMENT"];
                    if (Datos.ToString() != "")
                    {

                        if (Datos.Substring(0, 1) == "-")
                        {

                            Response.Redirect("rUsuario.aspx?i=" + Datos.ToString());
                        }
                        else
                        {
                            Response.Redirect("rBrandSite.aspx?i=" + Datos.ToString());
                        }

                    }
                } 
            }
            else
            {
                if (Session["Admin"] == null)
                {
                    Response.Redirect("../Admin.aspx");
                }
                string idBrandSite;
                try
                {
                    idBrandSite = Request["i"].ToString();
                }
                catch
                {
                    idBrandSite = "0";
                }
                SqlCommand cmdd = new SqlCommand("tra_ddlBrandSite");
                cmdd.CommandType = System.Data.CommandType.StoredProcedure;

                oSistema.CargarDropDownList(ddlBrandSite, cmdd);

                //SqlCommand cmd = new SqlCommand("tra_RUxBrandSite");
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;
                //cmd.Parameters.AddWithValue("@IdBrandSite", idBrandSite);

                //oSistema.CargarGridView(GridView1, cmd);

                //GridView1.DataBind();
                //try
                //{
                //    GridView1.HeaderRow.Cells[0].Visible = false;
                //    GridView1.HeaderRow.Cells[3].Visible = false;
                //}
                //catch { }
            }
        }

        //protected void GridView1_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        //e.Row.Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.RowIndex.ToString()));
        //        //e.Row.Cells[0].Visible = false;
        //        //e.Row.Cells[3].Visible = false;
        //        //e.Row.Cells[0].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[0].Text.ToString()));
        //        //e.Row.Cells[1].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[0].Text.ToString()));
        //        //e.Row.Cells[1].Attributes.Add("class", "cursorgvw");
        //        //e.Row.Cells[4].Attributes.Add("onclick", ClientScript.GetPostBackEventReference(GridView1, e.Row.Cells[3].Text.ToString()));
        //        //e.Row.Cells[4].Attributes.Add("class", "cursorgvw");
        //        //e.Row.Cells[5].Attributes.Add("onclick", "javascript: window.open('http://" + e.Row.Cells[5].Text.ToString() + "','_blank')");
        //        //e.Row.Cells[5].Attributes.Add("class", "cursorgvw");
        //    }      

        //}

        protected void ddlBrandSite_SelectedIndexChanged(object sender, EventArgs e)
        {
         //   SqlCommand cmd = new SqlCommand("tra_RUxBrandSite");
          //  cmd.CommandType = System.Data.CommandType.StoredProcedure;
           // cmd.Parameters.AddWithValue("@IdBrandSite", ddlBrandSite.SelectedValue.ToString());

          //  oSistema.CargarGridView(gvwrUxBransite, cmd);
           // try
            //{
             //   gvwrUxBransite.HeaderRow.Cells[0].Visible = false;
              //  gvwrUxBransite.HeaderRow.Cells[3].Visible = false;
            //}
            //catch { }
           
            gvwrUxBransite.DataBind();

        }

        protected void ddlPermisos_SelectedIndexChanged(object sender, EventArgs e)
        {//Selecciona individualmente las variables del renglón seleccionado en el gridview
            string idU;
            string idB;
            string Permiso;
            string nPermiso;
            //string permiso;
            idB = gvwrUxBransite.DataKeys[((GridViewRow)((DropDownList)sender).NamingContainer).RowIndex].Values[1].ToString();
            Permiso = gvwrUxBransite.DataKeys[((GridViewRow)((DropDownList)sender).NamingContainer).RowIndex].Values[2].ToString();
            nPermiso = ((System.Web.UI.WebControls.ListControl)((System.Web.UI.WebControls.DropDownList)(sender))).SelectedValue.ToString();
            idU = gvwrUxBransite.DataKeys[((GridViewRow)((DropDownList)sender).NamingContainer).RowIndex].Values[0].ToString();
            if (Permiso == "Administrador")
            {
                
            }
            else
            {
                oAdmin.Update_permiso(idB, idU, Permiso, nPermiso);
                if (nPermiso == "Administrador")
                {
                    gvwrUxBransite.DataBind();
                    //((System.Web.UI.WebControls.ListControl)((System.Web.UI.WebControls.DropDownList)(sender))).SelectedValue=Permiso;
                }
            }
        }

        protected void lkbCerrarSesion_Click()
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
    }
}