﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rBrandSites.aspx.cs" EnableEventValidation="false" Inherits="Identidad.Gestor.rBrandSites" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" onclick="cerrarSesion" href="#">Cerrar sesión</a>
                            <%--<asp:LinkButton ID="lkbCerrarSesion" CssClass="lkbCerrarSesion" OnClick="lkbCerrarSesion_Click" runat="server">Cerrar sesión</asp:LinkButton>--%>
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                    <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="AltaUsuarios.aspx">
                                <span id="lblBrandSites">Alta Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuario.aspx">
                                <span id="lblUsario">Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUxBrandSite.aspx">
                                <span id="lblUxBrandSite">UxBrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSite.aspx">
                                <span id="lblBrandSite">BrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">BrandSites</span>
                            </a>
                        </li>
                                               
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Reporte de BrandSites</h1><br />
                         <form id="form2" runat="server">
                             <asp:GridView ID="gvwCountBrandSites" runat="server" AutoGenerateColumns="False" BorderColor="White" DataSourceID="dsCountBrandSites" OnSelectedIndexChanged="gvwCountBrandSites_SelectedIndexChanged" CellPadding="15" Width="647px">
                                 <Columns>
                                     <asp:BoundField DataField="TotalBrandSites" HeaderText="TotalBrandSites" ReadOnly="True" SortExpression="TotalBrandSites" />
                                     <asp:BoundField DataField=" " HeaderText=" " ReadOnly="True" SortExpression=" " />
                                     <asp:BoundField DataField="Activados" HeaderText="Activados" ReadOnly="True" SortExpression="Activados" />
                                     <asp:BoundField DataField=" 1" HeaderText=" " ReadOnly="True" SortExpression=" 1" />
                                     <asp:BoundField DataField="Desactivados" HeaderText="Desactivados" ReadOnly="True" SortExpression="Desactivados" />
                                     <asp:BoundField DataField=" 2" HeaderText=" " ReadOnly="True" SortExpression=" 2" />
                                     <asp:BoundField DataField="Eliminados" HeaderText="Eliminados" ReadOnly="True" SortExpression="Eliminados" />
                                 </Columns>
                                 <HeaderStyle ForeColor="#00AAFF" HorizontalAlign="Left" />
                             </asp:GridView>
                             <hr />
                             <asp:SqlDataSource ID="dsCountBrandSites" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="rCountBrandSites" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <div>
        <asp:GridView ID="gvwrBrandSites" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="IdBrandSite" DataSourceID="dsrBrandSites" CssClass="mGrid tdGrid">
            <Columns>
                <asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("Nombre") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplNombreSitio" runat="server" NavigateUrl='<%# "/Gestor/rBrandSite.aspx?i="+Eval("idBrandSite") %>' Text='<%# Eval("Nombre") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Url" SortExpression="Url">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Url") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplUrl" runat="server" NavigateUrl='<%# "http://"+Eval("Url") %>' Text='<%# Eval("Url") %>' Target="_blank"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NombreCompleto" SortExpression="NombreCompleto">
                    <EditItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("NombreCompleto") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Eval("NombreCompleto") %>' NavigateUrl='<%# "/Gestor/rUsuario.aspx?i="+Eval("idUsuario") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="UsuariosParticipando" HeaderText="UsuariosParticipando" SortExpression="UsuariosParticipando" />
                <asp:BoundField DataField="Estatus" HeaderText="Estatus" SortExpression="Estatus" />
                <asp:BoundField DataField="FechaEstatus" HeaderText="FechaEstatus" SortExpression="FechaEstatus" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsrBrandSites" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="rBrandSites" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </div>
    </form>  
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>
    <script type="text/javascript">
        function cerrarSesion() {
            __doPostBack('lkbCerrarSesion');
        }
    </script>
</body>
</html>