﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rUsuarios.aspx.cs" EnableEventValidation="false" Inherits="Identidad.Gestor.rUsuarios" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" onclick="cerrarSesion" href="#">Cerrar sesión</a>
                            <%--<asp:LinkButton ID="lkbCerrarSesion" CssClass="lkbCerrarSesion" OnClick="lkbCerrarSesion_Click" runat="server">Cerrar sesión</asp:LinkButton>--%>
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                   <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="AltaUsuarios.aspx">
                                <span id="lblBrandSites">Alta Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuario.aspx">
                                <span id="lblUsario">Usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUxBrandSite.aspx">
                                <span id="lblUxBrandSite">UxBrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSite.aspx">
                                <span id="lblBrandSite">BrandSite</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">BrandSites</span>
                            </a>
                        </li>
                                               
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Reporte de Usuarios</h1><br />
                         <form id="form2" runat="server">
                             <asp:GridView ID="gvwCountUsuarios" runat="server" BorderStyle="None" BorderColor="White" AutoGenerateColumns="False" DataSourceID="dsCountUsuarios">
                                 <Columns>
                                     <asp:BoundField DataField="Total Usuarios Activos" HeaderText="Total Usuarios Activos" ReadOnly="True" SortExpression="Total Usuarios Activos" />
                                 </Columns>
                                 <HeaderStyle Font-Bold="True" ForeColor="#33CCFF" />
                             </asp:GridView>
                             <asp:SqlDataSource ID="dsCountUsuarios" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="rCountUsuarios" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <div>
        <asp:GridView ID="gvwrUsuarios" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="idUsuario" DataSourceID="dsrUsuarios" CssClass="mGrid tdGrid">
            <Columns>
                <asp:TemplateField HeaderText="NombreCompleto" SortExpression="NombreCompleto">
                    <EditItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("NombreCompleto") %>'></asp:Label>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# "/Gestor/rUsuario.aspx?i="+Eval("idUsuario") %>' Text='<%# Eval("NombreCompleto") %>' CssClass="mGrid tdGrid"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CorreoElectronico" HeaderText="CorreoElectronico" SortExpression="CorreoElectronico" />
                <asp:BoundField DataField="BrandSitesCreados" HeaderText="BrandSitesCreados" ReadOnly="True" SortExpression="BrandSitesCreados" />
                <asp:BoundField DataField="  " HeaderText="  " ReadOnly="True" SortExpression="  " />
                <asp:BoundField DataField="BrandSiteParticipa" HeaderText="BrandSiteParticipa" ReadOnly="True" SortExpression="BrandSiteParticipa" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="dsrUsuarios" runat="server" ConnectionString="<%$ ConnectionStrings:Identidad %>" SelectCommand="rUsuarius" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    </div>
    </form>  
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>
    <script type="text/javascript">
        function cerrarSesion() {
            __doPostBack('lkbCerrarSesion');
        }
    </script>
</body>
</html>

