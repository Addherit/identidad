﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;

namespace Identidad.Gestor
{
    public partial class AltaUsuario : System.Web.UI.Page
    {
        #region Variables


        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //protected void btnAltaUsuario_Click(object sender, EventArgs e)
        //{
        //    SqlCommand cmd = new SqlCommand("exec traUsuarios_NuevoDiseñador");
        //    cmd.Parameters.AddWithValue("@Correo", txtcorreo);
        //    cmd.Parameters.AddWithValue("@Nombre", txtnombre);
        //    cmd.Parameters.AddWithValue("@ApellidoP", txtapellidoP);
        //    cmd.Parameters.AddWithValue("@ApellidoM", txtapellidoM);
            

        //}


        protected void btnAltaUsuario_Click(object sender, EventArgs e)
        {
            SqlCommand cmd = new SqlCommand("exec traUsuarios_NuevoDiseñador");
            cmd.Parameters.AddWithValue("@Correo", txtcorreo);
            cmd.Parameters.AddWithValue("@Nombre", txtnombre);
            cmd.Parameters.AddWithValue("@ApellidoP", txtapellidoP);
            cmd.Parameters.AddWithValue("@ApellidoM", txtapellidoM);
        }
    }
}