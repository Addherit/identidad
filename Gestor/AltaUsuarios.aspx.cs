﻿using Identidad.App_Code;
using System;
using System.Data.SqlClient;

namespace Identidad.Gestor
{
    public partial class AltaUsuarios : System.Web.UI.Page
    {
        #region Variables

        cAdmin oAdmin = new cAdmin();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Admin"] == null){
                Response.Redirect("../Admin.aspx");
            }
        }

        protected void btnAltaUsuario_Click(object sender, EventArgs e)
        {
            if (txtcorreo.Value != "") {
                cStoredProcedure oStoredProcedure = new cStoredProcedure("[traUsuarios_Registro]");
            
            oStoredProcedure.agregarParametro("@Nombre", txtnombre.Value);
            oStoredProcedure.agregarParametro("@Apellidos", txtapellidoP.Value + " " + txtapellidoM.Value);
            oStoredProcedure.agregarParametro("@CorreoElectronico", txtcorreo.Value);

            oStoredProcedure.executeNonQuery();

            txtnombre.Value = "";
            txtapellidoP.Value = "";
            txtapellidoM.Value = "";
            txtcorreo.Value = "";

            lblmsg.Text = "Nuevo Diseñador dado de Alta";
            }
        }

        protected void lkbCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
    }
}