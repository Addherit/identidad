﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using System.Data;

namespace Identidad.Gestor
{
    

    public partial class MenuAdmin : System.Web.UI.Page
    {
        cSistema oSistema = new cSistema();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Admin"] == null)
            {
                Response.Redirect("../Admin.aspx");
            }
        }

        protected void lkbCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["Admin"] = null;

                Response.Redirect("../Admin.aspx");
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
    }
}