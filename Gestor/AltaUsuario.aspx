﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AltaUsuario.aspx.cs" Inherits="Identidad.Gestor.AltaUsuario" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head><title>
	Identidad.com | Administrador
</title><link href="../CSS/Site.css" rel="stylesheet" type="text/css" /><link href="../CSS/MenuSuperiorDisenador.css" rel="stylesheet" type="text/css" /><link href="../CSS/Menu.css" rel="stylesheet" type="text/css" /><link href="../CSS/Pages/DisenadorMaster.css" rel="stylesheet" type="text/css" /><meta http-equiv="X-UA-Compatible" content="IE=edge" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><meta name="application-name" content="Identidad.com" /><meta name="application-tooltip" content="Identidad.com" /><meta name="application-bavbutton-color" content="Blue" /><link href="../Images/Site/ID.ico" rel="icon" type="image/ico" /><link href="../Images/Site/ID.ico" rel="shortcut icon" type="image/x-icon" /><link rel="apple-touch-icon-precomposed" href="../Images/Site/ID.ico" /><link rel="apple-touch-icon" href="../Images/Site/ID.ico" /><link rel="apple-touch-startup-image" href="../Images/Site/ID.ico" /><meta name="apple-mobile-web-app-title" content="Identidad.com" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-touch-fullscreen" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" /><link rel="icon" href="../Images/Site/ID.ico" /><link rel="shortcut icon" href="../Images/Site/ID.ico" /><meta charset="utf-8" /><meta name="viewport" content="initial-scale=1.0, user-scalable=no" /><meta name="apple-mobile-web-app-capable" content="yes" /><meta name="apple-mobile-web-app-status-bar-style" content="black" />

     
</head>
<body>
   
        <div id="UpdatePanel1">
	                            
                <div id="page">
                    <div id="menuSuperior-contenedor">
                        <div id="menuSuperior">
                            <a id="lkbCerrarSesion" class="lkbCerrarSesion" href="#">Cerrar sesión</a>
                            
                        </div>                  
                    </div>
                    <%--Aqui se agrega el banner de los renglones  --%>
                    <div id="cabeza">
                    </div>
                    <%--Aqui termina el banner de los renglones  --%>
            
                    <div id="menu-contenedor">
                        <ul id="menu">
                        <li>
                            <a href="Home.aspx">
                                <span id="lblHome">.</span>
                            </a>
                        </li>
                        <li>
                            <a href="#.aspx">
                                <span id="lblBrandSites">Crear nuevo usuario</span>
                            </a>
                        </li>
                        <li>
                            <a href="rUsuarios.aspx">
                                <span id="lblMisUsuarios">Reporte Usuarios</span>
                            </a>
                        </li>
                        <li>
                            <a href="rBrandSites.aspx">
                                <span id="lblMisGrupos">Reporte BrandSites</span>
                            </a>
                        </li>
                        
                        </ul>
                    </div>

            
                    <div style="font-size:14px; font-weight:bold; margin-left:40px; margin-top:40px;">
                        <h1>Alta de Usuarios</h1><br />
                        <table>
                            <tr>
                                <td>
                                    <label>Correo</label>
                                </td>
                                <td style="width:30px;">
                                </td>
                                <td>
                                    <label>Nombre(s)</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" id="txtcorreo" runat="server" class="DropDownList" placeholder="correo@dominio.com" style="width:400px; height:30px;"/>
                                </td>
                                <td style="width:30px;">
                                </td>
                                <td>
                                    <input type="text" id="txtnombre" runat="server" class="DropDownList" placeholder="Ej. Juan" style="width:400px; height:30px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Apellido Paterno</label>
                                </td>
                                <td style="width:30px;">
                                </td>
                                <td>
                                    <label>Apellido Materno</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <input type="text" id="txtapellidoP" runat="server" class="DropDownList" style="width:400px; height:30px;" placeholder="Ej. Ramírez" />
                                </td>
                                <td>
                                </td>
                                <td>
                                    <input type="text" id="txtapellidoM" runat="server" class="DropDownList" placeholder="Ej.Soto" style="width:400px; height:30px;"/>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <%--<label>Guardar</label>
                                    <asp:Button ID="Button1" runat="server" Text="Button" CssClass="buttons colorVerde" />--%>
                                </td>
                            </tr>
                                                      
                        </table>

                        <asp:Button ID="btnAltaUsuario" runat="server" Text="Guardar"  class="buttons colorVerde" OnClick="btnAltaUsuario_Click" />

                        <%--<button id="btnAltaUsuario2" runat="server"  class="buttons colorVerde" >Guardar&nbsp;</button>--%>

                        <%-- <asp:Button ID="Button1" runat="server" CssClass="buttons colorVerde" Text="Conócenos" style="width:225px;"  />
                            <asp:LinkButton ID="LinkButton1" runat="server" style="color:white!important; font-weight:bolder!important;" meta:resourcekey="lkbIniciarSesion" OnClick="lkbConocenos_Click" Visible="false"></asp:LinkButton>
                        --%>      
                     </div>

                    <div id="divFooter"></div>
                </div>            
        </div>

</body>
</html>