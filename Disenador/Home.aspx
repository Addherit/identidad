﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="Identidad.Disenador.WebForm1" meta:resourcekey="Home" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/HomeDisenador.css" rel="stylesheet" type="text/css" />

<div class="divFondoPagina">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>

                <div class="divTituloCentrado" style="text-align:left; margin-left:40px; margin-top:40px; width:0!important;">
                    <asp:Label ID="lblInicio" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblInicio"></asp:Label>
                </div>
            
                <div style="width:40%; margin-left:40px; margin-top:40px;">
                    <asp:Label ID="lblBienvenida"  runat="server" meta:resourcekey="lblBienvenida"></asp:Label>
                </div>

                <div id="divFooter"></div>



            <!--Inicia Popup para terminos y condiciones--->
            <panel id="pnlTerminos" runat="server" style="display:none" >
                <div class="divTabla" style="height:280px!important; width:420px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblTituloTerminos" runat="server" meta:resourcekey="lblTituloTerminos"></asp:Label>
                    </div>
                    <hr />
                    <br />
                    <div class="divTR">
                        <div class="divTDCentrado">
                            
                            <a href="../Images/Site/identidad_com_contrato_general.pdf" target="_blank">Han cambiado los términos y condiciones</a>
                            
                            <br /><br />
                            <asp:Label ID="lblLeido" runat="server" meta:resourcekey="lblLeido"></asp:Label>
                            <asp:CheckBox ID="ckbLeido" runat="server"></asp:CheckBox>
                            <br /><br />
                            <asp:Label ID="lblNotificacion" runat="server" visible="false" Text=""></asp:Label>
                        </div>
                    </div>
                    <br /><br /><br /><br /><br />
                    <div class="divTR">
                        <div class="divTDCentrado">
                            <asp:Button ID="btnAceptar" class="buttons colorVerde" style="margin-left:240px;" runat="server" text="Aceptar" OnClick="btnAceptar_Click" />
                        </div>
                    </div>

                </div>
            </panel>

            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppTerminos" TargetControlID="Label1" runat="server" PopupControlID="pnlTerminos" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".10" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>



            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
</asp:Content>
