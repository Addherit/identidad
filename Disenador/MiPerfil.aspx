﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="MiPerfil.aspx.cs" Inherits="Identidad.Disenador.WebForm3" meta:resourcekey="MiPerfil" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/Controles/MiPerfil.ascx" TagPrefix="identidad" TagName="MiPerfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="divFondoPagina">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>

                    <div class="divTituloCentrado">
                        <asp:Label ID="lblMiPerfil" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblMiPerfil"></asp:Label>
                    </div>

                    <br /><br /><br />

                    <div class="contenido">
                        <div class="control">
                            <identidad:MiPerfil runat="server" ID="MiPerfil" />
                        </div>
                    </div>
        
                <div id="divFooter"></div>

            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblMiPerfil").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null)
        {
            prm.add_endRequest
            (
                function (sender, e)
                {
                    if (sender._postBackSettings.panelsToUpdate != null)
                    {
                        document.getElementById("lblMiPerfil").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

</asp:Content>
