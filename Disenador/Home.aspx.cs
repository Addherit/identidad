﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad.Disenador
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }

                    LeyoTerminos();
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Si no ah leido los terminos activa el popup
        private void LeyoTerminos()
        {
            oMetodo = oUsuario.LeyoTerminosCondiciones(Session["IdUsuario"].ToString());

            if(oMetodo.Pass == true)
            {
                if(oMetodo.oDato.ToString() == "0" || oMetodo.oDato.ToString() == "False")
                {
                    ppTerminos.Show();
                }
                else
                {
                    ppTerminos.Hide();
                }
            }
            else
            {
                ppTerminos.Show();
            }
        }

        // Aceptar terminos y condiciones
        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                if(ckbLeido.Checked == false)
                {
                    lblNotificacion.Text = "No puedes usar identidad.com si no aceptas los términos y condiciones";
                    lblNotificacion.Visible = true;

                    ppTerminos.Show();
                }
                else
                {
                    oMetodo = new cMetodo();
                    oMetodo = oUsuario.TerminosCondiciones(Session["IdUsuario"].ToString());

                    if(oMetodo.Pass == true)
                    {
                        ppTerminos.Hide();
                    }
                    else
                    {
                        lblNotificacion.Text = oMetodo.Message;
                        ppTerminos.Show();
                    }
                }
            }
            catch(Exception err)
            {

            }
        }

       

    }
}