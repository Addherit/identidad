﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="Pago.aspx.cs" Inherits="Identidad.Disenador.WebForm5" meta:resourcekey="Pago" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <link href="../CSS/Pages/Pago.css" rel="stylesheet" type="text/css" />

            <div class="divFondoPagina">

            <div class="divTituloCentrado">
                <asp:Label ID="lblPago" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblPago"></asp:Label>
            </div>
            
            <asp:GridView ID="gvTiposCuenta" runat="server" CssClass="mGrid GridPago" AutoGenerateColumns="false" >
            <Columns>

                <asp:TemplateField>
                        <ItemTemplate>
                            <table>
                                <tr>
                                    <td style="width:100px;">
                                        <asp:Image ID="imgCuenta" CssClass="imgCuenta imgCircular" runat="server" ImageUrl='<%#Eval ("UrlImagen") %>' />
                                    </td>
                                    <td>
                                        <asp:Label ID="lblIdTipoCuenta" runat="server" DataField ="IdTipoCuenta" Text='<%#Eval ("IdTipoCuenta") %>' Visible="false" ></asp:Label>
                                        <asp:Label ID="lblTipo" runat="server" CssClass="NombrePlan" DataField ="Tipo" Text='<%#Eval ("Tipo") %>' ></asp:Label>
                                        <br />
                                        <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                        <ItemTemplate>
                            <asp:RadioButton ID="rbSeleccionada" runat="server" AutoPostBack="true" OnCheckedChanged="rbSeleccionada_CheckedChanged" Checked='<%#Eval ("Seleccionada") %>' />
                        </ItemTemplate>
                </asp:TemplateField>
        
            </Columns>
            </asp:GridView>

            <div>
                <div id="divFooter"></div>
            </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblEsquemasPago").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblEsquemasPago").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

</asp:Content>
