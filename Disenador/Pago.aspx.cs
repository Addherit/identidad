﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;

namespace Identidad.Disenador
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cBrandSite oBrandSite = new cBrandSite();
        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        CargarDatos();
                    }
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarDatos()
        {
            oBrandSite.CargarTiposCuenta(gvTiposCuenta, Session["IdUsuario"].ToString());
        } //Carga los tipos de cuenta existentes para ser seleccionadas

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void rbSeleccionada_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                RadioButton Status = (RadioButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                
                oMetodo = oBrandSite.ActualizarTipoCuenta(Session["IdUsuario"].ToString(), ((System.Web.UI.WebControls.Label)((gvTiposCuenta.Rows[Row].FindControl("lblIdTipoCuenta")))).Text);

                CargarDatos();
                
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

    }
}