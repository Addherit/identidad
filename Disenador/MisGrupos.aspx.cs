﻿using Identidad.App_Code;
using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace Identidad.Disenador
{
    public partial class WebForm7 : System.Web.UI.Page
    {

        // Estan son las variables que voy a usar en esta página

        #region Variables

        cBrandSite oBrandSite = new cBrandSite();
        cUsuario oUsuario = new cUsuario();
        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        CargarBrandSites();
                    }
                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página. Un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarBrandSites()
        {
            oBrandSite.CargarMisBrandSites(gvMisBrandSite, Session["IdUsuario"].ToString());

            CargarGrupos();

        } //Carga los sitios para poder agregar nuevos grupos

        private void CargarGrupos()
        {
            for(int i = 0; i <= gvMisBrandSite.Rows.Count - 1; i ++)
            {
                oBrandSite.CargarGrupos(((System.Web.UI.WebControls.GridView)((gvMisBrandSite.Rows[i].FindControl("gvGrupos")))), 
                    ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[i].FindControl("lblIdBrandSite")))).Text);
            }

        } // Carga los grupos en cada brand site

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Se disparan por medio de un control en el HTML :)

        #region Eventos

        #region Catálogo de grupos

        protected void lkbAgregarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdBrandSiteSeleccionado.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;
                lblIdGrupoSeleccionado.Text = "0";
                txtNombreGrupo.Text = "";
                txtDescripcionGrupo.Text = "";

                ppEdicionGrupo.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                ppEdicionGrupo.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnAceptarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo = oBrandSite.EdicionGrupo(lblIdGrupoSeleccionado.Text, lblIdBrandSiteSeleccionado.Text, txtNombreGrupo.Text, txtDescripcionGrupo.Text);

                if(oMetodo.Pass == false)
                {
                    lblValidacionGrupo.Text = oMetodo.Message;
                    lblValidacionGrupo.Visible = true;
                    ppEdicionGrupo.Show();
                }
                else
                {
                    lblValidacionGrupo.Visible = false;
                    CargarGrupos();
                    ppEdicionGrupo.Hide();
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbEditarGrupo_Click(object sender, EventArgs e)
        {
            try
            {   
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                
                lblIdGrupoSeleccionado.Text = ((System.Web.UI.WebControls.Label)(((GridViewRow)Status.NamingContainer).FindControl("lblIdGrupo"))).Text;

                foreach (DataRow row in oBrandSite.cargarDatosGrupo(lblIdGrupoSeleccionado.Text).Rows)
                {
                    lblIdBrandSiteSeleccionado.Text = row["IdBrandSite"].ToString();
                    txtNombreGrupo.Text = row["Nombre"].ToString();
                    txtDescripcionGrupo.Text = row["Descripcion"].ToString();
                }

                ppEdicionGrupo.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        #region Editar integrantes

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                oBrandSite.CargarUsuariosBrandSiteGrupos(gvUsuariosBS, lblIdBrandSiteIntegrantes.Text, lblIdGrupoIntegrantes.Text, txtBusqueda.Text);

                ppEditarIntegrantes.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbEditarIntegrantesGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                lblIdBrandSiteIntegrantes.Text = ((System.Web.UI.WebControls.Label)(((GridViewRow)Status.NamingContainer).FindControl("lblIdBrandSitePertenece"))).Text;
                lblIdGrupoIntegrantes.Text = ((System.Web.UI.WebControls.Label)(((GridViewRow)Status.NamingContainer).FindControl("lblIdGrupo"))).Text;
                lblTituloIntegrantes.Text = "Selecciona a los integrantes del grupo: " + ((System.Web.UI.WebControls.Label)(((GridViewRow)Status.NamingContainer).FindControl("lblNombreGrupo"))).Text + ".";
                txtBusqueda.Text = "";
                ckbSeleccionarTodos.Checked = false;

                oBrandSite.CargarUsuariosBrandSiteGrupos(gvUsuariosBS, lblIdBrandSiteIntegrantes.Text, lblIdGrupoIntegrantes.Text, txtBusqueda.Text);
                oBrandSite.CargarIntegrantesGrupo(gvEnElGrupo, lblIdGrupoIntegrantes.Text);

                ppEditarIntegrantes.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbQuitarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                oMetodo = oBrandSite.EdicionIntegranteGrupo(((System.Web.UI.WebControls.Label)(gvEnElGrupo.Rows[Row].FindControl("lblIdUxG"))).Text, ((System.Web.UI.WebControls.Label)(gvEnElGrupo.Rows[Row].FindControl("lblIdUsuario"))).Text, lblIdGrupoIntegrantes.Text, "false");

                if(oMetodo.Pass == true)
                {
                    oBrandSite.CargarUsuariosBrandSiteGrupos(gvUsuariosBS, lblIdBrandSiteIntegrantes.Text, lblIdGrupoIntegrantes.Text, txtBusqueda.Text);
                    oBrandSite.CargarIntegrantesGrupo(gvEnElGrupo, lblIdGrupoIntegrantes.Text);
                }

                ppEditarIntegrantes.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                bool bSelecciono = false;

                for (int i = 0; gvUsuariosBS.Rows.Count > i; i++)
                {
                    if (((System.Web.UI.WebControls.CheckBox)(gvUsuariosBS.Rows[i].FindControl("ckbAgregarUsuario"))).Checked == true)
                    {
                        bSelecciono = true;
                        break;
                    }
                }

                if (bSelecciono == true)
                {
                    for (int i = 0; gvUsuariosBS.Rows.Count > i; i++)
                    {
                        if (((System.Web.UI.WebControls.CheckBox)(gvUsuariosBS.Rows[i].FindControl("ckbAgregarUsuario"))).Checked == true)
                        {
                            oBrandSite.EdicionIntegranteGrupo("0", ((System.Web.UI.WebControls.Label)(gvUsuariosBS.Rows[i].FindControl("lblIdUsuario"))).Text, lblIdGrupoIntegrantes.Text, "true");
                        }
                    }

                    oBrandSite.CargarUsuariosBrandSiteGrupos(gvUsuariosBS, lblIdBrandSiteIntegrantes.Text, lblIdGrupoIntegrantes.Text, txtBusqueda.Text);
                    oBrandSite.CargarIntegrantesGrupo(gvEnElGrupo, lblIdGrupoIntegrantes.Text);
                }

                ppEditarIntegrantes.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnSalir_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdBrandSiteIntegrantes.Text = "0";
                lblIdGrupoIntegrantes.Text = "0";
                lblTituloIntegrantes.Text = "";

                ppEditarIntegrantes.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void ckbSeleccionarTodos_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; gvUsuariosBS.Rows.Count > i; i++)
                {
                    ((System.Web.UI.WebControls.CheckBox)(gvUsuariosBS.Rows[i].FindControl("ckbAgregarUsuario"))).Checked = ckbSeleccionarTodos.Checked;
                }

                ppEditarIntegrantes.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        #region Eliminar grupo

        protected void lkbEliminarGrupo_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdGrupoEliminar.Text = ((System.Web.UI.WebControls.Label)(((GridViewRow)Status.NamingContainer).FindControl("lblIdGrupo"))).Text;

                ppEliminarGrupo.Show();

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                lblIdGrupoEliminar.Text = "0";
                ppEliminarGrupo.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo = oBrandSite.EliminarGrupo(lblIdGrupoEliminar.Text);

                if (oMetodo.Pass == true)
                {
                    CargarGrupos();
                }
                else
                {
                    lblConfirmarEliminar.Text = oMetodo.Message;
                    ppEliminarGrupo.Show();
                }

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }


        #endregion

        #endregion

    }
}