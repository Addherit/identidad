﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad.Disenador
{
    public partial class Disenador : System.Web.UI.MasterPage
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cUsuario oUsuario = new cUsuario();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Session["bandera"] = "0";

                if (Session["IdUsuario"] == null)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/site/login-registro.aspx", false);

                    return;
                }

                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        imgUsuario.ImageUrl = Session["FotoUsuario"].ToString();

                        Session["IdAdministradorBS"] = Session["IdUsuario"].ToString();
                        Session["EsAdministrador"] = "1";
                    }

                    Response.CacheControl = "private";
                    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                }
                Response.CacheControl = "private";
                Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void lkbCerrarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IdUsuario"] = null;
                Session["IdBrandSite"] = null;
                Session["FotoUsuario"] = null;
                Session["NombreUsuario"] = null;
                Session["IdAdministradorBS"] = null;
                Session["EsAdministrador"] = null;
                Session["PuedeEditiar"] = null;

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
                Response.Cookies["Cookie"].Expires = DateTime.Now.AddDays(-1d);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}