﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="MisGrupos.aspx.cs" Inherits="Identidad.Disenador.WebForm7" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/MisGrupos.css" rel="stylesheet" type="text/css"/>

    <div class="divFondoPagina">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                
                    <div class="divTituloCentrado">
                        <asp:Label ID="lblMisGrupos" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblMisGrupos"></asp:Label>
                    </div>
                    
                    <div>
                        <asp:GridView ID="gvMisBrandSite" runat="server" CssClass="mGrid" AutoGenerateColumns="false" style="width:100%;" border="0">
                            <Columns>

                                <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="width:100px; vertical-align:top;">
                                                        <asp:Image ID="imgLogo" CssClass="imgLogo imgCircular" runat="server" ImageUrl='<%#Eval ("UrlLogo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblIdBrandSite" runat="server" DataField ="IdBrandSite" Text='<%#Eval ("IdBrandSite") %>' Visible="false" ></asp:Label>
                                                        <asp:Label ID="lblNombre" style="font-size:150%; font-weight:bolder;" CssClass="MisBrandSites" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                        &nbsp;|&nbsp;
                                                        <asp:LinkButton Id="lkbAgregarGrupo" runat="server" OnClick="lkbAgregarGrupo_Click" meta:resourcekey="lkbAgregarGrupo"></asp:LinkButton>   
                                                        <br />
                                                        <asp:GridView ID="gvGrupos" runat="server" AutoGenerateColumns="false" border="0" style="width:700px;">
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>
                                                                        <div>
                                                                            <br />
                                                                            <asp:Label ID="lblIdGrupo" runat="server" DataField ="IdGrupo" Text='<%#Eval ("IdGrupo") %>' Visible="false" ></asp:Label>
                                                                            <asp:Label ID="lblIdBrandSitePertenece" runat="server" DataField ="IdBrandSite" Text='<%#Eval ("IdBrandSite") %>' Visible="false" ></asp:Label>
                                                                            - <asp:Label ID="lblNombreGrupo" style="font-size:150%;" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                                            <br />
                                                                            &nbsp; <asp:Label ID="lblDescripcion" runat="server" DataField ="Descripcion" Text='<%#Eval ("Descripcion") %>' ></asp:Label> 
                                                                            <br /><br />
                                                                            &nbsp; 
                                                                            <asp:LinkButton Id="lkbEditarGrupo" runat="server" OnClick="lkbEditarGrupo_Click" meta:resourcekey="lkbEditarGrupo" ></asp:LinkButton>
                                                                            |
                                                                            <asp:LinkButton Id="lkbEditarIntegrantesGrupo" runat="server" OnClick="lkbEditarIntegrantesGrupo_Click" meta:resourcekey="lkbEditarIntegrantesGrupo" ></asp:LinkButton>
                                                                            |
                                                                            <asp:LinkButton Id="lkbEliminarGrupo" runat="server" OnClick="lkbEliminarGrupo_Click" meta:resourcekey="lkbEliminarGrupo" ></asp:LinkButton>
                                                                            <br />
                                                                            <hr />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                    <div id="divFooter"></div>

                    <!--Inicia Popup para agregar/editar un grupo--->
                    <panel id="pnlAgregarGrupo" runat="server" style="display:none" >
                        <div class="divTabla divImportarExistente" style="height:325px;">
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblIdBrandSiteSeleccionado" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblIdGrupoSeleccionado" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblEdicionGrupo" runat="server" meta:resourcekey="lblEdicionGrupo"></asp:Label>
                            </div>
                            <hr />
                            <br />
                            <br />
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblNombreGrupo" runat="server" meta:resourcekey="lblNombreGrupo"></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtNombreGrupo" CssClass="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divTR" style="height:70px;">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblDescripcion" runat="server" meta:resourcekey="lblDescripcion"></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtDescripcionGrupo" class="Multiline" Height="70px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:Label ID="lblValidacionGrupo" runat="server" visible="false"></asp:Label>
                                </div>

                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoButtons">
                                    <asp:Button ID="btnCancelarGrupo" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarGrupo" OnClick="btnCancelarGrupo_Click" />
                                    <asp:Button ID="btnAceptarGrupo" class="buttons colorVerde" runat="server" meta:resourcekey="btnAceptarGrupo" OnClick="btnAceptarGrupo_Click" />
                                </div>

                            </div>
                        </div>
                    </panel>

                    <asp:Label ID="lblAgregarGrupo" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppEdicionGrupo" TargetControlID="lblAgregarGrupo" runat="server" PopupControlID="pnlAgregarGrupo" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".30" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Finaliza Popup para editar un grupo--->
                

                    <!--Inicia Popup para confirmar eliminar un grupo--->
                    <panel id="pnlConfirmarEliminar" runat="server" style="display:none" >
                        <div class="divTabla" style="height:340px!important;">
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblEliminar" runat="server" meta:resourcekey="lblEliminar"></asp:Label>
                            </div>
                            <hr />
                            <br /><br /><br />
                            <div class="divTR">
                                <div class="divTDCentrado">
                                    <asp:Label ID="lblConfirmarEliminar" runat="server" meta:resourcekey="lblConfirmarEliminar"></asp:Label>
                                    <br /><br /><br />
                                    <asp:Label ID="lblIdGrupoEliminar" runat="server" visible="false"></asp:Label>
                                    <asp:Button ID="btnCancelarEliminar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" />
                                    <asp:Button ID="btnEliminar" class="buttons colorRojo" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" />

                                </div>

                            </div>
                        </div>
                    </panel>

                    <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppEliminarGrupo" TargetControlID="Label2" runat="server" PopupControlID="pnlConfirmarEliminar" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".10" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Termina Popup para confirmar eliminar un grupo--->

                    <!--Inicia Popup para editar integrantes del grupo--->
                    <panel id="pnlEditarIntegrantes" runat="server" style="display:none" >
                        <div class="divTabla" style="height:500px!important; width:800px!important; min-width:800px!important;">
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblIdGrupoIntegrantes" runat="server" visible="false"></asp:Label>
                                <asp:Label ID="lblIdBrandSiteIntegrantes" runat="server" visible="false"></asp:Label>
                                <asp:Label ID="lblTituloIntegrantes" runat="server" meta:resourcekey="lblTituloIntegrantes"></asp:Label>
                            </div>
                            <hr />
                            <br />
                            <asp:Label ID="lblNombreBusqueda" Style="margin-left:50px;" runat="server" meta:resourcekey="lblNombreBusqueda" ></asp:Label>
                            <asp:TextBox ID="txtBusqueda" Style="margin-left:10px;" CssClass="TextBox" runat="server"></asp:TextBox>
                            <asp:Button ID="btnBuscar" class="buttons colorVerde" runat="server" meta:resourcekey="btnBuscar" OnClick="btnBuscar_Click"/>
                            <br />
                            <div style="overflow-y:auto; height:310px; min-height:310px; margin-top:20px;">                            
                                <div style="width:100%; min-height:310px; overflow-y:auto;">
                                    <div style=" float:left; width:49%!important; min-width:49%!important;">
                                        <asp:GridView ID="gvUsuariosBS" runat="server" CssClass="mGrid" AutoGenerateColumns="false">
                                            <Columns>

                                                <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td style="width:70px;">
                                                                        <asp:Image ID="imgUsuario" CssClass="imgUsuario imgCircular" runat="server" ImageUrl='<%#Eval ("Foto") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblIdUsuario" runat="server" DataField ="IdUsuario" Text='<%#Eval ("IdUsuario") %>' Visible="false" ></asp:Label>
                                                                        <asp:Label ID="lblNombre" CssClass="NombreUsuario" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                                        <br />
                                                                        <asp:Label ID="lblFechaActivo" CssClass="FechaAlta" runat="server" DataField ="FechaActivo" Text='<%#Eval ("FechaActivo") %>' ></asp:Label>
                                                                        <br /><br />
                                                                        <asp:CheckBox ID="ckbAgregarUsuario" runat="server"></asp:CheckBox>
                                                                        <asp:Label ID="lblAgregarAGrupo" runat="server" meta:resourcekey="lblAgregarAGrupo"></asp:Label>
                                                                        <br /><br />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                    <div style=" float:right; width:49%!important; min-width:49%!important;">
                                        <asp:GridView ID="gvEnElGrupo" runat="server" CssClass="mGrid" AutoGenerateColumns="false">
                                            <Columns>

                                                <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td style="width:70px;">
                                                                        <asp:Image ID="imgUsuario" CssClass="imgUsuario imgCircular" runat="server" ImageUrl='<%#Eval ("Foto") %>' />
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblIdUxG" runat="server" DataField ="IdUxG" Text='<%#Eval ("IdUxG") %>' Visible="false" ></asp:Label>
                                                                        <asp:Label ID="lblIdUsuario" runat="server" DataField ="IdUsuario" Text='<%#Eval ("IdUsuario") %>' Visible="false" ></asp:Label>
                                                                        <asp:Label ID="lblNombre" CssClass="NombreUsuario" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                                        <br /><br />
                                                                        <asp:LinkButton Id="lkbQuitarUsuario" runat="server" OnClick="lkbQuitarUsuario_Click" meta:resourcekey="lkbQuitarUsuario"></asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                        
                                                        </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>

                                    </div>
                                </div>
                            </div>
                            <div style="width:100%; text-align:right; margin-top:20px;">
                                                                <asp:Label ID="lblSeleccionarTodos" runat="server" meta:resourcekey="lblSeleccionarTodos" ></asp:Label>
                                <asp:CheckBox ID="ckbSeleccionarTodos" runat="server" OnCheckedChanged="ckbSeleccionarTodos_CheckedChanged" AutoPostBack="true" style="margin-right:10px;"></asp:CheckBox>

                                <asp:Button ID="btnSalir" class="buttons colorwhite" runat="server" OnClick="btnSalir_Click" meta:resourcekey="btnSalir"></asp:Button>
                                <asp:Button ID="btnAgregar" class="buttons colorVerde" runat="server" OnClick="btnAgregar_Click" meta:resourcekey="btnAgregar"></asp:Button>
                                
                            </div>

                        </div>
                    </panel>

                    <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppEditarIntegrantes" TargetControlID="Label5" runat="server" PopupControlID="pnlEditarIntegrantes" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".30" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Termina Popup para editar integrantes del grupo--->

                </ContentTemplate>
            </asp:UpdatePanel>

    </div>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblMisGrupos").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblMisGrupos").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

</asp:Content>
