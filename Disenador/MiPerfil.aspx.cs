﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;

namespace Identidad.Disenador
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        MiPerfil.setIdUsuario(Session["IdUsuario"].ToString(), false);
                    }

                }
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // borra la cache antes de cargar el sitio
                List<string> keys = new List<string>();
                IDictionaryEnumerator enumerator = Cache.GetEnumerator();

                while (enumerator.MoveNext())
                    keys.Add(enumerator.Key.ToString());

                for (int i = 0; i < keys.Count; i++)
                    Cache.Remove(keys[i]);

                // Crea eventos
                MiPerfil.EstadoCatalogo += new Identidad.Controles.MiPerfil.EventHandler(MiPerfil_OnEstadoCatalogo);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en MiPerfil
        private void MiPerfil_OnEstadoCatalogo(object sender, EventArgs e)
        {
            try
            {
                oMetodo = MiPerfil.ObtenerEstado();

                if (oMetodo.Pass == true)
                {
                    Image img = (Image)Master.FindControl("imgUsuario");
                    img.ImageUrl = Session["FotoUsuario"].ToString();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}