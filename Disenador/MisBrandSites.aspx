﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="MisBrandSites.aspx.cs" Inherits="Identidad.Disenador.WebForm2" meta:resourcekey="MisBrandSites" %>
<%@ Register Src="../Controles/Examinar.ascx" TagPrefix="identidad" TagName="Examinar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="divFondoPagina">

    <link href="../CSS/Pages/MisBrandSites.css" rel="stylesheet" type="text/css"/>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="divTituloCentrado">
                <asp:Label ID="lblMisBrandSites" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblMisBrandSites"></asp:Label>
            </div>
            <!--
            <asp:Button ID="btnAgregar" Style="margin-left:50px;" ToolTip="Genera un nuevo sitio aquí y define su nombre y url (dirección). Elige el plan y ¡listo!" class="buttons colorVerde" runat="server" meta:resourcekey="btnAgregar" OnClick="btnAgregar_Click" />
            <br /><br /><br /> -->
           
            <asp:GridView ID="gvMisBrandSite" CssClass="mGrid gridMisBrandSites" runat="server" AutoGenerateColumns="false" >
                <Columns>

                    <asp:TemplateField>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width:100px;">
                                            <asp:Image ID="imgLogo" CssClass="imgLogo imgCircular" ImageUrl='<%#Eval ("UrlLogo") %>' runat="server" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblIdBrandSite" runat="server" DataField ="IdBrandSite" Text='<%#Eval ("IdBrandSite") %>' Visible="false" ></asp:Label>
                                            <asp:Label ID="lblNombre" CssClass="tituloBrandSite" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                            <br /><br />
                                            <asp:LinkButton Id="lkbUrl"  ToolTip="Puedes ir directamente al sitio como lo verán los usuarios en general. Verás en la parte superior un panel de funciones como administrador que solo será visible para ti y tus colaboradores."  runat="server" OnClick="lkbUrl_Click" DataField ="UrlText" Text='<%#Eval ("UrlText") %>'></asp:LinkButton>   
                                            <br />
                                            <asp:LinkButton Id="lkbEditar" ToolTip="Puedes cambiar el nombre (no el url), el ícono o eliminar el sitio aquí." runat="server" OnClick="lkbEditar_Click" meta:resourcekey="lkbEditar"></asp:LinkButton>   
                                            |
                                            <asp:LinkButton Id="lkbDesigner" ToolTip="Irás directamente al modo edición para modificar, actualizar y enriquecer el sitio en contenidos y archivos." runat="server" OnClick="lkbDesigner_Click" meta:resourcekey="lkbDesigner"></asp:LinkButton>   
                                            
                                            <br /><br />
                                            <asp:Label ID="lblEstatus" CssClass="informacionAdicional" runat="server"  DataField ="Estatus" Text='<%#Eval ("Estatus") %>' ></asp:Label>
                                            |
                                            <asp:Label ID="lblConsumidores" CssClass="informacionAdicional" runat="server"  DataField ="CantidadConsumidores" Text='<%#Eval ("CantidadConsumidores") %>' ></asp:Label>
                                            <asp:Label ID="lblUsuarios" CssClass="informacionAdicional" meta:resourcekey="lblUsuarios" runat="server" ></asp:Label>
                                            |
                                            <asp:Label ID="lblPesoConvertido" CssClass="informacionAdicional" runat="server" DataField ="PesoConvertido" Text='<%#Eval ("PesoConvertido") %>' ></asp:Label>
                                            <asp:Label ID="lblPesoBytes" CssClass="informacionAdicional" runat="server" DataField ="PesoBytes" Text='<%#Eval ("PesoBytes") %>' Visible="false" ></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
            
            <%--<Ajax:ModalPopupExtender ID="MPE" runat="server" TargetControlID="lnkMPE" PopupControlID="Panel1" Enabled="True" CancelControlID="btnClose"></Ajax:ModalPopupExtender>--%>
           
            <asp:Label ID="lblEtEspacioTotal" Style="margin-left:25px;" CssClass="espacioTotal" runat="server" meta:resourcekey="lblEtEspacioTotal"></asp:Label>
            <asp:Label ID="lblEspacioTotal" CssClass="espacioTotal" runat="server" Text="0"></asp:Label>
              
            
            <div id="divFooter"></div>


   <!--Inicia Popup para agregar un brandsite--->
            <panel id="pnlAgregarBrandSite" runat="server" style="display:none" >
                <div class="divTabla" style="height:340px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblMiBrandSite" runat="server" meta:resourcekey="lblMiBrandSite"></asp:Label>
                    </div>
                    <hr />
                    <br />
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblIdBrandSitePopUp" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblEtNombre" runat="server" meta:resourcekey="lblNombre"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtNombre" CssClass="TextBox" onblur="onLeave(this)" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                            <asp:Label ID="lblUrl" runat="server" meta:resourcekey="lblUrl"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput">
                            <asp:TextBox ID="txtUrl" CssClass="TextBox" runat="server" onblur="onLeave(this)" onkeypress="return soloLetras(event)"></asp:TextBox>
                            <asp:Label ID="lblUrl2" runat="server"  onkeypress="return check(event)" Text=".identidad.com"></asp:Label>
                        </div>
                    </div>

                    <div class="divTR">
                        <div class="divTDIzq">

                        </div>
                        <div class="divTDDerechoInput">
                            <asp:LinkButton Id="lkbEliminar" runat="server" OnClick="lkbEliminar_Click" meta:resourcekey="lkbEliminar" Visible="false"></asp:LinkButton>   
                            <asp:LinkButton ID="lkbCambiarLogo" runat="server" OnClick="lkbCambiarLogo_Click" meta:resourcekey="lkbCambiarLogo" Visible="false"></asp:LinkButton>
                            <asp:Image ID="imgOk" runat="server" ImageUrl="~/Images/Site/Ok.png" Height="16px" Visible="false"></asp:Image>
                        </div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq">
                        </div>
                        <div class="divTDIzq">
                            <asp:Label ID="lblValidacion" runat="server" Visible="false"></asp:Label>
                        </div>
                        <div class="divTDDerechoInput"></div>
                    </div>
                    <div class="divTR">
                        <div class="divTDIzq"></div>
                        <div class="divTDDerechoButtons">
                            
                            <asp:Button ID="btnCancelar" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelar" OnClick="btnCancelar_Click" />
                            <asp:Button ID="btnGuardarNuevo" class="buttons colorVerde" runat="server" meta:resourcekey="btnGuardarNuevo" OnClick="btnGuardarNuevo_Click" />

                        </div>
                    </div>
                </div>
            </panel>

            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppAgregarBrandSite" TargetControlID="Label1" runat="server" PopupControlID="pnlAgregarBrandSite" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".30" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para agregar un brandsite--->
      
       
 




            <!--Inicia Popup para confirmar eliminar un brandsite--->
            <panel id="pnlConfirmarEliminar" runat="server" style="display:none" >
                <div class="divTabla" style="height:340px!important;">
                    <div class="divTablaTitulo">
                        <asp:Label ID="lblEliminar" runat="server" meta:resourcekey="lblEliminar"></asp:Label>
                    </div>
                    <hr />
                    <br /><br /><br />
                    <div class="divTR">
                        <div class="divTDCentrado">
                            <asp:Label ID="lblUrlBrandSite" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblConfirmarEliminar" runat="server" meta:resourcekey="lblConfirmarEliminar"></asp:Label>
                            <br /><br /><br />
                            
                            <asp:Button ID="btnCancelarEliminar" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarEliminar" OnClick="btnCancelarEliminar_Click" />
                            <asp:Button ID="btnEliminar" class="buttons colorRojo" runat="server" meta:resourcekey="btnEliminar" OnClick="btnEliminar_Click" />

                        </div>

                    </div>
                </div>
            </panel>

            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <Ajax:ModalPopupExtender ID="ppEliminarBrandSite" TargetControlID="Label2" runat="server" PopupControlID="pnlConfirmarEliminar" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                <Animations>
                    <OnShown>
                        <FadeIn Duration=".10" Fps="48" />                
                    </OnShown>
                </Animations>
            </Ajax:ModalPopupExtender>
            <!--Termina Popup para confirmar eliminar un brandsite--->

            <asp:Label ID="lblIdNuevoLogo" runat="server" visible="false" Text=""></asp:Label>
            <identidad:Examinar runat="server" id="eExaminarLogo" />

        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblBrandSites").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblBrandSites").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

    <script type="text/javascript">
        function soloLetras(e) {
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = " abcdefghijklmnñopqrstuvwxyz";
            especiales = ["8","37","39","46"]; //"8-37-39-46";

            tecla_especial = false
            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }

            var txtUrl = document.getElementById('ContentPlaceHolder1_txtUrl').value;
            document.getElementById('ContentPlaceHolder1_txtUrl').value = txtUrl.replace(/\s/g, '');

        }

        function check(e) {
            tecla = (document.all) ? e.keyCode : e.which;

            //Tecla de retroceso para borrar, siempre la permite
            if (tecla == 8) {
                return true;
            }

            // Patron de entrada, en este caso solo acepta numeros y letras
            patron = /[A-Za-z0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }
    </script>

    <script type="text/javascript">
        // Obtener el popUp Contactanos----------------------------------------------------------------
        var modal = document.getElementById("popUpPagar");

        // Obtener boton para abrir popUp
        var btn = document.getElementById("ContentPlaceHolder1_btnEnviarR");

        // Obtner botones que cierran el popUp
        var span = document.getElementsByClassName("closePagar")[0];
        var btnC = document.getElementById("closePagar");

        // Funcion para mostrar el popUp  
        btn.onclick = function () {//Plan1
            modal.style.display = "block";
        }

        // Funciones para cerrar el popUp
        spanPagar.onclick = function () {
            modalPagar.style.display = "none";
        }

    </script>

</div>
</asp:Content>
