﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="Tutoriales.aspx.cs" Inherits="Identidad.Disenador.WebForm6" meta:resourcekey="Tutoriales"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="divFondoPagina">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="divTituloCentrado">
                <asp:Label ID="lblTutoriales" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblTutoriales"></asp:Label>
            </div>
            <br /><br /><br />
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        // Se muestra el menú seleccionado
        document.getElementById("lblTutoriales").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblTutoriales").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>

</div>
</asp:Content>
