﻿using System;
using System.Web;
using Identidad.App_Code;
using System.Web.UI.WebControls;
using System.Data;

namespace Identidad.Disenador
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cAmazon oAmazon = new cAmazon();
        cBrandSite oBrandSite = new cBrandSite();
        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();
        float fEspacioTotal = 0;
        //int fnuevoBS;
        //public static int IdBrandSite;

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    Session["nuevoBrandsite"] = "0";
                }
                CargarMisBrandSite();
                //oBrandSite.CargarEstatusBS(ddlEstatus);
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        public void CargarMisBrandSite()
        {
            oBrandSite.CargarMisBrandSites(gvMisBrandSite, Session["IdUsuario"].ToString());
            
            fEspacioTotal = 0;

            float fEspacioBS = 0;
            
            for (int i = 0; i <= gvMisBrandSite.Rows.Count - 1; i++)
            {
                fEspacioBS = 0;

                if (float.TryParse(((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[i].FindControl("lblPesoBytes")))).Text, out fEspacioBS) == true)
                {
                    fEspacioTotal = fEspacioTotal + fEspacioBS;
                }
            }

            lblEspacioTotal.Text = oBrandSite.ConvertirPeso(fEspacioTotal);
        } //  Carga los sitios del diseñador y calcula el peso total del sitio

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        #region Agregar/Modificar

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                txtNombre.Text = "";
                txtUrl.Text = "";
                txtUrl.Enabled = true;
                lkbEliminar.Visible = false;
                lblValidacion.Visible = false;
                lkbCambiarLogo.Visible = false;
                lblIdNuevoLogo.Text = "";
                lblIdBrandSitePopUp.Text = "0";
                imgOk.Visible = false;
                Session["nuevoBrandsite"] = "1";
                ppAgregarBrandSite.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }

        }

       protected void btnGuardarNuevo_Click(object sender, EventArgs e)
        {
            int vencido;
            try
            {
                ppAgregarBrandSite.Show();
                //Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                if (Session["nuevoBrandsite"] != "1")
                {
                    Session["IdBrandSite"] = oBrandSite.ObtenerIdBrandSite(Session["NombreBrand"].ToString());
                    vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString()));
                }
                else
                {
                    vencido = 1;
                }
                //vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString()));
                if (vencido == 1)
                {
                    //ingresa el nuevo manual
                    oMetodo = oBrandSite.CrearBrandsite1(lblIdBrandSitePopUp.Text, txtNombre.Text, txtUrl.Text, "1", Session["IdUsuario"].ToString(), "0");
                    //Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                }
                else
                {
                    //actualiza el manual existente
                    oMetodo = oBrandSite.ActualizarBrandSite(lblIdBrandSitePopUp.Text, txtNombre.Text, txtUrl.Text, "1", Session["IdUsuario"].ToString(), "5");
                }


                if (oMetodo.Pass == true)
                {
                    Session["IdBrandSite"] = oBrandSite.ObtenerIdBrandSite(txtUrl.Text);
                    Session["NombreBrand"] = txtNombre.Text;
                    Session["sUrl"] = txtUrl.Text;

                    // Actualiza el logo si es una modificación

                    if (lblIdNuevoLogo.Text != "")
                    {
                        oMetodo = oBrandSite.CambiarLogo(lblIdNuevoLogo.Text, lblIdBrandSitePopUp.Text, Session["IdUsuario"].ToString());

                        if (oMetodo.Pass == true)
                        {
                            if (oBrandSite.CrearTemplate(/*lblIdBrandSitePopUp.Text*/Session["IdBrandSite"].ToString()) == true)
                            {
                                // Si termina de actualizar el logo regreso el label a vacio para su próximo uso
                                lblIdNuevoLogo.Text = "";
                            }
                        }
                    }

                    try
                    {
                        // Crea el subdominio
                        string sSubDominio = txtNombre.Text.ToLower();
                        sSubDominio = sSubDominio.Trim();

                        oAmazon.CrearSubdominio(sSubDominio);
                    }
                    catch (Exception eSubdominio)
                    {
                        oSistema.saveLog("Error al crear el subdominio con la url: " + txtUrl.Text + ".identidad.com " + eSubdominio.ToString());
                    }

                    CargarMisBrandSite();

                    ppAgregarBrandSite.Hide();

                    oBrandSite.CrearTemplate(/*lblIdBrandSitePopUp.Text*/Session["IdBrandSite"].ToString());
                    if (Session["nuevoBrandsite"].ToString() != "1")
                    {
                        if (txtUrl.Enabled)
                        {
                            Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                        }

                    }
                }
                else
                {
                    lblValidacion.Visible = true;
                    lblValidacion.Text = oMetodo.Message;
                }

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }

        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                lblUrlBrandSite.Text = "";
                lblIdBrandSitePopUp.Text = "0";
                lblIdNuevoLogo.Text = "";

                ppAgregarBrandSite.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbDesigner_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                Session["IdBrandSite"] = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;

                Response.Redirect("~/Designer/Home.aspx", false);

                //int vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString()));
                //if (vencido == 0)
                //{
                //    HttpContext.Current.ApplicationInstance.CompleteRequest();
                //    Response.Redirect("~/Designer/Home.aspx", false);
                //}
                //else
                //{
                //    Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                //    //MPE.Show();
                //}

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbEditar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdBrandSitePopUp.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;
                foreach (DataRow row in oBrandSite.cargarDatosBrandSite(lblIdBrandSitePopUp.Text).Rows)
                {
                    Session["NombreBrand"] = row["Nombre"].ToString();
                    txtNombre.Text = row["Nombre"].ToString();
                    txtUrl.Text = row["Url"].ToString().ToLower();
                    lblUrlBrandSite.Text = row["Url"].ToString();
                    //ddlEstatus.SelectedValue = row["IdEstatus"].ToString();
                }

                lblIdNuevoLogo.Text = "";
                lkbCambiarLogo.Visible = true;
                lkbEliminar.Visible = true;
                lblValidacion.Visible = false;
                imgOk.Visible = false;
                txtUrl.Enabled = false;

                ppAgregarBrandSite.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        #region Eliminar

        protected void lkbEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminarBrandSite.Show();

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                oMetodo = oBrandSite.EliminarBrandSite(lblIdBrandSitePopUp.Text, Session["IdUsuario"].ToString());

                if (oMetodo.Pass == true)
                {
                    try
                    {
                        // Elimina el subdominio
                        //oAmazon.EliminarSubdominio(oSistema.ObtenerSubdominio(lblUrlBrandSite.Text));
                        oAmazon.EliminarSubdominio(lblUrlBrandSite.Text);
                    }
                    catch (Exception eSubdominio)
                    {
                        oSistema.saveLog("Error al eliminar el subdominio con la url: " + txtUrl.Text + ".identidad.com " + eSubdominio.ToString());
                    }

                    lblUrlBrandSite.Text = "";
                    lblIdBrandSitePopUp.Text = "0";
                    CargarMisBrandSite();
                    ppEliminarBrandSite.Hide();
                }
                else
                {
                    lblConfirmarEliminar.Text = oMetodo.Message;
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelarEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ppEliminarBrandSite.Hide();
                ppAgregarBrandSite.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        #endregion

        protected void lkbUrl_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                string sIdBrandSite = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;
                Session["IdBrandSite"] = sIdBrandSite;

                //revision de que sea el usuario de diseño para que pueda entrar a todos los manuales sin importar su estado.
                int vencido;
                if (int.Parse(Session["IdUsuario"].ToString()) == 671)
                { vencido = 0; }
                else { vencido = oBrandSite.BrandSiteVencido(int.Parse(Session["IdBrandSite"].ToString())); }

                if (vencido == 0)
                {
                    sIdBrandSite = oSistema.EncriptarId(int.Parse(sIdBrandSite));

                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("~/Brandsite/Login.aspx?Id=" + sIdBrandSite, false);
                }
                else
                {
                    Response.Redirect("~/site/pagina-datos-pago.aspx", false);
                    //MPE.Show();
                }
                
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbCambiarLogo_Click(object sender, EventArgs e)
        {
            try
            {
                eExaminarLogo.Show(lblIdBrandSitePopUp.Text, "Te recomendamos seleccionar una imagen .jpg de 100 X 100 pixeles.", true);
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        // Asigno el evento a la página de mi user control examinar
        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                eExaminarLogo.ArchivoSeleccionado += new Identidad.Controles.Examinar.EventHandler(eExaminarLogo_OnArchivoSeleccionado);
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en Examinar
        private void eExaminarLogo_OnArchivoSeleccionado(object sender, EventArgs e)
        {
            try
            {
                lblIdNuevoLogo.Text = eExaminarLogo.ObtenerIdArchivoSeleccionado();

                if (lblIdNuevoLogo.Text != "")
                {
                    imgOk.Visible = true;
                }
                else
                {
                    imgOk.Visible = false;
                }

                ppAgregarBrandSite.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

    }
}