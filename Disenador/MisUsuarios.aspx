﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Disenador/Disenador.Master" AutoEventWireup="true" CodeBehind="MisUsuarios.aspx.cs" Inherits="Identidad.Disenador.WebForm4" meta:resourcekey="MisUsuarios"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="Ajax" %>
<%@ Register Src="~/Controles/MiPerfil.ascx" TagPrefix="identidad" TagName="MiPerfil" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <link href="../CSS/Pages/MisUsuarios.css" rel="stylesheet" type="text/css"/>
    <span id="chromeFix"></span>
    <div class="divFondoPagina">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                
                    <div class="divTituloCentrado">
                        <asp:Label ID="lblMisUsuarios" CssClass="fuenteTitulo" runat="server" meta:resourcekey="lblMisUsuarios"></asp:Label>
                    </div>
            
                    <div style="color: black">
                        <asp:GridView ID="gvMisBrandSite" runat="server" CssClass="mGrid gvMisBrandSite" AutoGenerateColumns="false" >
                            <Columns>

                                <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                        
                                        </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="width:100px;">
                                                        <asp:Image ID="imgLogo" CssClass="imgLogo imgCircular" runat="server" ImageUrl='<%#Eval ("UrlLogo") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblIdBrandSite" runat="server" DataField ="IdBrandSite" Text='<%#Eval ("IdBrandSite") %>' Visible="false" ></asp:Label>
                                                        <asp:Label ID="lblNombre" CssClass="MisBrandSites" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                                        <br /><br />
                                                        <a href="#usuarios">
                                                            <asp:LinkButton Id="lkbSeleccionar" runat="server" OnClick="lkbSeleccionar_Click" OnClientClick="lkbSeleccionar();" meta:resourcekey="lkbSeleccionar"></asp:LinkButton>   
                                                        </a>
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                        </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>
                    <br />
                  <!--  <asp:Button ID="btnImportar" Style="margin-left:50px;" class="buttons colorVerde" runat="server" meta:resourcekey="btnImportar" OnClick="btnImportar_Click" />-->
                    <asp:Button ID="btnNuevoUsuario" class="buttons colorVerde" Style="margin-left: 50px;" runat="server" meta:resourcekey="btnNuevoUsuario" OnClick="btnNuevoUsuario_Click" />
                    <asp:Label ID="lblMensajeMisUsuarios" CssClass="MensajeUsuarios" runat="server" Visible="false" meta:resourcekey="lblMensajeMisUsuarios"></asp:Label>
                    <br /><br />

                    <div class="divTituloCentrado">
                        <asp:Label ID="lblIdBrandSiteSeleccionado" runat="server" Visible="false" ></asp:Label>
                        <asp:Label ID="lblNombreBrandSiteSeleccionado" CssClass="fuenteTitulo" runat="server" Visible="false" ></asp:Label>
                    </div>
                
                    <asp:Label ID="lblNombreBusqueda" Style="margin-left:50px;" runat="server" meta:resourcekey="lblNombreBusqueda" Visible="false" ></asp:Label>
                    <asp:TextBox ID="txtNombre" Style="margin-left:10px;" CssClass="TextBox" runat="server" Visible="false"></asp:TextBox>
                    <asp:Button ID="btnBuscar" class="buttons colorVerde" runat="server" meta:resourcekey="btnBuscar" Visible="false"  OnClick="btnBuscar_Click"/>
                    <br /><br /><br />

                    <a name="usuarios">
                    <div style="width:100%; height:auto;">
                    <div id="Order" >
                       <!-- <asp:label id="lblOrder" runat="server" Visible="false" style="margin-left:55px;">Ordenar por:  </asp:label>
                        <asp:LinkButton ID="lkbNombre" runat="server" Text="Nombre" OnClick="lkbNombre_Click" Visible="false" style="margin-left:5px; cursor:pointer"></asp:LinkButton>
                        |
                        <asp:LinkButton ID="lkbNombreEmpresa" runat="server" Text="Empresa" OnClick="lkbNombreEmpresa_Click" Visible="false" style="margin-left:5px; cursor:pointer"></asp:LinkButton> -->
                    </div>
                    <div class="divMisUsuarios">
                        <Label id="lblUsuarios" visible="false" runat="server" style="padding-left: 50px; color: black; font-size: 20px;">Usuarios Históricos</Label>
                        <asp:GridView ID="gvMisUsuarios" runat="server" CssClass="mGrid" AutoGenerateColumns="false" Visible="false" >
                            <Columns>

                                <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblIdUsuario" runat="server" DataField ="IdUsuario" Text='<%#Eval ("IdUsuario") %>' Visible="false" ></asp:Label>
                                        </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="width:70px;">
                                                        <asp:Image ID="imgUsuario" CssClass="imgUsuario imgCircular" runat="server" ImageUrl='<%#Eval ("Foto") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNombre" CssClass="NombreUsuario" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label>
                                              
                                                        <!--<asp:Label ID="lblFechaActivo" CssClass="FechaAlta" runat="server" DataField ="FechaActivo" Text='<%#Eval ("FechaActivo") %>' ></asp:Label>-->
                                                        <br />
                                                        <asp:Label ID="lblEmpresa" runat="server" DataField ="CorreoElectronico" Text='<%#Eval ("CorreoElectronico") %>' ></asp:Label>
                                                        <br /><br />
                                                        <asp:LinkButton Id="lkbAgregarAcceso" runat="server" OnClick="lkbAgregarAcceso_Click" meta:resourcekey="lkbAgregarAcceso"></asp:LinkButton>   
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <label id="lblUsuarioLLeno" runat="server" visible="false">Los usuarios del Brand Site han llegado al tope. Para tener más usuarios favor de actualizar tu plan.</label>
                    </div>

                    <div class="divMisAccesos">
                        <Label id="lblColaboradores" visible="false" runat="server" style="padding-left: 50px; color: black; font-size: 20px;">Colaboradores en este sitio </Label>
                        <asp:GridView ID="gvAccesos" runat="server" CssClass="mGrid" AutoGenerateColumns="false" Visible="false" >
                            <Columns>

                                <asp:TemplateField Visible="false">
                                        <ItemTemplate>
                                        
                                        </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField>
                                        <ItemTemplate>
                                            <table>
                                                <tr>
                                                    <td style="width:70px;">
                                                        <asp:Image ID="imgUsuario" CssClass="imgUsuario imgCircular" runat="server" ImageUrl='<%#Eval ("Foto") %>' />
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblIdUxB" runat="server" DataField ="IdUxB" Text='<%#Eval ("IdUxB") %>' Visible="false" ></asp:Label>
                                                        <asp:Label ID="lblIdUsuario" runat="server" DataField ="IdUsuario" Text='<%#Eval ("IdUsuario") %>' Visible="false" ></asp:Label>
                                                        <asp:Label ID="lblNombre" CssClass="NombreUsuario" runat="server" DataField ="Nombre" Text='<%#Eval ("Nombre") %>' ></asp:Label> | <asp:Label ID="lblCorreoElectronico" runat="server" DataField ="CorreoElectronico" Text='<%#Eval ("CorreoElectronico") %>' ></asp:Label>
                                                       <!-- <br />
                                                        <asp:Label ID="lblEmpresa" runat="server" DataField ="Empresa" Text='<%#Eval ("NombreEmpresa") %>' ></asp:Label>
                                                        <br />--><br />
                                                        <asp:Label ID="lblEtPuedeInvitar" runat="server" text="Puede invitar " ></asp:Label>  <asp:CheckBox ID="ckbPuedeInvitar" runat="server" Checked='<%#Eval ("PuedeInvitar") %>' OnCheckedChanged="ckbPuedeInvitar_CheckedChanged" AutoPostBack="true" />
                                                        <br />
                                                        Invitado por <asp:Label ID="Label2"  runat="server" DataField ="UsuarioActualiza" Text='<%#Eval ("UsuarioActualiza") %>' ></asp:Label>
                                                        <br /><br />
                                                        <asp:LinkButton Id="lkbEliminarAcceso" runat="server" OnClick="lkbEliminarAcceso_Click" meta:resourcekey="lkbEliminarAcceso"></asp:LinkButton> | <asp:LinkButton Id="lkbReenviarUsuario" runat="server" OnClick="lkbReenviarUsuario_Click" meta:resourcekey="lkbReenviarUsuario"></asp:LinkButton>   
                                                        <br />
                                                        Puede editar el sitio 
                                                        <asp:CheckBox ID="ckbEsDeMiEquipo" runat="server" Checked='<%#Eval ("EsDeMiEquipo") %>' OnCheckedChanged="ckbEsDeMiEquipo_CheckedChanged" AutoPostBack="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        
                                        </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </div>
                    </div>
                    
                    <div>
                        <div id="divFooter"></div>
                    </div>

                    <!--Inicia Popup para importar un usuario--->
                    <panel id="pnlImportarExistente" runat="server" style="display:none" >
                        <div class="divTabla divImportarExistente">
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTituloImportarExistente" runat="server" meta:resourcekey="lblTituloImportarExistente"></asp:Label>
                            </div>
                            <hr />
                            <br />
                            <br />
                            <br />
                            <div class="divTR">
                                <div class="divTDIzq">
                                    <asp:Label ID="lblCorreoElectronico" runat="server" meta:resourcekey="lblCorreoElectronico"></asp:Label>
                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:TextBox ID="txtCorreoElectronico" CssClass="TextBox" runat="server" onblur="onLeave(this)"></asp:TextBox>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoInput">
                                    <asp:Label ID="lblValidacionImportacion" runat="server" visible="false"></asp:Label>
                                </div>

                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoButtons">
                                    
                                    <asp:Button ID="btnCancelarImportacion" style="margin-left:10px;" class="buttons colorwhite" runat="server" meta:resourcekey="btnCancelarImportacion" OnClick="btnCancelarImportacion_Click" />
                                    <asp:Button ID="btnImportarUsuario" class="buttons colorVerde" runat="server" meta:resourcekey="btnImportarUsuario" OnClick="btnImportarUsuario_Click" />

                                </div>

                            </div>
                        </div>
                    </panel>

                    <asp:Label ID="lblImportarExistente" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppImportarExistente" TargetControlID="lblImportarExistente" runat="server" PopupControlID="pnlImportarExistente" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".30" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Finaliza Popup para importar un usuario--->

                    <!--Inicia Popup para notificar envio de datos de usuario--->
                    <panel id="pnlAvisoDatosUsuario" runat="server" style="display:none" >
                        <div class="divTabla divEnvioDatos">
                            <div class="divTablaTitulo">
                                <asp:Label ID="lblTituloEnvioDatos" runat="server" meta:resourcekey="lblTituloEnvioDatos"></asp:Label>
                            </div>
                            <hr />
                            <br />
                            <br />
                            <br />
                            <div class="divTR">
                                <div class="divTDCentrado">
                                    <asp:Label ID="lblNotificarDatosUsuario" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="divTR">
                                <div class="divTDIzq">

                                </div>
                                <div class="divTDDerechoButtons">
                                    <asp:Button ID="btnOk" class="buttons colorVerde" runat="server" meta:resourcekey="btnOk" OnClick="btnOk_Click" />
                                </div>
                            </div>
                        </div>
                    </panel>

                    <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppAvisoDatosUsuario" TargetControlID="Label3" runat="server" PopupControlID="pnlAvisoDatosUsuario" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".30" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Termina Popup notificar envio de datos de usuario--->
                
                    <!--Inicia Popup para crear un nuevo usuario--->
                    <panel id="pnlNuevoUsuario" runat="server" style="display:none;" >
                        <div >
                            <identidad:MiPerfil runat="server" ID="MiPerfil" />
                        </div>
                    </panel>

                    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                    <Ajax:ModalPopupExtender ID="ppNuevoUsuario" TargetControlID="Label1" runat="server" PopupControlID="pnlNuevoUsuario" BackgroundCssClass="modalBackground" RepositionMode="RepositionOnWindowResizeAndScroll">
                        <Animations>
                            <OnShown>
                                <FadeIn Duration=".10" Fps="48" />                
                            </OnShown>
                        </Animations>
                    </Ajax:ModalPopupExtender>
                    <!--Termina Popup para crear nuevo usuario--->

                </ContentTemplate>
            </asp:UpdatePanel>


    </div>

    <script type="text/javascript">

        function lkbSeleccionar()
        {
            location.hash = '#usuarios';   
        }

        // Se muestra el menú seleccionado
        document.getElementById("lblMisUsuarios").setAttribute("class", "Seleccionado");

        // Si hay un postback tengo que cargar por segunda vez por que no entra al script

        //Sys.Application.initialize();

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        if (prm != null) {
            prm.add_endRequest
            (
                function (sender, e) {
                    if (sender._postBackSettings.panelsToUpdate != null) {
                        document.getElementById("lblMisUsuarios").setAttribute("class", "Seleccionado");
                    }
                }
            );
        };

    </script>


</asp:Content>
