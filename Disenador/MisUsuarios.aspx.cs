﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using Identidad.App_Code;

namespace Identidad.Disenador
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        // Estan son las variables que voy a usar en esta página :)

        #region Variables

        cBrandSite oBrandSite = new cBrandSite();
        cUsuario oUsuario = new cUsuario();
        cSistema oSistema = new cSistema();
        cMetodo oMetodo = new cMetodo();

        #endregion

        // Este es mi metodo Page_Load, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (Session["IdUsuario"] == null)
                    {
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                        Response.Redirect("~/site/login-registro.aspx", false);

                        return;
                    }
                    else
                    {
                        CargarBrandSites();
                    }
                }
                else
                    if (Request.Form["__EVENTTARGET"] == "ContentPlaceHolder1_MiPerfil_ComboBox1")
                        ppNuevoUsuario.Show();
                CargarBrandSites();
            }
            catch (Exception Error)
            {
                //oSistema.saveLog(Error.ToString());

                HttpContext.Current.ApplicationInstance.CompleteRequest();
                Response.Redirect("~/site/login-registro.aspx", false);
            }
        }

        // Estos son los métodos o funciones de la página, un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void CargarBrandSites()
        {
            oBrandSite.CargarMisBrandSites(gvMisBrandSite, Session["IdUsuario"].ToString());

            if (gvMisBrandSite.Rows.Count == 0)
            {
                botonesPrincipalesVisibles(false);

                lblMensajeMisUsuarios.Visible = true;
            }
            else
            {
                botonesPrincipalesVisibles(true);

                lblMensajeMisUsuarios.Visible = false;
            }

        } //Carga los sitios para poder seleccionar uno

        private void botonesPrincipalesVisibles(bool bVisibles)
        {
            btnImportar.Visible = bVisibles;
            btnNuevoUsuario.Visible = bVisibles;
        } // hace visible o invisible un par de botones

        private void ControlesVisibles(bool bVisibles)
        {
            lblNombreBusqueda.Text = "Correo Electronico";
            lblNombreBrandSiteSeleccionado.Visible = bVisibles;
            lblNombreBusqueda.Visible = bVisibles;
            txtNombre.Visible = bVisibles;
            btnBuscar.Visible = bVisibles;
            gvMisUsuarios.Visible = bVisibles;
            gvAccesos.Visible = bVisibles;
            lblColaboradores.Visible = bVisibles;
            lblUsuarios.Visible = bVisibles;
            lblOrder.Visible = bVisibles;
            lkbNombre.Visible = bVisibles;
            lkbNombreEmpresa.Visible = bVisibles;
        } // Hace los controles visibles o invisibles segun sea necesario en la página

       private void CargarMisUsuarios(string order="Nombre")
        {
            if (lblIdBrandSiteSeleccionado.Text != "")
            {
                oBrandSite.CargarMisUsuarios2(gvMisUsuarios, Session["IdUsuario"].ToString(), txtNombre.Text, lblIdBrandSiteSeleccionado.Text, order);
            }
        } // Carga los usuarioshistoricos del manual.

       private void BuscarUsuarios(string order = "Nombre")
       {
           if (lblIdBrandSiteSeleccionado.Text != "")
           {
               oBrandSite.BuscarUsuario(gvMisUsuarios, Session["IdUsuario"].ToString(), txtNombre.Text, lblIdBrandSiteSeleccionado.Text, order);
           }
       } // busca los usuarioshistoricos del manual

        
        private void CargarAccesos(string order = "Nombre")
        {
            if (lblIdBrandSiteSeleccionado.Text != "")
            {
                oBrandSite.CargarAccesosBrandSite(gvAccesos, lblIdBrandSiteSeleccionado.Text, order);
            }
        } // Carga los accesos o usuarios que tengan acceso a un sitio en especifico

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void lkbSeleccionar_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                lblIdBrandSiteSeleccionado.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;
                lblNombreBrandSiteSeleccionado.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblNombre")))).Text;

                ControlesVisibles(true);

                CargarMisUsuarios();

                CargarAccesos();
                lblUsuarioLLeno.Visible = false;

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnNuevoUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                MiPerfil.setIdUsuario("0", true);
                ppNuevoUsuario.Show();
                
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
                Response.Write("<script>alert('error')</script>");
            }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                MiPerfil.EstadoCatalogo += new Identidad.Controles.MiPerfil.EventHandler(MiPerfil_OnEstadoCatalogo);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        // Código que entra cuando se dispara el evento en MiPerfil
        private void MiPerfil_OnEstadoCatalogo(object sender, EventArgs e)
        {
            try
            {
                oMetodo = MiPerfil.ObtenerEstado();

                if (oMetodo.Pass == false)
                {
                    ppNuevoUsuario.Show();
                }
                else
                {
                    ppNuevoUsuario.Hide();

                    CargarMisUsuarios();

                    CargarAccesos();
                }

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbEliminarAcceso_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                oBrandSite.EliminarAccesoBrandSite(((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblIdUxB")))).Text, Session["IdUsuario"].ToString());

                CargarAccesos();

                CargarMisUsuarios();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbReenviarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton Status = (LinkButton)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                string sCorreoElectronico = ((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblCorreoElectronico")))).Text;

                oMetodo = oUsuario.RecuperarMiContraseña(sCorreoElectronico);

                ppAvisoDatosUsuario.Show();
                lblNotificarDatosUsuario.Text = oMetodo.Message;

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                ppAvisoDatosUsuario.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbAgregarAcceso_Click(object sender, EventArgs e)
        {
            try
            {
                string sidBrand = lblIdBrandSiteSeleccionado.Text;
                if (oUsuario.ValidarCantUsuarios(sidBrand))
                {
                    LinkButton Status = (LinkButton)sender;
                    int Row = ((GridViewRow)Status.NamingContainer).RowIndex;

                    oBrandSite.InsertarAccesoBrandSite(((System.Web.UI.WebControls.Label)((gvMisUsuarios.Rows[Row].FindControl("lblIdUsuario")))).Text, "3", lblIdBrandSiteSeleccionado.Text, Session["IdUsuario"].ToString());

                    CargarMisUsuarios();

                    CargarAccesos();
                }
                else
                    lblUsuarioLLeno.Visible = true;

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                BuscarUsuarios();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnImportar_Click(object sender, EventArgs e)
        {
            try
            {
                lblValidacionImportacion.Visible = false;
                txtCorreoElectronico.Text = "";
                ppImportarExistente.Show();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnImportarUsuario_Click(object sender, EventArgs e)
        {
            try
            {
                ppImportarExistente.Show();

                oMetodo = oBrandSite.ImportarUsuario(txtCorreoElectronico.Text, Session["IdUsuario"].ToString());

                if (oMetodo.Pass == true)
                {
                    lblValidacionImportacion.Visible = false;
                    ppImportarExistente.Hide();
                    CargarMisUsuarios();
                }
                else
                {
                    lblValidacionImportacion.Text = oMetodo.Message;
                    lblValidacionImportacion.Visible = true;
                }
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void btnCancelarImportacion_Click(object sender, EventArgs e)
        {
            try
            {
                ppImportarExistente.Hide();
            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ckbPuedeInvitar_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox Status = (CheckBox)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                int IdUxB = int.Parse(((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblIdUxB")))).Text);
                int IdUsuario = int.Parse(((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblIdUsuario")))).Text);
                bool bPuedeInvitar = ((System.Web.UI.WebControls.CheckBox)((gvAccesos.Rows[Row].FindControl("ckbPuedeInvitar")))).Checked;

                oBrandSite.EdicionInvitadoBrandSite(IdUxB, IdUsuario, int.Parse(lblIdBrandSiteSeleccionado.Text), int.Parse(Session["IdUsuario"].ToString()), bPuedeInvitar);
                

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void ckbEsDeMiEquipo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox Status = (CheckBox)sender;
                int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                int IdUxB = int.Parse(((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblIdUxB")))).Text);
                int IdUsuario = int.Parse(((System.Web.UI.WebControls.Label)((gvAccesos.Rows[Row].FindControl("lblIdUsuario")))).Text);
                bool bEsDeMiEquipo = ((System.Web.UI.WebControls.CheckBox)((gvAccesos.Rows[Row].FindControl("ckbEsDeMiEquipo")))).Checked;

                oBrandSite.EdicionEsDeMiEquipo(IdUxB, IdUsuario, int.Parse(lblIdBrandSiteSeleccionado.Text), int.Parse(Session["IdUsuario"].ToString()), bEsDeMiEquipo);

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        #endregion

        protected void lkbNombre_Click(object sender, EventArgs e)
        {
            try
            {
                //LinkButton Status = (LinkButton)sender;
                //int Row = ((GridViewRow)Status.NamingContainer).RowIndex;
                //lblIdBrandSiteSeleccionado.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblIdBrandSite")))).Text;
                //lblNombreBrandSiteSeleccionado.Text = ((System.Web.UI.WebControls.Label)((gvMisBrandSite.Rows[Row].FindControl("lblNombre")))).Text;

                ControlesVisibles(true);

                CargarMisUsuarios("Nombre");

                CargarAccesos("Nombre");

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }

        protected void lkbNombreEmpresa_Click(object sender, EventArgs e)
        {
            try
            {


                ControlesVisibles(true);

                CargarMisUsuarios("NombreEmpresa");

                CargarAccesos("NombreEmpresa");

            }
            catch (Exception error)
            {
                oSistema.saveLog(error.ToString());
            }
        }
    }
}