﻿using System;
using System.Web;
using Identidad.App_Code;

namespace Identidad
{
    public partial class _default : System.Web.UI.Page
    {
        string sJson = "";
        // Estan son las variables que voy a usar en esta página :)
        #region Variables

        cUsuario oUsuario = new cUsuario();
        cMetodo oMetodo = new cMetodo();
        cSistema oSistema = new cSistema();
        cBrandSite oBrandSite = new cBrandSite();
        string NombresRegistro;
        string ApellidosRegistro;
        string EmailRegistro;
        string PasswordRegistro;
        string PasswordConfirmRegistro;
        string AceptaTerminos;

        #endregion

        // Este es mi metodo Page_Load :) para los que no saben, este se dispara al cargar la página

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Redirect("~/site/index.aspx", true);
            ClientScript.GetPostBackEventReference(this, string.Empty);

            if (Request.Form["__EVENTTARGET"] == "Pagar_Click")
            {
                //llamamos el metodo que queremos ejecutar, en este caso el evento onclick del boton
                sJson = Page.Request.Params["__EVENTARGUMENT"];
                if (sJson.ToString() != "")
                {
                    Pagar_Click(this, new EventArgs());
                }
            }

            try
            {
                if (!IsPostBack)
                {

                    //lblValidacion.Visible = false;

                    Response.CacheControl = "private";
                    Response.ExpiresAbsolute = DateTime.Now.AddDays(-1d);
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);

                    NavegadorValido();
                    ObtenerSubDominio();
                }
                else
                {
                    string eventTarget = Convert.ToString(Request.Params.Get("__EVENTTARGET"));
                    if (eventTarget == "validaEnviar")
                    {
                        btnEnviar_Click();
                    }
                    else if (eventTarget == "mPYME")
                    {
                        btnComienzaPYME_Click();
                    }
                    else if (eventTarget == "mEmpresa")
                    {
                        btnComienzaEmpresa_Click();
                    }
                    else if (eventTarget == "mCorporativo")
                    {
                        btnCorporativo_Click();
                    }
                    else if (eventTarget == "mPremium")
                    {
                        btnPremium_Click();
                    }
                    else if (eventTarget == "mComienza1")
                    {
                        btnComienza1_Click();
                    }
                    else if (eventTarget == "mComienza2")
                    {
                        btnComienza2_Click();
                    }
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        // Estos son los métodos o funciones de la página, para los que no saben un método consiste generalmente de una serie de sentencias para llevar a cabo una acción, un juego de parámetros de entrada que regularán dicha acción o, posiblemente, un valor de salida (o valor de retorno) de algún tipo.

        #region Métodos

        private void ObtenerSubDominio()
        {
            try
            {
                string sUrl = HttpContext.Current.Request.Url.AbsoluteUri;

                string sNombreBrandSite = oSistema.ObtenerSubdominio(sUrl); //url.Substring(7).Split('.')[0];

                if (sNombreBrandSite != "identidad")
                {
                    int IdBrandSite = oBrandSite.ObtenerIdBrandSite(sNombreBrandSite);

                    if (IdBrandSite != 0)
                    {
                        string sIdBrandSiteEncriptado = oSistema.EncriptarId(IdBrandSite);

                        Response.Redirect("~/Brandsite/Login.aspx?Id=" + sIdBrandSiteEncriptado, false);
                    }
                }
            }
            catch (Exception e)
            {
                oSistema.saveLog(e.ToString());
            }
        }

        public void NavegadorValido()
        {
            System.Web.HttpBrowserCapabilities browser = Request.Browser;
            string s = "Browser Capabilities\n"
                + "Type = " + browser.Type + "\n"
                + "Name = " + browser.Browser + "\n"
                + "Version = " + browser.Version + "\n"
                + "Major Version = " + browser.MajorVersion + "\n"
                + "Minor Version = " + browser.MinorVersion + "\n"
                + "Platform = " + browser.Platform + "\n"
                + "Is Beta = " + browser.Beta + "\n"
                + "Is Crawler = " + browser.Crawler + "\n"
                + "Is AOL = " + browser.AOL + "\n"
                + "Is Win16 = " + browser.Win16 + "\n"
                + "Is Win32 = " + browser.Win32 + "\n"
                + "Supports Frames = " + browser.Frames + "\n"
                + "Supports Tables = " + browser.Tables + "\n"
                + "Supports Cookies = " + browser.Cookies + "\n"
                + "Supports VBScript = " + browser.VBScript + "\n"
                + "Supports JavaScript = " + browser.EcmaScriptVersion.ToString() + "\n"
                + "Supports Java Applets = " + browser.JavaApplets + "\n"
                + "Supports ActiveX Controls = " + browser.ActiveXControls + "\n";

            if (browser.Browser == "InternetExplorer")
            {
                if (float.Parse(browser.Version) < 9)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                    Response.Redirect("/Browser.html", false);
                }
            }
        }

        #endregion

        // Estos son los eventos que usare en esta página tales como clicks, index changed, etc. Para los que no saben se disparan por medio de un control en el HTML :)

        #region Eventos

        protected void lkbIniciarSesion_Click(object sender, EventArgs e)
        {
            try
            {
                txtUsuario.Text = "";
                txtContraseña.Text = "";
                lblValidacion.Visible = true;
                txtUsuario.Focus();
                                
                ppIniciarSesion.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbContactanos_Click(object sender, EventArgs e)
        {
            try
            {
                ppContactanos.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbRegistro_Click(object sender, EventArgs e)
        {
            try
            {
                ppContactanos.Hide();
                ppRegistro.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
        
        protected void lkbConocenos_Click(object sender, EventArgs e)
        {
            try
            {
                //Response.Write("<script>window.open('IDEN_Bienvenidos_documento.pdf','_blank');</script>");
                //Response.Redirect("IDEN_Bienvenidos_documento.pdf");
                //NavigareURL = "IDEN_Bienvenidos_documento.pdf";
                //Response.Write("<script>");
                //Response.Write("window.open('IDEN_Bienvenidos_documento.pdf','_blank','resizable=yes,scrollbars=yes,toolbar=yes,menubar=yes,location=no')");
                //Response.Write("</script>");
                //    ppConocenos.Show();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            //lblValidacion.Attributes["class"] = "divValidacion";
            try
            {
                //ppIniciarSesion.Show();

                //oMetodo = oUsuario.IniciarSesion(txtUsuario.Text, txtContraseña.Text);
                oMetodo = oUsuario.IniciarSesion(txtRCorreo.Text, txtRPassword.Text);

                if (oMetodo.Pass == true)
                {
                    //Session["IdUsuario"] = oUsuario.getIdUsuario(txtUsuario.Text);
                    Session["IdUsuario"] = oUsuario.getIdUsuario(txtRCorreo.Text);
                    Session["FotoUsuario"] = oUsuario.getFotoPerfil(Session["IdUsuario"].ToString());
                    Session.Timeout = 480;

                    oUsuario.getMisSitios(ddlMisSitios, Session["IdUsuario"].ToString());

                    if (ddlMisSitios.Items.Count > 1)
                    {
                        ppIniciarSesion.Hide();

                        HttpContext.Current.ApplicationInstance.CompleteRequest();

                        oUsuario.getMisSitios(ddlMisSitios, Session["IdUsuario"].ToString());

                        ppMisSitios.Show();
                    }
                    else
                    {
                        if (ddlMisSitios.Items.Count != 0)
                        {
                            HttpContext.Current.ApplicationInstance.CompleteRequest();

                            string[] sUrl = ddlMisSitios.Items[0].Value.Split('=');
                            string sUrlFinal;

                            if (sUrl.Length == 1)
                            {
                                sUrlFinal = sUrl[0];
                            }
                            else
                            {
                                sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                            }

                            Response.Redirect("~/" + sUrlFinal, false);

                        }
                        else
                        {
                            lblValidacion.Visible = true;
                            lblValidacion.Text = "¡Sitio no disponible!";
                        }
                    }
                }
                else
                {
                    txtRCorreo.Focus();

                    lblIniciarSesion.Visible = true;
                    lblIniciarSesion.Text = oMetodo.Message;
                    
                }

            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                ppIniciarSesion.Hide();
                ppMisSitios.Hide();
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlMisSitios.Items.Count != 0)
                {
                    HttpContext.Current.ApplicationInstance.CompleteRequest();

                    string [] sUrl = ddlMisSitios.SelectedValue.Split('=');
                    string sUrlFinal;

                    if (sUrl.Length == 1)
                    {
                        sUrlFinal = sUrl[0];
                    }
                    else
                    {
                        sUrlFinal = sUrl[0] + "=" + oSistema.EncriptarId(int.Parse(sUrl[1]));
                    }

                    Response.Redirect("~/" + sUrlFinal, false);
                }
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }

        protected void lkbRecuperarContraseña_Click(object sender, EventArgs e)
        {
            try
            {
                //ppIniciarSesion.Show();

                //oMetodo = oUsuario.RecuperarMiContraseña(txtUsuario.Text);
                oMetodo = oUsuario.RecuperarMiContraseña(txtRCorreo.Text);

                lblValidacion.Text = oMetodo.Message;
                lblValidacion.Visible = true;
                lblValidacion.Attributes["class"] = lblValidacion.Attributes["class"].ToString() + " divValidacionVerde";
                
            }
            catch (Exception Error)
            {
                oSistema.saveLog(Error.ToString());
            }
        }
        
        protected void btnContactanos_Click(object sender, EventArgs e)
        {
            ppContactanos.Show();
        }
        
        protected void btnEnviar_Click()
        {
            if (txtCorreo.Text != "" || txtComentarios.Text != "" || txtNombre.Text != "")
            {

            oMetodo = oUsuario.EnviarCorreoContactanos("5", txtCorreo.Text + " (" + txtNombre.Text + ") quiere ponerse en contacto contigo.", txtComentarios.Text);
            oMetodo = oUsuario.EnviarCorreoContactanos("8",txtCorreo.Text +" (" + txtNombre.Text + ") quiere ponerse en contacto contigo.", txtComentarios.Text);
            txtCorreo.Text = "";
            txtComentarios.Text = "";
            txtNombre.Text = "";
            }
            else
            {
                lblmessage.Visible = true;
            }
        }
        
        public string NavigareURL { get; set; }

        protected void lkbRegistroS_Click(object sender, EventArgs e)
        {
            NombresRegistro = txtNombreRegistro.Text.ToString();
            ApellidosRegistro = txtApellidoRegistro.Text.ToString();
            EmailRegistro = txtCorreoRegistro.Text.ToString();
            PasswordRegistro = txtPassRegistro.Text.ToString();
            ppRegistroSeleccion.Show();
        }

        protected void lkbPago_Click(object sender, EventArgs e)
        {
            ppPagos.Show();
        }

        protected void btnRegistroAceptar_Click(object sender, EventArgs e)
        {//funcion para la validación de los campos en el pop           
            NombresRegistro = txtNombreRegistro.Text;
            ApellidosRegistro = txtApellidoRegistro.Text;
            EmailRegistro = txtCorreoRegistro.Text;
            PasswordRegistro = txtPassRegistro.Text;
            PasswordConfirmRegistro = txtPassWRegistro.Text;
            AceptaTerminos = c5.Checked.ToString();

            if (NombresRegistro == "" || ApellidosRegistro == "" || EmailRegistro == "" || PasswordRegistro == "" || PasswordConfirmRegistro == "" || NombresRegistro == null || ApellidosRegistro == null || EmailRegistro == null || PasswordRegistro == null)
            {
                lblMensajeRegistro.Text = "Debe llenar todos los campos";
                lblMensajeRegistro.Visible = true;
                ppRegistro.Show();
            }
            else if (PasswordRegistro != PasswordConfirmRegistro)
            {
                lblMensajeRegistro.Text = "Las contraseña no coiciden";
                lblMensajeRegistro.Visible = true;
                ppRegistro.Show();
            }
            else if (AceptaTerminos == "False")
            {
                lblMensajeRegistro.Text = "Debe aceptar los terminos y condiciones";
                lblMensajeRegistro.Visible = true;
                ppRegistro.Show();
            }
            else if (NombresRegistro != "" && ApellidosRegistro != "" && EmailRegistro != "" && PasswordRegistro != "" && PasswordConfirmRegistro != "" && PasswordRegistro== PasswordConfirmRegistro && AceptaTerminos == "True")
            {
                ppRegistroSeleccion.Show();
                
                
            }
                                      
        }

        protected void btnComienza1_Click()
        {
            NombrePlan.Text = "Plan 1";
            lblmes.Text="$10.00";
            lblanual.Text = "$110.00";
            ppPagos.Show();
        }

        protected void btnComienza2_Click()
        {
            NombrePlan.Text = "Plan 2";
            lblmes.Text = "$20.00";
            lblanual.Text = "$220.00";
            ppPagos.Show();
        }

        protected void btnComienzaPYME_Click()
        {
            NombrePlan.Text = "PYME";
            lblmes.Text = "$49.00";
            lblanual.Text = "$539.00";
            ppPagos.Show();
        }

        protected void btnComienzaEmpresa_Click()
        {
            NombrePlan.Text = "Empresa";
            lblmes.Text = "$139.00";
            lblanual.Text = "$1,529.00";
            ppPagos.Show();
        }

        protected void btnCorporativo_Click()
        {
            NombrePlan.Text = "Corporativo";
            lblmes.Text = "$259.00";
            lblanual.Text = "$2,849.00";
            ppPagos.Show();
        }

        protected void btnPremium_Click()
        {
            NombrePlan.Text = "Premium";
            lblmes.Text = "$599.00";
            lblanual.Text = "$6,589.00";
            ppPagos.Show();
        }

        protected void Pagar_Click(object sender, EventArgs e)
        {
            if (txtNombresdf.Value != "" || txtRFCdf.Value != "" || txtApellidosdf.Value != ""|| txtCalledf.Value != "" )
            {
                
            string sURL = "";

            Request.Headers.Add("Accept", "application/vnd.conekta-v1.0.0+json");
            Request.Headers.Add("Content-type", "application/json");

            sURL = "https://api.conekta.io/charges";
            //sJson = "{\"description\": \"Descripcion \",\"amount\": 100,\"currency\":\"MXN\",\"card\": \"tok_test_visa_4242\",\"details\": {\"name\": \"Beddy Soto\",\"phone\": \"00000000000\",\"email\": \"beddy@beduardo.com\",\"line_items\": [{\"name\": \"Box of Cohiba S1s\",\"description\": \"Imported From Mex.\",\"unit_price\": 20000,\"quantity\": 1}] } }";
            var response = cHTTPrequest.ObtenerListaWS(sURL, sJson);
            //Label1.Text = response;
            string[] tokens = response.Split(',');
            Label1.Text = response;
                
            }
            else
            {
                ppPagos.Show();
                lblErrorPagos.Visible = true;
                lblErrorPagos.Text = "Faltan llenar campos de facturación o volver a elegir el Plan";
            }

        }


        #endregion


    }
}